package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.os.Build.VERSION;
import android.support.v7.internal.view.k;
import android.support.v7.internal.view.menu.i;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window.Callback;

class aj
  extends k
{
  aj(ag paramag, Window.Callback paramCallback)
  {
    super(paramCallback);
  }
  
  private boolean a()
  {
    if ((Build.VERSION.SDK_INT < 16) && ((this.a.c instanceof Activity))) {}
    while ((this.a.c instanceof Dialog)) {
      return true;
    }
    return false;
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
  {
    if (this.a.a(paramKeyEvent)) {
      return true;
    }
    return super.dispatchKeyEvent(paramKeyEvent);
  }
  
  public boolean dispatchKeyShortcutEvent(KeyEvent paramKeyEvent)
  {
    if (this.a.a(paramKeyEvent.getKeyCode(), paramKeyEvent)) {
      return true;
    }
    return super.dispatchKeyShortcutEvent(paramKeyEvent);
  }
  
  public void onContentChanged() {}
  
  public boolean onCreatePanelMenu(int paramInt, Menu paramMenu)
  {
    if ((paramInt == 0) && (!(paramMenu instanceof i))) {
      return false;
    }
    return super.onCreatePanelMenu(paramInt, paramMenu);
  }
  
  public boolean onMenuOpened(int paramInt, Menu paramMenu)
  {
    if (this.a.b(paramInt, paramMenu)) {
      return true;
    }
    return super.onMenuOpened(paramInt, paramMenu);
  }
  
  public void onPanelClosed(int paramInt, Menu paramMenu)
  {
    if (this.a.a(paramInt, paramMenu)) {
      return;
    }
    super.onPanelClosed(paramInt, paramMenu);
  }
  
  public boolean onPreparePanel(int paramInt, View paramView, Menu paramMenu)
  {
    if ((paramInt == 0) && (!(paramMenu instanceof i))) {}
    do
    {
      return false;
      if ((paramInt != 0) || (!a())) {
        break;
      }
      if ((this.a.c instanceof Activity)) {
        return ((Activity)this.a.c).onPrepareOptionsMenu(paramMenu);
      }
    } while (!(this.a.c instanceof Dialog));
    return ((Dialog)this.a.c).onPrepareOptionsMenu(paramMenu);
    return super.onPreparePanel(paramInt, paramView, paramMenu);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/app/aj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */