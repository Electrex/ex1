package android.support.v7.app;

import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.y;
import android.view.Window.Callback;

final class at
  implements y
{
  private at(AppCompatDelegateImplV7 paramAppCompatDelegateImplV7) {}
  
  public void a(i parami, boolean paramBoolean)
  {
    i locali = parami.p();
    if (locali != parami) {}
    AppCompatDelegateImplV7.PanelFeatureState localPanelFeatureState;
    for (int i = 1;; i = 0)
    {
      AppCompatDelegateImplV7 localAppCompatDelegateImplV7 = this.a;
      if (i != 0) {
        parami = locali;
      }
      localPanelFeatureState = AppCompatDelegateImplV7.a(localAppCompatDelegateImplV7, parami);
      if (localPanelFeatureState != null)
      {
        if (i == 0) {
          break;
        }
        AppCompatDelegateImplV7.a(this.a, localPanelFeatureState.a, localPanelFeatureState, locali);
        AppCompatDelegateImplV7.a(this.a, localPanelFeatureState, true);
      }
      return;
    }
    AppCompatDelegateImplV7.a(this.a, localPanelFeatureState, paramBoolean);
  }
  
  public boolean a(i parami)
  {
    if ((parami == null) && (this.a.e))
    {
      Window.Callback localCallback = this.a.m();
      if ((localCallback != null) && (!this.a.l())) {
        localCallback.onMenuOpened(8, parami);
      }
    }
    return true;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/app/at.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */