package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.res.Configuration;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

public abstract class af
{
  public static af a(Activity paramActivity, ae paramae)
  {
    if (Build.VERSION.SDK_INT >= 11) {
      return new ak(paramActivity, paramActivity.getWindow(), paramae);
    }
    return new AppCompatDelegateImplV7(paramActivity, paramActivity.getWindow(), paramae);
  }
  
  public static af a(Dialog paramDialog, ae paramae)
  {
    if (Build.VERSION.SDK_INT >= 11) {
      return new ak(paramDialog.getContext(), paramDialog.getWindow(), paramae);
    }
    return new AppCompatDelegateImplV7(paramDialog.getContext(), paramDialog.getWindow(), paramae);
  }
  
  public abstract a a();
  
  public abstract void a(int paramInt);
  
  public abstract void a(Configuration paramConfiguration);
  
  public abstract void a(Bundle paramBundle);
  
  public abstract void a(Toolbar paramToolbar);
  
  public abstract void a(View paramView);
  
  public abstract void a(View paramView, ViewGroup.LayoutParams paramLayoutParams);
  
  public abstract void a(CharSequence paramCharSequence);
  
  public abstract MenuInflater b();
  
  public abstract void b(Bundle paramBundle);
  
  public abstract void b(View paramView, ViewGroup.LayoutParams paramLayoutParams);
  
  public abstract boolean b(int paramInt);
  
  public abstract void c();
  
  public abstract void d();
  
  public abstract void e();
  
  public abstract void f();
  
  public abstract h g();
  
  public abstract void h();
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/app/af.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */