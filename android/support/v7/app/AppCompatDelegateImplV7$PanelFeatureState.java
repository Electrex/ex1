package android.support.v7.app;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.a.k;
import android.support.v7.a.l;
import android.support.v7.internal.view.menu.g;
import android.support.v7.internal.view.menu.y;
import android.support.v7.internal.view.menu.z;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;

final class AppCompatDelegateImplV7$PanelFeatureState
{
  int a;
  int b;
  int c;
  int d;
  int e;
  int f;
  ViewGroup g;
  View h;
  View i;
  android.support.v7.internal.view.menu.i j;
  g k;
  Context l;
  boolean m;
  boolean n;
  boolean o;
  public boolean p;
  boolean q;
  boolean r;
  Bundle s;
  
  AppCompatDelegateImplV7$PanelFeatureState(int paramInt)
  {
    this.a = paramInt;
    this.q = false;
  }
  
  z a(y paramy)
  {
    if (this.j == null) {
      return null;
    }
    if (this.k == null)
    {
      this.k = new g(this.l, android.support.v7.a.i.abc_list_menu_item_layout);
      this.k.a(paramy);
      this.j.a(this.k);
    }
    return this.k.a(this.g);
  }
  
  void a(Context paramContext)
  {
    TypedValue localTypedValue = new TypedValue();
    Resources.Theme localTheme = paramContext.getResources().newTheme();
    localTheme.setTo(paramContext.getTheme());
    localTheme.resolveAttribute(android.support.v7.a.b.actionBarPopupTheme, localTypedValue, true);
    if (localTypedValue.resourceId != 0) {
      localTheme.applyStyle(localTypedValue.resourceId, true);
    }
    localTheme.resolveAttribute(android.support.v7.a.b.panelMenuListTheme, localTypedValue, true);
    if (localTypedValue.resourceId != 0) {
      localTheme.applyStyle(localTypedValue.resourceId, true);
    }
    for (;;)
    {
      android.support.v7.internal.view.b localb = new android.support.v7.internal.view.b(paramContext, 0);
      localb.getTheme().setTo(localTheme);
      this.l = localb;
      TypedArray localTypedArray = localb.obtainStyledAttributes(l.Theme);
      this.b = localTypedArray.getResourceId(l.Theme_panelBackground, 0);
      this.f = localTypedArray.getResourceId(l.Theme_android_windowAnimationStyle, 0);
      localTypedArray.recycle();
      return;
      localTheme.applyStyle(k.Theme_AppCompat_CompactMenu, true);
    }
  }
  
  void a(android.support.v7.internal.view.menu.i parami)
  {
    if (parami == this.j) {}
    do
    {
      return;
      if (this.j != null) {
        this.j.b(this.k);
      }
      this.j = parami;
    } while ((parami == null) || (this.k == null));
    parami.a(this.k);
  }
  
  public boolean a()
  {
    boolean bool = true;
    if (this.h == null) {
      bool = false;
    }
    while ((this.i != null) || (this.k.a().getCount() > 0)) {
      return bool;
    }
    return false;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/app/AppCompatDelegateImplV7$PanelFeatureState.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */