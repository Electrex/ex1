package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.support.v7.a.b;
import android.support.v7.a.g;
import android.support.v7.a.l;
import android.support.v7.internal.widget.bh;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

class r
{
  private TextView A;
  private TextView B;
  private View C;
  private ListAdapter D;
  private int E = -1;
  private int F;
  private int G;
  private int H;
  private int I;
  private int J;
  private int K;
  private int L = 0;
  private Handler M;
  private final View.OnClickListener N = new s(this);
  private final Context a;
  private final au b;
  private final Window c;
  private CharSequence d;
  private CharSequence e;
  private ListView f;
  private View g;
  private int h;
  private int i;
  private int j;
  private int k;
  private int l;
  private boolean m = false;
  private Button n;
  private CharSequence o;
  private Message p;
  private Button q;
  private CharSequence r;
  private Message s;
  private Button t;
  private CharSequence u;
  private Message v;
  private ScrollView w;
  private int x = 0;
  private Drawable y;
  private ImageView z;
  
  public r(Context paramContext, au paramau, Window paramWindow)
  {
    this.a = paramContext;
    this.b = paramau;
    this.c = paramWindow;
    this.M = new z(paramau);
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(null, l.AlertDialog, b.alertDialogStyle, 0);
    this.F = localTypedArray.getResourceId(l.AlertDialog_android_layout, 0);
    this.G = localTypedArray.getResourceId(l.AlertDialog_buttonPanelSideLayout, 0);
    this.H = localTypedArray.getResourceId(l.AlertDialog_listLayout, 0);
    this.I = localTypedArray.getResourceId(l.AlertDialog_multiChoiceItemLayout, 0);
    this.J = localTypedArray.getResourceId(l.AlertDialog_singleChoiceItemLayout, 0);
    this.K = localTypedArray.getResourceId(l.AlertDialog_listItemLayout, 0);
    localTypedArray.recycle();
  }
  
  private void a(Button paramButton)
  {
    LinearLayout.LayoutParams localLayoutParams = (LinearLayout.LayoutParams)paramButton.getLayoutParams();
    localLayoutParams.gravity = 1;
    localLayoutParams.weight = 0.5F;
    paramButton.setLayoutParams(localLayoutParams);
  }
  
  private static boolean a(Context paramContext)
  {
    TypedValue localTypedValue = new TypedValue();
    paramContext.getTheme().resolveAttribute(b.alertDialogCenterButtons, localTypedValue, true);
    return localTypedValue.data != 0;
  }
  
  static boolean a(View paramView)
  {
    if (paramView.onCheckIsTextEditor()) {
      return true;
    }
    if (!(paramView instanceof ViewGroup)) {
      return false;
    }
    ViewGroup localViewGroup = (ViewGroup)paramView;
    int i1 = localViewGroup.getChildCount();
    while (i1 > 0)
    {
      i1--;
      if (a(localViewGroup.getChildAt(i1))) {
        return true;
      }
    }
    return false;
  }
  
  private boolean a(ViewGroup paramViewGroup)
  {
    if (this.C != null)
    {
      ViewGroup.LayoutParams localLayoutParams = new ViewGroup.LayoutParams(-1, -2);
      paramViewGroup.addView(this.C, 0, localLayoutParams);
      this.c.findViewById(g.title_template).setVisibility(8);
      return true;
    }
    this.z = ((ImageView)this.c.findViewById(16908294));
    int i1;
    if (!TextUtils.isEmpty(this.d)) {
      i1 = 1;
    }
    while (i1 != 0)
    {
      this.A = ((TextView)this.c.findViewById(g.alertTitle));
      this.A.setText(this.d);
      if (this.x != 0)
      {
        this.z.setImageResource(this.x);
        return true;
        i1 = 0;
      }
      else
      {
        if (this.y != null)
        {
          this.z.setImageDrawable(this.y);
          return true;
        }
        this.A.setPadding(this.z.getPaddingLeft(), this.z.getPaddingTop(), this.z.getPaddingRight(), this.z.getPaddingBottom());
        this.z.setVisibility(8);
        return true;
      }
    }
    this.c.findViewById(g.title_template).setVisibility(8);
    this.z.setVisibility(8);
    paramViewGroup.setVisibility(8);
    return false;
  }
  
  private int b()
  {
    if (this.G == 0) {
      return this.F;
    }
    if (this.L == 1) {
      return this.G;
    }
    return this.F;
  }
  
  private void b(ViewGroup paramViewGroup)
  {
    this.w = ((ScrollView)this.c.findViewById(g.scrollView));
    this.w.setFocusable(false);
    this.B = ((TextView)this.c.findViewById(16908299));
    if (this.B == null) {
      return;
    }
    if (this.e != null)
    {
      this.B.setText(this.e);
      return;
    }
    this.B.setVisibility(8);
    this.w.removeView(this.B);
    if (this.f != null)
    {
      ViewGroup localViewGroup = (ViewGroup)this.w.getParent();
      int i1 = localViewGroup.indexOfChild(this.w);
      localViewGroup.removeViewAt(i1);
      localViewGroup.addView(this.f, i1, new ViewGroup.LayoutParams(-1, -1));
      return;
    }
    paramViewGroup.setVisibility(8);
  }
  
  private void c()
  {
    b((ViewGroup)this.c.findViewById(g.contentPanel));
    boolean bool = d();
    ViewGroup localViewGroup = (ViewGroup)this.c.findViewById(g.topPanel);
    bh localbh = bh.a(this.a, null, l.AlertDialog, b.alertDialogStyle, 0);
    a(localViewGroup);
    View localView1 = this.c.findViewById(g.buttonPanel);
    if (!bool)
    {
      localView1.setVisibility(8);
      View localView3 = this.c.findViewById(g.textSpacerNoButtons);
      if (localView3 != null) {
        localView3.setVisibility(0);
      }
    }
    FrameLayout localFrameLayout1 = (FrameLayout)this.c.findViewById(g.customPanel);
    View localView2;
    if (this.g != null)
    {
      localView2 = this.g;
      int i1 = 0;
      if (localView2 != null) {
        i1 = 1;
      }
      if ((i1 == 0) || (!a(localView2))) {
        this.c.setFlags(131072, 131072);
      }
      if (i1 == 0) {
        break label346;
      }
      FrameLayout localFrameLayout2 = (FrameLayout)this.c.findViewById(g.custom);
      localFrameLayout2.addView(localView2, new ViewGroup.LayoutParams(-1, -1));
      if (this.m) {
        localFrameLayout2.setPadding(this.i, this.j, this.k, this.l);
      }
      if (this.f != null) {
        ((LinearLayout.LayoutParams)localFrameLayout1.getLayoutParams()).weight = 0.0F;
      }
    }
    for (;;)
    {
      ListView localListView = this.f;
      if ((localListView != null) && (this.D != null))
      {
        localListView.setAdapter(this.D);
        int i2 = this.E;
        if (i2 > -1)
        {
          localListView.setItemChecked(i2, true);
          localListView.setSelection(i2);
        }
      }
      localbh.b();
      return;
      if (this.h != 0)
      {
        localView2 = LayoutInflater.from(this.a).inflate(this.h, localFrameLayout1, false);
        break;
      }
      localView2 = null;
      break;
      label346:
      localFrameLayout1.setVisibility(8);
    }
  }
  
  private boolean d()
  {
    this.n = ((Button)this.c.findViewById(16908313));
    this.n.setOnClickListener(this.N);
    int i1;
    if (TextUtils.isEmpty(this.o))
    {
      this.n.setVisibility(8);
      i1 = 0;
      this.q = ((Button)this.c.findViewById(16908314));
      this.q.setOnClickListener(this.N);
      if (!TextUtils.isEmpty(this.r)) {
        break label196;
      }
      this.q.setVisibility(8);
      label96:
      this.t = ((Button)this.c.findViewById(16908315));
      this.t.setOnClickListener(this.N);
      if (!TextUtils.isEmpty(this.u)) {
        break label222;
      }
      this.t.setVisibility(8);
      label143:
      if (a(this.a))
      {
        if (i1 != 1) {
          break label248;
        }
        a(this.n);
      }
    }
    for (;;)
    {
      if (i1 == 0) {
        break label280;
      }
      return true;
      this.n.setText(this.o);
      this.n.setVisibility(0);
      i1 = 1;
      break;
      label196:
      this.q.setText(this.r);
      this.q.setVisibility(0);
      i1 |= 0x2;
      break label96;
      label222:
      this.t.setText(this.u);
      this.t.setVisibility(0);
      i1 |= 0x4;
      break label143;
      label248:
      if (i1 == 2) {
        a(this.q);
      } else if (i1 == 4) {
        a(this.t);
      }
    }
    label280:
    return false;
  }
  
  public void a()
  {
    this.b.a(1);
    int i1 = b();
    this.b.setContentView(i1);
    c();
  }
  
  public void a(int paramInt)
  {
    this.g = null;
    this.h = paramInt;
    this.m = false;
  }
  
  public void a(int paramInt, CharSequence paramCharSequence, DialogInterface.OnClickListener paramOnClickListener, Message paramMessage)
  {
    if ((paramMessage == null) && (paramOnClickListener != null)) {
      paramMessage = this.M.obtainMessage(paramInt, paramOnClickListener);
    }
    switch (paramInt)
    {
    default: 
      throw new IllegalArgumentException("Button does not exist");
    case -1: 
      this.o = paramCharSequence;
      this.p = paramMessage;
      return;
    case -2: 
      this.r = paramCharSequence;
      this.s = paramMessage;
      return;
    }
    this.u = paramCharSequence;
    this.v = paramMessage;
  }
  
  public void a(Drawable paramDrawable)
  {
    this.y = paramDrawable;
    this.x = 0;
    if (this.z != null)
    {
      if (paramDrawable != null) {
        this.z.setImageDrawable(paramDrawable);
      }
    }
    else {
      return;
    }
    this.z.setVisibility(8);
  }
  
  public void a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.g = paramView;
    this.h = 0;
    this.m = true;
    this.i = paramInt1;
    this.j = paramInt2;
    this.k = paramInt3;
    this.l = paramInt4;
  }
  
  public void a(CharSequence paramCharSequence)
  {
    this.d = paramCharSequence;
    if (this.A != null) {
      this.A.setText(paramCharSequence);
    }
  }
  
  public boolean a(int paramInt, KeyEvent paramKeyEvent)
  {
    return (this.w != null) && (this.w.executeKeyEvent(paramKeyEvent));
  }
  
  public void b(int paramInt)
  {
    this.y = null;
    this.x = paramInt;
    if (this.z != null)
    {
      if (paramInt != 0) {
        this.z.setImageResource(this.x);
      }
    }
    else {
      return;
    }
    this.z.setVisibility(8);
  }
  
  public void b(View paramView)
  {
    this.C = paramView;
  }
  
  public void b(CharSequence paramCharSequence)
  {
    this.e = paramCharSequence;
    if (this.B != null) {
      this.B.setText(paramCharSequence);
    }
  }
  
  public boolean b(int paramInt, KeyEvent paramKeyEvent)
  {
    return (this.w != null) && (this.w.executeKeyEvent(paramKeyEvent));
  }
  
  public int c(int paramInt)
  {
    TypedValue localTypedValue = new TypedValue();
    this.a.getTheme().resolveAttribute(paramInt, localTypedValue, true);
    return localTypedValue.resourceId;
  }
  
  public void c(View paramView)
  {
    this.g = paramView;
    this.h = 0;
    this.m = false;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/app/r.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */