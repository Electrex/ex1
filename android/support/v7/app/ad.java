package android.support.v7.app;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ar;
import android.support.v4.app.az;
import android.support.v4.app.ba;
import android.support.v4.app.o;
import android.support.v7.widget.Toolbar;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

public class ad
  extends o
  implements ba, ae, i
{
  private af n;
  
  public void a(az paramaz)
  {
    paramaz.a(this);
  }
  
  public void a(android.support.v7.d.a parama) {}
  
  public void a(Toolbar paramToolbar)
  {
    j().a(paramToolbar);
  }
  
  public boolean a(Intent paramIntent)
  {
    return ar.a(this, paramIntent);
  }
  
  public void addContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
  {
    j().b(paramView, paramLayoutParams);
  }
  
  public h b()
  {
    return j().g();
  }
  
  public void b(Intent paramIntent)
  {
    ar.b(this, paramIntent);
  }
  
  public void b(az paramaz) {}
  
  public void b(android.support.v7.d.a parama) {}
  
  public Intent c_()
  {
    return ar.b(this);
  }
  
  public void d()
  {
    j().e();
  }
  
  public a g()
  {
    return j().a();
  }
  
  public MenuInflater getMenuInflater()
  {
    return j().b();
  }
  
  public boolean h()
  {
    Intent localIntent = c_();
    if (localIntent != null)
    {
      if (a(localIntent))
      {
        az localaz = az.a(this);
        a(localaz);
        b(localaz);
        localaz.a();
      }
      for (;;)
      {
        try
        {
          android.support.v4.app.a.a(this);
          return true;
        }
        catch (IllegalStateException localIllegalStateException)
        {
          finish();
          continue;
        }
        b(localIntent);
      }
    }
    return false;
  }
  
  @Deprecated
  public void i() {}
  
  public void invalidateOptionsMenu()
  {
    j().e();
  }
  
  public af j()
  {
    if (this.n == null) {
      this.n = af.a(this, this);
    }
    return this.n;
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    j().a(paramConfiguration);
  }
  
  public void onContentChanged()
  {
    i();
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    j().h();
    super.onCreate(paramBundle);
    j().a(paramBundle);
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    j().f();
  }
  
  public final boolean onMenuItemSelected(int paramInt, MenuItem paramMenuItem)
  {
    if (super.onMenuItemSelected(paramInt, paramMenuItem)) {
      return true;
    }
    a locala = g();
    if ((paramMenuItem.getItemId() == 16908332) && (locala != null) && ((0x4 & locala.a()) != 0)) {
      return h();
    }
    return false;
  }
  
  protected void onPostCreate(Bundle paramBundle)
  {
    super.onPostCreate(paramBundle);
    j().b(paramBundle);
  }
  
  protected void onPostResume()
  {
    super.onPostResume();
    j().d();
  }
  
  protected void onStop()
  {
    super.onStop();
    j().c();
  }
  
  protected void onTitleChanged(CharSequence paramCharSequence, int paramInt)
  {
    super.onTitleChanged(paramCharSequence, paramInt);
    j().a(paramCharSequence);
  }
  
  public void setContentView(int paramInt)
  {
    j().a(paramInt);
  }
  
  public void setContentView(View paramView)
  {
    j().a(paramView);
  }
  
  public void setContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
  {
    j().a(paramView, paramLayoutParams);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/app/ad.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */