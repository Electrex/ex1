package android.support.v7.app;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.a.l;
import android.support.v7.d.b;
import android.support.v7.internal.view.f;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.Window;
import android.view.Window.Callback;

abstract class ag
  extends af
{
  final Context a;
  final Window b;
  final Window.Callback c;
  final ae d;
  boolean e;
  boolean f;
  boolean g;
  boolean h;
  boolean i;
  private a j;
  private MenuInflater k;
  private CharSequence l;
  private boolean m;
  
  ag(Context paramContext, Window paramWindow, ae paramae)
  {
    this.a = paramContext;
    this.b = paramWindow;
    this.d = paramae;
    this.c = this.b.getCallback();
    if ((this.c instanceof aj)) {
      throw new IllegalStateException("AppCompat has already installed itself into the Window");
    }
    this.b.setCallback(new aj(this, this.c));
  }
  
  public a a()
  {
    if ((this.e) && (this.j == null)) {
      this.j = i();
    }
    return this.j;
  }
  
  abstract android.support.v7.d.a a(b paramb);
  
  public void a(Bundle paramBundle)
  {
    TypedArray localTypedArray = this.a.obtainStyledAttributes(l.Theme);
    if (!localTypedArray.hasValue(l.Theme_windowActionBar))
    {
      localTypedArray.recycle();
      throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
    }
    if (localTypedArray.getBoolean(l.Theme_windowActionBar, false)) {
      this.e = true;
    }
    if (localTypedArray.getBoolean(l.Theme_windowActionBarOverlay, false)) {
      this.f = true;
    }
    if (localTypedArray.getBoolean(l.Theme_windowActionModeOverlay, false)) {
      this.g = true;
    }
    this.h = localTypedArray.getBoolean(l.Theme_android_windowIsFloating, false);
    this.i = localTypedArray.getBoolean(l.Theme_windowNoTitle, false);
    localTypedArray.recycle();
  }
  
  final void a(a parama)
  {
    this.j = parama;
  }
  
  public final void a(CharSequence paramCharSequence)
  {
    this.l = paramCharSequence;
    b(paramCharSequence);
  }
  
  abstract boolean a(int paramInt, KeyEvent paramKeyEvent);
  
  abstract boolean a(int paramInt, Menu paramMenu);
  
  abstract boolean a(KeyEvent paramKeyEvent);
  
  public MenuInflater b()
  {
    if (this.k == null) {
      this.k = new f(k());
    }
    return this.k;
  }
  
  abstract void b(CharSequence paramCharSequence);
  
  abstract boolean b(int paramInt, Menu paramMenu);
  
  public final void f()
  {
    this.m = true;
  }
  
  public final h g()
  {
    return new ai(this, null);
  }
  
  abstract a i();
  
  final a j()
  {
    return this.j;
  }
  
  final Context k()
  {
    a locala = a();
    Context localContext = null;
    if (locala != null) {
      localContext = locala.b();
    }
    if (localContext == null) {
      localContext = this.a;
    }
    return localContext;
  }
  
  final boolean l()
  {
    return this.m;
  }
  
  final Window.Callback m()
  {
    return this.b.getCallback();
  }
  
  final CharSequence n()
  {
    if ((this.c instanceof Activity)) {
      return ((Activity)this.c).getTitle();
    }
    return this.l;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/app/ag.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */