package android.support.v7.app;

import android.content.Context;
import android.graphics.drawable.Drawable;

public abstract interface h
{
  public abstract Drawable a();
  
  public abstract void a(int paramInt);
  
  public abstract void a(Drawable paramDrawable, int paramInt);
  
  public abstract Context b();
  
  public abstract boolean c();
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/app/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */