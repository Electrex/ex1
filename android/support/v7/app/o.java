package android.support.v7.app;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.Toolbar;

class o
  implements h
{
  final Toolbar a;
  final Drawable b;
  final CharSequence c;
  
  o(Toolbar paramToolbar)
  {
    this.a = paramToolbar;
    this.b = paramToolbar.getNavigationIcon();
    this.c = paramToolbar.getNavigationContentDescription();
  }
  
  public Drawable a()
  {
    return this.b;
  }
  
  public void a(int paramInt)
  {
    if (paramInt == 0)
    {
      this.a.setNavigationContentDescription(this.c);
      return;
    }
    this.a.setNavigationContentDescription(paramInt);
  }
  
  public void a(Drawable paramDrawable, int paramInt)
  {
    this.a.setNavigationIcon(paramDrawable);
    a(paramInt);
  }
  
  public Context b()
  {
    return this.a.getContext();
  }
  
  public boolean c()
  {
    return true;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/app/o.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */