package android.support.v7.app;

import android.support.v4.view.bv;
import android.support.v7.d.a;
import android.support.v7.d.b;
import android.support.v7.internal.widget.ActionBarContextView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.PopupWindow;

class aq
  implements b
{
  private b b;
  
  public aq(AppCompatDelegateImplV7 paramAppCompatDelegateImplV7, b paramb)
  {
    this.b = paramb;
  }
  
  public void a(a parama)
  {
    this.b.a(parama);
    if (this.a.l != null)
    {
      this.a.b.getDecorView().removeCallbacks(this.a.m);
      this.a.l.dismiss();
    }
    for (;;)
    {
      if (this.a.k != null) {
        this.a.k.removeAllViews();
      }
      if (this.a.d != null) {
        this.a.d.b(this.a.j);
      }
      this.a.j = null;
      return;
      if (this.a.k != null)
      {
        this.a.k.setVisibility(8);
        if (this.a.k.getParent() != null) {
          bv.s((View)this.a.k.getParent());
        }
      }
    }
  }
  
  public boolean a(a parama, Menu paramMenu)
  {
    return this.b.a(parama, paramMenu);
  }
  
  public boolean a(a parama, MenuItem paramMenuItem)
  {
    return this.b.a(parama, paramMenuItem);
  }
  
  public boolean b(a parama, Menu paramMenu)
  {
    return this.b.b(parama, paramMenu);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/app/aq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */