package android.support.v7.app;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.a.b;
import android.support.v7.internal.widget.bh;

class ai
  implements h
{
  private ai(ag paramag) {}
  
  public Drawable a()
  {
    Context localContext = b();
    int[] arrayOfInt = new int[1];
    arrayOfInt[0] = b.homeAsUpIndicator;
    bh localbh = bh.a(localContext, null, arrayOfInt);
    Drawable localDrawable = localbh.a(0);
    localbh.b();
    return localDrawable;
  }
  
  public void a(int paramInt)
  {
    a locala = this.a.a();
    if (locala != null) {
      locala.a(paramInt);
    }
  }
  
  public void a(Drawable paramDrawable, int paramInt)
  {
    a locala = this.a.a();
    if (locala != null)
    {
      locala.a(paramDrawable);
      locala.a(paramInt);
    }
  }
  
  public Context b()
  {
    return this.a.k();
  }
  
  public boolean c()
  {
    a locala = this.a.a();
    return (locala != null) && ((0x4 & locala.a()) != 0);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/app/ai.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */