package android.support.v7.app;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.v7.d.a;
import android.support.v7.internal.view.d;
import android.support.v7.internal.view.e;
import android.support.v7.internal.widget.NativeActionModeAwareLayout;
import android.support.v7.internal.widget.al;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.LayoutInflater.Factory2;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

@TargetApi(11)
class ak
  extends AppCompatDelegateImplV7
  implements al
{
  private NativeActionModeAwareLayout n;
  
  ak(Context paramContext, Window paramWindow, ae paramae)
  {
    super(paramContext, paramWindow, paramae);
  }
  
  public ActionMode a(View paramView, ActionMode.Callback paramCallback)
  {
    a locala = b(new e(paramView.getContext(), paramCallback));
    if (locala != null) {
      return new d(this.a, locala);
    }
    return null;
  }
  
  void a(ViewGroup paramViewGroup)
  {
    this.n = ((NativeActionModeAwareLayout)paramViewGroup.findViewById(16908290));
    if (this.n != null) {
      this.n.setActionModeForChildListener(this);
    }
  }
  
  View b(View paramView, String paramString, Context paramContext, AttributeSet paramAttributeSet)
  {
    View localView = super.b(paramView, paramString, paramContext, paramAttributeSet);
    if (localView != null) {
      return localView;
    }
    if ((this.c instanceof LayoutInflater.Factory2)) {
      return ((LayoutInflater.Factory2)this.c).onCreateView(paramView, paramString, paramContext, paramAttributeSet);
    }
    return null;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/app/ak.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */