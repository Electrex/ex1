package android.support.v7.app;

import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.y;
import android.view.Window.Callback;

final class ap
  implements y
{
  private ap(AppCompatDelegateImplV7 paramAppCompatDelegateImplV7) {}
  
  public void a(i parami, boolean paramBoolean)
  {
    AppCompatDelegateImplV7.a(this.a, parami);
  }
  
  public boolean a(i parami)
  {
    Window.Callback localCallback = this.a.m();
    if (localCallback != null) {
      localCallback.onMenuOpened(8, parami);
    }
    return true;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/app/ap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */