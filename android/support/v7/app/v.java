package android.support.v7.app;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.CursorAdapter;
import android.widget.ListView;

class v
  extends CursorAdapter
{
  private final int d;
  private final int e;
  
  v(t paramt, Context paramContext, Cursor paramCursor, boolean paramBoolean, ListView paramListView, r paramr)
  {
    super(paramContext, paramCursor, paramBoolean);
    Cursor localCursor = getCursor();
    this.d = localCursor.getColumnIndexOrThrow(this.c.I);
    this.e = localCursor.getColumnIndexOrThrow(this.c.J);
  }
  
  public void bindView(View paramView, Context paramContext, Cursor paramCursor)
  {
    ((CheckedTextView)paramView.findViewById(16908308)).setText(paramCursor.getString(this.d));
    ListView localListView = this.a;
    int i = paramCursor.getPosition();
    if (paramCursor.getInt(this.e) == 1) {}
    for (boolean bool = true;; bool = false)
    {
      localListView.setItemChecked(i, bool);
      return;
    }
  }
  
  public View newView(Context paramContext, Cursor paramCursor, ViewGroup paramViewGroup)
  {
    return this.c.b.inflate(r.j(this.b), paramViewGroup, false);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/app/v.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */