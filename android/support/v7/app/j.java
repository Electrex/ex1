package android.support.v7.app;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.bv;
import android.view.Window;

class j
  extends av
  implements k
{
  private final Activity a;
  
  public j(Activity paramActivity, Context paramContext)
  {
    super(paramContext);
    this.a = paramActivity;
  }
  
  public void a(float paramFloat)
  {
    if (paramFloat == 1.0F) {
      a(true);
    }
    for (;;)
    {
      super.b(paramFloat);
      return;
      if (paramFloat == 0.0F) {
        a(false);
      }
    }
  }
  
  boolean a()
  {
    return bv.h(this.a.getWindow().getDecorView()) == 1;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/app/j.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */