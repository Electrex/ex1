package android.support.v7.app;

import android.content.Context;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.a.b;
import android.support.v7.a.k;
import android.support.v7.a.l;

abstract class av
  extends Drawable
{
  private static final float b = (float)Math.toRadians(45.0D);
  private final Paint a = new Paint();
  private final float c;
  private final float d;
  private final float e;
  private final float f;
  private final float g;
  private final boolean h;
  private final Path i = new Path();
  private final int j;
  private boolean k = false;
  private float l;
  private float m;
  private float n;
  
  av(Context paramContext)
  {
    TypedArray localTypedArray = paramContext.getTheme().obtainStyledAttributes(null, l.DrawerArrowToggle, b.drawerArrowStyle, k.Base_Widget_AppCompat_DrawerArrowToggle);
    this.a.setAntiAlias(true);
    this.a.setColor(localTypedArray.getColor(l.DrawerArrowToggle_color, 0));
    this.j = localTypedArray.getDimensionPixelSize(l.DrawerArrowToggle_drawableSize, 0);
    this.e = Math.round(localTypedArray.getDimension(l.DrawerArrowToggle_barSize, 0.0F));
    this.d = Math.round(localTypedArray.getDimension(l.DrawerArrowToggle_topBottomBarArrowSize, 0.0F));
    this.c = localTypedArray.getDimension(l.DrawerArrowToggle_thickness, 0.0F);
    this.g = Math.round(localTypedArray.getDimension(l.DrawerArrowToggle_gapBetweenBars, 0.0F));
    this.h = localTypedArray.getBoolean(l.DrawerArrowToggle_spinBars, true);
    this.f = localTypedArray.getDimension(l.DrawerArrowToggle_middleBarArrowSize, 0.0F);
    this.n = (2 * ((int)(this.j - 3.0F * this.c - 2.0F * this.g) / 4));
    this.n = ((float)(this.n + (1.5D * this.c + this.g)));
    localTypedArray.recycle();
    this.a.setStyle(Paint.Style.STROKE);
    this.a.setStrokeJoin(Paint.Join.MITER);
    this.a.setStrokeCap(Paint.Cap.BUTT);
    this.a.setStrokeWidth(this.c);
    this.m = ((float)(this.c / 2.0F * Math.cos(b)));
  }
  
  private static float a(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    return paramFloat1 + paramFloat3 * (paramFloat2 - paramFloat1);
  }
  
  protected void a(boolean paramBoolean)
  {
    this.k = paramBoolean;
  }
  
  abstract boolean a();
  
  public void b(float paramFloat)
  {
    this.l = paramFloat;
    invalidateSelf();
  }
  
  public void draw(Canvas paramCanvas)
  {
    Rect localRect = getBounds();
    boolean bool = a();
    float f1 = a(this.e, this.d, this.l);
    float f2 = a(this.e, this.f, this.l);
    float f3 = Math.round(a(0.0F, this.m, this.l));
    float f4 = a(0.0F, b, this.l);
    float f5;
    float f6;
    label90:
    int i1;
    if (bool)
    {
      f5 = 0.0F;
      if (!bool) {
        break label324;
      }
      f6 = 180.0F;
      float f7 = a(f5, f6, this.l);
      float f8 = (float)Math.round(f1 * Math.cos(f4));
      float f9 = (float)Math.round(f1 * Math.sin(f4));
      this.i.rewind();
      float f10 = a(this.g + this.c, -this.m, this.l);
      float f11 = -f2 / 2.0F;
      this.i.moveTo(f11 + f3, 0.0F);
      this.i.rLineTo(f2 - f3 * 2.0F, 0.0F);
      this.i.moveTo(f11, f10);
      this.i.rLineTo(f8, f9);
      this.i.moveTo(f11, -f10);
      this.i.rLineTo(f8, -f9);
      this.i.close();
      paramCanvas.save();
      paramCanvas.translate(localRect.centerX(), this.n);
      if (!this.h) {
        break label336;
      }
      if (!(bool ^ this.k)) {
        break label330;
      }
      i1 = -1;
      label290:
      paramCanvas.rotate(f7 * i1);
    }
    for (;;)
    {
      paramCanvas.drawPath(this.i, this.a);
      paramCanvas.restore();
      return;
      f5 = -180.0F;
      break;
      label324:
      f6 = 0.0F;
      break label90;
      label330:
      i1 = 1;
      break label290;
      label336:
      if (bool) {
        paramCanvas.rotate(180.0F);
      }
    }
  }
  
  public int getIntrinsicHeight()
  {
    return this.j;
  }
  
  public int getIntrinsicWidth()
  {
    return this.j;
  }
  
  public int getOpacity()
  {
    return -3;
  }
  
  public boolean isAutoMirrored()
  {
    return true;
  }
  
  public void setAlpha(int paramInt)
  {
    this.a.setAlpha(paramInt);
  }
  
  public void setColorFilter(ColorFilter paramColorFilter)
  {
    this.a.setColorFilter(paramColorFilter);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/app/av.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */