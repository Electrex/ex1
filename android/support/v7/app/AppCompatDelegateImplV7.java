package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.media.AudioManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.view.ac;
import android.support.v4.view.bv;
import android.support.v4.view.cq;
import android.support.v7.a.d;
import android.support.v7.a.g;
import android.support.v7.a.l;
import android.support.v7.internal.view.c;
import android.support.v7.internal.view.menu.j;
import android.support.v7.internal.widget.ActionBarContextView;
import android.support.v7.internal.widget.ContentFrameLayout;
import android.support.v7.internal.widget.ViewStubCompat;
import android.support.v7.internal.widget.af;
import android.support.v7.internal.widget.ah;
import android.support.v7.internal.widget.bn;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.LayoutInflater.Factory;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.Window;
import android.view.Window.Callback;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

class AppCompatDelegateImplV7
  extends ag
  implements android.support.v4.view.am, j
{
  private boolean A;
  private int B;
  private final Runnable C = new al(this);
  private boolean D;
  private Rect E;
  private Rect F;
  private android.support.v7.internal.a.a G;
  android.support.v7.d.a j;
  ActionBarContextView k;
  PopupWindow l;
  Runnable m;
  private af n;
  private ap o;
  private at p;
  private boolean q;
  private ViewGroup r;
  private ViewGroup s;
  private TextView t;
  private View u;
  private boolean v;
  private boolean w;
  private boolean x;
  private AppCompatDelegateImplV7.PanelFeatureState[] y;
  private AppCompatDelegateImplV7.PanelFeatureState z;
  
  AppCompatDelegateImplV7(Context paramContext, Window paramWindow, ae paramae)
  {
    super(paramContext, paramWindow, paramae);
  }
  
  private AppCompatDelegateImplV7.PanelFeatureState a(int paramInt, boolean paramBoolean)
  {
    Object localObject = this.y;
    if ((localObject == null) || (localObject.length <= paramInt))
    {
      AppCompatDelegateImplV7.PanelFeatureState[] arrayOfPanelFeatureState = new AppCompatDelegateImplV7.PanelFeatureState[paramInt + 1];
      if (localObject != null) {
        System.arraycopy(localObject, 0, arrayOfPanelFeatureState, 0, localObject.length);
      }
      this.y = arrayOfPanelFeatureState;
      localObject = arrayOfPanelFeatureState;
    }
    AppCompatDelegateImplV7.PanelFeatureState localPanelFeatureState1 = localObject[paramInt];
    if (localPanelFeatureState1 == null)
    {
      AppCompatDelegateImplV7.PanelFeatureState localPanelFeatureState2 = new AppCompatDelegateImplV7.PanelFeatureState(paramInt);
      localObject[paramInt] = localPanelFeatureState2;
      return localPanelFeatureState2;
    }
    return localPanelFeatureState1;
  }
  
  private AppCompatDelegateImplV7.PanelFeatureState a(Menu paramMenu)
  {
    AppCompatDelegateImplV7.PanelFeatureState[] arrayOfPanelFeatureState = this.y;
    int i;
    if (arrayOfPanelFeatureState != null) {
      i = arrayOfPanelFeatureState.length;
    }
    for (int i1 = 0;; i1++)
    {
      if (i1 >= i) {
        break label55;
      }
      AppCompatDelegateImplV7.PanelFeatureState localPanelFeatureState = arrayOfPanelFeatureState[i1];
      if ((localPanelFeatureState != null) && (localPanelFeatureState.j == paramMenu))
      {
        return localPanelFeatureState;
        i = 0;
        break;
      }
    }
    label55:
    return null;
  }
  
  private void a(int paramInt, AppCompatDelegateImplV7.PanelFeatureState paramPanelFeatureState, Menu paramMenu)
  {
    if (paramMenu == null)
    {
      if ((paramPanelFeatureState == null) && (paramInt >= 0) && (paramInt < this.y.length)) {
        paramPanelFeatureState = this.y[paramInt];
      }
      if (paramPanelFeatureState != null) {
        paramMenu = paramPanelFeatureState.j;
      }
    }
    if ((paramPanelFeatureState != null) && (!paramPanelFeatureState.o)) {}
    Window.Callback localCallback;
    do
    {
      return;
      localCallback = m();
    } while (localCallback == null);
    localCallback.onPanelClosed(paramInt, paramMenu);
  }
  
  private void a(AppCompatDelegateImplV7.PanelFeatureState paramPanelFeatureState, KeyEvent paramKeyEvent)
  {
    int i = -1;
    if ((paramPanelFeatureState.o) || (l())) {}
    label113:
    label119:
    label123:
    label125:
    WindowManager localWindowManager;
    do
    {
      do
      {
        for (;;)
        {
          return;
          int i2;
          if (paramPanelFeatureState.a == 0)
          {
            Context localContext = this.a;
            if ((0xF & localContext.getResources().getConfiguration().screenLayout) != 4) {
              break label113;
            }
            i2 = 1;
            if (localContext.getApplicationInfo().targetSdkVersion < 11) {
              break label119;
            }
          }
          for (int i3 = 1;; i3 = 0)
          {
            if ((i2 != 0) && (i3 != 0)) {
              break label123;
            }
            Window.Callback localCallback = m();
            if ((localCallback == null) || (localCallback.onMenuOpened(paramPanelFeatureState.a, paramPanelFeatureState.j))) {
              break label125;
            }
            a(paramPanelFeatureState, true);
            return;
            i2 = 0;
            break;
          }
        }
        localWindowManager = (WindowManager)this.a.getSystemService("window");
      } while ((localWindowManager == null) || (!b(paramPanelFeatureState, paramKeyEvent)));
      if ((paramPanelFeatureState.g != null) && (!paramPanelFeatureState.q)) {
        break label410;
      }
      if (paramPanelFeatureState.g != null) {
        break;
      }
    } while ((!a(paramPanelFeatureState)) || (paramPanelFeatureState.g == null));
    label189:
    ViewGroup.LayoutParams localLayoutParams1;
    if ((c(paramPanelFeatureState)) && (paramPanelFeatureState.a()))
    {
      localLayoutParams1 = paramPanelFeatureState.h.getLayoutParams();
      if (localLayoutParams1 != null) {
        break label446;
      }
    }
    label410:
    label440:
    label446:
    for (ViewGroup.LayoutParams localLayoutParams2 = new ViewGroup.LayoutParams(-2, -2);; localLayoutParams2 = localLayoutParams1)
    {
      int i1 = paramPanelFeatureState.b;
      paramPanelFeatureState.g.setBackgroundResource(i1);
      ViewParent localViewParent = paramPanelFeatureState.h.getParent();
      if ((localViewParent != null) && ((localViewParent instanceof ViewGroup))) {
        ((ViewGroup)localViewParent).removeView(paramPanelFeatureState.h);
      }
      paramPanelFeatureState.g.addView(paramPanelFeatureState.h, localLayoutParams2);
      if (!paramPanelFeatureState.h.hasFocus()) {
        paramPanelFeatureState.h.requestFocus();
      }
      for (i = -2;; i = -2)
      {
        ViewGroup.LayoutParams localLayoutParams3;
        do
        {
          paramPanelFeatureState.n = false;
          WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams(i, -2, paramPanelFeatureState.d, paramPanelFeatureState.e, 1002, 8519680, -3);
          localLayoutParams.gravity = paramPanelFeatureState.c;
          localLayoutParams.windowAnimations = paramPanelFeatureState.f;
          localWindowManager.addView(paramPanelFeatureState.g, localLayoutParams);
          paramPanelFeatureState.o = true;
          return;
          if ((!paramPanelFeatureState.q) || (paramPanelFeatureState.g.getChildCount() <= 0)) {
            break label189;
          }
          paramPanelFeatureState.g.removeAllViews();
          break label189;
          break;
          if (paramPanelFeatureState.i == null) {
            break label440;
          }
          localLayoutParams3 = paramPanelFeatureState.i.getLayoutParams();
        } while ((localLayoutParams3 != null) && (localLayoutParams3.width == i));
      }
    }
  }
  
  private void a(AppCompatDelegateImplV7.PanelFeatureState paramPanelFeatureState, boolean paramBoolean)
  {
    if ((paramBoolean) && (paramPanelFeatureState.a == 0) && (this.n != null) && (this.n.e())) {
      b(paramPanelFeatureState.j);
    }
    do
    {
      return;
      boolean bool = paramPanelFeatureState.o;
      WindowManager localWindowManager = (WindowManager)this.a.getSystemService("window");
      if ((localWindowManager != null) && (bool) && (paramPanelFeatureState.g != null)) {
        localWindowManager.removeView(paramPanelFeatureState.g);
      }
      paramPanelFeatureState.m = false;
      paramPanelFeatureState.n = false;
      paramPanelFeatureState.o = false;
      if ((bool) && (paramBoolean)) {
        a(paramPanelFeatureState.a, paramPanelFeatureState, null);
      }
      paramPanelFeatureState.h = null;
      paramPanelFeatureState.q = true;
    } while (this.z != paramPanelFeatureState);
    this.z = null;
  }
  
  private void a(android.support.v7.internal.view.menu.i parami, boolean paramBoolean)
  {
    if ((this.n != null) && (this.n.d()) && ((!cq.b(ViewConfiguration.get(this.a))) || (this.n.f())))
    {
      Window.Callback localCallback = m();
      if ((!this.n.e()) || (!paramBoolean)) {
        if ((localCallback != null) && (!l()))
        {
          if ((this.A) && ((0x1 & this.B) != 0))
          {
            this.r.removeCallbacks(this.C);
            this.C.run();
          }
          AppCompatDelegateImplV7.PanelFeatureState localPanelFeatureState2 = a(0, true);
          if ((localPanelFeatureState2.j != null) && (!localPanelFeatureState2.r) && (localCallback.onPreparePanel(0, localPanelFeatureState2.i, localPanelFeatureState2.j)))
          {
            localCallback.onMenuOpened(8, localPanelFeatureState2.j);
            this.n.g();
          }
        }
      }
      do
      {
        return;
        this.n.h();
      } while (l());
      localCallback.onPanelClosed(8, a(0, true).j);
      return;
    }
    AppCompatDelegateImplV7.PanelFeatureState localPanelFeatureState1 = a(0, true);
    localPanelFeatureState1.q = true;
    a(localPanelFeatureState1, false);
    a(localPanelFeatureState1, null);
  }
  
  private void a(ContentFrameLayout paramContentFrameLayout)
  {
    paramContentFrameLayout.a(this.r.getPaddingLeft(), this.r.getPaddingTop(), this.r.getPaddingRight(), this.r.getPaddingBottom());
    TypedArray localTypedArray = this.a.obtainStyledAttributes(l.Theme);
    localTypedArray.getValue(l.Theme_windowMinWidthMajor, paramContentFrameLayout.getMinWidthMajor());
    localTypedArray.getValue(l.Theme_windowMinWidthMinor, paramContentFrameLayout.getMinWidthMinor());
    if (localTypedArray.hasValue(l.Theme_windowFixedWidthMajor)) {
      localTypedArray.getValue(l.Theme_windowFixedWidthMajor, paramContentFrameLayout.getFixedWidthMajor());
    }
    if (localTypedArray.hasValue(l.Theme_windowFixedWidthMinor)) {
      localTypedArray.getValue(l.Theme_windowFixedWidthMinor, paramContentFrameLayout.getFixedWidthMinor());
    }
    if (localTypedArray.hasValue(l.Theme_windowFixedHeightMajor)) {
      localTypedArray.getValue(l.Theme_windowFixedHeightMajor, paramContentFrameLayout.getFixedHeightMajor());
    }
    if (localTypedArray.hasValue(l.Theme_windowFixedHeightMinor)) {
      localTypedArray.getValue(l.Theme_windowFixedHeightMinor, paramContentFrameLayout.getFixedHeightMinor());
    }
    localTypedArray.recycle();
    paramContentFrameLayout.requestLayout();
  }
  
  private boolean a(AppCompatDelegateImplV7.PanelFeatureState paramPanelFeatureState)
  {
    paramPanelFeatureState.a(k());
    paramPanelFeatureState.g = new ar(this, paramPanelFeatureState.l);
    paramPanelFeatureState.c = 81;
    return true;
  }
  
  private boolean a(AppCompatDelegateImplV7.PanelFeatureState paramPanelFeatureState, int paramInt1, KeyEvent paramKeyEvent, int paramInt2)
  {
    boolean bool1 = paramKeyEvent.isSystem();
    boolean bool2 = false;
    if (bool1) {}
    do
    {
      return bool2;
      if (!paramPanelFeatureState.m)
      {
        boolean bool3 = b(paramPanelFeatureState, paramKeyEvent);
        bool2 = false;
        if (!bool3) {}
      }
      else
      {
        android.support.v7.internal.view.menu.i locali = paramPanelFeatureState.j;
        bool2 = false;
        if (locali != null) {
          bool2 = paramPanelFeatureState.j.performShortcut(paramInt1, paramKeyEvent, paramInt2);
        }
      }
    } while ((!bool2) || ((paramInt2 & 0x1) != 0) || (this.n != null));
    a(paramPanelFeatureState, true);
    return bool2;
  }
  
  private void b(android.support.v7.internal.view.menu.i parami)
  {
    if (this.x) {
      return;
    }
    this.x = true;
    this.n.j();
    Window.Callback localCallback = m();
    if ((localCallback != null) && (!l())) {
      localCallback.onPanelClosed(8, parami);
    }
    this.x = false;
  }
  
  private boolean b(AppCompatDelegateImplV7.PanelFeatureState paramPanelFeatureState)
  {
    Context localContext = this.a;
    TypedValue localTypedValue;
    Resources.Theme localTheme1;
    Resources.Theme localTheme2;
    Object localObject;
    if (((paramPanelFeatureState.a == 0) || (paramPanelFeatureState.a == 8)) && (this.n != null))
    {
      localTypedValue = new TypedValue();
      localTheme1 = localContext.getTheme();
      localTheme1.resolveAttribute(android.support.v7.a.b.actionBarTheme, localTypedValue, true);
      if (localTypedValue.resourceId != 0)
      {
        localTheme2 = localContext.getResources().newTheme();
        localTheme2.setTo(localTheme1);
        localTheme2.applyStyle(localTypedValue.resourceId, true);
        localTheme2.resolveAttribute(android.support.v7.a.b.actionBarWidgetTheme, localTypedValue, true);
        if (localTypedValue.resourceId != 0)
        {
          if (localTheme2 == null)
          {
            localTheme2 = localContext.getResources().newTheme();
            localTheme2.setTo(localTheme1);
          }
          localTheme2.applyStyle(localTypedValue.resourceId, true);
        }
        Resources.Theme localTheme3 = localTheme2;
        if (localTheme3 == null) {
          break label207;
        }
        localObject = new android.support.v7.internal.view.b(localContext, 0);
        ((Context)localObject).getTheme().setTo(localTheme3);
      }
    }
    for (;;)
    {
      android.support.v7.internal.view.menu.i locali = new android.support.v7.internal.view.menu.i((Context)localObject);
      locali.a(this);
      paramPanelFeatureState.a(locali);
      return true;
      localTheme1.resolveAttribute(android.support.v7.a.b.actionBarWidgetTheme, localTypedValue, true);
      localTheme2 = null;
      break;
      label207:
      localObject = localContext;
    }
  }
  
  private boolean b(AppCompatDelegateImplV7.PanelFeatureState paramPanelFeatureState, KeyEvent paramKeyEvent)
  {
    if (l()) {
      return false;
    }
    if (paramPanelFeatureState.m) {
      return true;
    }
    if ((this.z != null) && (this.z != paramPanelFeatureState)) {
      a(this.z, false);
    }
    Window.Callback localCallback = m();
    if (localCallback != null) {
      paramPanelFeatureState.i = localCallback.onCreatePanelView(paramPanelFeatureState.a);
    }
    if ((paramPanelFeatureState.a == 0) || (paramPanelFeatureState.a == 8)) {}
    for (int i = 1;; i = 0)
    {
      if ((i != 0) && (this.n != null)) {
        this.n.i();
      }
      if (paramPanelFeatureState.i != null) {
        break label397;
      }
      if ((paramPanelFeatureState.j != null) && (!paramPanelFeatureState.r)) {
        break label265;
      }
      if ((paramPanelFeatureState.j == null) && ((!b(paramPanelFeatureState)) || (paramPanelFeatureState.j == null))) {
        break;
      }
      if ((i != 0) && (this.n != null))
      {
        if (this.o == null) {
          this.o = new ap(this, null);
        }
        this.n.a(paramPanelFeatureState.j, this.o);
      }
      paramPanelFeatureState.j.g();
      if (localCallback.onCreatePanelMenu(paramPanelFeatureState.a, paramPanelFeatureState.j)) {
        break label260;
      }
      paramPanelFeatureState.a(null);
      if ((i == 0) || (this.n == null)) {
        break;
      }
      this.n.a(null, this.o);
      return false;
    }
    label260:
    paramPanelFeatureState.r = false;
    label265:
    paramPanelFeatureState.j.g();
    if (paramPanelFeatureState.s != null)
    {
      paramPanelFeatureState.j.b(paramPanelFeatureState.s);
      paramPanelFeatureState.s = null;
    }
    if (!localCallback.onPreparePanel(0, paramPanelFeatureState.i, paramPanelFeatureState.j))
    {
      if ((i != 0) && (this.n != null)) {
        this.n.a(null, this.o);
      }
      paramPanelFeatureState.j.h();
      return false;
    }
    int i1;
    if (paramKeyEvent != null)
    {
      i1 = paramKeyEvent.getDeviceId();
      if (KeyCharacterMap.load(i1).getKeyboardType() == 1) {
        break label420;
      }
    }
    label397:
    label420:
    for (boolean bool = true;; bool = false)
    {
      paramPanelFeatureState.p = bool;
      paramPanelFeatureState.j.setQwertyMode(paramPanelFeatureState.p);
      paramPanelFeatureState.j.h();
      paramPanelFeatureState.m = true;
      paramPanelFeatureState.n = false;
      this.z = paramPanelFeatureState;
      return true;
      i1 = -1;
      break;
    }
  }
  
  private void c(int paramInt)
  {
    a(a(paramInt, true), true);
  }
  
  private boolean c(AppCompatDelegateImplV7.PanelFeatureState paramPanelFeatureState)
  {
    if (paramPanelFeatureState.i != null)
    {
      paramPanelFeatureState.h = paramPanelFeatureState.i;
      return true;
    }
    if (paramPanelFeatureState.j == null) {
      return false;
    }
    if (this.p == null) {
      this.p = new at(this, null);
    }
    paramPanelFeatureState.h = ((View)paramPanelFeatureState.a(this.p));
    if (paramPanelFeatureState.h != null) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  private void d(int paramInt)
  {
    this.B |= 1 << paramInt;
    if ((!this.A) && (this.r != null))
    {
      bv.a(this.r, this.C);
      this.A = true;
    }
  }
  
  private boolean d(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramKeyEvent.getRepeatCount() == 0)
    {
      AppCompatDelegateImplV7.PanelFeatureState localPanelFeatureState = a(paramInt, true);
      if (!localPanelFeatureState.o) {
        return b(localPanelFeatureState, paramKeyEvent);
      }
    }
    return false;
  }
  
  private void e(int paramInt)
  {
    AppCompatDelegateImplV7.PanelFeatureState localPanelFeatureState1 = a(paramInt, true);
    if (localPanelFeatureState1.j != null)
    {
      Bundle localBundle = new Bundle();
      localPanelFeatureState1.j.a(localBundle);
      if (localBundle.size() > 0) {
        localPanelFeatureState1.s = localBundle;
      }
      localPanelFeatureState1.j.g();
      localPanelFeatureState1.j.clear();
    }
    localPanelFeatureState1.r = true;
    localPanelFeatureState1.q = true;
    if (((paramInt == 8) || (paramInt == 0)) && (this.n != null))
    {
      AppCompatDelegateImplV7.PanelFeatureState localPanelFeatureState2 = a(0, false);
      if (localPanelFeatureState2 != null)
      {
        localPanelFeatureState2.m = false;
        b(localPanelFeatureState2, null);
      }
    }
  }
  
  private void e(int paramInt, KeyEvent paramKeyEvent)
  {
    boolean bool1 = true;
    if (this.j != null) {
      return;
    }
    AppCompatDelegateImplV7.PanelFeatureState localPanelFeatureState = a(paramInt, bool1);
    if ((paramInt == 0) && (this.n != null) && (this.n.d()) && (!cq.b(ViewConfiguration.get(this.a)))) {
      if (!this.n.e())
      {
        if ((l()) || (!b(localPanelFeatureState, paramKeyEvent))) {
          break label230;
        }
        bool1 = this.n.g();
      }
    }
    for (;;)
    {
      label93:
      if (bool1)
      {
        AudioManager localAudioManager = (AudioManager)this.a.getSystemService("audio");
        if (localAudioManager != null)
        {
          localAudioManager.playSoundEffect(0);
          return;
          bool1 = this.n.h();
          continue;
          if ((localPanelFeatureState.o) || (localPanelFeatureState.n))
          {
            boolean bool2 = localPanelFeatureState.o;
            a(localPanelFeatureState, bool1);
            bool1 = bool2;
            continue;
          }
          if (!localPanelFeatureState.m) {
            break label230;
          }
          if (!localPanelFeatureState.r) {
            break label235;
          }
          localPanelFeatureState.m = false;
        }
      }
    }
    label230:
    label235:
    for (boolean bool3 = b(localPanelFeatureState, paramKeyEvent);; bool3 = bool1)
    {
      if (bool3)
      {
        a(localPanelFeatureState, paramKeyEvent);
        break label93;
        Log.w("AppCompatDelegate", "Couldn't get audio manager");
        return;
      }
      bool1 = false;
      break label93;
      break;
    }
  }
  
  private int f(int paramInt)
  {
    int i = 1;
    ViewGroup.MarginLayoutParams localMarginLayoutParams;
    int i4;
    int i5;
    label198:
    label205:
    int i3;
    if ((this.k != null) && ((this.k.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)))
    {
      localMarginLayoutParams = (ViewGroup.MarginLayoutParams)this.k.getLayoutParams();
      if (this.k.isShown())
      {
        if (this.E == null)
        {
          this.E = new Rect();
          this.F = new Rect();
        }
        Rect localRect1 = this.E;
        Rect localRect2 = this.F;
        localRect1.set(0, paramInt, 0, 0);
        bn.a(this.s, localRect1, localRect2);
        if (localRect2.top == 0)
        {
          i4 = paramInt;
          if (localMarginLayoutParams.topMargin == i4) {
            break label358;
          }
          localMarginLayoutParams.topMargin = paramInt;
          if (this.u != null) {
            break label279;
          }
          this.u = new View(this.a);
          this.u.setBackgroundColor(this.a.getResources().getColor(d.abc_input_method_navigation_guard));
          this.s.addView(this.u, -1, new ViewGroup.LayoutParams(-1, paramInt));
          i5 = i;
          if (this.u == null) {
            break label318;
          }
          if ((!this.g) && (i != 0)) {
            paramInt = 0;
          }
          int i6 = i5;
          i3 = i;
          i = i6;
          label228:
          if (i != 0) {
            this.k.setLayoutParams(localMarginLayoutParams);
          }
        }
      }
    }
    for (int i1 = i3;; i1 = 0)
    {
      View localView;
      int i2;
      if (this.u != null)
      {
        localView = this.u;
        i2 = 0;
        if (i1 == 0) {
          break label343;
        }
      }
      for (;;)
      {
        localView.setVisibility(i2);
        return paramInt;
        i4 = 0;
        break;
        label279:
        ViewGroup.LayoutParams localLayoutParams = this.u.getLayoutParams();
        if (localLayoutParams.height != paramInt)
        {
          localLayoutParams.height = paramInt;
          this.u.setLayoutParams(localLayoutParams);
        }
        i5 = i;
        break label198;
        label318:
        i = 0;
        break label205;
        if (localMarginLayoutParams.topMargin == 0) {
          break label350;
        }
        localMarginLayoutParams.topMargin = 0;
        i3 = 0;
        break label228;
        label343:
        i2 = 8;
      }
      label350:
      i3 = 0;
      i = 0;
      break label228;
      label358:
      i5 = 0;
      break label198;
    }
  }
  
  private void p()
  {
    if (!this.q)
    {
      LayoutInflater localLayoutInflater = LayoutInflater.from(this.a);
      if (!this.i) {
        if (this.h) {
          this.s = ((ViewGroup)localLayoutInflater.inflate(android.support.v7.a.i.abc_dialog_title_material, null));
        }
      }
      while (this.s == null)
      {
        throw new IllegalArgumentException("AppCompat does not support the current theme features");
        if (this.e)
        {
          TypedValue localTypedValue = new TypedValue();
          this.a.getTheme().resolveAttribute(android.support.v7.a.b.actionBarTheme, localTypedValue, true);
          if (localTypedValue.resourceId != 0) {}
          for (Object localObject = new android.support.v7.internal.view.b(this.a, localTypedValue.resourceId);; localObject = this.a)
          {
            this.s = ((ViewGroup)LayoutInflater.from((Context)localObject).inflate(android.support.v7.a.i.abc_screen_toolbar, null));
            this.n = ((af)this.s.findViewById(g.decor_content_parent));
            this.n.setWindowCallback(m());
            if (this.f) {
              this.n.a(9);
            }
            if (this.v) {
              this.n.a(2);
            }
            if (!this.w) {
              break;
            }
            this.n.a(5);
            break;
          }
          if (this.g) {}
          for (this.s = ((ViewGroup)localLayoutInflater.inflate(android.support.v7.a.i.abc_screen_simple_overlay_action_mode, null));; this.s = ((ViewGroup)localLayoutInflater.inflate(android.support.v7.a.i.abc_screen_simple, null)))
          {
            if (Build.VERSION.SDK_INT < 21) {
              break label300;
            }
            bv.a(this.s, new am(this));
            break;
          }
          label300:
          ((ah)this.s).setOnFitSystemWindowsListener(new an(this));
        }
      }
      if (this.n == null) {
        this.t = ((TextView)this.s.findViewById(g.title));
      }
      bn.b(this.s);
      ViewGroup localViewGroup = (ViewGroup)this.b.findViewById(16908290);
      ContentFrameLayout localContentFrameLayout = (ContentFrameLayout)this.s.findViewById(g.action_bar_activity_content);
      while (localViewGroup.getChildCount() > 0)
      {
        View localView = localViewGroup.getChildAt(0);
        localViewGroup.removeViewAt(0);
        localContentFrameLayout.addView(localView);
      }
      this.b.setContentView(this.s);
      localViewGroup.setId(-1);
      localContentFrameLayout.setId(16908290);
      if ((localViewGroup instanceof FrameLayout)) {
        ((FrameLayout)localViewGroup).setForeground(null);
      }
      CharSequence localCharSequence = n();
      if (!TextUtils.isEmpty(localCharSequence)) {
        b(localCharSequence);
      }
      a(localContentFrameLayout);
      a(this.s);
      this.q = true;
      AppCompatDelegateImplV7.PanelFeatureState localPanelFeatureState = a(0, false);
      if ((!l()) && ((localPanelFeatureState == null) || (localPanelFeatureState.j == null))) {
        d(8);
      }
    }
  }
  
  private void q()
  {
    if (this.q) {
      throw new AndroidRuntimeException("Window feature must be requested before adding content");
    }
  }
  
  android.support.v7.d.a a(android.support.v7.d.b paramb)
  {
    if (this.j != null) {
      this.j.c();
    }
    aq localaq = new aq(this, paramb);
    Object localObject;
    label242:
    boolean bool;
    if (this.k == null)
    {
      if (!this.h) {
        break label434;
      }
      TypedValue localTypedValue = new TypedValue();
      Resources.Theme localTheme1 = this.a.getTheme();
      localTheme1.resolveAttribute(android.support.v7.a.b.actionBarTheme, localTypedValue, true);
      if (localTypedValue.resourceId != 0)
      {
        Resources.Theme localTheme2 = this.a.getResources().newTheme();
        localTheme2.setTo(localTheme1);
        localTheme2.applyStyle(localTypedValue.resourceId, true);
        localObject = new android.support.v7.internal.view.b(this.a, 0);
        ((Context)localObject).getTheme().setTo(localTheme2);
        this.k = new ActionBarContextView((Context)localObject);
        this.l = new PopupWindow((Context)localObject, null, android.support.v7.a.b.actionModePopupWindowStyle);
        this.l.setContentView(this.k);
        this.l.setWidth(-1);
        ((Context)localObject).getTheme().resolveAttribute(android.support.v7.a.b.actionBarSize, localTypedValue, true);
        int i = TypedValue.complexToDimensionPixelSize(localTypedValue.data, ((Context)localObject).getResources().getDisplayMetrics());
        this.k.setContentHeight(i);
        this.l.setHeight(-2);
        this.m = new ao(this);
      }
    }
    else if (this.k != null)
    {
      this.k.c();
      Context localContext = this.k.getContext();
      ActionBarContextView localActionBarContextView = this.k;
      if (this.l != null) {
        break label481;
      }
      bool = true;
      label280:
      c localc = new c(localContext, localActionBarContextView, localaq, bool);
      if (!paramb.a(localc, localc.b())) {
        break label487;
      }
      localc.d();
      this.k.a(localc);
      this.k.setVisibility(0);
      this.j = localc;
      if (this.l != null) {
        this.b.getDecorView().post(this.m);
      }
      this.k.sendAccessibilityEvent(32);
      if (this.k.getParent() != null) {
        bv.s((View)this.k.getParent());
      }
    }
    for (;;)
    {
      if ((this.j != null) && (this.d != null)) {
        this.d.a(this.j);
      }
      return this.j;
      localObject = this.a;
      break;
      label434:
      ViewStubCompat localViewStubCompat = (ViewStubCompat)this.s.findViewById(g.action_mode_bar_stub);
      if (localViewStubCompat == null) {
        break label242;
      }
      localViewStubCompat.setLayoutInflater(LayoutInflater.from(k()));
      this.k = ((ActionBarContextView)localViewStubCompat.a());
      break label242;
      label481:
      bool = false;
      break label280;
      label487:
      this.j = null;
    }
  }
  
  public final View a(View paramView, String paramString, Context paramContext, AttributeSet paramAttributeSet)
  {
    View localView = b(paramView, paramString, paramContext, paramAttributeSet);
    if (localView != null) {
      return localView;
    }
    return c(paramView, paramString, paramContext, paramAttributeSet);
  }
  
  public void a(int paramInt)
  {
    p();
    ViewGroup localViewGroup = (ViewGroup)this.s.findViewById(16908290);
    localViewGroup.removeAllViews();
    LayoutInflater.from(this.a).inflate(paramInt, localViewGroup);
    this.c.onContentChanged();
  }
  
  public void a(Configuration paramConfiguration)
  {
    if ((this.e) && (this.q))
    {
      a locala = a();
      if (locala != null) {
        locala.a(paramConfiguration);
      }
    }
  }
  
  public void a(Bundle paramBundle)
  {
    super.a(paramBundle);
    this.r = ((ViewGroup)this.b.getDecorView());
    a locala;
    if (((this.c instanceof Activity)) && (android.support.v4.app.ar.c((Activity)this.c) != null))
    {
      locala = j();
      if (locala == null) {
        this.D = true;
      }
    }
    else
    {
      return;
    }
    locala.d(true);
  }
  
  public void a(android.support.v7.internal.view.menu.i parami)
  {
    a(parami, true);
  }
  
  public void a(Toolbar paramToolbar)
  {
    if (!(this.c instanceof Activity)) {
      return;
    }
    if ((a() instanceof android.support.v7.internal.a.i)) {
      throw new IllegalStateException("This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead.");
    }
    android.support.v7.internal.a.b localb = new android.support.v7.internal.a.b(paramToolbar, ((Activity)this.a).getTitle(), this.b);
    a(localb);
    this.b.setCallback(localb.e());
    localb.c();
  }
  
  public void a(View paramView)
  {
    p();
    ViewGroup localViewGroup = (ViewGroup)this.s.findViewById(16908290);
    localViewGroup.removeAllViews();
    localViewGroup.addView(paramView);
    this.c.onContentChanged();
  }
  
  public void a(View paramView, ViewGroup.LayoutParams paramLayoutParams)
  {
    p();
    ViewGroup localViewGroup = (ViewGroup)this.s.findViewById(16908290);
    localViewGroup.removeAllViews();
    localViewGroup.addView(paramView, paramLayoutParams);
    this.c.onContentChanged();
  }
  
  void a(ViewGroup paramViewGroup) {}
  
  boolean a(int paramInt, KeyEvent paramKeyEvent)
  {
    a locala = a();
    if ((locala != null) && (locala.a(paramInt, paramKeyEvent))) {}
    boolean bool;
    do
    {
      do
      {
        return true;
        if ((this.z == null) || (!a(this.z, paramKeyEvent.getKeyCode(), paramKeyEvent, 1))) {
          break;
        }
      } while (this.z == null);
      this.z.n = true;
      return true;
      if (this.z != null) {
        break;
      }
      AppCompatDelegateImplV7.PanelFeatureState localPanelFeatureState = a(0, true);
      b(localPanelFeatureState, paramKeyEvent);
      bool = a(localPanelFeatureState, paramKeyEvent.getKeyCode(), paramKeyEvent, 1);
      localPanelFeatureState.m = false;
    } while (bool);
    return false;
  }
  
  boolean a(int paramInt, Menu paramMenu)
  {
    if (paramInt == 8)
    {
      a locala = a();
      if (locala != null) {
        locala.f(false);
      }
      return true;
    }
    if (paramInt == 0)
    {
      AppCompatDelegateImplV7.PanelFeatureState localPanelFeatureState = a(paramInt, true);
      if (localPanelFeatureState.o) {
        a(localPanelFeatureState, false);
      }
    }
    return false;
  }
  
  public boolean a(android.support.v7.internal.view.menu.i parami, MenuItem paramMenuItem)
  {
    Window.Callback localCallback = m();
    if ((localCallback != null) && (!l()))
    {
      AppCompatDelegateImplV7.PanelFeatureState localPanelFeatureState = a(parami.p());
      if (localPanelFeatureState != null) {
        return localCallback.onMenuItemSelected(localPanelFeatureState.a, paramMenuItem);
      }
    }
    return false;
  }
  
  boolean a(KeyEvent paramKeyEvent)
  {
    int i = paramKeyEvent.getKeyCode();
    if (paramKeyEvent.getAction() == 0) {}
    for (int i1 = 1; i1 != 0; i1 = 0) {
      return c(i, paramKeyEvent);
    }
    return b(i, paramKeyEvent);
  }
  
  public android.support.v7.d.a b(android.support.v7.d.b paramb)
  {
    if (paramb == null) {
      throw new IllegalArgumentException("ActionMode callback can not be null.");
    }
    if (this.j != null) {
      this.j.c();
    }
    aq localaq = new aq(this, paramb);
    a locala = a();
    if (locala != null)
    {
      this.j = locala.a(localaq);
      if ((this.j != null) && (this.d != null)) {
        this.d.a(this.j);
      }
    }
    if (this.j == null) {
      this.j = a(localaq);
    }
    return this.j;
  }
  
  View b(View paramView, String paramString, Context paramContext, AttributeSet paramAttributeSet)
  {
    if ((this.c instanceof LayoutInflater.Factory))
    {
      View localView = ((LayoutInflater.Factory)this.c).onCreateView(paramString, paramContext, paramAttributeSet);
      if (localView != null) {
        return localView;
      }
    }
    return null;
  }
  
  public void b(Bundle paramBundle)
  {
    p();
  }
  
  public void b(View paramView, ViewGroup.LayoutParams paramLayoutParams)
  {
    p();
    ((ViewGroup)this.s.findViewById(16908290)).addView(paramView, paramLayoutParams);
    this.c.onContentChanged();
  }
  
  void b(CharSequence paramCharSequence)
  {
    if (this.n != null) {
      this.n.setWindowTitle(paramCharSequence);
    }
    do
    {
      return;
      if (a() != null)
      {
        a().b(paramCharSequence);
        return;
      }
    } while (this.t == null);
    this.t.setText(paramCharSequence);
  }
  
  public boolean b(int paramInt)
  {
    switch (paramInt)
    {
    case 3: 
    case 4: 
    case 6: 
    case 7: 
    default: 
      return this.b.requestFeature(paramInt);
    case 8: 
      q();
      this.e = true;
      return true;
    case 9: 
      q();
      this.f = true;
      return true;
    case 10: 
      q();
      this.g = true;
      return true;
    case 2: 
      q();
      this.v = true;
      return true;
    case 5: 
      q();
      this.w = true;
      return true;
    }
    q();
    this.i = true;
    return true;
  }
  
  boolean b(int paramInt, KeyEvent paramKeyEvent)
  {
    switch (paramInt)
    {
    }
    do
    {
      return false;
      e(0, paramKeyEvent);
      return true;
      AppCompatDelegateImplV7.PanelFeatureState localPanelFeatureState = a(0, false);
      if ((localPanelFeatureState != null) && (localPanelFeatureState.o))
      {
        a(localPanelFeatureState, true);
        return true;
      }
    } while (!o());
    return true;
  }
  
  boolean b(int paramInt, Menu paramMenu)
  {
    if (paramInt == 8)
    {
      a locala = a();
      if (locala != null) {
        locala.f(true);
      }
      return true;
    }
    return false;
  }
  
  public View c(View paramView, String paramString, Context paramContext, AttributeSet paramAttributeSet)
  {
    boolean bool1;
    if (Build.VERSION.SDK_INT < 21)
    {
      bool1 = true;
      if (this.G == null) {
        this.G = new android.support.v7.internal.a.a(this.a);
      }
      if ((!bool1) || (!this.q) || (paramView == null) || (paramView.getId() == 16908290)) {
        break label85;
      }
    }
    label85:
    for (boolean bool2 = true;; bool2 = false)
    {
      return this.G.a(paramView, paramString, paramContext, paramAttributeSet, bool2, bool1);
      bool1 = false;
      break;
    }
  }
  
  public void c()
  {
    a locala = a();
    if (locala != null) {
      locala.e(false);
    }
  }
  
  boolean c(int paramInt, KeyEvent paramKeyEvent)
  {
    switch (paramInt)
    {
    default: 
      int i = Build.VERSION.SDK_INT;
      boolean bool = false;
      if (i < 11) {
        bool = a(paramInt, paramKeyEvent);
      }
      return bool;
    }
    d(0, paramKeyEvent);
    return true;
  }
  
  public void d()
  {
    a locala = a();
    if (locala != null) {
      locala.e(true);
    }
  }
  
  public void e()
  {
    a locala = a();
    if ((locala != null) && (locala.c())) {
      return;
    }
    d(0);
  }
  
  public void h()
  {
    LayoutInflater localLayoutInflater = LayoutInflater.from(this.a);
    if (localLayoutInflater.getFactory() == null)
    {
      ac.a(localLayoutInflater, this);
      return;
    }
    Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
  }
  
  public a i()
  {
    p();
    android.support.v7.internal.a.i locali;
    if ((this.c instanceof Activity)) {
      locali = new android.support.v7.internal.a.i((Activity)this.c, this.f);
    }
    for (;;)
    {
      if (locali != null) {
        locali.d(this.D);
      }
      return locali;
      boolean bool = this.c instanceof Dialog;
      locali = null;
      if (bool) {
        locali = new android.support.v7.internal.a.i((Dialog)this.c);
      }
    }
  }
  
  boolean o()
  {
    if (this.j != null) {
      this.j.c();
    }
    a locala;
    do
    {
      return true;
      locala = a();
    } while ((locala != null) && (locala.d()));
    return false;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/app/AppCompatDelegateImplV7.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */