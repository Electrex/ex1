package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.view.ContextThemeWrapper;
import android.view.View;

public class ac
{
  private final t a;
  private int b;
  
  public ac(Context paramContext)
  {
    this(paramContext, ab.a(paramContext, 0));
  }
  
  public ac(Context paramContext, int paramInt)
  {
    this.a = new t(new ContextThemeWrapper(paramContext, ab.a(paramContext, paramInt)));
    this.b = paramInt;
  }
  
  public ab a()
  {
    ab localab = new ab(this.a.a, this.b, false);
    this.a.a(ab.a(localab));
    localab.setCancelable(this.a.o);
    if (this.a.o) {
      localab.setCanceledOnTouchOutside(true);
    }
    localab.setOnCancelListener(this.a.p);
    localab.setOnDismissListener(this.a.q);
    if (this.a.r != null) {
      localab.setOnKeyListener(this.a.r);
    }
    return localab;
  }
  
  public ac a(int paramInt)
  {
    this.a.c = paramInt;
    return this;
  }
  
  public ac a(int paramInt, DialogInterface.OnClickListener paramOnClickListener)
  {
    this.a.i = this.a.a.getText(paramInt);
    this.a.j = paramOnClickListener;
    return this;
  }
  
  public ac a(DialogInterface.OnDismissListener paramOnDismissListener)
  {
    this.a.q = paramOnDismissListener;
    return this;
  }
  
  public ac a(View paramView)
  {
    this.a.w = paramView;
    this.a.v = 0;
    this.a.B = false;
    return this;
  }
  
  public ac a(CharSequence paramCharSequence)
  {
    this.a.f = paramCharSequence;
    return this;
  }
  
  public ac a(CharSequence paramCharSequence, DialogInterface.OnClickListener paramOnClickListener)
  {
    this.a.i = paramCharSequence;
    this.a.j = paramOnClickListener;
    return this;
  }
  
  public ac a(boolean paramBoolean)
  {
    this.a.o = paramBoolean;
    return this;
  }
  
  public ac a(CharSequence[] paramArrayOfCharSequence, DialogInterface.OnClickListener paramOnClickListener)
  {
    this.a.s = paramArrayOfCharSequence;
    this.a.u = paramOnClickListener;
    return this;
  }
  
  public ac a(CharSequence[] paramArrayOfCharSequence, boolean[] paramArrayOfBoolean, DialogInterface.OnMultiChoiceClickListener paramOnMultiChoiceClickListener)
  {
    this.a.s = paramArrayOfCharSequence;
    this.a.G = paramOnMultiChoiceClickListener;
    this.a.C = paramArrayOfBoolean;
    this.a.D = true;
    return this;
  }
  
  public ab b()
  {
    ab localab = a();
    localab.show();
    return localab;
  }
  
  public ac b(int paramInt, DialogInterface.OnClickListener paramOnClickListener)
  {
    this.a.k = this.a.a.getText(paramInt);
    this.a.l = paramOnClickListener;
    return this;
  }
  
  public ac b(CharSequence paramCharSequence)
  {
    this.a.h = paramCharSequence;
    return this;
  }
  
  public ac b(CharSequence paramCharSequence, DialogInterface.OnClickListener paramOnClickListener)
  {
    this.a.k = paramCharSequence;
    this.a.l = paramOnClickListener;
    return this;
  }
  
  public ac c(CharSequence paramCharSequence, DialogInterface.OnClickListener paramOnClickListener)
  {
    this.a.m = paramCharSequence;
    this.a.n = paramOnClickListener;
    return this;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/app/ac.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */