package android.support.v7.internal.widget;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.ListAdapter;

class av
  implements DialogInterface.OnClickListener, ba
{
  private AlertDialog b;
  private ListAdapter c;
  private CharSequence d;
  
  private av(SpinnerCompat paramSpinnerCompat) {}
  
  public void a()
  {
    if (this.b != null)
    {
      this.b.dismiss();
      this.b = null;
    }
  }
  
  public void a(ListAdapter paramListAdapter)
  {
    this.c = paramListAdapter;
  }
  
  public void a(CharSequence paramCharSequence)
  {
    this.d = paramCharSequence;
  }
  
  public boolean b()
  {
    if (this.b != null) {
      return this.b.isShowing();
    }
    return false;
  }
  
  public void c()
  {
    if (this.c == null) {
      return;
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this.a.getContext());
    if (this.d != null) {
      localBuilder.setTitle(this.d);
    }
    this.b = localBuilder.setSingleChoiceItems(this.c, this.a.getSelectedItemPosition(), this).create();
    this.b.show();
  }
  
  public void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    this.a.setSelection(paramInt);
    if (this.a.s != null) {
      this.a.a(null, paramInt, this.c.getItemId(paramInt));
    }
    a();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/av.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */