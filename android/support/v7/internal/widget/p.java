package android.support.v7.internal.widget;

import android.content.pm.ResolveInfo;
import java.math.BigDecimal;

public final class p
  implements Comparable
{
  public final ResolveInfo a;
  public float b;
  
  public p(n paramn, ResolveInfo paramResolveInfo)
  {
    this.a = paramResolveInfo;
  }
  
  public int a(p paramp)
  {
    return Float.floatToIntBits(paramp.b) - Float.floatToIntBits(this.b);
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    p localp;
    do
    {
      return true;
      if (paramObject == null) {
        return false;
      }
      if (getClass() != paramObject.getClass()) {
        return false;
      }
      localp = (p)paramObject;
    } while (Float.floatToIntBits(this.b) == Float.floatToIntBits(localp.b));
    return false;
  }
  
  public int hashCode()
  {
    return 31 + Float.floatToIntBits(this.b);
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("[");
    localStringBuilder.append("resolveInfo:").append(this.a.toString());
    localStringBuilder.append("; weight:").append(new BigDecimal(this.b));
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/p.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */