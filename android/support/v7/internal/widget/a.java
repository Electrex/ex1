package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.os.Build.VERSION;
import android.support.v4.view.bv;
import android.support.v4.view.ea;
import android.support.v7.a.l;
import android.support.v7.internal.view.i;
import android.support.v7.widget.ActionMenuPresenter;
import android.support.v7.widget.ActionMenuView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

abstract class a
  extends ViewGroup
{
  private static final Interpolator j = new DecelerateInterpolator();
  protected final b a = new b(this);
  protected final Context b;
  protected ActionMenuView c;
  protected ActionMenuPresenter d;
  protected ViewGroup e;
  protected boolean f;
  protected boolean g;
  protected int h;
  protected ea i;
  
  a(Context paramContext)
  {
    this(paramContext, null);
  }
  
  a(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  a(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    TypedValue localTypedValue = new TypedValue();
    if ((paramContext.getTheme().resolveAttribute(android.support.v7.a.b.actionBarPopupTheme, localTypedValue, true)) && (localTypedValue.resourceId != 0))
    {
      this.b = new ContextThemeWrapper(paramContext, localTypedValue.resourceId);
      return;
    }
    this.b = paramContext;
  }
  
  protected static int a(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    if (paramBoolean) {
      return paramInt1 - paramInt2;
    }
    return paramInt1 + paramInt2;
  }
  
  protected int a(View paramView, int paramInt1, int paramInt2, int paramInt3)
  {
    paramView.measure(View.MeasureSpec.makeMeasureSpec(paramInt1, Integer.MIN_VALUE), paramInt2);
    return Math.max(0, paramInt1 - paramView.getMeasuredWidth() - paramInt3);
  }
  
  protected int a(View paramView, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
  {
    int k = paramView.getMeasuredWidth();
    int m = paramView.getMeasuredHeight();
    int n = paramInt2 + (paramInt3 - m) / 2;
    if (paramBoolean) {
      paramView.layout(paramInt1 - k, n, paramInt1, m + n);
    }
    for (;;)
    {
      if (paramBoolean) {
        k = -k;
      }
      return k;
      paramView.layout(paramInt1, n, paramInt1 + k, m + n);
    }
  }
  
  public void a(int paramInt)
  {
    if (this.i != null) {
      this.i.a();
    }
    if (paramInt == 0)
    {
      if (getVisibility() != 0)
      {
        bv.c(this, 0.0F);
        if ((this.e != null) && (this.c != null)) {
          bv.c(this.c, 0.0F);
        }
      }
      ea localea3 = bv.p(this).a(1.0F);
      localea3.a(200L);
      localea3.a(j);
      if ((this.e != null) && (this.c != null))
      {
        i locali2 = new i();
        ea localea4 = bv.p(this.c).a(1.0F);
        localea4.a(200L);
        locali2.a(this.a.a(localea3, paramInt));
        locali2.a(localea3).a(localea4);
        locali2.a();
        return;
      }
      localea3.a(this.a.a(localea3, paramInt));
      localea3.b();
      return;
    }
    ea localea1 = bv.p(this).a(0.0F);
    localea1.a(200L);
    localea1.a(j);
    if ((this.e != null) && (this.c != null))
    {
      i locali1 = new i();
      ea localea2 = bv.p(this.c).a(0.0F);
      localea2.a(200L);
      locali1.a(this.a.a(localea1, paramInt));
      locali1.a(localea1).a(localea2);
      locali1.a();
      return;
    }
    localea1.a(this.a.a(localea1, paramInt));
    localea1.b();
  }
  
  public boolean a()
  {
    if (this.d != null) {
      return this.d.c();
    }
    return false;
  }
  
  public int getAnimatedVisibility()
  {
    if (this.i != null) {
      return this.a.a;
    }
    return getVisibility();
  }
  
  public int getContentHeight()
  {
    return this.h;
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration)
  {
    if (Build.VERSION.SDK_INT >= 8) {
      super.onConfigurationChanged(paramConfiguration);
    }
    TypedArray localTypedArray = getContext().obtainStyledAttributes(null, l.ActionBar, android.support.v7.a.b.actionBarStyle, 0);
    setContentHeight(localTypedArray.getLayoutDimension(l.ActionBar_height, 0));
    localTypedArray.recycle();
    if (this.d != null) {
      this.d.a(paramConfiguration);
    }
  }
  
  public void setContentHeight(int paramInt)
  {
    this.h = paramInt;
    requestLayout();
  }
  
  public void setSplitToolbar(boolean paramBoolean)
  {
    this.f = paramBoolean;
  }
  
  public void setSplitView(ViewGroup paramViewGroup)
  {
    this.e = paramViewGroup;
  }
  
  public void setSplitWhenNarrow(boolean paramBoolean)
  {
    this.g = paramBoolean;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */