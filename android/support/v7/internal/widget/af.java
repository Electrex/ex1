package android.support.v7.internal.widget;

import android.support.v7.internal.view.menu.y;
import android.view.Menu;
import android.view.Window.Callback;

public abstract interface af
{
  public abstract void a(int paramInt);
  
  public abstract void a(Menu paramMenu, y paramy);
  
  public abstract boolean d();
  
  public abstract boolean e();
  
  public abstract boolean f();
  
  public abstract boolean g();
  
  public abstract boolean h();
  
  public abstract void i();
  
  public abstract void j();
  
  public abstract void setWindowCallback(Window.Callback paramCallback);
  
  public abstract void setWindowTitle(CharSequence paramCharSequence);
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/af.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */