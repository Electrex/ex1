package android.support.v7.internal.widget;

import android.content.Context;
import android.support.v7.widget.az;
import android.util.AttributeSet;

public class ActivityChooserView$InnerLayout
  extends az
{
  private static final int[] a = { 16842964 };
  
  public ActivityChooserView$InnerLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    bh localbh = bh.a(paramContext, paramAttributeSet, a);
    setBackgroundDrawable(localbh.a(0));
    localbh.b();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/ActivityChooserView$InnerLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */