package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v7.a.b;
import android.support.v7.app.d;
import android.support.v7.widget.ab;
import android.support.v7.widget.az;
import android.support.v7.widget.ba;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

class as
  extends az
  implements View.OnLongClickListener
{
  private final int[] b = { 16842964 };
  private d c;
  private TextView d;
  private ImageView e;
  private View f;
  
  public as(ao paramao, Context paramContext, d paramd, boolean paramBoolean)
  {
    super(paramContext, null, b.actionBarTabStyle);
    this.c = paramd;
    bh localbh = bh.a(paramContext, null, this.b, b.actionBarTabStyle, 0);
    if (localbh.e(0)) {
      setBackgroundDrawable(localbh.a(0));
    }
    localbh.b();
    if (paramBoolean) {
      setGravity(8388627);
    }
    a();
  }
  
  public void a()
  {
    d locald = this.c;
    View localView = locald.c();
    if (localView != null)
    {
      ViewParent localViewParent = localView.getParent();
      if (localViewParent != this)
      {
        if (localViewParent != null) {
          ((ViewGroup)localViewParent).removeView(localView);
        }
        addView(localView);
      }
      this.f = localView;
      if (this.d != null) {
        this.d.setVisibility(8);
      }
      if (this.e != null)
      {
        this.e.setVisibility(8);
        this.e.setImageDrawable(null);
      }
      return;
    }
    if (this.f != null)
    {
      removeView(this.f);
      this.f = null;
    }
    Drawable localDrawable = locald.a();
    CharSequence localCharSequence = locald.b();
    int i;
    if (localDrawable != null)
    {
      if (this.e == null)
      {
        ImageView localImageView = new ImageView(getContext());
        ba localba2 = new ba(-2, -2);
        localba2.h = 16;
        localImageView.setLayoutParams(localba2);
        addView(localImageView, 0);
        this.e = localImageView;
      }
      this.e.setImageDrawable(localDrawable);
      this.e.setVisibility(0);
      if (TextUtils.isEmpty(localCharSequence)) {
        break label372;
      }
      i = 1;
      label213:
      if (i == 0) {
        break label378;
      }
      if (this.d == null)
      {
        ab localab = new ab(getContext(), null, b.actionBarTabTextStyle);
        localab.setEllipsize(TextUtils.TruncateAt.END);
        ba localba1 = new ba(-2, -2);
        localba1.h = 16;
        localab.setLayoutParams(localba1);
        addView(localab);
        this.d = localab;
      }
      this.d.setText(localCharSequence);
      this.d.setVisibility(0);
    }
    for (;;)
    {
      if (this.e != null) {
        this.e.setContentDescription(locald.e());
      }
      if ((i != 0) || (TextUtils.isEmpty(locald.e()))) {
        break label405;
      }
      setOnLongClickListener(this);
      return;
      if (this.e == null) {
        break;
      }
      this.e.setVisibility(8);
      this.e.setImageDrawable(null);
      break;
      label372:
      i = 0;
      break label213;
      label378:
      if (this.d != null)
      {
        this.d.setVisibility(8);
        this.d.setText(null);
      }
    }
    label405:
    setOnLongClickListener(null);
    setLongClickable(false);
  }
  
  public void a(d paramd)
  {
    this.c = paramd;
    a();
  }
  
  public d b()
  {
    return this.c;
  }
  
  public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
  {
    super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
    paramAccessibilityEvent.setClassName(d.class.getName());
  }
  
  public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
  {
    super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
    if (Build.VERSION.SDK_INT >= 14) {
      paramAccessibilityNodeInfo.setClassName(d.class.getName());
    }
  }
  
  public boolean onLongClick(View paramView)
  {
    int[] arrayOfInt = new int[2];
    getLocationOnScreen(arrayOfInt);
    Context localContext = getContext();
    int i = getWidth();
    int j = getHeight();
    int k = localContext.getResources().getDisplayMetrics().widthPixels;
    Toast localToast = Toast.makeText(localContext, this.c.e(), 0);
    localToast.setGravity(49, arrayOfInt[0] + i / 2 - k / 2, j);
    localToast.show();
    return true;
  }
  
  public void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    if ((this.a.b > 0) && (getMeasuredWidth() > this.a.b)) {
      super.onMeasure(View.MeasureSpec.makeMeasureSpec(this.a.b, 1073741824), paramInt2);
    }
  }
  
  public void setSelected(boolean paramBoolean)
  {
    if (isSelected() != paramBoolean) {}
    for (int i = 1;; i = 0)
    {
      super.setSelected(paramBoolean);
      if ((i != 0) && (paramBoolean)) {
        sendAccessibilityEvent(4);
      }
      return;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/as.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */