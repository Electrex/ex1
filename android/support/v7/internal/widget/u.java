package android.support.v7.internal.widget;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.v4.view.bv;
import android.support.v7.a.g;
import android.support.v7.a.i;
import android.support.v7.a.j;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

class u
  extends BaseAdapter
{
  private n b;
  private int c;
  private boolean d;
  private boolean e;
  private boolean f;
  
  public int a()
  {
    int i = 0;
    int j = this.c;
    this.c = Integer.MAX_VALUE;
    int k = View.MeasureSpec.makeMeasureSpec(0, 0);
    int m = View.MeasureSpec.makeMeasureSpec(0, 0);
    int n = getCount();
    View localView = null;
    int i1 = 0;
    while (i < n)
    {
      localView = getView(i, localView, null);
      localView.measure(k, m);
      i1 = Math.max(i1, localView.getMeasuredWidth());
      i++;
    }
    this.c = j;
    return i1;
  }
  
  public void a(int paramInt)
  {
    if (this.c != paramInt)
    {
      this.c = paramInt;
      notifyDataSetChanged();
    }
  }
  
  public void a(n paramn)
  {
    n localn = ActivityChooserView.a(this.a).d();
    if ((localn != null) && (this.a.isShown())) {
      localn.unregisterObserver(ActivityChooserView.g(this.a));
    }
    this.b = paramn;
    if ((paramn != null) && (this.a.isShown())) {
      paramn.registerObserver(ActivityChooserView.g(this.a));
    }
    notifyDataSetChanged();
  }
  
  public void a(boolean paramBoolean)
  {
    if (this.f != paramBoolean)
    {
      this.f = paramBoolean;
      notifyDataSetChanged();
    }
  }
  
  public void a(boolean paramBoolean1, boolean paramBoolean2)
  {
    if ((this.d != paramBoolean1) || (this.e != paramBoolean2))
    {
      this.d = paramBoolean1;
      this.e = paramBoolean2;
      notifyDataSetChanged();
    }
  }
  
  public ResolveInfo b()
  {
    return this.b.b();
  }
  
  public int c()
  {
    return this.b.a();
  }
  
  public n d()
  {
    return this.b;
  }
  
  public boolean e()
  {
    return this.d;
  }
  
  public int getCount()
  {
    int i = this.b.a();
    if ((!this.d) && (this.b.b() != null)) {
      i--;
    }
    int j = Math.min(i, this.c);
    if (this.f) {
      j++;
    }
    return j;
  }
  
  public Object getItem(int paramInt)
  {
    switch (getItemViewType(paramInt))
    {
    default: 
      throw new IllegalArgumentException();
    case 1: 
      return null;
    }
    if ((!this.d) && (this.b.b() != null)) {
      paramInt++;
    }
    return this.b.a(paramInt);
  }
  
  public long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public int getItemViewType(int paramInt)
  {
    if ((this.f) && (paramInt == -1 + getCount())) {
      return 1;
    }
    return 0;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    switch (getItemViewType(paramInt))
    {
    default: 
      throw new IllegalArgumentException();
    case 1: 
      if ((paramView == null) || (paramView.getId() != 1))
      {
        paramView = LayoutInflater.from(this.a.getContext()).inflate(i.abc_activity_chooser_view_list_item, paramViewGroup, false);
        paramView.setId(1);
        ((TextView)paramView.findViewById(g.title)).setText(this.a.getContext().getString(j.abc_activity_chooser_view_see_all));
      }
      return paramView;
    }
    if ((paramView == null) || (paramView.getId() != g.list_item)) {
      paramView = LayoutInflater.from(this.a.getContext()).inflate(i.abc_activity_chooser_view_list_item, paramViewGroup, false);
    }
    PackageManager localPackageManager = this.a.getContext().getPackageManager();
    ImageView localImageView = (ImageView)paramView.findViewById(g.icon);
    ResolveInfo localResolveInfo = (ResolveInfo)getItem(paramInt);
    localImageView.setImageDrawable(localResolveInfo.loadIcon(localPackageManager));
    ((TextView)paramView.findViewById(g.title)).setText(localResolveInfo.loadLabel(localPackageManager));
    if ((this.d) && (paramInt == 0) && (this.e))
    {
      bv.b(paramView, true);
      return paramView;
    }
    bv.b(paramView, false);
    return paramView;
  }
  
  public int getViewTypeCount()
  {
    return 3;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/u.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */