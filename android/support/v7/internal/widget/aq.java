package android.support.v7.internal.widget;

import android.support.v7.app.d;
import android.support.v7.widget.az;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

class aq
  extends BaseAdapter
{
  private aq(ao paramao) {}
  
  public int getCount()
  {
    return ao.a(this.a).getChildCount();
  }
  
  public Object getItem(int paramInt)
  {
    return ((as)ao.a(this.a).getChildAt(paramInt)).b();
  }
  
  public long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramView == null) {
      return ao.a(this.a, (d)getItem(paramInt), true);
    }
    ((as)paramView).a((d)getItem(paramInt));
    return paramView;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/aq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */