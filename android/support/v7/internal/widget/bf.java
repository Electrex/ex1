package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build.VERSION;
import android.support.v7.a.b;
import android.support.v7.a.f;
import android.util.SparseArray;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

public final class bf
{
  public static final boolean a;
  private static final PorterDuff.Mode b;
  private static final WeakHashMap c;
  private static final bg d;
  private static final int[] e;
  private static final int[] f;
  private static final int[] g;
  private static final int[] h;
  private static final int[] i;
  private final WeakReference j;
  private SparseArray k;
  private ColorStateList l;
  
  static
  {
    if (Build.VERSION.SDK_INT < 21) {}
    for (boolean bool = true;; bool = false)
    {
      a = bool;
      b = PorterDuff.Mode.SRC_IN;
      c = new WeakHashMap();
      d = new bg(6);
      int[] arrayOfInt1 = new int[3];
      arrayOfInt1[0] = f.abc_textfield_search_default_mtrl_alpha;
      arrayOfInt1[1] = f.abc_textfield_default_mtrl_alpha;
      arrayOfInt1[2] = f.abc_ab_share_pack_mtrl_alpha;
      e = arrayOfInt1;
      int[] arrayOfInt2 = new int[12];
      arrayOfInt2[0] = f.abc_ic_ab_back_mtrl_am_alpha;
      arrayOfInt2[1] = f.abc_ic_go_search_api_mtrl_alpha;
      arrayOfInt2[2] = f.abc_ic_search_api_mtrl_alpha;
      arrayOfInt2[3] = f.abc_ic_commit_search_api_mtrl_alpha;
      arrayOfInt2[4] = f.abc_ic_clear_mtrl_alpha;
      arrayOfInt2[5] = f.abc_ic_menu_share_mtrl_alpha;
      arrayOfInt2[6] = f.abc_ic_menu_copy_mtrl_am_alpha;
      arrayOfInt2[7] = f.abc_ic_menu_cut_mtrl_alpha;
      arrayOfInt2[8] = f.abc_ic_menu_selectall_mtrl_alpha;
      arrayOfInt2[9] = f.abc_ic_menu_paste_mtrl_am_alpha;
      arrayOfInt2[10] = f.abc_ic_menu_moreoverflow_mtrl_alpha;
      arrayOfInt2[11] = f.abc_ic_voice_search_api_mtrl_alpha;
      f = arrayOfInt2;
      int[] arrayOfInt3 = new int[4];
      arrayOfInt3[0] = f.abc_textfield_activated_mtrl_alpha;
      arrayOfInt3[1] = f.abc_textfield_search_activated_mtrl_alpha;
      arrayOfInt3[2] = f.abc_cab_background_top_mtrl_alpha;
      arrayOfInt3[3] = f.abc_text_cursor_mtrl_alpha;
      g = arrayOfInt3;
      int[] arrayOfInt4 = new int[3];
      arrayOfInt4[0] = f.abc_popup_background_mtrl_mult;
      arrayOfInt4[1] = f.abc_cab_background_internal_bg;
      arrayOfInt4[2] = f.abc_menu_hardkey_panel_mtrl_mult;
      h = arrayOfInt4;
      int[] arrayOfInt5 = new int[12];
      arrayOfInt5[0] = f.abc_edit_text_material;
      arrayOfInt5[1] = f.abc_tab_indicator_material;
      arrayOfInt5[2] = f.abc_textfield_search_material;
      arrayOfInt5[3] = f.abc_spinner_mtrl_am_alpha;
      arrayOfInt5[4] = f.abc_btn_check_material;
      arrayOfInt5[5] = f.abc_btn_radio_material;
      arrayOfInt5[6] = f.abc_spinner_textfield_background_material;
      arrayOfInt5[7] = f.abc_ratingbar_full_material;
      arrayOfInt5[8] = f.abc_switch_track_mtrl_alpha;
      arrayOfInt5[9] = f.abc_switch_thumb_material;
      arrayOfInt5[10] = f.abc_btn_default_mtrl_shape;
      arrayOfInt5[11] = f.abc_btn_borderless_material;
      i = arrayOfInt5;
      return;
    }
  }
  
  private bf(Context paramContext)
  {
    this.j = new WeakReference(paramContext);
  }
  
  public static Drawable a(Context paramContext, int paramInt)
  {
    if (d(paramInt)) {
      return a(paramContext).a(paramInt);
    }
    return android.support.v4.a.a.a(paramContext, paramInt);
  }
  
  public static bf a(Context paramContext)
  {
    bf localbf = (bf)c.get(paramContext);
    if (localbf == null)
    {
      localbf = new bf(paramContext);
      c.put(paramContext, localbf);
    }
    return localbf;
  }
  
  private static void a(Drawable paramDrawable, int paramInt, PorterDuff.Mode paramMode)
  {
    if (paramMode == null) {
      paramMode = b;
    }
    PorterDuffColorFilter localPorterDuffColorFilter = d.a(paramInt, paramMode);
    if (localPorterDuffColorFilter == null)
    {
      localPorterDuffColorFilter = new PorterDuffColorFilter(paramInt, paramMode);
      d.a(paramInt, paramMode, localPorterDuffColorFilter);
    }
    paramDrawable.setColorFilter(localPorterDuffColorFilter);
  }
  
  public static void a(View paramView, be parambe)
  {
    Drawable localDrawable = paramView.getBackground();
    PorterDuff.Mode localMode;
    if (parambe.d)
    {
      int m = parambe.a.getColorForState(paramView.getDrawableState(), parambe.a.getDefaultColor());
      if (parambe.c)
      {
        localMode = parambe.b;
        a(localDrawable, m, localMode);
      }
    }
    for (;;)
    {
      if (Build.VERSION.SDK_INT <= 10) {
        paramView.invalidate();
      }
      return;
      localMode = null;
      break;
      localDrawable.clearColorFilter();
    }
  }
  
  private static boolean a(int[] paramArrayOfInt, int paramInt)
  {
    int m = paramArrayOfInt.length;
    for (int n = 0;; n++)
    {
      boolean bool = false;
      if (n < m)
      {
        if (paramArrayOfInt[n] == paramInt) {
          bool = true;
        }
      }
      else {
        return bool;
      }
    }
  }
  
  private ColorStateList b(Context paramContext)
  {
    if (this.l == null)
    {
      int m = bb.a(paramContext, b.colorControlNormal);
      int n = bb.a(paramContext, b.colorControlActivated);
      int[][] arrayOfInt = new int[7][];
      int[] arrayOfInt1 = new int[7];
      arrayOfInt[0] = bb.a;
      arrayOfInt1[0] = bb.c(paramContext, b.colorControlNormal);
      arrayOfInt[1] = bb.b;
      arrayOfInt1[1] = n;
      arrayOfInt[2] = bb.c;
      arrayOfInt1[2] = n;
      arrayOfInt[3] = bb.d;
      arrayOfInt1[3] = n;
      arrayOfInt[4] = bb.e;
      arrayOfInt1[4] = n;
      arrayOfInt[5] = bb.f;
      arrayOfInt1[5] = n;
      arrayOfInt[6] = bb.h;
      arrayOfInt1[6] = m;
      this.l = new ColorStateList(arrayOfInt, arrayOfInt1);
    }
    return this.l;
  }
  
  private ColorStateList c(Context paramContext)
  {
    int[][] arrayOfInt = new int[3][];
    int[] arrayOfInt1 = new int[3];
    arrayOfInt[0] = bb.a;
    arrayOfInt1[0] = bb.a(paramContext, 16842800, 0.1F);
    arrayOfInt[1] = bb.e;
    arrayOfInt1[1] = bb.a(paramContext, b.colorControlActivated, 0.3F);
    arrayOfInt[2] = bb.h;
    arrayOfInt1[2] = bb.a(paramContext, 16842800, 0.3F);
    return new ColorStateList(arrayOfInt, arrayOfInt1);
  }
  
  private ColorStateList d(Context paramContext)
  {
    int[][] arrayOfInt = new int[3][];
    int[] arrayOfInt1 = new int[3];
    ColorStateList localColorStateList = bb.b(paramContext, b.colorSwitchThumbNormal);
    if ((localColorStateList != null) && (localColorStateList.isStateful()))
    {
      arrayOfInt[0] = bb.a;
      arrayOfInt1[0] = localColorStateList.getColorForState(arrayOfInt[0], 0);
      arrayOfInt[1] = bb.e;
      arrayOfInt1[1] = bb.a(paramContext, b.colorControlActivated);
      arrayOfInt[2] = bb.h;
      arrayOfInt1[2] = localColorStateList.getDefaultColor();
    }
    for (;;)
    {
      return new ColorStateList(arrayOfInt, arrayOfInt1);
      arrayOfInt[0] = bb.a;
      arrayOfInt1[0] = bb.c(paramContext, b.colorSwitchThumbNormal);
      arrayOfInt[1] = bb.e;
      arrayOfInt1[1] = bb.a(paramContext, b.colorControlActivated);
      arrayOfInt[2] = bb.h;
      arrayOfInt1[2] = bb.a(paramContext, b.colorSwitchThumbNormal);
    }
  }
  
  private static boolean d(int paramInt)
  {
    return (a(f, paramInt)) || (a(e, paramInt)) || (a(g, paramInt)) || (a(i, paramInt)) || (a(h, paramInt)) || (paramInt == f.abc_cab_background_top_material);
  }
  
  private ColorStateList e(Context paramContext)
  {
    int[][] arrayOfInt = new int[3][];
    int[] arrayOfInt1 = new int[3];
    arrayOfInt[0] = bb.a;
    arrayOfInt1[0] = bb.c(paramContext, b.colorControlNormal);
    arrayOfInt[1] = bb.g;
    arrayOfInt1[1] = bb.a(paramContext, b.colorControlNormal);
    arrayOfInt[2] = bb.h;
    arrayOfInt1[2] = bb.a(paramContext, b.colorControlActivated);
    return new ColorStateList(arrayOfInt, arrayOfInt1);
  }
  
  private ColorStateList f(Context paramContext)
  {
    int[][] arrayOfInt = new int[4][];
    int[] arrayOfInt1 = new int[4];
    int m = bb.a(paramContext, b.colorButtonNormal);
    int n = bb.a(paramContext, b.colorControlHighlight);
    arrayOfInt[0] = bb.a;
    arrayOfInt1[0] = bb.c(paramContext, b.colorButtonNormal);
    arrayOfInt[1] = bb.d;
    arrayOfInt1[1] = android.support.v4.b.a.a(n, m);
    arrayOfInt[2] = bb.b;
    arrayOfInt1[2] = android.support.v4.b.a.a(n, m);
    arrayOfInt[3] = bb.h;
    arrayOfInt1[3] = m;
    return new ColorStateList(arrayOfInt, arrayOfInt1);
  }
  
  private ColorStateList g(Context paramContext)
  {
    int[][] arrayOfInt = new int[3][];
    int[] arrayOfInt1 = new int[3];
    arrayOfInt[0] = bb.a;
    arrayOfInt1[0] = bb.c(paramContext, b.colorControlNormal);
    arrayOfInt[1] = bb.g;
    arrayOfInt1[1] = bb.a(paramContext, b.colorControlNormal);
    arrayOfInt[2] = bb.h;
    arrayOfInt1[2] = bb.a(paramContext, b.colorControlActivated);
    return new ColorStateList(arrayOfInt, arrayOfInt1);
  }
  
  public Drawable a(int paramInt)
  {
    return a(paramInt, false);
  }
  
  public Drawable a(int paramInt, boolean paramBoolean)
  {
    Context localContext = (Context)this.j.get();
    if (localContext == null) {
      return null;
    }
    Drawable localDrawable = android.support.v4.a.a.a(localContext, paramInt);
    if (localDrawable != null)
    {
      if (Build.VERSION.SDK_INT >= 8) {
        localDrawable = localDrawable.mutate();
      }
      ColorStateList localColorStateList = c(paramInt);
      if (localColorStateList == null) {
        break label92;
      }
      localDrawable = android.support.v4.b.a.a.c(localDrawable);
      android.support.v4.b.a.a.a(localDrawable, localColorStateList);
      PorterDuff.Mode localMode = b(paramInt);
      if (localMode != null) {
        android.support.v4.b.a.a.a(localDrawable, localMode);
      }
    }
    for (;;)
    {
      return localDrawable;
      label92:
      if (paramInt == f.abc_cab_background_top_material)
      {
        Drawable[] arrayOfDrawable = new Drawable[2];
        arrayOfDrawable[0] = a(f.abc_cab_background_internal_bg);
        arrayOfDrawable[1] = a(f.abc_cab_background_top_mtrl_alpha);
        return new LayerDrawable(arrayOfDrawable);
      }
      if ((!a(paramInt, localDrawable)) && (paramBoolean)) {
        localDrawable = null;
      }
    }
  }
  
  public final boolean a(int paramInt, Drawable paramDrawable)
  {
    Context localContext = (Context)this.j.get();
    if (localContext == null) {
      return false;
    }
    int n;
    Object localObject;
    int i1;
    int m;
    if (a(e, paramInt))
    {
      n = b.colorControlNormal;
      localObject = null;
      i1 = 1;
      m = -1;
    }
    for (;;)
    {
      if (i1 != 0)
      {
        a(paramDrawable, bb.a(localContext, n), (PorterDuff.Mode)localObject);
        if (m != -1) {
          paramDrawable.setAlpha(m);
        }
        return true;
        if (a(g, paramInt))
        {
          n = b.colorControlActivated;
          i1 = 1;
          m = -1;
          localObject = null;
          continue;
        }
        if (a(h, paramInt))
        {
          PorterDuff.Mode localMode = PorterDuff.Mode.MULTIPLY;
          i1 = 1;
          localObject = localMode;
          n = 16842801;
          m = -1;
          continue;
        }
        if (paramInt == f.abc_list_divider_mtrl_alpha)
        {
          n = 16842800;
          m = Math.round(40.8F);
          i1 = 1;
          localObject = null;
        }
      }
      else
      {
        return false;
      }
      m = -1;
      n = 0;
      i1 = 0;
      localObject = null;
    }
  }
  
  final PorterDuff.Mode b(int paramInt)
  {
    int m = f.abc_switch_thumb_material;
    PorterDuff.Mode localMode = null;
    if (paramInt == m) {
      localMode = PorterDuff.Mode.MULTIPLY;
    }
    return localMode;
  }
  
  public final ColorStateList c(int paramInt)
  {
    Context localContext = (Context)this.j.get();
    Object localObject;
    if (localContext == null) {
      localObject = null;
    }
    ColorStateList localColorStateList;
    for (;;)
    {
      return (ColorStateList)localObject;
      SparseArray localSparseArray = this.k;
      localColorStateList = null;
      if (localSparseArray != null) {
        localColorStateList = (ColorStateList)this.k.get(paramInt);
      }
      if (localColorStateList != null) {
        break;
      }
      if (paramInt == f.abc_edit_text_material) {
        localObject = e(localContext);
      }
      while (localObject != null)
      {
        if (this.k == null) {
          this.k = new SparseArray();
        }
        this.k.append(paramInt, localObject);
        return (ColorStateList)localObject;
        if (paramInt == f.abc_switch_track_mtrl_alpha) {
          localObject = c(localContext);
        } else if (paramInt == f.abc_switch_thumb_material) {
          localObject = d(localContext);
        } else if ((paramInt == f.abc_btn_default_mtrl_shape) || (paramInt == f.abc_btn_borderless_material)) {
          localObject = f(localContext);
        } else if ((paramInt == f.abc_spinner_mtrl_am_alpha) || (paramInt == f.abc_spinner_textfield_background_material)) {
          localObject = g(localContext);
        } else if (a(f, paramInt)) {
          localObject = bb.b(localContext, b.colorControlNormal);
        } else if (a(i, paramInt)) {
          localObject = b(localContext);
        } else {
          localObject = localColorStateList;
        }
      }
    }
    return localColorStateList;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/bf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */