package android.support.v7.internal.widget;

import android.database.DataSetObserver;
import android.os.Parcelable;
import android.widget.Adapter;

class y
  extends DataSetObserver
{
  private Parcelable b = null;
  
  y(w paramw) {}
  
  public void onChanged()
  {
    this.a.u = true;
    this.a.A = this.a.z;
    this.a.z = this.a.getAdapter().getCount();
    if ((this.a.getAdapter().hasStableIds()) && (this.b != null) && (this.a.A == 0) && (this.a.z > 0))
    {
      w.a(this.a, this.b);
      this.b = null;
    }
    for (;;)
    {
      this.a.e();
      this.a.requestLayout();
      return;
      this.a.j();
    }
  }
  
  public void onInvalidated()
  {
    this.a.u = true;
    if (this.a.getAdapter().hasStableIds()) {
      this.b = w.a(this.a);
    }
    this.a.A = this.a.z;
    this.a.z = 0;
    this.a.x = -1;
    this.a.y = Long.MIN_VALUE;
    this.a.v = -1;
    this.a.w = Long.MIN_VALUE;
    this.a.o = false;
    this.a.e();
    this.a.requestLayout();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/y.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */