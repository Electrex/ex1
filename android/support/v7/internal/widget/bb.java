package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v4.b.a;
import android.util.TypedValue;

public class bb
{
  static final int[] a = { -16842910 };
  static final int[] b = { 16842908 };
  static final int[] c = { 16843518 };
  static final int[] d = { 16842919 };
  static final int[] e = { 16842912 };
  static final int[] f = { 16842913 };
  static final int[] g = { -16842919, -16842908 };
  static final int[] h = new int[0];
  private static final ThreadLocal i = new ThreadLocal();
  private static final int[] j = new int[1];
  
  public static int a(Context paramContext, int paramInt)
  {
    j[0] = paramInt;
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(null, j);
    try
    {
      int k = localTypedArray.getColor(0, 0);
      return k;
    }
    finally
    {
      localTypedArray.recycle();
    }
  }
  
  static int a(Context paramContext, int paramInt, float paramFloat)
  {
    int k = a(paramContext, paramInt);
    return a.b(k, Math.round(paramFloat * Color.alpha(k)));
  }
  
  public static ColorStateList a(int paramInt1, int paramInt2)
  {
    int[][] arrayOfInt = new int[2][];
    int[] arrayOfInt1 = new int[2];
    arrayOfInt[0] = a;
    arrayOfInt1[0] = paramInt2;
    arrayOfInt[1] = h;
    arrayOfInt1[1] = paramInt1;
    return new ColorStateList(arrayOfInt, arrayOfInt1);
  }
  
  private static TypedValue a()
  {
    TypedValue localTypedValue = (TypedValue)i.get();
    if (localTypedValue == null)
    {
      localTypedValue = new TypedValue();
      i.set(localTypedValue);
    }
    return localTypedValue;
  }
  
  public static ColorStateList b(Context paramContext, int paramInt)
  {
    j[0] = paramInt;
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(null, j);
    try
    {
      ColorStateList localColorStateList = localTypedArray.getColorStateList(0);
      return localColorStateList;
    }
    finally
    {
      localTypedArray.recycle();
    }
  }
  
  public static int c(Context paramContext, int paramInt)
  {
    ColorStateList localColorStateList = b(paramContext, paramInt);
    if ((localColorStateList != null) && (localColorStateList.isStateful())) {
      return localColorStateList.getColorForState(a, localColorStateList.getDefaultColor());
    }
    TypedValue localTypedValue = a();
    paramContext.getTheme().resolveAttribute(16842803, localTypedValue, true);
    return a(paramContext, paramInt, localTypedValue.getFloat());
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/bb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */