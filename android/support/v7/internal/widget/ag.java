package android.support.v7.internal.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.internal.view.menu.j;
import android.support.v7.internal.view.menu.y;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.Window.Callback;

public abstract interface ag
{
  public abstract ViewGroup a();
  
  public abstract void a(int paramInt);
  
  public abstract void a(Drawable paramDrawable);
  
  public abstract void a(y paramy, j paramj);
  
  public abstract void a(ao paramao);
  
  public abstract void a(Menu paramMenu, y paramy);
  
  public abstract void a(Window.Callback paramCallback);
  
  public abstract void a(CharSequence paramCharSequence);
  
  public abstract void a(boolean paramBoolean);
  
  public abstract Context b();
  
  public abstract void b(int paramInt);
  
  public abstract void b(Drawable paramDrawable);
  
  public abstract void b(CharSequence paramCharSequence);
  
  public abstract void b(boolean paramBoolean);
  
  public abstract void c(int paramInt);
  
  public abstract boolean c();
  
  public abstract void d(int paramInt);
  
  public abstract boolean d();
  
  public abstract void e();
  
  public abstract void e(int paramInt);
  
  public abstract CharSequence f();
  
  public abstract void g();
  
  public abstract void h();
  
  public abstract boolean i();
  
  public abstract boolean j();
  
  public abstract boolean k();
  
  public abstract boolean l();
  
  public abstract boolean m();
  
  public abstract void n();
  
  public abstract void o();
  
  public abstract int p();
  
  public abstract int q();
  
  public abstract Menu r();
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/ag.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */