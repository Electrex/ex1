package android.support.v7.internal.widget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.DataSetObservable;
import android.support.v4.d.a;
import android.text.TextUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class n
  extends DataSetObservable
{
  private static final String a = n.class.getSimpleName();
  private static final Object b = new Object();
  private static final Map c = new HashMap();
  private final Object d;
  private final List e;
  private final List f;
  private final Context g;
  private final String h;
  private Intent i;
  private q j;
  private int k;
  private boolean l;
  private boolean m;
  private boolean n;
  private boolean o;
  private s p;
  
  private boolean a(r paramr)
  {
    boolean bool = this.f.add(paramr);
    if (bool)
    {
      this.n = true;
      i();
      d();
      f();
      notifyChanged();
    }
    return bool;
  }
  
  private void d()
  {
    if (!this.m) {
      throw new IllegalStateException("No preceding call to #readHistoricalData");
    }
    if (!this.n) {}
    do
    {
      return;
      this.n = false;
    } while (TextUtils.isEmpty(this.h));
    t localt = new t(this, null);
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = this.f;
    arrayOfObject[1] = this.h;
    a.a(localt, arrayOfObject);
  }
  
  private void e()
  {
    boolean bool = g() | h();
    i();
    if (bool)
    {
      f();
      notifyChanged();
    }
  }
  
  private boolean f()
  {
    if ((this.j != null) && (this.i != null) && (!this.e.isEmpty()) && (!this.f.isEmpty()))
    {
      this.j.a(this.i, this.e, Collections.unmodifiableList(this.f));
      return true;
    }
    return false;
  }
  
  private boolean g()
  {
    boolean bool1 = this.o;
    boolean bool2 = false;
    if (bool1)
    {
      Intent localIntent = this.i;
      bool2 = false;
      if (localIntent != null)
      {
        this.o = false;
        this.e.clear();
        List localList = this.g.getPackageManager().queryIntentActivities(this.i, 0);
        int i1 = localList.size();
        for (int i2 = 0; i2 < i1; i2++)
        {
          ResolveInfo localResolveInfo = (ResolveInfo)localList.get(i2);
          this.e.add(new p(this, localResolveInfo));
        }
        bool2 = true;
      }
    }
    return bool2;
  }
  
  private boolean h()
  {
    if ((this.l) && (this.n) && (!TextUtils.isEmpty(this.h)))
    {
      this.l = false;
      this.m = true;
      j();
      return true;
    }
    return false;
  }
  
  private void i()
  {
    int i1 = this.f.size() - this.k;
    if (i1 <= 0) {}
    for (;;)
    {
      return;
      this.n = true;
      for (int i2 = 0; i2 < i1; i2++) {
        ((r)this.f.remove(0));
      }
    }
  }
  
  /* Error */
  private void j()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 55	android/support/v7/internal/widget/n:g	Landroid/content/Context;
    //   4: aload_0
    //   5: getfield 82	android/support/v7/internal/widget/n:h	Ljava/lang/String;
    //   8: invokevirtual 181	android/content/Context:openFileInput	(Ljava/lang/String;)Ljava/io/FileInputStream;
    //   11: astore_2
    //   12: invokestatic 187	android/util/Xml:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
    //   15: astore 11
    //   17: aload 11
    //   19: aload_2
    //   20: aconst_null
    //   21: invokeinterface 193 3 0
    //   26: iconst_0
    //   27: istore 12
    //   29: iload 12
    //   31: iconst_1
    //   32: if_icmpeq +21 -> 53
    //   35: iload 12
    //   37: iconst_2
    //   38: if_icmpeq +15 -> 53
    //   41: aload 11
    //   43: invokeinterface 196 1 0
    //   48: istore 12
    //   50: goto -21 -> 29
    //   53: ldc -58
    //   55: aload 11
    //   57: invokeinterface 201 1 0
    //   62: invokevirtual 206	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   65: ifne +55 -> 120
    //   68: new 175	org/xmlpull/v1/XmlPullParserException
    //   71: dup
    //   72: ldc -48
    //   74: invokespecial 209	org/xmlpull/v1/XmlPullParserException:<init>	(Ljava/lang/String;)V
    //   77: athrow
    //   78: astore 8
    //   80: getstatic 40	android/support/v7/internal/widget/n:a	Ljava/lang/String;
    //   83: new 211	java/lang/StringBuilder
    //   86: dup
    //   87: invokespecial 212	java/lang/StringBuilder:<init>	()V
    //   90: ldc -42
    //   92: invokevirtual 218	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   95: aload_0
    //   96: getfield 82	android/support/v7/internal/widget/n:h	Ljava/lang/String;
    //   99: invokevirtual 218	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   102: invokevirtual 221	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   105: aload 8
    //   107: invokestatic 226	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   110: pop
    //   111: aload_2
    //   112: ifnull +7 -> 119
    //   115: aload_2
    //   116: invokevirtual 231	java/io/FileInputStream:close	()V
    //   119: return
    //   120: aload_0
    //   121: getfield 61	android/support/v7/internal/widget/n:f	Ljava/util/List;
    //   124: astore 13
    //   126: aload 13
    //   128: invokeinterface 135 1 0
    //   133: aload 11
    //   135: invokeinterface 196 1 0
    //   140: istore 14
    //   142: iload 14
    //   144: iconst_1
    //   145: if_icmpne +15 -> 160
    //   148: aload_2
    //   149: ifnull -30 -> 119
    //   152: aload_2
    //   153: invokevirtual 231	java/io/FileInputStream:close	()V
    //   156: return
    //   157: astore 16
    //   159: return
    //   160: iload 14
    //   162: iconst_3
    //   163: if_icmpeq -30 -> 133
    //   166: iload 14
    //   168: iconst_4
    //   169: if_icmpeq -36 -> 133
    //   172: ldc -23
    //   174: aload 11
    //   176: invokeinterface 201 1 0
    //   181: invokevirtual 206	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   184: ifne +58 -> 242
    //   187: new 175	org/xmlpull/v1/XmlPullParserException
    //   190: dup
    //   191: ldc -21
    //   193: invokespecial 209	org/xmlpull/v1/XmlPullParserException:<init>	(Ljava/lang/String;)V
    //   196: athrow
    //   197: astore 5
    //   199: getstatic 40	android/support/v7/internal/widget/n:a	Ljava/lang/String;
    //   202: new 211	java/lang/StringBuilder
    //   205: dup
    //   206: invokespecial 212	java/lang/StringBuilder:<init>	()V
    //   209: ldc -42
    //   211: invokevirtual 218	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   214: aload_0
    //   215: getfield 82	android/support/v7/internal/widget/n:h	Ljava/lang/String;
    //   218: invokevirtual 218	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   221: invokevirtual 221	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   224: aload 5
    //   226: invokestatic 226	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   229: pop
    //   230: aload_2
    //   231: ifnull -112 -> 119
    //   234: aload_2
    //   235: invokevirtual 231	java/io/FileInputStream:close	()V
    //   238: return
    //   239: astore 7
    //   241: return
    //   242: aload 13
    //   244: new 171	android/support/v7/internal/widget/r
    //   247: dup
    //   248: aload 11
    //   250: aconst_null
    //   251: ldc -19
    //   253: invokeinterface 241 3 0
    //   258: aload 11
    //   260: aconst_null
    //   261: ldc -13
    //   263: invokeinterface 241 3 0
    //   268: invokestatic 249	java/lang/Long:parseLong	(Ljava/lang/String;)J
    //   271: aload 11
    //   273: aconst_null
    //   274: ldc -5
    //   276: invokeinterface 241 3 0
    //   281: invokestatic 257	java/lang/Float:parseFloat	(Ljava/lang/String;)F
    //   284: invokespecial 260	android/support/v7/internal/widget/r:<init>	(Ljava/lang/String;JF)V
    //   287: invokeinterface 67 2 0
    //   292: pop
    //   293: goto -160 -> 133
    //   296: astore_3
    //   297: aload_2
    //   298: ifnull +7 -> 305
    //   301: aload_2
    //   302: invokevirtual 231	java/io/FileInputStream:close	()V
    //   305: aload_3
    //   306: athrow
    //   307: astore 10
    //   309: return
    //   310: astore 4
    //   312: goto -7 -> 305
    //   315: astore_1
    //   316: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	317	0	this	n
    //   315	1	1	localFileNotFoundException	java.io.FileNotFoundException
    //   11	291	2	localFileInputStream	java.io.FileInputStream
    //   296	10	3	localObject	Object
    //   310	1	4	localIOException1	java.io.IOException
    //   197	28	5	localIOException2	java.io.IOException
    //   239	1	7	localIOException3	java.io.IOException
    //   78	28	8	localXmlPullParserException	org.xmlpull.v1.XmlPullParserException
    //   307	1	10	localIOException4	java.io.IOException
    //   15	257	11	localXmlPullParser	org.xmlpull.v1.XmlPullParser
    //   27	22	12	i1	int
    //   124	119	13	localList	List
    //   140	30	14	i2	int
    //   157	1	16	localIOException5	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   12	26	78	org/xmlpull/v1/XmlPullParserException
    //   41	50	78	org/xmlpull/v1/XmlPullParserException
    //   53	78	78	org/xmlpull/v1/XmlPullParserException
    //   120	133	78	org/xmlpull/v1/XmlPullParserException
    //   133	142	78	org/xmlpull/v1/XmlPullParserException
    //   172	197	78	org/xmlpull/v1/XmlPullParserException
    //   242	293	78	org/xmlpull/v1/XmlPullParserException
    //   152	156	157	java/io/IOException
    //   12	26	197	java/io/IOException
    //   41	50	197	java/io/IOException
    //   53	78	197	java/io/IOException
    //   120	133	197	java/io/IOException
    //   133	142	197	java/io/IOException
    //   172	197	197	java/io/IOException
    //   242	293	197	java/io/IOException
    //   234	238	239	java/io/IOException
    //   12	26	296	finally
    //   41	50	296	finally
    //   53	78	296	finally
    //   80	111	296	finally
    //   120	133	296	finally
    //   133	142	296	finally
    //   172	197	296	finally
    //   199	230	296	finally
    //   242	293	296	finally
    //   115	119	307	java/io/IOException
    //   301	305	310	java/io/IOException
    //   0	12	315	java/io/FileNotFoundException
  }
  
  public int a()
  {
    synchronized (this.d)
    {
      e();
      int i1 = this.e.size();
      return i1;
    }
  }
  
  public int a(ResolveInfo paramResolveInfo)
  {
    for (;;)
    {
      int i2;
      synchronized (this.d)
      {
        e();
        List localList = this.e;
        int i1 = localList.size();
        i2 = 0;
        if (i2 < i1)
        {
          if (((p)localList.get(i2)).a == paramResolveInfo) {
            return i2;
          }
        }
        else {
          return -1;
        }
      }
      i2++;
    }
  }
  
  public ResolveInfo a(int paramInt)
  {
    synchronized (this.d)
    {
      e();
      ResolveInfo localResolveInfo = ((p)this.e.get(paramInt)).a;
      return localResolveInfo;
    }
  }
  
  public Intent b(int paramInt)
  {
    synchronized (this.d)
    {
      if (this.i == null) {
        return null;
      }
      e();
      p localp = (p)this.e.get(paramInt);
      ComponentName localComponentName = new ComponentName(localp.a.activityInfo.packageName, localp.a.activityInfo.name);
      Intent localIntent1 = new Intent(this.i);
      localIntent1.setComponent(localComponentName);
      if (this.p != null)
      {
        Intent localIntent2 = new Intent(localIntent1);
        if (this.p.a(this, localIntent2)) {
          return null;
        }
      }
      a(new r(localComponentName, System.currentTimeMillis(), 1.0F));
      return localIntent1;
    }
  }
  
  public ResolveInfo b()
  {
    synchronized (this.d)
    {
      e();
      if (!this.e.isEmpty())
      {
        ResolveInfo localResolveInfo = ((p)this.e.get(0)).a;
        return localResolveInfo;
      }
      return null;
    }
  }
  
  public void c(int paramInt)
  {
    for (;;)
    {
      synchronized (this.d)
      {
        e();
        p localp1 = (p)this.e.get(paramInt);
        p localp2 = (p)this.e.get(0);
        if (localp2 != null)
        {
          f1 = 5.0F + (localp2.b - localp1.b);
          a(new r(new ComponentName(localp1.a.activityInfo.packageName, localp1.a.activityInfo.name), System.currentTimeMillis(), f1));
          return;
        }
      }
      float f1 = 1.0F;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/n.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */