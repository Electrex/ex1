package android.support.v7.internal.widget;

import android.util.SparseArray;
import android.view.View;

class c
{
  private final SparseArray b = new SparseArray();
  
  c(AbsSpinnerCompat paramAbsSpinnerCompat) {}
  
  View a(int paramInt)
  {
    View localView = (View)this.b.get(paramInt);
    if (localView != null) {
      this.b.delete(paramInt);
    }
    return localView;
  }
  
  void a()
  {
    SparseArray localSparseArray = this.b;
    int i = localSparseArray.size();
    for (int j = 0; j < i; j++)
    {
      View localView = (View)localSparseArray.valueAt(j);
      if (localView != null) {
        AbsSpinnerCompat.a(this.a, localView, true);
      }
    }
    localSparseArray.clear();
  }
  
  public void a(int paramInt, View paramView)
  {
    this.b.put(paramInt, paramView);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */