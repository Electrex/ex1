package android.support.v7.internal.widget;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.PopupWindow.OnDismissListener;

class v
  implements View.OnClickListener, View.OnLongClickListener, AdapterView.OnItemClickListener, PopupWindow.OnDismissListener
{
  private void a()
  {
    if (ActivityChooserView.f(this.a) != null) {
      ActivityChooserView.f(this.a).onDismiss();
    }
  }
  
  public void onClick(View paramView)
  {
    if (paramView == ActivityChooserView.c(this.a))
    {
      this.a.b();
      ResolveInfo localResolveInfo = ActivityChooserView.a(this.a).b();
      int i = ActivityChooserView.a(this.a).d().a(localResolveInfo);
      Intent localIntent = ActivityChooserView.a(this.a).d().b(i);
      if (localIntent != null)
      {
        localIntent.addFlags(524288);
        this.a.getContext().startActivity(localIntent);
      }
      return;
    }
    if (paramView == ActivityChooserView.d(this.a))
    {
      ActivityChooserView.a(this.a, false);
      ActivityChooserView.a(this.a, ActivityChooserView.e(this.a));
      return;
    }
    throw new IllegalArgumentException();
  }
  
  public void onDismiss()
  {
    a();
    if (this.a.a != null) {
      this.a.a.a(false);
    }
  }
  
  public void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    switch (((u)paramAdapterView.getAdapter()).getItemViewType(paramInt))
    {
    default: 
      throw new IllegalArgumentException();
    case 1: 
      ActivityChooserView.a(this.a, Integer.MAX_VALUE);
    }
    do
    {
      return;
      this.a.b();
      if (!ActivityChooserView.b(this.a)) {
        break;
      }
    } while (paramInt <= 0);
    ActivityChooserView.a(this.a).d().c(paramInt);
    return;
    if (ActivityChooserView.a(this.a).e()) {}
    for (;;)
    {
      Intent localIntent = ActivityChooserView.a(this.a).d().b(paramInt);
      if (localIntent == null) {
        break;
      }
      localIntent.addFlags(524288);
      this.a.getContext().startActivity(localIntent);
      return;
      paramInt++;
    }
  }
  
  public boolean onLongClick(View paramView)
  {
    if (paramView == ActivityChooserView.c(this.a))
    {
      if (ActivityChooserView.a(this.a).getCount() > 0)
      {
        ActivityChooserView.a(this.a, true);
        ActivityChooserView.a(this.a, ActivityChooserView.e(this.a));
      }
      return true;
    }
    throw new IllegalArgumentException();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/v.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */