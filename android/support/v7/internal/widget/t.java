package android.support.v7.internal.widget;

import android.content.ComponentName;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Xml;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import org.xmlpull.v1.XmlSerializer;

final class t
  extends AsyncTask
{
  private t(n paramn) {}
  
  public Void a(Object... paramVarArgs)
  {
    int i = 0;
    List localList = (List)paramVarArgs[0];
    String str = (String)paramVarArgs[1];
    FileOutputStream localFileOutputStream;
    for (;;)
    {
      try
      {
        localFileOutputStream = n.a(this.a).openFileOutput(str, 0);
        localXmlSerializer = Xml.newSerializer();
        int j;
        r localr;
        localXmlSerializer.endTag(null, "historical-records");
      }
      catch (FileNotFoundException localFileNotFoundException)
      {
        try
        {
          localXmlSerializer.setOutput(localFileOutputStream, null);
          localXmlSerializer.startDocument("UTF-8", Boolean.valueOf(true));
          localXmlSerializer.startTag(null, "historical-records");
          j = localList.size();
          if (i >= j) {
            break label215;
          }
          localr = (r)localList.remove(0);
          localXmlSerializer.startTag(null, "historical-record");
          localXmlSerializer.attribute(null, "activity", localr.a.flattenToString());
          localXmlSerializer.attribute(null, "time", String.valueOf(localr.b));
          localXmlSerializer.attribute(null, "weight", String.valueOf(localr.c));
          localXmlSerializer.endTag(null, "historical-record");
          i++;
          continue;
          localFileNotFoundException = localFileNotFoundException;
          Log.e(n.c(), "Error writing historical recrod file: " + str, localFileNotFoundException);
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
          XmlSerializer localXmlSerializer;
          Log.e(n.c(), "Error writing historical recrod file: " + n.b(this.a), localIllegalArgumentException);
          n.a(this.a, true);
          if (localFileOutputStream == null) {
            continue;
          }
          try
          {
            localFileOutputStream.close();
            return null;
          }
          catch (IOException localIOException5)
          {
            return null;
          }
        }
        catch (IllegalStateException localIllegalStateException)
        {
          Log.e(n.c(), "Error writing historical recrod file: " + n.b(this.a), localIllegalStateException);
          n.a(this.a, true);
          if (localFileOutputStream == null) {
            continue;
          }
          try
          {
            localFileOutputStream.close();
            return null;
          }
          catch (IOException localIOException4)
          {
            return null;
          }
        }
        catch (IOException localIOException2)
        {
          Log.e(n.c(), "Error writing historical recrod file: " + n.b(this.a), localIOException2);
          n.a(this.a, true);
          if (localFileOutputStream == null) {
            continue;
          }
          try
          {
            localFileOutputStream.close();
            return null;
          }
          catch (IOException localIOException3)
          {
            return null;
          }
        }
        finally
        {
          n.a(this.a, true);
          if (localFileOutputStream == null) {
            break label462;
          }
        }
        return null;
      }
      label215:
      localXmlSerializer.endDocument();
      n.a(this.a, true);
      if (localFileOutputStream != null) {
        try
        {
          localFileOutputStream.close();
          return null;
        }
        catch (IOException localIOException6)
        {
          return null;
        }
      }
    }
    try
    {
      localFileOutputStream.close();
      label462:
      throw ((Throwable)localObject);
    }
    catch (IOException localIOException1)
    {
      for (;;) {}
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/t.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */