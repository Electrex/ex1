package android.support.v7.internal.widget;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class d
  implements Parcelable.Creator
{
  public AbsSpinnerCompat.SavedState a(Parcel paramParcel)
  {
    return new AbsSpinnerCompat.SavedState(paramParcel);
  }
  
  public AbsSpinnerCompat.SavedState[] a(int paramInt)
  {
    return new AbsSpinnerCompat.SavedState[paramInt];
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */