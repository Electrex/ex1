package android.support.v7.internal.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.bv;
import android.support.v4.view.ea;
import android.support.v7.a.b;
import android.support.v7.a.f;
import android.support.v7.a.g;
import android.support.v7.a.l;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.y;
import android.support.v7.widget.ActionMenuPresenter;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.dv;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window.Callback;

public class bi
  implements ag
{
  private Toolbar a;
  private int b;
  private View c;
  private View d;
  private Drawable e;
  private Drawable f;
  private Drawable g;
  private boolean h;
  private CharSequence i;
  private CharSequence j;
  private CharSequence k;
  private Window.Callback l;
  private boolean m;
  private ActionMenuPresenter n;
  private int o = 0;
  private final bf p;
  private int q = 0;
  private Drawable r;
  
  public bi(Toolbar paramToolbar, boolean paramBoolean)
  {
    this(paramToolbar, paramBoolean, android.support.v7.a.j.abc_action_bar_up_description, f.abc_ic_ab_back_mtrl_am_alpha);
  }
  
  public bi(Toolbar paramToolbar, boolean paramBoolean, int paramInt1, int paramInt2)
  {
    this.a = paramToolbar;
    this.i = paramToolbar.getTitle();
    this.j = paramToolbar.getSubtitle();
    boolean bool;
    bh localbh;
    if (this.i != null)
    {
      bool = true;
      this.h = bool;
      this.g = paramToolbar.getNavigationIcon();
      if (!paramBoolean) {
        break label508;
      }
      localbh = bh.a(paramToolbar.getContext(), null, l.ActionBar, b.actionBarStyle, 0);
      CharSequence localCharSequence1 = localbh.c(l.ActionBar_title);
      if (!TextUtils.isEmpty(localCharSequence1)) {
        b(localCharSequence1);
      }
      CharSequence localCharSequence2 = localbh.c(l.ActionBar_subtitle);
      if (!TextUtils.isEmpty(localCharSequence2)) {
        c(localCharSequence2);
      }
      Drawable localDrawable1 = localbh.a(l.ActionBar_logo);
      if (localDrawable1 != null) {
        d(localDrawable1);
      }
      Drawable localDrawable2 = localbh.a(l.ActionBar_icon);
      if ((this.g == null) && (localDrawable2 != null)) {
        a(localDrawable2);
      }
      Drawable localDrawable3 = localbh.a(l.ActionBar_homeAsUpIndicator);
      if (localDrawable3 != null) {
        b(localDrawable3);
      }
      c(localbh.a(l.ActionBar_displayOptions, 0));
      int i1 = localbh.f(l.ActionBar_customNavigationLayout, 0);
      if (i1 != 0)
      {
        a(LayoutInflater.from(this.a.getContext()).inflate(i1, this.a, false));
        c(0x10 | this.b);
      }
      int i2 = localbh.e(l.ActionBar_height, 0);
      if (i2 > 0)
      {
        ViewGroup.LayoutParams localLayoutParams = this.a.getLayoutParams();
        localLayoutParams.height = i2;
        this.a.setLayoutParams(localLayoutParams);
      }
      int i3 = localbh.c(l.ActionBar_contentInsetStart, -1);
      int i4 = localbh.c(l.ActionBar_contentInsetEnd, -1);
      if ((i3 >= 0) || (i4 >= 0)) {
        this.a.a(Math.max(i3, 0), Math.max(i4, 0));
      }
      int i5 = localbh.f(l.ActionBar_titleTextStyle, 0);
      if (i5 != 0) {
        this.a.a(this.a.getContext(), i5);
      }
      int i6 = localbh.f(l.ActionBar_subtitleTextStyle, 0);
      if (i6 != 0) {
        this.a.b(this.a.getContext(), i6);
      }
      int i7 = localbh.f(l.ActionBar_popupTheme, 0);
      if (i7 != 0) {
        this.a.setPopupTheme(i7);
      }
      localbh.b();
    }
    for (this.p = localbh.c();; this.p = bf.a(paramToolbar.getContext()))
    {
      f(paramInt1);
      this.k = this.a.getNavigationContentDescription();
      c(this.p.a(paramInt2));
      this.a.setNavigationOnClickListener(new bj(this));
      return;
      bool = false;
      break;
      label508:
      this.b = s();
    }
  }
  
  private void e(CharSequence paramCharSequence)
  {
    this.i = paramCharSequence;
    if ((0x8 & this.b) != 0) {
      this.a.setTitle(paramCharSequence);
    }
  }
  
  private int s()
  {
    int i1 = 11;
    if (this.a.getNavigationIcon() != null) {
      i1 = 15;
    }
    return i1;
  }
  
  private void t()
  {
    int i1 = 0x2 & this.b;
    Drawable localDrawable = null;
    if (i1 != 0)
    {
      if ((0x1 & this.b) == 0) {
        break label51;
      }
      if (this.f == null) {
        break label43;
      }
      localDrawable = this.f;
    }
    for (;;)
    {
      this.a.setLogo(localDrawable);
      return;
      label43:
      localDrawable = this.e;
      continue;
      label51:
      localDrawable = this.e;
    }
  }
  
  private void u()
  {
    if ((0x4 & this.b) != 0)
    {
      if (TextUtils.isEmpty(this.k)) {
        this.a.setNavigationContentDescription(this.q);
      }
    }
    else {
      return;
    }
    this.a.setNavigationContentDescription(this.k);
  }
  
  private void v()
  {
    Toolbar localToolbar;
    if ((0x4 & this.b) != 0)
    {
      localToolbar = this.a;
      if (this.g == null) {
        break label32;
      }
    }
    label32:
    for (Drawable localDrawable = this.g;; localDrawable = this.r)
    {
      localToolbar.setNavigationIcon(localDrawable);
      return;
    }
  }
  
  public ViewGroup a()
  {
    return this.a;
  }
  
  public void a(int paramInt)
  {
    if (paramInt != 0) {}
    for (Drawable localDrawable = this.p.a(paramInt);; localDrawable = null)
    {
      a(localDrawable);
      return;
    }
  }
  
  public void a(Drawable paramDrawable)
  {
    this.e = paramDrawable;
    t();
  }
  
  public void a(y paramy, android.support.v7.internal.view.menu.j paramj)
  {
    this.a.a(paramy, paramj);
  }
  
  public void a(ao paramao)
  {
    if ((this.c != null) && (this.c.getParent() == this.a)) {
      this.a.removeView(this.c);
    }
    this.c = paramao;
    if ((paramao != null) && (this.o == 2))
    {
      this.a.addView(this.c, 0);
      dv localdv = (dv)this.c.getLayoutParams();
      localdv.width = -2;
      localdv.height = -2;
      localdv.a = 8388691;
      paramao.setAllowCollapse(true);
    }
  }
  
  public void a(Menu paramMenu, y paramy)
  {
    if (this.n == null)
    {
      this.n = new ActionMenuPresenter(this.a.getContext());
      this.n.a(g.action_menu_presenter);
    }
    this.n.a(paramy);
    this.a.a((i)paramMenu, this.n);
  }
  
  public void a(View paramView)
  {
    if ((this.d != null) && ((0x10 & this.b) != 0)) {
      this.a.removeView(this.d);
    }
    this.d = paramView;
    if ((paramView != null) && ((0x10 & this.b) != 0)) {
      this.a.addView(this.d);
    }
  }
  
  public void a(Window.Callback paramCallback)
  {
    this.l = paramCallback;
  }
  
  public void a(CharSequence paramCharSequence)
  {
    if (!this.h) {
      e(paramCharSequence);
    }
  }
  
  public void a(boolean paramBoolean)
  {
    this.a.setCollapsible(paramBoolean);
  }
  
  public Context b()
  {
    return this.a.getContext();
  }
  
  public void b(int paramInt)
  {
    if (paramInt != 0) {}
    for (Drawable localDrawable = this.p.a(paramInt);; localDrawable = null)
    {
      d(localDrawable);
      return;
    }
  }
  
  public void b(Drawable paramDrawable)
  {
    this.g = paramDrawable;
    v();
  }
  
  public void b(CharSequence paramCharSequence)
  {
    this.h = true;
    e(paramCharSequence);
  }
  
  public void b(boolean paramBoolean) {}
  
  public void c(int paramInt)
  {
    int i1 = paramInt ^ this.b;
    this.b = paramInt;
    if (i1 != 0)
    {
      if ((i1 & 0x4) != 0)
      {
        if ((paramInt & 0x4) == 0) {
          break label115;
        }
        v();
        u();
      }
      if ((i1 & 0x3) != 0) {
        t();
      }
      if ((i1 & 0x8) != 0)
      {
        if ((paramInt & 0x8) == 0) {
          break label126;
        }
        this.a.setTitle(this.i);
        this.a.setSubtitle(this.j);
      }
    }
    for (;;)
    {
      if (((i1 & 0x10) != 0) && (this.d != null))
      {
        if ((paramInt & 0x10) == 0) {
          break label145;
        }
        this.a.addView(this.d);
      }
      return;
      label115:
      this.a.setNavigationIcon(null);
      break;
      label126:
      this.a.setTitle(null);
      this.a.setSubtitle(null);
    }
    label145:
    this.a.removeView(this.d);
  }
  
  public void c(Drawable paramDrawable)
  {
    if (this.r != paramDrawable)
    {
      this.r = paramDrawable;
      v();
    }
  }
  
  public void c(CharSequence paramCharSequence)
  {
    this.j = paramCharSequence;
    if ((0x8 & this.b) != 0) {
      this.a.setSubtitle(paramCharSequence);
    }
  }
  
  public boolean c()
  {
    return false;
  }
  
  public void d(int paramInt)
  {
    if (paramInt == 8) {
      bv.p(this.a).a(0.0F).a(new bk(this));
    }
    while (paramInt != 0) {
      return;
    }
    bv.p(this.a).a(1.0F).a(new bl(this));
  }
  
  public void d(Drawable paramDrawable)
  {
    this.f = paramDrawable;
    t();
  }
  
  public void d(CharSequence paramCharSequence)
  {
    this.k = paramCharSequence;
    u();
  }
  
  public boolean d()
  {
    return this.a.g();
  }
  
  public void e()
  {
    this.a.h();
  }
  
  public void e(int paramInt)
  {
    if (paramInt == 0) {}
    for (Object localObject = null;; localObject = b().getString(paramInt))
    {
      d((CharSequence)localObject);
      return;
    }
  }
  
  public CharSequence f()
  {
    return this.a.getTitle();
  }
  
  public void f(int paramInt)
  {
    if (paramInt == this.q) {}
    do
    {
      return;
      this.q = paramInt;
    } while (!TextUtils.isEmpty(this.a.getNavigationContentDescription()));
    e(this.q);
  }
  
  public void g()
  {
    Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
  }
  
  public void h()
  {
    Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
  }
  
  public boolean i()
  {
    return this.a.a();
  }
  
  public boolean j()
  {
    return this.a.b();
  }
  
  public boolean k()
  {
    return this.a.c();
  }
  
  public boolean l()
  {
    return this.a.d();
  }
  
  public boolean m()
  {
    return this.a.e();
  }
  
  public void n()
  {
    this.m = true;
  }
  
  public void o()
  {
    this.a.f();
  }
  
  public int p()
  {
    return this.b;
  }
  
  public int q()
  {
    return this.o;
  }
  
  public Menu r()
  {
    return this.a.getMenu();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/bi.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */