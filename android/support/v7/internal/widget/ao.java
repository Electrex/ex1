package android.support.v7.internal.widget;

import android.content.res.Configuration;
import android.os.Build.VERSION;
import android.support.v7.a.b;
import android.support.v7.app.d;
import android.support.v7.internal.view.a;
import android.support.v7.widget.az;
import android.support.v7.widget.ba;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.AbsListView.LayoutParams;
import android.widget.HorizontalScrollView;

public class ao
  extends HorizontalScrollView
  implements z
{
  private static final Interpolator j = new DecelerateInterpolator();
  Runnable a;
  int b;
  int c;
  private ar d;
  private az e;
  private SpinnerCompat f;
  private boolean g;
  private int h;
  private int i;
  
  private as a(d paramd, boolean paramBoolean)
  {
    as localas = new as(this, getContext(), paramd, paramBoolean);
    if (paramBoolean)
    {
      localas.setBackgroundDrawable(null);
      localas.setLayoutParams(new AbsListView.LayoutParams(-1, this.h));
      return localas;
    }
    localas.setFocusable(true);
    if (this.d == null) {
      this.d = new ar(this, null);
    }
    localas.setOnClickListener(this.d);
    return localas;
  }
  
  private boolean a()
  {
    return (this.f != null) && (this.f.getParent() == this);
  }
  
  private void b()
  {
    if (a()) {
      return;
    }
    if (this.f == null) {
      this.f = d();
    }
    removeView(this.e);
    addView(this.f, new ViewGroup.LayoutParams(-2, -1));
    if (this.f.c() == null) {
      this.f.a(new aq(this, null));
    }
    if (this.a != null)
    {
      removeCallbacks(this.a);
      this.a = null;
    }
    this.f.setSelection(this.i);
  }
  
  private boolean c()
  {
    if (!a()) {
      return false;
    }
    removeView(this.f);
    addView(this.e, new ViewGroup.LayoutParams(-2, -1));
    setTabSelected(this.f.getSelectedItemPosition());
    return false;
  }
  
  private SpinnerCompat d()
  {
    SpinnerCompat localSpinnerCompat = new SpinnerCompat(getContext(), null, b.actionDropDownStyle);
    localSpinnerCompat.setLayoutParams(new ba(-2, -1));
    localSpinnerCompat.a(this);
    return localSpinnerCompat;
  }
  
  public void a(int paramInt)
  {
    View localView = this.e.getChildAt(paramInt);
    if (this.a != null) {
      removeCallbacks(this.a);
    }
    this.a = new ap(this, localView);
    post(this.a);
  }
  
  public void a(w paramw, View paramView, int paramInt, long paramLong)
  {
    ((as)paramView).b().d();
  }
  
  public void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    if (this.a != null) {
      post(this.a);
    }
  }
  
  protected void onConfigurationChanged(Configuration paramConfiguration)
  {
    if (Build.VERSION.SDK_INT >= 8) {
      super.onConfigurationChanged(paramConfiguration);
    }
    a locala = a.a(getContext());
    setContentHeight(locala.e());
    this.c = locala.g();
  }
  
  public void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    if (this.a != null) {
      removeCallbacks(this.a);
    }
  }
  
  public void onMeasure(int paramInt1, int paramInt2)
  {
    int k = 1;
    int m = View.MeasureSpec.getMode(paramInt1);
    boolean bool;
    label72:
    label87:
    int i1;
    if (m == 1073741824)
    {
      bool = k;
      setFillViewport(bool);
      int n = this.e.getChildCount();
      if ((n <= k) || ((m != 1073741824) && (m != Integer.MIN_VALUE))) {
        break label201;
      }
      if (n <= 2) {
        break label188;
      }
      this.b = ((int)(0.4F * View.MeasureSpec.getSize(paramInt1)));
      this.b = Math.min(this.b, this.c);
      i1 = View.MeasureSpec.makeMeasureSpec(this.h, 1073741824);
      if ((bool) || (!this.g)) {
        break label209;
      }
      label110:
      if (k == 0) {
        break label222;
      }
      this.e.measure(0, i1);
      if (this.e.getMeasuredWidth() <= View.MeasureSpec.getSize(paramInt1)) {
        break label214;
      }
      b();
    }
    for (;;)
    {
      int i2 = getMeasuredWidth();
      super.onMeasure(paramInt1, i1);
      int i3 = getMeasuredWidth();
      if ((bool) && (i2 != i3)) {
        setTabSelected(this.i);
      }
      return;
      bool = false;
      break;
      label188:
      this.b = (View.MeasureSpec.getSize(paramInt1) / 2);
      break label72;
      label201:
      this.b = -1;
      break label87;
      label209:
      k = 0;
      break label110;
      label214:
      c();
      continue;
      label222:
      c();
    }
  }
  
  public void setAllowCollapse(boolean paramBoolean)
  {
    this.g = paramBoolean;
  }
  
  public void setContentHeight(int paramInt)
  {
    this.h = paramInt;
    requestLayout();
  }
  
  public void setTabSelected(int paramInt)
  {
    this.i = paramInt;
    int k = this.e.getChildCount();
    int m = 0;
    if (m < k)
    {
      View localView = this.e.getChildAt(m);
      if (m == paramInt) {}
      for (boolean bool = true;; bool = false)
      {
        localView.setSelected(bool);
        if (bool) {
          a(paramInt);
        }
        m++;
        break;
      }
    }
    if ((this.f != null) && (paramInt >= 0)) {
      this.f.setSelection(paramInt);
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/ao.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */