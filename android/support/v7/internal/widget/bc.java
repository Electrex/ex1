package android.support.v7.internal.widget;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;

public class bc
  extends ContextWrapper
{
  private Resources a;
  
  private bc(Context paramContext)
  {
    super(paramContext);
  }
  
  public static Context a(Context paramContext)
  {
    if (!(paramContext instanceof bc)) {
      paramContext = new bc(paramContext);
    }
    return paramContext;
  }
  
  public Resources getResources()
  {
    if (this.a == null) {
      this.a = new bd(super.getResources(), bf.a(this));
    }
    return this.a;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/bc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */