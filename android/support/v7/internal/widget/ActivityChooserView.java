package android.support.v7.internal.widget;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.support.v7.a.j;
import android.support.v7.widget.az;
import android.support.v7.widget.bf;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow.OnDismissListener;

public class ActivityChooserView
  extends ViewGroup
{
  android.support.v4.view.n a;
  private final u b;
  private final v c;
  private final az d;
  private final FrameLayout e;
  private final ImageView f;
  private final FrameLayout g;
  private final int h;
  private final DataSetObserver i;
  private final ViewTreeObserver.OnGlobalLayoutListener j;
  private bf k;
  private PopupWindow.OnDismissListener l;
  private boolean m;
  private int n;
  private boolean o;
  private int p;
  
  private void a(int paramInt)
  {
    if (this.b.d() == null) {
      throw new IllegalStateException("No data model. Did you call #setDataModel?");
    }
    getViewTreeObserver().addOnGlobalLayoutListener(this.j);
    boolean bool;
    int i2;
    label58:
    label90:
    bf localbf;
    if (this.g.getVisibility() == 0)
    {
      bool = true;
      int i1 = this.b.c();
      if (!bool) {
        break label187;
      }
      i2 = 1;
      if ((paramInt == Integer.MAX_VALUE) || (i1 <= i2 + paramInt)) {
        break label193;
      }
      this.b.a(true);
      this.b.a(paramInt - 1);
      localbf = getListPopupWindow();
      if (!localbf.b())
      {
        if ((!this.m) && (bool)) {
          break label212;
        }
        this.b.a(true, bool);
      }
    }
    for (;;)
    {
      localbf.d(Math.min(this.b.a(), this.h));
      localbf.c();
      if (this.a != null) {
        this.a.a(true);
      }
      localbf.g().setContentDescription(getContext().getString(j.abc_activitychooserview_choose_application));
      return;
      bool = false;
      break;
      label187:
      i2 = 0;
      break label58;
      label193:
      this.b.a(false);
      this.b.a(paramInt);
      break label90;
      label212:
      this.b.a(false, false);
    }
  }
  
  private bf getListPopupWindow()
  {
    if (this.k == null)
    {
      this.k = new bf(getContext());
      this.k.a(this.b);
      this.k.a(this);
      this.k.a(true);
      this.k.a(this.c);
      this.k.a(this.c);
    }
    return this.k;
  }
  
  public boolean a()
  {
    if ((c()) || (!this.o)) {
      return false;
    }
    this.m = false;
    a(this.n);
    return true;
  }
  
  public boolean b()
  {
    if (c())
    {
      getListPopupWindow().a();
      ViewTreeObserver localViewTreeObserver = getViewTreeObserver();
      if (localViewTreeObserver.isAlive()) {
        localViewTreeObserver.removeGlobalOnLayoutListener(this.j);
      }
    }
    return true;
  }
  
  public boolean c()
  {
    return getListPopupWindow().b();
  }
  
  public n getDataModel()
  {
    return this.b.d();
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    n localn = this.b.d();
    if (localn != null) {
      localn.registerObserver(this.i);
    }
    this.o = true;
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    n localn = this.b.d();
    if (localn != null) {
      localn.unregisterObserver(this.i);
    }
    ViewTreeObserver localViewTreeObserver = getViewTreeObserver();
    if (localViewTreeObserver.isAlive()) {
      localViewTreeObserver.removeGlobalOnLayoutListener(this.j);
    }
    if (c()) {
      b();
    }
    this.o = false;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.d.layout(0, 0, paramInt3 - paramInt1, paramInt4 - paramInt2);
    if (!c()) {
      b();
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    az localaz = this.d;
    if (this.g.getVisibility() != 0) {
      paramInt2 = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(paramInt2), 1073741824);
    }
    measureChild(localaz, paramInt1, paramInt2);
    setMeasuredDimension(localaz.getMeasuredWidth(), localaz.getMeasuredHeight());
  }
  
  public void setActivityChooserModel(n paramn)
  {
    this.b.a(paramn);
    if (c())
    {
      b();
      a();
    }
  }
  
  public void setDefaultActionButtonContentDescription(int paramInt)
  {
    this.p = paramInt;
  }
  
  public void setExpandActivityOverflowButtonContentDescription(int paramInt)
  {
    String str = getContext().getString(paramInt);
    this.f.setContentDescription(str);
  }
  
  public void setExpandActivityOverflowButtonDrawable(Drawable paramDrawable)
  {
    this.f.setImageDrawable(paramDrawable);
  }
  
  public void setInitialActivityCount(int paramInt)
  {
    this.n = paramInt;
  }
  
  public void setOnDismissListener(PopupWindow.OnDismissListener paramOnDismissListener)
  {
    this.l = paramOnDismissListener;
  }
  
  public void setProvider(android.support.v4.view.n paramn)
  {
    this.a = paramn;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/widget/ActivityChooserView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */