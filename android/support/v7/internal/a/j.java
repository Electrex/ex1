package android.support.v7.internal.a;

import android.support.v4.view.bv;
import android.support.v4.view.er;
import android.support.v7.internal.widget.ActionBarContainer;
import android.view.View;

class j
  extends er
{
  j(i parami) {}
  
  public void b(View paramView)
  {
    if ((i.a(this.a)) && (i.b(this.a) != null))
    {
      bv.b(i.b(this.a), 0.0F);
      bv.b(i.c(this.a), 0.0F);
    }
    if ((i.d(this.a) != null) && (i.e(this.a) == 1)) {
      i.d(this.a).setVisibility(8);
    }
    i.c(this.a).setVisibility(8);
    i.c(this.a).setTransitioning(false);
    i.a(this.a, null);
    this.a.e();
    if (i.f(this.a) != null) {
      bv.s(i.f(this.a));
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/a/j.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */