package android.support.v7.internal.a;

import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.y;
import android.view.Window.Callback;

final class g
  implements y
{
  private g(b paramb) {}
  
  public void a(i parami, boolean paramBoolean)
  {
    if (b.a(this.a) != null) {
      b.a(this.a).onPanelClosed(0, parami);
    }
  }
  
  public boolean a(i parami)
  {
    if ((parami == null) && (b.a(this.a) != null)) {
      b.a(this.a).onMenuOpened(0, parami);
    }
    return true;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/a/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */