package android.support.v7.internal.a;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.drawable.Drawable;
import android.support.v4.view.bv;
import android.support.v7.a.k;
import android.support.v7.app.a;
import android.support.v7.internal.widget.ag;
import android.support.v7.internal.widget.bi;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.dw;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.Window.Callback;
import android.widget.ListAdapter;
import java.util.ArrayList;

public class b
  extends a
{
  private ag a;
  private boolean b;
  private Window.Callback c;
  private boolean d;
  private boolean e;
  private ArrayList f = new ArrayList();
  private Window g;
  private android.support.v7.internal.view.menu.g h;
  private final Runnable i = new c(this);
  private final dw j = new d(this);
  
  public b(Toolbar paramToolbar, CharSequence paramCharSequence, Window paramWindow)
  {
    this.a = new bi(paramToolbar, false);
    this.c = new h(this, paramWindow.getCallback());
    this.a.a(this.c);
    paramToolbar.setOnMenuItemClickListener(this.j);
    this.a.a(paramCharSequence);
    this.g = paramWindow;
  }
  
  private View a(Menu paramMenu)
  {
    b(paramMenu);
    if ((paramMenu == null) || (this.h == null)) {}
    while (this.h.a().getCount() <= 0) {
      return null;
    }
    return (View)this.h.a(this.a.a());
  }
  
  private void b(Menu paramMenu)
  {
    android.support.v7.internal.view.menu.i locali;
    Context localContext;
    Resources.Theme localTheme;
    if ((this.h == null) && ((paramMenu instanceof android.support.v7.internal.view.menu.i)))
    {
      locali = (android.support.v7.internal.view.menu.i)paramMenu;
      localContext = this.a.b();
      TypedValue localTypedValue = new TypedValue();
      localTheme = localContext.getResources().newTheme();
      localTheme.setTo(localContext.getTheme());
      localTheme.resolveAttribute(android.support.v7.a.b.panelMenuListTheme, localTypedValue, true);
      if (localTypedValue.resourceId == 0) {
        break label149;
      }
      localTheme.applyStyle(localTypedValue.resourceId, true);
    }
    for (;;)
    {
      ContextThemeWrapper localContextThemeWrapper = new ContextThemeWrapper(localContext, 0);
      localContextThemeWrapper.getTheme().setTo(localTheme);
      this.h = new android.support.v7.internal.view.menu.g(localContextThemeWrapper, android.support.v7.a.i.abc_list_menu_item_layout);
      this.h.a(new g(this, null));
      locali.a(this.h);
      return;
      label149:
      localTheme.applyStyle(k.Theme_AppCompat_CompactMenu, true);
    }
  }
  
  private Menu g()
  {
    if (!this.d)
    {
      this.a.a(new e(this, null), new f(this, null));
      this.d = true;
    }
    return this.a.r();
  }
  
  public int a()
  {
    return this.a.p();
  }
  
  public void a(float paramFloat)
  {
    bv.f(this.a.a(), paramFloat);
  }
  
  public void a(int paramInt)
  {
    this.a.e(paramInt);
  }
  
  public void a(int paramInt1, int paramInt2)
  {
    int k = this.a.p();
    this.a.c(paramInt1 & paramInt2 | k & (paramInt2 ^ 0xFFFFFFFF));
  }
  
  public void a(Configuration paramConfiguration)
  {
    super.a(paramConfiguration);
  }
  
  public void a(Drawable paramDrawable)
  {
    this.a.b(paramDrawable);
  }
  
  public void a(CharSequence paramCharSequence)
  {
    this.a.b(paramCharSequence);
  }
  
  public void a(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (int k = 4;; k = 0)
    {
      a(k, 4);
      return;
    }
  }
  
  public boolean a(int paramInt, KeyEvent paramKeyEvent)
  {
    Menu localMenu = g();
    boolean bool = false;
    if (localMenu != null) {
      bool = localMenu.performShortcut(paramInt, paramKeyEvent, 0);
    }
    return bool;
  }
  
  public Context b()
  {
    return this.a.b();
  }
  
  public void b(CharSequence paramCharSequence)
  {
    this.a.a(paramCharSequence);
  }
  
  public void b(boolean paramBoolean) {}
  
  public boolean c()
  {
    this.a.a().removeCallbacks(this.i);
    bv.a(this.a.a(), this.i);
    return true;
  }
  
  public void d(boolean paramBoolean) {}
  
  public boolean d()
  {
    if (this.a.d())
    {
      this.a.e();
      return true;
    }
    return false;
  }
  
  public Window.Callback e()
  {
    return this.c;
  }
  
  public void e(boolean paramBoolean) {}
  
  void f()
  {
    Menu localMenu = g();
    if ((localMenu instanceof android.support.v7.internal.view.menu.i)) {}
    for (locali = (android.support.v7.internal.view.menu.i)localMenu;; locali = null)
    {
      if (locali != null) {
        locali.g();
      }
      try
      {
        localMenu.clear();
        if ((!this.c.onCreatePanelMenu(0, localMenu)) || (!this.c.onPreparePanel(0, null, localMenu))) {
          localMenu.clear();
        }
        return;
      }
      finally
      {
        if (locali == null) {
          break;
        }
        locali.h();
      }
    }
  }
  
  public void f(boolean paramBoolean)
  {
    if (paramBoolean == this.e) {}
    for (;;)
    {
      return;
      this.e = paramBoolean;
      int k = this.f.size();
      for (int m = 0; m < k; m++) {
        ((android.support.v7.app.c)this.f.get(m)).a(paramBoolean);
      }
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/a/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */