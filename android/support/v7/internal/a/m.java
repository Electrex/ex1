package android.support.v7.internal.a;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.d.a;
import android.support.v7.d.b;
import android.support.v7.internal.view.f;
import android.support.v7.internal.view.menu.j;
import android.support.v7.internal.widget.ActionBarContextView;
import android.support.v7.internal.widget.ActionBarOverlayLayout;
import android.support.v7.internal.widget.ag;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.lang.ref.WeakReference;

public class m
  extends a
  implements j
{
  private final Context b;
  private final android.support.v7.internal.view.menu.i c;
  private b d;
  private WeakReference e;
  
  public m(i parami, Context paramContext, b paramb)
  {
    this.b = paramContext;
    this.d = paramb;
    this.c = new android.support.v7.internal.view.menu.i(paramContext).a(1);
    this.c.a(this);
  }
  
  public MenuInflater a()
  {
    return new f(this.b);
  }
  
  public void a(int paramInt)
  {
    b(i.k(this.a).getResources().getString(paramInt));
  }
  
  public void a(android.support.v7.internal.view.menu.i parami)
  {
    if (this.d == null) {
      return;
    }
    d();
    i.i(this.a).a();
  }
  
  public void a(View paramView)
  {
    i.i(this.a).setCustomView(paramView);
    this.e = new WeakReference(paramView);
  }
  
  public void a(CharSequence paramCharSequence)
  {
    i.i(this.a).setSubtitle(paramCharSequence);
  }
  
  public void a(boolean paramBoolean)
  {
    super.a(paramBoolean);
    i.i(this.a).setTitleOptional(paramBoolean);
  }
  
  public boolean a(android.support.v7.internal.view.menu.i parami, MenuItem paramMenuItem)
  {
    if (this.d != null) {
      return this.d.a(this, paramMenuItem);
    }
    return false;
  }
  
  public Menu b()
  {
    return this.c;
  }
  
  public void b(int paramInt)
  {
    a(i.k(this.a).getResources().getString(paramInt));
  }
  
  public void b(CharSequence paramCharSequence)
  {
    i.i(this.a).setTitle(paramCharSequence);
  }
  
  public void c()
  {
    if (this.a.a != this) {
      return;
    }
    if (!i.a(i.g(this.a), i.h(this.a), false))
    {
      this.a.b = this;
      this.a.c = this.d;
    }
    for (;;)
    {
      this.d = null;
      this.a.j(false);
      i.i(this.a).b();
      i.j(this.a).a().sendAccessibilityEvent(32);
      i.f(this.a).setHideOnContentScrollEnabled(this.a.d);
      this.a.a = null;
      return;
      this.d.a(this);
    }
  }
  
  public void d()
  {
    if (this.a.a != this) {
      return;
    }
    this.c.g();
    try
    {
      this.d.b(this, this.c);
      return;
    }
    finally
    {
      this.c.h();
    }
  }
  
  public boolean e()
  {
    this.c.g();
    try
    {
      boolean bool = this.d.a(this, this.c);
      return bool;
    }
    finally
    {
      this.c.h();
    }
  }
  
  public CharSequence f()
  {
    return i.i(this.a).getTitle();
  }
  
  public CharSequence g()
  {
    return i.i(this.a).getSubtitle();
  }
  
  public boolean h()
  {
    return i.i(this.a).d();
  }
  
  public View i()
  {
    if (this.e != null) {
      return (View)this.e.get();
    }
    return null;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/a/m.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */