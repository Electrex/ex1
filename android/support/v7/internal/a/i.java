package android.support.v7.internal.a;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.view.bv;
import android.support.v4.view.ea;
import android.support.v4.view.eq;
import android.support.v4.view.es;
import android.support.v7.a.g;
import android.support.v7.app.c;
import android.support.v7.internal.widget.ActionBarContainer;
import android.support.v7.internal.widget.ActionBarContextView;
import android.support.v7.internal.widget.ActionBarOverlayLayout;
import android.support.v7.internal.widget.ag;
import android.support.v7.internal.widget.ao;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import java.util.ArrayList;

public class i
  extends android.support.v7.app.a
  implements android.support.v7.internal.widget.l
{
  private static final boolean i;
  private boolean A;
  private int B = 0;
  private boolean C = true;
  private boolean D;
  private boolean E;
  private boolean F;
  private boolean G = true;
  private android.support.v7.internal.view.i H;
  private boolean I;
  m a;
  android.support.v7.d.a b;
  android.support.v7.d.b c;
  boolean d;
  final eq e = new j(this);
  final eq f = new k(this);
  final es g = new l(this);
  private Context j;
  private Context k;
  private Activity l;
  private Dialog m;
  private ActionBarOverlayLayout n;
  private ActionBarContainer o;
  private ag p;
  private ActionBarContextView q;
  private ActionBarContainer r;
  private View s;
  private ao t;
  private ArrayList u = new ArrayList();
  private int v = -1;
  private boolean w;
  private boolean x;
  private ArrayList y = new ArrayList();
  private int z;
  
  static
  {
    boolean bool1 = true;
    boolean bool2;
    if (!i.class.desiredAssertionStatus())
    {
      bool2 = bool1;
      h = bool2;
      if (Build.VERSION.SDK_INT < 14) {
        break label34;
      }
    }
    for (;;)
    {
      i = bool1;
      return;
      bool2 = false;
      break;
      label34:
      bool1 = false;
    }
  }
  
  public i(Activity paramActivity, boolean paramBoolean)
  {
    this.l = paramActivity;
    View localView = paramActivity.getWindow().getDecorView();
    a(localView);
    if (!paramBoolean) {
      this.s = localView.findViewById(16908290);
    }
  }
  
  public i(Dialog paramDialog)
  {
    this.m = paramDialog;
    a(paramDialog.getWindow().getDecorView());
  }
  
  private void a(View paramView)
  {
    this.n = ((ActionBarOverlayLayout)paramView.findViewById(g.decor_content_parent));
    if (this.n != null) {
      this.n.setActionBarVisibilityCallback(this);
    }
    this.p = b(paramView.findViewById(g.action_bar));
    this.q = ((ActionBarContextView)paramView.findViewById(g.action_context_bar));
    this.o = ((ActionBarContainer)paramView.findViewById(g.action_bar_container));
    this.r = ((ActionBarContainer)paramView.findViewById(g.split_action_bar));
    if ((this.p == null) || (this.q == null) || (this.o == null)) {
      throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with a compatible window decor layout");
    }
    this.j = this.p.b();
    int i1;
    int i2;
    label193:
    android.support.v7.internal.view.a locala;
    if (this.p.c())
    {
      i1 = 1;
      this.z = i1;
      if ((0x4 & this.p.p()) == 0) {
        break label309;
      }
      i2 = 1;
      if (i2 != 0) {
        this.w = true;
      }
      locala = android.support.v7.internal.view.a.a(this.j);
      if ((!locala.f()) && (i2 == 0)) {
        break label314;
      }
    }
    label309:
    label314:
    for (boolean bool = true;; bool = false)
    {
      b(bool);
      k(locala.d());
      TypedArray localTypedArray = this.j.obtainStyledAttributes(null, android.support.v7.a.l.ActionBar, android.support.v7.a.b.actionBarStyle, 0);
      if (localTypedArray.getBoolean(android.support.v7.a.l.ActionBar_hideOnContentScroll, false)) {
        c(true);
      }
      int i3 = localTypedArray.getDimensionPixelSize(android.support.v7.a.l.ActionBar_elevation, 0);
      if (i3 != 0) {
        a(i3);
      }
      localTypedArray.recycle();
      return;
      i1 = 0;
      break;
      i2 = 0;
      break label193;
    }
  }
  
  private ag b(View paramView)
  {
    if ((paramView instanceof ag)) {
      return (ag)paramView;
    }
    if ((paramView instanceof Toolbar)) {
      return ((Toolbar)paramView).getWrapper();
    }
    throw new IllegalStateException("Can't make a decor toolbar out of " + paramView.getClass().getSimpleName());
  }
  
  private static boolean b(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    if (paramBoolean3) {}
    while ((!paramBoolean1) && (!paramBoolean2)) {
      return true;
    }
    return false;
  }
  
  private void k()
  {
    if (!this.F)
    {
      this.F = true;
      if (this.n != null) {
        this.n.setShowingForActionMode(true);
      }
      l(false);
    }
  }
  
  private void k(boolean paramBoolean)
  {
    boolean bool1 = true;
    this.A = paramBoolean;
    boolean bool2;
    label45:
    label78:
    boolean bool3;
    label98:
    ActionBarOverlayLayout localActionBarOverlayLayout;
    if (!this.A)
    {
      this.p.a(null);
      this.o.setTabContainer(this.t);
      if (f() != 2) {
        break label155;
      }
      bool2 = bool1;
      if (this.t != null)
      {
        if (!bool2) {
          break label160;
        }
        this.t.setVisibility(0);
        if (this.n != null) {
          bv.s(this.n);
        }
      }
      ag localag = this.p;
      if ((this.A) || (!bool2)) {
        break label172;
      }
      bool3 = bool1;
      localag.a(bool3);
      localActionBarOverlayLayout = this.n;
      if ((this.A) || (!bool2)) {
        break label178;
      }
    }
    for (;;)
    {
      localActionBarOverlayLayout.setHasNonEmbeddedTabs(bool1);
      return;
      this.o.setTabContainer(null);
      this.p.a(this.t);
      break;
      label155:
      bool2 = false;
      break label45;
      label160:
      this.t.setVisibility(8);
      break label78;
      label172:
      bool3 = false;
      break label98;
      label178:
      bool1 = false;
    }
  }
  
  private void l()
  {
    if (this.F)
    {
      this.F = false;
      if (this.n != null) {
        this.n.setShowingForActionMode(false);
      }
      l(false);
    }
  }
  
  private void l(boolean paramBoolean)
  {
    if (b(this.D, this.E, this.F)) {
      if (!this.G)
      {
        this.G = true;
        h(paramBoolean);
      }
    }
    while (!this.G) {
      return;
    }
    this.G = false;
    i(paramBoolean);
  }
  
  public int a()
  {
    return this.p.p();
  }
  
  public android.support.v7.d.a a(android.support.v7.d.b paramb)
  {
    if (this.a != null) {
      this.a.c();
    }
    this.n.setHideOnContentScrollEnabled(false);
    this.q.c();
    m localm = new m(this, this.q.getContext(), paramb);
    if (localm.e())
    {
      localm.d();
      this.q.a(localm);
      j(true);
      if ((this.r != null) && (this.z == 1) && (this.r.getVisibility() != 0))
      {
        this.r.setVisibility(0);
        if (this.n != null) {
          bv.s(this.n);
        }
      }
      this.q.sendAccessibilityEvent(32);
      this.a = localm;
      return localm;
    }
    return null;
  }
  
  public void a(float paramFloat)
  {
    bv.f(this.o, paramFloat);
    if (this.r != null) {
      bv.f(this.r, paramFloat);
    }
  }
  
  public void a(int paramInt)
  {
    this.p.e(paramInt);
  }
  
  public void a(int paramInt1, int paramInt2)
  {
    int i1 = this.p.p();
    if ((paramInt2 & 0x4) != 0) {
      this.w = true;
    }
    this.p.c(paramInt1 & paramInt2 | i1 & (paramInt2 ^ 0xFFFFFFFF));
  }
  
  public void a(Configuration paramConfiguration)
  {
    k(android.support.v7.internal.view.a.a(this.j).d());
  }
  
  public void a(Drawable paramDrawable)
  {
    this.p.b(paramDrawable);
  }
  
  public void a(CharSequence paramCharSequence)
  {
    this.p.b(paramCharSequence);
  }
  
  public void a(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (int i1 = 4;; i1 = 0)
    {
      a(i1, 4);
      return;
    }
  }
  
  public Context b()
  {
    int i1;
    if (this.k == null)
    {
      TypedValue localTypedValue = new TypedValue();
      this.j.getTheme().resolveAttribute(android.support.v7.a.b.actionBarWidgetTheme, localTypedValue, true);
      i1 = localTypedValue.resourceId;
      if (i1 == 0) {
        break label61;
      }
    }
    label61:
    for (this.k = new ContextThemeWrapper(this.j, i1);; this.k = this.j) {
      return this.k;
    }
  }
  
  public void b(int paramInt)
  {
    this.B = paramInt;
  }
  
  public void b(CharSequence paramCharSequence)
  {
    this.p.a(paramCharSequence);
  }
  
  public void b(boolean paramBoolean)
  {
    this.p.b(paramBoolean);
  }
  
  public void c(boolean paramBoolean)
  {
    if ((paramBoolean) && (!this.n.a())) {
      throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
    }
    this.d = paramBoolean;
    this.n.setHideOnContentScrollEnabled(paramBoolean);
  }
  
  public void d(boolean paramBoolean)
  {
    if (!this.w) {
      a(paramBoolean);
    }
  }
  
  public boolean d()
  {
    if ((this.p != null) && (this.p.d()))
    {
      this.p.e();
      return true;
    }
    return false;
  }
  
  void e()
  {
    if (this.c != null)
    {
      this.c.a(this.b);
      this.b = null;
      this.c = null;
    }
  }
  
  public void e(boolean paramBoolean)
  {
    this.I = paramBoolean;
    if ((!paramBoolean) && (this.H != null)) {
      this.H.b();
    }
  }
  
  public int f()
  {
    return this.p.q();
  }
  
  public void f(boolean paramBoolean)
  {
    if (paramBoolean == this.x) {}
    for (;;)
    {
      return;
      this.x = paramBoolean;
      int i1 = this.y.size();
      for (int i2 = 0; i2 < i1; i2++) {
        ((c)this.y.get(i2)).a(paramBoolean);
      }
    }
  }
  
  public void g()
  {
    if (this.E)
    {
      this.E = false;
      l(true);
    }
  }
  
  public void g(boolean paramBoolean)
  {
    this.C = paramBoolean;
  }
  
  public void h()
  {
    if (!this.E)
    {
      this.E = true;
      l(true);
    }
  }
  
  public void h(boolean paramBoolean)
  {
    if (this.H != null) {
      this.H.b();
    }
    this.o.setVisibility(0);
    if ((this.B == 0) && (i) && ((this.I) || (paramBoolean)))
    {
      bv.b(this.o, 0.0F);
      float f1 = -this.o.getHeight();
      if (paramBoolean)
      {
        int[] arrayOfInt = { 0, 0 };
        this.o.getLocationInWindow(arrayOfInt);
        f1 -= arrayOfInt[1];
      }
      bv.b(this.o, f1);
      android.support.v7.internal.view.i locali = new android.support.v7.internal.view.i();
      ea localea = bv.p(this.o).c(0.0F);
      localea.a(this.g);
      locali.a(localea);
      if ((this.C) && (this.s != null))
      {
        bv.b(this.s, f1);
        locali.a(bv.p(this.s).c(0.0F));
      }
      if ((this.r != null) && (this.z == 1))
      {
        bv.b(this.r, this.r.getHeight());
        this.r.setVisibility(0);
        locali.a(bv.p(this.r).c(0.0F));
      }
      locali.a(AnimationUtils.loadInterpolator(this.j, 17432582));
      locali.a(250L);
      locali.a(this.f);
      this.H = locali;
      locali.a();
    }
    for (;;)
    {
      if (this.n != null) {
        bv.s(this.n);
      }
      return;
      bv.c(this.o, 1.0F);
      bv.b(this.o, 0.0F);
      if ((this.C) && (this.s != null)) {
        bv.b(this.s, 0.0F);
      }
      if ((this.r != null) && (this.z == 1))
      {
        bv.c(this.r, 1.0F);
        bv.b(this.r, 0.0F);
        this.r.setVisibility(0);
      }
      this.f.b(null);
    }
  }
  
  public void i()
  {
    if (this.H != null)
    {
      this.H.b();
      this.H = null;
    }
  }
  
  public void i(boolean paramBoolean)
  {
    if (this.H != null) {
      this.H.b();
    }
    if ((this.B == 0) && (i) && ((this.I) || (paramBoolean)))
    {
      bv.c(this.o, 1.0F);
      this.o.setTransitioning(true);
      android.support.v7.internal.view.i locali = new android.support.v7.internal.view.i();
      float f1 = -this.o.getHeight();
      if (paramBoolean)
      {
        int[] arrayOfInt = { 0, 0 };
        this.o.getLocationInWindow(arrayOfInt);
        f1 -= arrayOfInt[1];
      }
      ea localea = bv.p(this.o).c(f1);
      localea.a(this.g);
      locali.a(localea);
      if ((this.C) && (this.s != null)) {
        locali.a(bv.p(this.s).c(f1));
      }
      if ((this.r != null) && (this.r.getVisibility() == 0))
      {
        bv.c(this.r, 1.0F);
        locali.a(bv.p(this.r).c(this.r.getHeight()));
      }
      locali.a(AnimationUtils.loadInterpolator(this.j, 17432581));
      locali.a(250L);
      locali.a(this.e);
      this.H = locali;
      locali.a();
      return;
    }
    this.e.b(null);
  }
  
  public void j() {}
  
  public void j(boolean paramBoolean)
  {
    int i1;
    label20:
    ActionBarContextView localActionBarContextView;
    int i2;
    if (paramBoolean)
    {
      k();
      ag localag = this.p;
      if (!paramBoolean) {
        break label55;
      }
      i1 = 8;
      localag.d(i1);
      localActionBarContextView = this.q;
      i2 = 0;
      if (!paramBoolean) {
        break label60;
      }
    }
    for (;;)
    {
      localActionBarContextView.a(i2);
      return;
      l();
      break;
      label55:
      i1 = 0;
      break label20;
      label60:
      i2 = 8;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/a/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */