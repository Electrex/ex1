package android.support.v7.internal.a;

import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.j;
import android.support.v7.internal.widget.ag;
import android.view.MenuItem;
import android.view.Window.Callback;

final class f
  implements j
{
  private f(b paramb) {}
  
  public void a(i parami)
  {
    if (b.a(this.a) != null)
    {
      if (!b.c(this.a).j()) {
        break label41;
      }
      b.a(this.a).onPanelClosed(8, parami);
    }
    label41:
    while (!b.a(this.a).onPreparePanel(0, null, parami)) {
      return;
    }
    b.a(this.a).onMenuOpened(8, parami);
  }
  
  public boolean a(i parami, MenuItem paramMenuItem)
  {
    return false;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/a/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */