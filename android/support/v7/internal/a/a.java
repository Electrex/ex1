package android.support.v7.internal.a;

import android.content.Context;
import android.support.v7.internal.widget.bn;
import android.support.v7.widget.aa;
import android.support.v7.widget.ab;
import android.support.v7.widget.s;
import android.support.v7.widget.t;
import android.support.v7.widget.u;
import android.support.v7.widget.v;
import android.support.v7.widget.w;
import android.support.v7.widget.x;
import android.support.v7.widget.y;
import android.support.v7.widget.z;
import android.util.AttributeSet;
import android.view.View;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

public class a
{
  static final Class[] a = { Context.class, AttributeSet.class };
  private static final Map b = new HashMap();
  private final Context c;
  private final Object[] d = new Object[2];
  
  public a(Context paramContext)
  {
    this.c = paramContext;
  }
  
  /* Error */
  private View a(Context paramContext, String paramString, AttributeSet paramAttributeSet)
  {
    // Byte code:
    //   0: aload_2
    //   1: ldc 40
    //   3: invokevirtual 46	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   6: ifeq +13 -> 19
    //   9: aload_3
    //   10: aconst_null
    //   11: ldc 48
    //   13: invokeinterface 52 3 0
    //   18: astore_2
    //   19: aload_0
    //   20: getfield 33	android/support/v7/internal/a/a:d	[Ljava/lang/Object;
    //   23: iconst_0
    //   24: aload_1
    //   25: aastore
    //   26: aload_0
    //   27: getfield 33	android/support/v7/internal/a/a:d	[Ljava/lang/Object;
    //   30: iconst_1
    //   31: aload_3
    //   32: aastore
    //   33: iconst_m1
    //   34: aload_2
    //   35: bipush 46
    //   37: invokevirtual 56	java/lang/String:indexOf	(I)I
    //   40: if_icmpne +29 -> 69
    //   43: aload_0
    //   44: aload_2
    //   45: ldc 58
    //   47: invokespecial 61	android/support/v7/internal/a/a:a	(Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
    //   50: astore 7
    //   52: aload_0
    //   53: getfield 33	android/support/v7/internal/a/a:d	[Ljava/lang/Object;
    //   56: iconst_0
    //   57: aconst_null
    //   58: aastore
    //   59: aload_0
    //   60: getfield 33	android/support/v7/internal/a/a:d	[Ljava/lang/Object;
    //   63: iconst_1
    //   64: aconst_null
    //   65: aastore
    //   66: aload 7
    //   68: areturn
    //   69: aload_0
    //   70: aload_2
    //   71: aconst_null
    //   72: invokespecial 61	android/support/v7/internal/a/a:a	(Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
    //   75: astore 6
    //   77: aload_0
    //   78: getfield 33	android/support/v7/internal/a/a:d	[Ljava/lang/Object;
    //   81: iconst_0
    //   82: aconst_null
    //   83: aastore
    //   84: aload_0
    //   85: getfield 33	android/support/v7/internal/a/a:d	[Ljava/lang/Object;
    //   88: iconst_1
    //   89: aconst_null
    //   90: aastore
    //   91: aload 6
    //   93: areturn
    //   94: astore 5
    //   96: aload_0
    //   97: getfield 33	android/support/v7/internal/a/a:d	[Ljava/lang/Object;
    //   100: iconst_0
    //   101: aconst_null
    //   102: aastore
    //   103: aload_0
    //   104: getfield 33	android/support/v7/internal/a/a:d	[Ljava/lang/Object;
    //   107: iconst_1
    //   108: aconst_null
    //   109: aastore
    //   110: aconst_null
    //   111: areturn
    //   112: astore 4
    //   114: aload_0
    //   115: getfield 33	android/support/v7/internal/a/a:d	[Ljava/lang/Object;
    //   118: iconst_0
    //   119: aconst_null
    //   120: aastore
    //   121: aload_0
    //   122: getfield 33	android/support/v7/internal/a/a:d	[Ljava/lang/Object;
    //   125: iconst_1
    //   126: aconst_null
    //   127: aastore
    //   128: aload 4
    //   130: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	131	0	this	a
    //   0	131	1	paramContext	Context
    //   0	131	2	paramString	String
    //   0	131	3	paramAttributeSet	AttributeSet
    //   112	17	4	localObject	Object
    //   94	1	5	localException	Exception
    //   75	17	6	localView1	View
    //   50	17	7	localView2	View
    // Exception table:
    //   from	to	target	type
    //   19	52	94	java/lang/Exception
    //   69	77	94	java/lang/Exception
    //   19	52	112	finally
    //   69	77	112	finally
  }
  
  private View a(String paramString1, String paramString2)
  {
    Constructor localConstructor = (Constructor)b.get(paramString1);
    if (localConstructor == null) {}
    try
    {
      ClassLoader localClassLoader = this.c.getClassLoader();
      if (paramString2 != null) {}
      for (String str = paramString2 + paramString1;; str = paramString1)
      {
        localConstructor = localClassLoader.loadClass(str).asSubclass(View.class).getConstructor(a);
        b.put(paramString1, localConstructor);
        localConstructor.setAccessible(true);
        View localView = (View)localConstructor.newInstance(this.d);
        return localView;
      }
      return null;
    }
    catch (Exception localException) {}
  }
  
  public final View a(View paramView, String paramString, Context paramContext, AttributeSet paramAttributeSet, boolean paramBoolean1, boolean paramBoolean2)
  {
    if ((paramBoolean1) && (paramView != null)) {}
    for (Context localContext = paramView.getContext();; localContext = paramContext)
    {
      if (paramBoolean2) {
        localContext = bn.a(localContext, paramAttributeSet, true, true);
      }
      int i = -1;
      switch (paramString.hashCode())
      {
      }
      for (;;)
      {
        switch (i)
        {
        default: 
          if (paramContext == localContext) {
            break label474;
          }
          return a(localContext, paramString, paramAttributeSet);
          if (paramString.equals("EditText"))
          {
            i = 0;
            continue;
            if (paramString.equals("Spinner"))
            {
              i = 1;
              continue;
              if (paramString.equals("CheckBox"))
              {
                i = 2;
                continue;
                if (paramString.equals("RadioButton"))
                {
                  i = 3;
                  continue;
                  if (paramString.equals("CheckedTextView"))
                  {
                    i = 4;
                    continue;
                    if (paramString.equals("AutoCompleteTextView"))
                    {
                      i = 5;
                      continue;
                      if (paramString.equals("MultiAutoCompleteTextView"))
                      {
                        i = 6;
                        continue;
                        if (paramString.equals("RatingBar"))
                        {
                          i = 7;
                          continue;
                          if (paramString.equals("Button"))
                          {
                            i = 8;
                            continue;
                            if (paramString.equals("TextView")) {
                              i = 9;
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          break;
        }
      }
      return new w(localContext, paramAttributeSet);
      return new aa(localContext, paramAttributeSet);
      return new u(localContext, paramAttributeSet);
      return new y(localContext, paramAttributeSet);
      return new v(localContext, paramAttributeSet);
      return new s(localContext, paramAttributeSet);
      return new x(localContext, paramAttributeSet);
      return new z(localContext, paramAttributeSet);
      return new t(localContext, paramAttributeSet);
      return new ab(localContext, paramAttributeSet);
      label474:
      return null;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */