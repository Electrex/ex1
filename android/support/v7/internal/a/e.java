package android.support.v7.internal.a;

import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.y;
import android.support.v7.internal.widget.ag;
import android.view.Window.Callback;

final class e
  implements y
{
  private boolean b;
  
  private e(b paramb) {}
  
  public void a(i parami, boolean paramBoolean)
  {
    if (this.b) {
      return;
    }
    this.b = true;
    b.c(this.a).o();
    if (b.a(this.a) != null) {
      b.a(this.a).onPanelClosed(8, parami);
    }
    this.b = false;
  }
  
  public boolean a(i parami)
  {
    if (b.a(this.a) != null)
    {
      b.a(this.a).onMenuOpened(8, parami);
      return true;
    }
    return false;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/a/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */