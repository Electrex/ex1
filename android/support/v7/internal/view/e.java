package android.support.v7.internal.view;

import android.content.Context;
import android.support.v4.f.p;
import android.support.v7.internal.view.menu.ab;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.Menu;
import android.view.MenuItem;

public class e
  implements android.support.v7.d.b
{
  final ActionMode.Callback a;
  final Context b;
  final p c;
  final p d;
  
  public e(Context paramContext, ActionMode.Callback paramCallback)
  {
    this.b = paramContext;
    this.a = paramCallback;
    this.c = new p();
    this.d = new p();
  }
  
  private Menu a(Menu paramMenu)
  {
    Menu localMenu = (Menu)this.d.get(paramMenu);
    if (localMenu == null)
    {
      localMenu = ab.a(this.b, (android.support.v4.c.a.a)paramMenu);
      this.d.put(paramMenu, localMenu);
    }
    return localMenu;
  }
  
  private ActionMode b(android.support.v7.d.a parama)
  {
    d locald1 = (d)this.c.get(parama);
    if (locald1 != null) {
      return locald1;
    }
    d locald2 = new d(this.b, parama);
    this.c.put(parama, locald2);
    return locald2;
  }
  
  public void a(android.support.v7.d.a parama)
  {
    this.a.onDestroyActionMode(b(parama));
  }
  
  public boolean a(android.support.v7.d.a parama, Menu paramMenu)
  {
    return this.a.onCreateActionMode(b(parama), a(paramMenu));
  }
  
  public boolean a(android.support.v7.d.a parama, MenuItem paramMenuItem)
  {
    return this.a.onActionItemClicked(b(parama), ab.a(this.b, (android.support.v4.c.a.b)paramMenuItem));
  }
  
  public boolean b(android.support.v7.d.a parama, Menu paramMenu)
  {
    return this.a.onPrepareActionMode(b(parama), a(paramMenu));
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/view/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */