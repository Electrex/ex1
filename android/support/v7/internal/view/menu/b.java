package android.support.v7.internal.view.menu;

import android.support.v7.widget.bf;
import android.support.v7.widget.bk;

class b
  extends bk
{
  public b(ActionMenuItemView paramActionMenuItemView)
  {
    super(paramActionMenuItemView);
  }
  
  public bf a()
  {
    if (ActionMenuItemView.a(this.a) != null) {
      return ActionMenuItemView.a(this.a).a();
    }
    return null;
  }
  
  protected boolean b()
  {
    k localk = ActionMenuItemView.b(this.a);
    boolean bool1 = false;
    if (localk != null)
    {
      boolean bool2 = ActionMenuItemView.b(this.a).a(ActionMenuItemView.c(this.a));
      bool1 = false;
      if (bool2)
      {
        bf localbf = a();
        bool1 = false;
        if (localbf != null)
        {
          boolean bool3 = localbf.b();
          bool1 = false;
          if (bool3) {
            bool1 = true;
          }
        }
      }
    }
    return bool1;
  }
  
  protected boolean c()
  {
    bf localbf = a();
    if (localbf != null)
    {
      localbf.a();
      return true;
    }
    return false;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/view/menu/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */