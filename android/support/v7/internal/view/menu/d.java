package android.support.v7.internal.view.menu;

import android.content.Context;
import android.support.v4.view.bv;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public abstract class d
  implements x
{
  protected Context a;
  protected Context b;
  protected i c;
  protected LayoutInflater d;
  protected LayoutInflater e;
  protected z f;
  private y g;
  private int h;
  private int i;
  private int j;
  
  public d(Context paramContext, int paramInt1, int paramInt2)
  {
    this.a = paramContext;
    this.d = LayoutInflater.from(paramContext);
    this.h = paramInt1;
    this.i = paramInt2;
  }
  
  public y a()
  {
    return this.g;
  }
  
  public z a(ViewGroup paramViewGroup)
  {
    if (this.f == null)
    {
      this.f = ((z)this.d.inflate(this.h, paramViewGroup, false));
      this.f.a(this.c);
      b(true);
    }
    return this.f;
  }
  
  public View a(m paramm, View paramView, ViewGroup paramViewGroup)
  {
    if ((paramView instanceof aa)) {}
    for (aa localaa = (aa)paramView;; localaa = b(paramViewGroup))
    {
      a(paramm, localaa);
      return (View)localaa;
    }
  }
  
  public void a(int paramInt)
  {
    this.j = paramInt;
  }
  
  public void a(Context paramContext, i parami)
  {
    this.b = paramContext;
    this.e = LayoutInflater.from(this.b);
    this.c = parami;
  }
  
  public void a(i parami, boolean paramBoolean)
  {
    if (this.g != null) {
      this.g.a(parami, paramBoolean);
    }
  }
  
  public abstract void a(m paramm, aa paramaa);
  
  public void a(y paramy)
  {
    this.g = paramy;
  }
  
  protected void a(View paramView, int paramInt)
  {
    ViewGroup localViewGroup = (ViewGroup)paramView.getParent();
    if (localViewGroup != null) {
      localViewGroup.removeView(paramView);
    }
    ((ViewGroup)this.f).addView(paramView, paramInt);
  }
  
  public boolean a(int paramInt, m paramm)
  {
    return true;
  }
  
  public boolean a(ad paramad)
  {
    if (this.g != null) {
      return this.g.a(paramad);
    }
    return false;
  }
  
  public boolean a(i parami, m paramm)
  {
    return false;
  }
  
  protected boolean a(ViewGroup paramViewGroup, int paramInt)
  {
    paramViewGroup.removeViewAt(paramInt);
    return true;
  }
  
  public aa b(ViewGroup paramViewGroup)
  {
    return (aa)this.d.inflate(this.i, paramViewGroup, false);
  }
  
  public void b(boolean paramBoolean)
  {
    ViewGroup localViewGroup = (ViewGroup)this.f;
    if (localViewGroup == null) {}
    label190:
    label199:
    for (;;)
    {
      return;
      int k;
      int i1;
      if (this.c != null)
      {
        this.c.j();
        ArrayList localArrayList = this.c.i();
        int m = localArrayList.size();
        int n = 0;
        k = 0;
        if (n < m)
        {
          m localm1 = (m)localArrayList.get(n);
          if (!a(k, localm1)) {
            break label190;
          }
          View localView1 = localViewGroup.getChildAt(k);
          if ((localView1 instanceof aa)) {}
          for (m localm2 = ((aa)localView1).getItemData();; localm2 = null)
          {
            View localView2 = a(localm1, localView1, localViewGroup);
            if (localm1 != localm2)
            {
              localView2.setPressed(false);
              bv.u(localView2);
            }
            if (localView2 != localView1) {
              a(localView2, k);
            }
            i1 = k + 1;
            n++;
            k = i1;
            break;
          }
        }
      }
      for (;;)
      {
        if (k >= localViewGroup.getChildCount()) {
          break label199;
        }
        if (!a(localViewGroup, k))
        {
          k++;
          continue;
          i1 = k;
          break;
          k = 0;
        }
      }
    }
  }
  
  public boolean b()
  {
    return false;
  }
  
  public boolean b(i parami, m paramm)
  {
    return false;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/view/menu/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */