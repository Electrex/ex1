package android.support.v7.internal.view.menu;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

class w
  extends BaseAdapter
{
  private i b;
  private int c = -1;
  
  public w(v paramv, i parami)
  {
    this.b = parami;
    a();
  }
  
  public m a(int paramInt)
  {
    if (v.a(this.a)) {}
    for (ArrayList localArrayList = this.b.l();; localArrayList = this.b.i())
    {
      if ((this.c >= 0) && (paramInt >= this.c)) {
        paramInt++;
      }
      return (m)localArrayList.get(paramInt);
    }
  }
  
  void a()
  {
    m localm = v.c(this.a).r();
    if (localm != null)
    {
      ArrayList localArrayList = v.c(this.a).l();
      int i = localArrayList.size();
      for (int j = 0; j < i; j++) {
        if ((m)localArrayList.get(j) == localm)
        {
          this.c = j;
          return;
        }
      }
    }
    this.c = -1;
  }
  
  public int getCount()
  {
    if (v.a(this.a)) {}
    for (ArrayList localArrayList = this.b.l(); this.c < 0; localArrayList = this.b.i()) {
      return localArrayList.size();
    }
    return -1 + localArrayList.size();
  }
  
  public long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramView == null) {}
    for (View localView = v.b(this.a).inflate(v.a, paramViewGroup, false);; localView = paramView)
    {
      aa localaa = (aa)localView;
      if (this.a.b) {
        ((ListMenuItemView)localView).setForceShowIcon(true);
      }
      localaa.a(a(paramInt), 0);
      return localView;
    }
  }
  
  public void notifyDataSetChanged()
  {
    a();
    super.notifyDataSetChanged();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/view/menu/w.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */