package android.support.v7.internal.view.menu;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

class h
  extends BaseAdapter
{
  private int b = -1;
  
  public h(g paramg)
  {
    a();
  }
  
  public m a(int paramInt)
  {
    ArrayList localArrayList = this.a.c.l();
    int i = paramInt + g.a(this.a);
    if ((this.b >= 0) && (i >= this.b)) {
      i++;
    }
    return (m)localArrayList.get(i);
  }
  
  void a()
  {
    m localm = this.a.c.r();
    if (localm != null)
    {
      ArrayList localArrayList = this.a.c.l();
      int i = localArrayList.size();
      for (int j = 0; j < i; j++) {
        if ((m)localArrayList.get(j) == localm)
        {
          this.b = j;
          return;
        }
      }
    }
    this.b = -1;
  }
  
  public int getCount()
  {
    int i = this.a.c.l().size() - g.a(this.a);
    if (this.b < 0) {
      return i;
    }
    return i - 1;
  }
  
  public long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramView == null) {}
    for (View localView = this.a.b.inflate(this.a.f, paramViewGroup, false);; localView = paramView)
    {
      ((aa)localView).a(a(paramInt), 0);
      return localView;
    }
  }
  
  public void notifyDataSetChanged()
  {
    a();
    super.notifyDataSetChanged();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/view/menu/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */