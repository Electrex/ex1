package android.support.v7.internal.view.menu;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.v4.c.a.b;
import android.view.ActionProvider;

@TargetApi(16)
class t
  extends o
{
  t(Context paramContext, b paramb)
  {
    super(paramContext, paramb);
  }
  
  p a(ActionProvider paramActionProvider)
  {
    return new u(this, this.a, paramActionProvider);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/view/menu/t.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */