package android.support.v7.internal.view;

import android.support.v4.view.ea;
import android.support.v4.view.eq;
import android.support.v4.view.er;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Iterator;

public class i
{
  private final ArrayList a = new ArrayList();
  private long b = -1L;
  private Interpolator c;
  private eq d;
  private boolean e;
  private final er f = new j(this);
  
  private void c()
  {
    this.e = false;
  }
  
  public i a(long paramLong)
  {
    if (!this.e) {
      this.b = paramLong;
    }
    return this;
  }
  
  public i a(ea paramea)
  {
    if (!this.e) {
      this.a.add(paramea);
    }
    return this;
  }
  
  public i a(eq parameq)
  {
    if (!this.e) {
      this.d = parameq;
    }
    return this;
  }
  
  public i a(Interpolator paramInterpolator)
  {
    if (!this.e) {
      this.c = paramInterpolator;
    }
    return this;
  }
  
  public void a()
  {
    if (this.e) {
      return;
    }
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
    {
      ea localea = (ea)localIterator.next();
      if (this.b >= 0L) {
        localea.a(this.b);
      }
      if (this.c != null) {
        localea.a(this.c);
      }
      if (this.d != null) {
        localea.a(this.f);
      }
      localea.b();
    }
    this.e = true;
  }
  
  public void b()
  {
    if (!this.e) {
      return;
    }
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext()) {
      ((ea)localIterator.next()).a();
    }
    this.e = false;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/internal/view/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */