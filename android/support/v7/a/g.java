package android.support.v7.a;

public final class g
{
  public static final int action_bar = 2131492935;
  public static final int action_bar_activity_content = 2131492864;
  public static final int action_bar_container = 2131492934;
  public static final int action_bar_root = 2131492930;
  public static final int action_bar_spinner = 2131492865;
  public static final int action_bar_subtitle = 2131492907;
  public static final int action_bar_title = 2131492906;
  public static final int action_context_bar = 2131492936;
  public static final int action_menu_divider = 2131492866;
  public static final int action_menu_presenter = 2131492867;
  public static final int action_mode_bar = 2131492932;
  public static final int action_mode_bar_stub = 2131492931;
  public static final int action_mode_close_button = 2131492908;
  public static final int activity_chooser_view_content = 2131492909;
  public static final int alertTitle = 2131492919;
  public static final int always = 2131492893;
  public static final int beginning = 2131492890;
  public static final int buttonPanel = 2131492925;
  public static final int checkbox = 2131492927;
  public static final int collapseActionView = 2131492894;
  public static final int contentPanel = 2131492920;
  public static final int custom = 2131492924;
  public static final int customPanel = 2131492923;
  public static final int decor_content_parent = 2131492933;
  public static final int default_activity_button = 2131492912;
  public static final int dialog = 2131492898;
  public static final int disableHome = 2131492879;
  public static final int dropdown = 2131492899;
  public static final int edit_query = 2131492937;
  public static final int end = 2131492891;
  public static final int expand_activities_button = 2131492910;
  public static final int expanded_menu = 2131492926;
  public static final int home = 2131492870;
  public static final int homeAsUp = 2131492880;
  public static final int icon = 2131492914;
  public static final int ifRoom = 2131492895;
  public static final int image = 2131492911;
  public static final int listMode = 2131492876;
  public static final int list_item = 2131492913;
  public static final int middle = 2131492892;
  public static final int multiply = 2131492901;
  public static final int never = 2131492896;
  public static final int none = 2131492881;
  public static final int normal = 2131492877;
  public static final int parentPanel = 2131492916;
  public static final int progress_circular = 2131492871;
  public static final int progress_horizontal = 2131492872;
  public static final int radio = 2131492929;
  public static final int screen = 2131492902;
  public static final int scrollView = 2131492921;
  public static final int search_badge = 2131492939;
  public static final int search_bar = 2131492938;
  public static final int search_button = 2131492940;
  public static final int search_close_btn = 2131492945;
  public static final int search_edit_frame = 2131492941;
  public static final int search_go_btn = 2131492947;
  public static final int search_mag_icon = 2131492942;
  public static final int search_plate = 2131492943;
  public static final int search_src_text = 2131492944;
  public static final int search_voice_btn = 2131492948;
  public static final int select_dialog_listview = 2131492949;
  public static final int shortcut = 2131492928;
  public static final int showCustom = 2131492882;
  public static final int showHome = 2131492883;
  public static final int showTitle = 2131492884;
  public static final int split_action_bar = 2131492874;
  public static final int src_atop = 2131492903;
  public static final int src_in = 2131492904;
  public static final int src_over = 2131492905;
  public static final int submit_area = 2131492946;
  public static final int tabMode = 2131492878;
  public static final int textSpacerNoButtons = 2131492922;
  public static final int title = 2131492915;
  public static final int title_template = 2131492918;
  public static final int topPanel = 2131492917;
  public static final int up = 2131492875;
  public static final int useLogo = 2131492885;
  public static final int withText = 2131492897;
  public static final int wrap_content = 2131492900;
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/a/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */