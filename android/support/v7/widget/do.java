package android.support.v7.widget;

import android.view.View;
import java.util.ArrayList;

class do
{
  int a;
  int b;
  int c;
  final int d;
  private ArrayList f;
  
  int a(int paramInt)
  {
    if (this.a != Integer.MIN_VALUE) {
      paramInt = this.a;
    }
    while (this.f.size() == 0) {
      return paramInt;
    }
    a();
    return this.a;
  }
  
  void a()
  {
    View localView = (View)this.f.get(0);
    dl localdl = c(localView);
    this.a = this.e.a.a(localView);
    if (localdl.f)
    {
      StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem = this.e.f.f(localdl.c());
      if ((localFullSpanItem != null) && (localFullSpanItem.b == -1)) {
        this.a -= localFullSpanItem.a(this.d);
      }
    }
  }
  
  void a(View paramView)
  {
    dl localdl = c(paramView);
    localdl.e = this;
    this.f.add(0, paramView);
    this.a = Integer.MIN_VALUE;
    if (this.f.size() == 1) {
      this.b = Integer.MIN_VALUE;
    }
    if ((localdl.a()) || (localdl.b())) {
      this.c += this.e.a.c(paramView);
    }
  }
  
  void a(boolean paramBoolean, int paramInt)
  {
    int i;
    if (paramBoolean)
    {
      i = b(Integer.MIN_VALUE);
      e();
      if (i != Integer.MIN_VALUE) {
        break label32;
      }
    }
    label32:
    while (((paramBoolean) && (i < this.e.a.d())) || ((!paramBoolean) && (i > this.e.a.c())))
    {
      return;
      i = a(Integer.MIN_VALUE);
      break;
    }
    if (paramInt != Integer.MIN_VALUE) {
      i += paramInt;
    }
    this.b = i;
    this.a = i;
  }
  
  int b()
  {
    if (this.a != Integer.MIN_VALUE) {
      return this.a;
    }
    a();
    return this.a;
  }
  
  int b(int paramInt)
  {
    if (this.b != Integer.MIN_VALUE) {
      paramInt = this.b;
    }
    while (this.f.size() == 0) {
      return paramInt;
    }
    c();
    return this.b;
  }
  
  void b(View paramView)
  {
    dl localdl = c(paramView);
    localdl.e = this;
    this.f.add(paramView);
    this.b = Integer.MIN_VALUE;
    if (this.f.size() == 1) {
      this.a = Integer.MIN_VALUE;
    }
    if ((localdl.a()) || (localdl.b())) {
      this.c += this.e.a.c(paramView);
    }
  }
  
  dl c(View paramView)
  {
    return (dl)paramView.getLayoutParams();
  }
  
  void c()
  {
    View localView = (View)this.f.get(-1 + this.f.size());
    dl localdl = c(localView);
    this.b = this.e.a.b(localView);
    if (localdl.f)
    {
      StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem = this.e.f.f(localdl.c());
      if ((localFullSpanItem != null) && (localFullSpanItem.b == 1)) {
        this.b += localFullSpanItem.a(this.d);
      }
    }
  }
  
  void c(int paramInt)
  {
    this.a = paramInt;
    this.b = paramInt;
  }
  
  int d()
  {
    if (this.b != Integer.MIN_VALUE) {
      return this.b;
    }
    c();
    return this.b;
  }
  
  void d(int paramInt)
  {
    if (this.a != Integer.MIN_VALUE) {
      this.a = (paramInt + this.a);
    }
    if (this.b != Integer.MIN_VALUE) {
      this.b = (paramInt + this.b);
    }
  }
  
  void e()
  {
    this.f.clear();
    f();
    this.c = 0;
  }
  
  void f()
  {
    this.a = Integer.MIN_VALUE;
    this.b = Integer.MIN_VALUE;
  }
  
  void g()
  {
    int i = this.f.size();
    View localView = (View)this.f.remove(i - 1);
    dl localdl = c(localView);
    localdl.e = null;
    if ((localdl.a()) || (localdl.b())) {
      this.c -= this.e.a.c(localView);
    }
    if (i == 1) {
      this.a = Integer.MIN_VALUE;
    }
    this.b = Integer.MIN_VALUE;
  }
  
  void h()
  {
    View localView = (View)this.f.remove(0);
    dl localdl = c(localView);
    localdl.e = null;
    if (this.f.size() == 0) {
      this.b = Integer.MIN_VALUE;
    }
    if ((localdl.a()) || (localdl.b())) {
      this.c -= this.e.a.c(localView);
    }
    this.a = Integer.MIN_VALUE;
  }
  
  public int i()
  {
    return this.c;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/do.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */