package android.support.v7.widget;

import android.view.View;

public abstract class bu
{
  protected final cl a;
  private int b = Integer.MIN_VALUE;
  
  private bu(cl paramcl)
  {
    this.a = paramcl;
  }
  
  public static bu a(cl paramcl)
  {
    return new bv(paramcl);
  }
  
  public static bu a(cl paramcl, int paramInt)
  {
    switch (paramInt)
    {
    default: 
      throw new IllegalArgumentException("invalid orientation");
    case 0: 
      return a(paramcl);
    }
    return b(paramcl);
  }
  
  public static bu b(cl paramcl)
  {
    return new bw(paramcl);
  }
  
  public abstract int a(View paramView);
  
  public void a()
  {
    this.b = f();
  }
  
  public abstract void a(int paramInt);
  
  public int b()
  {
    if (Integer.MIN_VALUE == this.b) {
      return 0;
    }
    return f() - this.b;
  }
  
  public abstract int b(View paramView);
  
  public abstract int c();
  
  public abstract int c(View paramView);
  
  public abstract int d();
  
  public abstract int d(View paramView);
  
  public abstract int e();
  
  public abstract int f();
  
  public abstract int g();
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/bu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */