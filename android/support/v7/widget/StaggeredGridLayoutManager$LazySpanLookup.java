package android.support.v7.widget;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class StaggeredGridLayoutManager$LazySpanLookup
{
  int[] a;
  List b;
  
  private void c(int paramInt1, int paramInt2)
  {
    if (this.b == null) {
      return;
    }
    int i = paramInt1 + paramInt2;
    int j = -1 + this.b.size();
    label25:
    StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem;
    if (j >= 0)
    {
      localFullSpanItem = (StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem)this.b.get(j);
      if (localFullSpanItem.a >= paramInt1) {
        break label61;
      }
    }
    for (;;)
    {
      j--;
      break label25;
      break;
      label61:
      if (localFullSpanItem.a < i) {
        this.b.remove(j);
      } else {
        localFullSpanItem.a -= paramInt2;
      }
    }
  }
  
  private void d(int paramInt1, int paramInt2)
  {
    if (this.b == null) {
      return;
    }
    int i = -1 + this.b.size();
    label20:
    StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem;
    if (i >= 0)
    {
      localFullSpanItem = (StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem)this.b.get(i);
      if (localFullSpanItem.a >= paramInt1) {
        break label54;
      }
    }
    for (;;)
    {
      i--;
      break label20;
      break;
      label54:
      localFullSpanItem.a = (paramInt2 + localFullSpanItem.a);
    }
  }
  
  private int g(int paramInt)
  {
    if (this.b == null) {
      return -1;
    }
    StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem1 = f(paramInt);
    if (localFullSpanItem1 != null) {
      this.b.remove(localFullSpanItem1);
    }
    int i = this.b.size();
    int j = 0;
    if (j < i) {
      if (((StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem)this.b.get(j)).a < paramInt) {}
    }
    for (;;)
    {
      if (j != -1)
      {
        StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem2 = (StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem)this.b.get(j);
        this.b.remove(j);
        return localFullSpanItem2.a;
        j++;
        break;
      }
      return -1;
      j = -1;
    }
  }
  
  int a(int paramInt)
  {
    if (this.b != null) {
      for (int i = -1 + this.b.size(); i >= 0; i--) {
        if (((StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem)this.b.get(i)).a >= paramInt) {
          this.b.remove(i);
        }
      }
    }
    return b(paramInt);
  }
  
  public StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem a(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
  {
    StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem;
    if (this.b == null)
    {
      localFullSpanItem = null;
      return localFullSpanItem;
    }
    int i = this.b.size();
    for (int j = 0;; j++)
    {
      if (j >= i) {
        break label102;
      }
      localFullSpanItem = (StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem)this.b.get(j);
      if (localFullSpanItem.a >= paramInt2) {
        return null;
      }
      if ((localFullSpanItem.a >= paramInt1) && ((paramInt3 == 0) || (localFullSpanItem.b == paramInt3) || ((paramBoolean) && (localFullSpanItem.d)))) {
        break;
      }
    }
    label102:
    return null;
  }
  
  void a()
  {
    if (this.a != null) {
      Arrays.fill(this.a, -1);
    }
    this.b = null;
  }
  
  void a(int paramInt1, int paramInt2)
  {
    if ((this.a == null) || (paramInt1 >= this.a.length)) {
      return;
    }
    e(paramInt1 + paramInt2);
    System.arraycopy(this.a, paramInt1 + paramInt2, this.a, paramInt1, this.a.length - paramInt1 - paramInt2);
    Arrays.fill(this.a, this.a.length - paramInt2, this.a.length, -1);
    c(paramInt1, paramInt2);
  }
  
  void a(int paramInt, do paramdo)
  {
    e(paramInt);
    this.a[paramInt] = paramdo.d;
  }
  
  public void a(StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem paramFullSpanItem)
  {
    if (this.b == null) {
      this.b = new ArrayList();
    }
    int i = this.b.size();
    for (int j = 0; j < i; j++)
    {
      StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem = (StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem)this.b.get(j);
      if (localFullSpanItem.a == paramFullSpanItem.a) {
        this.b.remove(j);
      }
      if (localFullSpanItem.a >= paramFullSpanItem.a)
      {
        this.b.add(j, paramFullSpanItem);
        return;
      }
    }
    this.b.add(paramFullSpanItem);
  }
  
  int b(int paramInt)
  {
    if (this.a == null) {}
    while (paramInt >= this.a.length) {
      return -1;
    }
    int i = g(paramInt);
    if (i == -1)
    {
      Arrays.fill(this.a, paramInt, this.a.length, -1);
      return this.a.length;
    }
    Arrays.fill(this.a, paramInt, i + 1, -1);
    return i + 1;
  }
  
  void b(int paramInt1, int paramInt2)
  {
    if ((this.a == null) || (paramInt1 >= this.a.length)) {
      return;
    }
    e(paramInt1 + paramInt2);
    System.arraycopy(this.a, paramInt1, this.a, paramInt1 + paramInt2, this.a.length - paramInt1 - paramInt2);
    Arrays.fill(this.a, paramInt1, paramInt1 + paramInt2, -1);
    d(paramInt1, paramInt2);
  }
  
  int c(int paramInt)
  {
    if ((this.a == null) || (paramInt >= this.a.length)) {
      return -1;
    }
    return this.a[paramInt];
  }
  
  int d(int paramInt)
  {
    int i = this.a.length;
    while (i <= paramInt) {
      i *= 2;
    }
    return i;
  }
  
  void e(int paramInt)
  {
    if (this.a == null)
    {
      this.a = new int[1 + Math.max(paramInt, 10)];
      Arrays.fill(this.a, -1);
    }
    while (paramInt < this.a.length) {
      return;
    }
    int[] arrayOfInt = this.a;
    this.a = new int[d(paramInt)];
    System.arraycopy(arrayOfInt, 0, this.a, 0, arrayOfInt.length);
    Arrays.fill(this.a, arrayOfInt.length, this.a.length, -1);
  }
  
  public StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem f(int paramInt)
  {
    StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem;
    if (this.b == null)
    {
      localFullSpanItem = null;
      return localFullSpanItem;
    }
    for (int i = -1 + this.b.size();; i--)
    {
      if (i < 0) {
        break label55;
      }
      localFullSpanItem = (StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem)this.b.get(i);
      if (localFullSpanItem.a == paramInt) {
        break;
      }
    }
    label55:
    return null;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */