package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff.Mode;
import android.os.Build.VERSION;
import android.support.v7.a.b;
import android.support.v7.a.l;
import android.support.v7.internal.b.a;
import android.support.v7.internal.widget.bb;
import android.support.v7.internal.widget.be;
import android.support.v7.internal.widget.bf;
import android.support.v7.internal.widget.bh;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Button;

public class t
  extends Button
{
  private static final int[] a = { 16842964 };
  private be b;
  
  public t(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, b.buttonStyle);
  }
  
  public t(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    if (bf.a)
    {
      bh localbh = bh.a(getContext(), paramAttributeSet, a, paramInt, 0);
      if (localbh.e(0))
      {
        ColorStateList localColorStateList2 = localbh.c().c(localbh.f(0, -1));
        if (localColorStateList2 != null) {
          setSupportBackgroundTintList(localColorStateList2);
        }
      }
      localbh.b();
    }
    TypedArray localTypedArray1 = paramContext.obtainStyledAttributes(paramAttributeSet, l.AppCompatTextView, paramInt, 0);
    int i = localTypedArray1.getResourceId(l.AppCompatTextView_android_textAppearance, -1);
    localTypedArray1.recycle();
    if (i != -1)
    {
      TypedArray localTypedArray3 = paramContext.obtainStyledAttributes(i, l.TextAppearance);
      if (localTypedArray3.hasValue(l.TextAppearance_textAllCaps)) {
        setAllCaps(localTypedArray3.getBoolean(l.TextAppearance_textAllCaps, false));
      }
      localTypedArray3.recycle();
    }
    TypedArray localTypedArray2 = paramContext.obtainStyledAttributes(paramAttributeSet, l.AppCompatTextView, paramInt, 0);
    if (localTypedArray2.hasValue(l.AppCompatTextView_textAllCaps)) {
      setAllCaps(localTypedArray2.getBoolean(l.AppCompatTextView_textAllCaps, false));
    }
    localTypedArray2.recycle();
    ColorStateList localColorStateList1 = getTextColors();
    if ((localColorStateList1 != null) && (!localColorStateList1.isStateful())) {
      if (Build.VERSION.SDK_INT >= 21) {
        break label235;
      }
    }
    label235:
    for (int j = bb.c(paramContext, 16842808);; j = bb.a(paramContext, 16842808))
    {
      setTextColor(bb.a(localColorStateList1.getDefaultColor(), j));
      return;
    }
  }
  
  private void a()
  {
    if ((getBackground() != null) && (this.b != null)) {
      bf.a(this, this.b);
    }
  }
  
  protected void drawableStateChanged()
  {
    super.drawableStateChanged();
    a();
  }
  
  public ColorStateList getSupportBackgroundTintList()
  {
    if (this.b != null) {
      return this.b.a;
    }
    return null;
  }
  
  public PorterDuff.Mode getSupportBackgroundTintMode()
  {
    if (this.b != null) {
      return this.b.b;
    }
    return null;
  }
  
  public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
  {
    super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
    paramAccessibilityEvent.setClassName(Button.class.getName());
  }
  
  public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
  {
    super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
    paramAccessibilityNodeInfo.setClassName(Button.class.getName());
  }
  
  public void setAllCaps(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (a locala = new a(getContext());; locala = null)
    {
      setTransformationMethod(locala);
      return;
    }
  }
  
  public void setSupportBackgroundTintList(ColorStateList paramColorStateList)
  {
    if (this.b == null) {
      this.b = new be();
    }
    this.b.a = paramColorStateList;
    this.b.d = true;
    a();
  }
  
  public void setSupportBackgroundTintMode(PorterDuff.Mode paramMode)
  {
    if (this.b == null) {
      this.b = new be();
    }
    this.b.b = paramMode;
    this.b.c = true;
    a();
  }
  
  public void setTextAppearance(Context paramContext, int paramInt)
  {
    super.setTextAppearance(paramContext, paramInt);
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramInt, l.TextAppearance);
    if (localTypedArray.hasValue(l.TextAppearance_textAllCaps)) {
      setAllCaps(localTypedArray.getBoolean(l.TextAppearance_textAllCaps, false));
    }
    localTypedArray.recycle();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/t.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */