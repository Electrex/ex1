package android.support.v7.widget;

class ak
{
  long a = 0L;
  ak b;
  
  private void b()
  {
    if (this.b == null) {
      this.b = new ak();
    }
  }
  
  void a()
  {
    this.a = 0L;
    if (this.b != null) {
      this.b.a();
    }
  }
  
  void a(int paramInt)
  {
    if (paramInt >= 64)
    {
      b();
      this.b.a(paramInt - 64);
      return;
    }
    this.a |= 1L << paramInt;
  }
  
  void a(int paramInt, boolean paramBoolean)
  {
    if (paramInt >= 64)
    {
      b();
      this.b.a(paramInt - 64, paramBoolean);
    }
    label109:
    label115:
    for (;;)
    {
      return;
      boolean bool;
      if ((0x8000000000000000 & this.a) != 0L)
      {
        bool = true;
        long l = (1L << paramInt) - 1L;
        this.a = (l & this.a | (this.a & (l ^ 0xFFFFFFFFFFFFFFFF)) << 1);
        if (!paramBoolean) {
          break label109;
        }
        a(paramInt);
      }
      for (;;)
      {
        if ((!bool) && (this.b == null)) {
          break label115;
        }
        b();
        this.b.a(0, bool);
        return;
        bool = false;
        break;
        b(paramInt);
      }
    }
  }
  
  void b(int paramInt)
  {
    if (paramInt >= 64)
    {
      if (this.b != null) {
        this.b.b(paramInt - 64);
      }
      return;
    }
    this.a &= (0xFFFFFFFFFFFFFFFF ^ 1L << paramInt);
  }
  
  boolean c(int paramInt)
  {
    if (paramInt >= 64)
    {
      b();
      return this.b.c(paramInt - 64);
    }
    return (this.a & 1L << paramInt) != 0L;
  }
  
  boolean d(int paramInt)
  {
    if (paramInt >= 64)
    {
      b();
      bool = this.b.d(paramInt - 64);
      return bool;
    }
    long l1 = 1L << paramInt;
    if ((l1 & this.a) != 0L) {}
    for (boolean bool = true;; bool = false)
    {
      this.a &= (l1 ^ 0xFFFFFFFFFFFFFFFF);
      long l2 = l1 - 1L;
      this.a = (l2 & this.a | Long.rotateRight(this.a & (l2 ^ 0xFFFFFFFFFFFFFFFF), 1));
      if (this.b == null) {
        break;
      }
      if (this.b.c(0)) {
        a(63);
      }
      this.b.d(0);
      return bool;
    }
  }
  
  int e(int paramInt)
  {
    if (this.b == null)
    {
      if (paramInt >= 64) {
        return Long.bitCount(this.a);
      }
      return Long.bitCount(this.a & (1L << paramInt) - 1L);
    }
    if (paramInt < 64) {
      return Long.bitCount(this.a & (1L << paramInt) - 1L);
    }
    return this.b.e(paramInt - 64) + Long.bitCount(this.a);
  }
  
  public String toString()
  {
    if (this.b == null) {
      return Long.toBinaryString(this.a);
    }
    return this.b.toString() + "xx" + Long.toBinaryString(this.a);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/ak.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */