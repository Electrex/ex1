package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.support.v7.b.d;
import android.support.v7.b.e;
import android.util.AttributeSet;
import android.view.View.MeasureSpec;
import android.widget.FrameLayout;

public class CardView
  extends FrameLayout
  implements ad
{
  private static final ag a;
  private boolean b;
  private boolean c;
  private final Rect d = new Rect();
  private final Rect e = new Rect();
  
  static
  {
    if (Build.VERSION.SDK_INT >= 21) {
      a = new ac();
    }
    for (;;)
    {
      a.a();
      return;
      if (Build.VERSION.SDK_INT >= 17) {
        a = new ah();
      } else {
        a = new ae();
      }
    }
  }
  
  public CardView(Context paramContext)
  {
    super(paramContext);
    a(paramContext, null, 0);
  }
  
  public CardView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a(paramContext, paramAttributeSet, 0);
  }
  
  public CardView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    a(paramContext, paramAttributeSet, paramInt);
  }
  
  private void a(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, e.CardView, paramInt, d.CardView_Light);
    int i = localTypedArray.getColor(e.CardView_cardBackgroundColor, 0);
    float f1 = localTypedArray.getDimension(e.CardView_cardCornerRadius, 0.0F);
    float f2 = localTypedArray.getDimension(e.CardView_cardElevation, 0.0F);
    float f3 = localTypedArray.getDimension(e.CardView_cardMaxElevation, 0.0F);
    this.b = localTypedArray.getBoolean(e.CardView_cardUseCompatPadding, false);
    this.c = localTypedArray.getBoolean(e.CardView_cardPreventCornerOverlap, true);
    int j = localTypedArray.getDimensionPixelSize(e.CardView_contentPadding, 0);
    this.d.left = localTypedArray.getDimensionPixelSize(e.CardView_contentPaddingLeft, j);
    this.d.top = localTypedArray.getDimensionPixelSize(e.CardView_contentPaddingTop, j);
    this.d.right = localTypedArray.getDimensionPixelSize(e.CardView_contentPaddingRight, j);
    this.d.bottom = localTypedArray.getDimensionPixelSize(e.CardView_contentPaddingBottom, j);
    if (f2 > f3) {
      f3 = f2;
    }
    localTypedArray.recycle();
    a.a(this, paramContext, i, f1, f2, f3);
  }
  
  public void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.e.set(paramInt1, paramInt2, paramInt3, paramInt4);
    super.setPadding(paramInt1 + this.d.left, paramInt2 + this.d.top, paramInt3 + this.d.right, paramInt4 + this.d.bottom);
  }
  
  public float getCardElevation()
  {
    return a.e(this);
  }
  
  public int getContentPaddingBottom()
  {
    return this.d.bottom;
  }
  
  public int getContentPaddingLeft()
  {
    return this.d.left;
  }
  
  public int getContentPaddingRight()
  {
    return this.d.right;
  }
  
  public int getContentPaddingTop()
  {
    return this.d.top;
  }
  
  public float getMaxCardElevation()
  {
    return a.a(this);
  }
  
  public boolean getPreventCornerOverlap()
  {
    return this.c;
  }
  
  public float getRadius()
  {
    return a.d(this);
  }
  
  public boolean getUseCompatPadding()
  {
    return this.b;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    if (!(a instanceof ac))
    {
      int i = View.MeasureSpec.getMode(paramInt1);
      int j;
      switch (i)
      {
      default: 
        j = View.MeasureSpec.getMode(paramInt2);
        switch (j)
        {
        }
        break;
      }
      for (;;)
      {
        super.onMeasure(paramInt1, paramInt2);
        return;
        paramInt1 = View.MeasureSpec.makeMeasureSpec(Math.max((int)Math.ceil(a.b(this)), View.MeasureSpec.getSize(paramInt1)), i);
        break;
        paramInt2 = View.MeasureSpec.makeMeasureSpec(Math.max((int)Math.ceil(a.c(this)), View.MeasureSpec.getSize(paramInt2)), j);
      }
    }
    super.onMeasure(paramInt1, paramInt2);
  }
  
  public void setCardBackgroundColor(int paramInt)
  {
    a.a(this, paramInt);
  }
  
  public void setCardElevation(float paramFloat)
  {
    a.c(this, paramFloat);
  }
  
  public void setMaxCardElevation(float paramFloat)
  {
    a.b(this, paramFloat);
  }
  
  public void setPadding(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {}
  
  public void setPaddingRelative(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {}
  
  public void setPreventCornerOverlap(boolean paramBoolean)
  {
    if (paramBoolean == this.c) {
      return;
    }
    this.c = paramBoolean;
    a.h(this);
  }
  
  public void setRadius(float paramFloat)
  {
    a.a(this, paramFloat);
  }
  
  public void setUseCompatPadding(boolean paramBoolean)
  {
    if (this.b == paramBoolean) {
      return;
    }
    this.b = paramBoolean;
    a.g(this);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/CardView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */