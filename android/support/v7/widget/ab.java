package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.a.l;
import android.support.v7.internal.b.a;
import android.util.AttributeSet;
import android.widget.TextView;

public class ab
  extends TextView
{
  public ab(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public ab(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public ab(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    TypedArray localTypedArray1 = paramContext.obtainStyledAttributes(paramAttributeSet, l.AppCompatTextView, paramInt, 0);
    int i = localTypedArray1.getResourceId(l.AppCompatTextView_android_textAppearance, -1);
    localTypedArray1.recycle();
    if (i != -1)
    {
      TypedArray localTypedArray3 = paramContext.obtainStyledAttributes(i, l.TextAppearance);
      if (localTypedArray3.hasValue(l.TextAppearance_textAllCaps)) {
        setAllCaps(localTypedArray3.getBoolean(l.TextAppearance_textAllCaps, false));
      }
      localTypedArray3.recycle();
    }
    TypedArray localTypedArray2 = paramContext.obtainStyledAttributes(paramAttributeSet, l.AppCompatTextView, paramInt, 0);
    if (localTypedArray2.hasValue(l.AppCompatTextView_textAllCaps)) {
      setAllCaps(localTypedArray2.getBoolean(l.AppCompatTextView_textAllCaps, false));
    }
    localTypedArray2.recycle();
  }
  
  public void setAllCaps(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (a locala = new a(getContext());; locala = null)
    {
      setTransformationMethod(locala);
      return;
    }
  }
  
  public void setTextAppearance(Context paramContext, int paramInt)
  {
    super.setTextAppearance(paramContext, paramInt);
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramInt, l.TextAppearance);
    if (localTypedArray.hasValue(l.TextAppearance_textAllCaps)) {
      setAllCaps(localTypedArray.getBoolean(l.TextAppearance_textAllCaps, false));
    }
    localTypedArray.recycle();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/ab.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */