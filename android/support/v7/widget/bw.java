package android.support.v7.widget;

import android.view.View;

final class bw
  extends bu
{
  bw(cl paramcl)
  {
    super(paramcl, null);
  }
  
  public int a(View paramView)
  {
    cm localcm = (cm)paramView.getLayoutParams();
    return this.a.h(paramView) - localcm.topMargin;
  }
  
  public void a(int paramInt)
  {
    this.a.g(paramInt);
  }
  
  public int b(View paramView)
  {
    cm localcm = (cm)paramView.getLayoutParams();
    return this.a.j(paramView) + localcm.bottomMargin;
  }
  
  public int c()
  {
    return this.a.t();
  }
  
  public int c(View paramView)
  {
    cm localcm = (cm)paramView.getLayoutParams();
    return this.a.f(paramView) + localcm.topMargin + localcm.bottomMargin;
  }
  
  public int d()
  {
    return this.a.r() - this.a.v();
  }
  
  public int d(View paramView)
  {
    cm localcm = (cm)paramView.getLayoutParams();
    return this.a.e(paramView) + localcm.leftMargin + localcm.rightMargin;
  }
  
  public int e()
  {
    return this.a.r();
  }
  
  public int f()
  {
    return this.a.r() - this.a.t() - this.a.v();
  }
  
  public int g()
  {
    return this.a.v();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/bw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */