package android.support.v7.widget;

import android.support.v4.f.a;
import android.util.SparseArray;
import android.view.View;
import java.util.ArrayList;
import java.util.List;

public class cw
{
  a a = new a();
  a b = new a();
  a c = new a();
  final List d = new ArrayList();
  int e = 0;
  private int f = -1;
  private SparseArray g;
  private int h = 0;
  private int i = 0;
  private boolean j = false;
  private boolean k = false;
  private boolean l = false;
  private boolean m = false;
  
  private void a(a parama, cz paramcz)
  {
    for (int n = -1 + parama.size();; n--) {
      if (n >= 0)
      {
        if (paramcz == parama.c(n)) {
          parama.d(n);
        }
      }
      else {
        return;
      }
    }
  }
  
  void a(cz paramcz)
  {
    this.a.remove(paramcz);
    this.b.remove(paramcz);
    if (this.c != null) {
      a(this.c, paramcz);
    }
    this.d.remove(paramcz.a);
  }
  
  void a(View paramView)
  {
    this.d.remove(paramView);
  }
  
  public boolean a()
  {
    return this.k;
  }
  
  void b(View paramView)
  {
    if (!this.d.contains(paramView)) {
      this.d.add(paramView);
    }
  }
  
  public boolean b()
  {
    return this.m;
  }
  
  public int c()
  {
    return this.f;
  }
  
  public boolean d()
  {
    return this.f != -1;
  }
  
  public int e()
  {
    if (this.k) {
      return this.h - this.i;
    }
    return this.e;
  }
  
  public String toString()
  {
    return "State{mTargetPosition=" + this.f + ", mPreLayoutHolderMap=" + this.a + ", mPostLayoutHolderMap=" + this.b + ", mData=" + this.g + ", mItemCount=" + this.e + ", mPreviousLayoutItemCount=" + this.h + ", mDeletedInvisibleItemCountSincePreviousLayout=" + this.i + ", mStructureChanged=" + this.j + ", mInPreLayout=" + this.k + ", mRunSimpleAnimations=" + this.l + ", mRunPredictiveAnimations=" + this.m + '}';
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/cw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */