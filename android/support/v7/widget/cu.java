package android.support.v7.widget;

import android.util.Log;
import android.view.View;

public abstract class cu
{
  private int a;
  private RecyclerView b;
  private cl c;
  private boolean d;
  private boolean e;
  private View f;
  private final cv g;
  
  private void a(int paramInt1, int paramInt2)
  {
    if ((!this.e) || (this.a == -1)) {
      a();
    }
    this.d = false;
    if (this.f != null)
    {
      if (a(this.f) != this.a) {
        break label116;
      }
      a(this.f, this.b.e, this.g);
      cv.a(this.g, this.b);
      a();
    }
    for (;;)
    {
      if (this.e)
      {
        a(paramInt1, paramInt2, this.b.e, this.g);
        cv.a(this.g, this.b);
      }
      return;
      label116:
      Log.e("RecyclerView", "Passed over target position while smooth scrolling.");
      this.f = null;
    }
  }
  
  public int a(View paramView)
  {
    return this.b.e(paramView);
  }
  
  protected final void a()
  {
    if (!this.e) {
      return;
    }
    e();
    cw.d(this.b.e, -1);
    this.f = null;
    this.a = -1;
    this.d = false;
    this.e = false;
    cl.a(this.c, this);
    this.c = null;
    this.b = null;
  }
  
  public void a(int paramInt)
  {
    this.a = paramInt;
  }
  
  protected abstract void a(int paramInt1, int paramInt2, cw paramcw, cv paramcv);
  
  protected abstract void a(View paramView, cw paramcw, cv paramcv);
  
  protected void b(View paramView)
  {
    if (a(paramView) == d()) {
      this.f = paramView;
    }
  }
  
  public boolean b()
  {
    return this.d;
  }
  
  public boolean c()
  {
    return this.e;
  }
  
  public int d()
  {
    return this.a;
  }
  
  protected abstract void e();
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/cu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */