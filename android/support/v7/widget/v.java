package android.support.v7.widget;

import android.content.Context;
import android.support.v7.internal.widget.bf;
import android.support.v7.internal.widget.bh;
import android.util.AttributeSet;
import android.widget.CheckedTextView;

public class v
  extends CheckedTextView
{
  private static final int[] a = { 16843016 };
  private bf b;
  
  public v(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 16843720);
  }
  
  public v(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    if (bf.a)
    {
      bh localbh = bh.a(getContext(), paramAttributeSet, a, paramInt, 0);
      setCheckMarkDrawable(localbh.a(0));
      localbh.b();
      this.b = localbh.c();
    }
  }
  
  public void setCheckMarkDrawable(int paramInt)
  {
    if (this.b != null)
    {
      setCheckMarkDrawable(this.b.a(paramInt));
      return;
    }
    super.setCheckMarkDrawable(paramInt);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/v.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */