package android.support.v7.widget;

import android.content.Context;
import android.support.v7.d.c;
import android.support.v7.internal.view.menu.ad;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.m;
import android.support.v7.internal.view.menu.x;
import android.view.View;
import android.widget.ImageButton;

class du
  implements x
{
  i a;
  m b;
  
  private du(Toolbar paramToolbar) {}
  
  public void a(Context paramContext, i parami)
  {
    if ((this.a != null) && (this.b != null)) {
      this.a.d(this.b);
    }
    this.a = parami;
  }
  
  public void a(i parami, boolean paramBoolean) {}
  
  public boolean a(ad paramad)
  {
    return false;
  }
  
  public boolean a(i parami, m paramm)
  {
    Toolbar.b(this.c);
    if (Toolbar.c(this.c).getParent() != this.c) {
      this.c.addView(Toolbar.c(this.c));
    }
    this.c.a = paramm.getActionView();
    this.b = paramm;
    if (this.c.a.getParent() != this.c)
    {
      dv localdv = this.c.i();
      localdv.a = (0x800003 | 0x70 & Toolbar.d(this.c));
      localdv.b = 2;
      this.c.a.setLayoutParams(localdv);
      this.c.addView(this.c.a);
    }
    Toolbar.a(this.c, true);
    this.c.requestLayout();
    paramm.e(true);
    if ((this.c.a instanceof c)) {
      ((c)this.c.a).a();
    }
    return true;
  }
  
  public void b(boolean paramBoolean)
  {
    int i;
    int j;
    if (this.b != null)
    {
      i locali = this.a;
      i = 0;
      if (locali != null) {
        j = this.a.size();
      }
    }
    for (int k = 0;; k++)
    {
      i = 0;
      if (k < j)
      {
        if (this.a.getItem(k) == this.b) {
          i = 1;
        }
      }
      else
      {
        if (i == 0) {
          b(this.a, this.b);
        }
        return;
      }
    }
  }
  
  public boolean b()
  {
    return false;
  }
  
  public boolean b(i parami, m paramm)
  {
    if ((this.c.a instanceof c)) {
      ((c)this.c.a).b();
    }
    this.c.removeView(this.c.a);
    this.c.removeView(Toolbar.c(this.c));
    this.c.a = null;
    Toolbar.a(this.c, false);
    this.b = null;
    this.c.requestLayout();
    paramm.e(false);
    return true;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/du.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */