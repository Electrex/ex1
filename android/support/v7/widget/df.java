package android.support.v7.widget;

import android.view.View;

class df
{
  static int a(cw paramcw, bu parambu, View paramView1, View paramView2, cl paramcl, boolean paramBoolean)
  {
    if ((paramcl.p() == 0) || (paramcw.e() == 0) || (paramView1 == null) || (paramView2 == null)) {
      return 0;
    }
    if (!paramBoolean) {
      return 1 + Math.abs(paramcl.d(paramView1) - paramcl.d(paramView2));
    }
    int i = parambu.b(paramView2) - parambu.a(paramView1);
    return Math.min(parambu.f(), i);
  }
  
  static int a(cw paramcw, bu parambu, View paramView1, View paramView2, cl paramcl, boolean paramBoolean1, boolean paramBoolean2)
  {
    int i = paramcl.p();
    int j = 0;
    if (i != 0)
    {
      int k = paramcw.e();
      j = 0;
      if (k != 0)
      {
        j = 0;
        if (paramView1 != null)
        {
          j = 0;
          if (paramView2 != null) {
            break label46;
          }
        }
      }
    }
    for (;;)
    {
      return j;
      label46:
      int m = Math.min(paramcl.d(paramView1), paramcl.d(paramView2));
      int n = Math.max(paramcl.d(paramView1), paramcl.d(paramView2));
      if (paramBoolean2) {}
      for (j = Math.max(0, -1 + (paramcw.e() - n)); paramBoolean1; j = Math.max(0, m))
      {
        int i1 = Math.abs(parambu.b(paramView2) - parambu.a(paramView1));
        int i2 = 1 + Math.abs(paramcl.d(paramView1) - paramcl.d(paramView2));
        return Math.round(i1 / i2 * j + (parambu.c() - parambu.a(paramView1)));
      }
    }
  }
  
  static int b(cw paramcw, bu parambu, View paramView1, View paramView2, cl paramcl, boolean paramBoolean)
  {
    if ((paramcl.p() == 0) || (paramcw.e() == 0) || (paramView1 == null) || (paramView2 == null)) {
      return 0;
    }
    if (!paramBoolean) {
      return paramcw.e();
    }
    int i = parambu.b(paramView2) - parambu.a(paramView1);
    int j = 1 + Math.abs(paramcl.d(paramView1) - paramcl.d(paramView2));
    return (int)(i / j * paramcw.e());
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/df.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */