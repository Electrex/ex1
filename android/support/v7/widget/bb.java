package android.support.v7.widget;

import android.view.View;

class bb
{
  int a;
  int b;
  boolean c;
  
  bb(LinearLayoutManager paramLinearLayoutManager) {}
  
  private boolean a(View paramView, cw paramcw)
  {
    cm localcm = (cm)paramView.getLayoutParams();
    return (!localcm.a()) && (localcm.c() >= 0) && (localcm.c() < paramcw.e());
  }
  
  void a()
  {
    this.a = -1;
    this.b = Integer.MIN_VALUE;
    this.c = false;
  }
  
  public void a(View paramView)
  {
    int i = this.d.b.b();
    if (i >= 0) {
      b(paramView);
    }
    int k;
    int i1;
    do
    {
      int j;
      do
      {
        int i2;
        int i6;
        do
        {
          do
          {
            return;
            this.a = this.d.d(paramView);
            if (!this.c) {
              break;
            }
            i2 = this.d.b.d() - i - this.d.b.b(paramView);
            this.b = (this.d.b.d() - i2);
          } while (i2 <= 0);
          int i3 = this.d.b.c(paramView);
          int i4 = this.b - i3;
          int i5 = this.d.b.c();
          i6 = i4 - (i5 + Math.min(this.d.b.a(paramView) - i5, 0));
        } while (i6 >= 0);
        this.b += Math.min(i2, -i6);
        return;
        j = this.d.b.a(paramView);
        k = j - this.d.b.c();
        this.b = j;
      } while (k <= 0);
      int m = j + this.d.b.c(paramView);
      int n = this.d.b.d() - i - this.d.b.b(paramView);
      i1 = this.d.b.d() - Math.min(0, n) - m;
    } while (i1 >= 0);
    this.b -= Math.min(k, -i1);
  }
  
  void b()
  {
    if (this.c) {}
    for (int i = this.d.b.d();; i = this.d.b.c())
    {
      this.b = i;
      return;
    }
  }
  
  public void b(View paramView)
  {
    if (this.c) {}
    for (this.b = (this.d.b.b(paramView) + this.d.b.b());; this.b = this.d.b.a(paramView))
    {
      this.a = this.d.d(paramView);
      return;
    }
  }
  
  public String toString()
  {
    return "AnchorInfo{mPosition=" + this.a + ", mCoordinate=" + this.b + ", mLayoutFromEnd=" + this.c + '}';
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/bb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */