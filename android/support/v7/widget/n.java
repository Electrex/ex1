package android.support.v7.widget;

import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.j;
import android.view.MenuItem;

class n
  implements j
{
  private n(ActionMenuView paramActionMenuView) {}
  
  public void a(i parami)
  {
    if (ActionMenuView.b(this.a) != null) {
      ActionMenuView.b(this.a).a(parami);
    }
  }
  
  public boolean a(i parami, MenuItem paramMenuItem)
  {
    return (ActionMenuView.a(this.a) != null) && (ActionMenuView.a(this.a).a(paramMenuItem));
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/n.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */