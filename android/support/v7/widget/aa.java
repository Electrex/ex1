package android.support.v7.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v7.a.b;
import android.support.v7.internal.widget.be;
import android.support.v7.internal.widget.bf;
import android.support.v7.internal.widget.bh;
import android.util.AttributeSet;
import android.widget.ListPopupWindow;
import android.widget.Spinner;
import java.lang.reflect.Field;

public class aa
  extends Spinner
{
  private static final int[] a = { 16842964, 16843126 };
  private be b;
  
  public aa(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, b.spinnerStyle);
  }
  
  public aa(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    bh localbh;
    Drawable localDrawable;
    if (bf.a)
    {
      localbh = bh.a(getContext(), paramAttributeSet, a, paramInt, 0);
      if (localbh.e(0))
      {
        ColorStateList localColorStateList = localbh.c().c(localbh.f(0, -1));
        if (localColorStateList != null) {
          setSupportBackgroundTintList(localColorStateList);
        }
      }
      if (localbh.e(1))
      {
        localDrawable = localbh.a(1);
        if (Build.VERSION.SDK_INT < 16) {
          break label102;
        }
        setPopupBackgroundDrawable(localDrawable);
      }
    }
    for (;;)
    {
      localbh.b();
      return;
      label102:
      if (Build.VERSION.SDK_INT >= 11) {
        a(this, localDrawable);
      }
    }
  }
  
  private void a()
  {
    if ((getBackground() != null) && (this.b != null)) {
      bf.a(this, this.b);
    }
  }
  
  @TargetApi(11)
  private static void a(Spinner paramSpinner, Drawable paramDrawable)
  {
    try
    {
      Field localField = Spinner.class.getDeclaredField("mPopup");
      localField.setAccessible(true);
      Object localObject = localField.get(paramSpinner);
      if ((localObject instanceof ListPopupWindow)) {
        ((ListPopupWindow)localObject).setBackgroundDrawable(paramDrawable);
      }
      return;
    }
    catch (NoSuchFieldException localNoSuchFieldException)
    {
      localNoSuchFieldException.printStackTrace();
      return;
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      localIllegalAccessException.printStackTrace();
    }
  }
  
  protected void drawableStateChanged()
  {
    super.drawableStateChanged();
    a();
  }
  
  public ColorStateList getSupportBackgroundTintList()
  {
    if (this.b != null) {
      return this.b.a;
    }
    return null;
  }
  
  public PorterDuff.Mode getSupportBackgroundTintMode()
  {
    if (this.b != null) {
      return this.b.b;
    }
    return null;
  }
  
  public void setSupportBackgroundTintList(ColorStateList paramColorStateList)
  {
    if (this.b == null) {
      this.b = new be();
    }
    this.b.a = paramColorStateList;
    this.b.d = true;
    a();
  }
  
  public void setSupportBackgroundTintMode(PorterDuff.Mode paramMode)
  {
    if (this.b == null) {
      this.b = new be();
    }
    this.b.b = paramMode;
    this.b.c = true;
    a();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/aa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */