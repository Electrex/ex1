package android.support.v7.widget;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.a.a;
import android.support.v4.view.a.aj;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import java.util.List;

public class LinearLayoutManager
  extends cl
{
  int a;
  bu b;
  boolean c = false;
  int d = -1;
  int e = Integer.MIN_VALUE;
  LinearLayoutManager.SavedState f = null;
  final bb g = new bb(this);
  private bd k;
  private boolean l;
  private boolean m = false;
  private boolean n = false;
  private boolean o = true;
  private boolean p;
  
  public LinearLayoutManager(Context paramContext)
  {
    this(paramContext, 1, false);
  }
  
  public LinearLayoutManager(Context paramContext, int paramInt, boolean paramBoolean)
  {
    a(paramInt);
    a(paramBoolean);
  }
  
  private View A()
  {
    if (this.c) {}
    for (int i = -1 + p();; i = 0) {
      return e(i);
    }
  }
  
  private View B()
  {
    if (this.c) {}
    for (int i = 0;; i = -1 + p()) {
      return e(i);
    }
  }
  
  private int a(int paramInt, cq paramcq, cw paramcw, boolean paramBoolean)
  {
    int i = this.b.d() - paramInt;
    if (i > 0)
    {
      int j = -c(-i, paramcq, paramcw);
      int i1 = paramInt + j;
      if (paramBoolean)
      {
        int i2 = this.b.d() - i1;
        if (i2 > 0)
        {
          this.b.a(i2);
          j += i2;
        }
      }
      return j;
    }
    return 0;
  }
  
  private View a(boolean paramBoolean1, boolean paramBoolean2)
  {
    if (this.c) {
      return a(-1 + p(), -1, paramBoolean1, paramBoolean2);
    }
    return a(0, p(), paramBoolean1, paramBoolean2);
  }
  
  private void a(int paramInt1, int paramInt2, boolean paramBoolean, cw paramcw)
  {
    int i = -1;
    int j = 1;
    this.k.h = a(paramcw);
    this.k.f = paramInt1;
    int i1;
    if (paramInt1 == j)
    {
      bd localbd4 = this.k;
      localbd4.h += this.b.g();
      View localView2 = B();
      bd localbd5 = this.k;
      if (this.c) {}
      for (;;)
      {
        localbd5.e = i;
        this.k.d = (d(localView2) + this.k.e);
        this.k.b = this.b.b(localView2);
        i1 = this.b.b(localView2) - this.b.d();
        this.k.c = paramInt2;
        if (paramBoolean)
        {
          bd localbd3 = this.k;
          localbd3.c -= i1;
        }
        this.k.g = i1;
        return;
        i = j;
      }
    }
    View localView1 = A();
    bd localbd1 = this.k;
    localbd1.h += this.b.c();
    bd localbd2 = this.k;
    if (this.c) {}
    for (;;)
    {
      localbd2.e = j;
      this.k.d = (d(localView1) + this.k.e);
      this.k.b = this.b.a(localView1);
      i1 = -this.b.a(localView1) + this.b.c();
      break;
      j = i;
    }
  }
  
  private void a(bb parambb)
  {
    b(parambb.a, parambb.b);
  }
  
  private void a(cq paramcq, int paramInt)
  {
    if (paramInt < 0) {}
    for (;;)
    {
      return;
      int i = p();
      if (this.c) {
        for (int i1 = i - 1; i1 >= 0; i1--)
        {
          View localView2 = e(i1);
          if (this.b.b(localView2) > paramInt)
          {
            a(paramcq, i - 1, i1);
            return;
          }
        }
      } else {
        for (int j = 0; j < i; j++)
        {
          View localView1 = e(j);
          if (this.b.b(localView1) > paramInt)
          {
            a(paramcq, 0, j);
            return;
          }
        }
      }
    }
  }
  
  private void a(cq paramcq, int paramInt1, int paramInt2)
  {
    if (paramInt1 == paramInt2) {}
    for (;;)
    {
      return;
      if (paramInt2 > paramInt1) {
        for (int i = paramInt2 - 1; i >= paramInt1; i--) {
          a(i, paramcq);
        }
      } else {
        while (paramInt1 > paramInt2)
        {
          a(paramInt1, paramcq);
          paramInt1--;
        }
      }
    }
  }
  
  private void a(cq paramcq, bd parambd)
  {
    if (!parambd.a) {
      return;
    }
    if (parambd.f == -1)
    {
      b(paramcq, parambd.g);
      return;
    }
    a(paramcq, parambd.g);
  }
  
  private int b(int paramInt, cq paramcq, cw paramcw, boolean paramBoolean)
  {
    int i = paramInt - this.b.c();
    if (i > 0)
    {
      int j = -c(i, paramcq, paramcw);
      int i1 = paramInt + j;
      if (paramBoolean)
      {
        int i2 = i1 - this.b.c();
        if (i2 > 0)
        {
          this.b.a(-i2);
          j -= i2;
        }
      }
      return j;
    }
    return 0;
  }
  
  private View b(boolean paramBoolean1, boolean paramBoolean2)
  {
    if (this.c) {
      return a(0, p(), paramBoolean1, paramBoolean2);
    }
    return a(-1 + p(), -1, paramBoolean1, paramBoolean2);
  }
  
  private void b(int paramInt1, int paramInt2)
  {
    this.k.c = (this.b.d() - paramInt2);
    bd localbd = this.k;
    if (this.c) {}
    for (int i = -1;; i = 1)
    {
      localbd.e = i;
      this.k.d = paramInt1;
      this.k.f = 1;
      this.k.b = paramInt2;
      this.k.g = Integer.MIN_VALUE;
      return;
    }
  }
  
  private void b(bb parambb)
  {
    c(parambb.a, parambb.b);
  }
  
  private void b(cq paramcq, int paramInt)
  {
    int i = p();
    if (paramInt < 0) {}
    for (;;)
    {
      return;
      int j = this.b.e() - paramInt;
      if (this.c) {
        for (int i2 = 0; i2 < i; i2++)
        {
          View localView2 = e(i2);
          if (this.b.a(localView2) < j)
          {
            a(paramcq, 0, i2);
            return;
          }
        }
      } else {
        for (int i1 = i - 1; i1 >= 0; i1--)
        {
          View localView1 = e(i1);
          if (this.b.a(localView1) < j)
          {
            a(paramcq, i - 1, i1);
            return;
          }
        }
      }
    }
  }
  
  private void b(cq paramcq, cw paramcw, int paramInt1, int paramInt2)
  {
    if ((!paramcw.b()) || (p() == 0) || (paramcw.a()) || (!j())) {
      return;
    }
    int i = 0;
    int j = 0;
    List localList = paramcq.b();
    int i1 = localList.size();
    int i2 = d(e(0));
    int i3 = 0;
    if (i3 < i1)
    {
      cz localcz = (cz)localList.get(i3);
      int i6;
      int i7;
      if (localcz.r())
      {
        i6 = j;
        i7 = i;
      }
      for (;;)
      {
        i3++;
        i = i7;
        j = i6;
        break;
        int i4;
        if (localcz.e() < i2)
        {
          i4 = 1;
          label128:
          if (i4 == this.c) {
            break label176;
          }
        }
        label176:
        for (int i5 = -1;; i5 = 1)
        {
          if (i5 != -1) {
            break label182;
          }
          i7 = i + this.b.c(localcz.a);
          i6 = j;
          break;
          i4 = 0;
          break label128;
        }
        label182:
        i6 = j + this.b.c(localcz.a);
        i7 = i;
      }
    }
    this.k.k = localList;
    if (i > 0)
    {
      c(d(A()), paramInt1);
      this.k.h = i;
      this.k.c = 0;
      this.k.a();
      a(paramcq, this.k, paramcw, false);
    }
    if (j > 0)
    {
      b(d(B()), paramInt2);
      this.k.h = j;
      this.k.c = 0;
      this.k.a();
      a(paramcq, this.k, paramcw, false);
    }
    this.k.k = null;
  }
  
  private void b(cw paramcw, bb parambb)
  {
    if (d(paramcw, parambb)) {}
    while (c(paramcw, parambb)) {
      return;
    }
    parambb.b();
    if (this.n) {}
    for (int i = -1 + paramcw.e();; i = 0)
    {
      parambb.a = i;
      return;
    }
  }
  
  private void c(int paramInt1, int paramInt2)
  {
    this.k.c = (paramInt2 - this.b.c());
    this.k.d = paramInt1;
    bd localbd = this.k;
    if (this.c) {}
    for (int i = 1;; i = -1)
    {
      localbd.e = i;
      this.k.f = -1;
      this.k.b = paramInt2;
      this.k.g = Integer.MIN_VALUE;
      return;
    }
  }
  
  private boolean c(cw paramcw, bb parambb)
  {
    if (p() == 0) {}
    do
    {
      return false;
      View localView1 = w();
      if ((localView1 != null) && (bb.a(parambb, localView1, paramcw)))
      {
        parambb.a(localView1);
        return true;
      }
    } while (this.l != this.n);
    View localView2;
    if (parambb.c)
    {
      localView2 = k(paramcw);
      label59:
      if (localView2 == null) {
        break label173;
      }
      parambb.b(localView2);
      if ((!paramcw.a()) && (j()))
      {
        int i;
        if (this.b.a(localView2) < this.b.d())
        {
          int i1 = this.b.b(localView2);
          int i2 = this.b.c();
          i = 0;
          if (i1 >= i2) {}
        }
        else
        {
          i = 1;
        }
        if (i != 0) {
          if (!parambb.c) {
            break label175;
          }
        }
      }
    }
    label173:
    label175:
    for (int j = this.b.d();; j = this.b.c())
    {
      parambb.b = j;
      return true;
      localView2 = l(paramcw);
      break label59;
      break;
    }
  }
  
  private boolean d(cw paramcw, bb parambb)
  {
    if ((paramcw.a()) || (this.d == -1)) {
      return false;
    }
    if ((this.d < 0) || (this.d >= paramcw.e()))
    {
      this.d = -1;
      this.e = Integer.MIN_VALUE;
      return false;
    }
    parambb.a = this.d;
    if ((this.f != null) && (this.f.a()))
    {
      parambb.c = this.f.c;
      if (parambb.c)
      {
        parambb.b = (this.b.d() - this.f.b);
        return true;
      }
      parambb.b = (this.b.c() + this.f.b);
      return true;
    }
    if (this.e == Integer.MIN_VALUE)
    {
      View localView = b(this.d);
      if (localView != null)
      {
        if (this.b.c(localView) > this.b.f())
        {
          parambb.b();
          return true;
        }
        if (this.b.a(localView) - this.b.c() < 0)
        {
          parambb.b = this.b.c();
          parambb.c = false;
          return true;
        }
        if (this.b.d() - this.b.b(localView) < 0)
        {
          parambb.b = this.b.d();
          parambb.c = true;
          return true;
        }
        if (parambb.c) {}
        for (int j = this.b.b(localView) + this.b.b();; j = this.b.a(localView))
        {
          parambb.b = j;
          return true;
        }
      }
      if (p() > 0)
      {
        int i = d(e(0));
        if (this.d >= i) {
          break label360;
        }
      }
      label360:
      for (boolean bool1 = true;; bool1 = false)
      {
        boolean bool2 = this.c;
        boolean bool3 = false;
        if (bool1 == bool2) {
          bool3 = true;
        }
        parambb.c = bool3;
        parambb.b();
        return true;
      }
    }
    parambb.c = this.c;
    if (this.c)
    {
      parambb.b = (this.b.d() - this.e);
      return true;
    }
    parambb.b = (this.b.c() + this.e);
    return true;
  }
  
  private int h(cw paramcw)
  {
    if (p() == 0) {
      return 0;
    }
    f();
    bu localbu = this.b;
    if (!this.o) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      View localView = a(bool1, true);
      boolean bool2 = this.o;
      boolean bool3 = false;
      if (!bool2) {
        bool3 = true;
      }
      return df.a(paramcw, localbu, localView, b(bool3, true), this, this.o, this.c);
    }
  }
  
  private int i(int paramInt)
  {
    int i = -1;
    int j = 1;
    int i1 = Integer.MIN_VALUE;
    switch (paramInt)
    {
    default: 
      i = i1;
    case 1: 
    case 2: 
    case 33: 
    case 130: 
    case 17: 
      do
      {
        do
        {
          return i;
          return j;
        } while (this.a == j);
        return i1;
        if (this.a == j) {
          i1 = j;
        }
        return i1;
      } while (this.a == 0);
      return i1;
    }
    if (this.a == 0) {}
    for (;;)
    {
      return j;
      j = i1;
    }
  }
  
  private int i(cw paramcw)
  {
    if (p() == 0) {
      return 0;
    }
    f();
    bu localbu = this.b;
    if (!this.o) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      View localView = a(bool1, true);
      boolean bool2 = this.o;
      boolean bool3 = false;
      if (!bool2) {
        bool3 = true;
      }
      return df.a(paramcw, localbu, localView, b(bool3, true), this, this.o);
    }
  }
  
  private int j(cw paramcw)
  {
    if (p() == 0) {
      return 0;
    }
    f();
    bu localbu = this.b;
    if (!this.o) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      View localView = a(bool1, true);
      boolean bool2 = this.o;
      boolean bool3 = false;
      if (!bool2) {
        bool3 = true;
      }
      return df.b(paramcw, localbu, localView, b(bool3, true), this, this.o);
    }
  }
  
  private View j(int paramInt)
  {
    return a(0, p(), paramInt);
  }
  
  private View k(int paramInt)
  {
    return a(-1 + p(), -1, paramInt);
  }
  
  private View k(cw paramcw)
  {
    if (this.c) {
      return j(paramcw.e());
    }
    return k(paramcw.e());
  }
  
  private View l(cw paramcw)
  {
    if (this.c) {
      return k(paramcw.e());
    }
    return j(paramcw.e());
  }
  
  private void z()
  {
    int i = 1;
    if ((this.a == i) || (!e()))
    {
      this.c = this.m;
      return;
    }
    if (!this.m) {}
    for (;;)
    {
      this.c = i;
      return;
      i = 0;
    }
  }
  
  public int a(int paramInt, cq paramcq, cw paramcw)
  {
    if (this.a == 1) {
      return 0;
    }
    return c(paramInt, paramcq, paramcw);
  }
  
  int a(cq paramcq, bd parambd, cw paramcw, boolean paramBoolean)
  {
    int i = parambd.c;
    if (parambd.g != Integer.MIN_VALUE)
    {
      if (parambd.c < 0) {
        parambd.g += parambd.c;
      }
      a(paramcq, parambd);
    }
    int j = parambd.c + parambd.h;
    bc localbc = new bc();
    if ((j > 0) && (parambd.a(paramcw)))
    {
      localbc.a();
      a(paramcq, paramcw, parambd, localbc);
      if (!localbc.b) {
        break label104;
      }
    }
    for (;;)
    {
      return i - parambd.c;
      label104:
      parambd.b += localbc.a * parambd.f;
      if ((!localbc.c) || (this.k.k != null) || (!paramcw.a()))
      {
        parambd.c -= localbc.a;
        j -= localbc.a;
      }
      if (parambd.g != Integer.MIN_VALUE)
      {
        parambd.g += localbc.a;
        if (parambd.c < 0) {
          parambd.g += parambd.c;
        }
        a(paramcq, parambd);
      }
      if ((!paramBoolean) || (!localbc.d)) {
        break;
      }
    }
  }
  
  protected int a(cw paramcw)
  {
    if (paramcw.d()) {
      return this.b.f();
    }
    return 0;
  }
  
  public cm a()
  {
    return new cm(-2, -2);
  }
  
  View a(int paramInt1, int paramInt2, int paramInt3)
  {
    Object localObject1 = null;
    f();
    int i = this.b.c();
    int j = this.b.d();
    int i1;
    Object localObject2;
    label36:
    Object localObject3;
    Object localObject4;
    if (paramInt2 > paramInt1)
    {
      i1 = 1;
      localObject2 = null;
      if (paramInt1 == paramInt2) {
        break label156;
      }
      localObject3 = e(paramInt1);
      int i2 = d((View)localObject3);
      if ((i2 < 0) || (i2 >= paramInt3)) {
        break label175;
      }
      if (!((cm)((View)localObject3).getLayoutParams()).a()) {
        break label112;
      }
      if (localObject2 != null) {
        break label175;
      }
      localObject4 = localObject1;
    }
    for (;;)
    {
      paramInt1 += i1;
      localObject1 = localObject4;
      localObject2 = localObject3;
      break label36;
      i1 = -1;
      break;
      label112:
      if ((this.b.a((View)localObject3) >= j) || (this.b.b((View)localObject3) < i))
      {
        if (localObject1 != null) {
          break label175;
        }
        localObject4 = localObject3;
        localObject3 = localObject2;
        continue;
        label156:
        if (localObject1 == null) {
          break label168;
        }
      }
      for (;;)
      {
        localObject3 = localObject1;
        return (View)localObject3;
        label168:
        localObject1 = localObject2;
      }
      label175:
      localObject4 = localObject1;
      localObject3 = localObject2;
    }
  }
  
  View a(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
  {
    f();
    int i = this.b.c();
    int j = this.b.d();
    if (paramInt2 > paramInt1) {}
    Object localObject1;
    Object localObject2;
    for (int i1 = 1;; i1 = -1)
    {
      localObject1 = null;
      if (paramInt1 == paramInt2) {
        break label130;
      }
      localObject2 = e(paramInt1);
      int i2 = this.b.a((View)localObject2);
      int i3 = this.b.b((View)localObject2);
      if ((i2 >= j) || (i3 <= i)) {
        break label133;
      }
      if ((paramBoolean1) && ((i2 < i) || (i3 > j))) {
        break;
      }
      return (View)localObject2;
    }
    if ((paramBoolean2) && (localObject1 == null)) {}
    for (;;)
    {
      paramInt1 += i1;
      localObject1 = localObject2;
      break;
      label130:
      return (View)localObject1;
      label133:
      localObject2 = localObject1;
    }
  }
  
  public View a(View paramView, int paramInt, cq paramcq, cw paramcw)
  {
    z();
    if (p() == 0) {}
    label45:
    label145:
    label147:
    label154:
    for (;;)
    {
      return null;
      int i = i(paramInt);
      if (i != Integer.MIN_VALUE)
      {
        f();
        View localView1;
        if (i == -1)
        {
          localView1 = l(paramcw);
          if (localView1 == null) {
            break label145;
          }
          f();
          a(i, (int)(0.33F * this.b.f()), false, paramcw);
          this.k.g = Integer.MIN_VALUE;
          this.k.a = false;
          a(paramcq, this.k, paramcw, true);
          if (i != -1) {
            break label147;
          }
        }
        for (View localView2 = A();; localView2 = B())
        {
          if ((localView2 == localView1) || (!localView2.isFocusable())) {
            break label154;
          }
          return localView2;
          localView1 = k(paramcw);
          break label45;
          break;
        }
      }
    }
  }
  
  public void a(int paramInt)
  {
    if ((paramInt != 0) && (paramInt != 1)) {
      throw new IllegalArgumentException("invalid orientation:" + paramInt);
    }
    a(null);
    if (paramInt == this.a) {
      return;
    }
    this.a = paramInt;
    this.b = null;
    k();
  }
  
  public void a(Parcelable paramParcelable)
  {
    if ((paramParcelable instanceof LinearLayoutManager.SavedState))
    {
      this.f = ((LinearLayoutManager.SavedState)paramParcelable);
      k();
    }
  }
  
  public void a(RecyclerView paramRecyclerView, cq paramcq)
  {
    super.a(paramRecyclerView, paramcq);
    if (this.p)
    {
      c(paramcq);
      paramcq.a();
    }
  }
  
  public void a(cq paramcq, cw paramcw)
  {
    if ((this.f != null) && (this.f.a())) {
      this.d = this.f.a;
    }
    f();
    this.k.a = false;
    z();
    this.g.a();
    this.g.c = (this.c ^ this.n);
    b(paramcw, this.g);
    int i = a(paramcw);
    int j;
    int i1;
    int i2;
    View localView;
    int i21;
    label188:
    label200:
    int i15;
    int i17;
    if (this.k.j >= 0)
    {
      j = 0;
      i1 = j + this.b.c();
      i2 = i + this.b.g();
      if ((paramcw.a()) && (this.d != -1) && (this.e != Integer.MIN_VALUE))
      {
        localView = b(this.d);
        if (localView != null)
        {
          if (!this.c) {
            break label557;
          }
          i21 = this.b.d() - this.b.b(localView) - this.e;
          if (i21 <= 0) {
            break label588;
          }
          i1 += i21;
        }
      }
      a(paramcw, this.g);
      a(paramcq);
      this.k.i = paramcw.a();
      if (!this.g.c) {
        break label598;
      }
      b(this.g);
      this.k.h = i1;
      a(paramcq, this.k, paramcw, false);
      i15 = this.k.b;
      int i16 = this.k.d;
      if (this.k.c > 0) {
        i2 += this.k.c;
      }
      a(this.g);
      this.k.h = i2;
      bd localbd2 = this.k;
      localbd2.d += this.k.e;
      a(paramcq, this.k, paramcw, false);
      i17 = this.k.b;
      if (this.k.c <= 0) {
        break label842;
      }
      int i19 = this.k.c;
      c(i16, i15);
      this.k.h = i19;
      a(paramcq, this.k, paramcw, false);
    }
    label431:
    label557:
    label588:
    label598:
    label789:
    label842:
    for (int i18 = this.k.b;; i18 = i15)
    {
      int i5 = i18;
      int i3 = i17;
      int i13;
      int i14;
      if (p() > 0)
      {
        if (!(this.c ^ this.n)) {
          break label789;
        }
        int i11 = a(i3, paramcq, paramcw, true);
        int i12 = i5 + i11;
        i13 = i3 + i11;
        i14 = b(i12, paramcq, paramcw, false);
        i5 = i12 + i14;
      }
      int i9;
      int i10;
      for (i3 = i13 + i14;; i3 = i9 + i10)
      {
        b(paramcq, paramcw, i5, i3);
        if (!paramcw.a())
        {
          this.d = -1;
          this.e = Integer.MIN_VALUE;
          this.b.a();
        }
        this.l = this.n;
        this.f = null;
        return;
        j = i;
        i = 0;
        break;
        int i20 = this.b.a(localView) - this.b.c();
        i21 = this.e - i20;
        break label188;
        i2 -= i21;
        break label200;
        a(this.g);
        this.k.h = i2;
        a(paramcq, this.k, paramcw, false);
        i3 = this.k.b;
        int i4 = this.k.d;
        if (this.k.c > 0) {
          i1 += this.k.c;
        }
        b(this.g);
        this.k.h = i1;
        bd localbd1 = this.k;
        localbd1.d += this.k.e;
        a(paramcq, this.k, paramcw, false);
        i5 = this.k.b;
        if (this.k.c <= 0) {
          break label431;
        }
        int i6 = this.k.c;
        b(i4, i3);
        this.k.h = i6;
        a(paramcq, this.k, paramcw, false);
        i3 = this.k.b;
        break label431;
        int i7 = b(i5, paramcq, paramcw, true);
        int i8 = i5 + i7;
        i9 = i3 + i7;
        i10 = a(i9, paramcq, paramcw, false);
        i5 = i8 + i10;
      }
    }
  }
  
  void a(cq paramcq, cw paramcw, bd parambd, bc parambc)
  {
    View localView = parambd.a(paramcq);
    if (localView == null)
    {
      parambc.b = true;
      return;
    }
    cm localcm = (cm)localView.getLayoutParams();
    boolean bool4;
    label66:
    int i6;
    int i3;
    label128:
    int i;
    int i4;
    int j;
    if (parambd.k == null)
    {
      boolean bool3 = this.c;
      if (parambd.f == -1)
      {
        bool4 = true;
        if (bool3 != bool4) {
          break label239;
        }
        b(localView);
        a(localView, 0, 0);
        parambc.a = this.b.c(localView);
        if (this.a != 1) {
          break label354;
        }
        if (!e()) {
          break label298;
        }
        i6 = q() - u();
        i3 = i6 - this.b.d(localView);
        if (parambd.f != -1) {
          break label321;
        }
        int i9 = parambd.b;
        i = parambd.b - parambc.a;
        i4 = i6;
        j = i9;
      }
    }
    for (;;)
    {
      a(localView, i3 + localcm.leftMargin, i + localcm.topMargin, i4 - localcm.rightMargin, j - localcm.bottomMargin);
      if ((localcm.a()) || (localcm.b())) {
        parambc.c = true;
      }
      parambc.d = localView.isFocusable();
      return;
      bool4 = false;
      break;
      label239:
      b(localView, 0);
      break label66;
      boolean bool1 = this.c;
      if (parambd.f == -1) {}
      for (boolean bool2 = true;; bool2 = false)
      {
        if (bool1 != bool2) {
          break label288;
        }
        a(localView);
        break;
      }
      label288:
      a(localView, 0);
      break label66;
      label298:
      i3 = s();
      i6 = i3 + this.b.d(localView);
      break label128;
      label321:
      int i7 = parambd.b;
      int i8 = parambd.b + parambc.a;
      i = i7;
      i4 = i6;
      j = i8;
      continue;
      label354:
      i = t();
      j = i + this.b.d(localView);
      if (parambd.f == -1)
      {
        int i5 = parambd.b;
        i3 = parambd.b - parambc.a;
        i4 = i5;
      }
      else
      {
        int i1 = parambd.b;
        int i2 = parambd.b + parambc.a;
        i3 = i1;
        i4 = i2;
      }
    }
  }
  
  void a(cw paramcw, bb parambb) {}
  
  public void a(AccessibilityEvent paramAccessibilityEvent)
  {
    super.a(paramAccessibilityEvent);
    if (p() > 0)
    {
      aj localaj = a.a(paramAccessibilityEvent);
      localaj.b(h());
      localaj.c(i());
    }
  }
  
  public void a(String paramString)
  {
    if (this.f == null) {
      super.a(paramString);
    }
  }
  
  public void a(boolean paramBoolean)
  {
    a(null);
    if (paramBoolean == this.m) {
      return;
    }
    this.m = paramBoolean;
    k();
  }
  
  public int b(int paramInt, cq paramcq, cw paramcw)
  {
    if (this.a == 0) {
      return 0;
    }
    return c(paramInt, paramcq, paramcw);
  }
  
  public int b(cw paramcw)
  {
    return h(paramcw);
  }
  
  public Parcelable b()
  {
    if (this.f != null) {
      return new LinearLayoutManager.SavedState(this.f);
    }
    LinearLayoutManager.SavedState localSavedState = new LinearLayoutManager.SavedState();
    if (p() > 0)
    {
      f();
      boolean bool = this.l ^ this.c;
      localSavedState.c = bool;
      if (bool)
      {
        View localView2 = B();
        localSavedState.b = (this.b.d() - this.b.b(localView2));
        localSavedState.a = d(localView2);
        return localSavedState;
      }
      View localView1 = A();
      localSavedState.a = d(localView1);
      localSavedState.b = (this.b.a(localView1) - this.b.c());
      return localSavedState;
    }
    localSavedState.b();
    return localSavedState;
  }
  
  public View b(int paramInt)
  {
    int i = p();
    if (i == 0) {}
    int j;
    do
    {
      return null;
      j = paramInt - d(e(0));
    } while ((j < 0) || (j >= i));
    return e(j);
  }
  
  int c(int paramInt, cq paramcq, cw paramcw)
  {
    if ((p() == 0) || (paramInt == 0)) {
      return 0;
    }
    this.k.a = true;
    f();
    if (paramInt > 0) {}
    int j;
    int i1;
    for (int i = 1;; i = -1)
    {
      j = Math.abs(paramInt);
      a(i, j, true, paramcw);
      i1 = this.k.g + a(paramcq, this.k, paramcw, false);
      if (i1 >= 0) {
        break;
      }
      return 0;
    }
    if (j > i1) {
      paramInt = i * i1;
    }
    this.b.a(-paramInt);
    this.k.j = paramInt;
    return paramInt;
  }
  
  public int c(cw paramcw)
  {
    return h(paramcw);
  }
  
  public boolean c()
  {
    return this.a == 0;
  }
  
  public int d(cw paramcw)
  {
    return i(paramcw);
  }
  
  public boolean d()
  {
    return this.a == 1;
  }
  
  public int e(cw paramcw)
  {
    return i(paramcw);
  }
  
  protected boolean e()
  {
    return n() == 1;
  }
  
  public int f(cw paramcw)
  {
    return j(paramcw);
  }
  
  void f()
  {
    if (this.k == null) {
      this.k = g();
    }
    if (this.b == null) {
      this.b = bu.a(this, this.a);
    }
  }
  
  public int g(cw paramcw)
  {
    return j(paramcw);
  }
  
  bd g()
  {
    return new bd();
  }
  
  public int h()
  {
    View localView = a(0, p(), false, true);
    if (localView == null) {
      return -1;
    }
    return d(localView);
  }
  
  public int i()
  {
    View localView = a(-1 + p(), -1, false, true);
    if (localView == null) {
      return -1;
    }
    return d(localView);
  }
  
  public boolean j()
  {
    return (this.f == null) && (this.l == this.n);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/LinearLayoutManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */