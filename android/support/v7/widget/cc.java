package android.support.v7.widget;

import android.support.v4.d.h;
import android.view.ViewGroup;

public abstract class cc
{
  private final cd a = new cd();
  private boolean b = false;
  
  public abstract int a();
  
  public int a(int paramInt)
  {
    return 0;
  }
  
  public abstract cz a(ViewGroup paramViewGroup, int paramInt);
  
  public void a(RecyclerView paramRecyclerView) {}
  
  public void a(ce paramce)
  {
    this.a.registerObserver(paramce);
  }
  
  public void a(cz paramcz) {}
  
  public abstract void a(cz paramcz, int paramInt);
  
  public long b(int paramInt)
  {
    return -1L;
  }
  
  public final cz b(ViewGroup paramViewGroup, int paramInt)
  {
    h.a("RV CreateView");
    cz localcz = a(paramViewGroup, paramInt);
    localcz.e = paramInt;
    h.a();
    return localcz;
  }
  
  public void b(RecyclerView paramRecyclerView) {}
  
  public void b(ce paramce)
  {
    this.a.unregisterObserver(paramce);
  }
  
  public final void b(cz paramcz, int paramInt)
  {
    paramcz.b = paramInt;
    if (b()) {
      paramcz.d = b(paramInt);
    }
    paramcz.a(1, 519);
    h.a("RV OnBindView");
    a(paramcz, paramInt);
    h.a();
  }
  
  public final boolean b()
  {
    return this.b;
  }
  
  public boolean b(cz paramcz)
  {
    return false;
  }
  
  public final void c()
  {
    this.a.a();
  }
  
  public final void c(int paramInt)
  {
    this.a.a(paramInt, 1);
  }
  
  public void c(cz paramcz) {}
  
  public final void d(int paramInt)
  {
    this.a.b(paramInt, 1);
  }
  
  public void d(cz paramcz) {}
  
  public final void e(int paramInt)
  {
    this.a.c(paramInt, 1);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/cc.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */