package android.support.v7.widget;

import android.content.Context;
import android.view.View;

class ac
  implements ag
{
  public float a(ad paramad)
  {
    return ((dc)paramad.getBackground()).a();
  }
  
  public void a() {}
  
  public void a(ad paramad, float paramFloat)
  {
    ((dc)paramad.getBackground()).a(paramFloat);
  }
  
  public void a(ad paramad, int paramInt)
  {
    ((dc)paramad.getBackground()).a(paramInt);
  }
  
  public void a(ad paramad, Context paramContext, int paramInt, float paramFloat1, float paramFloat2, float paramFloat3)
  {
    paramad.setBackgroundDrawable(new dc(paramInt, paramFloat1));
    View localView = (View)paramad;
    localView.setClipToOutline(true);
    localView.setElevation(paramFloat2);
    b(paramad, paramFloat3);
  }
  
  public float b(ad paramad)
  {
    return 2.0F * d(paramad);
  }
  
  public void b(ad paramad, float paramFloat)
  {
    ((dc)paramad.getBackground()).a(paramFloat, paramad.getUseCompatPadding(), paramad.getPreventCornerOverlap());
    f(paramad);
  }
  
  public float c(ad paramad)
  {
    return 2.0F * d(paramad);
  }
  
  public void c(ad paramad, float paramFloat)
  {
    ((View)paramad).setElevation(paramFloat);
  }
  
  public float d(ad paramad)
  {
    return ((dc)paramad.getBackground()).b();
  }
  
  public float e(ad paramad)
  {
    return ((View)paramad).getElevation();
  }
  
  public void f(ad paramad)
  {
    if (!paramad.getUseCompatPadding())
    {
      paramad.a(0, 0, 0, 0);
      return;
    }
    float f1 = a(paramad);
    float f2 = d(paramad);
    int i = (int)Math.ceil(dd.b(f1, f2, paramad.getPreventCornerOverlap()));
    int j = (int)Math.ceil(dd.a(f1, f2, paramad.getPreventCornerOverlap()));
    paramad.a(i, j, i, j);
  }
  
  public void g(ad paramad)
  {
    b(paramad, a(paramad));
  }
  
  public void h(ad paramad)
  {
    b(paramad, a(paramad));
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/ac.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */