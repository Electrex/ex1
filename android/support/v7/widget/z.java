package android.support.v7.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Paint;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.support.v4.b.a.m;
import android.support.v4.view.bv;
import android.support.v7.a.b;
import android.support.v7.internal.widget.bf;
import android.support.v7.internal.widget.bh;
import android.util.AttributeSet;
import android.widget.RatingBar;

public class z
  extends RatingBar
{
  private static final int[] a = { 16843067, 16843068 };
  private Bitmap b;
  
  public z(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, b.ratingBarStyle);
  }
  
  public z(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    if (bf.a)
    {
      bh localbh = bh.a(getContext(), paramAttributeSet, a, paramInt, 0);
      Drawable localDrawable1 = localbh.b(0);
      if (localDrawable1 != null) {
        setIndeterminateDrawable(a(localDrawable1));
      }
      Drawable localDrawable2 = localbh.b(1);
      if (localDrawable2 != null) {
        setProgressDrawable(a(localDrawable2, false));
      }
      localbh.b();
    }
  }
  
  private Drawable a(Drawable paramDrawable)
  {
    if ((paramDrawable instanceof AnimationDrawable))
    {
      AnimationDrawable localAnimationDrawable1 = (AnimationDrawable)paramDrawable;
      int i = localAnimationDrawable1.getNumberOfFrames();
      AnimationDrawable localAnimationDrawable2 = new AnimationDrawable();
      localAnimationDrawable2.setOneShot(localAnimationDrawable1.isOneShot());
      for (int j = 0; j < i; j++)
      {
        Drawable localDrawable = a(localAnimationDrawable1.getFrame(j), true);
        localDrawable.setLevel(10000);
        localAnimationDrawable2.addFrame(localDrawable, localAnimationDrawable1.getDuration(j));
      }
      localAnimationDrawable2.setLevel(10000);
      paramDrawable = localAnimationDrawable2;
    }
    return paramDrawable;
  }
  
  private Drawable a(Drawable paramDrawable, boolean paramBoolean)
  {
    int i = 0;
    if ((paramDrawable instanceof m))
    {
      Drawable localDrawable2 = ((m)paramDrawable).a();
      if (localDrawable2 != null)
      {
        Drawable localDrawable3 = a(localDrawable2, paramBoolean);
        ((m)paramDrawable).a(localDrawable3);
      }
    }
    do
    {
      Object localObject = paramDrawable;
      for (;;)
      {
        return (Drawable)localObject;
        if (!(paramDrawable instanceof LayerDrawable)) {
          break;
        }
        LayerDrawable localLayerDrawable = (LayerDrawable)paramDrawable;
        int j = localLayerDrawable.getNumberOfLayers();
        Drawable[] arrayOfDrawable = new Drawable[j];
        int k = 0;
        if (k < j)
        {
          int m = localLayerDrawable.getId(k);
          Drawable localDrawable1 = localLayerDrawable.getDrawable(k);
          if ((m == 16908301) || (m == 16908303)) {}
          for (boolean bool = true;; bool = false)
          {
            arrayOfDrawable[k] = a(localDrawable1, bool);
            k++;
            break;
          }
        }
        localObject = new LayerDrawable(arrayOfDrawable);
        while (i < j)
        {
          ((LayerDrawable)localObject).setId(i, localLayerDrawable.getId(i));
          i++;
        }
      }
    } while (!(paramDrawable instanceof BitmapDrawable));
    Bitmap localBitmap = ((BitmapDrawable)paramDrawable).getBitmap();
    if (this.b == null) {
      this.b = localBitmap;
    }
    ShapeDrawable localShapeDrawable = new ShapeDrawable(getDrawableShape());
    BitmapShader localBitmapShader = new BitmapShader(localBitmap, Shader.TileMode.REPEAT, Shader.TileMode.CLAMP);
    localShapeDrawable.getPaint().setShader(localBitmapShader);
    if (paramBoolean) {
      return new ClipDrawable(localShapeDrawable, 3, 1);
    }
    return localShapeDrawable;
  }
  
  private Shape getDrawableShape()
  {
    return new RoundRectShape(new float[] { 5.0F, 5.0F, 5.0F, 5.0F, 5.0F, 5.0F, 5.0F, 5.0F }, null, null);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    try
    {
      super.onMeasure(paramInt1, paramInt2);
      if (this.b != null) {
        setMeasuredDimension(bv.a(this.b.getWidth() * getNumStars(), paramInt1, 0), getMeasuredHeight());
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/z.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */