package android.support.v7.widget;

import android.util.SparseArray;
import android.util.SparseIntArray;
import java.util.ArrayList;

public class cp
{
  private SparseArray a = new SparseArray();
  private SparseIntArray b = new SparseIntArray();
  private int c = 0;
  
  private ArrayList b(int paramInt)
  {
    ArrayList localArrayList = (ArrayList)this.a.get(paramInt);
    if (localArrayList == null)
    {
      localArrayList = new ArrayList();
      this.a.put(paramInt, localArrayList);
      if (this.b.indexOfKey(paramInt) < 0) {
        this.b.put(paramInt, 5);
      }
    }
    return localArrayList;
  }
  
  public cz a(int paramInt)
  {
    ArrayList localArrayList = (ArrayList)this.a.get(paramInt);
    if ((localArrayList != null) && (!localArrayList.isEmpty()))
    {
      int i = -1 + localArrayList.size();
      cz localcz = (cz)localArrayList.get(i);
      localArrayList.remove(i);
      return localcz;
    }
    return null;
  }
  
  public void a()
  {
    this.a.clear();
  }
  
  void a(cc paramcc)
  {
    this.c = (1 + this.c);
  }
  
  void a(cc paramcc1, cc paramcc2, boolean paramBoolean)
  {
    if (paramcc1 != null) {
      b();
    }
    if ((!paramBoolean) && (this.c == 0)) {
      a();
    }
    if (paramcc2 != null) {
      a(paramcc2);
    }
  }
  
  public void a(cz paramcz)
  {
    int i = paramcz.h();
    ArrayList localArrayList = b(i);
    if (this.b.get(i) <= localArrayList.size()) {
      return;
    }
    paramcz.u();
    localArrayList.add(paramcz);
  }
  
  void b()
  {
    this.c = (-1 + this.c);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/cp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */