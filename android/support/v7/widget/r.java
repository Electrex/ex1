package android.support.v7.widget;

class r
{
  int a;
  int b;
  int c;
  
  r(int paramInt1, int paramInt2, int paramInt3)
  {
    this.a = paramInt1;
    this.b = paramInt2;
    this.c = paramInt3;
  }
  
  String a()
  {
    switch (this.a)
    {
    default: 
      return "??";
    case 0: 
      return "add";
    case 1: 
      return "rm";
    case 2: 
      return "up";
    }
    return "mv";
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    r localr;
    do
    {
      do
      {
        return true;
        if ((paramObject == null) || (getClass() != paramObject.getClass())) {
          return false;
        }
        localr = (r)paramObject;
        if (this.a != localr.a) {
          return false;
        }
      } while ((this.a == 3) && (Math.abs(this.c - this.b) == 1) && (this.c == localr.b) && (this.b == localr.c));
      if (this.c != localr.c) {
        return false;
      }
    } while (this.b == localr.b);
    return false;
  }
  
  public int hashCode()
  {
    return 31 * (31 * this.a + this.b) + this.c;
  }
  
  public String toString()
  {
    return "[" + a() + ",s:" + this.b + "c:" + this.c + "]";
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/r.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */