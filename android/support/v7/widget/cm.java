package android.support.v7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;

public class cm
  extends ViewGroup.MarginLayoutParams
{
  cz a;
  final Rect b = new Rect();
  boolean c = true;
  boolean d = false;
  
  public cm(int paramInt1, int paramInt2)
  {
    super(paramInt1, paramInt2);
  }
  
  public cm(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public cm(cm paramcm)
  {
    super(paramcm);
  }
  
  public cm(ViewGroup.LayoutParams paramLayoutParams)
  {
    super(paramLayoutParams);
  }
  
  public cm(ViewGroup.MarginLayoutParams paramMarginLayoutParams)
  {
    super(paramMarginLayoutParams);
  }
  
  public boolean a()
  {
    return this.a.r();
  }
  
  public boolean b()
  {
    return this.a.p();
  }
  
  public int c()
  {
    return this.a.e();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/cm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */