package android.support.v7.widget;

import android.support.v4.d.h;
import android.support.v4.view.bv;
import android.support.v4.widget.aw;
import android.view.View;
import android.view.animation.Interpolator;
import java.util.ArrayList;

class cy
  implements Runnable
{
  private int b;
  private int c;
  private aw d;
  private Interpolator e = RecyclerView.q();
  private boolean f = false;
  private boolean g = false;
  
  public cy(RecyclerView paramRecyclerView)
  {
    this.d = aw.a(paramRecyclerView.getContext(), RecyclerView.q());
  }
  
  private float a(float paramFloat)
  {
    return (float)Math.sin((float)(0.4712389167638204D * (paramFloat - 0.5F)));
  }
  
  private int b(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = Math.abs(paramInt1);
    int j = Math.abs(paramInt2);
    int k;
    int m;
    int n;
    if (i > j)
    {
      k = 1;
      m = (int)Math.sqrt(paramInt3 * paramInt3 + paramInt4 * paramInt4);
      n = (int)Math.sqrt(paramInt1 * paramInt1 + paramInt2 * paramInt2);
      if (k == 0) {
        break label142;
      }
    }
    int i4;
    label142:
    for (int i1 = this.a.getWidth();; i1 = this.a.getHeight())
    {
      int i2 = i1 / 2;
      float f1 = Math.min(1.0F, 1.0F * n / i1);
      float f2 = i2 + i2 * a(f1);
      if (m <= 0) {
        break label154;
      }
      i4 = 4 * Math.round(1000.0F * Math.abs(f2 / m));
      return Math.min(i4, 2000);
      k = 0;
      break;
    }
    label154:
    if (k != 0) {}
    for (int i3 = i;; i3 = j)
    {
      i4 = (int)(300.0F * (1.0F + i3 / i1));
      break;
    }
  }
  
  private void c()
  {
    this.g = false;
    this.f = true;
  }
  
  private void d()
  {
    this.f = false;
    if (this.g) {
      a();
    }
  }
  
  void a()
  {
    if (this.f)
    {
      this.g = true;
      return;
    }
    this.a.removeCallbacks(this);
    bv.a(this.a, this);
  }
  
  public void a(int paramInt1, int paramInt2)
  {
    RecyclerView.b(this.a, 2);
    this.c = 0;
    this.b = 0;
    this.d.a(0, 0, paramInt1, paramInt2, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
    a();
  }
  
  public void a(int paramInt1, int paramInt2, int paramInt3)
  {
    a(paramInt1, paramInt2, paramInt3, RecyclerView.q());
  }
  
  public void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    a(paramInt1, paramInt2, b(paramInt1, paramInt2, paramInt3, paramInt4));
  }
  
  public void a(int paramInt1, int paramInt2, int paramInt3, Interpolator paramInterpolator)
  {
    if (this.e != paramInterpolator)
    {
      this.e = paramInterpolator;
      this.d = aw.a(this.a.getContext(), paramInterpolator);
    }
    RecyclerView.b(this.a, 2);
    this.c = 0;
    this.b = 0;
    this.d.a(0, 0, paramInt1, paramInt2, paramInt3);
    a();
  }
  
  public void b()
  {
    this.a.removeCallbacks(this);
    this.d.h();
  }
  
  public void b(int paramInt1, int paramInt2)
  {
    a(paramInt1, paramInt2, 0, 0);
  }
  
  public void run()
  {
    c();
    RecyclerView.e(this.a);
    aw localaw = this.d;
    cu localcu = RecyclerView.d(this.a).j;
    int i;
    int j;
    int k;
    int m;
    int n;
    int i2;
    int i3;
    int i12;
    int i4;
    int i5;
    int i6;
    int i11;
    if (localaw.g())
    {
      i = localaw.b();
      j = localaw.c();
      k = i - this.b;
      m = j - this.c;
      this.b = i;
      this.c = j;
      cc localcc = RecyclerView.f(this.a);
      n = 0;
      int i1 = 0;
      i2 = 0;
      i3 = 0;
      if (localcc != null)
      {
        this.a.b();
        RecyclerView.g(this.a);
        h.a("RV Scroll");
        i2 = 0;
        i3 = 0;
        if (k != 0)
        {
          i3 = RecyclerView.d(this.a).a(k, this.a.a, this.a.e);
          i2 = k - i3;
        }
        n = 0;
        i1 = 0;
        if (m != 0)
        {
          i1 = RecyclerView.d(this.a).b(m, this.a.a, this.a.e);
          n = m - i1;
        }
        h.a();
        if (RecyclerView.h(this.a))
        {
          int i13 = this.a.c.b();
          for (int i14 = 0; i14 < i13; i14++)
          {
            View localView1 = this.a.c.b(i14);
            cz localcz = this.a.a(localView1);
            if ((localcz != null) && (localcz.h != null))
            {
              View localView2 = localcz.h.a;
              int i15 = localView1.getLeft();
              int i16 = localView1.getTop();
              if ((i15 != localView2.getLeft()) || (i16 != localView2.getTop())) {
                localView2.layout(i15, i16, i15 + localView2.getWidth(), i16 + localView2.getHeight());
              }
            }
          }
        }
        if ((localcu != null) && (!localcu.b()) && (localcu.c()))
        {
          i12 = this.a.e.e();
          if (i12 != 0) {
            break label738;
          }
          localcu.a();
        }
        RecyclerView.i(this.a);
        this.a.a(false);
      }
      i4 = i2;
      i5 = i1;
      if (!RecyclerView.j(this.a).isEmpty()) {
        this.a.invalidate();
      }
      if (bv.a(this.a) != 2) {
        RecyclerView.a(this.a, k, m);
      }
      if ((i4 != 0) || (n != 0))
      {
        i6 = (int)localaw.f();
        if (i4 == i) {
          break label849;
        }
        if (i4 >= 0) {
          break label789;
        }
        i11 = -i6;
      }
    }
    label495:
    label516:
    label645:
    label673:
    label696:
    label738:
    label789:
    label824:
    label830:
    label836:
    label849:
    for (int i7 = i11;; i7 = 0)
    {
      if (n != j) {
        if (n < 0) {
          i6 = -i6;
        }
      }
      for (;;)
      {
        if (bv.a(this.a) != 2) {
          this.a.c(i7, i6);
        }
        if (((i7 != 0) || (i4 == i) || (localaw.d() == 0)) && ((i6 != 0) || (n == j) || (localaw.e() == 0))) {
          localaw.h();
        }
        if ((i3 != 0) || (i5 != 0)) {
          this.a.h(i3, i5);
        }
        if (!RecyclerView.k(this.a)) {
          this.a.invalidate();
        }
        int i8;
        int i9;
        int i10;
        if ((m != 0) && (RecyclerView.d(this.a).d()) && (i5 == m))
        {
          i8 = 1;
          if ((k == 0) || (!RecyclerView.d(this.a).c()) || (i3 != k)) {
            break label824;
          }
          i9 = 1;
          if (((k != 0) || (m != 0)) && (i9 == 0) && (i8 == 0)) {
            break label830;
          }
          i10 = 1;
          if ((!localaw.a()) && (i10 != 0)) {
            break label836;
          }
          RecyclerView.b(this.a, 0);
        }
        for (;;)
        {
          if ((localcu != null) && (localcu.b())) {
            cu.a(localcu, 0, 0);
          }
          d();
          return;
          if (localcu.d() >= i12)
          {
            localcu.a(i12 - 1);
            cu.a(localcu, k - i2, m - n);
            break;
          }
          cu.a(localcu, k - i2, m - n);
          break;
          if (i4 > 0)
          {
            i11 = i6;
            break label495;
          }
          i11 = 0;
          break label495;
          if (n > 0) {
            break label516;
          }
          i6 = 0;
          break label516;
          i8 = 0;
          break label645;
          i9 = 0;
          break label673;
          i10 = 0;
          break label696;
          a();
        }
        i6 = 0;
      }
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/cy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */