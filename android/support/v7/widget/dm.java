package android.support.v7.widget;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class dm
  implements Parcelable.Creator
{
  public StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem a(Parcel paramParcel)
  {
    return new StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem(paramParcel);
  }
  
  public StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem[] a(int paramInt)
  {
    return new StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem[paramInt];
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/dm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */