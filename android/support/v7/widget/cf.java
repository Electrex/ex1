package android.support.v7.widget;

import java.util.ArrayList;

public abstract class cf
{
  private ch a = null;
  private ArrayList b = new ArrayList();
  private long c = 120L;
  private long d = 120L;
  private long e = 250L;
  private long f = 250L;
  private boolean g = true;
  
  public abstract void a();
  
  void a(ch paramch)
  {
    this.a = paramch;
  }
  
  public final void a(cz paramcz, boolean paramBoolean)
  {
    d(paramcz, paramBoolean);
    if (this.a != null) {
      this.a.d(paramcz);
    }
  }
  
  public abstract boolean a(cz paramcz);
  
  public abstract boolean a(cz paramcz, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  public abstract boolean a(cz paramcz1, cz paramcz2, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  public final void b(cz paramcz, boolean paramBoolean)
  {
    c(paramcz, paramBoolean);
  }
  
  public abstract boolean b();
  
  public abstract boolean b(cz paramcz);
  
  public abstract void c();
  
  public abstract void c(cz paramcz);
  
  public void c(cz paramcz, boolean paramBoolean) {}
  
  public long d()
  {
    return this.e;
  }
  
  public final void d(cz paramcz)
  {
    k(paramcz);
    if (this.a != null) {
      this.a.a(paramcz);
    }
  }
  
  public void d(cz paramcz, boolean paramBoolean) {}
  
  public long e()
  {
    return this.c;
  }
  
  public final void e(cz paramcz)
  {
    o(paramcz);
    if (this.a != null) {
      this.a.c(paramcz);
    }
  }
  
  public long f()
  {
    return this.d;
  }
  
  public final void f(cz paramcz)
  {
    m(paramcz);
    if (this.a != null) {
      this.a.b(paramcz);
    }
  }
  
  public long g()
  {
    return this.f;
  }
  
  public final void g(cz paramcz)
  {
    j(paramcz);
  }
  
  public final void h(cz paramcz)
  {
    n(paramcz);
  }
  
  public boolean h()
  {
    return this.g;
  }
  
  public final void i()
  {
    int i = this.b.size();
    for (int j = 0; j < i; j++) {
      ((cg)this.b.get(j)).a();
    }
    this.b.clear();
  }
  
  public final void i(cz paramcz)
  {
    l(paramcz);
  }
  
  public void j(cz paramcz) {}
  
  public void k(cz paramcz) {}
  
  public void l(cz paramcz) {}
  
  public void m(cz paramcz) {}
  
  public void n(cz paramcz) {}
  
  public void o(cz paramcz) {}
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/cf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */