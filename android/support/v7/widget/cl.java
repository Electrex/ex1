package android.support.v7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.a.a;
import android.support.v4.view.a.g;
import android.support.v4.view.a.p;
import android.support.v4.view.a.q;
import android.support.v4.view.bv;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.accessibility.AccessibilityEvent;
import java.util.ArrayList;

public abstract class cl
{
  private boolean a = false;
  private boolean b = false;
  aj h;
  RecyclerView i;
  cu j;
  
  public static int a(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
  {
    int k = 1073741824;
    int m = Math.max(0, paramInt1 - paramInt2);
    if (paramBoolean) {
      if (paramInt3 < 0) {}
    }
    for (;;)
    {
      return View.MeasureSpec.makeMeasureSpec(paramInt3, k);
      k = 0;
      paramInt3 = 0;
      continue;
      if (paramInt3 < 0) {
        if (paramInt3 == -1)
        {
          paramInt3 = m;
        }
        else if (paramInt3 == -2)
        {
          k = Integer.MIN_VALUE;
          paramInt3 = m;
        }
        else
        {
          k = 0;
          paramInt3 = 0;
        }
      }
    }
  }
  
  private void a(int paramInt, View paramView)
  {
    this.h.d(paramInt);
  }
  
  private void a(cq paramcq, int paramInt, View paramView)
  {
    cz localcz = RecyclerView.b(paramView);
    if (localcz.c()) {
      return;
    }
    if ((localcz.n()) && (!localcz.r()) && (!localcz.p()) && (!RecyclerView.f(this.i).b()))
    {
      c(paramInt);
      paramcq.b(localcz);
      return;
    }
    d(paramInt);
    paramcq.c(paramView);
  }
  
  private void a(cu paramcu)
  {
    if (this.j == paramcu) {
      this.j = null;
    }
  }
  
  private void a(View paramView, int paramInt, boolean paramBoolean)
  {
    cz localcz = RecyclerView.b(paramView);
    cm localcm;
    if ((paramBoolean) || (localcz.r()))
    {
      this.i.e.b(paramView);
      localcm = (cm)paramView.getLayoutParams();
      if ((!localcz.k()) && (!localcz.i())) {
        break label126;
      }
      if (!localcz.i()) {
        break label118;
      }
      localcz.j();
      label67:
      this.h.a(paramView, paramInt, paramView.getLayoutParams(), false);
    }
    for (;;)
    {
      if (localcm.d)
      {
        localcz.a.invalidate();
        localcm.d = false;
      }
      return;
      this.i.e.a(paramView);
      break;
      label118:
      localcz.l();
      break label67;
      label126:
      if (paramView.getParent() == this.i)
      {
        int k = this.h.b(paramView);
        if (paramInt == -1) {
          paramInt = this.h.b();
        }
        if (k == -1) {
          throw new IllegalStateException("Added View has RecyclerView as parent but view is not a real child. Unfiltered index:" + this.i.indexOfChild(paramView));
        }
        if (k != paramInt) {
          RecyclerView.d(this.i).a(k, paramInt);
        }
      }
      else
      {
        this.h.a(paramView, paramInt, false);
        localcm.c = true;
        if ((this.j != null) && (this.j.c())) {
          this.j.b(paramView);
        }
      }
    }
  }
  
  public int a(int paramInt, cq paramcq, cw paramcw)
  {
    return 0;
  }
  
  public abstract cm a();
  
  public cm a(Context paramContext, AttributeSet paramAttributeSet)
  {
    return new cm(paramContext, paramAttributeSet);
  }
  
  public cm a(ViewGroup.LayoutParams paramLayoutParams)
  {
    if ((paramLayoutParams instanceof cm)) {
      return new cm((cm)paramLayoutParams);
    }
    if ((paramLayoutParams instanceof ViewGroup.MarginLayoutParams)) {
      return new cm((ViewGroup.MarginLayoutParams)paramLayoutParams);
    }
    return new cm(paramLayoutParams);
  }
  
  public View a(View paramView, int paramInt, cq paramcq, cw paramcw)
  {
    return null;
  }
  
  public void a(int paramInt1, int paramInt2)
  {
    View localView = e(paramInt1);
    if (localView == null) {
      throw new IllegalArgumentException("Cannot move a child from non-existing index:" + paramInt1);
    }
    d(paramInt1);
    c(localView, paramInt2);
  }
  
  public void a(int paramInt, cq paramcq)
  {
    View localView = e(paramInt);
    c(paramInt);
    paramcq.a(localView);
  }
  
  public void a(Parcelable paramParcelable) {}
  
  void a(g paramg)
  {
    a(this.i.a, this.i.e, paramg);
  }
  
  void a(RecyclerView paramRecyclerView)
  {
    if (paramRecyclerView == null)
    {
      this.i = null;
      this.h = null;
      return;
    }
    this.i = paramRecyclerView;
    this.h = paramRecyclerView.c;
  }
  
  public void a(RecyclerView paramRecyclerView, int paramInt1, int paramInt2) {}
  
  public void a(RecyclerView paramRecyclerView, int paramInt1, int paramInt2, int paramInt3) {}
  
  public void a(RecyclerView paramRecyclerView, cq paramcq)
  {
    d(paramRecyclerView);
  }
  
  public void a(cc paramcc1, cc paramcc2) {}
  
  public void a(cq paramcq)
  {
    for (int k = -1 + p(); k >= 0; k--) {
      a(paramcq, k, e(k));
    }
  }
  
  public void a(cq paramcq, cw paramcw)
  {
    Log.e("RecyclerView", "You must override onLayoutChildren(Recycler recycler, State state) ");
  }
  
  public void a(cq paramcq, cw paramcw, int paramInt1, int paramInt2)
  {
    RecyclerView.b(this.i, paramInt1, paramInt2);
  }
  
  public void a(cq paramcq, cw paramcw, g paramg)
  {
    if ((bv.b(this.i, -1)) || (bv.a(this.i, -1)))
    {
      paramg.a(8192);
      paramg.i(true);
    }
    if ((bv.b(this.i, 1)) || (bv.a(this.i, 1)))
    {
      paramg.a(4096);
      paramg.i(true);
    }
    paramg.b(p.a(c(paramcq, paramcw), d(paramcq, paramcw), e(paramcq, paramcw), b(paramcq, paramcw)));
  }
  
  public void a(cq paramcq, cw paramcw, View paramView, g paramg)
  {
    int k;
    if (d())
    {
      k = d(paramView);
      if (!c()) {
        break label51;
      }
    }
    label51:
    for (int m = d(paramView);; m = 0)
    {
      paramg.c(q.a(k, 1, m, 1, false, false));
      return;
      k = 0;
      break;
    }
  }
  
  public void a(cq paramcq, cw paramcw, AccessibilityEvent paramAccessibilityEvent)
  {
    int k = 1;
    android.support.v4.view.a.aj localaj = a.a(paramAccessibilityEvent);
    if ((this.i == null) || (localaj == null)) {
      return;
    }
    if ((bv.b(this.i, k)) || (bv.b(this.i, -1)) || (bv.a(this.i, -1)) || (bv.a(this.i, k))) {}
    for (;;)
    {
      localaj.a(k);
      if (RecyclerView.f(this.i) == null) {
        break;
      }
      localaj.a(RecyclerView.f(this.i).a());
      return;
      int m = 0;
    }
  }
  
  public void a(View paramView)
  {
    a(paramView, -1);
  }
  
  public void a(View paramView, int paramInt)
  {
    a(paramView, paramInt, true);
  }
  
  public void a(View paramView, int paramInt1, int paramInt2)
  {
    cm localcm = (cm)paramView.getLayoutParams();
    Rect localRect = this.i.h(paramView);
    int k = paramInt1 + (localRect.left + localRect.right);
    int m = paramInt2 + (localRect.top + localRect.bottom);
    paramView.measure(a(q(), k + (s() + u() + localcm.leftMargin + localcm.rightMargin), localcm.width, c()), a(r(), m + (t() + v() + localcm.topMargin + localcm.bottomMargin), localcm.height, d()));
  }
  
  public void a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    Rect localRect = ((cm)paramView.getLayoutParams()).b;
    paramView.layout(paramInt1 + localRect.left, paramInt2 + localRect.top, paramInt3 - localRect.right, paramInt4 - localRect.bottom);
  }
  
  public void a(View paramView, int paramInt, cm paramcm)
  {
    cz localcz = RecyclerView.b(paramView);
    if (localcz.r()) {
      this.i.e.b(paramView);
    }
    for (;;)
    {
      this.h.a(paramView, paramInt, paramcm, localcz.r());
      return;
      this.i.e.a(paramView);
    }
  }
  
  public void a(View paramView, Rect paramRect)
  {
    if (this.i == null)
    {
      paramRect.set(0, 0, 0, 0);
      return;
    }
    paramRect.set(this.i.h(paramView));
  }
  
  void a(View paramView, g paramg)
  {
    cz localcz = RecyclerView.b(paramView);
    if ((localcz != null) && (!localcz.r())) {
      a(this.i.a, this.i.e, paramView, paramg);
    }
  }
  
  public void a(View paramView, cq paramcq)
  {
    c(paramView);
    paramcq.a(paramView);
  }
  
  public void a(AccessibilityEvent paramAccessibilityEvent)
  {
    a(this.i.a, this.i.e, paramAccessibilityEvent);
  }
  
  public void a(Runnable paramRunnable)
  {
    if (this.i != null) {
      bv.a(this.i, paramRunnable);
    }
  }
  
  public void a(String paramString)
  {
    if (this.i != null) {
      this.i.a(paramString);
    }
  }
  
  boolean a(int paramInt, Bundle paramBundle)
  {
    return a(this.i.a, this.i.e, paramInt, paramBundle);
  }
  
  public boolean a(RecyclerView paramRecyclerView, cw paramcw, View paramView1, View paramView2)
  {
    return a(paramRecyclerView, paramView1, paramView2);
  }
  
  public boolean a(RecyclerView paramRecyclerView, View paramView, Rect paramRect, boolean paramBoolean)
  {
    int k = s();
    int m = t();
    int n = q() - u();
    int i1 = r() - v();
    int i2 = paramView.getLeft() + paramRect.left;
    int i3 = paramView.getTop() + paramRect.top;
    int i4 = i2 + paramRect.width();
    int i5 = i3 + paramRect.height();
    int i6 = Math.min(0, i2 - k);
    int i7 = Math.min(0, i3 - m);
    int i8 = Math.max(0, i4 - n);
    int i9 = Math.max(0, i5 - i1);
    int i11;
    int i12;
    if (n() == 1) {
      if (i8 != 0)
      {
        i11 = i8;
        if (i7 == 0) {
          break label215;
        }
        i12 = i7;
        label144:
        if ((i11 == 0) && (i12 == 0)) {
          break label241;
        }
        if (!paramBoolean) {
          break label230;
        }
        paramRecyclerView.scrollBy(i11, i12);
      }
    }
    for (;;)
    {
      return true;
      i8 = Math.max(i6, i4 - n);
      break;
      if (i6 != 0) {}
      for (int i10 = i6;; i10 = Math.min(i2 - k, i8))
      {
        i11 = i10;
        break;
      }
      label215:
      i12 = Math.min(i3 - m, i9);
      break label144;
      label230:
      paramRecyclerView.a(i11, i12);
    }
    label241:
    return false;
  }
  
  @Deprecated
  public boolean a(RecyclerView paramRecyclerView, View paramView1, View paramView2)
  {
    return (m()) || (paramRecyclerView.i());
  }
  
  public boolean a(RecyclerView paramRecyclerView, ArrayList paramArrayList, int paramInt1, int paramInt2)
  {
    return false;
  }
  
  public boolean a(cm paramcm)
  {
    return paramcm != null;
  }
  
  public boolean a(cq paramcq, cw paramcw, int paramInt, Bundle paramBundle)
  {
    if (this.i == null) {}
    int n;
    int m;
    do
    {
      return false;
      switch (paramInt)
      {
      default: 
        n = 0;
        m = 0;
      }
    } while ((m == 0) && (n == 0));
    this.i.scrollBy(n, m);
    return true;
    if (bv.b(this.i, -1)) {}
    for (int k = -(r() - t() - v());; k = 0)
    {
      if (bv.a(this.i, -1))
      {
        int i2 = -(q() - s() - u());
        m = k;
        n = i2;
        break;
        if (!bv.b(this.i, 1)) {
          break label207;
        }
      }
      label207:
      for (k = r() - t() - v();; k = 0)
      {
        if (bv.a(this.i, 1))
        {
          int i1 = q() - s() - u();
          m = k;
          n = i1;
          break;
        }
        m = k;
        n = 0;
        break;
      }
    }
  }
  
  public boolean a(cq paramcq, cw paramcw, View paramView, int paramInt, Bundle paramBundle)
  {
    return false;
  }
  
  boolean a(View paramView, int paramInt, Bundle paramBundle)
  {
    return a(this.i.a, this.i.e, paramView, paramInt, paramBundle);
  }
  
  public int b(int paramInt, cq paramcq, cw paramcw)
  {
    return 0;
  }
  
  public int b(cq paramcq, cw paramcw)
  {
    return 0;
  }
  
  public int b(cw paramcw)
  {
    return 0;
  }
  
  public Parcelable b()
  {
    return null;
  }
  
  public View b(int paramInt)
  {
    int k = p();
    int m = 0;
    if (m < k)
    {
      View localView = e(m);
      cz localcz = RecyclerView.b(localView);
      if (localcz == null) {}
      while ((localcz.e() != paramInt) || (localcz.c()) || ((!this.i.e.a()) && (localcz.r())))
      {
        m++;
        break;
      }
      return localView;
    }
    return null;
  }
  
  void b(RecyclerView paramRecyclerView)
  {
    this.b = true;
    c(paramRecyclerView);
  }
  
  public void b(RecyclerView paramRecyclerView, int paramInt1, int paramInt2) {}
  
  void b(RecyclerView paramRecyclerView, cq paramcq)
  {
    this.b = false;
    a(paramRecyclerView, paramcq);
  }
  
  void b(cq paramcq)
  {
    int k = paramcq.d();
    int m = k - 1;
    if (m >= 0)
    {
      View localView = paramcq.d(m);
      cz localcz = RecyclerView.b(localView);
      if (localcz.c()) {}
      for (;;)
      {
        m--;
        break;
        if (localcz.s()) {
          this.i.removeDetachedView(localView, false);
        }
        paramcq.b(localView);
      }
    }
    paramcq.e();
    if (k > 0) {
      this.i.invalidate();
    }
  }
  
  public void b(View paramView)
  {
    b(paramView, -1);
  }
  
  public void b(View paramView, int paramInt)
  {
    a(paramView, paramInt, false);
  }
  
  public boolean b(Runnable paramRunnable)
  {
    if (this.i != null) {
      return this.i.removeCallbacks(paramRunnable);
    }
    return false;
  }
  
  public int c(cq paramcq, cw paramcw)
  {
    if ((this.i == null) || (RecyclerView.f(this.i) == null)) {}
    while (!d()) {
      return 1;
    }
    return RecyclerView.f(this.i).a();
  }
  
  public int c(cw paramcw)
  {
    return 0;
  }
  
  public void c(int paramInt)
  {
    if (e(paramInt) != null) {
      this.h.a(paramInt);
    }
  }
  
  public void c(RecyclerView paramRecyclerView) {}
  
  public void c(RecyclerView paramRecyclerView, int paramInt1, int paramInt2) {}
  
  public void c(cq paramcq)
  {
    for (int k = -1 + p(); k >= 0; k--) {
      if (!RecyclerView.b(e(k)).c()) {
        a(k, paramcq);
      }
    }
  }
  
  public void c(View paramView)
  {
    this.h.a(paramView);
  }
  
  public void c(View paramView, int paramInt)
  {
    a(paramView, paramInt, (cm)paramView.getLayoutParams());
  }
  
  public boolean c()
  {
    return false;
  }
  
  public int d(cq paramcq, cw paramcw)
  {
    if ((this.i == null) || (RecyclerView.f(this.i) == null)) {}
    while (!c()) {
      return 1;
    }
    return RecyclerView.f(this.i).a();
  }
  
  public int d(cw paramcw)
  {
    return 0;
  }
  
  public int d(View paramView)
  {
    return ((cm)paramView.getLayoutParams()).c();
  }
  
  public View d(View paramView, int paramInt)
  {
    return null;
  }
  
  public void d(int paramInt)
  {
    a(paramInt, e(paramInt));
  }
  
  @Deprecated
  public void d(RecyclerView paramRecyclerView) {}
  
  public boolean d()
  {
    return false;
  }
  
  public int e(cw paramcw)
  {
    return 0;
  }
  
  public int e(View paramView)
  {
    Rect localRect = ((cm)paramView.getLayoutParams()).b;
    return paramView.getMeasuredWidth() + localRect.left + localRect.right;
  }
  
  public View e(int paramInt)
  {
    if (this.h != null) {
      return this.h.b(paramInt);
    }
    return null;
  }
  
  public void e(RecyclerView paramRecyclerView) {}
  
  public boolean e(cq paramcq, cw paramcw)
  {
    return false;
  }
  
  public int f(cw paramcw)
  {
    return 0;
  }
  
  public int f(View paramView)
  {
    Rect localRect = ((cm)paramView.getLayoutParams()).b;
    return paramView.getMeasuredHeight() + localRect.top + localRect.bottom;
  }
  
  public void f(int paramInt)
  {
    if (this.i != null) {
      this.i.b(paramInt);
    }
  }
  
  public int g(cw paramcw)
  {
    return 0;
  }
  
  public int g(View paramView)
  {
    return paramView.getLeft() - m(paramView);
  }
  
  public void g(int paramInt)
  {
    if (this.i != null) {
      this.i.a(paramInt);
    }
  }
  
  public int h(View paramView)
  {
    return paramView.getTop() - k(paramView);
  }
  
  public void h(int paramInt) {}
  
  public int i(View paramView)
  {
    return paramView.getRight() + n(paramView);
  }
  
  public int j(View paramView)
  {
    return paramView.getBottom() + l(paramView);
  }
  
  public boolean j()
  {
    return false;
  }
  
  public int k(View paramView)
  {
    return ((cm)paramView.getLayoutParams()).b.top;
  }
  
  public void k()
  {
    if (this.i != null) {
      this.i.requestLayout();
    }
  }
  
  public int l(View paramView)
  {
    return ((cm)paramView.getLayoutParams()).b.bottom;
  }
  
  public boolean l()
  {
    return this.b;
  }
  
  public int m(View paramView)
  {
    return ((cm)paramView.getLayoutParams()).b.left;
  }
  
  public boolean m()
  {
    return (this.j != null) && (this.j.c());
  }
  
  public int n()
  {
    return bv.h(this.i);
  }
  
  public int n(View paramView)
  {
    return ((cm)paramView.getLayoutParams()).b.right;
  }
  
  public int o()
  {
    return -1;
  }
  
  public int p()
  {
    if (this.h != null) {
      return this.h.b();
    }
    return 0;
  }
  
  public int q()
  {
    if (this.i != null) {
      return this.i.getWidth();
    }
    return 0;
  }
  
  public int r()
  {
    if (this.i != null) {
      return this.i.getHeight();
    }
    return 0;
  }
  
  public int s()
  {
    if (this.i != null) {
      return this.i.getPaddingLeft();
    }
    return 0;
  }
  
  public int t()
  {
    if (this.i != null) {
      return this.i.getPaddingTop();
    }
    return 0;
  }
  
  public int u()
  {
    if (this.i != null) {
      return this.i.getPaddingRight();
    }
    return 0;
  }
  
  public int v()
  {
    if (this.i != null) {
      return this.i.getPaddingBottom();
    }
    return 0;
  }
  
  public View w()
  {
    if (this.i == null) {}
    View localView;
    do
    {
      return null;
      localView = this.i.getFocusedChild();
    } while ((localView == null) || (this.h.c(localView)));
    return localView;
  }
  
  void x()
  {
    if (this.j != null) {
      this.j.a();
    }
  }
  
  public void y()
  {
    this.a = true;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/cl.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */