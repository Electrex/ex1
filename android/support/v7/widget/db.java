package android.support.v7.widget;

import android.os.Bundle;
import android.support.v4.view.a;
import android.support.v4.view.a.g;
import android.view.View;

class db
  extends a
{
  db(da paramda) {}
  
  public void a(View paramView, g paramg)
  {
    super.a(paramView, paramg);
    if ((!da.a(this.b)) && (this.b.b.getLayoutManager() != null)) {
      this.b.b.getLayoutManager().a(paramView, paramg);
    }
  }
  
  public boolean a(View paramView, int paramInt, Bundle paramBundle)
  {
    if (super.a(paramView, paramInt, paramBundle)) {
      return true;
    }
    if ((!da.a(this.b)) && (this.b.b.getLayoutManager() != null)) {
      return this.b.b.getLayoutManager().a(paramView, paramInt, paramBundle);
    }
    return false;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/db.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */