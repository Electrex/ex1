package android.support.v7.widget;

import android.view.View;
import android.view.ViewGroup.LayoutParams;

class ca
  implements al
{
  ca(RecyclerView paramRecyclerView) {}
  
  public int a()
  {
    return this.a.getChildCount();
  }
  
  public int a(View paramView)
  {
    return this.a.indexOfChild(paramView);
  }
  
  public void a(int paramInt)
  {
    View localView = this.a.getChildAt(paramInt);
    if (localView != null) {
      RecyclerView.b(this.a, localView);
    }
    this.a.removeViewAt(paramInt);
  }
  
  public void a(View paramView, int paramInt)
  {
    this.a.addView(paramView, paramInt);
    RecyclerView.a(this.a, paramView);
  }
  
  public void a(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams)
  {
    cz localcz = RecyclerView.b(paramView);
    if (localcz != null)
    {
      if ((!localcz.s()) && (!localcz.c())) {
        throw new IllegalArgumentException("Called attach on a child which is not detached: " + localcz);
      }
      localcz.m();
    }
    RecyclerView.a(this.a, paramView, paramInt, paramLayoutParams);
  }
  
  public cz b(View paramView)
  {
    return RecyclerView.b(paramView);
  }
  
  public View b(int paramInt)
  {
    return this.a.getChildAt(paramInt);
  }
  
  public void b()
  {
    int i = a();
    for (int j = 0; j < i; j++) {
      RecyclerView.b(this.a, b(j));
    }
    this.a.removeAllViews();
  }
  
  public void c(int paramInt)
  {
    View localView = b(paramInt);
    if (localView != null)
    {
      cz localcz = RecyclerView.b(localView);
      if (localcz != null)
      {
        if ((localcz.s()) && (!localcz.c())) {
          throw new IllegalArgumentException("called detach on an already detached child " + localcz);
        }
        localcz.b(256);
      }
    }
    RecyclerView.a(this.a, paramInt);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/ca.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */