package android.support.v7.widget;

import android.util.Log;
import android.view.animation.Interpolator;

public class cv
{
  private int a;
  private int b;
  private int c;
  private Interpolator d;
  private boolean e;
  private int f;
  
  private void a()
  {
    if ((this.d != null) && (this.c < 1)) {
      throw new IllegalStateException("If you provide an interpolator, you must set a positive duration");
    }
    if (this.c < 1) {
      throw new IllegalStateException("Scroll duration must be a positive number");
    }
  }
  
  private void a(RecyclerView paramRecyclerView)
  {
    if (this.e)
    {
      a();
      if (this.d == null) {
        if (this.c == Integer.MIN_VALUE) {
          RecyclerView.t(paramRecyclerView).b(this.a, this.b);
        }
      }
      for (;;)
      {
        this.f = (1 + this.f);
        if (this.f > 10) {
          Log.e("RecyclerView", "Smooth Scroll action is being updated too frequently. Make sure you are not changing it unless necessary");
        }
        this.e = false;
        return;
        RecyclerView.t(paramRecyclerView).a(this.a, this.b, this.c);
        continue;
        RecyclerView.t(paramRecyclerView).a(this.a, this.b, this.c, this.d);
      }
    }
    this.f = 0;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/cv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */