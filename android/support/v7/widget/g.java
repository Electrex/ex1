package android.support.v7.widget;

import android.content.Context;
import android.support.v7.a.b;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.v;
import android.view.View;

class g
  extends v
{
  public g(ActionMenuPresenter paramActionMenuPresenter, Context paramContext, i parami, View paramView, boolean paramBoolean)
  {
    super(paramContext, parami, paramView, paramBoolean, b.actionOverflowMenuStyle);
    a(8388613);
    a(paramActionMenuPresenter.g);
  }
  
  public void onDismiss()
  {
    super.onDismiss();
    ActionMenuPresenter.c(this.c).close();
    ActionMenuPresenter.a(this.c, null);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */