package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.support.v7.a.b;
import android.support.v7.internal.widget.bc;
import android.support.v7.internal.widget.be;
import android.support.v7.internal.widget.bf;
import android.support.v7.internal.widget.bh;
import android.util.AttributeSet;
import android.widget.EditText;

public class w
  extends EditText
{
  private static final int[] a = { 16842964 };
  private be b;
  
  public w(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, b.editTextStyle);
  }
  
  public w(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(bc.a(paramContext), paramAttributeSet, paramInt);
    if (bf.a)
    {
      bh localbh = bh.a(getContext(), paramAttributeSet, a, paramInt, 0);
      if (localbh.e(0))
      {
        ColorStateList localColorStateList = localbh.c().c(localbh.f(0, -1));
        if (localColorStateList != null) {
          setSupportBackgroundTintList(localColorStateList);
        }
      }
      localbh.b();
    }
  }
  
  private void a()
  {
    if ((getBackground() != null) && (this.b != null)) {
      bf.a(this, this.b);
    }
  }
  
  protected void drawableStateChanged()
  {
    super.drawableStateChanged();
    a();
  }
  
  public ColorStateList getSupportBackgroundTintList()
  {
    if (this.b != null) {
      return this.b.a;
    }
    return null;
  }
  
  public PorterDuff.Mode getSupportBackgroundTintMode()
  {
    if (this.b != null) {
      return this.b.b;
    }
    return null;
  }
  
  public void setSupportBackgroundTintList(ColorStateList paramColorStateList)
  {
    if (this.b == null) {
      this.b = new be();
    }
    this.b.a = paramColorStateList;
    this.b.d = true;
    a();
  }
  
  public void setSupportBackgroundTintMode(PorterDuff.Mode paramMode)
  {
    if (this.b == null) {
      this.b = new be();
    }
    this.b.b = paramMode;
    this.b.c = true;
    a();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/w.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */