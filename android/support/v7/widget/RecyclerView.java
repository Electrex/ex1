package android.support.v7.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.os.Parcelable;
import android.support.v4.d.h;
import android.support.v4.view.ba;
import android.support.v4.view.bp;
import android.support.v4.view.bq;
import android.support.v4.view.bv;
import android.support.v4.widget.z;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.FocusFinder;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.List;

public class RecyclerView
  extends ViewGroup
  implements bp
{
  private static final Interpolator ae;
  private static final boolean h;
  private final boolean A;
  private final AccessibilityManager B;
  private boolean C = false;
  private int D = 0;
  private z E;
  private z F;
  private z G;
  private z H;
  private int I = 0;
  private int J = -1;
  private VelocityTracker K;
  private int L;
  private int M;
  private int N;
  private int O;
  private int P;
  private final int Q;
  private final int R;
  private float S = Float.MIN_VALUE;
  private final cy T = new cy(this);
  private co U;
  private List V;
  private ch W = new ci(this, null);
  final cq a = new cq(this);
  private boolean aa = false;
  private da ab;
  private final int[] ac = new int[2];
  private Runnable ad = new by(this);
  p b;
  aj c;
  cf d = new am();
  final cw e = new cw();
  boolean f = false;
  boolean g = false;
  private final cs i = new cs(this, null);
  private RecyclerView.SavedState j;
  private boolean k;
  private final Runnable l = new bx(this);
  private final Rect m = new Rect();
  private cc n;
  private cl o;
  private cr p;
  private final ArrayList q = new ArrayList();
  private final ArrayList r = new ArrayList();
  private cn s;
  private boolean t;
  private boolean u;
  private boolean v;
  private boolean w;
  private boolean x;
  private int y;
  private boolean z;
  
  static
  {
    if ((Build.VERSION.SDK_INT == 18) || (Build.VERSION.SDK_INT == 19) || (Build.VERSION.SDK_INT == 20)) {}
    for (boolean bool = true;; bool = false)
    {
      h = bool;
      ae = new bz();
      return;
    }
  }
  
  public RecyclerView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public RecyclerView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public RecyclerView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    setFocusableInTouchMode(true);
    if (Build.VERSION.SDK_INT >= 16) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      this.A = bool1;
      ViewConfiguration localViewConfiguration = ViewConfiguration.get(paramContext);
      this.P = localViewConfiguration.getScaledTouchSlop();
      this.Q = localViewConfiguration.getScaledMinimumFlingVelocity();
      this.R = localViewConfiguration.getScaledMaximumFlingVelocity();
      int i1 = bv.a(this);
      boolean bool2 = false;
      if (i1 == 2) {
        bool2 = true;
      }
      setWillNotDraw(bool2);
      this.d.a(this.W);
      a();
      s();
      if (bv.e(this) == 0) {
        bv.c(this, 1);
      }
      this.B = ((AccessibilityManager)getContext().getSystemService("accessibility"));
      setAccessibilityDelegateCompat(new da(this));
      return;
    }
  }
  
  private boolean A()
  {
    return (this.d != null) && (this.d.h());
  }
  
  private void B()
  {
    if ((!this.aa) && (this.t))
    {
      bv.a(this, this.ad);
      this.aa = true;
    }
  }
  
  private boolean C()
  {
    return (this.d != null) && (this.o.j());
  }
  
  private void D()
  {
    boolean bool1 = true;
    if (this.C)
    {
      this.b.a();
      o();
      this.o.e(this);
    }
    boolean bool2;
    label89:
    boolean bool3;
    label149:
    cw localcw2;
    if ((this.d != null) && (this.o.j()))
    {
      this.b.b();
      if (((!this.f) || (this.g)) && (!this.f) && ((!this.g) || (!A()))) {
        break label208;
      }
      bool2 = bool1;
      cw localcw1 = this.e;
      if ((!this.v) || (this.d == null) || ((!this.C) && (!bool2) && (!cl.a(this.o))) || ((this.C) && (!this.n.b()))) {
        break label213;
      }
      bool3 = bool1;
      cw.c(localcw1, bool3);
      localcw2 = this.e;
      if ((!cw.b(this.e)) || (!bool2) || (this.C) || (!C())) {
        break label219;
      }
    }
    for (;;)
    {
      cw.d(localcw2, bool1);
      return;
      this.b.e();
      break;
      label208:
      bool2 = false;
      break label89;
      label213:
      bool3 = false;
      break label149;
      label219:
      bool1 = false;
    }
  }
  
  private void E()
  {
    if (this.C) {
      return;
    }
    this.C = true;
    int i1 = this.c.c();
    for (int i2 = 0; i2 < i1; i2++)
    {
      cz localcz = b(this.c.c(i2));
      if ((localcz != null) && (!localcz.c())) {
        localcz.b(512);
      }
    }
    this.a.g();
  }
  
  private void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i1;
    if (paramInt2 < 0)
    {
      d();
      if (!this.E.a(-paramInt2 / getWidth(), 1.0F - paramInt3 / getHeight())) {
        i1 = 0;
      }
    }
    for (;;)
    {
      int i2;
      if (paramInt4 < 0)
      {
        f();
        if (!this.F.a(-paramInt4 / getHeight(), paramInt1 / getWidth()))
        {
          i2 = 0;
          if (i1 == 0) {}
        }
        else
        {
          i2 = 1;
        }
      }
      for (;;)
      {
        if ((i2 != 0) || (paramInt2 != 0) || (paramInt4 != 0)) {
          bv.d(this);
        }
        return;
        i1 = 1;
        break;
        if (paramInt2 <= 0) {
          break label218;
        }
        e();
        if (!this.G.a(paramInt2 / getWidth(), paramInt3 / getHeight()))
        {
          i1 = 0;
          break;
        }
        i1 = 1;
        break;
        if (paramInt4 > 0)
        {
          g();
          if (!this.H.a(paramInt4 / getHeight(), 1.0F - paramInt1 / getWidth()))
          {
            i2 = 0;
            if (i1 == 0) {}
          }
          else
          {
            i2 = 1;
          }
        }
        else
        {
          i2 = i1;
        }
      }
      label218:
      i1 = 0;
    }
  }
  
  private void a(android.support.v4.f.a parama)
  {
    List localList = this.e.d;
    int i1 = -1 + localList.size();
    if (i1 >= 0)
    {
      View localView = (View)localList.get(i1);
      cz localcz = b(localView);
      ck localck = (ck)this.e.a.remove(localcz);
      if (!this.e.a()) {
        this.e.b.remove(localcz);
      }
      if (parama.remove(localView) != null) {
        this.o.a(localView, this.a);
      }
      for (;;)
      {
        i1--;
        break;
        if (localck != null) {
          a(localck);
        } else {
          a(new ck(localcz, localView.getLeft(), localView.getTop(), localView.getRight(), localView.getBottom()));
        }
      }
    }
    localList.clear();
  }
  
  private void a(cc paramcc, boolean paramBoolean1, boolean paramBoolean2)
  {
    if (this.n != null)
    {
      this.n.b(this.i);
      this.n.b(this);
    }
    if ((!paramBoolean1) || (paramBoolean2))
    {
      if (this.d != null) {
        this.d.c();
      }
      if (this.o != null)
      {
        this.o.c(this.a);
        this.o.b(this.a);
      }
      this.a.a();
    }
    this.b.a();
    cc localcc = this.n;
    this.n = paramcc;
    if (paramcc != null)
    {
      paramcc.a(this.i);
      paramcc.a(this);
    }
    if (this.o != null) {
      this.o.a(localcc, this.n);
    }
    this.a.a(localcc, this.n, paramBoolean1);
    cw.a(this.e, true);
    o();
  }
  
  private void a(ck paramck)
  {
    View localView = paramck.a.a;
    b(paramck.a);
    int i1 = paramck.b;
    int i2 = paramck.c;
    int i3 = localView.getLeft();
    int i4 = localView.getTop();
    if ((i1 != i3) || (i2 != i4))
    {
      paramck.a.a(false);
      localView.layout(i3, i4, i3 + localView.getWidth(), i4 + localView.getHeight());
      if (this.d.a(paramck.a, i1, i2, i3, i4)) {
        B();
      }
    }
    do
    {
      return;
      paramck.a.a(false);
    } while (!this.d.a(paramck.a));
    B();
  }
  
  private void a(cz paramcz, Rect paramRect, int paramInt1, int paramInt2)
  {
    if ((paramRect != null) && ((paramRect.left != paramInt1) || (paramRect.top != paramInt2)))
    {
      paramcz.a(false);
      if (this.d.a(paramcz, paramRect.left, paramRect.top, paramInt1, paramInt2)) {
        B();
      }
    }
    do
    {
      return;
      paramcz.a(false);
    } while (!this.d.b(paramcz));
    B();
  }
  
  private void a(cz paramcz1, cz paramcz2)
  {
    paramcz1.a(false);
    b(paramcz1);
    paramcz1.g = paramcz2;
    this.a.d(paramcz1);
    int i1 = paramcz1.a.getLeft();
    int i2 = paramcz1.a.getTop();
    int i3;
    int i4;
    if ((paramcz2 == null) || (paramcz2.c()))
    {
      i3 = i2;
      i4 = i1;
    }
    for (;;)
    {
      if (this.d.a(paramcz1, paramcz2, i1, i2, i4, i3)) {
        B();
      }
      return;
      i4 = paramcz2.a.getLeft();
      i3 = paramcz2.a.getTop();
      paramcz2.a(false);
      paramcz2.h = paramcz1;
    }
  }
  
  private void a(int[] paramArrayOfInt)
  {
    int i1 = this.c.b();
    if (i1 == 0)
    {
      paramArrayOfInt[0] = 0;
      paramArrayOfInt[1] = 0;
      return;
    }
    int i2 = Integer.MAX_VALUE;
    int i3 = Integer.MIN_VALUE;
    int i4 = 0;
    cz localcz;
    int i6;
    if (i4 < i1)
    {
      localcz = b(this.c.b(i4));
      if (localcz.c()) {
        i6 = i2;
      }
    }
    for (;;)
    {
      i4++;
      i2 = i6;
      break;
      int i5 = localcz.e();
      if (i5 < i2) {
        i2 = i5;
      }
      if (i5 > i3)
      {
        i3 = i5;
        i6 = i2;
        continue;
        paramArrayOfInt[0] = i2;
        paramArrayOfInt[1] = i3;
      }
      else
      {
        i6 = i2;
      }
    }
  }
  
  private boolean a(MotionEvent paramMotionEvent)
  {
    int i1 = paramMotionEvent.getAction();
    if ((i1 == 3) || (i1 == 0)) {
      this.s = null;
    }
    int i2 = this.r.size();
    for (int i3 = 0; i3 < i2; i3++)
    {
      cn localcn = (cn)this.r.get(i3);
      if ((localcn.a(this, paramMotionEvent)) && (i1 != 3))
      {
        this.s = localcn;
        return true;
      }
    }
    return false;
  }
  
  static cz b(View paramView)
  {
    if (paramView == null) {
      return null;
    }
    return ((cm)paramView.getLayoutParams()).a;
  }
  
  private void b(cz paramcz)
  {
    View localView = paramcz.a;
    if (localView.getParent() == this) {}
    for (int i1 = 1;; i1 = 0)
    {
      this.a.d(a(localView));
      if (!paramcz.s()) {
        break;
      }
      this.c.a(localView, -1, localView.getLayoutParams(), true);
      return;
    }
    if (i1 == 0)
    {
      this.c.a(localView, true);
      return;
    }
    this.c.d(localView);
  }
  
  private boolean b(MotionEvent paramMotionEvent)
  {
    int i1 = paramMotionEvent.getAction();
    int i2;
    if (this.s != null)
    {
      if (i1 == 0) {
        this.s = null;
      }
    }
    else
    {
      if (i1 == 0) {
        break label110;
      }
      i2 = this.r.size();
    }
    for (int i3 = 0; i3 < i2; i3++)
    {
      cn localcn = (cn)this.r.get(i3);
      if (localcn.a(this, paramMotionEvent))
      {
        this.s = localcn;
        return true;
        this.s.b(this, paramMotionEvent);
        if ((i1 == 3) || (i1 == 1)) {
          this.s = null;
        }
        return true;
      }
    }
    label110:
    return false;
  }
  
  private int c(cz paramcz)
  {
    if ((paramcz.a(524)) || (!paramcz.q())) {
      return -1;
    }
    return this.b.b(paramcz.b);
  }
  
  private void c(MotionEvent paramMotionEvent)
  {
    int i1 = ba.b(paramMotionEvent);
    if (ba.b(paramMotionEvent, i1) == this.J) {
      if (i1 != 0) {
        break label81;
      }
    }
    label81:
    for (int i2 = 1;; i2 = 0)
    {
      this.J = ba.b(paramMotionEvent, i2);
      int i3 = (int)(0.5F + ba.c(paramMotionEvent, i2));
      this.N = i3;
      this.L = i3;
      int i4 = (int)(0.5F + ba.d(paramMotionEvent, i2));
      this.O = i4;
      this.M = i4;
      return;
    }
  }
  
  private float getScrollFactor()
  {
    if (this.S == Float.MIN_VALUE)
    {
      TypedValue localTypedValue = new TypedValue();
      if (getContext().getTheme().resolveAttribute(16842829, localTypedValue, true)) {
        this.S = localTypedValue.getDimension(getContext().getResources().getDisplayMetrics());
      }
    }
    else
    {
      return this.S;
    }
    return 0.0F;
  }
  
  private void i(int paramInt1, int paramInt2)
  {
    z localz = this.E;
    boolean bool1 = false;
    if (localz != null)
    {
      boolean bool2 = this.E.a();
      bool1 = false;
      if (!bool2)
      {
        bool1 = false;
        if (paramInt1 > 0) {
          bool1 = this.E.c();
        }
      }
    }
    if ((this.G != null) && (!this.G.a()) && (paramInt1 < 0)) {
      bool1 |= this.G.c();
    }
    if ((this.F != null) && (!this.F.a()) && (paramInt2 > 0)) {
      bool1 |= this.F.c();
    }
    if ((this.H != null) && (!this.H.a()) && (paramInt2 < 0)) {
      bool1 |= this.H.c();
    }
    if (bool1) {
      bv.d(this);
    }
  }
  
  private boolean i(View paramView)
  {
    b();
    boolean bool = this.c.e(paramView);
    if (bool)
    {
      cz localcz = b(paramView);
      this.a.d(localcz);
      this.a.b(localcz);
    }
    a(false);
    return bool;
  }
  
  private void j(int paramInt1, int paramInt2)
  {
    int i1 = View.MeasureSpec.getMode(paramInt1);
    int i2 = View.MeasureSpec.getMode(paramInt2);
    int i3 = View.MeasureSpec.getSize(paramInt1);
    int i4 = View.MeasureSpec.getSize(paramInt2);
    switch (i1)
    {
    default: 
      i3 = bv.n(this);
    }
    switch (i2)
    {
    default: 
      i4 = bv.o(this);
    }
    setMeasuredDimension(i3, i4);
  }
  
  private void j(View paramView)
  {
    if (this.n != null) {
      this.n.d(b(paramView));
    }
    g(paramView);
  }
  
  private void k(View paramView)
  {
    if (this.n != null) {
      this.n.c(b(paramView));
    }
    f(paramView);
  }
  
  private boolean k(int paramInt1, int paramInt2)
  {
    int i1 = this.c.b();
    boolean bool;
    if (i1 == 0) {
      if (paramInt1 == 0)
      {
        bool = false;
        if (paramInt2 == 0) {}
      }
      else
      {
        bool = true;
      }
    }
    int i2;
    do
    {
      return bool;
      i2 = 0;
      bool = false;
    } while (i2 >= i1);
    cz localcz = b(this.c.b(i2));
    if (localcz.c()) {}
    int i3;
    do
    {
      i2++;
      break;
      i3 = localcz.e();
    } while ((i3 >= paramInt1) && (i3 <= paramInt2));
    return true;
  }
  
  private void s()
  {
    this.c = new aj(new ca(this));
  }
  
  private void setScrollState(int paramInt)
  {
    if (paramInt == this.I) {
      return;
    }
    this.I = paramInt;
    if (paramInt != 2) {
      u();
    }
    d(paramInt);
  }
  
  private void t()
  {
    this.l.run();
  }
  
  private void u()
  {
    this.T.b();
    if (this.o != null) {
      this.o.x();
    }
  }
  
  private void v()
  {
    z localz = this.E;
    boolean bool = false;
    if (localz != null) {
      bool = this.E.c();
    }
    if (this.F != null) {
      bool |= this.F.c();
    }
    if (this.G != null) {
      bool |= this.G.c();
    }
    if (this.H != null) {
      bool |= this.H.c();
    }
    if (bool) {
      bv.d(this);
    }
  }
  
  private void w()
  {
    if (this.K != null) {
      this.K.clear();
    }
    v();
    setScrollState(0);
  }
  
  private void x()
  {
    this.D = (1 + this.D);
  }
  
  private void y()
  {
    this.D = (-1 + this.D);
    if (this.D < 1)
    {
      this.D = 0;
      z();
    }
  }
  
  private void z()
  {
    int i1 = this.y;
    this.y = 0;
    if ((i1 != 0) && (this.B != null) && (this.B.isEnabled()))
    {
      AccessibilityEvent localAccessibilityEvent = AccessibilityEvent.obtain();
      localAccessibilityEvent.setEventType(2048);
      android.support.v4.view.a.a.a(localAccessibilityEvent, i1);
      sendAccessibilityEventUnchecked(localAccessibilityEvent);
    }
  }
  
  long a(cz paramcz)
  {
    if (this.n.b()) {
      return paramcz.g();
    }
    return paramcz.b;
  }
  
  cz a(int paramInt, boolean paramBoolean)
  {
    int i1 = this.c.c();
    for (int i2 = 0; i2 < i1; i2++)
    {
      cz localcz = b(this.c.c(i2));
      if ((localcz != null) && (!localcz.r())) {
        if (paramBoolean)
        {
          if (localcz.b != paramInt) {}
        }
        else {
          while (localcz.e() == paramInt) {
            return localcz;
          }
        }
      }
    }
    return null;
  }
  
  public cz a(View paramView)
  {
    ViewParent localViewParent = paramView.getParent();
    if ((localViewParent != null) && (localViewParent != this)) {
      throw new IllegalArgumentException("View " + paramView + " is not a direct child of " + this);
    }
    return b(paramView);
  }
  
  public View a(float paramFloat1, float paramFloat2)
  {
    for (int i1 = -1 + this.c.b(); i1 >= 0; i1--)
    {
      View localView = this.c.b(i1);
      float f1 = bv.l(localView);
      float f2 = bv.m(localView);
      if ((paramFloat1 >= f1 + localView.getLeft()) && (paramFloat1 <= f1 + localView.getRight()) && (paramFloat2 >= f2 + localView.getTop()) && (paramFloat2 <= f2 + localView.getBottom())) {
        return localView;
      }
    }
    return null;
  }
  
  void a()
  {
    this.b = new p(new cb(this));
  }
  
  public void a(int paramInt)
  {
    int i1 = this.c.b();
    for (int i2 = 0; i2 < i1; i2++) {
      this.c.b(i2).offsetTopAndBottom(paramInt);
    }
  }
  
  public void a(int paramInt1, int paramInt2)
  {
    if (this.o == null) {
      Log.e("RecyclerView", "Cannot smooth scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
    }
    for (;;)
    {
      return;
      if (!this.o.c()) {
        paramInt1 = 0;
      }
      boolean bool = this.o.d();
      int i1 = 0;
      if (!bool) {}
      while ((paramInt1 != 0) || (i1 != 0))
      {
        this.T.b(paramInt1, i1);
        return;
        i1 = paramInt2;
      }
    }
  }
  
  void a(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    int i1 = paramInt1 + paramInt2;
    int i2 = this.c.c();
    int i3 = 0;
    if (i3 < i2)
    {
      cz localcz = b(this.c.c(i3));
      if ((localcz != null) && (!localcz.c()))
      {
        if (localcz.b < i1) {
          break label84;
        }
        localcz.a(-paramInt2, paramBoolean);
        cw.a(this.e, true);
      }
      for (;;)
      {
        i3++;
        break;
        label84:
        if (localcz.b >= paramInt1)
        {
          localcz.a(paramInt1 - 1, -paramInt2, paramBoolean);
          cw.a(this.e, true);
        }
      }
    }
    this.a.b(paramInt1, paramInt2, paramBoolean);
    requestLayout();
  }
  
  public void a(cn paramcn)
  {
    this.r.add(paramcn);
  }
  
  void a(String paramString)
  {
    if (i())
    {
      if (paramString == null) {
        throw new IllegalStateException("Cannot call this method while RecyclerView is computing a layout or scrolling");
      }
      throw new IllegalStateException(paramString);
    }
  }
  
  void a(boolean paramBoolean)
  {
    if (this.w)
    {
      if ((paramBoolean) && (this.x) && (this.o != null) && (this.n != null)) {
        j();
      }
      this.w = false;
      this.x = false;
    }
  }
  
  boolean a(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3, int paramInt4)
  {
    t();
    cc localcc = this.n;
    int i1 = 0;
    int i2 = 0;
    int i3 = 0;
    int i4 = 0;
    if (localcc != null)
    {
      b();
      x();
      h.a("RV Scroll");
      i3 = 0;
      i4 = 0;
      if (paramInt1 != 0)
      {
        i3 = this.o.a(paramInt1, this.a, this.e);
        i4 = paramInt1 - i3;
      }
      i1 = 0;
      i2 = 0;
      if (paramInt2 != 0)
      {
        i1 = this.o.b(paramInt2, this.a, this.e);
        i2 = paramInt2 - i1;
      }
      h.a();
      if (A())
      {
        int i7 = this.c.b();
        int i8 = 0;
        if (i8 < i7)
        {
          View localView1 = this.c.b(i8);
          cz localcz1 = a(localView1);
          cz localcz2;
          if ((localcz1 != null) && (localcz1.h != null))
          {
            localcz2 = localcz1.h;
            if (localcz2 == null) {
              break label259;
            }
          }
          label259:
          for (View localView2 = localcz2.a;; localView2 = null)
          {
            if (localView2 != null)
            {
              int i9 = localView1.getLeft();
              int i10 = localView1.getTop();
              if ((i9 != localView2.getLeft()) || (i10 != localView2.getTop())) {
                localView2.layout(i9, i10, i9 + localView2.getWidth(), i10 + localView2.getHeight());
              }
            }
            i8++;
            break;
          }
        }
      }
      y();
      a(false);
    }
    int i5 = i3;
    int i6 = i2;
    if (!this.q.isEmpty()) {
      invalidate();
    }
    if (bv.a(this) != 2)
    {
      if (paramBoolean) {
        a(paramInt3, i4, paramInt4, i6);
      }
      i(paramInt1, paramInt2);
    }
    if ((i5 != 0) || (i1 != 0)) {
      h(i5, i1);
    }
    if (!awakenScrollBars()) {
      invalidate();
    }
    return (i5 != 0) || (i1 != 0);
  }
  
  boolean a(AccessibilityEvent paramAccessibilityEvent)
  {
    boolean bool1 = i();
    boolean bool2 = false;
    if (bool1) {
      if (paramAccessibilityEvent == null) {
        break label51;
      }
    }
    label51:
    for (int i1 = android.support.v4.view.a.a.b(paramAccessibilityEvent);; i1 = 0)
    {
      int i2 = 0;
      if (i1 == 0) {}
      for (;;)
      {
        this.y = (i2 | this.y);
        bool2 = true;
        return bool2;
        i2 = i1;
      }
    }
  }
  
  public void addFocusables(ArrayList paramArrayList, int paramInt1, int paramInt2)
  {
    if ((this.o == null) || (!this.o.a(this, paramArrayList, paramInt1, paramInt2))) {
      super.addFocusables(paramArrayList, paramInt1, paramInt2);
    }
  }
  
  void b()
  {
    if (!this.w)
    {
      this.w = true;
      this.x = false;
    }
  }
  
  public void b(int paramInt)
  {
    int i1 = this.c.b();
    for (int i2 = 0; i2 < i1; i2++) {
      this.c.b(i2).offsetLeftAndRight(paramInt);
    }
  }
  
  public boolean b(int paramInt1, int paramInt2)
  {
    if (this.o == null) {
      Log.e("RecyclerView", "Cannot fling without a LayoutManager set. Call setLayoutManager with a non-null argument.");
    }
    int i1;
    int i2;
    do
    {
      return false;
      boolean bool1 = this.o.c();
      boolean bool2 = this.o.d();
      if ((!bool1) || (Math.abs(paramInt1) < this.Q)) {
        paramInt1 = 0;
      }
      if ((!bool2) || (Math.abs(paramInt2) < this.Q)) {
        paramInt2 = 0;
      }
      i1 = Math.max(-this.R, Math.min(paramInt1, this.R));
      i2 = Math.max(-this.R, Math.min(paramInt2, this.R));
    } while ((i1 == 0) && (i2 == 0));
    this.T.a(i1, i2);
    return true;
  }
  
  @Deprecated
  public int c(View paramView)
  {
    return d(paramView);
  }
  
  public void c()
  {
    setScrollState(0);
    u();
  }
  
  public void c(int paramInt) {}
  
  void c(int paramInt1, int paramInt2)
  {
    if (paramInt1 < 0)
    {
      d();
      this.E.a(-paramInt1);
      if (paramInt2 >= 0) {
        break label69;
      }
      f();
      this.F.a(-paramInt2);
    }
    for (;;)
    {
      if ((paramInt1 != 0) || (paramInt2 != 0)) {
        bv.d(this);
      }
      return;
      if (paramInt1 <= 0) {
        break;
      }
      e();
      this.G.a(paramInt1);
      break;
      label69:
      if (paramInt2 > 0)
      {
        g();
        this.H.a(paramInt2);
      }
    }
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return ((paramLayoutParams instanceof cm)) && (this.o.a((cm)paramLayoutParams));
  }
  
  public int computeHorizontalScrollExtent()
  {
    if (this.o.c()) {
      return this.o.d(this.e);
    }
    return 0;
  }
  
  public int computeHorizontalScrollOffset()
  {
    if (this.o.c()) {
      return this.o.b(this.e);
    }
    return 0;
  }
  
  public int computeHorizontalScrollRange()
  {
    if (this.o.c()) {
      return this.o.f(this.e);
    }
    return 0;
  }
  
  public int computeVerticalScrollExtent()
  {
    if (this.o.d()) {
      return this.o.e(this.e);
    }
    return 0;
  }
  
  public int computeVerticalScrollOffset()
  {
    if (this.o.d()) {
      return this.o.c(this.e);
    }
    return 0;
  }
  
  public int computeVerticalScrollRange()
  {
    if (this.o.d()) {
      return this.o.g(this.e);
    }
    return 0;
  }
  
  public int d(View paramView)
  {
    cz localcz = b(paramView);
    if (localcz != null) {
      return localcz.f();
    }
    return -1;
  }
  
  void d()
  {
    if (this.E != null) {
      return;
    }
    this.E = new z(getContext());
    if (this.k)
    {
      this.E.a(getMeasuredHeight() - getPaddingTop() - getPaddingBottom(), getMeasuredWidth() - getPaddingLeft() - getPaddingRight());
      return;
    }
    this.E.a(getMeasuredHeight(), getMeasuredWidth());
  }
  
  void d(int paramInt)
  {
    if (this.o != null) {
      this.o.h(paramInt);
    }
    c(paramInt);
    if (this.U != null) {
      this.U.a(this, paramInt);
    }
    if (this.V != null) {
      for (int i1 = -1 + this.V.size(); i1 >= 0; i1--) {
        ((co)this.V.get(i1)).a(this, paramInt);
      }
    }
  }
  
  void d(int paramInt1, int paramInt2)
  {
    int i1 = this.c.c();
    int i2;
    int i3;
    if (paramInt1 < paramInt2)
    {
      i2 = -1;
      i3 = paramInt2;
    }
    cz localcz;
    for (int i4 = paramInt1;; i4 = paramInt2)
    {
      for (int i5 = 0;; i5++)
      {
        if (i5 >= i1) {
          break label129;
        }
        localcz = b(this.c.c(i5));
        if ((localcz != null) && (localcz.b >= i4) && (localcz.b <= i3)) {
          break;
        }
      }
      i2 = 1;
      i3 = paramInt1;
    }
    if (localcz.b == paramInt1) {
      localcz.a(paramInt2 - paramInt1, false);
    }
    for (;;)
    {
      cw.a(this.e, true);
      break;
      localcz.a(i2, false);
    }
    label129:
    this.a.a(paramInt1, paramInt2);
    requestLayout();
  }
  
  protected void dispatchRestoreInstanceState(SparseArray paramSparseArray)
  {
    dispatchThawSelfOnly(paramSparseArray);
  }
  
  protected void dispatchSaveInstanceState(SparseArray paramSparseArray)
  {
    dispatchFreezeSelfOnly(paramSparseArray);
  }
  
  public void draw(Canvas paramCanvas)
  {
    int i1 = 1;
    super.draw(paramCanvas);
    int i2 = this.q.size();
    for (int i3 = 0; i3 < i2; i3++) {
      ((cj)this.q.get(i3)).b(paramCanvas, this, this.e);
    }
    int i14;
    int i4;
    if ((this.E != null) && (!this.E.a()))
    {
      int i13 = paramCanvas.save();
      if (this.k)
      {
        i14 = getPaddingBottom();
        paramCanvas.rotate(270.0F);
        paramCanvas.translate(i14 + -getHeight(), 0.0F);
        if ((this.E == null) || (!this.E.a(paramCanvas))) {
          break label466;
        }
        i4 = i1;
        label129:
        paramCanvas.restoreToCount(i13);
      }
    }
    for (;;)
    {
      int i12;
      label200:
      int i9;
      label255:
      int i10;
      if ((this.F != null) && (!this.F.a()))
      {
        int i11 = paramCanvas.save();
        if (this.k) {
          paramCanvas.translate(getPaddingLeft(), getPaddingTop());
        }
        if ((this.F != null) && (this.F.a(paramCanvas)))
        {
          i12 = i1;
          i4 |= i12;
          paramCanvas.restoreToCount(i11);
        }
      }
      else
      {
        if ((this.G != null) && (!this.G.a()))
        {
          int i7 = paramCanvas.save();
          int i8 = getWidth();
          if (!this.k) {
            break label478;
          }
          i9 = getPaddingTop();
          paramCanvas.rotate(90.0F);
          paramCanvas.translate(-i9, -i8);
          if ((this.G == null) || (!this.G.a(paramCanvas))) {
            break label484;
          }
          i10 = i1;
          label295:
          i4 |= i10;
          paramCanvas.restoreToCount(i7);
        }
        if ((this.H != null) && (!this.H.a()))
        {
          int i5 = paramCanvas.save();
          paramCanvas.rotate(180.0F);
          if (!this.k) {
            break label490;
          }
          paramCanvas.translate(-getWidth() + getPaddingRight(), -getHeight() + getPaddingBottom());
          label371:
          z localz = this.H;
          int i6 = 0;
          if (localz != null)
          {
            boolean bool = this.H.a(paramCanvas);
            i6 = 0;
            if (bool) {
              i6 = i1;
            }
          }
          i4 |= i6;
          paramCanvas.restoreToCount(i5);
        }
        if ((i4 != 0) || (this.d == null) || (this.q.size() <= 0) || (!this.d.b())) {
          break label509;
        }
      }
      for (;;)
      {
        if (i1 != 0) {
          bv.d(this);
        }
        return;
        i14 = 0;
        break;
        label466:
        i4 = 0;
        break label129;
        i12 = 0;
        break label200;
        label478:
        i9 = 0;
        break label255;
        label484:
        i10 = 0;
        break label295;
        label490:
        paramCanvas.translate(-getWidth(), -getHeight());
        break label371;
        label509:
        i1 = i4;
      }
      i4 = 0;
    }
  }
  
  public int e(View paramView)
  {
    cz localcz = b(paramView);
    if (localcz != null) {
      return localcz.e();
    }
    return -1;
  }
  
  void e()
  {
    if (this.G != null) {
      return;
    }
    this.G = new z(getContext());
    if (this.k)
    {
      this.G.a(getMeasuredHeight() - getPaddingTop() - getPaddingBottom(), getMeasuredWidth() - getPaddingLeft() - getPaddingRight());
      return;
    }
    this.G.a(getMeasuredHeight(), getMeasuredWidth());
  }
  
  void e(int paramInt1, int paramInt2)
  {
    int i1 = this.c.c();
    for (int i2 = 0; i2 < i1; i2++)
    {
      cz localcz = b(this.c.c(i2));
      if ((localcz != null) && (!localcz.c()) && (localcz.b >= paramInt1))
      {
        localcz.a(paramInt2, false);
        cw.a(this.e, true);
      }
    }
    this.a.b(paramInt1, paramInt2);
    requestLayout();
  }
  
  void f()
  {
    if (this.F != null) {
      return;
    }
    this.F = new z(getContext());
    if (this.k)
    {
      this.F.a(getMeasuredWidth() - getPaddingLeft() - getPaddingRight(), getMeasuredHeight() - getPaddingTop() - getPaddingBottom());
      return;
    }
    this.F.a(getMeasuredWidth(), getMeasuredHeight());
  }
  
  void f(int paramInt1, int paramInt2)
  {
    int i1 = this.c.c();
    int i2 = paramInt1 + paramInt2;
    int i3 = 0;
    if (i3 < i1)
    {
      View localView = this.c.c(i3);
      cz localcz = b(localView);
      if ((localcz == null) || (localcz.c())) {}
      for (;;)
      {
        i3++;
        break;
        if ((localcz.b >= paramInt1) && (localcz.b < i2))
        {
          localcz.b(2);
          if (A()) {
            localcz.b(64);
          }
          ((cm)localView.getLayoutParams()).c = true;
        }
      }
    }
    this.a.c(paramInt1, paramInt2);
  }
  
  public void f(View paramView) {}
  
  public View focusSearch(View paramView, int paramInt)
  {
    View localView = this.o.d(paramView, paramInt);
    if (localView != null) {}
    do
    {
      return localView;
      localView = FocusFinder.getInstance().findNextFocus(this, paramView, paramInt);
      if ((localView == null) && (this.n != null) && (this.o != null))
      {
        b();
        localView = this.o.a(paramView, paramInt, this.a, this.e);
        a(false);
      }
    } while (localView != null);
    return super.focusSearch(paramView, paramInt);
  }
  
  void g()
  {
    if (this.H != null) {
      return;
    }
    this.H = new z(getContext());
    if (this.k)
    {
      this.H.a(getMeasuredWidth() - getPaddingLeft() - getPaddingRight(), getMeasuredHeight() - getPaddingTop() - getPaddingBottom());
      return;
    }
    this.H.a(getMeasuredWidth(), getMeasuredHeight());
  }
  
  public void g(int paramInt1, int paramInt2) {}
  
  public void g(View paramView) {}
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams()
  {
    if (this.o == null) {
      throw new IllegalStateException("RecyclerView has no LayoutManager");
    }
    return this.o.a();
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
  {
    if (this.o == null) {
      throw new IllegalStateException("RecyclerView has no LayoutManager");
    }
    return this.o.a(getContext(), paramAttributeSet);
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    if (this.o == null) {
      throw new IllegalStateException("RecyclerView has no LayoutManager");
    }
    return this.o.a(paramLayoutParams);
  }
  
  public cc getAdapter()
  {
    return this.n;
  }
  
  public int getBaseline()
  {
    if (this.o != null) {
      return this.o.o();
    }
    return super.getBaseline();
  }
  
  public da getCompatAccessibilityDelegate()
  {
    return this.ab;
  }
  
  public cf getItemAnimator()
  {
    return this.d;
  }
  
  public cl getLayoutManager()
  {
    return this.o;
  }
  
  public cp getRecycledViewPool()
  {
    return this.a.f();
  }
  
  public int getScrollState()
  {
    return this.I;
  }
  
  Rect h(View paramView)
  {
    cm localcm = (cm)paramView.getLayoutParams();
    if (!localcm.c) {
      return localcm.b;
    }
    Rect localRect = localcm.b;
    localRect.set(0, 0, 0, 0);
    int i1 = this.q.size();
    for (int i2 = 0; i2 < i1; i2++)
    {
      this.m.set(0, 0, 0, 0);
      ((cj)this.q.get(i2)).a(this.m, paramView, this, this.e);
      localRect.left += this.m.left;
      localRect.top += this.m.top;
      localRect.right += this.m.right;
      localRect.bottom += this.m.bottom;
    }
    localcm.c = false;
    return localRect;
  }
  
  void h()
  {
    this.H = null;
    this.F = null;
    this.G = null;
    this.E = null;
  }
  
  void h(int paramInt1, int paramInt2)
  {
    int i1 = getScrollX();
    int i2 = getScrollY();
    onScrollChanged(i1, i2, i1, i2);
    g(paramInt1, paramInt2);
    if (this.U != null) {
      this.U.a(this, paramInt1, paramInt2);
    }
    if (this.V != null) {
      for (int i3 = -1 + this.V.size(); i3 >= 0; i3--) {
        ((co)this.V.get(i3)).a(this, paramInt1, paramInt2);
      }
    }
  }
  
  boolean i()
  {
    return this.D > 0;
  }
  
  void j()
  {
    if (this.n == null)
    {
      Log.e("RecyclerView", "No adapter attached; skipping layout");
      return;
    }
    if (this.o == null)
    {
      Log.e("RecyclerView", "No layout manager attached; skipping layout");
      return;
    }
    this.e.d.clear();
    b();
    x();
    D();
    cw localcw1 = this.e;
    android.support.v4.f.a locala1;
    int i18;
    label191:
    cz localcz8;
    if ((cw.b(this.e)) && (this.g) && (A()))
    {
      locala1 = new android.support.v4.f.a();
      localcw1.c = locala1;
      this.g = false;
      this.f = false;
      cw.b(this.e, cw.a(this.e));
      this.e.e = this.n.a();
      a(this.ac);
      if (!cw.b(this.e)) {
        break label301;
      }
      this.e.a.clear();
      this.e.b.clear();
      int i17 = this.c.b();
      i18 = 0;
      if (i18 >= i17) {
        break label301;
      }
      localcz8 = b(this.c.b(i18));
      if ((!localcz8.c()) && ((!localcz8.n()) || (this.n.b()))) {
        break label249;
      }
    }
    for (;;)
    {
      i18++;
      break label191;
      locala1 = null;
      break;
      label249:
      View localView3 = localcz8.a;
      this.e.a.put(localcz8, new ck(localcz8, localView3.getLeft(), localView3.getTop(), localView3.getRight(), localView3.getBottom()));
    }
    label301:
    android.support.v4.f.a locala4;
    int i12;
    label482:
    View localView2;
    if (cw.a(this.e))
    {
      l();
      if (this.e.c != null)
      {
        int i15 = this.c.b();
        for (int i16 = 0; i16 < i15; i16++)
        {
          cz localcz7 = b(this.c.b(i16));
          if ((localcz7.p()) && (!localcz7.r()) && (!localcz7.c()))
          {
            long l4 = a(localcz7);
            this.e.c.put(Long.valueOf(l4), localcz7);
            this.e.a.remove(localcz7);
          }
        }
      }
      boolean bool2 = cw.c(this.e);
      cw.a(this.e, false);
      this.o.a(this.a, this.e);
      cw.a(this.e, bool2);
      locala4 = new android.support.v4.f.a();
      i12 = 0;
      if (i12 < this.c.b())
      {
        localView2 = this.c.b(i12);
        if (!b(localView2).c()) {}
      }
    }
    label516:
    label525:
    label754:
    label766:
    label930:
    label936:
    label1035:
    label1302:
    label1493:
    label1564:
    label1611:
    label1732:
    label1742:
    for (;;)
    {
      i12++;
      break label482;
      int i13 = 0;
      if (i13 < this.e.a.size()) {
        if (((cz)this.e.a.b(i13)).a != localView2) {}
      }
      for (int i14 = 1;; i14 = 0)
      {
        if (i14 != 0) {
          break label1742;
        }
        locala4.put(localView2, new Rect(localView2.getLeft(), localView2.getTop(), localView2.getRight(), localView2.getBottom()));
        break label516;
        i13++;
        break label525;
        m();
        this.b.c();
        for (android.support.v4.f.a locala2 = locala4;; locala2 = null)
        {
          this.e.e = this.n.a();
          cw.b(this.e, 0);
          cw.b(this.e, false);
          this.o.a(this.a, this.e);
          cw.a(this.e, false);
          this.j = null;
          cw localcw2 = this.e;
          boolean bool1;
          android.support.v4.f.a locala3;
          int i2;
          cz localcz5;
          if ((cw.b(this.e)) && (this.d != null))
          {
            bool1 = true;
            cw.c(localcw2, bool1);
            if (!cw.b(this.e)) {
              break label1611;
            }
            if (this.e.c == null) {
              break label930;
            }
            locala3 = new android.support.v4.f.a();
            int i1 = this.c.b();
            i2 = 0;
            if (i2 >= i1) {
              break label1035;
            }
            localcz5 = b(this.c.b(i2));
            if (!localcz5.c()) {
              break label936;
            }
          }
          for (;;)
          {
            i2++;
            break label766;
            m();
            this.b.e();
            if (this.e.c == null) {
              break label1732;
            }
            int i10 = this.c.b();
            for (int i11 = 0; i11 < i10; i11++)
            {
              cz localcz6 = b(this.c.b(i11));
              if ((localcz6.p()) && (!localcz6.r()) && (!localcz6.c()))
              {
                long l3 = a(localcz6);
                this.e.c.put(Long.valueOf(l3), localcz6);
                this.e.a.remove(localcz6);
              }
            }
            bool1 = false;
            break;
            locala3 = null;
            break label754;
            View localView1 = localcz5.a;
            long l2 = a(localcz5);
            if ((locala3 != null) && (this.e.c.get(Long.valueOf(l2)) != null)) {
              locala3.put(Long.valueOf(l2), localcz5);
            } else {
              this.e.b.put(localcz5, new ck(localcz5, localView1.getLeft(), localView1.getTop(), localView1.getRight(), localView1.getBottom()));
            }
          }
          a(locala2);
          for (int i3 = -1 + this.e.a.size(); i3 >= 0; i3--)
          {
            cz localcz4 = (cz)this.e.a.b(i3);
            if (!this.e.b.containsKey(localcz4))
            {
              ck localck4 = (ck)this.e.a.c(i3);
              this.e.a.d(i3);
              this.a.d(localck4.a);
              a(localck4);
            }
          }
          int i4 = this.e.b.size();
          if (i4 > 0)
          {
            int i9 = i4 - 1;
            if (i9 >= 0)
            {
              cz localcz3 = (cz)this.e.b.b(i9);
              ck localck3 = (ck)this.e.b.c(i9);
              if ((this.e.a.isEmpty()) || (!this.e.a.containsKey(localcz3)))
              {
                this.e.b.d(i9);
                if (locala2 == null) {
                  break label1302;
                }
              }
              for (Rect localRect = (Rect)locala2.get(localcz3.a);; localRect = null)
              {
                a(localcz3, localRect, localck3.b, localck3.c);
                i9--;
                break;
              }
            }
          }
          int i5 = this.e.b.size();
          for (int i6 = 0; i6 < i5; i6++)
          {
            cz localcz2 = (cz)this.e.b.b(i6);
            ck localck1 = (ck)this.e.b.c(i6);
            ck localck2 = (ck)this.e.a.get(localcz2);
            if ((localck2 != null) && (localck1 != null) && ((localck2.b != localck1.b) || (localck2.c != localck1.c)))
            {
              localcz2.a(false);
              if (this.d.a(localcz2, localck2.b, localck2.c, localck1.b, localck1.c)) {
                B();
              }
            }
          }
          int i7;
          int i8;
          long l1;
          cz localcz1;
          if (this.e.c != null)
          {
            i7 = this.e.c.size();
            i8 = i7 - 1;
            if (i8 < 0) {
              break label1611;
            }
            l1 = ((Long)this.e.c.b(i8)).longValue();
            localcz1 = (cz)this.e.c.get(Long.valueOf(l1));
            if (!localcz1.c()) {
              break label1564;
            }
          }
          for (;;)
          {
            i8--;
            break label1493;
            i7 = 0;
            break;
            if ((cq.a(this.a) != null) && (cq.a(this.a).contains(localcz1))) {
              a(localcz1, (cz)locala3.get(Long.valueOf(l1)));
            }
          }
          a(false);
          this.o.b(this.a);
          cw.c(this.e, this.e.e);
          this.C = false;
          cw.c(this.e, false);
          cw.d(this.e, false);
          y();
          cl.a(this.o, false);
          if (cq.a(this.a) != null) {
            cq.a(this.a).clear();
          }
          this.e.c = null;
          if (!k(this.ac[0], this.ac[1])) {
            break;
          }
          h(0, 0);
          return;
        }
      }
    }
  }
  
  void k()
  {
    int i1 = this.c.c();
    for (int i2 = 0; i2 < i1; i2++) {
      ((cm)this.c.c(i2).getLayoutParams()).c = true;
    }
    this.a.j();
  }
  
  void l()
  {
    int i1 = this.c.c();
    for (int i2 = 0; i2 < i1; i2++)
    {
      cz localcz = b(this.c.c(i2));
      if (!localcz.c()) {
        localcz.b();
      }
    }
  }
  
  void m()
  {
    int i1 = this.c.c();
    for (int i2 = 0; i2 < i1; i2++)
    {
      cz localcz = b(this.c.c(i2));
      if (!localcz.c()) {
        localcz.a();
      }
    }
    this.a.i();
  }
  
  void n()
  {
    int i1 = this.c.b();
    int i2 = 0;
    if (i2 < i1)
    {
      cz localcz = b(this.c.b(i2));
      if ((localcz == null) || (localcz.c())) {}
      for (;;)
      {
        i2++;
        break;
        if ((localcz.r()) || (localcz.n()))
        {
          requestLayout();
        }
        else if (localcz.o())
        {
          int i3 = this.n.a(localcz.b);
          if (localcz.h() != i3) {
            break label130;
          }
          if ((!localcz.p()) || (!A())) {
            this.n.b(localcz, localcz.b);
          } else {
            requestLayout();
          }
        }
      }
      label130:
      requestLayout();
    }
  }
  
  void o()
  {
    int i1 = this.c.c();
    for (int i2 = 0; i2 < i1; i2++)
    {
      cz localcz = b(this.c.c(i2));
      if ((localcz != null) && (!localcz.c())) {
        localcz.b(6);
      }
    }
    k();
    this.a.h();
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    this.D = 0;
    this.t = true;
    this.v = false;
    if (this.o != null) {
      this.o.b(this);
    }
    this.aa = false;
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    if (this.d != null) {
      this.d.c();
    }
    this.v = false;
    c();
    this.t = false;
    if (this.o != null) {
      this.o.b(this, this.a);
    }
    removeCallbacks(this.ad);
  }
  
  public void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    int i1 = this.q.size();
    for (int i2 = 0; i2 < i1; i2++) {
      ((cj)this.q.get(i2)).a(paramCanvas, this, this.e);
    }
  }
  
  public boolean onGenericMotionEvent(MotionEvent paramMotionEvent)
  {
    if (this.o == null) {}
    label100:
    label103:
    for (;;)
    {
      return false;
      if (((0x2 & ba.d(paramMotionEvent)) != 0) && (paramMotionEvent.getAction() == 8))
      {
        float f1;
        if (this.o.d())
        {
          f1 = ba.e(paramMotionEvent, 9);
          if (!this.o.c()) {
            break label100;
          }
        }
        for (float f2 = ba.e(paramMotionEvent, 10);; f2 = 0.0F)
        {
          if ((f1 == 0.0F) && (f2 == 0.0F)) {
            break label103;
          }
          float f3 = getScrollFactor();
          scrollBy((int)(f2 * f3), (int)(f1 * f3));
          return false;
          f1 = 0.0F;
          break;
        }
      }
    }
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    int i1 = -1;
    if (a(paramMotionEvent))
    {
      w();
      return true;
    }
    boolean bool1 = this.o.c();
    boolean bool2 = this.o.d();
    if (this.K == null) {
      this.K = VelocityTracker.obtain();
    }
    this.K.addMovement(paramMotionEvent);
    int i2 = ba.a(paramMotionEvent);
    int i3 = ba.b(paramMotionEvent);
    label112:
    int i8;
    int i14;
    switch (i2)
    {
    case 4: 
    default: 
    case 0: 
    case 5: 
    case 2: 
      for (;;)
      {
        if (this.I != 1)
        {
          return false;
          this.J = ba.b(paramMotionEvent, 0);
          int i17 = (int)(0.5F + paramMotionEvent.getX());
          this.N = i17;
          this.L = i17;
          int i18 = (int)(0.5F + paramMotionEvent.getY());
          this.O = i18;
          this.M = i18;
          if (this.I == 2)
          {
            getParent().requestDisallowInterceptTouchEvent(true);
            setScrollState(1);
            continue;
            this.J = ba.b(paramMotionEvent, i3);
            int i15 = (int)(0.5F + ba.c(paramMotionEvent, i3));
            this.N = i15;
            this.L = i15;
            int i16 = (int)(0.5F + ba.d(paramMotionEvent, i3));
            this.O = i16;
            this.M = i16;
            continue;
            int i4 = ba.a(paramMotionEvent, this.J);
            if (i4 < 0)
            {
              Log.e("RecyclerView", "Error processing scroll; pointer index for id " + this.J + " not found. Did any MotionEvents get skipped?");
              return false;
            }
            int i5 = (int)(0.5F + ba.c(paramMotionEvent, i4));
            int i6 = (int)(0.5F + ba.d(paramMotionEvent, i4));
            if (this.I != 1)
            {
              int i7 = i5 - this.L;
              i8 = i6 - this.M;
              if ((!bool1) || (Math.abs(i7) <= this.P)) {
                break label519;
              }
              int i12 = this.L;
              int i13 = this.P;
              if (i7 < 0)
              {
                i14 = i1;
                label407:
                this.N = (i12 + i14 * i13);
              }
            }
          }
        }
      }
    }
    label489:
    label519:
    for (int i9 = 1;; i9 = 0)
    {
      int i10;
      int i11;
      if ((bool2) && (Math.abs(i8) > this.P))
      {
        i10 = this.M;
        i11 = this.P;
        if (i8 >= 0) {
          break label489;
        }
      }
      for (;;)
      {
        this.O = (i10 + i1 * i11);
        i9 = 1;
        if (i9 == 0) {
          break;
        }
        setScrollState(1);
        break;
        i14 = 1;
        break label407;
        i1 = 1;
      }
      c(paramMotionEvent);
      break label112;
      this.K.clear();
      break label112;
      w();
      break label112;
      break;
    }
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    b();
    h.a("RV OnLayout");
    j();
    h.a();
    a(false);
    this.v = true;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    if (this.z)
    {
      b();
      D();
      if (cw.a(this.e))
      {
        cw.b(this.e, true);
        this.z = false;
        a(false);
      }
    }
    else
    {
      if (this.n == null) {
        break label107;
      }
      this.e.e = this.n.a();
      label65:
      if (this.o != null) {
        break label118;
      }
      j(paramInt1, paramInt2);
    }
    for (;;)
    {
      cw.b(this.e, false);
      return;
      this.b.e();
      cw.b(this.e, false);
      break;
      label107:
      this.e.e = 0;
      break label65;
      label118:
      this.o.a(this.a, this.e, paramInt1, paramInt2);
    }
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    this.j = ((RecyclerView.SavedState)paramParcelable);
    super.onRestoreInstanceState(this.j.getSuperState());
    if ((this.o != null) && (this.j.a != null)) {
      this.o.a(this.j.a);
    }
  }
  
  protected Parcelable onSaveInstanceState()
  {
    RecyclerView.SavedState localSavedState = new RecyclerView.SavedState(super.onSaveInstanceState());
    if (this.j != null)
    {
      RecyclerView.SavedState.a(localSavedState, this.j);
      return localSavedState;
    }
    if (this.o != null)
    {
      localSavedState.a = this.o.b();
      return localSavedState;
    }
    localSavedState.a = null;
    return localSavedState;
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    if ((paramInt1 != paramInt3) || (paramInt2 != paramInt4)) {
      h();
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    int i1 = -1;
    if (b(paramMotionEvent))
    {
      w();
      return true;
    }
    boolean bool1 = this.o.c();
    boolean bool2 = this.o.d();
    if (this.K == null) {
      this.K = VelocityTracker.obtain();
    }
    this.K.addMovement(paramMotionEvent);
    int i2 = ba.a(paramMotionEvent);
    int i3 = ba.b(paramMotionEvent);
    int i5;
    int i6;
    int i12;
    int i18;
    switch (i2)
    {
    case 4: 
    default: 
      return true;
    case 0: 
      this.J = ba.b(paramMotionEvent, 0);
      int i21 = (int)(0.5F + paramMotionEvent.getX());
      this.N = i21;
      this.L = i21;
      int i22 = (int)(0.5F + paramMotionEvent.getY());
      this.O = i22;
      this.M = i22;
      return true;
    case 5: 
      this.J = ba.b(paramMotionEvent, i3);
      int i19 = (int)(0.5F + ba.c(paramMotionEvent, i3));
      this.N = i19;
      this.L = i19;
      int i20 = (int)(0.5F + ba.d(paramMotionEvent, i3));
      this.O = i20;
      this.M = i20;
      return true;
    case 2: 
      int i4 = ba.a(paramMotionEvent, this.J);
      if (i4 < 0)
      {
        Log.e("RecyclerView", "Error processing scroll; pointer index for id " + this.J + " not found. Did any MotionEvents get skipped?");
        return false;
      }
      i5 = (int)(0.5F + ba.c(paramMotionEvent, i4));
      i6 = (int)(0.5F + ba.d(paramMotionEvent, i4));
      if (this.I != 1)
      {
        int i11 = i5 - this.L;
        i12 = i6 - this.M;
        if ((!bool1) || (Math.abs(i11) <= this.P)) {
          break label674;
        }
        int i16 = this.L;
        int i17 = this.P;
        if (i11 >= 0) {
          break label535;
        }
        i18 = i1;
        this.N = (i16 + i18 * i17);
      }
      break;
    }
    label423:
    label535:
    label546:
    label662:
    label674:
    for (int i13 = 1;; i13 = 0)
    {
      int i7;
      int i8;
      if ((bool2) && (Math.abs(i12) > this.P))
      {
        int i14 = this.M;
        int i15 = this.P;
        if (i12 < 0)
        {
          this.O = (i14 + i1 * i15);
          i13 = 1;
        }
      }
      else
      {
        if (i13 != 0) {
          setScrollState(1);
        }
        if (this.I == 1)
        {
          i7 = i5 - this.N;
          i8 = i6 - this.O;
          if (!bool1) {
            break label546;
          }
        }
      }
      for (int i9 = -i7;; i9 = 0)
      {
        int i10 = 0;
        if (bool2) {
          i10 = -i8;
        }
        if (a(i9, i10, true, i5, i6)) {
          getParent().requestDisallowInterceptTouchEvent(true);
        }
        this.N = i5;
        this.O = i6;
        return true;
        i18 = 1;
        break;
        i1 = 1;
        break label423;
      }
      c(paramMotionEvent);
      return true;
      this.K.computeCurrentVelocity(1000, this.R);
      float f1;
      if (bool1)
      {
        f1 = -bq.a(this.K, this.J);
        if (!bool2) {
          break label662;
        }
      }
      for (float f2 = -bq.b(this.K, this.J);; f2 = 0.0F)
      {
        if (((f1 == 0.0F) && (f2 == 0.0F)) || (!b((int)f1, (int)f2))) {
          setScrollState(0);
        }
        this.K.clear();
        v();
        return true;
        f1 = 0.0F;
        break;
      }
      w();
      return true;
    }
  }
  
  public boolean p()
  {
    return (!this.v) || (this.C) || (this.b.d());
  }
  
  protected void removeDetachedView(View paramView, boolean paramBoolean)
  {
    cz localcz = b(paramView);
    if (localcz != null)
    {
      if (!localcz.s()) {
        break label32;
      }
      localcz.m();
    }
    label32:
    while (localcz.c())
    {
      j(paramView);
      super.removeDetachedView(paramView, paramBoolean);
      return;
    }
    throw new IllegalArgumentException("Called removeDetachedView with a view which is not flagged as tmp detached." + localcz);
  }
  
  public void requestChildFocus(View paramView1, View paramView2)
  {
    Rect localRect1;
    if ((!this.o.a(this, this.e, paramView1, paramView2)) && (paramView2 != null))
    {
      this.m.set(0, 0, paramView2.getWidth(), paramView2.getHeight());
      ViewGroup.LayoutParams localLayoutParams = paramView2.getLayoutParams();
      if ((localLayoutParams instanceof cm))
      {
        cm localcm = (cm)localLayoutParams;
        if (!localcm.c)
        {
          Rect localRect2 = localcm.b;
          Rect localRect3 = this.m;
          localRect3.left -= localRect2.left;
          Rect localRect4 = this.m;
          localRect4.right += localRect2.right;
          Rect localRect5 = this.m;
          localRect5.top -= localRect2.top;
          Rect localRect6 = this.m;
          localRect6.bottom += localRect2.bottom;
        }
      }
      offsetDescendantRectToMyCoords(paramView2, this.m);
      offsetRectIntoDescendantCoords(paramView1, this.m);
      localRect1 = this.m;
      if (this.v) {
        break label210;
      }
    }
    label210:
    for (boolean bool = true;; bool = false)
    {
      requestChildRectangleOnScreen(paramView1, localRect1, bool);
      super.requestChildFocus(paramView1, paramView2);
      return;
    }
  }
  
  public boolean requestChildRectangleOnScreen(View paramView, Rect paramRect, boolean paramBoolean)
  {
    return this.o.a(this, paramView, paramRect, paramBoolean);
  }
  
  public void requestLayout()
  {
    if (!this.w)
    {
      super.requestLayout();
      return;
    }
    this.x = true;
  }
  
  public void scrollBy(int paramInt1, int paramInt2)
  {
    if (this.o == null) {
      Log.e("RecyclerView", "Cannot scroll without a LayoutManager set. Call setLayoutManager with a non-null argument.");
    }
    boolean bool1;
    boolean bool2;
    do
    {
      return;
      bool1 = this.o.c();
      bool2 = this.o.d();
    } while ((!bool1) && (!bool2));
    int i1;
    if (bool1)
    {
      i1 = paramInt1;
      if (!bool2) {
        break label78;
      }
    }
    label78:
    for (int i2 = paramInt2;; i2 = 0)
    {
      a(i1, i2, false, 0, 0);
      return;
      i1 = 0;
      break;
    }
  }
  
  public void scrollTo(int paramInt1, int paramInt2)
  {
    throw new UnsupportedOperationException("RecyclerView does not support scrolling to an absolute position.");
  }
  
  public void sendAccessibilityEventUnchecked(AccessibilityEvent paramAccessibilityEvent)
  {
    if (a(paramAccessibilityEvent)) {
      return;
    }
    super.sendAccessibilityEventUnchecked(paramAccessibilityEvent);
  }
  
  public void setAccessibilityDelegateCompat(da paramda)
  {
    this.ab = paramda;
    bv.a(this, this.ab);
  }
  
  public void setAdapter(cc paramcc)
  {
    a(paramcc, false, true);
    requestLayout();
  }
  
  public void setClipToPadding(boolean paramBoolean)
  {
    if (paramBoolean != this.k) {
      h();
    }
    this.k = paramBoolean;
    super.setClipToPadding(paramBoolean);
    if (this.v) {
      requestLayout();
    }
  }
  
  public void setHasFixedSize(boolean paramBoolean)
  {
    this.u = paramBoolean;
  }
  
  public void setItemAnimator(cf paramcf)
  {
    if (this.d != null)
    {
      this.d.c();
      this.d.a(null);
    }
    this.d = paramcf;
    if (this.d != null) {
      this.d.a(this.W);
    }
  }
  
  public void setItemViewCacheSize(int paramInt)
  {
    this.a.a(paramInt);
  }
  
  public void setLayoutManager(cl paramcl)
  {
    if (paramcl == this.o) {
      return;
    }
    if (this.o != null)
    {
      if (this.t) {
        this.o.b(this, this.a);
      }
      this.o.a(null);
    }
    this.a.a();
    this.c.a();
    this.o = paramcl;
    if (paramcl != null)
    {
      if (paramcl.i != null) {
        throw new IllegalArgumentException("LayoutManager " + paramcl + " is already attached to a RecyclerView: " + paramcl.i);
      }
      this.o.a(this);
      if (this.t) {
        this.o.b(this);
      }
    }
    requestLayout();
  }
  
  @Deprecated
  public void setOnScrollListener(co paramco)
  {
    this.U = paramco;
  }
  
  public void setRecycledViewPool(cp paramcp)
  {
    this.a.a(paramcp);
  }
  
  public void setRecyclerListener(cr paramcr)
  {
    this.p = paramcr;
  }
  
  public void setScrollingTouchSlop(int paramInt)
  {
    ViewConfiguration localViewConfiguration = ViewConfiguration.get(getContext());
    switch (paramInt)
    {
    default: 
      Log.w("RecyclerView", "setScrollingTouchSlop(): bad argument constant " + paramInt + "; using default value");
    case 0: 
      this.P = localViewConfiguration.getScaledTouchSlop();
      return;
    }
    this.P = android.support.v4.view.cq.a(localViewConfiguration);
  }
  
  public void setViewCacheExtension(cx paramcx)
  {
    this.a.a(paramcx);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/RecyclerView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */