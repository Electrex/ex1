package android.support.v7.widget;

import android.support.v4.f.n;
import android.support.v4.f.o;
import java.util.ArrayList;
import java.util.List;

class p
  implements bt
{
  final ArrayList a = new ArrayList();
  final ArrayList b = new ArrayList();
  final q c;
  Runnable d;
  final boolean e;
  final bs f;
  private n g = new o(30);
  
  p(q paramq)
  {
    this(paramq, false);
  }
  
  p(q paramq, boolean paramBoolean)
  {
    this.c = paramq;
    this.e = paramBoolean;
    this.f = new bs(this);
  }
  
  private void b(r paramr)
  {
    g(paramr);
  }
  
  private void c(r paramr)
  {
    int i = paramr.b;
    int j = paramr.b + paramr.c;
    int k = -1;
    int m = paramr.b;
    int n = 0;
    if (m < j) {
      if ((this.c.a(m) != null) || (c(m)))
      {
        if (k != 0) {
          break label217;
        }
        e(a(1, i, n));
      }
    }
    label100:
    label211:
    label217:
    for (int i1 = 1;; i1 = 0)
    {
      k = 1;
      int i6;
      int i4;
      int i5;
      if (i1 != 0)
      {
        i6 = m - n;
        i4 = j - n;
        i5 = 1;
        int i7 = i6 + 1;
        n = i5;
        j = i4;
        m = i7;
        break;
        if (k != 1) {
          break label211;
        }
        g(a(1, i, n));
      }
      for (i1 = 1;; i1 = 0)
      {
        k = 0;
        break;
        int i2 = n + 1;
        int i3 = m;
        i4 = j;
        i5 = i2;
        i6 = i3;
        break label100;
        if (n != paramr.c)
        {
          a(paramr);
          paramr = a(1, i, n);
        }
        if (k == 0)
        {
          e(paramr);
          return;
        }
        g(paramr);
        return;
      }
    }
  }
  
  private boolean c(int paramInt)
  {
    int i = this.b.size();
    for (int j = 0; j < i; j++)
    {
      r localr = (r)this.b.get(j);
      if (localr.a == 3)
      {
        if (a(localr.c, j + 1) == paramInt) {
          return true;
        }
      }
      else if (localr.a == 0)
      {
        int k = localr.b + localr.c;
        for (int m = localr.b; m < k; m++) {
          if (a(m, j + 1) == paramInt) {
            return true;
          }
        }
      }
    }
    return false;
  }
  
  private void d(r paramr)
  {
    int i = paramr.b;
    int j = paramr.b + paramr.c;
    int k = paramr.b;
    int m = -1;
    int n = 0;
    if (k < j)
    {
      int i1;
      int i2;
      if ((this.c.a(k) != null) || (c(k)))
      {
        if (m == 0)
        {
          e(a(2, i, n));
          n = 0;
          i = k;
        }
        i1 = i;
        i2 = n;
      }
      for (int i3 = 1;; i3 = 0)
      {
        int i4 = i2 + 1;
        k++;
        int i5 = i3;
        n = i4;
        i = i1;
        m = i5;
        break;
        if (m == 1)
        {
          g(a(2, i, n));
          n = 0;
          i = k;
        }
        i1 = i;
        i2 = n;
      }
    }
    if (n != paramr.c)
    {
      a(paramr);
      paramr = a(2, i, n);
    }
    if (m == 0)
    {
      e(paramr);
      return;
    }
    g(paramr);
  }
  
  private int e(int paramInt1, int paramInt2)
  {
    int i = -1 + this.b.size();
    int j = paramInt1;
    if (i >= 0)
    {
      r localr2 = (r)this.b.get(i);
      int m;
      int n;
      label66:
      label106:
      int i1;
      if (localr2.a == 3) {
        if (localr2.b < localr2.c)
        {
          m = localr2.b;
          n = localr2.c;
          if ((j < m) || (j > n)) {
            break label204;
          }
          if (m != localr2.b) {
            break label159;
          }
          if (paramInt2 != 0) {
            break label139;
          }
          localr2.c = (1 + localr2.c);
          i1 = j + 1;
          label112:
          j = i1;
        }
      }
      for (;;)
      {
        i--;
        break;
        m = localr2.c;
        n = localr2.b;
        break label66;
        label139:
        if (paramInt2 != 1) {
          break label106;
        }
        localr2.c = (-1 + localr2.c);
        break label106;
        label159:
        if (paramInt2 == 0) {
          localr2.b = (1 + localr2.b);
        }
        for (;;)
        {
          i1 = j - 1;
          break;
          if (paramInt2 == 1) {
            localr2.b = (-1 + localr2.b);
          }
        }
        label204:
        if (j < localr2.b)
        {
          if (paramInt2 == 0)
          {
            localr2.b = (1 + localr2.b);
            localr2.c = (1 + localr2.c);
            i1 = j;
            break label112;
          }
          if (paramInt2 == 1)
          {
            localr2.b = (-1 + localr2.b);
            localr2.c = (-1 + localr2.c);
          }
        }
        i1 = j;
        break label112;
        if (localr2.b <= j)
        {
          if (localr2.a == 0) {
            j -= localr2.c;
          } else if (localr2.a == 1) {
            j += localr2.c;
          }
        }
        else if (paramInt2 == 0) {
          localr2.b = (1 + localr2.b);
        } else if (paramInt2 == 1) {
          localr2.b = (-1 + localr2.b);
        }
      }
    }
    int k = -1 + this.b.size();
    if (k >= 0)
    {
      r localr1 = (r)this.b.get(k);
      if (localr1.a == 3) {
        if ((localr1.c == localr1.b) || (localr1.c < 0))
        {
          this.b.remove(k);
          a(localr1);
        }
      }
      for (;;)
      {
        k--;
        break;
        if (localr1.c <= 0)
        {
          this.b.remove(k);
          a(localr1);
        }
      }
    }
    return j;
  }
  
  private void e(r paramr)
  {
    if ((paramr.a == 0) || (paramr.a == 3)) {
      throw new IllegalArgumentException("should not dispatch add or move for pre layout");
    }
    int i = e(paramr.b, paramr.a);
    int j = paramr.b;
    int k;
    int m;
    int n;
    int i1;
    int i2;
    label110:
    int i3;
    int i4;
    switch (paramr.a)
    {
    default: 
      throw new IllegalArgumentException("op should be remove or update." + paramr);
    case 2: 
      k = 1;
      m = 1;
      n = i;
      i1 = j;
      i2 = 1;
      if (i2 >= paramr.c) {
        break label280;
      }
      i3 = e(paramr.b + k * i2, paramr.a);
      switch (paramr.a)
      {
      default: 
        i4 = 0;
        label167:
        if (i4 != 0) {
          m++;
        }
        break;
      }
      break;
    }
    for (;;)
    {
      i2++;
      break label110;
      k = 0;
      break;
      if (i3 == n + 1)
      {
        i4 = 1;
        break label167;
      }
      i4 = 0;
      break label167;
      if (i3 == n)
      {
        i4 = 1;
        break label167;
      }
      i4 = 0;
      break label167;
      r localr2 = a(paramr.a, n, m);
      a(localr2, i1);
      a(localr2);
      if (paramr.a == 2) {
        i1 += m;
      }
      m = 1;
      n = i3;
    }
    label280:
    a(paramr);
    if (m > 0)
    {
      r localr1 = a(paramr.a, n, m);
      a(localr1, i1);
      a(localr1);
    }
  }
  
  private void f(r paramr)
  {
    g(paramr);
  }
  
  private void g(r paramr)
  {
    this.b.add(paramr);
    switch (paramr.a)
    {
    default: 
      throw new IllegalArgumentException("Unknown update op type for " + paramr);
    case 0: 
      this.c.d(paramr.b, paramr.c);
      return;
    case 3: 
      this.c.e(paramr.b, paramr.c);
      return;
    case 1: 
      this.c.b(paramr.b, paramr.c);
      return;
    }
    this.c.c(paramr.b, paramr.c);
  }
  
  int a(int paramInt)
  {
    return a(paramInt, 0);
  }
  
  int a(int paramInt1, int paramInt2)
  {
    int i = this.b.size();
    int j = paramInt1;
    r localr;
    if (paramInt2 < i)
    {
      localr = (r)this.b.get(paramInt2);
      if (localr.a == 3) {
        if (localr.b == j) {
          j = localr.c;
        }
      }
    }
    for (;;)
    {
      paramInt2++;
      break;
      if (localr.b < j) {
        j--;
      }
      if (localr.c <= j)
      {
        j++;
        continue;
        if (localr.b <= j) {
          if (localr.a == 1)
          {
            if (j < localr.b + localr.c)
            {
              j = -1;
              return j;
            }
            j -= localr.c;
          }
          else if (localr.a == 0)
          {
            j += localr.c;
          }
        }
      }
    }
  }
  
  public r a(int paramInt1, int paramInt2, int paramInt3)
  {
    r localr = (r)this.g.a();
    if (localr == null) {
      return new r(paramInt1, paramInt2, paramInt3);
    }
    localr.a = paramInt1;
    localr.b = paramInt2;
    localr.c = paramInt3;
    return localr;
  }
  
  void a()
  {
    a(this.a);
    a(this.b);
  }
  
  public void a(r paramr)
  {
    if (!this.e) {
      this.g.a(paramr);
    }
  }
  
  void a(r paramr, int paramInt)
  {
    this.c.a(paramr);
    switch (paramr.a)
    {
    default: 
      throw new IllegalArgumentException("only remove and update ops can be dispatched in first pass");
    case 1: 
      this.c.a(paramInt, paramr.c);
      return;
    }
    this.c.c(paramInt, paramr.c);
  }
  
  void a(List paramList)
  {
    int i = paramList.size();
    for (int j = 0; j < i; j++) {
      a((r)paramList.get(j));
    }
    paramList.clear();
  }
  
  public int b(int paramInt)
  {
    int i = this.a.size();
    int j = 0;
    int k = paramInt;
    r localr;
    if (j < i)
    {
      localr = (r)this.a.get(j);
      switch (localr.a)
      {
      }
    }
    for (;;)
    {
      j++;
      break;
      if (localr.b <= k)
      {
        k += localr.c;
        continue;
        if (localr.b <= k)
        {
          if (localr.b + localr.c > k)
          {
            k = -1;
            return k;
          }
          k -= localr.c;
          continue;
          if (localr.b == k)
          {
            k = localr.c;
          }
          else
          {
            if (localr.b < k) {
              k--;
            }
            if (localr.c <= k) {
              k++;
            }
          }
        }
      }
    }
  }
  
  void b()
  {
    this.f.a(this.a);
    int i = this.a.size();
    int j = 0;
    if (j < i)
    {
      r localr = (r)this.a.get(j);
      switch (localr.a)
      {
      }
      for (;;)
      {
        if (this.d != null) {
          this.d.run();
        }
        j++;
        break;
        f(localr);
        continue;
        c(localr);
        continue;
        d(localr);
        continue;
        b(localr);
      }
    }
    this.a.clear();
  }
  
  boolean b(int paramInt1, int paramInt2)
  {
    this.a.add(a(2, paramInt1, paramInt2));
    return this.a.size() == 1;
  }
  
  void c()
  {
    int i = this.b.size();
    for (int j = 0; j < i; j++) {
      this.c.b((r)this.b.get(j));
    }
    a(this.b);
  }
  
  boolean c(int paramInt1, int paramInt2)
  {
    this.a.add(a(0, paramInt1, paramInt2));
    return this.a.size() == 1;
  }
  
  boolean d()
  {
    return this.a.size() > 0;
  }
  
  boolean d(int paramInt1, int paramInt2)
  {
    this.a.add(a(1, paramInt1, paramInt2));
    return this.a.size() == 1;
  }
  
  void e()
  {
    c();
    int i = this.a.size();
    int j = 0;
    if (j < i)
    {
      r localr = (r)this.a.get(j);
      switch (localr.a)
      {
      }
      for (;;)
      {
        if (this.d != null) {
          this.d.run();
        }
        j++;
        break;
        this.c.b(localr);
        this.c.d(localr.b, localr.c);
        continue;
        this.c.b(localr);
        this.c.a(localr.b, localr.c);
        continue;
        this.c.b(localr);
        this.c.c(localr.b, localr.c);
        continue;
        this.c.b(localr);
        this.c.e(localr.b, localr.c);
      }
    }
    a(this.a);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/p.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */