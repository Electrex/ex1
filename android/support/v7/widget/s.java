package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.support.v7.a.b;
import android.support.v7.internal.widget.bc;
import android.support.v7.internal.widget.be;
import android.support.v7.internal.widget.bf;
import android.support.v7.internal.widget.bh;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

public class s
  extends AutoCompleteTextView
{
  private static final int[] a = { 16842964, 16843126 };
  private bf b;
  private be c;
  
  public s(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public s(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, b.autoCompleteTextViewStyle);
  }
  
  public s(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(bc.a(paramContext), paramAttributeSet, paramInt);
    if (bf.a)
    {
      bh localbh = bh.a(getContext(), paramAttributeSet, a, paramInt, 0);
      this.b = localbh.c();
      if (localbh.e(0))
      {
        ColorStateList localColorStateList = localbh.c().c(localbh.f(0, -1));
        if (localColorStateList != null) {
          setSupportBackgroundTintList(localColorStateList);
        }
      }
      if (localbh.e(1)) {
        setDropDownBackgroundDrawable(localbh.a(1));
      }
      localbh.b();
    }
  }
  
  private void a()
  {
    if ((getBackground() != null) && (this.c != null)) {
      bf.a(this, this.c);
    }
  }
  
  protected void drawableStateChanged()
  {
    super.drawableStateChanged();
    a();
  }
  
  public ColorStateList getSupportBackgroundTintList()
  {
    if (this.c != null) {
      return this.c.a;
    }
    return null;
  }
  
  public PorterDuff.Mode getSupportBackgroundTintMode()
  {
    if (this.c != null) {
      return this.c.b;
    }
    return null;
  }
  
  public void setDropDownBackgroundResource(int paramInt)
  {
    setDropDownBackgroundDrawable(this.b.a(paramInt));
  }
  
  public void setSupportBackgroundTintList(ColorStateList paramColorStateList)
  {
    if (this.c == null) {
      this.c = new be();
    }
    this.c.a = paramColorStateList;
    this.c.d = true;
    a();
  }
  
  public void setSupportBackgroundTintMode(PorterDuff.Mode paramMode)
  {
    if (this.c == null) {
      this.c = new be();
    }
    this.c.b = paramMode;
    this.c.c = true;
    a();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/s.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */