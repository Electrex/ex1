package android.support.v7.widget;

import android.content.Context;
import android.support.v4.view.ba;
import android.support.v4.view.ea;
import android.support.v4.widget.ag;
import android.support.v7.a.b;
import android.support.v7.internal.widget.aj;
import android.view.MotionEvent;
import android.view.View;

class bj
  extends aj
{
  private boolean f;
  private boolean g;
  private boolean h;
  private ea i;
  private ag j;
  
  public bj(Context paramContext, boolean paramBoolean)
  {
    super(paramContext, null, b.dropDownListViewStyle);
    this.g = paramBoolean;
    setCacheColorHint(0);
  }
  
  private void a(View paramView, int paramInt)
  {
    performItemClick(paramView, paramInt, getItemIdAtPosition(paramInt));
  }
  
  private void a(View paramView, int paramInt, float paramFloat1, float paramFloat2)
  {
    this.h = true;
    setPressed(true);
    layoutChildren();
    setSelection(paramInt);
    a(paramInt, paramView, paramFloat1, paramFloat2);
    setSelectorEnabled(false);
    refreshDrawableState();
  }
  
  private void d()
  {
    this.h = false;
    setPressed(false);
    drawableStateChanged();
    if (this.i != null)
    {
      this.i.a();
      this.i = null;
    }
  }
  
  public boolean a(MotionEvent paramMotionEvent, int paramInt)
  {
    int k = ba.a(paramMotionEvent);
    int n;
    boolean bool2;
    switch (k)
    {
    default: 
      n = 0;
    case 3: 
      for (bool2 = true;; bool2 = false)
      {
        label38:
        if ((!bool2) || (n != 0)) {
          d();
        }
        if (!bool2) {
          break;
        }
        if (this.j == null) {
          this.j = new ag(this);
        }
        this.j.a(true);
        this.j.onTouch(this, paramMotionEvent);
        label95:
        return bool2;
        n = 0;
      }
    }
    for (boolean bool1 = false;; bool1 = true)
    {
      int m = paramMotionEvent.findPointerIndex(paramInt);
      if (m < 0)
      {
        n = 0;
        bool2 = false;
        break label38;
      }
      int i1 = (int)paramMotionEvent.getX(m);
      int i2 = (int)paramMotionEvent.getY(m);
      int i3 = pointToPosition(i1, i2);
      if (i3 == -1)
      {
        bool2 = bool1;
        n = 1;
        break label38;
      }
      View localView = getChildAt(i3 - getFirstVisiblePosition());
      a(localView, i3, i1, i2);
      if (k != 1) {
        break;
      }
      a(localView, i3);
      break;
      if (this.j == null) {
        break label95;
      }
      this.j.a(false);
      return bool2;
    }
  }
  
  protected boolean c()
  {
    return (this.h) || (super.c());
  }
  
  public boolean hasFocus()
  {
    return (this.g) || (super.hasFocus());
  }
  
  public boolean hasWindowFocus()
  {
    return (this.g) || (super.hasWindowFocus());
  }
  
  public boolean isFocused()
  {
    return (this.g) || (super.isFocused());
  }
  
  public boolean isInTouchMode()
  {
    return ((this.g) && (this.f)) || (super.isInTouchMode());
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/bj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */