package android.support.v7.widget;

import android.support.v4.view.bv;
import android.util.Log;
import android.view.View;

public abstract class cz
{
  public final View a;
  int b = -1;
  int c = -1;
  long d = -1L;
  int e = -1;
  int f = -1;
  cz g = null;
  cz h = null;
  RecyclerView i;
  private int j;
  private int k = 0;
  private cq l = null;
  
  public cz(View paramView)
  {
    if (paramView == null) {
      throw new IllegalArgumentException("itemView may not be null");
    }
    this.a = paramView;
  }
  
  private boolean w()
  {
    return (0x10 & this.j) != 0;
  }
  
  private boolean x()
  {
    return ((0x10 & this.j) == 0) && (bv.c(this.a));
  }
  
  void a()
  {
    this.c = -1;
    this.f = -1;
  }
  
  void a(int paramInt1, int paramInt2)
  {
    this.j = (this.j & (paramInt2 ^ 0xFFFFFFFF) | paramInt1 & paramInt2);
  }
  
  void a(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    b(8);
    a(paramInt2, paramBoolean);
    this.b = paramInt1;
  }
  
  void a(int paramInt, boolean paramBoolean)
  {
    if (this.c == -1) {
      this.c = this.b;
    }
    if (this.f == -1) {
      this.f = this.b;
    }
    if (paramBoolean) {
      this.f = (paramInt + this.f);
    }
    this.b = (paramInt + this.b);
    if (this.a.getLayoutParams() != null) {
      ((cm)this.a.getLayoutParams()).c = true;
    }
  }
  
  void a(cq paramcq)
  {
    this.l = paramcq;
  }
  
  public final void a(boolean paramBoolean)
  {
    int m;
    if (paramBoolean)
    {
      m = -1 + this.k;
      this.k = m;
      if (this.k >= 0) {
        break label64;
      }
      this.k = 0;
      Log.e("View", "isRecyclable decremented below 0: unmatched pair of setIsRecyable() calls for " + this);
    }
    label64:
    do
    {
      return;
      m = 1 + this.k;
      break;
      if ((!paramBoolean) && (this.k == 1))
      {
        this.j = (0x10 | this.j);
        return;
      }
    } while ((!paramBoolean) || (this.k != 0));
    this.j = (0xFFFFFFEF & this.j);
  }
  
  boolean a(int paramInt)
  {
    return (paramInt & this.j) != 0;
  }
  
  void b()
  {
    if (this.c == -1) {
      this.c = this.b;
    }
  }
  
  void b(int paramInt)
  {
    this.j = (paramInt | this.j);
  }
  
  boolean c()
  {
    return (0x80 & this.j) != 0;
  }
  
  @Deprecated
  public final int d()
  {
    if (this.f == -1) {
      return this.b;
    }
    return this.f;
  }
  
  public final int e()
  {
    if (this.f == -1) {
      return this.b;
    }
    return this.f;
  }
  
  public final int f()
  {
    if (this.i == null) {
      return -1;
    }
    return RecyclerView.a(this.i, this);
  }
  
  public final long g()
  {
    return this.d;
  }
  
  public final int h()
  {
    return this.e;
  }
  
  boolean i()
  {
    return this.l != null;
  }
  
  void j()
  {
    this.l.d(this);
  }
  
  boolean k()
  {
    return (0x20 & this.j) != 0;
  }
  
  void l()
  {
    this.j = (0xFFFFFFDF & this.j);
  }
  
  void m()
  {
    this.j = (0xFEFF & this.j);
  }
  
  boolean n()
  {
    return (0x4 & this.j) != 0;
  }
  
  boolean o()
  {
    return (0x2 & this.j) != 0;
  }
  
  boolean p()
  {
    return (0x40 & this.j) != 0;
  }
  
  boolean q()
  {
    return (0x1 & this.j) != 0;
  }
  
  boolean r()
  {
    return (0x8 & this.j) != 0;
  }
  
  boolean s()
  {
    return (0x100 & this.j) != 0;
  }
  
  boolean t()
  {
    return ((0x200 & this.j) != 0) || (n());
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("ViewHolder{" + Integer.toHexString(hashCode()) + " position=" + this.b + " id=" + this.d + ", oldPos=" + this.c + ", pLpos:" + this.f);
    if (i()) {
      localStringBuilder.append(" scrap");
    }
    if (n()) {
      localStringBuilder.append(" invalid");
    }
    if (!q()) {
      localStringBuilder.append(" unbound");
    }
    if (o()) {
      localStringBuilder.append(" update");
    }
    if (r()) {
      localStringBuilder.append(" removed");
    }
    if (c()) {
      localStringBuilder.append(" ignored");
    }
    if (p()) {
      localStringBuilder.append(" changed");
    }
    if (s()) {
      localStringBuilder.append(" tmpDetached");
    }
    if (!v()) {
      localStringBuilder.append(" not recyclable(" + this.k + ")");
    }
    if (t()) {
      localStringBuilder.append("undefined adapter position");
    }
    if (this.a.getParent() == null) {
      localStringBuilder.append(" no parent");
    }
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
  
  void u()
  {
    this.j = 0;
    this.b = -1;
    this.c = -1;
    this.d = -1L;
    this.f = -1;
    this.k = 0;
    this.g = null;
    this.h = null;
  }
  
  public final boolean v()
  {
    return ((0x10 & this.j) == 0) && (!bv.c(this.a));
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/cz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */