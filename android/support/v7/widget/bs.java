package android.support.v7.widget;

import java.util.List;

class bs
{
  final bt a;
  
  public bs(bt parambt)
  {
    this.a = parambt;
  }
  
  private void a(List paramList, int paramInt1, int paramInt2)
  {
    r localr1 = (r)paramList.get(paramInt1);
    r localr2 = (r)paramList.get(paramInt2);
    switch (localr2.a)
    {
    default: 
      return;
    case 1: 
      a(paramList, paramInt1, localr1, paramInt2, localr2);
      return;
    case 0: 
      c(paramList, paramInt1, localr1, paramInt2, localr2);
      return;
    }
    b(paramList, paramInt1, localr1, paramInt2, localr2);
  }
  
  private int b(List paramList)
  {
    int i = 0;
    int j = -1 + paramList.size();
    if (j >= 0) {
      if (((r)paramList.get(j)).a == 3)
      {
        if (i == 0) {
          break label52;
        }
        return j;
      }
    }
    label52:
    for (int k = 1;; k = i)
    {
      j--;
      i = k;
      break;
      return -1;
    }
  }
  
  private void c(List paramList, int paramInt1, r paramr1, int paramInt2, r paramr2)
  {
    int i = paramr1.c;
    int j = paramr2.b;
    int k = 0;
    if (i < j) {
      k = -1;
    }
    if (paramr1.b < paramr2.b) {
      k++;
    }
    if (paramr2.b <= paramr1.b) {
      paramr1.b += paramr2.c;
    }
    if (paramr2.b <= paramr1.c) {
      paramr1.c += paramr2.c;
    }
    paramr2.b = (k + paramr2.b);
    paramList.set(paramInt1, paramr2);
    paramList.set(paramInt2, paramr1);
  }
  
  void a(List paramList)
  {
    for (;;)
    {
      int i = b(paramList);
      if (i == -1) {
        break;
      }
      a(paramList, i, i + 1);
    }
  }
  
  void a(List paramList, int paramInt1, r paramr1, int paramInt2, r paramr2)
  {
    int i = 0;
    int j;
    if (paramr1.b < paramr1.c)
    {
      if ((paramr2.b != paramr1.b) || (paramr2.c != paramr1.c - paramr1.b)) {
        break label614;
      }
      j = 1;
    }
    for (;;)
    {
      label70:
      r localr;
      if (paramr1.c < paramr2.b)
      {
        paramr2.b = (-1 + paramr2.b);
        if (paramr1.b > paramr2.b) {
          break label241;
        }
        paramr2.b = (1 + paramr2.b);
        localr = null;
      }
      for (;;)
      {
        label97:
        if (j != 0)
        {
          paramList.set(paramInt1, paramr2);
          paramList.remove(paramInt2);
          this.a.a(paramr1);
        }
        label241:
        label587:
        label597:
        for (;;)
        {
          return;
          if ((paramr2.b != 1 + paramr1.c) || (paramr2.c != paramr1.b - paramr1.c)) {
            break label605;
          }
          i = 1;
          j = 1;
          break;
          if (paramr1.c >= paramr2.b + paramr2.c) {
            break label70;
          }
          paramr2.c = (-1 + paramr2.c);
          paramr1.a = 1;
          paramr1.c = 1;
          if (paramr2.c == 0)
          {
            paramList.remove(paramInt2);
            this.a.a(paramr2);
            return;
            if (paramr1.b >= paramr2.b + paramr2.c) {
              break label599;
            }
            int k = paramr2.b + paramr2.c - paramr1.b;
            localr = this.a.a(1, 1 + paramr1.b, k);
            paramr2.c = (paramr1.b - paramr2.b);
            break label97;
            if (i != 0)
            {
              if (localr != null)
              {
                if (paramr1.b > localr.b) {
                  paramr1.b -= localr.c;
                }
                if (paramr1.c > localr.b) {
                  paramr1.c -= localr.c;
                }
              }
              if (paramr1.b > paramr2.b) {
                paramr1.b -= paramr2.c;
              }
              if (paramr1.c > paramr2.b) {
                paramr1.c -= paramr2.c;
              }
              paramList.set(paramInt1, paramr2);
              if (paramr1.b == paramr1.c) {
                break label587;
              }
              paramList.set(paramInt2, paramr1);
            }
            for (;;)
            {
              if (localr == null) {
                break label597;
              }
              paramList.add(paramInt1, localr);
              return;
              if (localr != null)
              {
                if (paramr1.b >= localr.b) {
                  paramr1.b -= localr.c;
                }
                if (paramr1.c >= localr.b) {
                  paramr1.c -= localr.c;
                }
              }
              if (paramr1.b >= paramr2.b) {
                paramr1.b -= paramr2.c;
              }
              if (paramr1.c < paramr2.b) {
                break;
              }
              paramr1.c -= paramr2.c;
              break;
              paramList.remove(paramInt2);
            }
          }
        }
        label599:
        localr = null;
      }
      label605:
      i = 1;
      j = 0;
      continue;
      label614:
      i = 0;
      j = 0;
    }
  }
  
  void b(List paramList, int paramInt1, r paramr1, int paramInt2, r paramr2)
  {
    Object localObject1 = null;
    Object localObject2;
    if (paramr1.c < paramr2.b)
    {
      paramr2.b = (-1 + paramr2.b);
      localObject2 = null;
    }
    for (;;)
    {
      if (paramr1.b <= paramr2.b)
      {
        paramr2.b = (1 + paramr2.b);
        label54:
        paramList.set(paramInt2, paramr1);
        if (paramr2.c <= 0) {
          break label244;
        }
        paramList.set(paramInt1, paramr2);
      }
      for (;;)
      {
        if (localObject2 != null) {
          paramList.add(paramInt1, localObject2);
        }
        if (localObject1 != null) {
          paramList.add(paramInt1, localObject1);
        }
        return;
        if (paramr1.c >= paramr2.b + paramr2.c) {
          break label266;
        }
        paramr2.c = (-1 + paramr2.c);
        localObject2 = this.a.a(2, paramr1.b, 1);
        break;
        int i = paramr1.b;
        int j = paramr2.b + paramr2.c;
        localObject1 = null;
        if (i >= j) {
          break label54;
        }
        int k = paramr2.b + paramr2.c - paramr1.b;
        localObject1 = this.a.a(2, 1 + paramr1.b, k);
        paramr2.c -= k;
        break label54;
        label244:
        paramList.remove(paramInt1);
        this.a.a(paramr2);
      }
      label266:
      localObject2 = null;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/bs.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */