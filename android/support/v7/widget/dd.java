package android.support.v7.widget;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.FillType;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.Drawable;
import android.support.v7.b.b;
import android.support.v7.b.c;

class dd
  extends Drawable
{
  static final double a = Math.cos(Math.toRadians(45.0D));
  static de c;
  final int b;
  Paint d;
  Paint e;
  Paint f;
  final RectF g;
  float h;
  Path i;
  float j;
  float k;
  float l;
  float m;
  private boolean n = true;
  private final int o;
  private final int p;
  private boolean q = true;
  private boolean r = false;
  
  dd(Resources paramResources, int paramInt, float paramFloat1, float paramFloat2, float paramFloat3)
  {
    this.o = paramResources.getColor(b.cardview_shadow_start_color);
    this.p = paramResources.getColor(b.cardview_shadow_end_color);
    this.b = paramResources.getDimensionPixelSize(c.cardview_compat_inset_shadow);
    this.d = new Paint(5);
    this.d.setColor(paramInt);
    this.e = new Paint(5);
    this.e.setStyle(Paint.Style.FILL);
    this.h = ((int)(0.5F + paramFloat1));
    this.g = new RectF();
    this.f = new Paint(this.e);
    this.f.setAntiAlias(false);
    a(paramFloat2, paramFloat3);
  }
  
  static float a(float paramFloat1, float paramFloat2, boolean paramBoolean)
  {
    if (paramBoolean) {
      return (float)(1.5F * paramFloat1 + (1.0D - a) * paramFloat2);
    }
    return 1.5F * paramFloat1;
  }
  
  private void a(Canvas paramCanvas)
  {
    float f1 = -this.h - this.l;
    float f2 = this.h + this.b + this.m / 2.0F;
    int i1;
    if (this.g.width() - 2.0F * f2 > 0.0F)
    {
      i1 = 1;
      if (this.g.height() - 2.0F * f2 <= 0.0F) {
        break label405;
      }
    }
    label405:
    for (int i2 = 1;; i2 = 0)
    {
      int i3 = paramCanvas.save();
      paramCanvas.translate(f2 + this.g.left, f2 + this.g.top);
      paramCanvas.drawPath(this.i, this.e);
      if (i1 != 0) {
        paramCanvas.drawRect(0.0F, f1, this.g.width() - 2.0F * f2, -this.h, this.f);
      }
      paramCanvas.restoreToCount(i3);
      int i4 = paramCanvas.save();
      paramCanvas.translate(this.g.right - f2, this.g.bottom - f2);
      paramCanvas.rotate(180.0F);
      paramCanvas.drawPath(this.i, this.e);
      if (i1 != 0) {
        paramCanvas.drawRect(0.0F, f1, this.g.width() - 2.0F * f2, -this.h + this.l, this.f);
      }
      paramCanvas.restoreToCount(i4);
      int i5 = paramCanvas.save();
      paramCanvas.translate(f2 + this.g.left, this.g.bottom - f2);
      paramCanvas.rotate(270.0F);
      paramCanvas.drawPath(this.i, this.e);
      if (i2 != 0) {
        paramCanvas.drawRect(0.0F, f1, this.g.height() - 2.0F * f2, -this.h, this.f);
      }
      paramCanvas.restoreToCount(i5);
      int i6 = paramCanvas.save();
      paramCanvas.translate(this.g.right - f2, f2 + this.g.top);
      paramCanvas.rotate(90.0F);
      paramCanvas.drawPath(this.i, this.e);
      if (i2 != 0) {
        paramCanvas.drawRect(0.0F, f1, this.g.height() - 2.0F * f2, -this.h, this.f);
      }
      paramCanvas.restoreToCount(i6);
      return;
      i1 = 0;
      break;
    }
  }
  
  static float b(float paramFloat1, float paramFloat2, boolean paramBoolean)
  {
    if (paramBoolean) {
      paramFloat1 = (float)(paramFloat1 + (1.0D - a) * paramFloat2);
    }
    return paramFloat1;
  }
  
  private void b(Rect paramRect)
  {
    float f1 = 1.5F * this.k;
    this.g.set(paramRect.left + this.k, f1 + paramRect.top, paramRect.right - this.k, paramRect.bottom - f1);
    f();
  }
  
  private int d(float paramFloat)
  {
    int i1 = (int)(0.5F + paramFloat);
    if (i1 % 2 == 1) {
      i1--;
    }
    return i1;
  }
  
  private void f()
  {
    RectF localRectF1 = new RectF(-this.h, -this.h, this.h, this.h);
    RectF localRectF2 = new RectF(localRectF1);
    localRectF2.inset(-this.l, -this.l);
    if (this.i == null) {
      this.i = new Path();
    }
    for (;;)
    {
      this.i.setFillType(Path.FillType.EVEN_ODD);
      this.i.moveTo(-this.h, 0.0F);
      this.i.rLineTo(-this.l, 0.0F);
      this.i.arcTo(localRectF2, 180.0F, 90.0F, false);
      this.i.arcTo(localRectF1, 270.0F, -90.0F, false);
      this.i.close();
      float f1 = this.h / (this.h + this.l);
      Paint localPaint1 = this.e;
      float f2 = this.h + this.l;
      int[] arrayOfInt1 = new int[3];
      arrayOfInt1[0] = this.o;
      arrayOfInt1[1] = this.o;
      arrayOfInt1[2] = this.p;
      localPaint1.setShader(new RadialGradient(0.0F, 0.0F, f2, arrayOfInt1, new float[] { 0.0F, f1, 1.0F }, Shader.TileMode.CLAMP));
      Paint localPaint2 = this.f;
      float f3 = -this.h + this.l;
      float f4 = -this.h - this.l;
      int[] arrayOfInt2 = new int[3];
      arrayOfInt2[0] = this.o;
      arrayOfInt2[1] = this.o;
      arrayOfInt2[2] = this.p;
      localPaint2.setShader(new LinearGradient(0.0F, f3, 0.0F, f4, arrayOfInt2, new float[] { 0.0F, 0.5F, 1.0F }, Shader.TileMode.CLAMP));
      this.f.setAntiAlias(false);
      return;
      this.i.reset();
    }
  }
  
  float a()
  {
    return this.h;
  }
  
  void a(float paramFloat)
  {
    float f1 = (int)(0.5F + paramFloat);
    if (this.h == f1) {
      return;
    }
    this.h = f1;
    this.n = true;
    invalidateSelf();
  }
  
  void a(float paramFloat1, float paramFloat2)
  {
    if ((paramFloat1 < 0.0F) || (paramFloat2 < 0.0F)) {
      throw new IllegalArgumentException("invalid shadow size");
    }
    float f1 = d(paramFloat1);
    float f2 = d(paramFloat2);
    if (f1 > f2)
    {
      if (!this.r) {
        this.r = true;
      }
      f1 = f2;
    }
    if ((this.m == f1) && (this.k == f2)) {
      return;
    }
    this.m = f1;
    this.k = f2;
    this.l = ((int)(0.5F + (f1 * 1.5F + this.b)));
    this.j = (f2 + this.b);
    this.n = true;
    invalidateSelf();
  }
  
  public void a(int paramInt)
  {
    this.d.setColor(paramInt);
    invalidateSelf();
  }
  
  void a(Rect paramRect)
  {
    getPadding(paramRect);
  }
  
  public void a(boolean paramBoolean)
  {
    this.q = paramBoolean;
    invalidateSelf();
  }
  
  float b()
  {
    return this.m;
  }
  
  void b(float paramFloat)
  {
    a(paramFloat, this.k);
  }
  
  float c()
  {
    return this.k;
  }
  
  void c(float paramFloat)
  {
    a(this.m, paramFloat);
  }
  
  float d()
  {
    return 2.0F * Math.max(this.k, this.h + this.b + this.k / 2.0F) + 2.0F * (this.k + this.b);
  }
  
  public void draw(Canvas paramCanvas)
  {
    if (this.n)
    {
      b(getBounds());
      this.n = false;
    }
    paramCanvas.translate(0.0F, this.m / 2.0F);
    a(paramCanvas);
    paramCanvas.translate(0.0F, -this.m / 2.0F);
    c.a(paramCanvas, this.g, this.h, this.d);
  }
  
  float e()
  {
    return 2.0F * Math.max(this.k, this.h + this.b + 1.5F * this.k / 2.0F) + 2.0F * (1.5F * this.k + this.b);
  }
  
  public int getOpacity()
  {
    return -3;
  }
  
  public boolean getPadding(Rect paramRect)
  {
    int i1 = (int)Math.ceil(a(this.k, this.h, this.q));
    int i2 = (int)Math.ceil(b(this.k, this.h, this.q));
    paramRect.set(i2, i1, i2, i1);
    return true;
  }
  
  protected void onBoundsChange(Rect paramRect)
  {
    super.onBoundsChange(paramRect);
    this.n = true;
  }
  
  public void setAlpha(int paramInt)
  {
    this.d.setAlpha(paramInt);
    this.e.setAlpha(paramInt);
    this.f.setAlpha(paramInt);
  }
  
  public void setColorFilter(ColorFilter paramColorFilter)
  {
    this.d.setColorFilter(paramColorFilter);
    this.e.setColorFilter(paramColorFilter);
    this.f.setColorFilter(paramColorFilter);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/dd.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */