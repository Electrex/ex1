package android.support.v7.widget;

import android.view.View;

final class bv
  extends bu
{
  bv(cl paramcl)
  {
    super(paramcl, null);
  }
  
  public int a(View paramView)
  {
    cm localcm = (cm)paramView.getLayoutParams();
    return this.a.g(paramView) - localcm.leftMargin;
  }
  
  public void a(int paramInt)
  {
    this.a.f(paramInt);
  }
  
  public int b(View paramView)
  {
    cm localcm = (cm)paramView.getLayoutParams();
    return this.a.i(paramView) + localcm.rightMargin;
  }
  
  public int c()
  {
    return this.a.s();
  }
  
  public int c(View paramView)
  {
    cm localcm = (cm)paramView.getLayoutParams();
    return this.a.e(paramView) + localcm.leftMargin + localcm.rightMargin;
  }
  
  public int d()
  {
    return this.a.q() - this.a.u();
  }
  
  public int d(View paramView)
  {
    cm localcm = (cm)paramView.getLayoutParams();
    return this.a.f(paramView) + localcm.topMargin + localcm.bottomMargin;
  }
  
  public int e()
  {
    return this.a.q();
  }
  
  public int f()
  {
    return this.a.q() - this.a.s() - this.a.u();
  }
  
  public int g()
  {
    return this.a.u();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/bv.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */