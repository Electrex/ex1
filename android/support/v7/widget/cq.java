package android.support.v7.widget;

import android.support.v4.view.bv;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class cq
{
  final ArrayList a = new ArrayList();
  final ArrayList b = new ArrayList();
  private ArrayList d = null;
  private final List e = Collections.unmodifiableList(this.a);
  private int f = 2;
  private cp g;
  private cx h;
  
  public cq(RecyclerView paramRecyclerView) {}
  
  private void a(ViewGroup paramViewGroup, boolean paramBoolean)
  {
    for (int i = -1 + paramViewGroup.getChildCount(); i >= 0; i--)
    {
      View localView = paramViewGroup.getChildAt(i);
      if ((localView instanceof ViewGroup)) {
        a((ViewGroup)localView, true);
      }
    }
    if (!paramBoolean) {
      return;
    }
    if (paramViewGroup.getVisibility() == 4)
    {
      paramViewGroup.setVisibility(0);
      paramViewGroup.setVisibility(4);
      return;
    }
    int j = paramViewGroup.getVisibility();
    paramViewGroup.setVisibility(4);
    paramViewGroup.setVisibility(j);
  }
  
  private void d(View paramView)
  {
    if ((RecyclerView.q(this.c) != null) && (RecyclerView.q(this.c).isEnabled()))
    {
      if (bv.e(paramView) == 0) {
        bv.c(paramView, 1);
      }
      if (!bv.b(paramView)) {
        bv.a(paramView, RecyclerView.r(this.c).b());
      }
    }
  }
  
  private void f(cz paramcz)
  {
    if ((paramcz.a instanceof ViewGroup)) {
      a((ViewGroup)paramcz.a, false);
    }
  }
  
  cz a(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    int i = 0;
    int j = this.a.size();
    int k = 0;
    cz localcz2;
    int m;
    if (k < j)
    {
      localcz2 = (cz)this.a.get(k);
      if ((localcz2.k()) || (localcz2.e() != paramInt1) || (localcz2.n()) || ((!cw.d(this.c.e)) && (localcz2.r()))) {
        break label269;
      }
      if ((paramInt2 != -1) && (localcz2.h() != paramInt2)) {
        Log.e("RecyclerView", "Scrap view for position " + paramInt1 + " isn't dirty but has" + " wrong view type! (found " + localcz2.h() + " but expected " + paramInt2 + ")");
      }
    }
    else
    {
      if (!paramBoolean)
      {
        View localView = this.c.c.a(paramInt1, paramInt2);
        if (localView != null) {
          this.c.d.c(this.c.a(localView));
        }
      }
      m = this.b.size();
    }
    for (;;)
    {
      if (i >= m) {
        break label281;
      }
      cz localcz1 = (cz)this.b.get(i);
      if ((!localcz1.n()) && (localcz1.e() == paramInt1))
      {
        if (!paramBoolean) {
          this.b.remove(i);
        }
        return localcz1;
        localcz2.b(32);
        return localcz2;
        label269:
        k++;
        break;
      }
      i++;
    }
    label281:
    return null;
  }
  
  cz a(long paramLong, int paramInt, boolean paramBoolean)
  {
    cz localcz;
    for (int i = -1 + this.a.size(); i >= 0; i--)
    {
      localcz = (cz)this.a.get(i);
      if ((localcz.g() == paramLong) && (!localcz.k()))
      {
        if (paramInt == localcz.h())
        {
          localcz.b(32);
          if ((localcz.r()) && (!this.c.e.a())) {
            localcz.a(2, 14);
          }
          return localcz;
        }
        if (!paramBoolean)
        {
          this.a.remove(i);
          this.c.removeDetachedView(localcz.a, false);
          b(localcz.a);
        }
      }
    }
    for (int j = -1 + this.b.size();; j--)
    {
      if (j < 0) {
        break label223;
      }
      localcz = (cz)this.b.get(j);
      if (localcz.g() == paramLong)
      {
        if (paramInt == localcz.h())
        {
          if (paramBoolean) {
            break;
          }
          this.b.remove(j);
          return localcz;
        }
        if (!paramBoolean) {
          c(j);
        }
      }
    }
    label223:
    return null;
  }
  
  View a(int paramInt, boolean paramBoolean)
  {
    boolean bool = true;
    if ((paramInt < 0) || (paramInt >= this.c.e.e())) {
      throw new IndexOutOfBoundsException("Invalid item position " + paramInt + "(" + paramInt + "). Item count:" + this.c.e.e());
    }
    int i3;
    int i;
    cz localcz1;
    if (this.c.e.a())
    {
      cz localcz4 = e(paramInt);
      if (localcz4 != null)
      {
        i3 = bool;
        i = i3;
        localcz1 = localcz4;
      }
    }
    for (;;)
    {
      label174:
      int j;
      if (localcz1 == null)
      {
        localcz1 = a(paramInt, -1, paramBoolean);
        if (localcz1 != null) {
          if (!a(localcz1)) {
            if (!paramBoolean)
            {
              localcz1.b(4);
              if (localcz1.i())
              {
                this.c.removeDetachedView(localcz1.a, false);
                localcz1.j();
                b(localcz1);
              }
            }
            else
            {
              localcz1 = null;
              j = i;
            }
          }
        }
      }
      for (;;)
      {
        cz localcz3;
        int k;
        if (localcz1 == null)
        {
          int i1 = this.c.b.a(paramInt);
          if ((i1 < 0) || (i1 >= RecyclerView.f(this.c).a()))
          {
            throw new IndexOutOfBoundsException("Inconsistency detected. Invalid item position " + paramInt + "(offset:" + i1 + ")." + "state:" + this.c.e.e());
            i3 = 0;
            break;
            if (!localcz1.k()) {
              break label174;
            }
            localcz1.l();
            break label174;
            j = bool;
            continue;
          }
          int i2 = RecyclerView.f(this.c).a(i1);
          if (RecyclerView.f(this.c).b())
          {
            localcz1 = a(RecyclerView.f(this.c).b(i1), i2, paramBoolean);
            if (localcz1 != null)
            {
              localcz1.b = i1;
              j = bool;
            }
          }
          if ((localcz1 == null) && (this.h != null))
          {
            View localView = this.h.a(this, paramInt, i2);
            if (localView != null)
            {
              localcz1 = this.c.a(localView);
              if (localcz1 == null) {
                throw new IllegalArgumentException("getViewForPositionAndType returned a view which does not have a ViewHolder");
              }
              if (localcz1.c()) {
                throw new IllegalArgumentException("getViewForPositionAndType returned a view that is ignored. You must call stopIgnoring before returning this view.");
              }
            }
          }
          if (localcz1 == null)
          {
            localcz1 = f().a(i2);
            if (localcz1 != null)
            {
              localcz1.u();
              if (RecyclerView.r()) {
                f(localcz1);
              }
            }
          }
          if (localcz1 == null)
          {
            localcz3 = RecyclerView.f(this.c).b(this.c, i2);
            k = j;
          }
        }
        for (cz localcz2 = localcz3;; localcz2 = localcz1)
        {
          int n;
          if ((this.c.e.a()) && (localcz2.q()))
          {
            localcz2.f = paramInt;
            n = 0;
          }
          for (;;)
          {
            ViewGroup.LayoutParams localLayoutParams = localcz2.a.getLayoutParams();
            cm localcm;
            if (localLayoutParams == null)
            {
              localcm = (cm)this.c.generateDefaultLayoutParams();
              localcz2.a.setLayoutParams(localcm);
              label590:
              localcm.a = localcz2;
              if ((k == 0) || (n == 0)) {
                break label762;
              }
            }
            for (;;)
            {
              localcm.d = bool;
              return localcz2.a;
              if ((localcz2.q()) && (!localcz2.o()) && (!localcz2.n())) {
                break label767;
              }
              int m = this.c.b.a(paramInt);
              localcz2.i = this.c;
              RecyclerView.f(this.c).b(localcz2, m);
              d(localcz2.a);
              if (this.c.e.a()) {
                localcz2.f = paramInt;
              }
              n = bool;
              break;
              if (!this.c.checkLayoutParams(localLayoutParams))
              {
                localcm = (cm)this.c.generateLayoutParams(localLayoutParams);
                localcz2.a.setLayoutParams(localcm);
                break label590;
              }
              localcm = (cm)localLayoutParams;
              break label590;
              label762:
              bool = false;
            }
            label767:
            n = 0;
          }
          k = j;
        }
        j = i;
      }
      localcz1 = null;
      i = 0;
    }
  }
  
  public void a()
  {
    this.a.clear();
    c();
  }
  
  public void a(int paramInt)
  {
    this.f = paramInt;
    for (int i = -1 + this.b.size(); (i >= 0) && (this.b.size() > paramInt); i--) {
      c(i);
    }
  }
  
  void a(int paramInt1, int paramInt2)
  {
    int i;
    int j;
    int k;
    int n;
    label25:
    cz localcz;
    if (paramInt1 < paramInt2)
    {
      i = -1;
      j = paramInt2;
      k = paramInt1;
      int m = this.b.size();
      n = 0;
      if (n >= m) {
        return;
      }
      localcz = (cz)this.b.get(n);
      if ((localcz != null) && (localcz.b >= k) && (localcz.b <= j)) {
        break label88;
      }
    }
    for (;;)
    {
      n++;
      break label25;
      i = 1;
      j = paramInt1;
      k = paramInt2;
      break;
      label88:
      if (localcz.b == paramInt1) {
        localcz.a(paramInt2 - paramInt1, false);
      } else {
        localcz.a(i, false);
      }
    }
  }
  
  void a(cc paramcc1, cc paramcc2, boolean paramBoolean)
  {
    a();
    f().a(paramcc1, paramcc2, paramBoolean);
  }
  
  void a(cp paramcp)
  {
    if (this.g != null) {
      this.g.b();
    }
    this.g = paramcp;
    if (paramcp != null) {
      this.g.a(this.c.getAdapter());
    }
  }
  
  void a(cx paramcx)
  {
    this.h = paramcx;
  }
  
  public void a(View paramView)
  {
    cz localcz = RecyclerView.b(paramView);
    if (localcz.s()) {
      this.c.removeDetachedView(paramView, false);
    }
    if (localcz.i()) {
      localcz.j();
    }
    for (;;)
    {
      b(localcz);
      return;
      if (localcz.k()) {
        localcz.l();
      }
    }
  }
  
  boolean a(cz paramcz)
  {
    if (paramcz.r()) {}
    do
    {
      return true;
      if ((paramcz.b < 0) || (paramcz.b >= RecyclerView.f(this.c).a())) {
        throw new IndexOutOfBoundsException("Inconsistency detected. Invalid view holder adapter position" + paramcz);
      }
      if ((!this.c.e.a()) && (RecyclerView.f(this.c).a(paramcz.b) != paramcz.h())) {
        return false;
      }
    } while ((!RecyclerView.f(this.c).b()) || (paramcz.g() == RecyclerView.f(this.c).b(paramcz.b)));
    return false;
  }
  
  public View b(int paramInt)
  {
    return a(paramInt, false);
  }
  
  public List b()
  {
    return this.e;
  }
  
  void b(int paramInt1, int paramInt2)
  {
    int i = this.b.size();
    for (int j = 0; j < i; j++)
    {
      cz localcz = (cz)this.b.get(j);
      if ((localcz != null) && (localcz.e() >= paramInt1)) {
        localcz.a(paramInt2, true);
      }
    }
  }
  
  void b(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    int i = paramInt1 + paramInt2;
    int j = -1 + this.b.size();
    if (j >= 0)
    {
      cz localcz = (cz)this.b.get(j);
      if (localcz != null)
      {
        if (localcz.e() < i) {
          break label64;
        }
        localcz.a(-paramInt2, paramBoolean);
      }
      for (;;)
      {
        j--;
        break;
        label64:
        if (localcz.e() >= paramInt1)
        {
          localcz.b(8);
          c(j);
        }
      }
    }
  }
  
  void b(cz paramcz)
  {
    boolean bool1 = true;
    if ((paramcz.i()) || (paramcz.a.getParent() != null))
    {
      StringBuilder localStringBuilder = new StringBuilder().append("Scrapped or attached views may not be recycled. isScrap:").append(paramcz.i()).append(" isAttached:");
      if (paramcz.a.getParent() != null) {}
      for (;;)
      {
        throw new IllegalArgumentException(bool1);
        bool1 = false;
      }
    }
    if (paramcz.s()) {
      throw new IllegalArgumentException("Tmp detached view should be removed from RecyclerView before it can be recycled: " + paramcz);
    }
    if (paramcz.c()) {
      throw new IllegalArgumentException("Trying to recycle an ignored view holder. You should first call stopIgnoringView(view) before calling recycle.");
    }
    boolean bool2 = cz.a(paramcz);
    boolean bool3;
    boolean bool4;
    label250:
    boolean bool6;
    boolean bool5;
    if ((RecyclerView.f(this.c) != null) && (bool2) && (RecyclerView.f(this.c).b(paramcz)))
    {
      bool3 = bool1;
      if ((!bool3) && (!paramcz.v())) {
        break label321;
      }
      if ((paramcz.n()) || (paramcz.r()) || (paramcz.p())) {
        break label315;
      }
      int i = this.b.size();
      if ((i == this.f) && (i > 0)) {
        c(0);
      }
      if (i >= this.f) {
        break label315;
      }
      this.b.add(paramcz);
      bool4 = bool1;
      if (bool4) {
        break label305;
      }
      c(paramcz);
      bool6 = bool1;
      bool5 = bool4;
    }
    for (;;)
    {
      this.c.e.a(paramcz);
      if ((!bool5) && (!bool6) && (bool2)) {
        paramcz.i = null;
      }
      return;
      bool3 = false;
      break;
      label305:
      bool5 = bool4;
      bool6 = false;
      continue;
      label315:
      bool4 = false;
      break label250;
      label321:
      bool5 = false;
      bool6 = false;
    }
  }
  
  void b(View paramView)
  {
    cz localcz = RecyclerView.b(paramView);
    cz.a(localcz, null);
    localcz.l();
    b(localcz);
  }
  
  void c()
  {
    for (int i = -1 + this.b.size(); i >= 0; i--) {
      c(i);
    }
    this.b.clear();
  }
  
  void c(int paramInt)
  {
    c((cz)this.b.get(paramInt));
    this.b.remove(paramInt);
  }
  
  void c(int paramInt1, int paramInt2)
  {
    int i = paramInt1 + paramInt2;
    int j = this.b.size();
    int k = 0;
    if (k < j)
    {
      cz localcz = (cz)this.b.get(k);
      if (localcz == null) {}
      for (;;)
      {
        k++;
        break;
        int m = localcz.e();
        if ((m >= paramInt1) && (m < i)) {
          localcz.b(2);
        }
      }
    }
  }
  
  void c(cz paramcz)
  {
    bv.a(paramcz.a, null);
    e(paramcz);
    paramcz.i = null;
    f().a(paramcz);
  }
  
  void c(View paramView)
  {
    cz localcz = RecyclerView.b(paramView);
    localcz.a(this);
    if ((!localcz.p()) || (!RecyclerView.h(this.c)))
    {
      if ((localcz.n()) && (!localcz.r()) && (!RecyclerView.f(this.c).b())) {
        throw new IllegalArgumentException("Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool.");
      }
      this.a.add(localcz);
      return;
    }
    if (this.d == null) {
      this.d = new ArrayList();
    }
    this.d.add(localcz);
  }
  
  int d()
  {
    return this.a.size();
  }
  
  View d(int paramInt)
  {
    return ((cz)this.a.get(paramInt)).a;
  }
  
  void d(cz paramcz)
  {
    if ((!paramcz.p()) || (!RecyclerView.h(this.c)) || (this.d == null)) {
      this.a.remove(paramcz);
    }
    for (;;)
    {
      cz.a(paramcz, null);
      paramcz.l();
      return;
      this.d.remove(paramcz);
    }
  }
  
  cz e(int paramInt)
  {
    int i = 0;
    int j;
    if (this.d != null)
    {
      j = this.d.size();
      if (j != 0) {}
    }
    else
    {
      return null;
    }
    for (int k = 0; k < j; k++)
    {
      cz localcz2 = (cz)this.d.get(k);
      if ((!localcz2.k()) && (localcz2.e() == paramInt))
      {
        localcz2.b(32);
        return localcz2;
      }
    }
    if (RecyclerView.f(this.c).b())
    {
      int m = this.c.b.a(paramInt);
      if ((m > 0) && (m < RecyclerView.f(this.c).a()))
      {
        long l = RecyclerView.f(this.c).b(m);
        while (i < j)
        {
          cz localcz1 = (cz)this.d.get(i);
          if ((!localcz1.k()) && (localcz1.g() == l))
          {
            localcz1.b(32);
            return localcz1;
          }
          i++;
        }
      }
    }
    return null;
  }
  
  void e()
  {
    this.a.clear();
  }
  
  void e(cz paramcz)
  {
    if (RecyclerView.s(this.c) != null) {
      RecyclerView.s(this.c).a(paramcz);
    }
    if (RecyclerView.f(this.c) != null) {
      RecyclerView.f(this.c).a(paramcz);
    }
    if (this.c.e != null) {
      this.c.e.a(paramcz);
    }
  }
  
  cp f()
  {
    if (this.g == null) {
      this.g = new cp();
    }
    return this.g;
  }
  
  void g()
  {
    int i = this.b.size();
    for (int j = 0; j < i; j++)
    {
      cz localcz = (cz)this.b.get(j);
      if (localcz != null) {
        localcz.b(512);
      }
    }
  }
  
  void h()
  {
    int i;
    int j;
    if ((RecyclerView.f(this.c) != null) && (RecyclerView.f(this.c).b()))
    {
      i = this.b.size();
      j = 0;
    }
    while (j < i)
    {
      cz localcz = (cz)this.b.get(j);
      if (localcz != null) {
        localcz.b(6);
      }
      j++;
      continue;
      c();
    }
  }
  
  void i()
  {
    int i = 0;
    int j = this.b.size();
    for (int k = 0; k < j; k++) {
      ((cz)this.b.get(k)).a();
    }
    int m = this.a.size();
    for (int n = 0; n < m; n++) {
      ((cz)this.a.get(n)).a();
    }
    if (this.d != null)
    {
      int i1 = this.d.size();
      while (i < i1)
      {
        ((cz)this.d.get(i)).a();
        i++;
      }
    }
  }
  
  void j()
  {
    int i = this.b.size();
    for (int j = 0; j < i; j++)
    {
      cm localcm = (cm)((cz)this.b.get(j)).a.getLayoutParams();
      if (localcm != null) {
        localcm.c = true;
      }
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/cq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */