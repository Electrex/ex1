package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.a.l;
import android.util.AttributeSet;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;

public class ba
  extends ViewGroup.MarginLayoutParams
{
  public float g;
  public int h = -1;
  
  public ba(int paramInt1, int paramInt2)
  {
    super(paramInt1, paramInt2);
    this.g = 0.0F;
  }
  
  public ba(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, l.LinearLayoutCompat_Layout);
    this.g = localTypedArray.getFloat(l.LinearLayoutCompat_Layout_android_layout_weight, 0.0F);
    this.h = localTypedArray.getInt(l.LinearLayoutCompat_Layout_android_layout_gravity, -1);
    localTypedArray.recycle();
  }
  
  public ba(ViewGroup.LayoutParams paramLayoutParams)
  {
    super(paramLayoutParams);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/ba.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */