package android.support.v7.widget;

import android.support.v4.view.bv;
import android.support.v4.view.ea;
import android.view.View;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class am
  extends cf
{
  private ArrayList a = new ArrayList();
  private ArrayList b = new ArrayList();
  private ArrayList c = new ArrayList();
  private ArrayList d = new ArrayList();
  private ArrayList e = new ArrayList();
  private ArrayList f = new ArrayList();
  private ArrayList g = new ArrayList();
  private ArrayList h = new ArrayList();
  private ArrayList i = new ArrayList();
  private ArrayList j = new ArrayList();
  private ArrayList k = new ArrayList();
  
  private void a(av paramav)
  {
    cz localcz1 = paramav.a;
    if (localcz1 == null) {}
    for (View localView1 = null;; localView1 = localcz1.a)
    {
      cz localcz2 = paramav.b;
      View localView2 = null;
      if (localcz2 != null) {
        localView2 = localcz2.a;
      }
      if (localView1 != null)
      {
        ea localea2 = bv.p(localView1).a(g());
        this.k.add(paramav.a);
        localea2.b(paramav.e - paramav.c);
        localea2.c(paramav.f - paramav.d);
        localea2.a(0.0F).a(new at(this, paramav, localea2)).b();
      }
      if (localView2 != null)
      {
        ea localea1 = bv.p(localView2);
        this.k.add(paramav.b);
        localea1.b(0.0F).c(0.0F).a(g()).a(1.0F).a(new au(this, paramav, localea1, localView2)).b();
      }
      return;
    }
  }
  
  private void a(List paramList, cz paramcz)
  {
    for (int m = -1 + paramList.size(); m >= 0; m--)
    {
      av localav = (av)paramList.get(m);
      if ((a(localav, paramcz)) && (localav.a == null) && (localav.b == null)) {
        paramList.remove(localav);
      }
    }
  }
  
  private boolean a(av paramav, cz paramcz)
  {
    boolean bool1 = false;
    if (paramav.b == paramcz) {
      paramav.b = null;
    }
    for (;;)
    {
      bv.c(paramcz.a, 1.0F);
      bv.a(paramcz.a, 0.0F);
      bv.b(paramcz.a, 0.0F);
      a(paramcz, bool1);
      boolean bool2 = true;
      cz localcz;
      do
      {
        return bool2;
        localcz = paramav.a;
        bool2 = false;
      } while (localcz != paramcz);
      paramav.a = null;
      bool1 = true;
    }
  }
  
  private void b(av paramav)
  {
    if (paramav.a != null) {
      a(paramav, paramav.a);
    }
    if (paramav.b != null) {
      a(paramav, paramav.b);
    }
  }
  
  private void b(cz paramcz, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    View localView = paramcz.a;
    int m = paramInt3 - paramInt1;
    int n = paramInt4 - paramInt2;
    if (m != 0) {
      bv.p(localView).b(0.0F);
    }
    if (n != 0) {
      bv.p(localView).c(0.0F);
    }
    ea localea = bv.p(localView);
    this.i.add(paramcz);
    localea.a(d()).a(new as(this, paramcz, m, n, localea)).b();
  }
  
  private void j()
  {
    if (!b()) {
      i();
    }
  }
  
  private void p(cz paramcz)
  {
    ea localea = bv.p(paramcz.a);
    this.j.add(paramcz);
    localea.a(f()).a(0.0F).a(new aq(this, paramcz, localea)).b();
  }
  
  private void q(cz paramcz)
  {
    ea localea = bv.p(paramcz.a);
    this.h.add(paramcz);
    localea.a(1.0F).a(e()).a(new ar(this, paramcz, localea)).b();
  }
  
  public void a()
  {
    int m;
    int n;
    label24:
    int i1;
    if (!this.a.isEmpty())
    {
      m = 1;
      if (this.c.isEmpty()) {
        break label72;
      }
      n = 1;
      if (this.d.isEmpty()) {
        break label77;
      }
      i1 = 1;
      label36:
      if (this.b.isEmpty()) {
        break label82;
      }
    }
    label72:
    label77:
    label82:
    for (int i2 = 1;; i2 = 0)
    {
      if ((m != 0) || (n != 0) || (i2 != 0) || (i1 != 0)) {
        break label88;
      }
      return;
      m = 0;
      break;
      n = 0;
      break label24;
      i1 = 0;
      break label36;
    }
    label88:
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext()) {
      p((cz)localIterator.next());
    }
    this.a.clear();
    an localan;
    label211:
    ao localao;
    label291:
    ArrayList localArrayList3;
    ap localap;
    long l1;
    label366:
    long l2;
    if (n != 0)
    {
      ArrayList localArrayList1 = new ArrayList();
      localArrayList1.addAll(this.c);
      this.f.add(localArrayList1);
      this.c.clear();
      localan = new an(this, localArrayList1);
      if (m != 0) {
        bv.a(((aw)localArrayList1.get(0)).a.a, localan, f());
      }
    }
    else
    {
      if (i1 != 0)
      {
        ArrayList localArrayList2 = new ArrayList();
        localArrayList2.addAll(this.d);
        this.g.add(localArrayList2);
        this.d.clear();
        localao = new ao(this, localArrayList2);
        if (m == 0) {
          break label428;
        }
        bv.a(((av)localArrayList2.get(0)).a.a, localao, f());
      }
      if (i2 == 0) {
        break label436;
      }
      localArrayList3 = new ArrayList();
      localArrayList3.addAll(this.b);
      this.e.add(localArrayList3);
      this.b.clear();
      localap = new ap(this, localArrayList3);
      if ((m == 0) && (n == 0) && (i1 == 0)) {
        break label456;
      }
      if (m == 0) {
        break label438;
      }
      l1 = f();
      if (n == 0) {
        break label444;
      }
      l2 = d();
      label376:
      if (i1 == 0) {
        break label450;
      }
    }
    label428:
    label436:
    label438:
    label444:
    label450:
    for (long l3 = g();; l3 = 0L)
    {
      long l4 = l1 + Math.max(l2, l3);
      bv.a(((cz)localArrayList3.get(0)).a, localap, l4);
      return;
      localan.run();
      break label211;
      localao.run();
      break label291;
      break;
      l1 = 0L;
      break label366;
      l2 = 0L;
      break label376;
    }
    label456:
    localap.run();
  }
  
  void a(List paramList)
  {
    for (int m = -1 + paramList.size(); m >= 0; m--) {
      bv.p(((cz)paramList.get(m)).a).a();
    }
  }
  
  public boolean a(cz paramcz)
  {
    c(paramcz);
    this.a.add(paramcz);
    return true;
  }
  
  public boolean a(cz paramcz, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    View localView = paramcz.a;
    int m = (int)(paramInt1 + bv.l(paramcz.a));
    int n = (int)(paramInt2 + bv.m(paramcz.a));
    c(paramcz);
    int i1 = paramInt3 - m;
    int i2 = paramInt4 - n;
    if ((i1 == 0) && (i2 == 0))
    {
      e(paramcz);
      return false;
    }
    if (i1 != 0) {
      bv.a(localView, -i1);
    }
    if (i2 != 0) {
      bv.b(localView, -i2);
    }
    this.c.add(new aw(paramcz, m, n, paramInt3, paramInt4, null));
    return true;
  }
  
  public boolean a(cz paramcz1, cz paramcz2, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    float f1 = bv.l(paramcz1.a);
    float f2 = bv.m(paramcz1.a);
    float f3 = bv.f(paramcz1.a);
    c(paramcz1);
    int m = (int)(paramInt3 - paramInt1 - f1);
    int n = (int)(paramInt4 - paramInt2 - f2);
    bv.a(paramcz1.a, f1);
    bv.b(paramcz1.a, f2);
    bv.c(paramcz1.a, f3);
    if ((paramcz2 != null) && (paramcz2.a != null))
    {
      c(paramcz2);
      bv.a(paramcz2.a, -m);
      bv.b(paramcz2.a, -n);
      bv.c(paramcz2.a, 0.0F);
    }
    this.d.add(new av(paramcz1, paramcz2, paramInt1, paramInt2, paramInt3, paramInt4, null));
    return true;
  }
  
  public boolean b()
  {
    return (!this.b.isEmpty()) || (!this.d.isEmpty()) || (!this.c.isEmpty()) || (!this.a.isEmpty()) || (!this.i.isEmpty()) || (!this.j.isEmpty()) || (!this.h.isEmpty()) || (!this.k.isEmpty()) || (!this.f.isEmpty()) || (!this.e.isEmpty()) || (!this.g.isEmpty());
  }
  
  public boolean b(cz paramcz)
  {
    c(paramcz);
    bv.c(paramcz.a, 0.0F);
    this.b.add(paramcz);
    return true;
  }
  
  public void c()
  {
    for (int m = -1 + this.c.size(); m >= 0; m--)
    {
      aw localaw2 = (aw)this.c.get(m);
      View localView2 = localaw2.a.a;
      bv.b(localView2, 0.0F);
      bv.a(localView2, 0.0F);
      e(localaw2.a);
      this.c.remove(m);
    }
    for (int n = -1 + this.a.size(); n >= 0; n--)
    {
      d((cz)this.a.get(n));
      this.a.remove(n);
    }
    for (int i1 = -1 + this.b.size(); i1 >= 0; i1--)
    {
      cz localcz2 = (cz)this.b.get(i1);
      bv.c(localcz2.a, 1.0F);
      f(localcz2);
      this.b.remove(i1);
    }
    for (int i2 = -1 + this.d.size(); i2 >= 0; i2--) {
      b((av)this.d.get(i2));
    }
    this.d.clear();
    if (!b()) {
      return;
    }
    for (int i3 = -1 + this.f.size(); i3 >= 0; i3--)
    {
      ArrayList localArrayList3 = (ArrayList)this.f.get(i3);
      for (int i8 = -1 + localArrayList3.size(); i8 >= 0; i8--)
      {
        aw localaw1 = (aw)localArrayList3.get(i8);
        View localView1 = localaw1.a.a;
        bv.b(localView1, 0.0F);
        bv.a(localView1, 0.0F);
        e(localaw1.a);
        localArrayList3.remove(i8);
        if (localArrayList3.isEmpty()) {
          this.f.remove(localArrayList3);
        }
      }
    }
    for (int i4 = -1 + this.e.size(); i4 >= 0; i4--)
    {
      ArrayList localArrayList2 = (ArrayList)this.e.get(i4);
      for (int i7 = -1 + localArrayList2.size(); i7 >= 0; i7--)
      {
        cz localcz1 = (cz)localArrayList2.get(i7);
        bv.c(localcz1.a, 1.0F);
        f(localcz1);
        localArrayList2.remove(i7);
        if (localArrayList2.isEmpty()) {
          this.e.remove(localArrayList2);
        }
      }
    }
    for (int i5 = -1 + this.g.size(); i5 >= 0; i5--)
    {
      ArrayList localArrayList1 = (ArrayList)this.g.get(i5);
      for (int i6 = -1 + localArrayList1.size(); i6 >= 0; i6--)
      {
        b((av)localArrayList1.get(i6));
        if (localArrayList1.isEmpty()) {
          this.g.remove(localArrayList1);
        }
      }
    }
    a(this.j);
    a(this.i);
    a(this.h);
    a(this.k);
    i();
  }
  
  public void c(cz paramcz)
  {
    View localView = paramcz.a;
    bv.p(localView).a();
    for (int m = -1 + this.c.size(); m >= 0; m--) {
      if (((aw)this.c.get(m)).a == paramcz)
      {
        bv.b(localView, 0.0F);
        bv.a(localView, 0.0F);
        e(paramcz);
        this.c.remove(m);
      }
    }
    a(this.d, paramcz);
    if (this.a.remove(paramcz))
    {
      bv.c(localView, 1.0F);
      d(paramcz);
    }
    if (this.b.remove(paramcz))
    {
      bv.c(localView, 1.0F);
      f(paramcz);
    }
    for (int n = -1 + this.g.size(); n >= 0; n--)
    {
      ArrayList localArrayList3 = (ArrayList)this.g.get(n);
      a(localArrayList3, paramcz);
      if (localArrayList3.isEmpty()) {
        this.g.remove(n);
      }
    }
    int i1 = -1 + this.f.size();
    if (i1 >= 0)
    {
      ArrayList localArrayList2 = (ArrayList)this.f.get(i1);
      for (int i3 = -1 + localArrayList2.size();; i3--)
      {
        if (i3 >= 0)
        {
          if (((aw)localArrayList2.get(i3)).a != paramcz) {
            continue;
          }
          bv.b(localView, 0.0F);
          bv.a(localView, 0.0F);
          e(paramcz);
          localArrayList2.remove(i3);
          if (localArrayList2.isEmpty()) {
            this.f.remove(i1);
          }
        }
        i1--;
        break;
      }
    }
    for (int i2 = -1 + this.e.size(); i2 >= 0; i2--)
    {
      ArrayList localArrayList1 = (ArrayList)this.e.get(i2);
      if (localArrayList1.remove(paramcz))
      {
        bv.c(localView, 1.0F);
        f(paramcz);
        if (localArrayList1.isEmpty()) {
          this.e.remove(i2);
        }
      }
    }
    if ((!this.j.remove(paramcz)) || ((!this.h.remove(paramcz)) || ((!this.k.remove(paramcz)) || (this.i.remove(paramcz))))) {}
    j();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/am.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */