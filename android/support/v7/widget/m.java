package android.support.v7.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup.LayoutParams;

public class m
  extends ba
{
  @ViewDebug.ExportedProperty
  public boolean a;
  @ViewDebug.ExportedProperty
  public int b;
  @ViewDebug.ExportedProperty
  public int c;
  @ViewDebug.ExportedProperty
  public boolean d;
  @ViewDebug.ExportedProperty
  public boolean e;
  boolean f;
  
  public m(int paramInt1, int paramInt2)
  {
    super(paramInt1, paramInt2);
    this.a = false;
  }
  
  public m(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public m(m paramm)
  {
    super(paramm);
    this.a = paramm.a;
  }
  
  public m(ViewGroup.LayoutParams paramLayoutParams)
  {
    super(paramLayoutParams);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/m.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */