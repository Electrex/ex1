package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v4.view.n;
import android.support.v4.view.o;
import android.support.v7.internal.view.menu.ActionMenuItemView;
import android.support.v7.internal.view.menu.aa;
import android.support.v7.internal.view.menu.ad;
import android.support.v7.internal.view.menu.m;
import android.support.v7.internal.view.menu.v;
import android.support.v7.internal.view.menu.z;
import android.util.DisplayMetrics;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import java.util.ArrayList;

public class ActionMenuPresenter
  extends android.support.v7.internal.view.menu.d
  implements o
{
  final h g = new h(this, null);
  int h;
  private View i;
  private boolean j;
  private boolean k;
  private int l;
  private int m;
  private int n;
  private boolean o;
  private boolean p;
  private boolean q;
  private boolean r;
  private int s;
  private final SparseBooleanArray t = new SparseBooleanArray();
  private View u;
  private g v;
  private b w;
  private d x;
  private c y;
  
  public ActionMenuPresenter(Context paramContext)
  {
    super(paramContext, android.support.v7.a.i.abc_action_menu_layout, android.support.v7.a.i.abc_action_menu_item_layout);
  }
  
  private View a(MenuItem paramMenuItem)
  {
    ViewGroup localViewGroup = (ViewGroup)this.f;
    View localView;
    if (localViewGroup == null)
    {
      localView = null;
      return localView;
    }
    int i1 = localViewGroup.getChildCount();
    for (int i2 = 0;; i2++)
    {
      if (i2 >= i1) {
        break label68;
      }
      localView = localViewGroup.getChildAt(i2);
      if (((localView instanceof aa)) && (((aa)localView).getItemData() == paramMenuItem)) {
        break;
      }
    }
    label68:
    return null;
  }
  
  public z a(ViewGroup paramViewGroup)
  {
    z localz = super.a(paramViewGroup);
    ((ActionMenuView)localz).setPresenter(this);
    return localz;
  }
  
  public View a(m paramm, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramm.getActionView();
    if ((localView == null) || (paramm.n())) {
      localView = super.a(paramm, paramView, paramViewGroup);
    }
    if (paramm.isActionViewExpanded()) {}
    for (int i1 = 8;; i1 = 0)
    {
      localView.setVisibility(i1);
      ActionMenuView localActionMenuView = (ActionMenuView)paramViewGroup;
      ViewGroup.LayoutParams localLayoutParams = localView.getLayoutParams();
      if (!localActionMenuView.checkLayoutParams(localLayoutParams)) {
        localView.setLayoutParams(localActionMenuView.a(localLayoutParams));
      }
      return localView;
    }
  }
  
  public void a(int paramInt, boolean paramBoolean)
  {
    this.l = paramInt;
    this.p = paramBoolean;
    this.q = true;
  }
  
  public void a(Context paramContext, android.support.v7.internal.view.menu.i parami)
  {
    super.a(paramContext, parami);
    Resources localResources = paramContext.getResources();
    android.support.v7.internal.view.a locala = android.support.v7.internal.view.a.a(paramContext);
    if (!this.k) {
      this.j = locala.b();
    }
    if (!this.q) {
      this.l = locala.c();
    }
    if (!this.o) {
      this.n = locala.a();
    }
    int i1 = this.l;
    if (this.j)
    {
      if (this.i == null)
      {
        this.i = new e(this, this.a);
        int i2 = View.MeasureSpec.makeMeasureSpec(0, 0);
        this.i.measure(i2, i2);
      }
      i1 -= this.i.getMeasuredWidth();
    }
    for (;;)
    {
      this.m = i1;
      this.s = ((int)(56.0F * localResources.getDisplayMetrics().density));
      this.u = null;
      return;
      this.i = null;
    }
  }
  
  public void a(Configuration paramConfiguration)
  {
    if (!this.o) {
      this.n = this.b.getResources().getInteger(android.support.v7.a.h.abc_max_action_buttons);
    }
    if (this.c != null) {
      this.c.b(true);
    }
  }
  
  public void a(android.support.v7.internal.view.menu.i parami, boolean paramBoolean)
  {
    e();
    super.a(parami, paramBoolean);
  }
  
  public void a(m paramm, aa paramaa)
  {
    paramaa.a(paramm, 0);
    ActionMenuView localActionMenuView = (ActionMenuView)this.f;
    ActionMenuItemView localActionMenuItemView = (ActionMenuItemView)paramaa;
    localActionMenuItemView.setItemInvoker(localActionMenuView);
    if (this.y == null) {
      this.y = new c(this, null);
    }
    localActionMenuItemView.setPopupCallback(this.y);
  }
  
  public void a(ActionMenuView paramActionMenuView)
  {
    this.f = paramActionMenuView;
    paramActionMenuView.a(this.c);
  }
  
  public void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      super.a(null);
      return;
    }
    this.c.a(false);
  }
  
  public boolean a(int paramInt, m paramm)
  {
    return paramm.j();
  }
  
  public boolean a(ad paramad)
  {
    if (!paramad.hasVisibleItems()) {
      return false;
    }
    for (ad localad = paramad; localad.s() != this.c; localad = (ad)localad.s()) {}
    View localView = a(localad.getItem());
    if (localView == null)
    {
      if (this.i == null) {
        return false;
      }
      localView = this.i;
    }
    this.h = paramad.getItem().getItemId();
    this.w = new b(this, this.b, paramad);
    this.w.a(localView);
    this.w.a();
    super.a(paramad);
    return true;
  }
  
  public boolean a(ViewGroup paramViewGroup, int paramInt)
  {
    if (paramViewGroup.getChildAt(paramInt) == this.i) {
      return false;
    }
    return super.a(paramViewGroup, paramInt);
  }
  
  public void b(int paramInt)
  {
    this.n = paramInt;
    this.o = true;
  }
  
  public void b(boolean paramBoolean)
  {
    int i1 = 1;
    ViewGroup localViewGroup1 = (ViewGroup)((View)this.f).getParent();
    if (localViewGroup1 != null) {
      android.support.v7.internal.c.a.a(localViewGroup1);
    }
    super.b(paramBoolean);
    ((View)this.f).requestLayout();
    if (this.c != null)
    {
      ArrayList localArrayList2 = this.c.k();
      int i5 = localArrayList2.size();
      for (int i6 = 0; i6 < i5; i6++)
      {
        n localn = ((m)localArrayList2.get(i6)).a();
        if (localn != null) {
          localn.a(this);
        }
      }
    }
    ArrayList localArrayList1;
    int i2;
    int i3;
    int i4;
    if (this.c != null)
    {
      localArrayList1 = this.c.l();
      boolean bool = this.j;
      i2 = 0;
      if (bool)
      {
        i2 = 0;
        if (localArrayList1 != null)
        {
          i3 = localArrayList1.size();
          if (i3 != i1) {
            break label290;
          }
          if (((m)localArrayList1.get(0)).isActionViewExpanded()) {
            break label284;
          }
          i4 = i1;
          label173:
          i2 = i4;
        }
      }
      if (i2 == 0) {
        break label306;
      }
      if (this.i == null) {
        this.i = new e(this, this.a);
      }
      ViewGroup localViewGroup2 = (ViewGroup)this.i.getParent();
      if (localViewGroup2 != this.f)
      {
        if (localViewGroup2 != null) {
          localViewGroup2.removeView(this.i);
        }
        ActionMenuView localActionMenuView = (ActionMenuView)this.f;
        localActionMenuView.addView(this.i, localActionMenuView.c());
      }
    }
    for (;;)
    {
      ((ActionMenuView)this.f).setOverflowReserved(this.j);
      return;
      localArrayList1 = null;
      break;
      label284:
      i4 = 0;
      break label173;
      label290:
      if (i3 > 0) {}
      for (;;)
      {
        i2 = i1;
        break;
        i1 = 0;
      }
      label306:
      if ((this.i != null) && (this.i.getParent() == this.f)) {
        ((ViewGroup)this.f).removeView(this.i);
      }
    }
  }
  
  public boolean b()
  {
    ArrayList localArrayList = this.c.i();
    int i1 = localArrayList.size();
    int i2 = this.n;
    int i3 = this.m;
    int i4 = View.MeasureSpec.makeMeasureSpec(0, 0);
    ViewGroup localViewGroup = (ViewGroup)this.f;
    int i5 = 0;
    int i6 = 0;
    int i7 = 0;
    int i8 = 0;
    m localm3;
    if (i8 < i1)
    {
      localm3 = (m)localArrayList.get(i8);
      if (localm3.l())
      {
        i5++;
        label80:
        if ((!this.r) || (!localm3.isActionViewExpanded())) {
          break label830;
        }
      }
    }
    label291:
    label428:
    label496:
    label500:
    label538:
    label619:
    label643:
    label649:
    label792:
    label799:
    label814:
    label830:
    for (int i36 = 0;; i36 = i2)
    {
      i8++;
      i2 = i36;
      break;
      if (localm3.k())
      {
        i6++;
        break label80;
      }
      i7 = 1;
      break label80;
      if ((this.j) && ((i7 != 0) || (i5 + i6 > i2))) {
        i2--;
      }
      int i9 = i2 - i5;
      SparseBooleanArray localSparseBooleanArray = this.t;
      localSparseBooleanArray.clear();
      int i10;
      int i35;
      if (this.p)
      {
        i10 = i3 / this.s;
        i35 = i3 % this.s;
      }
      for (int i11 = this.s + i35 / i10;; i11 = 0)
      {
        int i12 = 0;
        int i13 = 0;
        int i14 = i10;
        m localm1;
        View localView2;
        int i15;
        int i33;
        if (i12 < i1)
        {
          localm1 = (m)localArrayList.get(i12);
          if (localm1.l())
          {
            localView2 = a(localm1, this.u, localViewGroup);
            if (this.u == null) {
              this.u = localView2;
            }
            if (this.p)
            {
              i14 -= ActionMenuView.a(localView2, i11, i14, i4, 0);
              i15 = localView2.getMeasuredWidth();
              i33 = i3 - i15;
              if (i13 != 0) {
                break label814;
              }
            }
          }
        }
        for (;;)
        {
          int i34 = localm1.getGroupId();
          if (i34 != 0) {
            localSparseBooleanArray.put(i34, true);
          }
          localm1.d(true);
          int i16 = i33;
          int i17 = i9;
          i12++;
          i3 = i16;
          i9 = i17;
          i13 = i15;
          break;
          localView2.measure(i4, i4);
          break label291;
          int i18;
          boolean bool1;
          boolean bool2;
          View localView1;
          int i27;
          int i26;
          int i30;
          boolean bool3;
          int i19;
          int i20;
          if (localm1.k())
          {
            i18 = localm1.getGroupId();
            bool1 = localSparseBooleanArray.get(i18);
            if (((i9 > 0) || (bool1)) && (i3 > 0) && ((!this.p) || (i14 > 0)))
            {
              bool2 = true;
              if (!bool2) {
                break label799;
              }
              localView1 = a(localm1, this.u, localViewGroup);
              if (this.u == null) {
                this.u = localView1;
              }
              if (!this.p) {
                break label619;
              }
              int i31 = ActionMenuView.a(localView1, i11, i14, i4, 0);
              int i32 = i14 - i31;
              if (i31 != 0) {
                break label792;
              }
              i27 = 0;
              i26 = i32;
              int i28 = localView1.getMeasuredWidth();
              i3 -= i28;
              if (i13 == 0) {
                i13 = i28;
              }
              if (!this.p) {
                break label649;
              }
              if (i3 < 0) {
                break label643;
              }
              i30 = 1;
              bool3 = i27 & i30;
              i19 = i13;
              i20 = i26;
            }
          }
          for (;;)
          {
            int i21;
            if ((bool3) && (i18 != 0))
            {
              localSparseBooleanArray.put(i18, true);
              i21 = i9;
            }
            for (;;)
            {
              if (bool3) {
                i21--;
              }
              localm1.d(bool3);
              i15 = i19;
              i16 = i3;
              int i22 = i20;
              i17 = i21;
              i14 = i22;
              break;
              bool2 = false;
              break label428;
              localView1.measure(i4, i4);
              int i25 = bool2;
              i26 = i14;
              i27 = i25;
              break label500;
              i30 = 0;
              break label538;
              if (i3 + i13 > 0) {}
              for (int i29 = 1;; i29 = 0)
              {
                bool3 = i27 & i29;
                i19 = i13;
                i20 = i26;
                break;
              }
              if (bool1)
              {
                localSparseBooleanArray.put(i18, false);
                int i23 = i9;
                int i24 = 0;
                for (;;)
                {
                  if (i24 < i12)
                  {
                    m localm2 = (m)localArrayList.get(i24);
                    if (localm2.getGroupId() == i18)
                    {
                      if (localm2.j()) {
                        i23++;
                      }
                      localm2.d(false);
                    }
                    i24++;
                    continue;
                    localm1.d(false);
                    i15 = i13;
                    i16 = i3;
                    i17 = i9;
                    break;
                    return true;
                  }
                }
                i21 = i23;
              }
              else
              {
                i21 = i9;
              }
            }
            i27 = bool2;
            break label496;
            bool3 = bool2;
            i19 = i13;
            i20 = i14;
          }
          i15 = i13;
        }
        i10 = 0;
      }
    }
  }
  
  public void c(boolean paramBoolean)
  {
    this.j = paramBoolean;
    this.k = true;
  }
  
  public boolean c()
  {
    if ((this.j) && (!g()) && (this.c != null) && (this.f != null) && (this.x == null) && (!this.c.l().isEmpty()))
    {
      this.x = new d(this, new g(this, this.b, this.c, this.i, true));
      ((View)this.f).post(this.x);
      super.a(null);
      return true;
    }
    return false;
  }
  
  public void d(boolean paramBoolean)
  {
    this.r = paramBoolean;
  }
  
  public boolean d()
  {
    if ((this.x != null) && (this.f != null))
    {
      ((View)this.f).removeCallbacks(this.x);
      this.x = null;
      return true;
    }
    g localg = this.v;
    if (localg != null)
    {
      localg.e();
      return true;
    }
    return false;
  }
  
  public boolean e()
  {
    return d() | f();
  }
  
  public boolean f()
  {
    if (this.w != null)
    {
      this.w.e();
      return true;
    }
    return false;
  }
  
  public boolean g()
  {
    return (this.v != null) && (this.v.f());
  }
  
  public boolean h()
  {
    return (this.x != null) || (g());
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/ActionMenuPresenter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */