package android.support.v7.widget;

class av
{
  public cz a;
  public cz b;
  public int c;
  public int d;
  public int e;
  public int f;
  
  private av(cz paramcz1, cz paramcz2)
  {
    this.a = paramcz1;
    this.b = paramcz2;
  }
  
  private av(cz paramcz1, cz paramcz2, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this(paramcz1, paramcz2);
    this.c = paramInt1;
    this.d = paramInt2;
    this.e = paramInt3;
    this.f = paramInt4;
  }
  
  public String toString()
  {
    return "ChangeInfo{oldHolder=" + this.a + ", newHolder=" + this.b + ", fromX=" + this.c + ", fromY=" + this.d + ", toX=" + this.e + ", toY=" + this.f + '}';
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/av.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */