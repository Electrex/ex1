package android.support.v7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.View;

class ae
  implements ag
{
  final RectF a = new RectF();
  
  private dd i(ad paramad)
  {
    return (dd)paramad.getBackground();
  }
  
  public float a(ad paramad)
  {
    return i(paramad).c();
  }
  
  dd a(Context paramContext, int paramInt, float paramFloat1, float paramFloat2, float paramFloat3)
  {
    return new dd(paramContext.getResources(), paramInt, paramFloat1, paramFloat2, paramFloat3);
  }
  
  public void a()
  {
    dd.c = new af(this);
  }
  
  public void a(ad paramad, float paramFloat)
  {
    i(paramad).a(paramFloat);
    f(paramad);
  }
  
  public void a(ad paramad, int paramInt)
  {
    i(paramad).a(paramInt);
  }
  
  public void a(ad paramad, Context paramContext, int paramInt, float paramFloat1, float paramFloat2, float paramFloat3)
  {
    dd localdd = a(paramContext, paramInt, paramFloat1, paramFloat2, paramFloat3);
    localdd.a(paramad.getPreventCornerOverlap());
    paramad.setBackgroundDrawable(localdd);
    f(paramad);
  }
  
  public float b(ad paramad)
  {
    return i(paramad).d();
  }
  
  public void b(ad paramad, float paramFloat)
  {
    i(paramad).c(paramFloat);
    f(paramad);
  }
  
  public float c(ad paramad)
  {
    return i(paramad).e();
  }
  
  public void c(ad paramad, float paramFloat)
  {
    i(paramad).b(paramFloat);
  }
  
  public float d(ad paramad)
  {
    return i(paramad).a();
  }
  
  public float e(ad paramad)
  {
    return i(paramad).b();
  }
  
  public void f(ad paramad)
  {
    Rect localRect = new Rect();
    i(paramad).a(localRect);
    ((View)paramad).setMinimumHeight((int)Math.ceil(c(paramad)));
    ((View)paramad).setMinimumWidth((int)Math.ceil(b(paramad)));
    paramad.a(localRect.left, localRect.top, localRect.right, localRect.bottom);
  }
  
  public void g(ad paramad) {}
  
  public void h(ad paramad)
  {
    i(paramad).a(paramad.getPreventCornerOverlap());
    f(paramad);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/ae.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */