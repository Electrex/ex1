package android.support.v7.widget;

import android.view.View;
import android.view.ViewGroup.LayoutParams;
import java.util.ArrayList;
import java.util.List;

class aj
{
  final al a;
  final ak b;
  final List c;
  
  aj(al paramal)
  {
    this.a = paramal;
    this.b = new ak();
    this.c = new ArrayList();
  }
  
  private int e(int paramInt)
  {
    if (paramInt < 0)
    {
      j = -1;
      return j;
    }
    int i = this.a.a();
    int j = paramInt;
    for (;;)
    {
      if (j >= i) {
        break label69;
      }
      int k = paramInt - (j - this.b.e(j));
      if (k == 0)
      {
        while (this.b.c(j)) {
          j++;
        }
        break;
      }
      j += k;
    }
    label69:
    return -1;
  }
  
  View a(int paramInt1, int paramInt2)
  {
    int i = this.c.size();
    for (int j = 0; j < i; j++)
    {
      View localView = (View)this.c.get(j);
      cz localcz = this.a.b(localView);
      if ((localcz.e() == paramInt1) && (!localcz.n()) && ((paramInt2 == -1) || (localcz.h() == paramInt2))) {
        return localView;
      }
    }
    return null;
  }
  
  void a()
  {
    this.b.a();
    this.c.clear();
    this.a.b();
  }
  
  void a(int paramInt)
  {
    int i = e(paramInt);
    View localView = this.a.b(i);
    if (localView == null) {
      return;
    }
    if (this.b.d(i)) {
      this.c.remove(localView);
    }
    this.a.a(i);
  }
  
  void a(View paramView)
  {
    int i = this.a.a(paramView);
    if (i < 0) {
      return;
    }
    if (this.b.d(i)) {
      this.c.remove(paramView);
    }
    this.a.a(i);
  }
  
  void a(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams, boolean paramBoolean)
  {
    if (paramInt < 0) {}
    for (int i = this.a.a();; i = e(paramInt))
    {
      this.b.a(i, paramBoolean);
      if (paramBoolean) {
        this.c.add(paramView);
      }
      this.a.a(paramView, i, paramLayoutParams);
      return;
    }
  }
  
  void a(View paramView, int paramInt, boolean paramBoolean)
  {
    if (paramInt < 0) {}
    for (int i = this.a.a();; i = e(paramInt))
    {
      this.b.a(i, paramBoolean);
      if (paramBoolean) {
        this.c.add(paramView);
      }
      this.a.a(paramView, i);
      return;
    }
  }
  
  void a(View paramView, boolean paramBoolean)
  {
    a(paramView, -1, paramBoolean);
  }
  
  int b()
  {
    return this.a.a() - this.c.size();
  }
  
  int b(View paramView)
  {
    int i = this.a.a(paramView);
    if (i == -1) {}
    while (this.b.c(i)) {
      return -1;
    }
    return i - this.b.e(i);
  }
  
  View b(int paramInt)
  {
    int i = e(paramInt);
    return this.a.b(i);
  }
  
  int c()
  {
    return this.a.a();
  }
  
  View c(int paramInt)
  {
    return this.a.b(paramInt);
  }
  
  boolean c(View paramView)
  {
    return this.c.contains(paramView);
  }
  
  void d(int paramInt)
  {
    int i = e(paramInt);
    this.b.d(i);
    this.a.c(i);
  }
  
  void d(View paramView)
  {
    int i = this.a.a(paramView);
    if (i < 0) {
      throw new IllegalArgumentException("view is not a child, cannot hide " + paramView);
    }
    this.b.a(i);
    this.c.add(paramView);
  }
  
  boolean e(View paramView)
  {
    int i = this.a.a(paramView);
    if (i == -1)
    {
      if (this.c.remove(paramView)) {}
      return true;
    }
    if (this.b.c(i))
    {
      this.b.d(i);
      if (!this.c.remove(paramView)) {}
      this.a.a(i);
      return true;
    }
    return false;
  }
  
  public String toString()
  {
    return this.b.toString() + ", hidden list:" + this.c.size();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/aj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */