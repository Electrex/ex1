package android.support.v7.widget;

import android.content.Context;
import android.graphics.Rect;
import android.os.Parcelable;
import android.support.v4.view.a.a;
import android.support.v4.view.a.aj;
import android.support.v4.view.a.g;
import android.support.v4.view.a.q;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.accessibility.AccessibilityEvent;
import java.util.ArrayList;
import java.util.BitSet;

public class StaggeredGridLayoutManager
  extends cl
{
  private boolean A;
  private final Runnable B;
  bu a;
  bu b;
  boolean c;
  int d;
  int e;
  StaggeredGridLayoutManager.LazySpanLookup f;
  private int g;
  private do[] k;
  private int l;
  private int m;
  private ay n;
  private boolean o;
  private BitSet p;
  private int q;
  private boolean r;
  private boolean s;
  private StaggeredGridLayoutManager.SavedState t;
  private int u;
  private int v;
  private int w;
  private final Rect x;
  private final dk y;
  private boolean z;
  
  private boolean A()
  {
    if ((p() == 0) || (this.q == 0) || (!l())) {
      return false;
    }
    int i5;
    int i1;
    if (this.c)
    {
      int i4 = D();
      i5 = E();
      i1 = i4;
    }
    int j;
    for (int i2 = i5; (i1 == 0) && (e() != null); i2 = j)
    {
      this.f.a();
      y();
      k();
      return true;
      int i = E();
      j = D();
      i1 = i;
    }
    if (!this.z) {
      return false;
    }
    if (this.c) {}
    StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem1;
    for (int i3 = -1;; i3 = 1)
    {
      localFullSpanItem1 = this.f.a(i1, i2 + 1, i3, true);
      if (localFullSpanItem1 != null) {
        break;
      }
      this.z = false;
      this.f.a(i2 + 1);
      return false;
    }
    StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem2 = this.f.a(i1, localFullSpanItem1.a, i3 * -1, true);
    if (localFullSpanItem2 == null) {
      this.f.a(localFullSpanItem1.a);
    }
    for (;;)
    {
      y();
      k();
      return true;
      this.f.a(1 + localFullSpanItem2.a);
    }
  }
  
  private void B()
  {
    if (this.a == null)
    {
      this.a = bu.a(this, this.l);
      this.b = bu.a(this, 1 - this.l);
      this.n = new ay();
    }
  }
  
  private void C()
  {
    int i = 1;
    if ((this.l == i) || (!f()))
    {
      this.c = this.o;
      return;
    }
    if (!this.o) {}
    for (;;)
    {
      this.c = i;
      return;
      i = 0;
    }
  }
  
  private int D()
  {
    int i = p();
    if (i == 0) {
      return 0;
    }
    return d(e(i - 1));
  }
  
  private int E()
  {
    if (p() == 0) {
      return 0;
    }
    return d(e(0));
  }
  
  private int a(int paramInt1, int paramInt2, int paramInt3)
  {
    if ((paramInt2 == 0) && (paramInt3 == 0)) {}
    int i;
    do
    {
      return paramInt1;
      i = View.MeasureSpec.getMode(paramInt1);
    } while ((i != Integer.MIN_VALUE) && (i != 1073741824));
    return View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(paramInt1) - paramInt2 - paramInt3, i);
  }
  
  private int a(cq paramcq, ay paramay, cw paramcw)
  {
    this.p.set(0, this.g, true);
    int i1;
    int i2;
    int i3;
    label92:
    View localView;
    dl localdl;
    int i6;
    int i7;
    int i8;
    label154:
    do localdo2;
    label175:
    do localdo1;
    label190:
    label211:
    int i16;
    label243:
    int i11;
    int i10;
    if (paramay.d == 1)
    {
      int i17 = this.a.d() + this.n.a;
      int i18 = i17 + this.n.e + this.a.g();
      i1 = i17;
      i2 = i18;
      c(paramay.d, i2);
      if (!this.c) {
        break label477;
      }
      i3 = this.a.d();
      if ((!paramay.a(paramcw)) || (this.p.isEmpty())) {
        break label771;
      }
      localView = paramay.a(paramcq);
      localdl = (dl)localView.getLayoutParams();
      i6 = localdl.c();
      i7 = this.f.c(i6);
      if (i7 != -1) {
        break label489;
      }
      i8 = 1;
      if (i8 == 0) {
        break label505;
      }
      if (!localdl.f) {
        break label495;
      }
      localdo2 = this.k[0];
      this.f.a(i6, localdo2);
      localdo1 = localdo2;
      localdl.e = localdo1;
      if (paramay.d != 1) {
        break label517;
      }
      b(localView);
      a(localView, localdl);
      if (paramay.d != 1) {
        break label539;
      }
      if (!localdl.f) {
        break label527;
      }
      i16 = l(i3);
      i11 = i16 + this.a.c(localView);
      if ((i8 == 0) || (!localdl.f)) {
        break label844;
      }
      StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem3 = a(i16);
      localFullSpanItem3.b = -1;
      localFullSpanItem3.a = i6;
      this.f.a(localFullSpanItem3);
      i10 = i16;
    }
    for (;;)
    {
      label330:
      int i12;
      label356:
      int i13;
      if ((localdl.f) && (paramay.c == -1))
      {
        if (i8 != 0) {
          this.z = true;
        }
      }
      else
      {
        a(localView, localdl, paramay);
        if (!localdl.f) {
          break label709;
        }
        i12 = this.b.c();
        i13 = i12 + this.b.c(localView);
        if (this.l != 1) {
          break label732;
        }
        b(localView, i12, i10, i13, i11);
        label392:
        if (!localdl.f) {
          break label749;
        }
        c(this.n.d, i2);
      }
      for (;;)
      {
        a(paramcq, this.n, localdo1, i1);
        break label92;
        int i = this.a.c() - this.n.a;
        int j = i - this.n.e - this.a.c();
        i1 = i;
        i2 = j;
        break;
        label477:
        i3 = this.a.c();
        break label92;
        label489:
        i8 = 0;
        break label154;
        label495:
        localdo2 = a(paramay);
        break label175;
        label505:
        localdo1 = this.k[i7];
        break label190;
        label517:
        b(localView, 0);
        break label211;
        label527:
        i16 = localdo1.b(i3);
        break label243;
        label539:
        if (localdl.f) {}
        for (int i9 = k(i3);; i9 = localdo1.a(i3))
        {
          i10 = i9 - this.a.c(localView);
          if ((i8 != 0) && (localdl.f))
          {
            StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem2 = i(i9);
            localFullSpanItem2.b = 1;
            localFullSpanItem2.a = i6;
            this.f.a(localFullSpanItem2);
          }
          i11 = i9;
          break;
        }
        int i15;
        if (paramay.d == 1) {
          if (!i()) {
            i15 = 1;
          }
        }
        for (;;)
        {
          if (i15 == 0) {
            break label707;
          }
          StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem1 = this.f.f(i6);
          if (localFullSpanItem1 != null) {
            localFullSpanItem1.d = true;
          }
          this.z = true;
          break;
          i15 = 0;
          continue;
          if (!z()) {
            i15 = 1;
          } else {
            i15 = 0;
          }
        }
        label707:
        break label330;
        label709:
        i12 = localdo1.d * this.m + this.b.c();
        break label356;
        label732:
        b(localView, i10, i12, i11, i13);
        break label392;
        label749:
        int i14 = this.n.d;
        a(localdo1, i14, i2);
      }
      label771:
      if (this.n.d == -1)
      {
        int i5 = k(this.a.c());
        return Math.max(0, this.n.a + (i1 - i5));
      }
      int i4 = l(this.a.d());
      return Math.max(0, this.n.a + (i4 - i1));
      label844:
      i10 = i16;
    }
  }
  
  private int a(cw paramcw)
  {
    if (p() == 0) {
      return 0;
    }
    B();
    bu localbu = this.a;
    if (!this.A) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      View localView = a(bool1, true);
      boolean bool2 = this.A;
      boolean bool3 = false;
      if (!bool2) {
        bool3 = true;
      }
      return df.a(paramcw, localbu, localView, b(bool3, true), this, this.A, this.c);
    }
  }
  
  private StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem a(int paramInt)
  {
    StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem = new StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem();
    localFullSpanItem.c = new int[this.g];
    for (int i = 0; i < this.g; i++) {
      localFullSpanItem.c[i] = (paramInt - this.k[i].b(paramInt));
    }
    return localFullSpanItem;
  }
  
  private do a(ay paramay)
  {
    Object localObject1 = null;
    int i = -1;
    int i1;
    int j;
    int i7;
    int i8;
    label52:
    do localdo2;
    int i9;
    if (n(paramay.d))
    {
      i1 = -1 + this.g;
      j = i;
      if (paramay.d != 1) {
        break label118;
      }
      int i6 = this.a.c();
      i7 = i1;
      i8 = Integer.MAX_VALUE;
      if (i7 == j) {
        break label187;
      }
      localdo2 = this.k[i7];
      i9 = localdo2.b(i6);
      if (i9 >= i8) {
        break label199;
      }
    }
    for (Object localObject3 = localdo2;; localObject3 = localObject1)
    {
      i7 += i;
      localObject1 = localObject3;
      i8 = i9;
      break label52;
      j = this.g;
      i = 1;
      i1 = 0;
      break;
      label118:
      int i2 = this.a.d();
      int i3 = i1;
      int i4 = Integer.MIN_VALUE;
      do localdo1;
      int i5;
      if (i3 != j)
      {
        localdo1 = this.k[i3];
        i5 = localdo1.a(i2);
        if (i5 <= i4) {
          break label189;
        }
      }
      for (Object localObject2 = localdo1;; localObject2 = localObject1)
      {
        i3 += i;
        localObject1 = localObject2;
        i4 = i5;
        break;
        label187:
        return (do)localObject1;
        label189:
        i5 = i4;
      }
      label199:
      i9 = i8;
    }
  }
  
  private void a(int paramInt, cw paramcw)
  {
    int i = 1;
    this.n.a = 0;
    this.n.b = paramInt;
    label61:
    ay localay;
    if (m())
    {
      int j = paramcw.c();
      int i1 = this.c;
      if (j < paramInt)
      {
        int i2 = i;
        if (i1 != i2) {
          break label95;
        }
        this.n.e = 0;
        this.n.d = -1;
        localay = this.n;
        if (!this.c) {
          break label123;
        }
      }
    }
    for (;;)
    {
      localay.c = i;
      return;
      int i3 = 0;
      break;
      label95:
      this.n.e = this.a.f();
      break label61;
      this.n.e = 0;
      break label61;
      label123:
      i = -1;
    }
  }
  
  private void a(cq paramcq, int paramInt)
  {
    while (p() > 0)
    {
      View localView = e(0);
      if (this.a.b(localView) >= paramInt) {
        break;
      }
      dl localdl = (dl)localView.getLayoutParams();
      if (localdl.f) {
        for (int i = 0; i < this.g; i++) {
          this.k[i].h();
        }
      }
      localdl.e.h();
      a(localView, paramcq);
    }
  }
  
  private void a(cq paramcq, ay paramay, do paramdo, int paramInt)
  {
    if (paramay.d == -1)
    {
      b(paramcq, Math.max(paramInt, j(paramdo.b())) + (this.a.e() - this.a.c()));
      return;
    }
    a(paramcq, Math.min(paramInt, m(paramdo.d())) - (this.a.e() - this.a.c()));
  }
  
  private void a(cq paramcq, cw paramcw, boolean paramBoolean)
  {
    int i = l(this.a.d());
    int j = this.a.d() - i;
    if (j > 0)
    {
      int i1 = j - -c(-j, paramcq, paramcw);
      if ((paramBoolean) && (i1 > 0)) {
        this.a.a(i1);
      }
    }
  }
  
  private void a(dk paramdk)
  {
    if (this.t.c > 0) {
      if (this.t.c == this.g)
      {
        int i = 0;
        if (i < this.g)
        {
          this.k[i].e();
          int j = this.t.d[i];
          if (j != Integer.MIN_VALUE)
          {
            if (!this.t.i) {
              break label95;
            }
            j += this.a.d();
          }
          for (;;)
          {
            this.k[i].c(j);
            i++;
            break;
            label95:
            j += this.a.c();
          }
        }
      }
      else
      {
        this.t.a();
        this.t.a = this.t.b;
      }
    }
    this.s = this.t.j;
    a(this.t.h);
    C();
    if (this.t.a != -1) {
      this.d = this.t.a;
    }
    for (paramdk.c = this.t.i;; paramdk.c = this.c)
    {
      if (this.t.e > 1)
      {
        this.f.a = this.t.f;
        this.f.b = this.t.g;
      }
      return;
    }
  }
  
  private void a(do paramdo, int paramInt1, int paramInt2)
  {
    int i = paramdo.i();
    if (paramInt1 == -1) {
      if (i + paramdo.b() < paramInt2) {
        this.p.set(paramdo.d, false);
      }
    }
    while (paramdo.d() - i <= paramInt2) {
      return;
    }
    this.p.set(paramdo.d, false);
  }
  
  private void a(View paramView, dl paramdl)
  {
    if (paramdl.f)
    {
      if (this.l == 1)
      {
        b(paramView, this.u, b(paramdl.height, this.w));
        return;
      }
      b(paramView, b(paramdl.width, this.v), this.u);
      return;
    }
    if (this.l == 1)
    {
      b(paramView, this.v, b(paramdl.height, this.w));
      return;
    }
    b(paramView, b(paramdl.width, this.v), this.w);
  }
  
  private void a(View paramView, dl paramdl, ay paramay)
  {
    if (paramay.d == 1)
    {
      if (paramdl.f)
      {
        o(paramView);
        return;
      }
      paramdl.e.b(paramView);
      return;
    }
    if (paramdl.f)
    {
      p(paramView);
      return;
    }
    paramdl.e.a(paramView);
  }
  
  private boolean a(do paramdo)
  {
    if (this.c)
    {
      if (paramdo.d() >= this.a.d()) {}
    }
    else {
      while (paramdo.b() > this.a.c()) {
        return true;
      }
    }
    return false;
  }
  
  private int b(int paramInt1, int paramInt2)
  {
    if (paramInt1 < 0) {
      return paramInt2;
    }
    return View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824);
  }
  
  private void b(int paramInt1, int paramInt2, int paramInt3)
  {
    int i;
    int j;
    int i1;
    if (this.c)
    {
      i = D();
      if (paramInt3 != 3) {
        break label100;
      }
      if (paramInt1 >= paramInt2) {
        break label89;
      }
      j = paramInt2 + 1;
      i1 = paramInt1;
      label31:
      this.f.b(i1);
      switch (paramInt3)
      {
      case 2: 
      default: 
        label72:
        if (j > i) {
          break;
        }
      }
    }
    for (;;)
    {
      return;
      i = E();
      break;
      label89:
      j = paramInt1 + 1;
      i1 = paramInt2;
      break label31;
      label100:
      j = paramInt1 + paramInt2;
      i1 = paramInt1;
      break label31;
      this.f.b(paramInt1, paramInt2);
      break label72;
      this.f.a(paramInt1, paramInt2);
      break label72;
      this.f.a(paramInt1, 1);
      this.f.b(paramInt2, 1);
      break label72;
      if (this.c) {}
      for (int i2 = E(); i1 <= i2; i2 = D())
      {
        k();
        return;
      }
    }
  }
  
  private void b(int paramInt, cw paramcw)
  {
    int i = 1;
    this.n.a = 0;
    this.n.b = paramInt;
    if (m())
    {
      int j = paramcw.c();
      int i1 = this.c;
      if (j > paramInt)
      {
        int i2 = i;
        if (i1 != i2) {
          break label97;
        }
        this.n.e = 0;
      }
    }
    for (;;)
    {
      this.n.d = i;
      ay localay = this.n;
      if (this.c) {
        i = -1;
      }
      localay.c = i;
      return;
      int i3 = 0;
      break;
      label97:
      this.n.e = this.a.f();
      continue;
      this.n.e = 0;
    }
  }
  
  private void b(cq paramcq, int paramInt)
  {
    for (int i = -1 + p(); i >= 0; i--)
    {
      View localView = e(i);
      if (this.a.a(localView) <= paramInt) {
        break;
      }
      dl localdl = (dl)localView.getLayoutParams();
      if (localdl.f) {
        for (int j = 0; j < this.g; j++) {
          this.k[j].g();
        }
      }
      localdl.e.g();
      a(localView, paramcq);
    }
  }
  
  private void b(cq paramcq, cw paramcw, boolean paramBoolean)
  {
    int i = k(this.a.c()) - this.a.c();
    if (i > 0)
    {
      int j = i - c(i, paramcq, paramcw);
      if ((paramBoolean) && (j > 0)) {
        this.a.a(-j);
      }
    }
  }
  
  private void b(View paramView, int paramInt1, int paramInt2)
  {
    a(paramView, this.x);
    dl localdl = (dl)paramView.getLayoutParams();
    paramView.measure(a(paramInt1, localdl.leftMargin + this.x.left, localdl.rightMargin + this.x.right), a(paramInt2, localdl.topMargin + this.x.top, localdl.bottomMargin + this.x.bottom));
  }
  
  private void b(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    dl localdl = (dl)paramView.getLayoutParams();
    a(paramView, paramInt1 + localdl.leftMargin, paramInt2 + localdl.topMargin, paramInt3 - localdl.rightMargin, paramInt4 - localdl.bottomMargin);
  }
  
  private void c(int paramInt1, int paramInt2)
  {
    int i = 0;
    if (i < this.g)
    {
      if (do.a(this.k[i]).isEmpty()) {}
      for (;;)
      {
        i++;
        break;
        a(this.k[i], paramInt1, paramInt2);
      }
    }
  }
  
  private boolean c(cw paramcw, dk paramdk)
  {
    if (this.r) {}
    for (int i = q(paramcw.e());; i = p(paramcw.e()))
    {
      paramdk.a = i;
      paramdk.b = Integer.MIN_VALUE;
      return true;
    }
  }
  
  private int h(cw paramcw)
  {
    if (p() == 0) {
      return 0;
    }
    B();
    bu localbu = this.a;
    if (!this.A) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      View localView = a(bool1, true);
      boolean bool2 = this.A;
      boolean bool3 = false;
      if (!bool2) {
        bool3 = true;
      }
      return df.a(paramcw, localbu, localView, b(bool3, true), this, this.A);
    }
  }
  
  private int i(cw paramcw)
  {
    if (p() == 0) {
      return 0;
    }
    B();
    bu localbu = this.a;
    if (!this.A) {}
    for (boolean bool1 = true;; bool1 = false)
    {
      View localView = a(bool1, true);
      boolean bool2 = this.A;
      boolean bool3 = false;
      if (!bool2) {
        bool3 = true;
      }
      return df.b(paramcw, localbu, localView, b(bool3, true), this, this.A);
    }
  }
  
  private StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem i(int paramInt)
  {
    StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem localFullSpanItem = new StaggeredGridLayoutManager.LazySpanLookup.FullSpanItem();
    localFullSpanItem.c = new int[this.g];
    for (int i = 0; i < this.g; i++) {
      localFullSpanItem.c[i] = (this.k[i].a(paramInt) - paramInt);
    }
    return localFullSpanItem;
  }
  
  private int j(int paramInt)
  {
    int i = this.k[0].a(paramInt);
    for (int j = 1; j < this.g; j++)
    {
      int i1 = this.k[j].a(paramInt);
      if (i1 > i) {
        i = i1;
      }
    }
    return i;
  }
  
  private int k(int paramInt)
  {
    int i = this.k[0].a(paramInt);
    for (int j = 1; j < this.g; j++)
    {
      int i1 = this.k[j].a(paramInt);
      if (i1 < i) {
        i = i1;
      }
    }
    return i;
  }
  
  private int l(int paramInt)
  {
    int i = this.k[0].b(paramInt);
    for (int j = 1; j < this.g; j++)
    {
      int i1 = this.k[j].b(paramInt);
      if (i1 > i) {
        i = i1;
      }
    }
    return i;
  }
  
  private int m(int paramInt)
  {
    int i = this.k[0].b(paramInt);
    for (int j = 1; j < this.g; j++)
    {
      int i1 = this.k[j].b(paramInt);
      if (i1 < i) {
        i = i1;
      }
    }
    return i;
  }
  
  private boolean n(int paramInt)
  {
    int i1;
    if (this.l == 0) {
      if (paramInt == -1)
      {
        i1 = 1;
        if (i1 == this.c) {
          break label32;
        }
      }
    }
    label32:
    label66:
    label69:
    for (;;)
    {
      return true;
      i1 = 0;
      break;
      return false;
      int i;
      if (paramInt == -1)
      {
        i = 1;
        if (i != this.c) {
          break label66;
        }
      }
      for (int j = 1;; j = 0)
      {
        if (j == f()) {
          break label69;
        }
        return false;
        i = 0;
        break;
      }
    }
  }
  
  private int o(int paramInt)
  {
    int i = -1;
    if (p() == 0)
    {
      if (this.c) {
        return 1;
      }
      return i;
    }
    int j;
    if (paramInt < E())
    {
      j = 1;
      if (j == this.c) {
        break label45;
      }
    }
    for (;;)
    {
      return i;
      j = 0;
      break;
      label45:
      i = 1;
    }
  }
  
  private void o(View paramView)
  {
    for (int i = -1 + this.g; i >= 0; i--) {
      this.k[i].b(paramView);
    }
  }
  
  private int p(int paramInt)
  {
    int i = p();
    for (int j = 0; j < i; j++)
    {
      int i1 = d(e(j));
      if ((i1 >= 0) && (i1 < paramInt)) {
        return i1;
      }
    }
    return 0;
  }
  
  private void p(View paramView)
  {
    for (int i = -1 + this.g; i >= 0; i--) {
      this.k[i].a(paramView);
    }
  }
  
  private int q(int paramInt)
  {
    for (int i = -1 + p(); i >= 0; i--)
    {
      int j = d(e(i));
      if ((j >= 0) && (j < paramInt)) {
        return j;
      }
    }
    return 0;
  }
  
  public int a(int paramInt, cq paramcq, cw paramcw)
  {
    return c(paramInt, paramcq, paramcw);
  }
  
  public cm a()
  {
    return new dl(-2, -2);
  }
  
  public cm a(Context paramContext, AttributeSet paramAttributeSet)
  {
    return new dl(paramContext, paramAttributeSet);
  }
  
  public cm a(ViewGroup.LayoutParams paramLayoutParams)
  {
    if ((paramLayoutParams instanceof ViewGroup.MarginLayoutParams)) {
      return new dl((ViewGroup.MarginLayoutParams)paramLayoutParams);
    }
    return new dl(paramLayoutParams);
  }
  
  View a(boolean paramBoolean1, boolean paramBoolean2)
  {
    B();
    int i = this.a.c();
    int j = this.a.d();
    int i1 = p();
    int i2 = 0;
    Object localObject = null;
    if (i2 < i1)
    {
      View localView = e(i2);
      int i3 = this.a.a(localView);
      if ((this.a.b(localView) <= i) || (i3 >= j)) {}
      for (;;)
      {
        i2++;
        break;
        if ((i3 >= i) || (!paramBoolean1)) {
          return localView;
        }
        if ((paramBoolean2) && (localObject == null)) {
          localObject = localView;
        }
      }
    }
    return (View)localObject;
  }
  
  public void a(Parcelable paramParcelable)
  {
    if ((paramParcelable instanceof StaggeredGridLayoutManager.SavedState))
    {
      this.t = ((StaggeredGridLayoutManager.SavedState)paramParcelable);
      k();
    }
  }
  
  public void a(RecyclerView paramRecyclerView, int paramInt1, int paramInt2)
  {
    b(paramInt1, paramInt2, 0);
  }
  
  public void a(RecyclerView paramRecyclerView, int paramInt1, int paramInt2, int paramInt3)
  {
    b(paramInt1, paramInt2, 3);
  }
  
  public void a(RecyclerView paramRecyclerView, cq paramcq)
  {
    b(this.B);
    for (int i = 0; i < this.g; i++) {
      this.k[i].e();
    }
  }
  
  public void a(cq paramcq, cw paramcw)
  {
    B();
    dk localdk = this.y;
    localdk.a();
    if (this.t != null) {
      a(localdk);
    }
    for (;;)
    {
      a(paramcw, localdk);
      if ((this.t == null) && ((localdk.c != this.r) || (f() != this.s)))
      {
        this.f.a();
        localdk.d = true;
      }
      if ((p() <= 0) || ((this.t != null) && (this.t.c >= 1))) {
        break label206;
      }
      if (!localdk.d) {
        break;
      }
      for (int i3 = 0; i3 < this.g; i3++)
      {
        this.k[i3].e();
        if (localdk.b != Integer.MIN_VALUE) {
          this.k[i3].c(localdk.b);
        }
      }
      C();
      localdk.c = this.c;
    }
    for (int i2 = 0; i2 < this.g; i2++) {
      this.k[i2].a(this.c, localdk.b);
    }
    label206:
    a(paramcq);
    this.z = false;
    g();
    if (localdk.c)
    {
      a(localdk.a, paramcw);
      a(paramcq, this.n, paramcw);
      b(localdk.a, paramcw);
      ay localay2 = this.n;
      localay2.b += this.n.c;
      a(paramcq, this.n, paramcw);
      if (p() > 0)
      {
        if (!this.c) {
          break label500;
        }
        a(paramcq, paramcw, true);
        b(paramcq, paramcw, false);
      }
    }
    for (;;)
    {
      if (!paramcw.a())
      {
        int i = this.q;
        int j = 0;
        if (i != 0)
        {
          int i1 = p();
          j = 0;
          if (i1 > 0) {
            if (!this.z)
            {
              View localView = e();
              j = 0;
              if (localView == null) {}
            }
            else
            {
              j = 1;
            }
          }
        }
        if (j != 0)
        {
          b(this.B);
          a(this.B);
        }
        this.d = -1;
        this.e = Integer.MIN_VALUE;
      }
      this.r = localdk.c;
      this.s = f();
      this.t = null;
      return;
      b(localdk.a, paramcw);
      a(paramcq, this.n, paramcw);
      a(localdk.a, paramcw);
      ay localay1 = this.n;
      localay1.b += this.n.c;
      a(paramcq, this.n, paramcw);
      break;
      label500:
      b(paramcq, paramcw, true);
      a(paramcq, paramcw, false);
    }
  }
  
  public void a(cq paramcq, cw paramcw, View paramView, g paramg)
  {
    ViewGroup.LayoutParams localLayoutParams = paramView.getLayoutParams();
    if (!(localLayoutParams instanceof dl))
    {
      super.a(paramView, paramg);
      return;
    }
    dl localdl = (dl)localLayoutParams;
    if (this.l == 0)
    {
      int i1 = localdl.d();
      if (localdl.f) {}
      for (int i2 = this.g;; i2 = 1)
      {
        paramg.c(q.a(i1, i2, -1, -1, localdl.f, false));
        return;
      }
    }
    int i = localdl.d();
    if (localdl.f) {}
    for (int j = this.g;; j = 1)
    {
      paramg.c(q.a(-1, -1, i, j, localdl.f, false));
      return;
    }
  }
  
  void a(cw paramcw, dk paramdk)
  {
    if (b(paramcw, paramdk)) {}
    while (c(paramcw, paramdk)) {
      return;
    }
    paramdk.b();
    paramdk.a = 0;
  }
  
  public void a(AccessibilityEvent paramAccessibilityEvent)
  {
    super.a(paramAccessibilityEvent);
    aj localaj;
    View localView1;
    View localView2;
    if (p() > 0)
    {
      localaj = a.a(paramAccessibilityEvent);
      localView1 = a(false, true);
      localView2 = b(false, true);
      if ((localView1 != null) && (localView2 != null)) {}
    }
    else
    {
      return;
    }
    int i = d(localView1);
    int j = d(localView2);
    if (i < j)
    {
      localaj.b(i);
      localaj.c(j);
      return;
    }
    localaj.b(j);
    localaj.c(i);
  }
  
  public void a(String paramString)
  {
    if (this.t == null) {
      super.a(paramString);
    }
  }
  
  public void a(boolean paramBoolean)
  {
    a(null);
    if ((this.t != null) && (this.t.h != paramBoolean)) {
      this.t.h = paramBoolean;
    }
    this.o = paramBoolean;
    k();
  }
  
  public boolean a(cm paramcm)
  {
    return paramcm instanceof dl;
  }
  
  public int b(int paramInt, cq paramcq, cw paramcw)
  {
    return c(paramInt, paramcq, paramcw);
  }
  
  public int b(cw paramcw)
  {
    return a(paramcw);
  }
  
  public Parcelable b()
  {
    if (this.t != null) {
      return new StaggeredGridLayoutManager.SavedState(this.t);
    }
    StaggeredGridLayoutManager.SavedState localSavedState = new StaggeredGridLayoutManager.SavedState();
    localSavedState.h = this.o;
    localSavedState.i = this.r;
    localSavedState.j = this.s;
    int i;
    label122:
    int j;
    label155:
    int i1;
    if ((this.f != null) && (this.f.a != null))
    {
      localSavedState.f = this.f.a;
      localSavedState.e = localSavedState.f.length;
      localSavedState.g = this.f.b;
      if (p() <= 0) {
        break label267;
      }
      B();
      if (!this.r) {
        break label224;
      }
      i = D();
      localSavedState.a = i;
      localSavedState.b = h();
      localSavedState.c = this.g;
      localSavedState.d = new int[this.g];
      j = 0;
      if (j >= this.g) {
        return localSavedState;
      }
      if (!this.r) {
        break label232;
      }
      i1 = this.k[j].b(Integer.MIN_VALUE);
      if (i1 != Integer.MIN_VALUE) {
        i1 -= this.a.d();
      }
    }
    for (;;)
    {
      localSavedState.d[j] = i1;
      j++;
      break label155;
      localSavedState.e = 0;
      break;
      label224:
      i = E();
      break label122;
      label232:
      i1 = this.k[j].a(Integer.MIN_VALUE);
      if (i1 != Integer.MIN_VALUE) {
        i1 -= this.a.c();
      }
    }
    label267:
    localSavedState.a = -1;
    localSavedState.b = -1;
    localSavedState.c = 0;
    return localSavedState;
  }
  
  View b(boolean paramBoolean1, boolean paramBoolean2)
  {
    B();
    int i = this.a.c();
    int j = this.a.d();
    int i1 = -1 + p();
    Object localObject = null;
    if (i1 >= 0)
    {
      View localView = e(i1);
      int i2 = this.a.a(localView);
      int i3 = this.a.b(localView);
      if ((i3 <= i) || (i2 >= j)) {}
      for (;;)
      {
        i1--;
        break;
        if ((i3 <= j) || (!paramBoolean1)) {
          return localView;
        }
        if ((paramBoolean2) && (localObject == null)) {
          localObject = localView;
        }
      }
    }
    return (View)localObject;
  }
  
  public void b(RecyclerView paramRecyclerView, int paramInt1, int paramInt2)
  {
    b(paramInt1, paramInt2, 1);
  }
  
  boolean b(cw paramcw, dk paramdk)
  {
    if ((paramcw.a()) || (this.d == -1)) {
      return false;
    }
    if ((this.d < 0) || (this.d >= paramcw.e()))
    {
      this.d = -1;
      this.e = Integer.MIN_VALUE;
      return false;
    }
    if ((this.t == null) || (this.t.a == -1) || (this.t.c < 1))
    {
      View localView = b(this.d);
      if (localView != null)
      {
        if (this.c) {}
        for (int j = D();; j = E())
        {
          paramdk.a = j;
          if (this.e == Integer.MIN_VALUE) {
            break label188;
          }
          if (!paramdk.c) {
            break;
          }
          paramdk.b = (this.a.d() - this.e - this.a.b(localView));
          return true;
        }
        paramdk.b = (this.a.c() + this.e - this.a.a(localView));
        return true;
        label188:
        if (this.a.c(localView) > this.a.f())
        {
          if (paramdk.c) {}
          for (int i3 = this.a.d();; i3 = this.a.c())
          {
            paramdk.b = i3;
            return true;
          }
        }
        int i1 = this.a.a(localView) - this.a.c();
        if (i1 < 0)
        {
          paramdk.b = (-i1);
          return true;
        }
        int i2 = this.a.d() - this.a.b(localView);
        if (i2 < 0)
        {
          paramdk.b = i2;
          return true;
        }
        paramdk.b = Integer.MIN_VALUE;
        return true;
      }
      paramdk.a = this.d;
      if (this.e == Integer.MIN_VALUE)
      {
        int i = o(paramdk.a);
        boolean bool = false;
        if (i == 1) {
          bool = true;
        }
        paramdk.c = bool;
        paramdk.b();
      }
      for (;;)
      {
        paramdk.d = true;
        return true;
        paramdk.a(this.e);
      }
    }
    paramdk.b = Integer.MIN_VALUE;
    paramdk.a = this.d;
    return true;
  }
  
  int c(int paramInt, cq paramcq, cw paramcw)
  {
    int i = 1;
    int j = -1;
    B();
    int i1;
    int i3;
    label103:
    int i4;
    if (paramInt > 0)
    {
      this.n.d = i;
      ay localay3 = this.n;
      if (this.c)
      {
        localay3.c = j;
        i1 = D();
        this.n.b = (i1 + this.n.c);
        int i2 = Math.abs(paramInt);
        this.n.a = i2;
        ay localay2 = this.n;
        if (!m()) {
          break label200;
        }
        i3 = this.a.f();
        localay2.e = i3;
        i4 = a(paramcq, this.n, paramcw);
        if (i2 >= i4) {
          break label206;
        }
      }
    }
    for (;;)
    {
      this.a.a(-paramInt);
      this.r = this.c;
      return paramInt;
      j = i;
      break;
      this.n.d = j;
      ay localay1 = this.n;
      if (this.c) {}
      for (;;)
      {
        localay1.c = i;
        i1 = E();
        break;
        i = j;
      }
      label200:
      i3 = 0;
      break label103;
      label206:
      if (paramInt < 0) {
        paramInt = -i4;
      } else {
        paramInt = i4;
      }
    }
  }
  
  public int c(cq paramcq, cw paramcw)
  {
    if (this.l == 0) {
      return this.g;
    }
    return super.c(paramcq, paramcw);
  }
  
  public int c(cw paramcw)
  {
    return a(paramcw);
  }
  
  public void c(RecyclerView paramRecyclerView, int paramInt1, int paramInt2)
  {
    b(paramInt1, paramInt2, 2);
  }
  
  public boolean c()
  {
    return this.l == 0;
  }
  
  public int d(cq paramcq, cw paramcw)
  {
    if (this.l == 1) {
      return this.g;
    }
    return super.d(paramcq, paramcw);
  }
  
  public int d(cw paramcw)
  {
    return h(paramcw);
  }
  
  public boolean d()
  {
    return this.l == 1;
  }
  
  public int e(cw paramcw)
  {
    return h(paramcw);
  }
  
  View e()
  {
    int i = -1 + p();
    BitSet localBitSet = new BitSet(this.g);
    localBitSet.set(0, this.g, true);
    int j;
    int i1;
    if ((this.l == 1) && (f()))
    {
      j = 1;
      if (!this.c) {
        break label128;
      }
      i1 = -1;
      label56:
      if (i >= i1) {
        break label138;
      }
    }
    int i3;
    View localView1;
    dl localdl1;
    label128:
    label138:
    for (int i2 = 1;; i2 = -1)
    {
      i3 = i;
      if (i3 == i1) {
        break label356;
      }
      localView1 = e(i3);
      localdl1 = (dl)localView1.getLayoutParams();
      if (!localBitSet.get(localdl1.e.d)) {
        break label156;
      }
      if (!a(localdl1.e)) {
        break label144;
      }
      return localView1;
      j = -1;
      break;
      i1 = i + 1;
      i = 0;
      break label56;
    }
    label144:
    localBitSet.clear(localdl1.e.d);
    label156:
    if (localdl1.f) {}
    label282:
    label350:
    label354:
    label356:
    label358:
    label362:
    for (;;)
    {
      i3 += i2;
      break;
      if (i3 + i2 != i1)
      {
        View localView2 = e(i3 + i2);
        int i6;
        if (this.c)
        {
          int i9 = this.a.b(localView1);
          int i10 = this.a.b(localView2);
          if (i9 < i10) {
            return localView1;
          }
          if (i9 != i10) {
            break label358;
          }
          i6 = 1;
        }
        for (;;)
        {
          if (i6 == 0) {
            break label362;
          }
          dl localdl2 = (dl)localView2.getLayoutParams();
          int i7;
          if (localdl1.e.d - localdl2.e.d < 0)
          {
            i7 = 1;
            if (j >= 0) {
              break label350;
            }
          }
          for (int i8 = 1;; i8 = 0)
          {
            if (i7 == i8) {
              break label354;
            }
            return localView1;
            int i4 = this.a.a(localView1);
            int i5 = this.a.a(localView2);
            if (i4 > i5) {
              return localView1;
            }
            if (i4 != i5) {
              break label358;
            }
            i6 = 1;
            break;
            i7 = 0;
            break label282;
          }
          break;
          return null;
          i6 = 0;
        }
      }
    }
  }
  
  public void e(RecyclerView paramRecyclerView)
  {
    this.f.a();
    k();
  }
  
  public int f(cw paramcw)
  {
    return i(paramcw);
  }
  
  public void f(int paramInt)
  {
    super.f(paramInt);
    for (int i = 0; i < this.g; i++) {
      this.k[i].d(paramInt);
    }
  }
  
  boolean f()
  {
    return n() == 1;
  }
  
  public int g(cw paramcw)
  {
    return i(paramcw);
  }
  
  void g()
  {
    this.m = (this.b.f() / this.g);
    this.u = View.MeasureSpec.makeMeasureSpec(this.b.f(), 1073741824);
    if (this.l == 1)
    {
      this.v = View.MeasureSpec.makeMeasureSpec(this.m, 1073741824);
      this.w = View.MeasureSpec.makeMeasureSpec(0, 0);
      return;
    }
    this.w = View.MeasureSpec.makeMeasureSpec(this.m, 1073741824);
    this.v = View.MeasureSpec.makeMeasureSpec(0, 0);
  }
  
  public void g(int paramInt)
  {
    super.g(paramInt);
    for (int i = 0; i < this.g; i++) {
      this.k[i].d(paramInt);
    }
  }
  
  int h()
  {
    if (this.c) {}
    for (View localView = b(true, true); localView == null; localView = a(true, true)) {
      return -1;
    }
    return d(localView);
  }
  
  public void h(int paramInt)
  {
    if (paramInt == 0) {
      A();
    }
  }
  
  boolean i()
  {
    int i = 1;
    int j = this.k[0].b(Integer.MIN_VALUE);
    for (int i1 = i;; i1++) {
      if (i1 < this.g)
      {
        if (this.k[i1].b(Integer.MIN_VALUE) != j) {
          i = 0;
        }
      }
      else {
        return i;
      }
    }
  }
  
  public boolean j()
  {
    return this.t == null;
  }
  
  boolean z()
  {
    int i = 1;
    int j = this.k[0].a(Integer.MIN_VALUE);
    for (int i1 = i;; i1++) {
      if (i1 < this.g)
      {
        if (this.k[i1].a(Integer.MIN_VALUE) != j) {
          i = 0;
        }
      }
      else {
        return i;
      }
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/StaggeredGridLayoutManager.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */