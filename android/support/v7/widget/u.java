package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v7.a.b;
import android.support.v7.internal.widget.bf;
import android.support.v7.internal.widget.bh;
import android.util.AttributeSet;
import android.widget.CheckBox;

public class u
  extends CheckBox
{
  private static final int[] a = { 16843015 };
  private bf b;
  private Drawable c;
  
  public u(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, b.checkboxStyle);
  }
  
  public u(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    if (bf.a)
    {
      bh localbh = bh.a(getContext(), paramAttributeSet, a, paramInt, 0);
      setButtonDrawable(localbh.a(0));
      localbh.b();
      this.b = localbh.c();
    }
  }
  
  public int getCompoundPaddingLeft()
  {
    int i = super.getCompoundPaddingLeft();
    if ((Build.VERSION.SDK_INT < 17) && (this.c != null)) {
      i += this.c.getIntrinsicWidth();
    }
    return i;
  }
  
  public void setButtonDrawable(int paramInt)
  {
    if (this.b != null)
    {
      setButtonDrawable(this.b.a(paramInt));
      return;
    }
    super.setButtonDrawable(paramInt);
  }
  
  public void setButtonDrawable(Drawable paramDrawable)
  {
    super.setButtonDrawable(paramDrawable);
    this.c = paramDrawable;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/u.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */