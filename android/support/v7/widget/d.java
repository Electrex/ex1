package android.support.v7.widget;

import android.support.v7.internal.view.menu.i;
import android.view.View;

class d
  implements Runnable
{
  private g b;
  
  public d(ActionMenuPresenter paramActionMenuPresenter, g paramg)
  {
    this.b = paramg;
  }
  
  public void run()
  {
    ActionMenuPresenter.f(this.a).f();
    View localView = (View)ActionMenuPresenter.g(this.a);
    if ((localView != null) && (localView.getWindowToken() != null) && (this.b.d())) {
      ActionMenuPresenter.a(this.a, this.b);
    }
    ActionMenuPresenter.a(this.a, null);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */