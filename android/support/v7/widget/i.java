package android.support.v7.widget;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class i
  implements Parcelable.Creator
{
  public ActionMenuPresenter.SavedState a(Parcel paramParcel)
  {
    return new ActionMenuPresenter.SavedState(paramParcel);
  }
  
  public ActionMenuPresenter.SavedState[] a(int paramInt)
  {
    return new ActionMenuPresenter.SavedState[paramInt];
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */