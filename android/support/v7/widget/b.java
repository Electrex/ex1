package android.support.v7.widget;

import android.content.Context;
import android.support.v7.internal.view.menu.ad;
import android.support.v7.internal.view.menu.m;
import android.support.v7.internal.view.menu.v;
import android.view.MenuItem;
import android.view.View;

class b
  extends v
{
  private ad d;
  
  public b(ActionMenuPresenter paramActionMenuPresenter, Context paramContext, ad paramad)
  {
    super(paramContext, paramad, null, false, android.support.v7.a.b.actionOverflowMenuStyle);
    this.d = paramad;
    View localView;
    int i;
    if (!((m)paramad.getItem()).j())
    {
      if (ActionMenuPresenter.d(paramActionMenuPresenter) == null)
      {
        localView = (View)ActionMenuPresenter.e(paramActionMenuPresenter);
        a(localView);
      }
    }
    else
    {
      a(paramActionMenuPresenter.g);
      i = paramad.size();
    }
    for (int j = 0;; j++)
    {
      boolean bool = false;
      if (j < i)
      {
        MenuItem localMenuItem = paramad.getItem(j);
        if ((localMenuItem.isVisible()) && (localMenuItem.getIcon() != null)) {
          bool = true;
        }
      }
      else
      {
        a(bool);
        return;
        localView = ActionMenuPresenter.d(paramActionMenuPresenter);
        break;
      }
    }
  }
  
  public void onDismiss()
  {
    super.onDismiss();
    ActionMenuPresenter.a(this.c, null);
    this.c.h = 0;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v7/widget/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */