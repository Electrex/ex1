package android.support.v4.view;

import android.view.MenuItem;
import android.view.View;

abstract interface aw
{
  public abstract MenuItem a(MenuItem paramMenuItem, View paramView);
  
  public abstract View a(MenuItem paramMenuItem);
  
  public abstract void a(MenuItem paramMenuItem, int paramInt);
  
  public abstract MenuItem b(MenuItem paramMenuItem, int paramInt);
  
  public abstract boolean b(MenuItem paramMenuItem);
  
  public abstract boolean c(MenuItem paramMenuItem);
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/aw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */