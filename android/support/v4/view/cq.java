package android.support.v4.view;

import android.os.Build.VERSION;
import android.view.ViewConfiguration;

public class cq
{
  static final cv a = new cr();
  
  static
  {
    if (Build.VERSION.SDK_INT >= 14)
    {
      a = new cu();
      return;
    }
    if (Build.VERSION.SDK_INT >= 11)
    {
      a = new ct();
      return;
    }
    if (Build.VERSION.SDK_INT >= 8)
    {
      a = new cs();
      return;
    }
  }
  
  public static int a(ViewConfiguration paramViewConfiguration)
  {
    return a.a(paramViewConfiguration);
  }
  
  public static boolean b(ViewConfiguration paramViewConfiguration)
  {
    return a.b(paramViewConfiguration);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/cq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */