package android.support.v4.view;

import android.view.View;
import android.view.ViewParent;

public class bk
{
  private final View a;
  private ViewParent b;
  private boolean c;
  private int[] d;
  
  public void a(boolean paramBoolean)
  {
    if (this.c) {
      bv.v(this.a);
    }
    this.c = paramBoolean;
  }
  
  public boolean a()
  {
    return this.c;
  }
  
  public boolean a(float paramFloat1, float paramFloat2)
  {
    if ((a()) && (this.b != null)) {
      return dt.a(this.b, this.a, paramFloat1, paramFloat2);
    }
    return false;
  }
  
  public boolean a(float paramFloat1, float paramFloat2, boolean paramBoolean)
  {
    if ((a()) && (this.b != null)) {
      return dt.a(this.b, this.a, paramFloat1, paramFloat2, paramBoolean);
    }
    return false;
  }
  
  public boolean a(int paramInt)
  {
    if (b()) {
      return true;
    }
    if (a())
    {
      ViewParent localViewParent = this.a.getParent();
      View localView = this.a;
      while (localViewParent != null)
      {
        if (dt.a(localViewParent, localView, this.a, paramInt))
        {
          this.b = localViewParent;
          dt.b(localViewParent, localView, this.a, paramInt);
          return true;
        }
        if ((localViewParent instanceof View)) {
          localView = (View)localViewParent;
        }
        localViewParent = localViewParent.getParent();
      }
    }
    return false;
  }
  
  public boolean a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfInt)
  {
    boolean bool1 = a();
    boolean bool2 = false;
    int k;
    int i;
    if (bool1)
    {
      ViewParent localViewParent = this.b;
      bool2 = false;
      if (localViewParent != null)
      {
        if ((paramInt1 == 0) && (paramInt2 == 0) && (paramInt3 == 0) && (paramInt4 == 0)) {
          break label133;
        }
        if (paramArrayOfInt == null) {
          break label153;
        }
        this.a.getLocationInWindow(paramArrayOfInt);
        k = paramArrayOfInt[0];
        i = paramArrayOfInt[1];
      }
    }
    for (int j = k;; j = 0)
    {
      dt.a(this.b, this.a, paramInt1, paramInt2, paramInt3, paramInt4);
      if (paramArrayOfInt != null)
      {
        this.a.getLocationInWindow(paramArrayOfInt);
        paramArrayOfInt[0] -= j;
        paramArrayOfInt[1] -= i;
      }
      bool2 = true;
      label133:
      do
      {
        return bool2;
        bool2 = false;
      } while (paramArrayOfInt == null);
      paramArrayOfInt[0] = 0;
      paramArrayOfInt[1] = 0;
      return false;
      label153:
      i = 0;
    }
  }
  
  public boolean a(int paramInt1, int paramInt2, int[] paramArrayOfInt1, int[] paramArrayOfInt2)
  {
    boolean bool1 = a();
    boolean bool2 = false;
    int j;
    int i;
    if (bool1)
    {
      ViewParent localViewParent = this.b;
      bool2 = false;
      if (localViewParent != null)
      {
        if ((paramInt1 == 0) && (paramInt2 == 0)) {
          break label168;
        }
        if (paramArrayOfInt2 == null) {
          break label188;
        }
        this.a.getLocationInWindow(paramArrayOfInt2);
        j = paramArrayOfInt2[0];
        i = paramArrayOfInt2[1];
      }
    }
    for (;;)
    {
      if (paramArrayOfInt1 == null)
      {
        if (this.d == null) {
          this.d = new int[2];
        }
        paramArrayOfInt1 = this.d;
      }
      paramArrayOfInt1[0] = 0;
      paramArrayOfInt1[1] = 0;
      dt.a(this.b, this.a, paramInt1, paramInt2, paramArrayOfInt1);
      if (paramArrayOfInt2 != null)
      {
        this.a.getLocationInWindow(paramArrayOfInt2);
        paramArrayOfInt2[0] -= j;
        paramArrayOfInt2[1] -= i;
      }
      if (paramArrayOfInt1[0] == 0)
      {
        int k = paramArrayOfInt1[1];
        bool2 = false;
        if (k == 0) {}
      }
      else
      {
        bool2 = true;
      }
      label168:
      do
      {
        return bool2;
        bool2 = false;
      } while (paramArrayOfInt2 == null);
      paramArrayOfInt2[0] = 0;
      paramArrayOfInt2[1] = 0;
      return false;
      label188:
      i = 0;
      j = 0;
    }
  }
  
  public boolean b()
  {
    return this.b != null;
  }
  
  public void c()
  {
    if (this.b != null)
    {
      dt.a(this.b, this.a);
      this.b = null;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/bk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */