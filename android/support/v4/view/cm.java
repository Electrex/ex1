package android.support.v4.view;

import android.graphics.Paint;
import android.view.View;

class cm
{
  public static int a(View paramView)
  {
    return paramView.getLayoutDirection();
  }
  
  public static void a(View paramView, Paint paramPaint)
  {
    paramView.setLayerPaint(paramPaint);
  }
  
  public static int b(View paramView)
  {
    return paramView.getWindowSystemUiVisibility();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/cm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */