package android.support.v4.view;

import android.os.Build.VERSION;
import android.view.LayoutInflater;

public class ac
{
  static final ad a = new ae();
  
  static
  {
    int i = Build.VERSION.SDK_INT;
    if (i >= 21)
    {
      a = new ag();
      return;
    }
    if (i >= 11)
    {
      a = new af();
      return;
    }
  }
  
  public static void a(LayoutInflater paramLayoutInflater, am paramam)
  {
    a.a(paramLayoutInflater, paramam);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/ac.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */