package android.support.v4.view;

import android.view.View;
import android.view.ViewParent;

class dx
  extends dw
{
  public void a(ViewParent paramViewParent, View paramView)
  {
    dz.a(paramViewParent, paramView);
  }
  
  public void a(ViewParent paramViewParent, View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    dz.a(paramViewParent, paramView, paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public void a(ViewParent paramViewParent, View paramView, int paramInt1, int paramInt2, int[] paramArrayOfInt)
  {
    dz.a(paramViewParent, paramView, paramInt1, paramInt2, paramArrayOfInt);
  }
  
  public boolean a(ViewParent paramViewParent, View paramView, float paramFloat1, float paramFloat2)
  {
    return dz.a(paramViewParent, paramView, paramFloat1, paramFloat2);
  }
  
  public boolean a(ViewParent paramViewParent, View paramView, float paramFloat1, float paramFloat2, boolean paramBoolean)
  {
    return dz.a(paramViewParent, paramView, paramFloat1, paramFloat2, paramBoolean);
  }
  
  public boolean a(ViewParent paramViewParent, View paramView1, View paramView2, int paramInt)
  {
    return dz.a(paramViewParent, paramView1, paramView2, paramInt);
  }
  
  public void b(ViewParent paramViewParent, View paramView1, View paramView2, int paramInt)
  {
    dz.b(paramViewParent, paramView1, paramView2, paramInt);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/dx.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */