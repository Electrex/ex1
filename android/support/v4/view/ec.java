package android.support.v4.view;

import android.view.View;
import android.view.animation.Interpolator;
import java.util.WeakHashMap;

class ec
  implements ej
{
  WeakHashMap a = null;
  
  private void a(View paramView)
  {
    if (this.a != null)
    {
      Runnable localRunnable = (Runnable)this.a.get(paramView);
      if (localRunnable != null) {
        paramView.removeCallbacks(localRunnable);
      }
    }
  }
  
  private void c(ea paramea, View paramView)
  {
    Object localObject = paramView.getTag(2113929216);
    if ((localObject instanceof eq)) {}
    for (eq localeq = (eq)localObject;; localeq = null)
    {
      Runnable localRunnable1 = ea.a(paramea);
      Runnable localRunnable2 = ea.b(paramea);
      if (localRunnable1 != null) {
        localRunnable1.run();
      }
      if (localeq != null)
      {
        localeq.a(paramView);
        localeq.b(paramView);
      }
      if (localRunnable2 != null) {
        localRunnable2.run();
      }
      if (this.a != null) {
        this.a.remove(paramView);
      }
      return;
    }
  }
  
  private void d(ea paramea, View paramView)
  {
    if (this.a != null) {}
    for (Object localObject = (Runnable)this.a.get(paramView);; localObject = null)
    {
      if (localObject == null)
      {
        localObject = new ed(this, paramea, paramView, null);
        if (this.a == null) {
          this.a = new WeakHashMap();
        }
        this.a.put(paramView, localObject);
      }
      paramView.removeCallbacks((Runnable)localObject);
      paramView.post((Runnable)localObject);
      return;
    }
  }
  
  public void a(ea paramea, View paramView)
  {
    d(paramea, paramView);
  }
  
  public void a(ea paramea, View paramView, float paramFloat)
  {
    d(paramea, paramView);
  }
  
  public void a(ea paramea, View paramView, long paramLong) {}
  
  public void a(ea paramea, View paramView, eq parameq)
  {
    paramView.setTag(2113929216, parameq);
  }
  
  public void a(ea paramea, View paramView, es parames) {}
  
  public void a(ea paramea, View paramView, Interpolator paramInterpolator) {}
  
  public void b(ea paramea, View paramView)
  {
    a(paramView);
    c(paramea, paramView);
  }
  
  public void b(ea paramea, View paramView, float paramFloat)
  {
    d(paramea, paramView);
  }
  
  public void c(ea paramea, View paramView, float paramFloat)
  {
    d(paramea, paramView);
  }
  
  public void d(ea paramea, View paramView, float paramFloat)
  {
    d(paramea, paramView);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/ec.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */