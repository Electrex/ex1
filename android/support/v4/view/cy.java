package android.support.v4.view;

import android.os.Build.VERSION;
import android.view.ViewGroup;

public class cy
{
  static final db a = new de();
  
  static
  {
    int i = Build.VERSION.SDK_INT;
    if (i >= 21)
    {
      a = new dd();
      return;
    }
    if (i >= 18)
    {
      a = new dc();
      return;
    }
    if (i >= 14)
    {
      a = new da();
      return;
    }
    if (i >= 11)
    {
      a = new cz();
      return;
    }
  }
  
  public static void a(ViewGroup paramViewGroup, boolean paramBoolean)
  {
    a.a(paramViewGroup, paramBoolean);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/cy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */