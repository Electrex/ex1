package android.support.v4.view;

import android.view.View;
import android.view.animation.Interpolator;
import java.util.WeakHashMap;

class ee
  extends ec
{
  WeakHashMap b = null;
  
  public void a(ea paramea, View paramView)
  {
    ek.a(paramView);
  }
  
  public void a(ea paramea, View paramView, float paramFloat)
  {
    ek.a(paramView, paramFloat);
  }
  
  public void a(ea paramea, View paramView, long paramLong)
  {
    ek.a(paramView, paramLong);
  }
  
  public void a(ea paramea, View paramView, eq parameq)
  {
    paramView.setTag(2113929216, parameq);
    ek.a(paramView, new ef(paramea));
  }
  
  public void a(ea paramea, View paramView, Interpolator paramInterpolator)
  {
    ek.a(paramView, paramInterpolator);
  }
  
  public void b(ea paramea, View paramView)
  {
    ek.b(paramView);
  }
  
  public void b(ea paramea, View paramView, float paramFloat)
  {
    ek.b(paramView, paramFloat);
  }
  
  public void c(ea paramea, View paramView, float paramFloat)
  {
    ek.c(paramView, paramFloat);
  }
  
  public void d(ea paramea, View paramView, float paramFloat)
  {
    ek.d(paramView, paramFloat);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/ee.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */