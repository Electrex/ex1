package android.support.v4.view;

import android.view.View;
import java.lang.reflect.Field;
import java.util.WeakHashMap;

class ca
  extends bz
{
  static Field b;
  static boolean c = false;
  
  public void a(View paramView, a parama)
  {
    if (parama == null) {}
    for (Object localObject = null;; localObject = parama.a())
    {
      ck.a(paramView, localObject);
      return;
    }
  }
  
  public boolean a(View paramView, int paramInt)
  {
    return ck.a(paramView, paramInt);
  }
  
  /* Error */
  public boolean b(View paramView)
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_2
    //   2: getstatic 12	android/support/v4/view/ca:c	Z
    //   5: ifeq +5 -> 10
    //   8: iconst_0
    //   9: ireturn
    //   10: getstatic 35	android/support/v4/view/ca:b	Ljava/lang/reflect/Field;
    //   13: ifnonnull +20 -> 33
    //   16: ldc 37
    //   18: ldc 39
    //   20: invokevirtual 45	java/lang/Class:getDeclaredField	(Ljava/lang/String;)Ljava/lang/reflect/Field;
    //   23: putstatic 35	android/support/v4/view/ca:b	Ljava/lang/reflect/Field;
    //   26: getstatic 35	android/support/v4/view/ca:b	Ljava/lang/reflect/Field;
    //   29: iconst_1
    //   30: invokevirtual 51	java/lang/reflect/Field:setAccessible	(Z)V
    //   33: getstatic 35	android/support/v4/view/ca:b	Ljava/lang/reflect/Field;
    //   36: aload_1
    //   37: invokevirtual 55	java/lang/reflect/Field:get	(Ljava/lang/Object;)Ljava/lang/Object;
    //   40: astore 4
    //   42: aload 4
    //   44: ifnull +13 -> 57
    //   47: iload_2
    //   48: ireturn
    //   49: astore 5
    //   51: iload_2
    //   52: putstatic 12	android/support/v4/view/ca:c	Z
    //   55: iconst_0
    //   56: ireturn
    //   57: iconst_0
    //   58: istore_2
    //   59: goto -12 -> 47
    //   62: astore_3
    //   63: iload_2
    //   64: putstatic 12	android/support/v4/view/ca:c	Z
    //   67: iconst_0
    //   68: ireturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	69	0	this	ca
    //   0	69	1	paramView	View
    //   1	63	2	bool	boolean
    //   62	1	3	localThrowable1	Throwable
    //   40	3	4	localObject	Object
    //   49	1	5	localThrowable2	Throwable
    // Exception table:
    //   from	to	target	type
    //   16	33	49	java/lang/Throwable
    //   33	42	62	java/lang/Throwable
  }
  
  public boolean b(View paramView, int paramInt)
  {
    return ck.b(paramView, paramInt);
  }
  
  public ea q(View paramView)
  {
    if (this.a == null) {
      this.a = new WeakHashMap();
    }
    ea localea = (ea)this.a.get(paramView);
    if (localea == null)
    {
      localea = new ea(paramView);
      this.a.put(paramView, localea);
    }
    return localea;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/ca.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */