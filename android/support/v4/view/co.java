package android.support.v4.view;

import android.view.View;
import android.view.WindowInsets;

class co
{
  public static et a(View paramView, et paramet)
  {
    if ((paramet instanceof eu))
    {
      WindowInsets localWindowInsets1 = ((eu)paramet).e();
      WindowInsets localWindowInsets2 = paramView.onApplyWindowInsets(localWindowInsets1);
      if (localWindowInsets2 != localWindowInsets1) {
        paramet = new eu(localWindowInsets2);
      }
    }
    return paramet;
  }
  
  public static void a(View paramView)
  {
    paramView.requestApplyInsets();
  }
  
  public static void a(View paramView, float paramFloat)
  {
    paramView.setElevation(paramFloat);
  }
  
  public static void a(View paramView, bn parambn)
  {
    paramView.setOnApplyWindowInsetsListener(new cp(parambn));
  }
  
  public static void b(View paramView)
  {
    paramView.stopNestedScroll();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/co.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */