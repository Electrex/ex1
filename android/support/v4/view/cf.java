package android.support.v4.view;

import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

abstract interface cf
{
  public abstract int a(int paramInt1, int paramInt2, int paramInt3);
  
  public abstract int a(View paramView);
  
  public abstract et a(View paramView, et paramet);
  
  public abstract void a(View paramView, float paramFloat);
  
  public abstract void a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  public abstract void a(View paramView, int paramInt, Paint paramPaint);
  
  public abstract void a(View paramView, Paint paramPaint);
  
  public abstract void a(View paramView, a parama);
  
  public abstract void a(View paramView, bn parambn);
  
  public abstract void a(View paramView, Runnable paramRunnable);
  
  public abstract void a(View paramView, Runnable paramRunnable, long paramLong);
  
  public abstract void a(View paramView, boolean paramBoolean);
  
  public abstract void a(ViewGroup paramViewGroup, boolean paramBoolean);
  
  public abstract boolean a(View paramView, int paramInt);
  
  public abstract void b(View paramView, float paramFloat);
  
  public abstract void b(View paramView, boolean paramBoolean);
  
  public abstract boolean b(View paramView);
  
  public abstract boolean b(View paramView, int paramInt);
  
  public abstract void c(View paramView, float paramFloat);
  
  public abstract void c(View paramView, int paramInt);
  
  public abstract boolean c(View paramView);
  
  public abstract void d(View paramView);
  
  public abstract void d(View paramView, float paramFloat);
  
  public abstract int e(View paramView);
  
  public abstract void e(View paramView, float paramFloat);
  
  public abstract float f(View paramView);
  
  public abstract void f(View paramView, float paramFloat);
  
  public abstract int g(View paramView);
  
  public abstract int h(View paramView);
  
  public abstract ViewParent i(View paramView);
  
  public abstract boolean j(View paramView);
  
  public abstract int k(View paramView);
  
  public abstract float l(View paramView);
  
  public abstract float m(View paramView);
  
  public abstract float n(View paramView);
  
  public abstract int o(View paramView);
  
  public abstract int p(View paramView);
  
  public abstract ea q(View paramView);
  
  public abstract int r(View paramView);
  
  public abstract void s(View paramView);
  
  public abstract boolean t(View paramView);
  
  public abstract void u(View paramView);
  
  public abstract void v(View paramView);
  
  public abstract boolean w(View paramView);
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/cf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */