package android.support.v4.view;

import android.os.Build.VERSION;
import android.view.View;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;

public class ea
{
  static final ej a = new ec();
  private WeakReference b;
  private Runnable c = null;
  private Runnable d = null;
  private int e = -1;
  
  static
  {
    int i = Build.VERSION.SDK_INT;
    if (i >= 19)
    {
      a = new ei();
      return;
    }
    if (i >= 18)
    {
      a = new eg();
      return;
    }
    if (i >= 16)
    {
      a = new eh();
      return;
    }
    if (i >= 14)
    {
      a = new ee();
      return;
    }
  }
  
  ea(View paramView)
  {
    this.b = new WeakReference(paramView);
  }
  
  public ea a(float paramFloat)
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.a(this, localView, paramFloat);
    }
    return this;
  }
  
  public ea a(long paramLong)
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.a(this, localView, paramLong);
    }
    return this;
  }
  
  public ea a(eq parameq)
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.a(this, localView, parameq);
    }
    return this;
  }
  
  public ea a(es parames)
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.a(this, localView, parames);
    }
    return this;
  }
  
  public ea a(Interpolator paramInterpolator)
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.a(this, localView, paramInterpolator);
    }
    return this;
  }
  
  public void a()
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.a(this, localView);
    }
  }
  
  public ea b(float paramFloat)
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.b(this, localView, paramFloat);
    }
    return this;
  }
  
  public void b()
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.b(this, localView);
    }
  }
  
  public ea c(float paramFloat)
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.c(this, localView, paramFloat);
    }
    return this;
  }
  
  public ea d(float paramFloat)
  {
    View localView = (View)this.b.get();
    if (localView != null) {
      a.d(this, localView, paramFloat);
    }
    return this;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/ea.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */