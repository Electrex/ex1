package android.support.v4.view;

import android.graphics.Paint;
import android.view.View;

class bz
  extends by
{
  public int a(int paramInt1, int paramInt2, int paramInt3)
  {
    return cj.a(paramInt1, paramInt2, paramInt3);
  }
  
  long a()
  {
    return cj.a();
  }
  
  public void a(View paramView, float paramFloat)
  {
    cj.a(paramView, paramFloat);
  }
  
  public void a(View paramView, int paramInt, Paint paramPaint)
  {
    cj.a(paramView, paramInt, paramPaint);
  }
  
  public void a(View paramView, Paint paramPaint)
  {
    a(paramView, g(paramView), paramPaint);
    paramView.invalidate();
  }
  
  public void a(View paramView, boolean paramBoolean)
  {
    cj.a(paramView, paramBoolean);
  }
  
  public void b(View paramView, float paramFloat)
  {
    cj.b(paramView, paramFloat);
  }
  
  public void b(View paramView, boolean paramBoolean)
  {
    cj.b(paramView, paramBoolean);
  }
  
  public void c(View paramView, float paramFloat)
  {
    cj.c(paramView, paramFloat);
  }
  
  public void d(View paramView, float paramFloat)
  {
    cj.d(paramView, paramFloat);
  }
  
  public void e(View paramView, float paramFloat)
  {
    cj.e(paramView, paramFloat);
  }
  
  public float f(View paramView)
  {
    return cj.a(paramView);
  }
  
  public int g(View paramView)
  {
    return cj.b(paramView);
  }
  
  public int k(View paramView)
  {
    return cj.c(paramView);
  }
  
  public float l(View paramView)
  {
    return cj.d(paramView);
  }
  
  public float m(View paramView)
  {
    return cj.e(paramView);
  }
  
  public float n(View paramView)
  {
    return cj.f(paramView);
  }
  
  public void u(View paramView)
  {
    cj.g(paramView);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/bz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */