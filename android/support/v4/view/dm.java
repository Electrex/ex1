package android.support.v4.view;

import android.os.Bundle;
import android.support.v4.view.a.aj;
import android.support.v4.view.a.g;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

class dm
  extends a
{
  dm(ViewPager paramViewPager) {}
  
  private boolean b()
  {
    return (ViewPager.a(this.b) != null) && (ViewPager.a(this.b).b() > 1);
  }
  
  public void a(View paramView, g paramg)
  {
    super.a(paramView, paramg);
    paramg.b(ViewPager.class.getName());
    paramg.i(b());
    if (this.b.canScrollHorizontally(1)) {
      paramg.a(4096);
    }
    if (this.b.canScrollHorizontally(-1)) {
      paramg.a(8192);
    }
  }
  
  public boolean a(View paramView, int paramInt, Bundle paramBundle)
  {
    if (super.a(paramView, paramInt, paramBundle)) {
      return true;
    }
    switch (paramInt)
    {
    default: 
      return false;
    case 4096: 
      if (this.b.canScrollHorizontally(1))
      {
        this.b.setCurrentItem(1 + ViewPager.b(this.b));
        return true;
      }
      return false;
    }
    if (this.b.canScrollHorizontally(-1))
    {
      this.b.setCurrentItem(-1 + ViewPager.b(this.b));
      return true;
    }
    return false;
  }
  
  public void d(View paramView, AccessibilityEvent paramAccessibilityEvent)
  {
    super.d(paramView, paramAccessibilityEvent);
    paramAccessibilityEvent.setClassName(ViewPager.class.getName());
    aj localaj = aj.a();
    localaj.a(b());
    if ((paramAccessibilityEvent.getEventType() == 4096) && (ViewPager.a(this.b) != null))
    {
      localaj.a(ViewPager.a(this.b).b());
      localaj.b(ViewPager.b(this.b));
      localaj.c(ViewPager.b(this.b));
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/dm.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */