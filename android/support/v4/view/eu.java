package android.support.v4.view;

import android.view.WindowInsets;

class eu
  extends et
{
  private final WindowInsets a;
  
  eu(WindowInsets paramWindowInsets)
  {
    this.a = paramWindowInsets;
  }
  
  public int a()
  {
    return this.a.getSystemWindowInsetLeft();
  }
  
  public et a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    return new eu(this.a.replaceSystemWindowInsets(paramInt1, paramInt2, paramInt3, paramInt4));
  }
  
  public int b()
  {
    return this.a.getSystemWindowInsetTop();
  }
  
  public int c()
  {
    return this.a.getSystemWindowInsetRight();
  }
  
  public int d()
  {
    return this.a.getSystemWindowInsetBottom();
  }
  
  WindowInsets e()
  {
    return this.a;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/eu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */