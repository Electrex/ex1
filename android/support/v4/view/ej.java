package android.support.v4.view;

import android.view.View;
import android.view.animation.Interpolator;

abstract interface ej
{
  public abstract void a(ea paramea, View paramView);
  
  public abstract void a(ea paramea, View paramView, float paramFloat);
  
  public abstract void a(ea paramea, View paramView, long paramLong);
  
  public abstract void a(ea paramea, View paramView, eq parameq);
  
  public abstract void a(ea paramea, View paramView, es parames);
  
  public abstract void a(ea paramea, View paramView, Interpolator paramInterpolator);
  
  public abstract void b(ea paramea, View paramView);
  
  public abstract void b(ea paramea, View paramView, float paramFloat);
  
  public abstract void c(ea paramea, View paramView, float paramFloat);
  
  public abstract void d(ea paramea, View paramView, float paramFloat);
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/ej.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */