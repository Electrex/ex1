package android.support.v4.view;

import android.view.View;
import android.view.ViewParent;

class cb
  extends ca
{
  public void a(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    cl.a(paramView, paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public void a(View paramView, Runnable paramRunnable)
  {
    cl.a(paramView, paramRunnable);
  }
  
  public void a(View paramView, Runnable paramRunnable, long paramLong)
  {
    cl.a(paramView, paramRunnable, paramLong);
  }
  
  public void c(View paramView, int paramInt)
  {
    if (paramInt == 4) {
      paramInt = 2;
    }
    cl.a(paramView, paramInt);
  }
  
  public boolean c(View paramView)
  {
    return cl.a(paramView);
  }
  
  public void d(View paramView)
  {
    cl.b(paramView);
  }
  
  public int e(View paramView)
  {
    return cl.c(paramView);
  }
  
  public ViewParent i(View paramView)
  {
    return cl.d(paramView);
  }
  
  public int o(View paramView)
  {
    return cl.e(paramView);
  }
  
  public int p(View paramView)
  {
    return cl.f(paramView);
  }
  
  public void s(View paramView)
  {
    cl.g(paramView);
  }
  
  public boolean t(View paramView)
  {
    return cl.h(paramView);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/cb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */