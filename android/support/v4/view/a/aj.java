package android.support.v4.view.a;

import android.os.Build.VERSION;
import android.view.View;

public class aj
{
  private static final am a = new ao();
  private final Object b;
  
  static
  {
    if (Build.VERSION.SDK_INT >= 16)
    {
      a = new an();
      return;
    }
    if (Build.VERSION.SDK_INT >= 15)
    {
      a = new al();
      return;
    }
    if (Build.VERSION.SDK_INT >= 14)
    {
      a = new ak();
      return;
    }
  }
  
  public aj(Object paramObject)
  {
    this.b = paramObject;
  }
  
  public static aj a()
  {
    return new aj(a.a());
  }
  
  public void a(int paramInt)
  {
    a.b(this.b, paramInt);
  }
  
  public void a(View paramView)
  {
    a.a(this.b, paramView);
  }
  
  public void a(boolean paramBoolean)
  {
    a.a(this.b, paramBoolean);
  }
  
  public void b(int paramInt)
  {
    a.a(this.b, paramInt);
  }
  
  public void c(int paramInt)
  {
    a.e(this.b, paramInt);
  }
  
  public void d(int paramInt)
  {
    a.c(this.b, paramInt);
  }
  
  public void e(int paramInt)
  {
    a.d(this.b, paramInt);
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {}
    aj localaj;
    do
    {
      do
      {
        return true;
        if (paramObject == null) {
          return false;
        }
        if (getClass() != paramObject.getClass()) {
          return false;
        }
        localaj = (aj)paramObject;
        if (this.b != null) {
          break;
        }
      } while (localaj.b == null);
      return false;
    } while (this.b.equals(localaj.b));
    return false;
  }
  
  public void f(int paramInt)
  {
    a.f(this.b, paramInt);
  }
  
  public void g(int paramInt)
  {
    a.g(this.b, paramInt);
  }
  
  public int hashCode()
  {
    if (this.b == null) {
      return 0;
    }
    return this.b.hashCode();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/view/a/aj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */