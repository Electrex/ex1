package android.support.v4.d;

import android.os.Parcel;
import android.os.Parcelable.Creator;

class d
  implements Parcelable.Creator
{
  final e a;
  
  public d(e parame)
  {
    this.a = parame;
  }
  
  public Object createFromParcel(Parcel paramParcel)
  {
    return this.a.a(paramParcel, null);
  }
  
  public Object[] newArray(int paramInt)
  {
    return this.a.a(paramInt);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/d/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */