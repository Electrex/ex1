package android.support.v4.d;

import android.os.Build.VERSION;
import android.os.Parcelable.Creator;

public class c
{
  public static Parcelable.Creator a(e parame)
  {
    if (Build.VERSION.SDK_INT >= 13) {
      return g.a(parame);
    }
    return new d(parame);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/d/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */