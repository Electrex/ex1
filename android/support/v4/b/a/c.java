package android.support.v4.b.a;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;

abstract interface c
{
  public abstract void a(Drawable paramDrawable);
  
  public abstract void a(Drawable paramDrawable, float paramFloat1, float paramFloat2);
  
  public abstract void a(Drawable paramDrawable, int paramInt);
  
  public abstract void a(Drawable paramDrawable, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
  
  public abstract void a(Drawable paramDrawable, ColorStateList paramColorStateList);
  
  public abstract void a(Drawable paramDrawable, PorterDuff.Mode paramMode);
  
  public abstract void a(Drawable paramDrawable, boolean paramBoolean);
  
  public abstract boolean b(Drawable paramDrawable);
  
  public abstract Drawable c(Drawable paramDrawable);
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/b/a/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */