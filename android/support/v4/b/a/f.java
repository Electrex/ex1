package android.support.v4.b.a;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;

class f
  extends e
{
  public void a(Drawable paramDrawable, float paramFloat1, float paramFloat2)
  {
    l.a(paramDrawable, paramFloat1, paramFloat2);
  }
  
  public void a(Drawable paramDrawable, int paramInt)
  {
    l.a(paramDrawable, paramInt);
  }
  
  public void a(Drawable paramDrawable, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    l.a(paramDrawable, paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public void a(Drawable paramDrawable, ColorStateList paramColorStateList)
  {
    l.a(paramDrawable, paramColorStateList);
  }
  
  public void a(Drawable paramDrawable, PorterDuff.Mode paramMode)
  {
    l.a(paramDrawable, paramMode);
  }
  
  public Drawable c(Drawable paramDrawable)
  {
    return l.a(paramDrawable);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/b/a/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */