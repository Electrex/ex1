package android.support.v4.b.a;

import android.content.res.Resources.Theme;
import android.graphics.Outline;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

class q
  extends p
{
  q(Drawable paramDrawable)
  {
    super(paramDrawable);
  }
  
  public void applyTheme(Resources.Theme paramTheme)
  {
    this.b.applyTheme(paramTheme);
  }
  
  public boolean canApplyTheme()
  {
    return this.b.canApplyTheme();
  }
  
  public Rect getDirtyBounds()
  {
    return this.b.getDirtyBounds();
  }
  
  public void getOutline(Outline paramOutline)
  {
    this.b.getOutline(paramOutline);
  }
  
  public void setHotspot(float paramFloat1, float paramFloat2)
  {
    this.b.setHotspot(paramFloat1, paramFloat2);
  }
  
  public void setHotspotBounds(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.b.setHotspotBounds(paramInt1, paramInt2, paramInt3, paramInt4);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/b/a/q.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */