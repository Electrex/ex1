package android.support.v4.b.a;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.Callback;

class n
  extends Drawable
  implements Drawable.Callback, m
{
  static final PorterDuff.Mode a = PorterDuff.Mode.SRC_IN;
  Drawable b;
  private ColorStateList c;
  private PorterDuff.Mode d = a;
  private int e = Integer.MIN_VALUE;
  
  n(Drawable paramDrawable)
  {
    a(paramDrawable);
  }
  
  private boolean a(int[] paramArrayOfInt)
  {
    if ((this.c != null) && (this.d != null))
    {
      int i = this.c.getColorForState(paramArrayOfInt, this.c.getDefaultColor());
      if (i != this.e)
      {
        setColorFilter(i, this.d);
        this.e = i;
        return true;
      }
    }
    return false;
  }
  
  public Drawable a()
  {
    return this.b;
  }
  
  public void a(Drawable paramDrawable)
  {
    if (this.b != null) {
      this.b.setCallback(null);
    }
    this.b = paramDrawable;
    if (paramDrawable != null) {
      paramDrawable.setCallback(this);
    }
    invalidateSelf();
  }
  
  public void draw(Canvas paramCanvas)
  {
    this.b.draw(paramCanvas);
  }
  
  public int getChangingConfigurations()
  {
    return this.b.getChangingConfigurations();
  }
  
  public Drawable getCurrent()
  {
    return this.b.getCurrent();
  }
  
  public int getIntrinsicHeight()
  {
    return this.b.getIntrinsicHeight();
  }
  
  public int getIntrinsicWidth()
  {
    return this.b.getIntrinsicWidth();
  }
  
  public int getMinimumHeight()
  {
    return this.b.getMinimumHeight();
  }
  
  public int getMinimumWidth()
  {
    return this.b.getMinimumWidth();
  }
  
  public int getOpacity()
  {
    return this.b.getOpacity();
  }
  
  public boolean getPadding(Rect paramRect)
  {
    return this.b.getPadding(paramRect);
  }
  
  public int[] getState()
  {
    return this.b.getState();
  }
  
  public Region getTransparentRegion()
  {
    return this.b.getTransparentRegion();
  }
  
  public void invalidateDrawable(Drawable paramDrawable)
  {
    invalidateSelf();
  }
  
  public boolean isStateful()
  {
    return ((this.c != null) && (this.c.isStateful())) || (this.b.isStateful());
  }
  
  public Drawable mutate()
  {
    Drawable localDrawable1 = this.b;
    Drawable localDrawable2 = localDrawable1.mutate();
    if (localDrawable2 != localDrawable1) {
      a(localDrawable2);
    }
    return this;
  }
  
  protected void onBoundsChange(Rect paramRect)
  {
    this.b.setBounds(paramRect);
  }
  
  protected boolean onLevelChange(int paramInt)
  {
    return this.b.setLevel(paramInt);
  }
  
  public void scheduleDrawable(Drawable paramDrawable, Runnable paramRunnable, long paramLong)
  {
    scheduleSelf(paramRunnable, paramLong);
  }
  
  public void setAlpha(int paramInt)
  {
    this.b.setAlpha(paramInt);
  }
  
  public void setChangingConfigurations(int paramInt)
  {
    this.b.setChangingConfigurations(paramInt);
  }
  
  public void setColorFilter(ColorFilter paramColorFilter)
  {
    this.b.setColorFilter(paramColorFilter);
  }
  
  public void setDither(boolean paramBoolean)
  {
    this.b.setDither(paramBoolean);
  }
  
  public void setFilterBitmap(boolean paramBoolean)
  {
    this.b.setFilterBitmap(paramBoolean);
  }
  
  public boolean setState(int[] paramArrayOfInt)
  {
    boolean bool = this.b.setState(paramArrayOfInt);
    return (a(paramArrayOfInt)) || (bool);
  }
  
  public void setTint(int paramInt)
  {
    setTintList(ColorStateList.valueOf(paramInt));
  }
  
  public void setTintList(ColorStateList paramColorStateList)
  {
    this.c = paramColorStateList;
    a(getState());
  }
  
  public void setTintMode(PorterDuff.Mode paramMode)
  {
    this.d = paramMode;
    a(getState());
  }
  
  public boolean setVisible(boolean paramBoolean1, boolean paramBoolean2)
  {
    return (super.setVisible(paramBoolean1, paramBoolean2)) || (this.b.setVisible(paramBoolean1, paramBoolean2));
  }
  
  public void unscheduleDrawable(Drawable paramDrawable, Runnable paramRunnable)
  {
    unscheduleSelf(paramRunnable);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/b/a/n.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */