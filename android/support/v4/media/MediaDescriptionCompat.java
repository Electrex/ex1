package android.support.v4.media;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;

public final class MediaDescriptionCompat
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR = new a();
  private final String a;
  private final CharSequence b;
  private final CharSequence c;
  private final CharSequence d;
  private final Bitmap e;
  private final Uri f;
  private final Bundle g;
  private Object h;
  
  private MediaDescriptionCompat(Parcel paramParcel)
  {
    this.a = paramParcel.readString();
    this.b = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
    this.c = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
    this.d = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
    this.e = ((Bitmap)paramParcel.readParcelable(null));
    this.f = ((Uri)paramParcel.readParcelable(null));
    this.g = paramParcel.readBundle();
  }
  
  private MediaDescriptionCompat(String paramString, CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, Bitmap paramBitmap, Uri paramUri, Bundle paramBundle)
  {
    this.a = paramString;
    this.b = paramCharSequence1;
    this.c = paramCharSequence2;
    this.d = paramCharSequence3;
    this.e = paramBitmap;
    this.f = paramUri;
    this.g = paramBundle;
  }
  
  public static MediaDescriptionCompat a(Object paramObject)
  {
    if ((paramObject == null) || (Build.VERSION.SDK_INT < 21)) {
      return null;
    }
    b localb = new b();
    localb.a(c.a(paramObject));
    localb.a(c.b(paramObject));
    localb.b(c.c(paramObject));
    localb.c(c.d(paramObject));
    localb.a(c.e(paramObject));
    localb.a(c.f(paramObject));
    localb.a(c.g(paramObject));
    MediaDescriptionCompat localMediaDescriptionCompat = localb.a();
    localMediaDescriptionCompat.h = paramObject;
    return localMediaDescriptionCompat;
  }
  
  public Object a()
  {
    if ((this.h != null) || (Build.VERSION.SDK_INT < 21)) {
      return this.h;
    }
    Object localObject = d.a();
    d.a(localObject, this.a);
    d.a(localObject, this.b);
    d.b(localObject, this.c);
    d.c(localObject, this.d);
    d.a(localObject, this.e);
    d.a(localObject, this.f);
    d.a(localObject, this.g);
    this.h = d.a(localObject);
    return this.h;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String toString()
  {
    return this.b + ", " + this.c + ", " + this.d;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    if (Build.VERSION.SDK_INT < 21)
    {
      paramParcel.writeString(this.a);
      TextUtils.writeToParcel(this.b, paramParcel, paramInt);
      TextUtils.writeToParcel(this.c, paramParcel, paramInt);
      TextUtils.writeToParcel(this.d, paramParcel, paramInt);
      paramParcel.writeParcelable(this.e, paramInt);
      paramParcel.writeParcelable(this.f, paramInt);
      paramParcel.writeBundle(this.g);
      return;
    }
    c.a(a(), paramParcel, paramInt);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/media/MediaDescriptionCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */