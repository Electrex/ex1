package android.support.v4.media.session;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class c
  implements Parcelable.Creator
{
  public MediaSessionCompat.ResultReceiverWrapper a(Parcel paramParcel)
  {
    return new MediaSessionCompat.ResultReceiverWrapper(paramParcel);
  }
  
  public MediaSessionCompat.ResultReceiverWrapper[] a(int paramInt)
  {
    return new MediaSessionCompat.ResultReceiverWrapper[paramInt];
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/media/session/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */