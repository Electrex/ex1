package android.support.v4.media.session;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class g
  implements Parcelable.Creator
{
  public PlaybackStateCompat.CustomAction a(Parcel paramParcel)
  {
    return new PlaybackStateCompat.CustomAction(paramParcel, null);
  }
  
  public PlaybackStateCompat.CustomAction[] a(int paramInt)
  {
    return new PlaybackStateCompat.CustomAction[paramInt];
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/media/session/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */