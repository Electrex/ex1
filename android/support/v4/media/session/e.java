package android.support.v4.media.session;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class e
  implements Parcelable.Creator
{
  public ParcelableVolumeInfo a(Parcel paramParcel)
  {
    return new ParcelableVolumeInfo(paramParcel);
  }
  
  public ParcelableVolumeInfo[] a(int paramInt)
  {
    return new ParcelableVolumeInfo[paramInt];
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/media/session/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */