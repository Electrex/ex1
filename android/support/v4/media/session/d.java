package android.support.v4.media.session;

import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable.Creator;

final class d
  implements Parcelable.Creator
{
  public MediaSessionCompat.Token a(Parcel paramParcel)
  {
    if (Build.VERSION.SDK_INT >= 21) {}
    for (Object localObject = paramParcel.readParcelable(null);; localObject = paramParcel.readStrongBinder()) {
      return new MediaSessionCompat.Token(localObject);
    }
  }
  
  public MediaSessionCompat.Token[] a(int paramInt)
  {
    return new MediaSessionCompat.Token[paramInt];
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/media/session/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */