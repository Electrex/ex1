package android.support.v4.media;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

public final class b
{
  private String a;
  private CharSequence b;
  private CharSequence c;
  private CharSequence d;
  private Bitmap e;
  private Uri f;
  private Bundle g;
  
  public MediaDescriptionCompat a()
  {
    return new MediaDescriptionCompat(this.a, this.b, this.c, this.d, this.e, this.f, this.g, null);
  }
  
  public b a(Bitmap paramBitmap)
  {
    this.e = paramBitmap;
    return this;
  }
  
  public b a(Uri paramUri)
  {
    this.f = paramUri;
    return this;
  }
  
  public b a(Bundle paramBundle)
  {
    this.g = paramBundle;
    return this;
  }
  
  public b a(CharSequence paramCharSequence)
  {
    this.b = paramCharSequence;
    return this;
  }
  
  public b a(String paramString)
  {
    this.a = paramString;
    return this;
  }
  
  public b b(CharSequence paramCharSequence)
  {
    this.c = paramCharSequence;
    return this;
  }
  
  public b c(CharSequence paramCharSequence)
  {
    this.d = paramCharSequence;
    return this;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/media/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */