package android.support.v4.f;

import java.util.Iterator;

final class h
  implements Iterator
{
  final int a;
  int b;
  int c;
  boolean d = false;
  
  h(g paramg, int paramInt)
  {
    this.a = paramInt;
    this.b = paramg.a();
  }
  
  public boolean hasNext()
  {
    return this.c < this.b;
  }
  
  public Object next()
  {
    Object localObject = this.e.a(this.c, this.a);
    this.c = (1 + this.c);
    this.d = true;
    return localObject;
  }
  
  public void remove()
  {
    if (!this.d) {
      throw new IllegalStateException();
    }
    this.c = (-1 + this.c);
    this.b = (-1 + this.b);
    this.d = false;
    this.e.a(this.c);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/f/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */