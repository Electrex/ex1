package android.support.v4.f;

public class o
  implements n
{
  private final Object[] a;
  private int b;
  
  public o(int paramInt)
  {
    if (paramInt <= 0) {
      throw new IllegalArgumentException("The max pool size must be > 0");
    }
    this.a = new Object[paramInt];
  }
  
  private boolean b(Object paramObject)
  {
    for (int i = 0;; i++)
    {
      int j = this.b;
      boolean bool = false;
      if (i < j)
      {
        if (this.a[i] == paramObject) {
          bool = true;
        }
      }
      else {
        return bool;
      }
    }
  }
  
  public Object a()
  {
    if (this.b > 0)
    {
      int i = -1 + this.b;
      Object localObject = this.a[i];
      this.a[i] = null;
      this.b = (-1 + this.b);
      return localObject;
    }
    return null;
  }
  
  public boolean a(Object paramObject)
  {
    if (b(paramObject)) {
      throw new IllegalStateException("Already in the pool!");
    }
    if (this.b < this.a.length)
    {
      this.a[this.b] = paramObject;
      this.b = (1 + this.b);
      return true;
    }
    return false;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/f/o.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */