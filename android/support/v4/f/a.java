package android.support.v4.f;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class a
  extends p
  implements Map
{
  g a;
  
  private g b()
  {
    if (this.a == null) {
      this.a = new b(this);
    }
    return this.a;
  }
  
  public boolean a(Collection paramCollection)
  {
    return g.c(this, paramCollection);
  }
  
  public Set entrySet()
  {
    return b().d();
  }
  
  public Set keySet()
  {
    return b().e();
  }
  
  public void putAll(Map paramMap)
  {
    a(this.h + paramMap.size());
    Iterator localIterator = paramMap.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      put(localEntry.getKey(), localEntry.getValue());
    }
  }
  
  public Collection values()
  {
    return b().f();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/f/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */