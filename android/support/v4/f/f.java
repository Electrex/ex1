package android.support.v4.f;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;

public class f
{
  private final LinkedHashMap a;
  private int b;
  private int c;
  private int d;
  private int e;
  private int f;
  private int g;
  private int h;
  
  public f(int paramInt)
  {
    if (paramInt <= 0) {
      throw new IllegalArgumentException("maxSize <= 0");
    }
    this.c = paramInt;
    this.a = new LinkedHashMap(0, 0.75F, true);
  }
  
  private int c(Object paramObject1, Object paramObject2)
  {
    int i = b(paramObject1, paramObject2);
    if (i < 0) {
      throw new IllegalStateException("Negative size: " + paramObject1 + "=" + paramObject2);
    }
    return i;
  }
  
  public final Object a(Object paramObject)
  {
    if (paramObject == null) {
      throw new NullPointerException("key == null");
    }
    Object localObject3;
    try
    {
      Object localObject2 = this.a.get(paramObject);
      if (localObject2 != null)
      {
        this.g = (1 + this.g);
        return localObject2;
      }
      this.h = (1 + this.h);
      localObject3 = b(paramObject);
      if (localObject3 == null) {
        return null;
      }
    }
    finally {}
    try
    {
      this.e = (1 + this.e);
      Object localObject5 = this.a.put(paramObject, localObject3);
      if (localObject5 != null) {
        this.a.put(paramObject, localObject5);
      }
      for (;;)
      {
        if (localObject5 == null) {
          break;
        }
        a(false, paramObject, localObject3, localObject5);
        return localObject5;
        this.b += c(paramObject, localObject3);
      }
      a(this.c);
    }
    finally {}
    return localObject3;
  }
  
  public final Object a(Object paramObject1, Object paramObject2)
  {
    if ((paramObject1 == null) || (paramObject2 == null)) {
      throw new NullPointerException("key == null || value == null");
    }
    try
    {
      this.d = (1 + this.d);
      this.b += c(paramObject1, paramObject2);
      Object localObject2 = this.a.put(paramObject1, paramObject2);
      if (localObject2 != null) {
        this.b -= c(paramObject1, localObject2);
      }
      if (localObject2 != null) {
        a(false, paramObject1, localObject2, paramObject2);
      }
      a(this.c);
      return localObject2;
    }
    finally {}
  }
  
  public void a(int paramInt)
  {
    Object localObject2;
    Object localObject3;
    try
    {
      if ((this.b < 0) || ((this.a.isEmpty()) && (this.b != 0))) {
        throw new IllegalStateException(getClass().getName() + ".sizeOf() is reporting inconsistent results!");
      }
    }
    finally
    {
      throw ((Throwable)localObject1);
      if ((this.b <= paramInt) || (this.a.isEmpty())) {
        return;
      }
      Map.Entry localEntry = (Map.Entry)this.a.entrySet().iterator().next();
      localObject2 = localEntry.getKey();
      localObject3 = localEntry.getValue();
      this.a.remove(localObject2);
      this.b -= c(localObject2, localObject3);
      this.f = (1 + this.f);
    }
  }
  
  protected void a(boolean paramBoolean, Object paramObject1, Object paramObject2, Object paramObject3) {}
  
  protected int b(Object paramObject1, Object paramObject2)
  {
    return 1;
  }
  
  protected Object b(Object paramObject)
  {
    return null;
  }
  
  public final String toString()
  {
    try
    {
      int i = this.g + this.h;
      int j = 0;
      if (i != 0) {
        j = 100 * this.g / i;
      }
      Object[] arrayOfObject = new Object[4];
      arrayOfObject[0] = Integer.valueOf(this.c);
      arrayOfObject[1] = Integer.valueOf(this.g);
      arrayOfObject[2] = Integer.valueOf(this.h);
      arrayOfObject[3] = Integer.valueOf(j);
      String str = String.format("LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]", arrayOfObject);
      return str;
    }
    finally {}
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/f/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */