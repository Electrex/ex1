package android.support.v4.f;

import java.util.Iterator;
import java.util.Map.Entry;

final class k
  implements Iterator, Map.Entry
{
  int a;
  int b;
  boolean c = false;
  
  k(g paramg)
  {
    this.a = (-1 + paramg.a());
    this.b = -1;
  }
  
  public Map.Entry a()
  {
    this.b = (1 + this.b);
    this.c = true;
    return this;
  }
  
  public final boolean equals(Object paramObject)
  {
    int i = 1;
    if (!this.c) {
      throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }
    if (!(paramObject instanceof Map.Entry)) {
      return false;
    }
    Map.Entry localEntry = (Map.Entry)paramObject;
    if ((c.a(localEntry.getKey(), this.d.a(this.b, 0))) && (c.a(localEntry.getValue(), this.d.a(this.b, i)))) {}
    for (;;)
    {
      return i;
      int j = 0;
    }
  }
  
  public Object getKey()
  {
    if (!this.c) {
      throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }
    return this.d.a(this.b, 0);
  }
  
  public Object getValue()
  {
    if (!this.c) {
      throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }
    return this.d.a(this.b, 1);
  }
  
  public boolean hasNext()
  {
    return this.b < this.a;
  }
  
  public final int hashCode()
  {
    if (!this.c) {
      throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }
    Object localObject1 = this.d.a(this.b, 0);
    Object localObject2 = this.d.a(this.b, 1);
    int i;
    int j;
    if (localObject1 == null)
    {
      i = 0;
      j = 0;
      if (localObject2 != null) {
        break label69;
      }
    }
    for (;;)
    {
      return j ^ i;
      i = localObject1.hashCode();
      break;
      label69:
      j = localObject2.hashCode();
    }
  }
  
  public void remove()
  {
    if (!this.c) {
      throw new IllegalStateException();
    }
    this.d.a(this.b);
    this.b = (-1 + this.b);
    this.a = (-1 + this.a);
    this.c = false;
  }
  
  public Object setValue(Object paramObject)
  {
    if (!this.c) {
      throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }
    return this.d.a(this.b, paramObject);
  }
  
  public final String toString()
  {
    return getKey() + "=" + getValue();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/f/k.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */