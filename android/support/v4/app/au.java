package android.support.v4.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;

class au
  extends at
{
  public Intent a(Activity paramActivity)
  {
    Intent localIntent = av.a(paramActivity);
    if (localIntent == null) {
      localIntent = b(paramActivity);
    }
    return localIntent;
  }
  
  public String a(Context paramContext, ActivityInfo paramActivityInfo)
  {
    String str = av.a(paramActivityInfo);
    if (str == null) {
      str = super.a(paramContext, paramActivityInfo);
    }
    return str;
  }
  
  public boolean a(Activity paramActivity, Intent paramIntent)
  {
    return av.a(paramActivity, paramIntent);
  }
  
  Intent b(Activity paramActivity)
  {
    return super.a(paramActivity);
  }
  
  public void b(Activity paramActivity, Intent paramIntent)
  {
    av.b(paramActivity, paramIntent);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/app/au.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */