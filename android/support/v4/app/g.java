package android.support.v4.app;

import android.support.v4.f.a;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import java.util.ArrayList;

class g
  implements ViewTreeObserver.OnPreDrawListener
{
  g(e parame, View paramView, Object paramObject, ArrayList paramArrayList, j paramj, boolean paramBoolean, Fragment paramFragment1, Fragment paramFragment2) {}
  
  public boolean onPreDraw()
  {
    this.a.getViewTreeObserver().removeOnPreDrawListener(this);
    if (this.b != null)
    {
      ag.a(this.b, this.c);
      this.c.clear();
      a locala = e.a(this.h, this.d, this.e, this.f);
      this.c.add(this.d.d);
      this.c.addAll(locala.values());
      ag.b(this.b, this.c);
      e.a(this.h, locala, this.d);
      e.a(this.h, this.d, this.f, this.g, this.e, locala);
    }
    return true;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/app/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */