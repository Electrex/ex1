package android.support.v4.app;

import android.os.Parcelable;
import android.support.v4.view.bo;
import android.view.View;
import android.view.ViewGroup;

public abstract class aa
  extends bo
{
  private final t a;
  private af b = null;
  private Fragment c = null;
  
  public aa(t paramt)
  {
    this.a = paramt;
  }
  
  private static String a(int paramInt, long paramLong)
  {
    return "android:switcher:" + paramInt + ":" + paramLong;
  }
  
  public Parcelable a()
  {
    return null;
  }
  
  public abstract Fragment a(int paramInt);
  
  public Object a(ViewGroup paramViewGroup, int paramInt)
  {
    if (this.b == null) {
      this.b = this.a.a();
    }
    long l = b(paramInt);
    String str = a(paramViewGroup.getId(), l);
    Fragment localFragment = this.a.a(str);
    if (localFragment != null) {
      this.b.b(localFragment);
    }
    for (;;)
    {
      if (localFragment != this.c)
      {
        localFragment.c(false);
        localFragment.d(false);
      }
      return localFragment;
      localFragment = a(paramInt);
      this.b.a(paramViewGroup.getId(), localFragment, a(paramViewGroup.getId(), l));
    }
  }
  
  public void a(Parcelable paramParcelable, ClassLoader paramClassLoader) {}
  
  public void a(ViewGroup paramViewGroup) {}
  
  public void a(ViewGroup paramViewGroup, int paramInt, Object paramObject)
  {
    if (this.b == null) {
      this.b = this.a.a();
    }
    this.b.a((Fragment)paramObject);
  }
  
  public boolean a(View paramView, Object paramObject)
  {
    return ((Fragment)paramObject).i() == paramView;
  }
  
  public long b(int paramInt)
  {
    return paramInt;
  }
  
  public void b(ViewGroup paramViewGroup)
  {
    if (this.b != null)
    {
      this.b.b();
      this.b = null;
      this.a.b();
    }
  }
  
  public void b(ViewGroup paramViewGroup, int paramInt, Object paramObject)
  {
    Fragment localFragment = (Fragment)paramObject;
    if (localFragment != this.c)
    {
      if (this.c != null)
      {
        this.c.c(false);
        this.c.d(false);
      }
      if (localFragment != null)
      {
        localFragment.c(true);
        localFragment.d(true);
      }
      this.c = localFragment;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/app/aa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */