package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;

final class BackStackState
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR = new k();
  final int[] a;
  final int b;
  final int c;
  final String d;
  final int e;
  final int f;
  final CharSequence g;
  final int h;
  final CharSequence i;
  final ArrayList j;
  final ArrayList k;
  
  public BackStackState(Parcel paramParcel)
  {
    this.a = paramParcel.createIntArray();
    this.b = paramParcel.readInt();
    this.c = paramParcel.readInt();
    this.d = paramParcel.readString();
    this.e = paramParcel.readInt();
    this.f = paramParcel.readInt();
    this.g = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
    this.h = paramParcel.readInt();
    this.i = ((CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel));
    this.j = paramParcel.createStringArrayList();
    this.k = paramParcel.createStringArrayList();
  }
  
  public BackStackState(v paramv, e parame)
  {
    i locali1 = parame.c;
    int m = 0;
    while (locali1 != null)
    {
      if (locali1.i != null) {
        m += locali1.i.size();
      }
      locali1 = locali1.a;
    }
    this.a = new int[m + 7 * parame.e];
    if (!parame.l) {
      throw new IllegalStateException("Not on back stack");
    }
    i locali2 = parame.c;
    int n = 0;
    if (locali2 != null)
    {
      int[] arrayOfInt1 = this.a;
      int i1 = n + 1;
      arrayOfInt1[n] = locali2.c;
      int[] arrayOfInt2 = this.a;
      int i2 = i1 + 1;
      if (locali2.d != null) {}
      int i7;
      int i9;
      for (int i3 = locali2.d.g;; i3 = -1)
      {
        arrayOfInt2[i1] = i3;
        int[] arrayOfInt3 = this.a;
        int i4 = i2 + 1;
        arrayOfInt3[i2] = locali2.e;
        int[] arrayOfInt4 = this.a;
        int i5 = i4 + 1;
        arrayOfInt4[i4] = locali2.f;
        int[] arrayOfInt5 = this.a;
        int i6 = i5 + 1;
        arrayOfInt5[i5] = locali2.g;
        int[] arrayOfInt6 = this.a;
        i7 = i6 + 1;
        arrayOfInt6[i6] = locali2.h;
        if (locali2.i == null) {
          break label347;
        }
        int i8 = locali2.i.size();
        int[] arrayOfInt8 = this.a;
        i9 = i7 + 1;
        arrayOfInt8[i7] = i8;
        int i10 = 0;
        while (i10 < i8)
        {
          int[] arrayOfInt9 = this.a;
          int i11 = i9 + 1;
          arrayOfInt9[i9] = ((Fragment)locali2.i.get(i10)).g;
          i10++;
          i9 = i11;
        }
      }
      n = i9;
      for (;;)
      {
        locali2 = locali2.a;
        break;
        label347:
        int[] arrayOfInt7 = this.a;
        n = i7 + 1;
        arrayOfInt7[i7] = 0;
      }
    }
    this.b = parame.j;
    this.c = parame.k;
    this.d = parame.n;
    this.e = parame.p;
    this.f = parame.q;
    this.g = parame.r;
    this.h = parame.s;
    this.i = parame.t;
    this.j = parame.u;
    this.k = parame.v;
  }
  
  public e a(v paramv)
  {
    e locale = new e(paramv);
    int m = 0;
    int i8;
    for (int n = 0; n < this.a.length; n = i8)
    {
      i locali = new i();
      int[] arrayOfInt1 = this.a;
      int i1 = n + 1;
      locali.c = arrayOfInt1[n];
      if (v.a) {
        Log.v("FragmentManager", "Instantiate " + locale + " op #" + m + " base fragment #" + this.a[i1]);
      }
      int[] arrayOfInt2 = this.a;
      int i2 = i1 + 1;
      int i3 = arrayOfInt2[i1];
      if (i3 >= 0) {}
      for (locali.d = ((Fragment)paramv.f.get(i3));; locali.d = null)
      {
        int[] arrayOfInt3 = this.a;
        int i4 = i2 + 1;
        locali.e = arrayOfInt3[i2];
        int[] arrayOfInt4 = this.a;
        int i5 = i4 + 1;
        locali.f = arrayOfInt4[i4];
        int[] arrayOfInt5 = this.a;
        int i6 = i5 + 1;
        locali.g = arrayOfInt5[i5];
        int[] arrayOfInt6 = this.a;
        int i7 = i6 + 1;
        locali.h = arrayOfInt6[i6];
        int[] arrayOfInt7 = this.a;
        i8 = i7 + 1;
        int i9 = arrayOfInt7[i7];
        if (i9 <= 0) {
          break;
        }
        locali.i = new ArrayList(i9);
        int i10 = 0;
        while (i10 < i9)
        {
          if (v.a) {
            Log.v("FragmentManager", "Instantiate " + locale + " set remove fragment #" + this.a[i8]);
          }
          ArrayList localArrayList = paramv.f;
          int[] arrayOfInt8 = this.a;
          int i11 = i8 + 1;
          Fragment localFragment = (Fragment)localArrayList.get(arrayOfInt8[i8]);
          locali.i.add(localFragment);
          i10++;
          i8 = i11;
        }
      }
      locale.a(locali);
      m++;
    }
    locale.j = this.b;
    locale.k = this.c;
    locale.n = this.d;
    locale.p = this.e;
    locale.l = true;
    locale.q = this.f;
    locale.r = this.g;
    locale.s = this.h;
    locale.t = this.i;
    locale.u = this.j;
    locale.v = this.k;
    locale.a(1);
    return locale;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeIntArray(this.a);
    paramParcel.writeInt(this.b);
    paramParcel.writeInt(this.c);
    paramParcel.writeString(this.d);
    paramParcel.writeInt(this.e);
    paramParcel.writeInt(this.f);
    TextUtils.writeToParcel(this.g, paramParcel, 0);
    paramParcel.writeInt(this.h);
    TextUtils.writeToParcel(this.i, paramParcel, 0);
    paramParcel.writeStringList(this.j);
    paramParcel.writeStringList(this.k);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/app/BackStackState.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */