package android.support.v4.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.a.a;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

public class az
  implements Iterable
{
  private static final bb a = new bc();
  private final ArrayList b = new ArrayList();
  private final Context c;
  
  static
  {
    if (Build.VERSION.SDK_INT >= 11)
    {
      a = new bd();
      return;
    }
  }
  
  private az(Context paramContext)
  {
    this.c = paramContext;
  }
  
  public static az a(Context paramContext)
  {
    return new az(paramContext);
  }
  
  public az a(Activity paramActivity)
  {
    boolean bool = paramActivity instanceof ba;
    Intent localIntent1 = null;
    if (bool) {
      localIntent1 = ((ba)paramActivity).c_();
    }
    if (localIntent1 == null) {}
    for (Intent localIntent2 = ar.b(paramActivity);; localIntent2 = localIntent1)
    {
      if (localIntent2 != null)
      {
        ComponentName localComponentName = localIntent2.getComponent();
        if (localComponentName == null) {
          localComponentName = localIntent2.resolveActivity(this.c.getPackageManager());
        }
        a(localComponentName);
        a(localIntent2);
      }
      return this;
    }
  }
  
  public az a(ComponentName paramComponentName)
  {
    int i = this.b.size();
    try
    {
      Intent localIntent;
      for (Object localObject = ar.a(this.c, paramComponentName); localObject != null; localObject = localIntent)
      {
        this.b.add(i, localObject);
        localIntent = ar.a(this.c, ((Intent)localObject).getComponent());
      }
      return this;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      Log.e("TaskStackBuilder", "Bad ComponentName while traversing activity parent metadata");
      throw new IllegalArgumentException(localNameNotFoundException);
    }
  }
  
  public az a(Intent paramIntent)
  {
    this.b.add(paramIntent);
    return this;
  }
  
  public az a(Class paramClass)
  {
    return a(new ComponentName(this.c, paramClass));
  }
  
  public void a()
  {
    a(null);
  }
  
  public void a(Bundle paramBundle)
  {
    if (this.b.isEmpty()) {
      throw new IllegalStateException("No intents added to TaskStackBuilder; cannot startActivities");
    }
    Intent[] arrayOfIntent = (Intent[])this.b.toArray(new Intent[this.b.size()]);
    arrayOfIntent[0] = new Intent(arrayOfIntent[0]).addFlags(268484608);
    if (!a.a(this.c, arrayOfIntent, paramBundle))
    {
      Intent localIntent = new Intent(arrayOfIntent[(-1 + arrayOfIntent.length)]);
      localIntent.addFlags(268435456);
      this.c.startActivity(localIntent);
    }
  }
  
  public Iterator iterator()
  {
    return this.b.iterator();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/app/az.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */