package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class ab
  implements Parcelable.Creator
{
  public FragmentState a(Parcel paramParcel)
  {
    return new FragmentState(paramParcel);
  }
  
  public FragmentState[] a(int paramInt)
  {
    return new FragmentState[paramInt];
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/app/ab.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */