package android.support.v4.app;

import android.graphics.Rect;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class ag
{
  public static Object a(Object paramObject)
  {
    if (paramObject != null) {
      paramObject = ((Transition)paramObject).clone();
    }
    return paramObject;
  }
  
  public static Object a(Object paramObject, View paramView1, ArrayList paramArrayList, Map paramMap, View paramView2)
  {
    if (paramObject != null)
    {
      b(paramArrayList, paramView1);
      if (paramMap != null) {
        paramArrayList.removeAll(paramMap.values());
      }
      if (paramArrayList.isEmpty()) {
        paramObject = null;
      }
    }
    else
    {
      return paramObject;
    }
    paramArrayList.add(paramView2);
    b((Transition)paramObject, paramArrayList);
    return paramObject;
  }
  
  public static Object a(Object paramObject1, Object paramObject2, Object paramObject3, boolean paramBoolean)
  {
    Transition localTransition1 = (Transition)paramObject1;
    Transition localTransition2 = (Transition)paramObject2;
    Transition localTransition3 = (Transition)paramObject3;
    if ((localTransition1 != null) && (localTransition2 != null)) {}
    for (;;)
    {
      if (paramBoolean)
      {
        TransitionSet localTransitionSet1 = new TransitionSet();
        if (localTransition1 != null) {
          localTransitionSet1.addTransition(localTransition1);
        }
        if (localTransition2 != null) {
          localTransitionSet1.addTransition(localTransition2);
        }
        if (localTransition3 != null) {
          localTransitionSet1.addTransition(localTransition3);
        }
        return localTransitionSet1;
      }
      Object localObject;
      if ((localTransition2 != null) && (localTransition1 != null)) {
        localObject = new TransitionSet().addTransition(localTransition2).addTransition(localTransition1).setOrdering(1);
      }
      while (localTransition3 != null)
      {
        TransitionSet localTransitionSet2 = new TransitionSet();
        if (localObject != null) {
          localTransitionSet2.addTransition((Transition)localObject);
        }
        localTransitionSet2.addTransition(localTransition3);
        return localTransitionSet2;
        if (localTransition2 != null)
        {
          localObject = localTransition2;
        }
        else
        {
          localObject = null;
          if (localTransition1 != null) {
            localObject = localTransition1;
          }
        }
      }
      return localObject;
      paramBoolean = true;
    }
  }
  
  public static String a(View paramView)
  {
    return paramView.getTransitionName();
  }
  
  private static void a(Transition paramTransition, al paramal)
  {
    if (paramTransition != null) {
      paramTransition.setEpicenterCallback(new aj(paramal));
    }
  }
  
  public static void a(View paramView1, View paramView2, Object paramObject1, ArrayList paramArrayList1, Object paramObject2, ArrayList paramArrayList2, Object paramObject3, ArrayList paramArrayList3, Object paramObject4, ArrayList paramArrayList4, Map paramMap)
  {
    Transition localTransition1 = (Transition)paramObject1;
    Transition localTransition2 = (Transition)paramObject2;
    Transition localTransition3 = (Transition)paramObject3;
    Transition localTransition4 = (Transition)paramObject4;
    if (localTransition4 != null) {
      paramView1.getViewTreeObserver().addOnPreDrawListener(new ak(paramView1, localTransition1, paramView2, paramArrayList1, localTransition2, paramArrayList2, localTransition3, paramArrayList3, paramMap, paramArrayList4, localTransition4));
    }
  }
  
  public static void a(ViewGroup paramViewGroup, Object paramObject)
  {
    TransitionManager.beginDelayedTransition(paramViewGroup, (Transition)paramObject);
  }
  
  public static void a(Object paramObject, View paramView)
  {
    ((Transition)paramObject).setEpicenterCallback(new ah(c(paramView)));
  }
  
  public static void a(Object paramObject, View paramView, boolean paramBoolean)
  {
    ((Transition)paramObject).excludeTarget(paramView, paramBoolean);
  }
  
  public static void a(Object paramObject1, Object paramObject2, View paramView1, am paramam, View paramView2, al paramal, Map paramMap1, ArrayList paramArrayList1, Map paramMap2, ArrayList paramArrayList2)
  {
    if ((paramObject1 != null) || (paramObject2 != null))
    {
      Transition localTransition = (Transition)paramObject1;
      if (localTransition != null) {
        localTransition.addTarget(paramView2);
      }
      if (paramObject2 != null) {
        b((Transition)paramObject2, paramArrayList2);
      }
      if (paramam != null) {
        paramView1.getViewTreeObserver().addOnPreDrawListener(new ai(paramView1, paramam, paramMap1, paramMap2, localTransition, paramArrayList1, paramView2));
      }
      a(localTransition, paramal);
    }
  }
  
  public static void a(Object paramObject, ArrayList paramArrayList)
  {
    Transition localTransition = (Transition)paramObject;
    if ((localTransition instanceof TransitionSet))
    {
      TransitionSet localTransitionSet = (TransitionSet)localTransition;
      int j = localTransitionSet.getTransitionCount();
      for (int k = 0; k < j; k++) {
        a(localTransitionSet.getTransitionAt(k), paramArrayList);
      }
    }
    if (!a(localTransition))
    {
      List localList = localTransition.getTargets();
      if ((localList != null) && (localList.size() == paramArrayList.size()) && (localList.containsAll(paramArrayList))) {
        for (int i = -1 + paramArrayList.size(); i >= 0; i--) {
          localTransition.removeTarget((View)paramArrayList.get(i));
        }
      }
    }
  }
  
  public static void a(Map paramMap, View paramView)
  {
    if (paramView.getVisibility() == 0)
    {
      String str = paramView.getTransitionName();
      if (str != null) {
        paramMap.put(str, paramView);
      }
      if ((paramView instanceof ViewGroup))
      {
        ViewGroup localViewGroup = (ViewGroup)paramView;
        int i = localViewGroup.getChildCount();
        for (int j = 0; j < i; j++) {
          a(paramMap, localViewGroup.getChildAt(j));
        }
      }
    }
  }
  
  private static boolean a(Transition paramTransition)
  {
    return (!a(paramTransition.getTargetIds())) || (!a(paramTransition.getTargetNames())) || (!a(paramTransition.getTargetTypes()));
  }
  
  private static boolean a(List paramList)
  {
    return (paramList == null) || (paramList.isEmpty());
  }
  
  public static void b(Object paramObject, ArrayList paramArrayList)
  {
    int i = 0;
    Transition localTransition = (Transition)paramObject;
    if ((localTransition instanceof TransitionSet))
    {
      TransitionSet localTransitionSet = (TransitionSet)localTransition;
      int m = localTransitionSet.getTransitionCount();
      while (i < m)
      {
        b(localTransitionSet.getTransitionAt(i), paramArrayList);
        i++;
      }
    }
    if ((!a(localTransition)) && (a(localTransition.getTargets())))
    {
      int j = paramArrayList.size();
      for (int k = 0; k < j; k++) {
        localTransition.addTarget((View)paramArrayList.get(k));
      }
    }
  }
  
  private static void b(ArrayList paramArrayList, View paramView)
  {
    ViewGroup localViewGroup;
    if (paramView.getVisibility() == 0)
    {
      if (!(paramView instanceof ViewGroup)) {
        break label65;
      }
      localViewGroup = (ViewGroup)paramView;
      if (!localViewGroup.isTransitionGroup()) {
        break label33;
      }
      paramArrayList.add(localViewGroup);
    }
    for (;;)
    {
      return;
      label33:
      int i = localViewGroup.getChildCount();
      for (int j = 0; j < i; j++) {
        b(paramArrayList, localViewGroup.getChildAt(j));
      }
    }
    label65:
    paramArrayList.add(paramView);
  }
  
  private static Rect c(View paramView)
  {
    Rect localRect = new Rect();
    int[] arrayOfInt = new int[2];
    paramView.getLocationOnScreen(arrayOfInt);
    localRect.set(arrayOfInt[0], arrayOfInt[1], arrayOfInt[0] + paramView.getWidth(), arrayOfInt[1] + paramView.getHeight());
    return localRect;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/app/ag.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */