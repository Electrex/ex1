package android.support.v4.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;

abstract interface as
{
  public abstract Intent a(Activity paramActivity);
  
  public abstract String a(Context paramContext, ActivityInfo paramActivityInfo);
  
  public abstract boolean a(Activity paramActivity, Intent paramIntent);
  
  public abstract void b(Activity paramActivity, Intent paramIntent);
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/app/as.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */