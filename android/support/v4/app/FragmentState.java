package android.support.v4.app;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.Log;

final class FragmentState
  implements Parcelable
{
  public static final Parcelable.Creator CREATOR = new ab();
  final String a;
  final int b;
  final boolean c;
  final int d;
  final int e;
  final String f;
  final boolean g;
  final boolean h;
  final Bundle i;
  Bundle j;
  Fragment k;
  
  public FragmentState(Parcel paramParcel)
  {
    this.a = paramParcel.readString();
    this.b = paramParcel.readInt();
    boolean bool2;
    boolean bool3;
    if (paramParcel.readInt() != 0)
    {
      bool2 = bool1;
      this.c = bool2;
      this.d = paramParcel.readInt();
      this.e = paramParcel.readInt();
      this.f = paramParcel.readString();
      if (paramParcel.readInt() == 0) {
        break label110;
      }
      bool3 = bool1;
      label70:
      this.g = bool3;
      if (paramParcel.readInt() == 0) {
        break label116;
      }
    }
    for (;;)
    {
      this.h = bool1;
      this.i = paramParcel.readBundle();
      this.j = paramParcel.readBundle();
      return;
      bool2 = false;
      break;
      label110:
      bool3 = false;
      break label70;
      label116:
      bool1 = false;
    }
  }
  
  public FragmentState(Fragment paramFragment)
  {
    this.a = paramFragment.getClass().getName();
    this.b = paramFragment.g;
    this.c = paramFragment.p;
    this.d = paramFragment.x;
    this.e = paramFragment.y;
    this.f = paramFragment.z;
    this.g = paramFragment.C;
    this.h = paramFragment.B;
    this.i = paramFragment.i;
  }
  
  public Fragment a(o paramo, Fragment paramFragment)
  {
    if (this.k != null) {
      return this.k;
    }
    if (this.i != null) {
      this.i.setClassLoader(paramo.getClassLoader());
    }
    this.k = Fragment.a(paramo, this.a, this.i);
    if (this.j != null)
    {
      this.j.setClassLoader(paramo.getClassLoader());
      this.k.e = this.j;
    }
    this.k.a(this.b, paramFragment);
    this.k.p = this.c;
    this.k.r = true;
    this.k.x = this.d;
    this.k.y = this.e;
    this.k.z = this.f;
    this.k.C = this.g;
    this.k.B = this.h;
    this.k.t = paramo.b;
    if (v.a) {
      Log.v("FragmentManager", "Instantiated fragment " + this.k);
    }
    return this.k;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    int m = 1;
    paramParcel.writeString(this.a);
    paramParcel.writeInt(this.b);
    int n;
    int i1;
    if (this.c)
    {
      n = m;
      paramParcel.writeInt(n);
      paramParcel.writeInt(this.d);
      paramParcel.writeInt(this.e);
      paramParcel.writeString(this.f);
      if (!this.g) {
        break label109;
      }
      i1 = m;
      label68:
      paramParcel.writeInt(i1);
      if (!this.h) {
        break label115;
      }
    }
    for (;;)
    {
      paramParcel.writeInt(m);
      paramParcel.writeBundle(this.i);
      paramParcel.writeBundle(this.j);
      return;
      n = 0;
      break;
      label109:
      i1 = 0;
      break label68;
      label115:
      m = 0;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/app/FragmentState.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */