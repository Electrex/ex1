package android.support.v4.app;

import android.content.Context;
import android.os.Parcelable;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import java.util.ArrayList;

public class FragmentTabHost
  extends TabHost
  implements TabHost.OnTabChangeListener
{
  private final ArrayList a;
  private Context b;
  private t c;
  private int d;
  private TabHost.OnTabChangeListener e;
  private ae f;
  private boolean g;
  
  private af a(String paramString, af paramaf)
  {
    Object localObject1 = null;
    int i = 0;
    Object localObject2;
    if (i < this.a.size())
    {
      localObject2 = (ae)this.a.get(i);
      if (!ae.b((ae)localObject2).equals(paramString)) {
        break label202;
      }
    }
    for (;;)
    {
      i++;
      localObject1 = localObject2;
      break;
      if (localObject1 == null) {
        throw new IllegalStateException("No tab known for tag " + paramString);
      }
      if (this.f != localObject1)
      {
        if (paramaf == null) {
          paramaf = this.c.a();
        }
        if ((this.f != null) && (ae.a(this.f) != null)) {
          paramaf.a(ae.a(this.f));
        }
        if (localObject1 != null)
        {
          if (ae.a((ae)localObject1) != null) {
            break label190;
          }
          ae.a((ae)localObject1, Fragment.a(this.b, ae.c((ae)localObject1).getName(), ae.d((ae)localObject1)));
          paramaf.a(this.d, ae.a((ae)localObject1), ae.b((ae)localObject1));
        }
      }
      for (;;)
      {
        this.f = ((ae)localObject1);
        return paramaf;
        label190:
        paramaf.b(ae.a((ae)localObject1));
      }
      label202:
      localObject2 = localObject1;
    }
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    String str = getCurrentTabTag();
    af localaf1 = null;
    int i = 0;
    if (i < this.a.size())
    {
      ae localae = (ae)this.a.get(i);
      ae.a(localae, this.c.a(ae.b(localae)));
      if ((ae.a(localae) != null) && (!ae.a(localae).g()))
      {
        if (!ae.b(localae).equals(str)) {
          break label98;
        }
        this.f = localae;
      }
      for (;;)
      {
        i++;
        break;
        label98:
        if (localaf1 == null) {
          localaf1 = this.c.a();
        }
        localaf1.a(ae.a(localae));
      }
    }
    this.g = true;
    af localaf2 = a(str, localaf1);
    if (localaf2 != null)
    {
      localaf2.a();
      this.c.b();
    }
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    this.g = false;
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    FragmentTabHost.SavedState localSavedState = (FragmentTabHost.SavedState)paramParcelable;
    super.onRestoreInstanceState(localSavedState.getSuperState());
    setCurrentTabByTag(localSavedState.a);
  }
  
  protected Parcelable onSaveInstanceState()
  {
    FragmentTabHost.SavedState localSavedState = new FragmentTabHost.SavedState(super.onSaveInstanceState());
    localSavedState.a = getCurrentTabTag();
    return localSavedState;
  }
  
  public void onTabChanged(String paramString)
  {
    if (this.g)
    {
      af localaf = a(paramString, null);
      if (localaf != null) {
        localaf.a();
      }
    }
    if (this.e != null) {
      this.e.onTabChanged(paramString);
    }
  }
  
  public void setOnTabChangedListener(TabHost.OnTabChangeListener paramOnTabChangeListener)
  {
    this.e = paramOnTabChangeListener;
  }
  
  @Deprecated
  public void setup()
  {
    throw new IllegalStateException("Must call setup() that takes a Context and FragmentManager");
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/app/FragmentTabHost.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */