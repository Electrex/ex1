package android.support.v4.app;

import android.app.Activity;
import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.f.d;
import android.support.v4.f.p;
import android.support.v4.view.ac;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnCreateContextMenuListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class Fragment
  implements ComponentCallbacks, View.OnCreateContextMenuListener
{
  static final Object a = new Object();
  private static final p aa = new p();
  boolean A;
  boolean B;
  boolean C;
  boolean D;
  boolean E;
  boolean F = true;
  boolean G;
  int H;
  ViewGroup I;
  View J;
  View K;
  boolean L;
  boolean M = true;
  ap N;
  boolean O;
  boolean P;
  Object Q = null;
  Object R = a;
  Object S = null;
  Object T = a;
  Object U = null;
  Object V = a;
  Boolean W;
  Boolean X;
  ax Y = null;
  ax Z = null;
  int b = 0;
  View c;
  int d;
  Bundle e;
  SparseArray f;
  int g = -1;
  String h;
  Bundle i;
  Fragment j;
  int k = -1;
  int l;
  boolean m;
  boolean n;
  boolean o;
  boolean p;
  boolean q;
  boolean r;
  int s;
  v t;
  o u;
  v v;
  Fragment w;
  int x;
  int y;
  String z;
  
  public static Fragment a(Context paramContext, String paramString)
  {
    return a(paramContext, paramString, null);
  }
  
  public static Fragment a(Context paramContext, String paramString, Bundle paramBundle)
  {
    try
    {
      Class localClass = (Class)aa.get(paramString);
      if (localClass == null)
      {
        localClass = paramContext.getClassLoader().loadClass(paramString);
        aa.put(paramString, localClass);
      }
      Fragment localFragment = (Fragment)localClass.newInstance();
      if (paramBundle != null)
      {
        paramBundle.setClassLoader(localFragment.getClass().getClassLoader());
        localFragment.i = paramBundle;
      }
      return localFragment;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new m("Unable to instantiate fragment " + paramString + ": make sure class name exists, is public, and has an" + " empty constructor that is public", localClassNotFoundException);
    }
    catch (InstantiationException localInstantiationException)
    {
      throw new m("Unable to instantiate fragment " + paramString + ": make sure class name exists, is public, and has an" + " empty constructor that is public", localInstantiationException);
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      throw new m("Unable to instantiate fragment " + paramString + ": make sure class name exists, is public, and has an" + " empty constructor that is public", localIllegalAccessException);
    }
  }
  
  static boolean b(Context paramContext, String paramString)
  {
    try
    {
      Class localClass = (Class)aa.get(paramString);
      if (localClass == null)
      {
        localClass = paramContext.getClassLoader().loadClass(paramString);
        aa.put(paramString, localClass);
      }
      boolean bool = Fragment.class.isAssignableFrom(localClass);
      return bool;
    }
    catch (ClassNotFoundException localClassNotFoundException) {}
    return false;
  }
  
  void A()
  {
    this.v = new v();
    this.v.a(this.u, new l(this), this);
  }
  
  void B()
  {
    if (this.v != null)
    {
      this.v.i();
      this.v.e();
    }
    this.G = false;
    j();
    if (!this.G) {
      throw new ay("Fragment " + this + " did not call through to super.onStart()");
    }
    if (this.v != null) {
      this.v.l();
    }
    if (this.N != null) {
      this.N.g();
    }
  }
  
  void C()
  {
    if (this.v != null)
    {
      this.v.i();
      this.v.e();
    }
    this.G = false;
    k();
    if (!this.G) {
      throw new ay("Fragment " + this + " did not call through to super.onResume()");
    }
    if (this.v != null)
    {
      this.v.m();
      this.v.e();
    }
  }
  
  void D()
  {
    onLowMemory();
    if (this.v != null) {
      this.v.s();
    }
  }
  
  void E()
  {
    if (this.v != null) {
      this.v.n();
    }
    this.G = false;
    l();
    if (!this.G) {
      throw new ay("Fragment " + this + " did not call through to super.onPause()");
    }
  }
  
  void F()
  {
    if (this.v != null) {
      this.v.o();
    }
    this.G = false;
    m();
    if (!this.G) {
      throw new ay("Fragment " + this + " did not call through to super.onStop()");
    }
  }
  
  void G()
  {
    if (this.v != null) {
      this.v.p();
    }
    if (this.O)
    {
      this.O = false;
      if (!this.P)
      {
        this.P = true;
        this.N = this.u.a(this.h, this.O, false);
      }
      if (this.N != null)
      {
        if (this.u.h) {
          break label83;
        }
        this.N.c();
      }
    }
    return;
    label83:
    this.N.d();
  }
  
  void H()
  {
    if (this.v != null) {
      this.v.q();
    }
    this.G = false;
    n();
    if (!this.G) {
      throw new ay("Fragment " + this + " did not call through to super.onDestroyView()");
    }
    if (this.N != null) {
      this.N.f();
    }
  }
  
  void I()
  {
    if (this.v != null) {
      this.v.r();
    }
    this.G = false;
    o();
    if (!this.G) {
      throw new ay("Fragment " + this + " did not call through to super.onDestroy()");
    }
  }
  
  public View a(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return null;
  }
  
  public Animation a(int paramInt1, boolean paramBoolean, int paramInt2)
  {
    return null;
  }
  
  public void a(int paramInt1, int paramInt2, Intent paramIntent) {}
  
  final void a(int paramInt, Fragment paramFragment)
  {
    this.g = paramInt;
    if (paramFragment != null)
    {
      this.h = (paramFragment.h + ":" + this.g);
      return;
    }
    this.h = ("android:fragment:" + this.g);
  }
  
  public void a(Activity paramActivity)
  {
    this.G = true;
  }
  
  public void a(Activity paramActivity, AttributeSet paramAttributeSet, Bundle paramBundle)
  {
    this.G = true;
  }
  
  void a(Configuration paramConfiguration)
  {
    onConfigurationChanged(paramConfiguration);
    if (this.v != null) {
      this.v.a(paramConfiguration);
    }
  }
  
  final void a(Bundle paramBundle)
  {
    if (this.f != null)
    {
      this.K.restoreHierarchyState(this.f);
      this.f = null;
    }
    this.G = false;
    f(paramBundle);
    if (!this.G) {
      throw new ay("Fragment " + this + " did not call through to super.onViewStateRestored()");
    }
  }
  
  public void a(Menu paramMenu) {}
  
  public void a(Menu paramMenu, MenuInflater paramMenuInflater) {}
  
  public void a(View paramView, Bundle paramBundle) {}
  
  public void a(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
  {
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mFragmentId=#");
    paramPrintWriter.print(Integer.toHexString(this.x));
    paramPrintWriter.print(" mContainerId=#");
    paramPrintWriter.print(Integer.toHexString(this.y));
    paramPrintWriter.print(" mTag=");
    paramPrintWriter.println(this.z);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mState=");
    paramPrintWriter.print(this.b);
    paramPrintWriter.print(" mIndex=");
    paramPrintWriter.print(this.g);
    paramPrintWriter.print(" mWho=");
    paramPrintWriter.print(this.h);
    paramPrintWriter.print(" mBackStackNesting=");
    paramPrintWriter.println(this.s);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mAdded=");
    paramPrintWriter.print(this.m);
    paramPrintWriter.print(" mRemoving=");
    paramPrintWriter.print(this.n);
    paramPrintWriter.print(" mResumed=");
    paramPrintWriter.print(this.o);
    paramPrintWriter.print(" mFromLayout=");
    paramPrintWriter.print(this.p);
    paramPrintWriter.print(" mInLayout=");
    paramPrintWriter.println(this.q);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mHidden=");
    paramPrintWriter.print(this.A);
    paramPrintWriter.print(" mDetached=");
    paramPrintWriter.print(this.B);
    paramPrintWriter.print(" mMenuVisible=");
    paramPrintWriter.print(this.F);
    paramPrintWriter.print(" mHasMenu=");
    paramPrintWriter.println(this.E);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mRetainInstance=");
    paramPrintWriter.print(this.C);
    paramPrintWriter.print(" mRetaining=");
    paramPrintWriter.print(this.D);
    paramPrintWriter.print(" mUserVisibleHint=");
    paramPrintWriter.println(this.M);
    if (this.t != null)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mFragmentManager=");
      paramPrintWriter.println(this.t);
    }
    if (this.u != null)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mActivity=");
      paramPrintWriter.println(this.u);
    }
    if (this.w != null)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mParentFragment=");
      paramPrintWriter.println(this.w);
    }
    if (this.i != null)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mArguments=");
      paramPrintWriter.println(this.i);
    }
    if (this.e != null)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mSavedFragmentState=");
      paramPrintWriter.println(this.e);
    }
    if (this.f != null)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mSavedViewState=");
      paramPrintWriter.println(this.f);
    }
    if (this.j != null)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mTarget=");
      paramPrintWriter.print(this.j);
      paramPrintWriter.print(" mTargetRequestCode=");
      paramPrintWriter.println(this.l);
    }
    if (this.H != 0)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mNextAnim=");
      paramPrintWriter.println(this.H);
    }
    if (this.I != null)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mContainer=");
      paramPrintWriter.println(this.I);
    }
    if (this.J != null)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mView=");
      paramPrintWriter.println(this.J);
    }
    if (this.K != null)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mInnerView=");
      paramPrintWriter.println(this.J);
    }
    if (this.c != null)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mAnimatingAway=");
      paramPrintWriter.println(this.c);
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mStateAfterAnimating=");
      paramPrintWriter.println(this.d);
    }
    if (this.N != null)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.println("Loader Manager:");
      this.N.a(paramString + "  ", paramFileDescriptor, paramPrintWriter, paramArrayOfString);
    }
    if (this.v != null)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.println("Child " + this.v + ":");
      this.v.a(paramString + "  ", paramFileDescriptor, paramPrintWriter, paramArrayOfString);
    }
  }
  
  public void a(boolean paramBoolean) {}
  
  final boolean a()
  {
    return this.s > 0;
  }
  
  public boolean a(MenuItem paramMenuItem)
  {
    return false;
  }
  
  public final Bundle b()
  {
    return this.i;
  }
  
  View b(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    if (this.v != null) {
      this.v.i();
    }
    return a(paramLayoutInflater, paramViewGroup, paramBundle);
  }
  
  public void b(Bundle paramBundle)
  {
    if (this.g >= 0) {
      throw new IllegalStateException("Fragment already active");
    }
    this.i = paramBundle;
  }
  
  public void b(Menu paramMenu) {}
  
  public void b(boolean paramBoolean)
  {
    if ((paramBoolean) && (this.w != null)) {
      throw new IllegalStateException("Can't retain fragements that are nested in other fragments");
    }
    this.C = paramBoolean;
  }
  
  boolean b(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    boolean bool1 = this.A;
    boolean bool2 = false;
    if (!bool1)
    {
      boolean bool3 = this.E;
      bool2 = false;
      if (bool3)
      {
        boolean bool4 = this.F;
        bool2 = false;
        if (bool4)
        {
          bool2 = true;
          a(paramMenu, paramMenuInflater);
        }
      }
      if (this.v != null) {
        bool2 |= this.v.a(paramMenu, paramMenuInflater);
      }
    }
    return bool2;
  }
  
  public boolean b(MenuItem paramMenuItem)
  {
    return false;
  }
  
  public final o c()
  {
    return this.u;
  }
  
  public LayoutInflater c(Bundle paramBundle)
  {
    LayoutInflater localLayoutInflater = this.u.getLayoutInflater().cloneInContext(this.u);
    e();
    ac.a(localLayoutInflater, this.v.t());
    return localLayoutInflater;
  }
  
  public void c(boolean paramBoolean)
  {
    if (this.F != paramBoolean)
    {
      this.F = paramBoolean;
      if ((this.E) && (f()) && (!h())) {
        this.u.d();
      }
    }
  }
  
  boolean c(Menu paramMenu)
  {
    boolean bool1 = this.A;
    boolean bool2 = false;
    if (!bool1)
    {
      boolean bool3 = this.E;
      bool2 = false;
      if (bool3)
      {
        boolean bool4 = this.F;
        bool2 = false;
        if (bool4)
        {
          bool2 = true;
          a(paramMenu);
        }
      }
      if (this.v != null) {
        bool2 |= this.v.a(paramMenu);
      }
    }
    return bool2;
  }
  
  boolean c(MenuItem paramMenuItem)
  {
    if (!this.A)
    {
      if ((this.E) && (this.F) && (a(paramMenuItem))) {}
      while ((this.v != null) && (this.v.a(paramMenuItem))) {
        return true;
      }
    }
    return false;
  }
  
  public final Resources d()
  {
    if (this.u == null) {
      throw new IllegalStateException("Fragment " + this + " not attached to Activity");
    }
    return this.u.getResources();
  }
  
  public void d(Bundle paramBundle)
  {
    this.G = true;
  }
  
  void d(Menu paramMenu)
  {
    if (!this.A)
    {
      if ((this.E) && (this.F)) {
        b(paramMenu);
      }
      if (this.v != null) {
        this.v.b(paramMenu);
      }
    }
  }
  
  public void d(boolean paramBoolean)
  {
    if ((!this.M) && (paramBoolean) && (this.b < 4)) {
      this.t.a(this);
    }
    this.M = paramBoolean;
    if (!paramBoolean) {}
    for (boolean bool = true;; bool = false)
    {
      this.L = bool;
      return;
    }
  }
  
  boolean d(MenuItem paramMenuItem)
  {
    if (!this.A)
    {
      if (b(paramMenuItem)) {}
      while ((this.v != null) && (this.v.b(paramMenuItem))) {
        return true;
      }
    }
    return false;
  }
  
  public final t e()
  {
    if (this.v == null)
    {
      A();
      if (this.b < 5) {
        break label31;
      }
      this.v.m();
    }
    for (;;)
    {
      return this.v;
      label31:
      if (this.b >= 4) {
        this.v.l();
      } else if (this.b >= 2) {
        this.v.k();
      } else if (this.b >= 1) {
        this.v.j();
      }
    }
  }
  
  public void e(Bundle paramBundle)
  {
    this.G = true;
  }
  
  public final boolean equals(Object paramObject)
  {
    return super.equals(paramObject);
  }
  
  public void f(Bundle paramBundle)
  {
    this.G = true;
  }
  
  public final boolean f()
  {
    return (this.u != null) && (this.m);
  }
  
  public void g(Bundle paramBundle) {}
  
  public final boolean g()
  {
    return this.B;
  }
  
  void h(Bundle paramBundle)
  {
    if (this.v != null) {
      this.v.i();
    }
    this.G = false;
    d(paramBundle);
    if (!this.G) {
      throw new ay("Fragment " + this + " did not call through to super.onCreate()");
    }
    if (paramBundle != null)
    {
      Parcelable localParcelable = paramBundle.getParcelable("android:support:fragments");
      if (localParcelable != null)
      {
        if (this.v == null) {
          A();
        }
        this.v.a(localParcelable, null);
        this.v.j();
      }
    }
  }
  
  public final boolean h()
  {
    return this.A;
  }
  
  public final int hashCode()
  {
    return super.hashCode();
  }
  
  public View i()
  {
    return this.J;
  }
  
  void i(Bundle paramBundle)
  {
    if (this.v != null) {
      this.v.i();
    }
    this.G = false;
    e(paramBundle);
    if (!this.G) {
      throw new ay("Fragment " + this + " did not call through to super.onActivityCreated()");
    }
    if (this.v != null) {
      this.v.k();
    }
  }
  
  public void j()
  {
    this.G = true;
    if (!this.O)
    {
      this.O = true;
      if (!this.P)
      {
        this.P = true;
        this.N = this.u.a(this.h, this.O, false);
      }
      if (this.N != null) {
        this.N.b();
      }
    }
  }
  
  void j(Bundle paramBundle)
  {
    g(paramBundle);
    if (this.v != null)
    {
      Parcelable localParcelable = this.v.h();
      if (localParcelable != null) {
        paramBundle.putParcelable("android:support:fragments", localParcelable);
      }
    }
  }
  
  public void k()
  {
    this.G = true;
  }
  
  public void l()
  {
    this.G = true;
  }
  
  public void m()
  {
    this.G = true;
  }
  
  public void n()
  {
    this.G = true;
  }
  
  public void o()
  {
    this.G = true;
    if (!this.P)
    {
      this.P = true;
      this.N = this.u.a(this.h, this.O, false);
    }
    if (this.N != null) {
      this.N.h();
    }
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    this.G = true;
  }
  
  public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo)
  {
    c().onCreateContextMenu(paramContextMenu, paramView, paramContextMenuInfo);
  }
  
  public void onLowMemory()
  {
    this.G = true;
  }
  
  void p()
  {
    this.g = -1;
    this.h = null;
    this.m = false;
    this.n = false;
    this.o = false;
    this.p = false;
    this.q = false;
    this.r = false;
    this.s = 0;
    this.t = null;
    this.v = null;
    this.u = null;
    this.x = 0;
    this.y = 0;
    this.z = null;
    this.A = false;
    this.B = false;
    this.D = false;
    this.N = null;
    this.O = false;
    this.P = false;
  }
  
  public void q()
  {
    this.G = true;
  }
  
  public void r() {}
  
  public Object s()
  {
    return this.Q;
  }
  
  public Object t()
  {
    if (this.R == a) {
      return s();
    }
    return this.R;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(128);
    d.a(this, localStringBuilder);
    if (this.g >= 0)
    {
      localStringBuilder.append(" #");
      localStringBuilder.append(this.g);
    }
    if (this.x != 0)
    {
      localStringBuilder.append(" id=0x");
      localStringBuilder.append(Integer.toHexString(this.x));
    }
    if (this.z != null)
    {
      localStringBuilder.append(" ");
      localStringBuilder.append(this.z);
    }
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  public Object u()
  {
    return this.S;
  }
  
  public Object v()
  {
    if (this.T == a) {
      return u();
    }
    return this.T;
  }
  
  public Object w()
  {
    return this.U;
  }
  
  public Object x()
  {
    if (this.V == a) {
      return w();
    }
    return this.V;
  }
  
  public boolean y()
  {
    if (this.X == null) {
      return true;
    }
    return this.X.booleanValue();
  }
  
  public boolean z()
  {
    if (this.W == null) {
      return true;
    }
    return this.W.booleanValue();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/app/Fragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */