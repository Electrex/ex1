package android.support.v4.app;

import android.graphics.Rect;
import android.transition.Transition;
import android.transition.Transition.EpicenterCallback;

final class aj
  extends Transition.EpicenterCallback
{
  private Rect b;
  
  aj(al paramal) {}
  
  public Rect onGetEpicenter(Transition paramTransition)
  {
    if ((this.b == null) && (this.a.a != null)) {
      this.b = ag.b(this.a.a);
    }
    return this.b;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/app/aj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */