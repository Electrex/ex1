package android.support.v4.app;

import android.os.Build.VERSION;
import android.support.v4.f.a;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;

final class e
  extends af
  implements Runnable
{
  static final boolean a;
  final v b;
  i c;
  i d;
  int e;
  int f;
  int g;
  int h;
  int i;
  int j;
  int k;
  boolean l;
  boolean m = true;
  String n;
  boolean o;
  int p = -1;
  int q;
  CharSequence r;
  int s;
  CharSequence t;
  ArrayList u;
  ArrayList v;
  
  static
  {
    if (Build.VERSION.SDK_INT >= 21) {}
    for (boolean bool = true;; bool = false)
    {
      a = bool;
      return;
    }
  }
  
  public e(v paramv)
  {
    this.b = paramv;
  }
  
  private j a(SparseArray paramSparseArray1, SparseArray paramSparseArray2, boolean paramBoolean)
  {
    j localj = new j(this);
    localj.d = new View(this.b.o);
    int i1 = 0;
    int i2 = 0;
    int i3 = paramSparseArray1.size();
    int i4 = 0;
    if (i1 < i3) {
      if (!a(paramSparseArray1.keyAt(i1), localj, paramBoolean, paramSparseArray1, paramSparseArray2)) {
        break label142;
      }
    }
    label142:
    for (int i6 = 1;; i6 = i2)
    {
      i1++;
      i2 = i6;
      break;
      while (i4 < paramSparseArray2.size())
      {
        int i5 = paramSparseArray2.keyAt(i4);
        if ((paramSparseArray1.get(i5) == null) && (a(i5, localj, paramBoolean, paramSparseArray1, paramSparseArray2))) {
          i2 = 1;
        }
        i4++;
      }
      if (i2 == 0) {
        localj = null;
      }
      return localj;
    }
  }
  
  private a a(j paramj, Fragment paramFragment, boolean paramBoolean)
  {
    a locala = new a();
    if (this.u != null)
    {
      ag.a(locala, paramFragment.i());
      if (!paramBoolean) {
        break label74;
      }
      locala.a(this.v);
    }
    while (paramBoolean)
    {
      if (paramFragment.Y != null) {
        paramFragment.Y.a(this.v, locala);
      }
      a(paramj, locala, false);
      return locala;
      label74:
      locala = a(this.u, this.v, locala);
    }
    if (paramFragment.Z != null) {
      paramFragment.Z.a(this.v, locala);
    }
    b(paramj, locala, false);
    return locala;
  }
  
  private a a(j paramj, boolean paramBoolean, Fragment paramFragment)
  {
    a locala = b(paramj, paramFragment, paramBoolean);
    if (paramBoolean)
    {
      if (paramFragment.Z != null) {
        paramFragment.Z.a(this.v, locala);
      }
      a(paramj, locala, true);
      return locala;
    }
    if (paramFragment.Y != null) {
      paramFragment.Y.a(this.v, locala);
    }
    b(paramj, locala, true);
    return locala;
  }
  
  private static a a(ArrayList paramArrayList1, ArrayList paramArrayList2, a parama)
  {
    if (parama.isEmpty()) {
      return parama;
    }
    a locala = new a();
    int i1 = paramArrayList1.size();
    for (int i2 = 0; i2 < i1; i2++)
    {
      View localView = (View)parama.get(paramArrayList1.get(i2));
      if (localView != null) {
        locala.put(paramArrayList2.get(i2), localView);
      }
    }
    return locala;
  }
  
  private static Object a(Fragment paramFragment1, Fragment paramFragment2, boolean paramBoolean)
  {
    if ((paramFragment1 == null) || (paramFragment2 == null)) {
      return null;
    }
    if (paramBoolean) {}
    for (Object localObject = paramFragment2.x();; localObject = paramFragment1.w()) {
      return ag.a(localObject);
    }
  }
  
  private static Object a(Fragment paramFragment, boolean paramBoolean)
  {
    if (paramFragment == null) {
      return null;
    }
    if (paramBoolean) {}
    for (Object localObject = paramFragment.v();; localObject = paramFragment.s()) {
      return ag.a(localObject);
    }
  }
  
  private static Object a(Object paramObject, Fragment paramFragment, ArrayList paramArrayList, a parama, View paramView)
  {
    if (paramObject != null) {
      paramObject = ag.a(paramObject, paramFragment.i(), paramArrayList, parama, paramView);
    }
    return paramObject;
  }
  
  private void a(int paramInt1, Fragment paramFragment, String paramString, int paramInt2)
  {
    paramFragment.t = this.b;
    if (paramString != null)
    {
      if ((paramFragment.z != null) && (!paramString.equals(paramFragment.z))) {
        throw new IllegalStateException("Can't change tag of fragment " + paramFragment + ": was " + paramFragment.z + " now " + paramString);
      }
      paramFragment.z = paramString;
    }
    if (paramInt1 != 0)
    {
      if ((paramFragment.x != 0) && (paramFragment.x != paramInt1)) {
        throw new IllegalStateException("Can't change container ID of fragment " + paramFragment + ": was " + paramFragment.x + " now " + paramInt1);
      }
      paramFragment.x = paramInt1;
      paramFragment.y = paramInt1;
    }
    i locali = new i();
    locali.c = paramInt2;
    locali.d = paramFragment;
    a(locali);
  }
  
  private void a(j paramj, int paramInt, Object paramObject)
  {
    if (this.b.g != null)
    {
      int i1 = 0;
      if (i1 < this.b.g.size())
      {
        Fragment localFragment = (Fragment)this.b.g.get(i1);
        if ((localFragment.J != null) && (localFragment.I != null) && (localFragment.y == paramInt))
        {
          if (!localFragment.A) {
            break label122;
          }
          if (!paramj.b.contains(localFragment.J))
          {
            ag.a(paramObject, localFragment.J, true);
            paramj.b.add(localFragment.J);
          }
        }
        for (;;)
        {
          i1++;
          break;
          label122:
          ag.a(paramObject, localFragment.J, false);
          paramj.b.remove(localFragment.J);
        }
      }
    }
  }
  
  private void a(j paramj, Fragment paramFragment1, Fragment paramFragment2, boolean paramBoolean, a parama)
  {
    if (paramBoolean) {}
    for (ax localax = paramFragment2.Y;; localax = paramFragment1.Y)
    {
      if (localax != null) {
        localax.b(new ArrayList(parama.keySet()), new ArrayList(parama.values()), null);
      }
      return;
    }
  }
  
  private void a(j paramj, a parama, boolean paramBoolean)
  {
    int i1;
    int i2;
    label13:
    String str1;
    String str2;
    if (this.v == null)
    {
      i1 = 0;
      i2 = 0;
      if (i2 >= i1) {
        return;
      }
      str1 = (String)this.u.get(i2);
      View localView = (View)parama.get((String)this.v.get(i2));
      if (localView != null)
      {
        str2 = ag.a(localView);
        if (!paramBoolean) {
          break label100;
        }
        a(paramj.a, str1, str2);
      }
    }
    for (;;)
    {
      i2++;
      break label13;
      i1 = this.v.size();
      break;
      label100:
      a(paramj.a, str2, str1);
    }
  }
  
  private void a(j paramj, View paramView, Object paramObject, Fragment paramFragment1, Fragment paramFragment2, boolean paramBoolean, ArrayList paramArrayList)
  {
    paramView.getViewTreeObserver().addOnPreDrawListener(new g(this, paramView, paramObject, paramArrayList, paramj, paramBoolean, paramFragment1, paramFragment2));
  }
  
  private static void a(j paramj, ArrayList paramArrayList1, ArrayList paramArrayList2)
  {
    if (paramArrayList1 != null) {
      for (int i1 = 0; i1 < paramArrayList1.size(); i1++)
      {
        String str1 = (String)paramArrayList1.get(i1);
        String str2 = (String)paramArrayList2.get(i1);
        a(paramj.a, str1, str2);
      }
    }
  }
  
  private void a(a parama, j paramj)
  {
    if ((this.v != null) && (!parama.isEmpty()))
    {
      View localView = (View)parama.get(this.v.get(0));
      if (localView != null) {
        paramj.c.a = localView;
      }
    }
  }
  
  private static void a(a parama, String paramString1, String paramString2)
  {
    if ((paramString1 != null) && (paramString2 != null) && (!paramString1.equals(paramString2))) {}
    for (int i1 = 0; i1 < parama.size(); i1++) {
      if (paramString1.equals(parama.c(i1)))
      {
        parama.a(i1, paramString2);
        return;
      }
    }
    parama.put(paramString1, paramString2);
  }
  
  private static void a(SparseArray paramSparseArray, Fragment paramFragment)
  {
    if (paramFragment != null)
    {
      int i1 = paramFragment.y;
      if ((i1 != 0) && (!paramFragment.h()) && (paramFragment.f()) && (paramFragment.i() != null) && (paramSparseArray.get(i1) == null)) {
        paramSparseArray.put(i1, paramFragment);
      }
    }
  }
  
  private void a(View paramView, j paramj, int paramInt, Object paramObject)
  {
    paramView.getViewTreeObserver().addOnPreDrawListener(new h(this, paramView, paramj, paramInt, paramObject));
  }
  
  private boolean a(int paramInt, j paramj, boolean paramBoolean, SparseArray paramSparseArray1, SparseArray paramSparseArray2)
  {
    ViewGroup localViewGroup = (ViewGroup)this.b.p.a(paramInt);
    if (localViewGroup == null) {
      return false;
    }
    Fragment localFragment1 = (Fragment)paramSparseArray2.get(paramInt);
    Fragment localFragment2 = (Fragment)paramSparseArray1.get(paramInt);
    Object localObject1 = a(localFragment1, paramBoolean);
    Object localObject2 = a(localFragment1, localFragment2, paramBoolean);
    Object localObject3 = b(localFragment2, paramBoolean);
    if ((localObject1 == null) && (localObject2 == null) && (localObject3 == null)) {
      return false;
    }
    ArrayList localArrayList1 = new ArrayList();
    a locala1 = null;
    ax localax;
    if (localObject2 != null)
    {
      locala1 = a(paramj, localFragment2, paramBoolean);
      localArrayList1.add(paramj.d);
      localArrayList1.addAll(locala1.values());
      if (!paramBoolean) {
        break label449;
      }
      localax = localFragment2.Y;
      if (localax != null) {
        localax.a(new ArrayList(locala1.keySet()), new ArrayList(locala1.values()), null);
      }
    }
    ArrayList localArrayList2 = new ArrayList();
    Object localObject4 = a(localObject3, localFragment2, localArrayList2, locala1, paramj.d);
    if ((this.v != null) && (locala1 != null))
    {
      View localView = (View)locala1.get(this.v.get(0));
      if (localView != null)
      {
        if (localObject4 != null) {
          ag.a(localObject4, localView);
        }
        if (localObject2 != null) {
          ag.a(localObject2, localView);
        }
      }
    }
    f localf = new f(this, localFragment1);
    if (localObject2 != null) {
      a(paramj, localViewGroup, localObject2, localFragment1, localFragment2, paramBoolean, localArrayList1);
    }
    ArrayList localArrayList3 = new ArrayList();
    a locala2 = new a();
    if (paramBoolean) {}
    for (boolean bool = localFragment1.z();; bool = localFragment1.y())
    {
      Object localObject5 = ag.a(localObject1, localObject4, localObject2, bool);
      if (localObject5 != null)
      {
        ag.a(localObject1, localObject2, localViewGroup, localf, paramj.d, paramj.c, paramj.a, localArrayList3, locala2, localArrayList1);
        a(localViewGroup, paramj, paramInt, localObject5);
        ag.a(localObject5, paramj.d, true);
        a(paramj, paramInt, localObject5);
        ag.a(localViewGroup, localObject5);
        ag.a(localViewGroup, paramj.d, localObject1, localArrayList3, localObject4, localArrayList2, localObject2, localArrayList1, localObject5, paramj.b, locala2);
      }
      if (localObject5 == null) {
        break label469;
      }
      return true;
      label449:
      localax = localFragment1.Y;
      break;
    }
    label469:
    return false;
  }
  
  private a b(j paramj, Fragment paramFragment, boolean paramBoolean)
  {
    a locala = new a();
    View localView = paramFragment.i();
    if ((localView != null) && (this.u != null))
    {
      ag.a(locala, localView);
      if (paramBoolean) {
        locala = a(this.u, this.v, locala);
      }
    }
    else
    {
      return locala;
    }
    locala.a(this.v);
    return locala;
  }
  
  private static Object b(Fragment paramFragment, boolean paramBoolean)
  {
    if (paramFragment == null) {
      return null;
    }
    if (paramBoolean) {}
    for (Object localObject = paramFragment.t();; localObject = paramFragment.u()) {
      return ag.a(localObject);
    }
  }
  
  private void b(j paramj, a parama, boolean paramBoolean)
  {
    int i1 = parama.size();
    int i2 = 0;
    if (i2 < i1)
    {
      String str1 = (String)parama.b(i2);
      String str2 = ag.a((View)parama.c(i2));
      if (paramBoolean) {
        a(paramj.a, str1, str2);
      }
      for (;;)
      {
        i2++;
        break;
        a(paramj.a, str2, str1);
      }
    }
  }
  
  private void b(SparseArray paramSparseArray, Fragment paramFragment)
  {
    if (paramFragment != null)
    {
      int i1 = paramFragment.y;
      if (i1 != 0) {
        paramSparseArray.put(i1, paramFragment);
      }
    }
  }
  
  private void b(SparseArray paramSparseArray1, SparseArray paramSparseArray2)
  {
    if (!this.b.p.a()) {}
    i locali;
    do
    {
      return;
      locali = this.c;
    } while (locali == null);
    switch (locali.c)
    {
    }
    for (;;)
    {
      locali = locali.a;
      break;
      b(paramSparseArray2, locali.d);
      continue;
      Fragment localFragment1 = locali.d;
      Fragment localFragment2;
      if (this.b.g != null)
      {
        localFragment2 = localFragment1;
        int i1 = 0;
        if (i1 < this.b.g.size())
        {
          Fragment localFragment3 = (Fragment)this.b.g.get(i1);
          if ((localFragment2 == null) || (localFragment3.y == localFragment2.y))
          {
            if (localFragment3 != localFragment2) {
              break label181;
            }
            localFragment2 = null;
          }
          for (;;)
          {
            i1++;
            break;
            label181:
            a(paramSparseArray1, localFragment3);
          }
        }
      }
      else
      {
        localFragment2 = localFragment1;
      }
      b(paramSparseArray2, localFragment2);
      continue;
      a(paramSparseArray1, locali.d);
      continue;
      a(paramSparseArray1, locali.d);
      continue;
      b(paramSparseArray2, locali.d);
      continue;
      a(paramSparseArray1, locali.d);
      continue;
      b(paramSparseArray2, locali.d);
    }
  }
  
  public int a()
  {
    return a(false);
  }
  
  int a(boolean paramBoolean)
  {
    if (this.o) {
      throw new IllegalStateException("commit already called");
    }
    if (v.a)
    {
      Log.v("FragmentManager", "Commit: " + this);
      a("  ", null, new PrintWriter(new android.support.v4.f.e("FragmentManager")), null);
    }
    this.o = true;
    if (this.l) {}
    for (this.p = this.b.a(this);; this.p = -1)
    {
      this.b.a(this, paramBoolean);
      return this.p;
    }
  }
  
  public af a(int paramInt, Fragment paramFragment, String paramString)
  {
    a(paramInt, paramFragment, paramString, 1);
    return this;
  }
  
  public af a(Fragment paramFragment)
  {
    i locali = new i();
    locali.c = 6;
    locali.d = paramFragment;
    a(locali);
    return this;
  }
  
  public j a(boolean paramBoolean, j paramj, SparseArray paramSparseArray1, SparseArray paramSparseArray2)
  {
    if (v.a)
    {
      Log.v("FragmentManager", "popFromBackStack: " + this);
      a("  ", null, new PrintWriter(new android.support.v4.f.e("FragmentManager")), null);
    }
    if (a)
    {
      if (paramj != null) {
        break label216;
      }
      if ((paramSparseArray1.size() != 0) || (paramSparseArray2.size() != 0)) {
        paramj = a(paramSparseArray1, paramSparseArray2, true);
      }
    }
    label93:
    a(-1);
    int i1;
    label105:
    int i2;
    label112:
    i locali;
    int i3;
    if (paramj != null)
    {
      i1 = 0;
      if (paramj == null) {
        break label244;
      }
      i2 = 0;
      locali = this.d;
      if (locali == null) {
        break label569;
      }
      if (paramj == null) {
        break label253;
      }
      i3 = 0;
      label130:
      if (paramj == null) {
        break label263;
      }
    }
    label216:
    label244:
    label253:
    label263:
    for (int i4 = 0;; i4 = locali.h) {
      switch (locali.c)
      {
      default: 
        throw new IllegalArgumentException("Unknown cmd: " + locali.c);
        if (paramBoolean) {
          break label93;
        }
        a(paramj, this.v, this.u);
        break label93;
        i1 = this.k;
        break label105;
        i2 = this.j;
        break label112;
        i3 = locali.g;
        break label130;
      }
    }
    Fragment localFragment8 = locali.d;
    localFragment8.H = i4;
    this.b.a(localFragment8, v.c(i2), i1);
    for (;;)
    {
      locali = locali.b;
      break;
      Fragment localFragment6 = locali.d;
      if (localFragment6 != null)
      {
        localFragment6.H = i4;
        this.b.a(localFragment6, v.c(i2), i1);
      }
      if (locali.i != null)
      {
        for (int i5 = 0; i5 < locali.i.size(); i5++)
        {
          Fragment localFragment7 = (Fragment)locali.i.get(i5);
          localFragment7.H = i3;
          this.b.a(localFragment7, false);
        }
        Fragment localFragment5 = locali.d;
        localFragment5.H = i3;
        this.b.a(localFragment5, false);
        continue;
        Fragment localFragment4 = locali.d;
        localFragment4.H = i3;
        this.b.c(localFragment4, v.c(i2), i1);
        continue;
        Fragment localFragment3 = locali.d;
        localFragment3.H = i4;
        this.b.b(localFragment3, v.c(i2), i1);
        continue;
        Fragment localFragment2 = locali.d;
        localFragment2.H = i3;
        this.b.e(localFragment2, v.c(i2), i1);
        continue;
        Fragment localFragment1 = locali.d;
        localFragment1.H = i3;
        this.b.d(localFragment1, v.c(i2), i1);
      }
    }
    label569:
    if (paramBoolean)
    {
      this.b.a(this.b.n, v.c(i2), i1, true);
      paramj = null;
    }
    if (this.p >= 0)
    {
      this.b.b(this.p);
      this.p = -1;
    }
    return paramj;
  }
  
  void a(int paramInt)
  {
    if (!this.l) {}
    for (;;)
    {
      return;
      if (v.a) {
        Log.v("FragmentManager", "Bump nesting in " + this + " by " + paramInt);
      }
      for (i locali = this.c; locali != null; locali = locali.a)
      {
        if (locali.d != null)
        {
          Fragment localFragment2 = locali.d;
          localFragment2.s = (paramInt + localFragment2.s);
          if (v.a) {
            Log.v("FragmentManager", "Bump nesting of " + locali.d + " to " + locali.d.s);
          }
        }
        if (locali.i != null) {
          for (int i1 = -1 + locali.i.size(); i1 >= 0; i1--)
          {
            Fragment localFragment1 = (Fragment)locali.i.get(i1);
            localFragment1.s = (paramInt + localFragment1.s);
            if (v.a) {
              Log.v("FragmentManager", "Bump nesting of " + localFragment1 + " to " + localFragment1.s);
            }
          }
        }
      }
    }
  }
  
  void a(i parami)
  {
    if (this.c == null)
    {
      this.d = parami;
      this.c = parami;
    }
    for (;;)
    {
      parami.e = this.f;
      parami.f = this.g;
      parami.g = this.h;
      parami.h = this.i;
      this.e = (1 + this.e);
      return;
      parami.b = this.d;
      this.d.a = parami;
      this.d = parami;
    }
  }
  
  public void a(SparseArray paramSparseArray1, SparseArray paramSparseArray2)
  {
    if (!this.b.p.a()) {}
    i locali;
    do
    {
      return;
      locali = this.c;
    } while (locali == null);
    switch (locali.c)
    {
    }
    for (;;)
    {
      locali = locali.a;
      break;
      a(paramSparseArray1, locali.d);
      continue;
      if (locali.i != null) {
        for (int i1 = -1 + locali.i.size(); i1 >= 0; i1--) {
          b(paramSparseArray2, (Fragment)locali.i.get(i1));
        }
      }
      a(paramSparseArray1, locali.d);
      continue;
      b(paramSparseArray2, locali.d);
      continue;
      b(paramSparseArray2, locali.d);
      continue;
      a(paramSparseArray1, locali.d);
      continue;
      b(paramSparseArray2, locali.d);
      continue;
      a(paramSparseArray1, locali.d);
    }
  }
  
  public void a(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString)
  {
    a(paramString, paramPrintWriter, true);
  }
  
  public void a(String paramString, PrintWriter paramPrintWriter, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mName=");
      paramPrintWriter.print(this.n);
      paramPrintWriter.print(" mIndex=");
      paramPrintWriter.print(this.p);
      paramPrintWriter.print(" mCommitted=");
      paramPrintWriter.println(this.o);
      if (this.j != 0)
      {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mTransition=#");
        paramPrintWriter.print(Integer.toHexString(this.j));
        paramPrintWriter.print(" mTransitionStyle=#");
        paramPrintWriter.println(Integer.toHexString(this.k));
      }
      if ((this.f != 0) || (this.g != 0))
      {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mEnterAnim=#");
        paramPrintWriter.print(Integer.toHexString(this.f));
        paramPrintWriter.print(" mExitAnim=#");
        paramPrintWriter.println(Integer.toHexString(this.g));
      }
      if ((this.h != 0) || (this.i != 0))
      {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mPopEnterAnim=#");
        paramPrintWriter.print(Integer.toHexString(this.h));
        paramPrintWriter.print(" mPopExitAnim=#");
        paramPrintWriter.println(Integer.toHexString(this.i));
      }
      if ((this.q != 0) || (this.r != null))
      {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mBreadCrumbTitleRes=#");
        paramPrintWriter.print(Integer.toHexString(this.q));
        paramPrintWriter.print(" mBreadCrumbTitleText=");
        paramPrintWriter.println(this.r);
      }
      if ((this.s != 0) || (this.t != null))
      {
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("mBreadCrumbShortTitleRes=#");
        paramPrintWriter.print(Integer.toHexString(this.s));
        paramPrintWriter.print(" mBreadCrumbShortTitleText=");
        paramPrintWriter.println(this.t);
      }
    }
    if (this.c != null)
    {
      paramPrintWriter.print(paramString);
      paramPrintWriter.println("Operations:");
      String str1 = paramString + "    ";
      i locali1 = this.c;
      int i1 = 0;
      i locali2 = locali1;
      while (locali2 != null)
      {
        String str2;
        int i2;
        switch (locali2.c)
        {
        default: 
          str2 = "cmd=" + locali2.c;
          paramPrintWriter.print(paramString);
          paramPrintWriter.print("  Op #");
          paramPrintWriter.print(i1);
          paramPrintWriter.print(": ");
          paramPrintWriter.print(str2);
          paramPrintWriter.print(" ");
          paramPrintWriter.println(locali2.d);
          if (paramBoolean)
          {
            if ((locali2.e != 0) || (locali2.f != 0))
            {
              paramPrintWriter.print(paramString);
              paramPrintWriter.print("enterAnim=#");
              paramPrintWriter.print(Integer.toHexString(locali2.e));
              paramPrintWriter.print(" exitAnim=#");
              paramPrintWriter.println(Integer.toHexString(locali2.f));
            }
            if ((locali2.g != 0) || (locali2.h != 0))
            {
              paramPrintWriter.print(paramString);
              paramPrintWriter.print("popEnterAnim=#");
              paramPrintWriter.print(Integer.toHexString(locali2.g));
              paramPrintWriter.print(" popExitAnim=#");
              paramPrintWriter.println(Integer.toHexString(locali2.h));
            }
          }
          if ((locali2.i == null) || (locali2.i.size() <= 0)) {
            break label808;
          }
          i2 = 0;
          label645:
          if (i2 >= locali2.i.size()) {
            break label808;
          }
          paramPrintWriter.print(str1);
          if (locali2.i.size() == 1) {
            paramPrintWriter.print("Removed: ");
          }
          break;
        }
        for (;;)
        {
          paramPrintWriter.println(locali2.i.get(i2));
          i2++;
          break label645;
          str2 = "NULL";
          break;
          str2 = "ADD";
          break;
          str2 = "REPLACE";
          break;
          str2 = "REMOVE";
          break;
          str2 = "HIDE";
          break;
          str2 = "SHOW";
          break;
          str2 = "DETACH";
          break;
          str2 = "ATTACH";
          break;
          if (i2 == 0) {
            paramPrintWriter.println("Removed:");
          }
          paramPrintWriter.print(str1);
          paramPrintWriter.print("  #");
          paramPrintWriter.print(i2);
          paramPrintWriter.print(": ");
        }
        label808:
        locali2 = locali2.a;
        i1++;
      }
    }
  }
  
  public int b()
  {
    return a(true);
  }
  
  public af b(Fragment paramFragment)
  {
    i locali = new i();
    locali.c = 7;
    locali.d = paramFragment;
    a(locali);
    return this;
  }
  
  public String c()
  {
    return this.n;
  }
  
  public void run()
  {
    if (v.a) {
      Log.v("FragmentManager", "Run: " + this);
    }
    if ((this.l) && (this.p < 0)) {
      throw new IllegalStateException("addToBackStack() called after commit()");
    }
    a(1);
    SparseArray localSparseArray1;
    SparseArray localSparseArray2;
    if (a)
    {
      localSparseArray1 = new SparseArray();
      localSparseArray2 = new SparseArray();
      b(localSparseArray1, localSparseArray2);
    }
    for (j localj = a(localSparseArray1, localSparseArray2, false);; localj = null)
    {
      int i1;
      label106:
      int i2;
      label113:
      i locali;
      int i3;
      if (localj != null)
      {
        i1 = 0;
        if (localj == null) {
          break label225;
        }
        i2 = 0;
        locali = this.c;
        if (locali == null) {
          break label729;
        }
        if (localj == null) {
          break label234;
        }
        i3 = 0;
        label131:
        if (localj == null) {
          break label244;
        }
      }
      label225:
      label234:
      label244:
      for (int i4 = 0;; i4 = locali.f) {
        switch (locali.c)
        {
        default: 
          throw new IllegalArgumentException("Unknown cmd: " + locali.c);
          i1 = this.k;
          break label106;
          i2 = this.j;
          break label113;
          i3 = locali.e;
          break label131;
        }
      }
      Fragment localFragment9 = locali.d;
      localFragment9.H = i3;
      this.b.a(localFragment9, false);
      for (;;)
      {
        locali = locali.a;
        break;
        Fragment localFragment6 = locali.d;
        Fragment localFragment7;
        if (this.b.g != null)
        {
          int i5 = 0;
          localFragment7 = localFragment6;
          if (i5 < this.b.g.size())
          {
            Fragment localFragment8 = (Fragment)this.b.g.get(i5);
            if (v.a) {
              Log.v("FragmentManager", "OP_REPLACE: adding=" + localFragment7 + " old=" + localFragment8);
            }
            if ((localFragment7 == null) || (localFragment8.y == localFragment7.y))
            {
              if (localFragment8 != localFragment7) {
                break label429;
              }
              locali.d = null;
              localFragment7 = null;
            }
            for (;;)
            {
              i5++;
              break;
              label429:
              if (locali.i == null) {
                locali.i = new ArrayList();
              }
              locali.i.add(localFragment8);
              localFragment8.H = i4;
              if (this.l)
              {
                localFragment8.s = (1 + localFragment8.s);
                if (v.a) {
                  Log.v("FragmentManager", "Bump nesting of " + localFragment8 + " to " + localFragment8.s);
                }
              }
              this.b.a(localFragment8, i2, i1);
            }
          }
        }
        else
        {
          localFragment7 = localFragment6;
        }
        if (localFragment7 != null)
        {
          localFragment7.H = i3;
          this.b.a(localFragment7, false);
          continue;
          Fragment localFragment5 = locali.d;
          localFragment5.H = i4;
          this.b.a(localFragment5, i2, i1);
          continue;
          Fragment localFragment4 = locali.d;
          localFragment4.H = i4;
          this.b.b(localFragment4, i2, i1);
          continue;
          Fragment localFragment3 = locali.d;
          localFragment3.H = i3;
          this.b.c(localFragment3, i2, i1);
          continue;
          Fragment localFragment2 = locali.d;
          localFragment2.H = i4;
          this.b.d(localFragment2, i2, i1);
          continue;
          Fragment localFragment1 = locali.d;
          localFragment1.H = i3;
          this.b.e(localFragment1, i2, i1);
        }
      }
      label729:
      this.b.a(this.b.n, i2, i1, true);
      if (this.l) {
        this.b.b(this);
      }
      return;
    }
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder(128);
    localStringBuilder.append("BackStackEntry{");
    localStringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
    if (this.p >= 0)
    {
      localStringBuilder.append(" #");
      localStringBuilder.append(this.p);
    }
    if (this.n != null)
    {
      localStringBuilder.append(" ");
      localStringBuilder.append(this.n);
    }
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/app/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */