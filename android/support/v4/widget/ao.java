package android.support.v4.widget;

import android.os.Bundle;
import android.support.v4.view.a.aj;
import android.support.v4.view.a.g;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.widget.ScrollView;

class ao
  extends android.support.v4.view.a
{
  public void a(View paramView, g paramg)
  {
    super.a(paramView, paramg);
    NestedScrollView localNestedScrollView = (NestedScrollView)paramView;
    paramg.b(ScrollView.class.getName());
    if (localNestedScrollView.isEnabled())
    {
      int i = NestedScrollView.a(localNestedScrollView);
      if (i > 0)
      {
        paramg.i(true);
        if (localNestedScrollView.getScrollY() > 0) {
          paramg.a(8192);
        }
        if (localNestedScrollView.getScrollY() < i) {
          paramg.a(4096);
        }
      }
    }
  }
  
  public boolean a(View paramView, int paramInt, Bundle paramBundle)
  {
    if (super.a(paramView, paramInt, paramBundle)) {
      return true;
    }
    NestedScrollView localNestedScrollView = (NestedScrollView)paramView;
    if (!localNestedScrollView.isEnabled()) {
      return false;
    }
    switch (paramInt)
    {
    default: 
      return false;
    case 4096: 
      int k = Math.min(localNestedScrollView.getHeight() - localNestedScrollView.getPaddingBottom() - localNestedScrollView.getPaddingTop() + localNestedScrollView.getScrollY(), NestedScrollView.a(localNestedScrollView));
      if (k != localNestedScrollView.getScrollY())
      {
        localNestedScrollView.b(0, k);
        return true;
      }
      return false;
    }
    int i = localNestedScrollView.getHeight() - localNestedScrollView.getPaddingBottom() - localNestedScrollView.getPaddingTop();
    int j = Math.max(localNestedScrollView.getScrollY() - i, 0);
    if (j != localNestedScrollView.getScrollY())
    {
      localNestedScrollView.b(0, j);
      return true;
    }
    return false;
  }
  
  public void d(View paramView, AccessibilityEvent paramAccessibilityEvent)
  {
    super.d(paramView, paramAccessibilityEvent);
    NestedScrollView localNestedScrollView = (NestedScrollView)paramView;
    paramAccessibilityEvent.setClassName(ScrollView.class.getName());
    aj localaj = android.support.v4.view.a.a.a(paramAccessibilityEvent);
    if (NestedScrollView.a(localNestedScrollView) > 0) {}
    for (boolean bool = true;; bool = false)
    {
      localaj.a(bool);
      localaj.d(localNestedScrollView.getScrollX());
      localaj.e(localNestedScrollView.getScrollY());
      localaj.f(localNestedScrollView.getScrollX());
      localaj.g(NestedScrollView.a(localNestedScrollView));
      return;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/widget/ao.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */