package android.support.v4.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.a.a;
import android.support.v4.view.ba;
import android.support.v4.view.cy;
import android.support.v4.view.v;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;

public class DrawerLayout
  extends ViewGroup
  implements y
{
  static final o a = new q();
  private static final int[] b;
  private static final boolean c;
  private CharSequence A;
  private Object B;
  private boolean C;
  private final n d = new n(this);
  private int e;
  private int f = -1728053248;
  private float g;
  private Paint h = new Paint();
  private final bv i;
  private final bv j;
  private final u k;
  private final u l;
  private int m;
  private boolean n;
  private boolean o = true;
  private int p;
  private int q;
  private boolean r;
  private boolean s;
  private r t;
  private float u;
  private float v;
  private Drawable w;
  private Drawable x;
  private Drawable y;
  private CharSequence z;
  
  static
  {
    boolean bool = true;
    int[] arrayOfInt = new int[bool];
    arrayOfInt[0] = 16842931;
    b = arrayOfInt;
    if (Build.VERSION.SDK_INT >= 19) {}
    for (;;)
    {
      c = bool;
      if (Build.VERSION.SDK_INT < 21) {
        break;
      }
      a = new p();
      return;
      bool = false;
    }
  }
  
  public DrawerLayout(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public DrawerLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public DrawerLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    setDescendantFocusability(262144);
    float f1 = getResources().getDisplayMetrics().density;
    this.e = ((int)(0.5F + 64.0F * f1));
    float f2 = f1 * 400.0F;
    this.k = new u(this, 3);
    this.l = new u(this, 5);
    this.i = bv.a(this, 1.0F, this.k);
    this.i.a(1);
    this.i.a(f2);
    this.k.a(this.i);
    this.j = bv.a(this, 1.0F, this.l);
    this.j.a(2);
    this.j.a(f2);
    this.l.a(this.j);
    setFocusableInTouchMode(true);
    android.support.v4.view.bv.c(this, 1);
    android.support.v4.view.bv.a(this, new m(this));
    cy.a(this, false);
    if (android.support.v4.view.bv.t(this))
    {
      a.a(this);
      this.y = a.a(paramContext);
    }
  }
  
  private void a(View paramView, boolean paramBoolean)
  {
    int i1 = getChildCount();
    int i2 = 0;
    if (i2 < i1)
    {
      View localView = getChildAt(i2);
      if (((!paramBoolean) && (!g(localView))) || ((paramBoolean) && (localView == paramView))) {
        android.support.v4.view.bv.c(localView, 1);
      }
      for (;;)
      {
        i2++;
        break;
        android.support.v4.view.bv.c(localView, 4);
      }
    }
  }
  
  static String c(int paramInt)
  {
    if ((paramInt & 0x3) == 3) {
      return "LEFT";
    }
    if ((paramInt & 0x5) == 5) {
      return "RIGHT";
    }
    return Integer.toHexString(paramInt);
  }
  
  private boolean f()
  {
    int i1 = getChildCount();
    for (int i2 = 0; i2 < i1; i2++) {
      if (((s)getChildAt(i2).getLayoutParams()).c) {
        return true;
      }
    }
    return false;
  }
  
  private boolean g()
  {
    return h() != null;
  }
  
  private View h()
  {
    int i1 = getChildCount();
    for (int i2 = 0; i2 < i1; i2++)
    {
      View localView = getChildAt(i2);
      if ((g(localView)) && (k(localView))) {
        return localView;
      }
    }
    return null;
  }
  
  private static boolean m(View paramView)
  {
    Drawable localDrawable = paramView.getBackground();
    boolean bool = false;
    if (localDrawable != null)
    {
      int i1 = localDrawable.getOpacity();
      bool = false;
      if (i1 == -1) {
        bool = true;
      }
    }
    return bool;
  }
  
  private static boolean n(View paramView)
  {
    return (android.support.v4.view.bv.e(paramView) != 4) && (android.support.v4.view.bv.e(paramView) != 2);
  }
  
  public int a(View paramView)
  {
    int i1 = e(paramView);
    if (i1 == 3) {
      return this.p;
    }
    if (i1 == 5) {
      return this.q;
    }
    return 0;
  }
  
  View a()
  {
    int i1 = getChildCount();
    for (int i2 = 0; i2 < i1; i2++)
    {
      View localView = getChildAt(i2);
      if (((s)localView.getLayoutParams()).d) {
        return localView;
      }
    }
    return null;
  }
  
  public CharSequence a(int paramInt)
  {
    int i1 = android.support.v4.view.q.a(paramInt, android.support.v4.view.bv.h(this));
    if (i1 == 3) {
      return this.z;
    }
    if (i1 == 5) {
      return this.A;
    }
    return null;
  }
  
  public void a(int paramInt1, int paramInt2)
  {
    a(getResources().getDrawable(paramInt1), paramInt2);
  }
  
  void a(int paramInt1, int paramInt2, View paramView)
  {
    int i1 = 1;
    int i2 = this.i.a();
    int i3 = this.j.a();
    s locals;
    if ((i2 == i1) || (i3 == i1)) {
      if ((paramView != null) && (paramInt2 == 0))
      {
        locals = (s)paramView.getLayoutParams();
        if (locals.b != 0.0F) {
          break label125;
        }
        b(paramView);
      }
    }
    for (;;)
    {
      if (i1 != this.m)
      {
        this.m = i1;
        if (this.t != null) {
          this.t.a(i1);
        }
      }
      return;
      if ((i2 == 2) || (i3 == 2))
      {
        i1 = 2;
        break;
      }
      i1 = 0;
      break;
      label125:
      if (locals.b == 1.0F) {
        c(paramView);
      }
    }
  }
  
  public void a(Drawable paramDrawable, int paramInt)
  {
    int i1 = android.support.v4.view.q.a(paramInt, android.support.v4.view.bv.h(this));
    if ((i1 & 0x3) == 3)
    {
      this.w = paramDrawable;
      invalidate();
    }
    if ((i1 & 0x5) == 5)
    {
      this.x = paramDrawable;
      invalidate();
    }
  }
  
  void a(View paramView, float paramFloat)
  {
    if (this.t != null) {
      this.t.a(paramView, paramFloat);
    }
  }
  
  public void a(Object paramObject, boolean paramBoolean)
  {
    this.B = paramObject;
    this.C = paramBoolean;
    if ((!paramBoolean) && (getBackground() == null)) {}
    for (boolean bool = true;; bool = false)
    {
      setWillNotDraw(bool);
      requestLayout();
      return;
    }
  }
  
  void a(boolean paramBoolean)
  {
    int i1 = getChildCount();
    int i2 = 0;
    boolean bool = false;
    while (i2 < i1)
    {
      View localView = getChildAt(i2);
      s locals = (s)localView.getLayoutParams();
      if ((!g(localView)) || ((paramBoolean) && (!locals.c)))
      {
        i2++;
      }
      else
      {
        int i3 = localView.getWidth();
        if (a(localView, 3)) {
          bool |= this.i.a(localView, -i3, localView.getTop());
        }
        for (;;)
        {
          locals.c = false;
          break;
          bool |= this.j.a(localView, getWidth(), localView.getTop());
        }
      }
    }
    this.k.a();
    this.l.a();
    if (bool) {
      invalidate();
    }
  }
  
  boolean a(View paramView, int paramInt)
  {
    return (paramInt & e(paramView)) == paramInt;
  }
  
  public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams)
  {
    super.addView(paramView, paramInt, paramLayoutParams);
    if ((a() != null) || (g(paramView))) {
      android.support.v4.view.bv.c(paramView, 4);
    }
    for (;;)
    {
      if (!c) {
        android.support.v4.view.bv.a(paramView, this.d);
      }
      return;
      android.support.v4.view.bv.c(paramView, 1);
    }
  }
  
  View b(int paramInt)
  {
    int i1 = 0x7 & android.support.v4.view.q.a(paramInt, android.support.v4.view.bv.h(this));
    int i2 = getChildCount();
    for (int i3 = 0; i3 < i2; i3++)
    {
      View localView = getChildAt(i3);
      if ((0x7 & e(localView)) == i1) {
        return localView;
      }
    }
    return null;
  }
  
  public void b()
  {
    a(false);
  }
  
  public void b(int paramInt1, int paramInt2)
  {
    int i1 = android.support.v4.view.q.a(paramInt2, android.support.v4.view.bv.h(this));
    bv localbv;
    if (i1 == 3)
    {
      this.p = paramInt1;
      if (paramInt1 != 0)
      {
        if (i1 != 3) {
          break label78;
        }
        localbv = this.i;
        label34:
        localbv.e();
      }
      switch (paramInt1)
      {
      }
    }
    label78:
    View localView1;
    do
    {
      View localView2;
      do
      {
        return;
        if (i1 != 5) {
          break;
        }
        this.q = paramInt1;
        break;
        localbv = this.j;
        break label34;
        localView2 = b(i1);
      } while (localView2 == null);
      h(localView2);
      return;
      localView1 = b(i1);
    } while (localView1 == null);
    i(localView1);
  }
  
  void b(View paramView)
  {
    s locals = (s)paramView.getLayoutParams();
    if (locals.d)
    {
      locals.d = false;
      if (this.t != null) {
        this.t.b(paramView);
      }
      a(paramView, false);
      if (hasWindowFocus())
      {
        View localView = getRootView();
        if (localView != null) {
          localView.sendAccessibilityEvent(32);
        }
      }
    }
  }
  
  void b(View paramView, float paramFloat)
  {
    s locals = (s)paramView.getLayoutParams();
    if (paramFloat == locals.b) {
      return;
    }
    locals.b = paramFloat;
    a(paramView, paramFloat);
  }
  
  void c()
  {
    int i1 = 0;
    if (!this.s)
    {
      long l1 = SystemClock.uptimeMillis();
      MotionEvent localMotionEvent = MotionEvent.obtain(l1, l1, 3, 0.0F, 0.0F, 0);
      int i2 = getChildCount();
      while (i1 < i2)
      {
        getChildAt(i1).dispatchTouchEvent(localMotionEvent);
        i1++;
      }
      localMotionEvent.recycle();
      this.s = true;
    }
  }
  
  void c(View paramView)
  {
    s locals = (s)paramView.getLayoutParams();
    if (!locals.d)
    {
      locals.d = true;
      if (this.t != null) {
        this.t.a(paramView);
      }
      a(paramView, true);
      if (hasWindowFocus()) {
        sendAccessibilityEvent(32);
      }
      paramView.requestFocus();
    }
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return ((paramLayoutParams instanceof s)) && (super.checkLayoutParams(paramLayoutParams));
  }
  
  public void computeScroll()
  {
    int i1 = getChildCount();
    float f1 = 0.0F;
    for (int i2 = 0; i2 < i1; i2++) {
      f1 = Math.max(f1, ((s)getChildAt(i2).getLayoutParams()).b);
    }
    this.g = f1;
    if ((this.i.a(true) | this.j.a(true))) {
      android.support.v4.view.bv.d(this);
    }
  }
  
  float d(View paramView)
  {
    return ((s)paramView.getLayoutParams()).b;
  }
  
  public void d(int paramInt)
  {
    View localView = b(paramInt);
    if (localView == null) {
      throw new IllegalArgumentException("No drawer view found with gravity " + c(paramInt));
    }
    h(localView);
  }
  
  protected boolean drawChild(Canvas paramCanvas, View paramView, long paramLong)
  {
    int i1 = getHeight();
    boolean bool1 = f(paramView);
    int i2 = getWidth();
    int i3 = paramCanvas.save();
    int i4 = 0;
    View localView;
    int i16;
    int i17;
    if (bool1)
    {
      int i14 = getChildCount();
      int i15 = 0;
      for (;;)
      {
        if (i15 < i14)
        {
          localView = getChildAt(i15);
          if ((localView != paramView) && (localView.getVisibility() == 0) && (m(localView)) && (g(localView)))
          {
            if (localView.getHeight() < i1)
            {
              i16 = i2;
              i15++;
              i2 = i16;
              continue;
            }
            if (a(localView, 3))
            {
              i17 = localView.getRight();
              if (i17 <= i4) {
                break label496;
              }
            }
          }
        }
      }
    }
    for (;;)
    {
      i4 = i17;
      i16 = i2;
      break;
      i16 = localView.getLeft();
      if (i16 < i2) {
        break;
      }
      i16 = i2;
      break;
      paramCanvas.clipRect(i4, 0, i2, getHeight());
      int i5 = i2;
      boolean bool2 = super.drawChild(paramCanvas, paramView, paramLong);
      paramCanvas.restoreToCount(i3);
      if ((this.g > 0.0F) && (bool1))
      {
        int i13 = (int)(((0xFF000000 & this.f) >>> 24) * this.g) << 24 | 0xFFFFFF & this.f;
        this.h.setColor(i13);
        paramCanvas.drawRect(i4, 0.0F, i5, getHeight(), this.h);
      }
      do
      {
        return bool2;
        if ((this.w != null) && (a(paramView, 3)))
        {
          int i10 = this.w.getIntrinsicWidth();
          int i11 = paramView.getRight();
          int i12 = this.i.b();
          float f2 = Math.max(0.0F, Math.min(i11 / i12, 1.0F));
          this.w.setBounds(i11, paramView.getTop(), i10 + i11, paramView.getBottom());
          this.w.setAlpha((int)(255.0F * f2));
          this.w.draw(paramCanvas);
          return bool2;
        }
      } while ((this.x == null) || (!a(paramView, 5)));
      int i6 = this.x.getIntrinsicWidth();
      int i7 = paramView.getLeft();
      int i8 = getWidth() - i7;
      int i9 = this.j.b();
      float f1 = Math.max(0.0F, Math.min(i8 / i9, 1.0F));
      this.x.setBounds(i7 - i6, paramView.getTop(), i7, paramView.getBottom());
      this.x.setAlpha((int)(255.0F * f1));
      this.x.draw(paramCanvas);
      return bool2;
      label496:
      i17 = i4;
    }
  }
  
  int e(View paramView)
  {
    return android.support.v4.view.q.a(((s)paramView.getLayoutParams()).a, android.support.v4.view.bv.h(this));
  }
  
  public void e(int paramInt)
  {
    View localView = b(paramInt);
    if (localView == null) {
      throw new IllegalArgumentException("No drawer view found with gravity " + c(paramInt));
    }
    i(localView);
  }
  
  public boolean f(int paramInt)
  {
    View localView = b(paramInt);
    if (localView != null) {
      return j(localView);
    }
    return false;
  }
  
  boolean f(View paramView)
  {
    return ((s)paramView.getLayoutParams()).a == 0;
  }
  
  public boolean g(int paramInt)
  {
    View localView = b(paramInt);
    if (localView != null) {
      return k(localView);
    }
    return false;
  }
  
  boolean g(View paramView)
  {
    return (0x7 & android.support.v4.view.q.a(((s)paramView.getLayoutParams()).a, android.support.v4.view.bv.h(paramView))) != 0;
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams()
  {
    return new s(-1, -1);
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
  {
    return new s(getContext(), paramAttributeSet);
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    if ((paramLayoutParams instanceof s)) {
      return new s((s)paramLayoutParams);
    }
    if ((paramLayoutParams instanceof ViewGroup.MarginLayoutParams)) {
      return new s((ViewGroup.MarginLayoutParams)paramLayoutParams);
    }
    return new s(paramLayoutParams);
  }
  
  public Drawable getStatusBarBackgroundDrawable()
  {
    return this.y;
  }
  
  public void h(View paramView)
  {
    if (!g(paramView)) {
      throw new IllegalArgumentException("View " + paramView + " is not a sliding drawer");
    }
    if (this.o)
    {
      s locals = (s)paramView.getLayoutParams();
      locals.b = 1.0F;
      locals.d = true;
      a(paramView, true);
    }
    for (;;)
    {
      invalidate();
      return;
      if (a(paramView, 3)) {
        this.i.a(paramView, 0, paramView.getTop());
      } else {
        this.j.a(paramView, getWidth() - paramView.getWidth(), paramView.getTop());
      }
    }
  }
  
  public void i(View paramView)
  {
    if (!g(paramView)) {
      throw new IllegalArgumentException("View " + paramView + " is not a sliding drawer");
    }
    if (this.o)
    {
      s locals = (s)paramView.getLayoutParams();
      locals.b = 0.0F;
      locals.d = false;
    }
    for (;;)
    {
      invalidate();
      return;
      if (a(paramView, 3)) {
        this.i.a(paramView, -paramView.getWidth(), paramView.getTop());
      } else {
        this.j.a(paramView, getWidth(), paramView.getTop());
      }
    }
  }
  
  public boolean j(View paramView)
  {
    if (!g(paramView)) {
      throw new IllegalArgumentException("View " + paramView + " is not a drawer");
    }
    return ((s)paramView.getLayoutParams()).d;
  }
  
  public boolean k(View paramView)
  {
    if (!g(paramView)) {
      throw new IllegalArgumentException("View " + paramView + " is not a drawer");
    }
    return ((s)paramView.getLayoutParams()).b > 0.0F;
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    this.o = true;
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    this.o = true;
  }
  
  public void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if ((this.C) && (this.y != null))
    {
      int i1 = a.a(this.B);
      if (i1 > 0)
      {
        this.y.setBounds(0, 0, getWidth(), i1);
        this.y.draw(paramCanvas);
      }
    }
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    int i1 = ba.a(paramMotionEvent);
    boolean bool1 = this.i.a(paramMotionEvent) | this.j.a(paramMotionEvent);
    switch (i1)
    {
    default: 
      i2 = 0;
      boolean bool2;
      if ((!bool1) && (i2 == 0) && (!f()))
      {
        boolean bool3 = this.s;
        bool2 = false;
        if (!bool3) {}
      }
      else
      {
        bool2 = true;
      }
      return bool2;
    case 0: 
      label59:
      float f1 = paramMotionEvent.getX();
      float f2 = paramMotionEvent.getY();
      this.u = f1;
      this.v = f2;
      if (this.g > 0.0F)
      {
        View localView = this.i.d((int)f1, (int)f2);
        if ((localView == null) || (!f(localView))) {
          break;
        }
      }
      break;
    }
    for (int i2 = 1;; i2 = 0)
    {
      this.r = false;
      this.s = false;
      break label59;
      if (!this.i.d(3)) {
        break;
      }
      this.k.a();
      this.l.a();
      i2 = 0;
      break label59;
      a(true);
      this.r = false;
      this.s = false;
      break;
    }
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) && (g()))
    {
      v.b(paramKeyEvent);
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4)
    {
      View localView = h();
      if ((localView != null) && (a(localView) == 0)) {
        b();
      }
      return localView != null;
    }
    return super.onKeyUp(paramInt, paramKeyEvent);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.n = true;
    int i1 = paramInt3 - paramInt1;
    int i2 = getChildCount();
    int i3 = 0;
    if (i3 < i2)
    {
      View localView = getChildAt(i3);
      if (localView.getVisibility() == 8) {}
      s locals;
      for (;;)
      {
        i3++;
        break;
        locals = (s)localView.getLayoutParams();
        if (!f(localView)) {
          break label110;
        }
        localView.layout(locals.leftMargin, locals.topMargin, locals.leftMargin + localView.getMeasuredWidth(), locals.topMargin + localView.getMeasuredHeight());
      }
      label110:
      int i4 = localView.getMeasuredWidth();
      int i5 = localView.getMeasuredHeight();
      int i6;
      float f1;
      label162:
      int i7;
      if (a(localView, 3))
      {
        i6 = -i4 + (int)(i4 * locals.b);
        f1 = (i4 + i6) / i4;
        if (f1 == locals.b) {
          break label313;
        }
        i7 = 1;
        label176:
        switch (0x70 & locals.a)
        {
        default: 
          localView.layout(i6, locals.topMargin, i4 + i6, i5 + locals.topMargin);
          label237:
          if (i7 != 0) {
            b(localView, f1);
          }
          if (locals.b <= 0.0F) {
            break;
          }
        }
      }
      for (int i10 = 0; localView.getVisibility() != i10; i10 = 4)
      {
        localView.setVisibility(i10);
        break;
        i6 = i1 - (int)(i4 * locals.b);
        f1 = (i1 - i6) / i4;
        break label162;
        label313:
        i7 = 0;
        break label176;
        int i11 = paramInt4 - paramInt2;
        localView.layout(i6, i11 - locals.bottomMargin - localView.getMeasuredHeight(), i4 + i6, i11 - locals.bottomMargin);
        break label237;
        int i8 = paramInt4 - paramInt2;
        int i9 = (i8 - i5) / 2;
        if (i9 < locals.topMargin) {
          i9 = locals.topMargin;
        }
        for (;;)
        {
          localView.layout(i6, i9, i4 + i6, i5 + i9);
          break;
          if (i9 + i5 > i8 - locals.bottomMargin) {
            i9 = i8 - locals.bottomMargin - i5;
          }
        }
      }
    }
    this.n = false;
    this.o = false;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i1 = 300;
    int i2 = View.MeasureSpec.getMode(paramInt1);
    int i3 = View.MeasureSpec.getMode(paramInt2);
    int i4 = View.MeasureSpec.getSize(paramInt1);
    int i5 = View.MeasureSpec.getSize(paramInt2);
    label70:
    int i6;
    label94:
    int i7;
    int i9;
    label109:
    View localView;
    if ((i2 != 1073741824) || (i3 != 1073741824))
    {
      if (!isInEditMode()) {
        break label162;
      }
      if (i2 == Integer.MIN_VALUE)
      {
        if (i3 != Integer.MIN_VALUE) {
          break label151;
        }
        i1 = i5;
        setMeasuredDimension(i4, i1);
        if ((this.B == null) || (!android.support.v4.view.bv.t(this))) {
          break label173;
        }
        i6 = 1;
        i7 = android.support.v4.view.bv.h(this);
        int i8 = getChildCount();
        i9 = 0;
        if (i9 >= i8) {
          return;
        }
        localView = getChildAt(i9);
        if (localView.getVisibility() != 8) {
          break label179;
        }
      }
    }
    for (;;)
    {
      i9++;
      break label109;
      if (i2 != 0) {
        break;
      }
      i4 = i1;
      break;
      label151:
      if (i3 == 0) {
        break label70;
      }
      i1 = i5;
      break label70;
      label162:
      throw new IllegalArgumentException("DrawerLayout must be measured with MeasureSpec.EXACTLY.");
      label173:
      i6 = 0;
      break label94;
      label179:
      s locals = (s)localView.getLayoutParams();
      int i11;
      if (i6 != 0)
      {
        i11 = android.support.v4.view.q.a(locals.a, i7);
        if (!android.support.v4.view.bv.t(localView)) {
          break label286;
        }
        a.a(localView, this.B, i11);
      }
      for (;;)
      {
        if (!f(localView)) {
          break label305;
        }
        localView.measure(View.MeasureSpec.makeMeasureSpec(i4 - locals.leftMargin - locals.rightMargin, 1073741824), View.MeasureSpec.makeMeasureSpec(i1 - locals.topMargin - locals.bottomMargin, 1073741824));
        break;
        label286:
        a.a(locals, this.B, i11);
      }
      label305:
      if (!g(localView)) {
        break label441;
      }
      int i10 = 0x7 & e(localView);
      if ((0x0 & i10) != 0) {
        throw new IllegalStateException("Child drawer has absolute gravity " + c(i10) + " but this " + "DrawerLayout" + " already has a " + "drawer view along that edge");
      }
      localView.measure(getChildMeasureSpec(paramInt1, this.e + locals.leftMargin + locals.rightMargin, locals.width), getChildMeasureSpec(paramInt2, locals.topMargin + locals.bottomMargin, locals.height));
    }
    label441:
    throw new IllegalStateException("Child " + localView + " at index " + i9 + " does not have a valid layout_gravity - must be Gravity.LEFT, " + "Gravity.RIGHT or Gravity.NO_GRAVITY");
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    DrawerLayout.SavedState localSavedState = (DrawerLayout.SavedState)paramParcelable;
    super.onRestoreInstanceState(localSavedState.getSuperState());
    if (localSavedState.a != 0)
    {
      View localView = b(localSavedState.a);
      if (localView != null) {
        h(localView);
      }
    }
    b(localSavedState.b, 3);
    b(localSavedState.c, 5);
  }
  
  protected Parcelable onSaveInstanceState()
  {
    DrawerLayout.SavedState localSavedState = new DrawerLayout.SavedState(super.onSaveInstanceState());
    View localView = a();
    if (localView != null) {
      localSavedState.a = ((s)localView.getLayoutParams()).a;
    }
    localSavedState.b = this.p;
    localSavedState.c = this.q;
    return localSavedState;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    this.i.b(paramMotionEvent);
    this.j.b(paramMotionEvent);
    boolean bool;
    switch (0xFF & paramMotionEvent.getAction())
    {
    case 2: 
    default: 
      return true;
    case 0: 
      float f5 = paramMotionEvent.getX();
      float f6 = paramMotionEvent.getY();
      this.u = f5;
      this.v = f6;
      this.r = false;
      this.s = false;
      return true;
    case 1: 
      float f1 = paramMotionEvent.getX();
      float f2 = paramMotionEvent.getY();
      View localView1 = this.i.d((int)f1, (int)f2);
      if ((localView1 != null) && (f(localView1)))
      {
        float f3 = f1 - this.u;
        float f4 = f2 - this.v;
        int i1 = this.i.d();
        if (f3 * f3 + f4 * f4 < i1 * i1)
        {
          View localView2 = a();
          if (localView2 != null) {
            if (a(localView2) == 2) {
              bool = true;
            }
          }
        }
      }
      break;
    }
    for (;;)
    {
      a(bool);
      this.r = false;
      return true;
      bool = false;
      continue;
      a(true);
      this.r = false;
      this.s = false;
      return true;
      bool = true;
    }
  }
  
  public void requestDisallowInterceptTouchEvent(boolean paramBoolean)
  {
    super.requestDisallowInterceptTouchEvent(paramBoolean);
    this.r = paramBoolean;
    if (paramBoolean) {
      a(true);
    }
  }
  
  public void requestLayout()
  {
    if (!this.n) {
      super.requestLayout();
    }
  }
  
  public void setDrawerListener(r paramr)
  {
    this.t = paramr;
  }
  
  public void setDrawerLockMode(int paramInt)
  {
    b(paramInt, 3);
    b(paramInt, 5);
  }
  
  public void setScrimColor(int paramInt)
  {
    this.f = paramInt;
    invalidate();
  }
  
  public void setStatusBarBackground(int paramInt)
  {
    if (paramInt != 0) {}
    for (Drawable localDrawable = a.a(getContext(), paramInt);; localDrawable = null)
    {
      this.y = localDrawable;
      invalidate();
      return;
    }
  }
  
  public void setStatusBarBackground(Drawable paramDrawable)
  {
    this.y = paramDrawable;
    invalidate();
  }
  
  public void setStatusBarBackgroundColor(int paramInt)
  {
    this.y = new ColorDrawable(paramInt);
    invalidate();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/widget/DrawerLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */