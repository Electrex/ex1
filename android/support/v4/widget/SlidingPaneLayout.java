package android.support.v4.widget;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Parcelable;
import android.support.v4.view.ba;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import java.util.ArrayList;

public class SlidingPaneLayout
  extends ViewGroup
{
  static final bi a = new bj();
  private int b;
  private int c;
  private Drawable d;
  private Drawable e;
  private final int f;
  private boolean g;
  private View h;
  private float i;
  private float j;
  private int k;
  private boolean l;
  private int m;
  private float n;
  private float o;
  private bg p;
  private final bv q;
  private boolean r;
  private boolean s;
  private final Rect t;
  private final ArrayList u;
  
  static
  {
    int i1 = Build.VERSION.SDK_INT;
    if (i1 >= 17)
    {
      a = new bl();
      return;
    }
    if (i1 >= 16)
    {
      a = new bk();
      return;
    }
  }
  
  private void a(float paramFloat)
  {
    boolean bool = f();
    bf localbf = (bf)this.h.getLayoutParams();
    int i6;
    int i1;
    label41:
    int i3;
    label50:
    View localView;
    if (localbf.c) {
      if (bool)
      {
        i6 = localbf.rightMargin;
        if (i6 > 0) {
          break label89;
        }
        i1 = 1;
        int i2 = getChildCount();
        i3 = 0;
        if (i3 >= i2) {
          return;
        }
        localView = getChildAt(i3);
        if (localView != this.h) {
          break label95;
        }
      }
    }
    label89:
    label95:
    do
    {
      i3++;
      break label50;
      i6 = localbf.leftMargin;
      break;
      i1 = 0;
      break label41;
      int i4 = (int)((1.0F - this.j) * this.m);
      this.j = paramFloat;
      int i5 = i4 - (int)((1.0F - paramFloat) * this.m);
      if (bool) {
        i5 = -i5;
      }
      localView.offsetLeftAndRight(i5);
    } while (i1 == 0);
    if (bool) {}
    for (float f1 = this.j - 1.0F;; f1 = 1.0F - this.j)
    {
      a(localView, f1, this.c);
      break;
    }
  }
  
  private void a(View paramView, float paramFloat, int paramInt)
  {
    bf localbf = (bf)paramView.getLayoutParams();
    if ((paramFloat > 0.0F) && (paramInt != 0))
    {
      i1 = (int)(paramFloat * ((0xFF000000 & paramInt) >>> 24)) << 24 | 0xFFFFFF & paramInt;
      if (localbf.d == null) {
        localbf.d = new Paint();
      }
      localbf.d.setColorFilter(new PorterDuffColorFilter(i1, PorterDuff.Mode.SRC_OVER));
      if (android.support.v4.view.bv.g(paramView) != 2) {
        android.support.v4.view.bv.a(paramView, 2, localbf.d);
      }
      d(paramView);
    }
    while (android.support.v4.view.bv.g(paramView) == 0)
    {
      int i1;
      return;
    }
    if (localbf.d != null) {
      localbf.d.setColorFilter(null);
    }
    be localbe = new be(this, paramView);
    this.u.add(localbe);
    android.support.v4.view.bv.a(this, localbe);
  }
  
  private boolean a(View paramView, int paramInt)
  {
    boolean bool1;
    if (!this.s)
    {
      boolean bool2 = a(0.0F, paramInt);
      bool1 = false;
      if (!bool2) {}
    }
    else
    {
      this.r = false;
      bool1 = true;
    }
    return bool1;
  }
  
  private boolean b(View paramView, int paramInt)
  {
    if ((this.s) || (a(1.0F, paramInt)))
    {
      this.r = true;
      return true;
    }
    return false;
  }
  
  private static boolean c(View paramView)
  {
    if (android.support.v4.view.bv.j(paramView)) {}
    Drawable localDrawable;
    do
    {
      return true;
      if (Build.VERSION.SDK_INT >= 18) {
        return false;
      }
      localDrawable = paramView.getBackground();
      if (localDrawable == null) {
        break;
      }
    } while (localDrawable.getOpacity() == -1);
    return false;
    return false;
  }
  
  private void d(View paramView)
  {
    a.a(this, paramView);
  }
  
  private boolean f()
  {
    return android.support.v4.view.bv.h(this) == 1;
  }
  
  void a()
  {
    int i1 = getChildCount();
    for (int i2 = 0; i2 < i1; i2++)
    {
      View localView = getChildAt(i2);
      if (localView.getVisibility() == 4) {
        localView.setVisibility(0);
      }
    }
  }
  
  void a(View paramView)
  {
    boolean bool = f();
    int i1;
    int i2;
    label29:
    int i3;
    int i4;
    int i8;
    int i7;
    int i6;
    int i5;
    if (bool)
    {
      i1 = getWidth() - getPaddingRight();
      if (!bool) {
        break label120;
      }
      i2 = getPaddingLeft();
      i3 = getPaddingTop();
      i4 = getHeight() - getPaddingBottom();
      if ((paramView == null) || (!c(paramView))) {
        break label134;
      }
      i8 = paramView.getLeft();
      i7 = paramView.getRight();
      i6 = paramView.getTop();
      i5 = paramView.getBottom();
    }
    int i10;
    View localView;
    for (;;)
    {
      int i9 = getChildCount();
      i10 = 0;
      if (i10 < i9)
      {
        localView = getChildAt(i10);
        if (localView != paramView) {
          break label149;
        }
      }
      return;
      i1 = getPaddingLeft();
      break;
      label120:
      i2 = getWidth() - getPaddingRight();
      break label29;
      label134:
      i5 = 0;
      i6 = 0;
      i7 = 0;
      i8 = 0;
    }
    label149:
    int i11;
    label157:
    int i14;
    if (bool)
    {
      i11 = i2;
      int i12 = Math.max(i11, localView.getLeft());
      int i13 = Math.max(i3, localView.getTop());
      if (!bool) {
        break label262;
      }
      i14 = i1;
      label188:
      int i15 = Math.min(i14, localView.getRight());
      int i16 = Math.min(i4, localView.getBottom());
      if ((i12 < i8) || (i13 < i6) || (i15 > i7) || (i16 > i5)) {
        break label269;
      }
    }
    label262:
    label269:
    for (int i17 = 4;; i17 = 0)
    {
      localView.setVisibility(i17);
      i10++;
      break;
      i11 = i1;
      break label157;
      i14 = i2;
      break label188;
    }
  }
  
  boolean a(float paramFloat, int paramInt)
  {
    if (!this.g) {
      return false;
    }
    boolean bool = f();
    bf localbf = (bf)this.h.getLayoutParams();
    int i2;
    int i3;
    if (bool)
    {
      i2 = getPaddingRight() + localbf.rightMargin;
      i3 = this.h.getWidth();
    }
    for (int i1 = (int)(getWidth() - (i2 + paramFloat * this.k + i3)); this.q.a(this.h, i1, this.h.getTop()); i1 = (int)(getPaddingLeft() + localbf.leftMargin + paramFloat * this.k))
    {
      a();
      android.support.v4.view.bv.d(this);
      return true;
    }
    return false;
  }
  
  public boolean b()
  {
    return b(this.h, 0);
  }
  
  boolean b(View paramView)
  {
    if (paramView == null) {
      return false;
    }
    bf localbf = (bf)paramView.getLayoutParams();
    if ((this.g) && (localbf.c) && (this.i > 0.0F)) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  public boolean c()
  {
    return a(this.h, 0);
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return ((paramLayoutParams instanceof bf)) && (super.checkLayoutParams(paramLayoutParams));
  }
  
  public void computeScroll()
  {
    if (this.q.a(true))
    {
      if (!this.g) {
        this.q.f();
      }
    }
    else {
      return;
    }
    android.support.v4.view.bv.d(this);
  }
  
  public boolean d()
  {
    return (!this.g) || (this.i == 1.0F);
  }
  
  public void draw(Canvas paramCanvas)
  {
    super.draw(paramCanvas);
    Drawable localDrawable;
    if (f())
    {
      localDrawable = this.e;
      if (getChildCount() <= 1) {
        break label48;
      }
    }
    label48:
    for (View localView = getChildAt(1);; localView = null)
    {
      if ((localView != null) && (localDrawable != null)) {
        break label53;
      }
      return;
      localDrawable = this.d;
      break;
    }
    label53:
    int i1 = localView.getTop();
    int i2 = localView.getBottom();
    int i3 = localDrawable.getIntrinsicWidth();
    int i5;
    int i4;
    if (f())
    {
      i5 = localView.getRight();
      i4 = i5 + i3;
    }
    for (;;)
    {
      localDrawable.setBounds(i5, i1, i4, i2);
      localDrawable.draw(paramCanvas);
      return;
      i4 = localView.getLeft();
      i5 = i4 - i3;
    }
  }
  
  protected boolean drawChild(Canvas paramCanvas, View paramView, long paramLong)
  {
    bf localbf = (bf)paramView.getLayoutParams();
    int i1 = paramCanvas.save(2);
    boolean bool;
    if ((this.g) && (!localbf.b) && (this.h != null))
    {
      paramCanvas.getClipBounds(this.t);
      if (f())
      {
        this.t.left = Math.max(this.t.left, this.h.getRight());
        paramCanvas.clipRect(this.t);
      }
    }
    else
    {
      if (Build.VERSION.SDK_INT < 11) {
        break label140;
      }
      bool = super.drawChild(paramCanvas, paramView, paramLong);
    }
    for (;;)
    {
      paramCanvas.restoreToCount(i1);
      return bool;
      this.t.right = Math.min(this.t.right, this.h.getLeft());
      break;
      label140:
      if ((localbf.c) && (this.i > 0.0F))
      {
        if (!paramView.isDrawingCacheEnabled()) {
          paramView.setDrawingCacheEnabled(true);
        }
        Bitmap localBitmap = paramView.getDrawingCache();
        if (localBitmap != null)
        {
          paramCanvas.drawBitmap(localBitmap, paramView.getLeft(), paramView.getTop(), localbf.d);
          bool = false;
        }
        else
        {
          Log.e("SlidingPaneLayout", "drawChild: child view " + paramView + " returned null drawing cache");
          bool = super.drawChild(paramCanvas, paramView, paramLong);
        }
      }
      else
      {
        if (paramView.isDrawingCacheEnabled()) {
          paramView.setDrawingCacheEnabled(false);
        }
        bool = super.drawChild(paramCanvas, paramView, paramLong);
      }
    }
  }
  
  public boolean e()
  {
    return this.g;
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams()
  {
    return new bf();
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
  {
    return new bf(getContext(), paramAttributeSet);
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    if ((paramLayoutParams instanceof ViewGroup.MarginLayoutParams)) {
      return new bf((ViewGroup.MarginLayoutParams)paramLayoutParams);
    }
    return new bf(paramLayoutParams);
  }
  
  public int getCoveredFadeColor()
  {
    return this.c;
  }
  
  public int getParallaxDistance()
  {
    return this.m;
  }
  
  public int getSliderFadeColor()
  {
    return this.b;
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    this.s = true;
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    this.s = true;
    int i1 = this.u.size();
    for (int i2 = 0; i2 < i1; i2++) {
      ((be)this.u.get(i2)).run();
    }
    this.u.clear();
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    int i1 = ba.a(paramMotionEvent);
    if ((!this.g) && (i1 == 0) && (getChildCount() > 1))
    {
      View localView = getChildAt(1);
      if (localView != null) {
        if (this.q.b(localView, (int)paramMotionEvent.getX(), (int)paramMotionEvent.getY())) {
          break label100;
        }
      }
    }
    boolean bool1;
    label100:
    for (boolean bool2 = true;; bool2 = false)
    {
      this.r = bool2;
      if ((this.g) && ((!this.l) || (i1 == 0))) {
        break;
      }
      this.q.e();
      bool1 = super.onInterceptTouchEvent(paramMotionEvent);
      return bool1;
    }
    if ((i1 == 3) || (i1 == 1))
    {
      this.q.e();
      return false;
    }
    switch (i1)
    {
    }
    label152:
    float f3;
    float f4;
    do
    {
      for (int i2 = 0;; i2 = 1)
      {
        if (!this.q.a(paramMotionEvent))
        {
          bool1 = false;
          if (i2 == 0) {
            break;
          }
        }
        return true;
        this.l = false;
        float f5 = paramMotionEvent.getX();
        float f6 = paramMotionEvent.getY();
        this.n = f5;
        this.o = f6;
        if ((!this.q.b(this.h, (int)f5, (int)f6)) || (!b(this.h))) {
          break label152;
        }
      }
      float f1 = paramMotionEvent.getX();
      float f2 = paramMotionEvent.getY();
      f3 = Math.abs(f1 - this.n);
      f4 = Math.abs(f2 - this.o);
    } while ((f3 <= this.q.d()) || (f4 <= f3));
    this.q.e();
    this.l = true;
    return false;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    boolean bool1 = f();
    int i1;
    int i2;
    label36:
    int i3;
    label47:
    int i4;
    int i5;
    if (bool1)
    {
      this.q.a(2);
      i1 = paramInt3 - paramInt1;
      if (!bool1) {
        break label154;
      }
      i2 = getPaddingRight();
      if (!bool1) {
        break label163;
      }
      i3 = getPaddingLeft();
      i4 = getPaddingTop();
      i5 = getChildCount();
      if (this.s) {
        if ((!this.g) || (!this.r)) {
          break label172;
        }
      }
    }
    int i7;
    View localView;
    int i14;
    int i15;
    label154:
    label163:
    label172:
    for (float f1 = 1.0F;; f1 = 0.0F)
    {
      this.i = f1;
      int i6 = 0;
      for (i7 = i2;; i7 = i15)
      {
        if (i6 >= i5) {
          break label462;
        }
        localView = getChildAt(i6);
        if (localView.getVisibility() != 8) {
          break;
        }
        i14 = i2;
        i15 = i7;
        i6++;
        i2 = i14;
      }
      this.q.a(1);
      break;
      i2 = getPaddingLeft();
      break label36;
      i3 = getPaddingRight();
      break label47;
    }
    bf localbf = (bf)localView.getLayoutParams();
    int i9 = localView.getMeasuredWidth();
    int i18;
    label257:
    boolean bool2;
    label281:
    int i10;
    int i11;
    label325:
    int i13;
    int i12;
    if (localbf.b)
    {
      int i16 = localbf.leftMargin + localbf.rightMargin;
      int i17 = Math.min(i2, i1 - i3 - this.f) - i7 - i16;
      this.k = i17;
      if (bool1)
      {
        i18 = localbf.rightMargin;
        if (i17 + (i7 + i18) + i9 / 2 <= i1 - i3) {
          break label393;
        }
        bool2 = true;
        localbf.c = bool2;
        int i19 = (int)(i17 * this.i);
        i10 = i7 + (i18 + i19);
        this.i = (i19 / this.k);
        i11 = 0;
        if (!bool1) {
          break label445;
        }
        i13 = i11 + (i1 - i10);
        i12 = i13 - i9;
      }
    }
    for (;;)
    {
      localView.layout(i12, i4, i13, i4 + localView.getMeasuredHeight());
      i14 = i2 + localView.getWidth();
      i15 = i10;
      break;
      i18 = localbf.leftMargin;
      break label257;
      label393:
      bool2 = false;
      break label281;
      if ((this.g) && (this.m != 0))
      {
        i11 = (int)((1.0F - this.i) * this.m);
        i10 = i2;
        break label325;
      }
      i10 = i2;
      i11 = 0;
      break label325;
      label445:
      i12 = i10 - i11;
      i13 = i12 + i9;
    }
    label462:
    if (this.s)
    {
      if (!this.g) {
        break label537;
      }
      if (this.m != 0) {
        a(this.i);
      }
      if (((bf)this.h.getLayoutParams()).c) {
        a(this.h, this.i, this.b);
      }
    }
    for (;;)
    {
      a(this.h);
      this.s = false;
      return;
      label537:
      for (int i8 = 0; i8 < i5; i8++) {
        a(getChildAt(i8), 0.0F, this.b);
      }
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i1 = View.MeasureSpec.getMode(paramInt1);
    int i2 = View.MeasureSpec.getSize(paramInt1);
    int i3 = View.MeasureSpec.getMode(paramInt2);
    int i4 = View.MeasureSpec.getSize(paramInt2);
    int i5;
    int i6;
    int i7;
    if (i1 != 1073741824) {
      if (isInEditMode()) {
        if (i1 == Integer.MIN_VALUE)
        {
          i5 = i3;
          i6 = i2;
          i7 = i4;
        }
      }
    }
    for (;;)
    {
      int i9;
      int i8;
      label90:
      boolean bool1;
      int i10;
      int i11;
      int i12;
      int i13;
      int i14;
      float f1;
      label148:
      View localView2;
      bf localbf2;
      int i30;
      float f3;
      int i31;
      switch (i5)
      {
      default: 
        i9 = 0;
        i8 = -1;
        bool1 = false;
        i10 = i6 - getPaddingLeft() - getPaddingRight();
        i11 = getChildCount();
        if (i11 > 2) {
          Log.e("SlidingPaneLayout", "onMeasure: More than two child views are not supported.");
        }
        this.h = null;
        i12 = 0;
        i13 = i10;
        i14 = i9;
        f1 = 0.0F;
        if (i12 >= i11) {
          break label645;
        }
        localView2 = getChildAt(i12);
        localbf2 = (bf)localView2.getLayoutParams();
        if (localView2.getVisibility() == 8)
        {
          localbf2.c = false;
          i30 = i13;
          f3 = f1;
          i31 = i14;
        }
        break;
      }
      for (boolean bool4 = bool1;; bool4 = bool1)
      {
        i12++;
        bool1 = bool4;
        i14 = i31;
        f1 = f3;
        i13 = i30;
        break label148;
        if (i1 != 0) {
          break label1130;
        }
        i5 = i3;
        i6 = 300;
        i7 = i4;
        break;
        throw new IllegalStateException("Width must have an exact value or MATCH_PARENT");
        if (i3 != 0) {
          break label1130;
        }
        if (isInEditMode())
        {
          if (i3 != 0) {
            break label1130;
          }
          i5 = Integer.MIN_VALUE;
          i6 = i2;
          i7 = 300;
          break;
        }
        throw new IllegalStateException("Height must not be UNSPECIFIED");
        i9 = i7 - getPaddingTop() - getPaddingBottom();
        i8 = i9;
        break label90;
        i8 = i7 - getPaddingTop() - getPaddingBottom();
        i9 = 0;
        break label90;
        if (localbf2.a <= 0.0F) {
          break label391;
        }
        f1 += localbf2.a;
        if (localbf2.width != 0) {
          break label391;
        }
        i30 = i13;
        f3 = f1;
        i31 = i14;
      }
      label391:
      int i24 = localbf2.leftMargin + localbf2.rightMargin;
      int i25;
      label427:
      int i26;
      label447:
      int i29;
      if (localbf2.width == -2)
      {
        i25 = View.MeasureSpec.makeMeasureSpec(i10 - i24, Integer.MIN_VALUE);
        if (localbf2.height != -2) {
          break label601;
        }
        i26 = View.MeasureSpec.makeMeasureSpec(i8, Integer.MIN_VALUE);
        localView2.measure(i25, i26);
        int i27 = localView2.getMeasuredWidth();
        int i28 = localView2.getMeasuredHeight();
        if ((i5 == Integer.MIN_VALUE) && (i28 > i14)) {
          i14 = Math.min(i28, i8);
        }
        i29 = i13 - i27;
        if (i29 >= 0) {
          break label639;
        }
      }
      label601:
      label639:
      for (boolean bool2 = true;; bool2 = false)
      {
        localbf2.b = bool2;
        boolean bool3 = bool2 | bool1;
        if (localbf2.b) {
          this.h = localView2;
        }
        i30 = i29;
        i31 = i14;
        float f2 = f1;
        bool4 = bool3;
        f3 = f2;
        break;
        if (localbf2.width == -1)
        {
          i25 = View.MeasureSpec.makeMeasureSpec(i10 - i24, 1073741824);
          break label427;
        }
        i25 = View.MeasureSpec.makeMeasureSpec(localbf2.width, 1073741824);
        break label427;
        if (localbf2.height == -1)
        {
          i26 = View.MeasureSpec.makeMeasureSpec(i8, 1073741824);
          break label447;
        }
        i26 = View.MeasureSpec.makeMeasureSpec(localbf2.height, 1073741824);
        break label447;
      }
      label645:
      if ((bool1) || (f1 > 0.0F))
      {
        int i15 = i10 - this.f;
        int i16 = 0;
        if (i16 < i11)
        {
          View localView1 = getChildAt(i16);
          if (localView1.getVisibility() == 8) {}
          for (;;)
          {
            i16++;
            break;
            bf localbf1 = (bf)localView1.getLayoutParams();
            if (localView1.getVisibility() != 8)
            {
              int i17;
              label741:
              int i18;
              label749:
              int i23;
              if ((localbf1.width == 0) && (localbf1.a > 0.0F))
              {
                i17 = 1;
                if (i17 == 0) {
                  break label837;
                }
                i18 = 0;
                if ((!bool1) || (localView1 == this.h)) {
                  break label901;
                }
                if ((localbf1.width >= 0) || ((i18 <= i15) && (localbf1.a <= 0.0F))) {
                  continue;
                }
                if (i17 == 0) {
                  break label885;
                }
                if (localbf1.height != -2) {
                  break label847;
                }
                i23 = View.MeasureSpec.makeMeasureSpec(i8, Integer.MIN_VALUE);
              }
              for (;;)
              {
                localView1.measure(View.MeasureSpec.makeMeasureSpec(i15, 1073741824), i23);
                break;
                i17 = 0;
                break label741;
                label837:
                i18 = localView1.getMeasuredWidth();
                break label749;
                label847:
                if (localbf1.height == -1)
                {
                  i23 = View.MeasureSpec.makeMeasureSpec(i8, 1073741824);
                }
                else
                {
                  i23 = View.MeasureSpec.makeMeasureSpec(localbf1.height, 1073741824);
                  continue;
                  label885:
                  i23 = View.MeasureSpec.makeMeasureSpec(localView1.getMeasuredHeight(), 1073741824);
                }
              }
              label901:
              if (localbf1.a > 0.0F)
              {
                int i19;
                if (localbf1.width == 0) {
                  if (localbf1.height == -2) {
                    i19 = View.MeasureSpec.makeMeasureSpec(i8, Integer.MIN_VALUE);
                  }
                }
                for (;;)
                {
                  if (!bool1) {
                    break label1043;
                  }
                  int i21 = i10 - (localbf1.leftMargin + localbf1.rightMargin);
                  int i22 = View.MeasureSpec.makeMeasureSpec(i21, 1073741824);
                  if (i18 == i21) {
                    break;
                  }
                  localView1.measure(i22, i19);
                  break;
                  if (localbf1.height == -1)
                  {
                    i19 = View.MeasureSpec.makeMeasureSpec(i8, 1073741824);
                  }
                  else
                  {
                    i19 = View.MeasureSpec.makeMeasureSpec(localbf1.height, 1073741824);
                    continue;
                    i19 = View.MeasureSpec.makeMeasureSpec(localView1.getMeasuredHeight(), 1073741824);
                  }
                }
                label1043:
                int i20 = Math.max(0, i13);
                localView1.measure(View.MeasureSpec.makeMeasureSpec(i18 + (int)(localbf1.a * i20 / f1), 1073741824), i19);
              }
            }
          }
        }
      }
      setMeasuredDimension(i6, i14 + getPaddingTop() + getPaddingBottom());
      this.g = bool1;
      if ((this.q.a() != 0) && (!bool1)) {
        this.q.f();
      }
      return;
      label1130:
      i5 = i3;
      i6 = i2;
      i7 = i4;
    }
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    SlidingPaneLayout.SavedState localSavedState = (SlidingPaneLayout.SavedState)paramParcelable;
    super.onRestoreInstanceState(localSavedState.getSuperState());
    if (localSavedState.a) {
      b();
    }
    for (;;)
    {
      this.r = localSavedState.a;
      return;
      c();
    }
  }
  
  protected Parcelable onSaveInstanceState()
  {
    SlidingPaneLayout.SavedState localSavedState = new SlidingPaneLayout.SavedState(super.onSaveInstanceState());
    if (e()) {}
    for (boolean bool = d();; bool = this.r)
    {
      localSavedState.a = bool;
      return localSavedState;
    }
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    if (paramInt1 != paramInt3) {
      this.s = true;
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool;
    if (!this.g) {
      bool = super.onTouchEvent(paramMotionEvent);
    }
    float f1;
    float f2;
    float f3;
    float f4;
    int i2;
    do
    {
      do
      {
        return bool;
        this.q.b(paramMotionEvent);
        int i1 = paramMotionEvent.getAction();
        bool = true;
        switch (i1 & 0xFF)
        {
        default: 
          return bool;
        case 0: 
          float f5 = paramMotionEvent.getX();
          float f6 = paramMotionEvent.getY();
          this.n = f5;
          this.o = f6;
          return bool;
        }
      } while (!b(this.h));
      f1 = paramMotionEvent.getX();
      f2 = paramMotionEvent.getY();
      f3 = f1 - this.n;
      f4 = f2 - this.o;
      i2 = this.q.d();
    } while ((f3 * f3 + f4 * f4 >= i2 * i2) || (!this.q.b(this.h, (int)f1, (int)f2)));
    a(this.h, 0);
    return bool;
  }
  
  public void requestChildFocus(View paramView1, View paramView2)
  {
    super.requestChildFocus(paramView1, paramView2);
    if ((!isInTouchMode()) && (!this.g)) {
      if (paramView1 != this.h) {
        break label36;
      }
    }
    label36:
    for (boolean bool = true;; bool = false)
    {
      this.r = bool;
      return;
    }
  }
  
  public void setCoveredFadeColor(int paramInt)
  {
    this.c = paramInt;
  }
  
  public void setPanelSlideListener(bg parambg)
  {
    this.p = parambg;
  }
  
  public void setParallaxDistance(int paramInt)
  {
    this.m = paramInt;
    requestLayout();
  }
  
  @Deprecated
  public void setShadowDrawable(Drawable paramDrawable)
  {
    setShadowDrawableLeft(paramDrawable);
  }
  
  public void setShadowDrawableLeft(Drawable paramDrawable)
  {
    this.d = paramDrawable;
  }
  
  public void setShadowDrawableRight(Drawable paramDrawable)
  {
    this.e = paramDrawable;
  }
  
  @Deprecated
  public void setShadowResource(int paramInt)
  {
    setShadowDrawable(getResources().getDrawable(paramInt));
  }
  
  public void setShadowResourceLeft(int paramInt)
  {
    setShadowDrawableLeft(getResources().getDrawable(paramInt));
  }
  
  public void setShadowResourceRight(int paramInt)
  {
    setShadowDrawableRight(getResources().getDrawable(paramInt));
  }
  
  public void setSliderFadeColor(int paramInt)
  {
    this.b = paramInt;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/widget/SlidingPaneLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */