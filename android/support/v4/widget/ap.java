package android.support.v4.widget;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class ap
  implements Parcelable.Creator
{
  public NestedScrollView.SavedState a(Parcel paramParcel)
  {
    return new NestedScrollView.SavedState(paramParcel);
  }
  
  public NestedScrollView.SavedState[] a(int paramInt)
  {
    return new NestedScrollView.SavedState[paramInt];
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/widget/ap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */