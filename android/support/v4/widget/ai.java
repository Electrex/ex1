package android.support.v4.widget;

import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.Transformation;

class ai
  extends Animation
{
  ai(ah paramah, am paramam) {}
  
  public void applyTransformation(float paramFloat, Transformation paramTransformation)
  {
    if (this.b.a)
    {
      ah.a(this.b, paramFloat, this.a);
      return;
    }
    float f1 = (float)Math.toRadians(this.a.c() / (6.283185307179586D * this.a.h()));
    float f2 = this.a.f();
    float f3 = this.a.e();
    float f4 = this.a.i();
    float f5 = f2 + (0.8F - f1) * ah.a().getInterpolation(paramFloat);
    this.a.c(f5);
    float f6 = f3 + 0.8F * ah.b().getInterpolation(paramFloat);
    this.a.b(f6);
    float f7 = f4 + 0.25F * paramFloat;
    this.a.d(f7);
    float f8 = 144.0F * paramFloat + 720.0F * (ah.a(this.b) / 5.0F);
    this.b.c(f8);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/widget/ai.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */