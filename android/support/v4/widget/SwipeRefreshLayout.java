package android.support.v4.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.view.ba;
import android.support.v4.view.bv;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;

public class SwipeRefreshLayout
  extends ViewGroup
{
  private static final String c = SwipeRefreshLayout.class.getSimpleName();
  private static final int[] s = { 16842766 };
  private Animation A;
  private Animation B;
  private float C;
  private boolean D;
  private int E;
  private int F;
  private boolean G;
  private Animation.AnimationListener H = new bm(this);
  private final Animation I = new br(this);
  private final Animation J = new bs(this);
  protected int a;
  protected int b;
  private View d;
  private bu e;
  private boolean f = false;
  private int g;
  private float h = -1.0F;
  private int i;
  private int j;
  private boolean k = false;
  private float l;
  private float m;
  private boolean n;
  private int o = -1;
  private boolean p;
  private boolean q;
  private final DecelerateInterpolator r;
  private e t;
  private int u = -1;
  private float v;
  private ah w;
  private Animation x;
  private Animation y;
  private Animation z;
  
  public SwipeRefreshLayout(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public SwipeRefreshLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.g = ViewConfiguration.get(paramContext).getScaledTouchSlop();
    this.i = getResources().getInteger(17694721);
    setWillNotDraw(false);
    this.r = new DecelerateInterpolator(2.0F);
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, s);
    setEnabled(localTypedArray.getBoolean(0, true));
    localTypedArray.recycle();
    DisplayMetrics localDisplayMetrics = getResources().getDisplayMetrics();
    this.E = ((int)(40.0F * localDisplayMetrics.density));
    this.F = ((int)(40.0F * localDisplayMetrics.density));
    b();
    bv.a(this, true);
    this.C = (64.0F * localDisplayMetrics.density);
    this.h = this.C;
  }
  
  private float a(MotionEvent paramMotionEvent, int paramInt)
  {
    int i1 = ba.a(paramMotionEvent, paramInt);
    if (i1 < 0) {
      return -1.0F;
    }
    return ba.d(paramMotionEvent, i1);
  }
  
  private Animation a(int paramInt1, int paramInt2)
  {
    if ((this.p) && (c())) {
      return null;
    }
    bp localbp = new bp(this, paramInt1, paramInt2);
    localbp.setDuration(300L);
    this.t.a(null);
    this.t.clearAnimation();
    this.t.startAnimation(localbp);
    return localbp;
  }
  
  private void a(float paramFloat)
  {
    a(this.a + (int)(paramFloat * (this.b - this.a)) - this.t.getTop(), false);
  }
  
  private void a(int paramInt, Animation.AnimationListener paramAnimationListener)
  {
    this.a = paramInt;
    this.I.reset();
    this.I.setDuration(200L);
    this.I.setInterpolator(this.r);
    if (paramAnimationListener != null) {
      this.t.a(paramAnimationListener);
    }
    this.t.clearAnimation();
    this.t.startAnimation(this.I);
  }
  
  private void a(int paramInt, boolean paramBoolean)
  {
    this.t.bringToFront();
    this.t.offsetTopAndBottom(paramInt);
    this.j = this.t.getTop();
    if ((paramBoolean) && (Build.VERSION.SDK_INT < 11)) {
      invalidate();
    }
  }
  
  private void a(MotionEvent paramMotionEvent)
  {
    int i1 = ba.b(paramMotionEvent);
    if (ba.b(paramMotionEvent, i1) == this.o) {
      if (i1 != 0) {
        break label33;
      }
    }
    label33:
    for (int i2 = 1;; i2 = 0)
    {
      this.o = ba.b(paramMotionEvent, i2);
      return;
    }
  }
  
  private void a(Animation.AnimationListener paramAnimationListener)
  {
    this.t.setVisibility(0);
    if (Build.VERSION.SDK_INT >= 11) {
      this.w.setAlpha(255);
    }
    this.x = new bn(this);
    this.x.setDuration(this.i);
    if (paramAnimationListener != null) {
      this.t.a(paramAnimationListener);
    }
    this.t.clearAnimation();
    this.t.startAnimation(this.x);
  }
  
  private void a(boolean paramBoolean1, boolean paramBoolean2)
  {
    if (this.f != paramBoolean1)
    {
      this.D = paramBoolean2;
      f();
      this.f = paramBoolean1;
      if (this.f) {
        a(this.j, this.H);
      }
    }
    else
    {
      return;
    }
    b(this.H);
  }
  
  private boolean a(Animation paramAnimation)
  {
    return (paramAnimation != null) && (paramAnimation.hasStarted()) && (!paramAnimation.hasEnded());
  }
  
  private void b()
  {
    this.t = new e(getContext(), -328966, 20.0F);
    this.w = new ah(getContext(), this);
    this.w.b(-328966);
    this.t.setImageDrawable(this.w);
    this.t.setVisibility(8);
    addView(this.t);
  }
  
  private void b(int paramInt, Animation.AnimationListener paramAnimationListener)
  {
    if (this.p)
    {
      c(paramInt, paramAnimationListener);
      return;
    }
    this.a = paramInt;
    this.J.reset();
    this.J.setDuration(200L);
    this.J.setInterpolator(this.r);
    if (paramAnimationListener != null) {
      this.t.a(paramAnimationListener);
    }
    this.t.clearAnimation();
    this.t.startAnimation(this.J);
  }
  
  private void b(Animation.AnimationListener paramAnimationListener)
  {
    this.y = new bo(this);
    this.y.setDuration(150L);
    this.t.a(paramAnimationListener);
    this.t.clearAnimation();
    this.t.startAnimation(this.y);
  }
  
  private void c(int paramInt, Animation.AnimationListener paramAnimationListener)
  {
    this.a = paramInt;
    if (c()) {}
    for (this.v = this.w.getAlpha();; this.v = bv.q(this.t))
    {
      this.B = new bt(this);
      this.B.setDuration(150L);
      if (paramAnimationListener != null) {
        this.t.a(paramAnimationListener);
      }
      this.t.clearAnimation();
      this.t.startAnimation(this.B);
      return;
    }
  }
  
  private boolean c()
  {
    return Build.VERSION.SDK_INT < 11;
  }
  
  private void d()
  {
    this.z = a(this.w.getAlpha(), 76);
  }
  
  private void e()
  {
    this.A = a(this.w.getAlpha(), 255);
  }
  
  private void f()
  {
    if (this.d == null) {}
    for (int i1 = 0;; i1++) {
      if (i1 < getChildCount())
      {
        View localView = getChildAt(i1);
        if (!localView.equals(this.t)) {
          this.d = localView;
        }
      }
      else
      {
        return;
      }
    }
  }
  
  private void setAnimationProgress(float paramFloat)
  {
    if (c())
    {
      setColorViewAlpha((int)(255.0F * paramFloat));
      return;
    }
    bv.d(this.t, paramFloat);
    bv.e(this.t, paramFloat);
  }
  
  private void setColorViewAlpha(int paramInt)
  {
    this.t.getBackground().setAlpha(paramInt);
    this.w.setAlpha(paramInt);
  }
  
  public void a(boolean paramBoolean, int paramInt1, int paramInt2)
  {
    this.p = paramBoolean;
    this.t.setVisibility(8);
    this.j = paramInt1;
    this.b = paramInt1;
    this.C = paramInt2;
    this.G = true;
    this.t.invalidate();
  }
  
  public boolean a()
  {
    if (Build.VERSION.SDK_INT < 14)
    {
      if ((this.d instanceof AbsListView))
      {
        AbsListView localAbsListView = (AbsListView)this.d;
        return (localAbsListView.getChildCount() > 0) && ((localAbsListView.getFirstVisiblePosition() > 0) || (localAbsListView.getChildAt(0).getTop() < localAbsListView.getPaddingTop()));
      }
      boolean bool;
      if (!bv.b(this.d, -1))
      {
        int i1 = this.d.getScrollY();
        bool = false;
        if (i1 <= 0) {}
      }
      else
      {
        bool = true;
      }
      return bool;
    }
    return bv.b(this.d, -1);
  }
  
  protected int getChildDrawingOrder(int paramInt1, int paramInt2)
  {
    if (this.u < 0) {}
    do
    {
      return paramInt2;
      if (paramInt2 == paramInt1 - 1) {
        return this.u;
      }
    } while (paramInt2 < this.u);
    return paramInt2 + 1;
  }
  
  public int getProgressCircleDiameter()
  {
    if (this.t != null) {
      return this.t.getMeasuredHeight();
    }
    return 0;
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    f();
    int i1 = ba.a(paramMotionEvent);
    if ((this.q) && (i1 == 0)) {
      this.q = false;
    }
    if ((!isEnabled()) || (this.q) || (a()) || (this.f)) {
      return false;
    }
    switch (i1)
    {
    }
    for (;;)
    {
      return this.n;
      a(this.b - this.t.getTop(), true);
      this.o = ba.b(paramMotionEvent, 0);
      this.n = false;
      float f2 = a(paramMotionEvent, this.o);
      if (f2 == -1.0F) {
        break;
      }
      this.m = f2;
      continue;
      if (this.o == -1)
      {
        Log.e(c, "Got ACTION_MOVE event but don't have an active pointer id.");
        return false;
      }
      float f1 = a(paramMotionEvent, this.o);
      if (f1 == -1.0F) {
        break;
      }
      if ((f1 - this.m > this.g) && (!this.n))
      {
        this.l = (this.m + this.g);
        this.n = true;
        this.w.setAlpha(76);
        continue;
        a(paramMotionEvent);
        continue;
        this.n = false;
        this.o = -1;
      }
    }
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i1 = getMeasuredWidth();
    int i2 = getMeasuredHeight();
    if (getChildCount() == 0) {}
    do
    {
      return;
      if (this.d == null) {
        f();
      }
    } while (this.d == null);
    View localView = this.d;
    int i3 = getPaddingLeft();
    int i4 = getPaddingTop();
    int i5 = i1 - getPaddingLeft() - getPaddingRight();
    int i6 = i2 - getPaddingTop() - getPaddingBottom();
    localView.layout(i3, i4, i5 + i3, i6 + i4);
    int i7 = this.t.getMeasuredWidth();
    int i8 = this.t.getMeasuredHeight();
    this.t.layout(i1 / 2 - i7 / 2, this.j, i1 / 2 + i7 / 2, i8 + this.j);
  }
  
  public void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    if (this.d == null) {
      f();
    }
    if (this.d == null) {}
    for (;;)
    {
      return;
      this.d.measure(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth() - getPaddingLeft() - getPaddingRight(), 1073741824), View.MeasureSpec.makeMeasureSpec(getMeasuredHeight() - getPaddingTop() - getPaddingBottom(), 1073741824));
      this.t.measure(View.MeasureSpec.makeMeasureSpec(this.E, 1073741824), View.MeasureSpec.makeMeasureSpec(this.F, 1073741824));
      if ((!this.G) && (!this.k))
      {
        this.k = true;
        int i2 = -this.t.getMeasuredHeight();
        this.b = i2;
        this.j = i2;
      }
      this.u = -1;
      for (int i1 = 0; i1 < getChildCount(); i1++) {
        if (getChildAt(i1) == this.t)
        {
          this.u = i1;
          return;
        }
      }
    }
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    int i1 = ba.a(paramMotionEvent);
    if ((this.q) && (i1 == 0)) {
      this.q = false;
    }
    if ((!isEnabled()) || (this.q) || (a())) {
      return false;
    }
    switch (i1)
    {
    case 4: 
    default: 
    case 0: 
    case 2: 
    case 5: 
    case 6: 
      for (;;)
      {
        return true;
        this.o = ba.b(paramMotionEvent, 0);
        this.n = false;
        continue;
        int i2 = ba.a(paramMotionEvent, this.o);
        if (i2 < 0)
        {
          Log.e(c, "Got ACTION_MOVE event but have an invalid active pointer id.");
          return false;
        }
        float f2 = 0.5F * (ba.d(paramMotionEvent, i2) - this.l);
        if (this.n)
        {
          this.w.a(true);
          float f3 = f2 / this.h;
          if (f3 < 0.0F) {
            return false;
          }
          float f4 = Math.min(1.0F, Math.abs(f3));
          float f5 = 5.0F * (float)Math.max(f4 - 0.4D, 0.0D) / 3.0F;
          float f6 = Math.abs(f2) - this.h;
          float f7;
          label248:
          float f9;
          int i3;
          if (this.G)
          {
            f7 = this.C - this.b;
            float f8 = Math.max(0.0F, Math.min(f6, 2.0F * f7) / f7);
            f9 = 2.0F * (float)(f8 / 4.0F - Math.pow(f8 / 4.0F, 2.0D));
            float f10 = 2.0F * (f7 * f9);
            i3 = this.b + (int)(f10 + f7 * f4);
            if (this.t.getVisibility() != 0) {
              this.t.setVisibility(0);
            }
            if (!this.p)
            {
              bv.d(this.t, 1.0F);
              bv.e(this.t, 1.0F);
            }
            if (f2 >= this.h) {
              break label504;
            }
            if (this.p) {
              setAnimationProgress(f2 / this.h);
            }
            if ((this.w.getAlpha() > 76) && (!a(this.z))) {
              d();
            }
            float f12 = 0.8F * f5;
            this.w.a(0.0F, Math.min(0.8F, f12));
            this.w.a(Math.min(1.0F, f5));
          }
          for (;;)
          {
            float f11 = 0.5F * (-0.25F + 0.4F * f5 + 2.0F * f9);
            this.w.b(f11);
            a(i3 - this.j, true);
            break;
            f7 = this.C;
            break label248;
            label504:
            if ((this.w.getAlpha() < 255) && (!a(this.A))) {
              e();
            }
          }
          this.o = ba.b(paramMotionEvent, ba.b(paramMotionEvent));
          continue;
          a(paramMotionEvent);
        }
      }
    }
    if (this.o == -1)
    {
      if (i1 == 1) {
        Log.e(c, "Got ACTION_UP event but don't have an active pointer id.");
      }
      return false;
    }
    float f1 = 0.5F * (ba.d(paramMotionEvent, ba.a(paramMotionEvent, this.o)) - this.l);
    this.n = false;
    if (f1 > this.h) {
      a(true, true);
    }
    for (;;)
    {
      this.o = -1;
      return false;
      this.f = false;
      this.w.a(0.0F, 0.0F);
      boolean bool = this.p;
      bq localbq = null;
      if (!bool) {
        localbq = new bq(this);
      }
      b(this.j, localbq);
      this.w.a(false);
    }
  }
  
  public void requestDisallowInterceptTouchEvent(boolean paramBoolean) {}
  
  @Deprecated
  public void setColorScheme(int... paramVarArgs)
  {
    setColorSchemeResources(paramVarArgs);
  }
  
  public void setColorSchemeColors(int... paramVarArgs)
  {
    f();
    this.w.a(paramVarArgs);
  }
  
  public void setColorSchemeResources(int... paramVarArgs)
  {
    Resources localResources = getResources();
    int[] arrayOfInt = new int[paramVarArgs.length];
    for (int i1 = 0; i1 < paramVarArgs.length; i1++) {
      arrayOfInt[i1] = localResources.getColor(paramVarArgs[i1]);
    }
    setColorSchemeColors(arrayOfInt);
  }
  
  public void setDistanceToTriggerSync(int paramInt)
  {
    this.h = paramInt;
  }
  
  public void setOnRefreshListener(bu parambu)
  {
    this.e = parambu;
  }
  
  @Deprecated
  public void setProgressBackgroundColor(int paramInt)
  {
    setProgressBackgroundColorSchemeResource(paramInt);
  }
  
  public void setProgressBackgroundColorSchemeColor(int paramInt)
  {
    this.t.setBackgroundColor(paramInt);
    this.w.b(paramInt);
  }
  
  public void setProgressBackgroundColorSchemeResource(int paramInt)
  {
    setProgressBackgroundColorSchemeColor(getResources().getColor(paramInt));
  }
  
  public void setRefreshing(boolean paramBoolean)
  {
    if ((paramBoolean) && (this.f != paramBoolean))
    {
      this.f = paramBoolean;
      if (!this.G) {}
      for (int i1 = (int)(this.C + this.b);; i1 = (int)this.C)
      {
        a(i1 - this.j, true);
        this.D = false;
        a(this.H);
        return;
      }
    }
    a(paramBoolean, false);
  }
  
  public void setSize(int paramInt)
  {
    if ((paramInt != 0) && (paramInt != 1)) {
      return;
    }
    DisplayMetrics localDisplayMetrics = getResources().getDisplayMetrics();
    int i2;
    if (paramInt == 0)
    {
      i2 = (int)(56.0F * localDisplayMetrics.density);
      this.E = i2;
    }
    int i1;
    for (this.F = i2;; this.F = i1)
    {
      this.t.setImageDrawable(null);
      this.w.a(paramInt);
      this.t.setImageDrawable(this.w);
      return;
      i1 = (int)(40.0F * localDisplayMetrics.density);
      this.E = i1;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/widget/SwipeRefreshLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */