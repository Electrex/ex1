package android.support.v4.widget;

import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

class aj
  implements Animation.AnimationListener
{
  aj(ah paramah, am paramam) {}
  
  public void onAnimationEnd(Animation paramAnimation) {}
  
  public void onAnimationRepeat(Animation paramAnimation)
  {
    this.a.j();
    this.a.a();
    this.a.b(this.a.g());
    if (this.b.a)
    {
      this.b.a = false;
      paramAnimation.setDuration(1333L);
      this.a.a(false);
      return;
    }
    ah.a(this.b, (1.0F + ah.a(this.b)) % 5.0F);
  }
  
  public void onAnimationStart(Animation paramAnimation)
  {
    ah.a(this.b, 0.0F);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/widget/aj.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */