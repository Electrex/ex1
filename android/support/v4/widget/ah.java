package android.support.v4.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.Callback;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import java.util.ArrayList;

class ah
  extends Drawable
  implements Animatable
{
  private static final Interpolator b = new LinearInterpolator();
  private static final Interpolator c = new al(null);
  private static final Interpolator d = new an(null);
  private static final Interpolator e = new AccelerateDecelerateInterpolator();
  boolean a;
  private final int[] f = { -16777216 };
  private final ArrayList g = new ArrayList();
  private final am h;
  private float i;
  private Resources j;
  private View k;
  private Animation l;
  private float m;
  private double n;
  private double o;
  private final Drawable.Callback p = new ak(this);
  
  public ah(Context paramContext, View paramView)
  {
    this.k = paramView;
    this.j = paramContext.getResources();
    this.h = new am(this.p);
    this.h.a(this.f);
    a(1);
    c();
  }
  
  private void a(double paramDouble1, double paramDouble2, double paramDouble3, double paramDouble4, float paramFloat1, float paramFloat2)
  {
    am localam = this.h;
    float f1 = this.j.getDisplayMetrics().density;
    this.n = (paramDouble1 * f1);
    this.o = (paramDouble2 * f1);
    localam.a(f1 * (float)paramDouble4);
    localam.a(paramDouble3 * f1);
    localam.b(0);
    localam.a(paramFloat1 * f1, f1 * paramFloat2);
    localam.a((int)this.n, (int)this.o);
  }
  
  private void a(float paramFloat, am paramam)
  {
    float f1 = (float)(1.0D + Math.floor(paramam.i() / 0.8F));
    paramam.b(paramam.e() + paramFloat * (paramam.f() - paramam.e()));
    paramam.d(paramam.i() + paramFloat * (f1 - paramam.i()));
  }
  
  private void c()
  {
    am localam = this.h;
    ai localai = new ai(this, localam);
    localai.setRepeatCount(-1);
    localai.setRepeatMode(1);
    localai.setInterpolator(b);
    localai.setAnimationListener(new aj(this, localam));
    this.l = localai;
  }
  
  public void a(float paramFloat)
  {
    this.h.e(paramFloat);
  }
  
  public void a(float paramFloat1, float paramFloat2)
  {
    this.h.b(paramFloat1);
    this.h.c(paramFloat2);
  }
  
  public void a(int paramInt)
  {
    if (paramInt == 0)
    {
      a(56.0D, 56.0D, 12.5D, 3.0D, 12.0F, 6.0F);
      return;
    }
    a(40.0D, 40.0D, 8.75D, 2.5D, 10.0F, 5.0F);
  }
  
  public void a(boolean paramBoolean)
  {
    this.h.a(paramBoolean);
  }
  
  public void a(int... paramVarArgs)
  {
    this.h.a(paramVarArgs);
    this.h.b(0);
  }
  
  public void b(float paramFloat)
  {
    this.h.d(paramFloat);
  }
  
  public void b(int paramInt)
  {
    this.h.a(paramInt);
  }
  
  void c(float paramFloat)
  {
    this.i = paramFloat;
    invalidateSelf();
  }
  
  public void draw(Canvas paramCanvas)
  {
    Rect localRect = getBounds();
    int i1 = paramCanvas.save();
    paramCanvas.rotate(this.i, localRect.exactCenterX(), localRect.exactCenterY());
    this.h.a(paramCanvas, localRect);
    paramCanvas.restoreToCount(i1);
  }
  
  public int getAlpha()
  {
    return this.h.b();
  }
  
  public int getIntrinsicHeight()
  {
    return (int)this.o;
  }
  
  public int getIntrinsicWidth()
  {
    return (int)this.n;
  }
  
  public int getOpacity()
  {
    return -3;
  }
  
  public boolean isRunning()
  {
    ArrayList localArrayList = this.g;
    int i1 = localArrayList.size();
    for (int i2 = 0; i2 < i1; i2++)
    {
      Animation localAnimation = (Animation)localArrayList.get(i2);
      if ((localAnimation.hasStarted()) && (!localAnimation.hasEnded())) {
        return true;
      }
    }
    return false;
  }
  
  public void setAlpha(int paramInt)
  {
    this.h.c(paramInt);
  }
  
  public void setColorFilter(ColorFilter paramColorFilter)
  {
    this.h.a(paramColorFilter);
  }
  
  public void start()
  {
    this.l.reset();
    this.h.j();
    if (this.h.g() != this.h.d())
    {
      this.a = true;
      this.l.setDuration(666L);
      this.k.startAnimation(this.l);
      return;
    }
    this.h.b(0);
    this.h.k();
    this.l.setDuration(1333L);
    this.k.startAnimation(this.l);
  }
  
  public void stop()
  {
    this.k.clearAnimation();
    c(0.0F);
    this.h.a(false);
    this.h.b(0);
    this.h.k();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/widget/ah.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */