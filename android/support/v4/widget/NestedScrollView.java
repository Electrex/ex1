package android.support.v4.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Parcelable;
import android.support.v4.view.ba;
import android.support.v4.view.bj;
import android.support.v4.view.bk;
import android.support.v4.view.bl;
import android.support.v4.view.bm;
import android.support.v4.view.bq;
import android.support.v4.view.bv;
import android.util.Log;
import android.util.TypedValue;
import android.view.FocusFinder;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import java.util.ArrayList;
import java.util.List;

public class NestedScrollView
  extends FrameLayout
  implements bj, bl
{
  private static final ao v = new ao();
  private static final int[] w = { 16843130 };
  private long a;
  private final Rect b;
  private aw c;
  private z d;
  private z e;
  private int f;
  private boolean g;
  private boolean h;
  private View i;
  private boolean j;
  private VelocityTracker k;
  private boolean l;
  private boolean m;
  private int n;
  private int o;
  private int p;
  private int q;
  private final int[] r;
  private final int[] s;
  private int t;
  private NestedScrollView.SavedState u;
  private final bm x;
  private final bk y;
  private float z;
  
  private View a(boolean paramBoolean, int paramInt1, int paramInt2)
  {
    ArrayList localArrayList = getFocusables(2);
    Object localObject1 = null;
    int i1 = 0;
    int i2 = localArrayList.size();
    int i3 = 0;
    View localView;
    int i4;
    int i5;
    int i7;
    label87:
    Object localObject2;
    int i6;
    if (i3 < i2)
    {
      localView = (View)localArrayList.get(i3);
      i4 = localView.getTop();
      i5 = localView.getBottom();
      if ((paramInt1 >= i5) || (i4 >= paramInt2)) {
        break label221;
      }
      if ((paramInt1 < i4) && (i5 < paramInt2))
      {
        i7 = 1;
        if (localObject1 != null) {
          break label124;
        }
        int i9 = i7;
        localObject2 = localView;
        i6 = i9;
      }
    }
    for (;;)
    {
      i3++;
      localObject1 = localObject2;
      i1 = i6;
      break;
      i7 = 0;
      break label87;
      label124:
      if (((paramBoolean) && (i4 < ((View)localObject1).getTop())) || ((!paramBoolean) && (i5 > ((View)localObject1).getBottom()))) {}
      for (int i8 = 1;; i8 = 0)
      {
        if (i1 == 0) {
          break label187;
        }
        if ((i7 == 0) || (i8 == 0)) {
          break label221;
        }
        localObject2 = localView;
        i6 = i1;
        break;
      }
      label187:
      if (i7 != 0)
      {
        localObject2 = localView;
        i6 = 1;
      }
      else if (i8 != 0)
      {
        localObject2 = localView;
        i6 = i1;
        continue;
        return (View)localObject1;
      }
      else
      {
        label221:
        i6 = i1;
        localObject2 = localObject1;
      }
    }
  }
  
  private void a(MotionEvent paramMotionEvent)
  {
    int i1 = (0xFF00 & paramMotionEvent.getAction()) >> 8;
    if (ba.b(paramMotionEvent, i1) == this.q) {
      if (i1 != 0) {
        break label63;
      }
    }
    label63:
    for (int i2 = 1;; i2 = 0)
    {
      this.f = ((int)ba.d(paramMotionEvent, i2));
      this.q = ba.b(paramMotionEvent, i2);
      if (this.k != null) {
        this.k.clear();
      }
      return;
    }
  }
  
  private boolean a()
  {
    View localView = getChildAt(0);
    boolean bool = false;
    if (localView != null)
    {
      int i1 = localView.getHeight();
      int i2 = getHeight();
      int i3 = i1 + getPaddingTop() + getPaddingBottom();
      bool = false;
      if (i2 < i3) {
        bool = true;
      }
    }
    return bool;
  }
  
  private boolean a(int paramInt1, int paramInt2, int paramInt3)
  {
    int i1 = getHeight();
    int i2 = getScrollY();
    int i3 = i2 + i1;
    if (paramInt1 == 33) {}
    boolean bool2;
    for (boolean bool1 = true;; bool1 = false)
    {
      Object localObject = a(bool1, paramInt2, paramInt3);
      if (localObject == null) {
        localObject = this;
      }
      if (paramInt2 < i2) {
        break;
      }
      bool2 = false;
      if (paramInt3 > i3) {
        break;
      }
      if (localObject != findFocus()) {
        ((View)localObject).requestFocus(paramInt1);
      }
      return bool2;
    }
    if (bool1) {}
    for (int i4 = paramInt2 - i2;; i4 = paramInt3 - i3)
    {
      e(i4);
      bool2 = true;
      break;
    }
  }
  
  private boolean a(Rect paramRect, boolean paramBoolean)
  {
    int i1 = a(paramRect);
    if (i1 != 0) {}
    for (boolean bool = true;; bool = false)
    {
      if (bool)
      {
        if (!paramBoolean) {
          break;
        }
        scrollBy(0, i1);
      }
      return bool;
    }
    a(0, i1);
    return bool;
  }
  
  private boolean a(View paramView)
  {
    boolean bool1 = a(paramView, 0, getHeight());
    boolean bool2 = false;
    if (!bool1) {
      bool2 = true;
    }
    return bool2;
  }
  
  private boolean a(View paramView, int paramInt1, int paramInt2)
  {
    paramView.getDrawingRect(this.b);
    offsetDescendantRectToMyCoords(paramView, this.b);
    return (paramInt1 + this.b.bottom >= getScrollY()) && (this.b.top - paramInt1 <= paramInt2 + getScrollY());
  }
  
  private static boolean a(View paramView1, View paramView2)
  {
    if (paramView1 == paramView2) {
      return true;
    }
    ViewParent localViewParent = paramView1.getParent();
    if (((localViewParent instanceof ViewGroup)) && (a((View)localViewParent, paramView2))) {}
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
  
  private static int b(int paramInt1, int paramInt2, int paramInt3)
  {
    if ((paramInt2 >= paramInt3) || (paramInt1 < 0)) {
      paramInt1 = 0;
    }
    while (paramInt2 + paramInt1 <= paramInt3) {
      return paramInt1;
    }
    return paramInt3 - paramInt2;
  }
  
  private void b()
  {
    if (this.k == null)
    {
      this.k = VelocityTracker.obtain();
      return;
    }
    this.k.clear();
  }
  
  private void b(View paramView)
  {
    paramView.getDrawingRect(this.b);
    offsetDescendantRectToMyCoords(paramView, this.b);
    int i1 = a(this.b);
    if (i1 != 0) {
      scrollBy(0, i1);
    }
  }
  
  private void c()
  {
    if (this.k == null) {
      this.k = VelocityTracker.obtain();
    }
  }
  
  private boolean c(int paramInt1, int paramInt2)
  {
    int i1 = getChildCount();
    boolean bool = false;
    if (i1 > 0)
    {
      int i2 = getScrollY();
      View localView = getChildAt(0);
      int i3 = localView.getTop() - i2;
      bool = false;
      if (paramInt2 >= i3)
      {
        int i4 = localView.getBottom() - i2;
        bool = false;
        if (paramInt2 < i4)
        {
          int i5 = localView.getLeft();
          bool = false;
          if (paramInt1 >= i5)
          {
            int i6 = localView.getRight();
            bool = false;
            if (paramInt1 < i6) {
              bool = true;
            }
          }
        }
      }
    }
    return bool;
  }
  
  private void d()
  {
    if (this.k != null)
    {
      this.k.recycle();
      this.k = null;
    }
  }
  
  private void e()
  {
    this.j = false;
    d();
    if (this.d != null)
    {
      this.d.c();
      this.e.c();
    }
  }
  
  private void e(int paramInt)
  {
    if (paramInt != 0)
    {
      if (this.m) {
        a(0, paramInt);
      }
    }
    else {
      return;
    }
    scrollBy(0, paramInt);
  }
  
  private void f()
  {
    if (bv.a(this) != 2)
    {
      if (this.d == null)
      {
        Context localContext = getContext();
        this.d = new z(localContext);
        this.e = new z(localContext);
      }
      return;
    }
    this.d = null;
    this.e = null;
  }
  
  private void f(int paramInt)
  {
    int i1 = getScrollY();
    if (((i1 > 0) || (paramInt > 0)) && ((i1 < getScrollRange()) || (paramInt < 0))) {}
    for (boolean bool = true;; bool = false)
    {
      if (!dispatchNestedPreFling(0.0F, paramInt))
      {
        dispatchNestedFling(0.0F, paramInt, bool);
        if (bool) {
          d(paramInt);
        }
      }
      return;
    }
  }
  
  private int getScrollRange()
  {
    int i1 = getChildCount();
    int i2 = 0;
    if (i1 > 0) {
      i2 = Math.max(0, getChildAt(0).getHeight() - (getHeight() - getPaddingBottom() - getPaddingTop()));
    }
    return i2;
  }
  
  private float getVerticalScrollFactorCompat()
  {
    if (this.z == 0.0F)
    {
      TypedValue localTypedValue = new TypedValue();
      Context localContext = getContext();
      if (!localContext.getTheme().resolveAttribute(16842829, localTypedValue, true)) {
        throw new IllegalStateException("Expected theme to define listPreferredItemHeight.");
      }
      this.z = localTypedValue.getDimension(localContext.getResources().getDisplayMetrics());
    }
    return this.z;
  }
  
  protected int a(Rect paramRect)
  {
    if (getChildCount() == 0) {
      return 0;
    }
    int i1 = getHeight();
    int i2 = getScrollY();
    int i3 = i2 + i1;
    int i4 = getVerticalFadingEdgeLength();
    if (paramRect.top > 0) {
      i2 += i4;
    }
    if (paramRect.bottom < getChildAt(0).getHeight()) {
      i3 -= i4;
    }
    int i7;
    int i5;
    if ((paramRect.bottom > i3) && (paramRect.top > i2)) {
      if (paramRect.height() > i1)
      {
        i7 = 0 + (paramRect.top - i2);
        i5 = Math.min(i7, getChildAt(0).getBottom() - i3);
      }
    }
    for (;;)
    {
      return i5;
      i7 = 0 + (paramRect.bottom - i3);
      break;
      if ((paramRect.top < i2) && (paramRect.bottom < i3))
      {
        if (paramRect.height() > i1) {}
        for (int i6 = 0 - (i3 - paramRect.bottom);; i6 = 0 - (i2 - paramRect.top))
        {
          i5 = Math.max(i6, -getScrollY());
          break;
        }
      }
      i5 = 0;
    }
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    if (getChildCount() == 0) {
      return;
    }
    if (AnimationUtils.currentAnimationTimeMillis() - this.a > 250L)
    {
      int i1 = getHeight() - getPaddingBottom() - getPaddingTop();
      int i2 = Math.max(0, getChildAt(0).getHeight() - i1);
      int i3 = getScrollY();
      int i4 = Math.max(0, Math.min(i3 + paramInt2, i2)) - i3;
      this.c.a(getScrollX(), i3, 0, i4);
      bv.d(this);
    }
    for (;;)
    {
      this.a = AnimationUtils.currentAnimationTimeMillis();
      return;
      if (!this.c.a()) {
        this.c.h();
      }
      scrollBy(paramInt1, paramInt2);
    }
  }
  
  public boolean a(int paramInt)
  {
    int i1;
    int i2;
    if (paramInt == 130)
    {
      i1 = 1;
      i2 = getHeight();
      if (i1 == 0) {
        break label124;
      }
      this.b.top = (i2 + getScrollY());
      int i3 = getChildCount();
      if (i3 > 0)
      {
        View localView = getChildAt(i3 - 1);
        if (i2 + this.b.top > localView.getBottom()) {
          this.b.top = (localView.getBottom() - i2);
        }
      }
    }
    for (;;)
    {
      this.b.bottom = (i2 + this.b.top);
      return a(paramInt, this.b.top, this.b.bottom);
      i1 = 0;
      break;
      label124:
      this.b.top = (getScrollY() - i2);
      if (this.b.top < 0) {
        this.b.top = 0;
      }
    }
  }
  
  boolean a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, boolean paramBoolean)
  {
    int i1 = bv.a(this);
    int i2;
    int i3;
    label34:
    int i4;
    label53:
    int i5;
    label72:
    int i6;
    int i7;
    int i8;
    int i10;
    int i11;
    boolean bool1;
    if (computeHorizontalScrollRange() > computeHorizontalScrollExtent())
    {
      i2 = 1;
      if (computeVerticalScrollRange() <= computeVerticalScrollExtent()) {
        break label177;
      }
      i3 = 1;
      if ((i1 != 0) && ((i1 != 1) || (i2 == 0))) {
        break label183;
      }
      i4 = 1;
      if ((i1 != 0) && ((i1 != 1) || (i3 == 0))) {
        break label189;
      }
      i5 = 1;
      i6 = paramInt3 + paramInt1;
      if (i4 == 0) {
        paramInt7 = 0;
      }
      i7 = paramInt4 + paramInt2;
      if (i5 == 0) {
        paramInt8 = 0;
      }
      i8 = -paramInt7;
      int i9 = paramInt7 + paramInt5;
      i10 = -paramInt8;
      i11 = paramInt8 + paramInt6;
      if (i6 <= i9) {
        break label195;
      }
      i8 = i9;
      bool1 = true;
    }
    for (;;)
    {
      label137:
      boolean bool2;
      if (i7 > i11) {
        bool2 = true;
      }
      for (;;)
      {
        onOverScrolled(i8, i11, bool1, bool2);
        if ((bool1) || (bool2))
        {
          return true;
          i2 = 0;
          break;
          label177:
          i3 = 0;
          break label34;
          label183:
          i4 = 0;
          break label53;
          label189:
          i5 = 0;
          break label72;
          label195:
          if (i6 >= i8) {
            break label237;
          }
          bool1 = true;
          break label137;
          if (i7 >= i10) {
            break label227;
          }
          bool2 = true;
          i11 = i10;
          continue;
        }
        return false;
        label227:
        i11 = i7;
        bool2 = false;
      }
      label237:
      i8 = i6;
      bool1 = false;
    }
  }
  
  public boolean a(KeyEvent paramKeyEvent)
  {
    int i1 = 33;
    this.b.setEmpty();
    boolean bool1;
    boolean bool3;
    if (!a())
    {
      boolean bool2 = isFocused();
      bool1 = false;
      if (bool2)
      {
        int i3 = paramKeyEvent.getKeyCode();
        bool1 = false;
        if (i3 != 4)
        {
          View localView1 = findFocus();
          if (localView1 == this) {
            localView1 = null;
          }
          View localView2 = FocusFinder.getInstance().findNextFocus(this, localView1, 130);
          if ((localView2 == null) || (localView2 == this) || (!localView2.requestFocus(130))) {
            break label107;
          }
          bool3 = true;
          bool1 = bool3;
        }
      }
    }
    label107:
    int i2;
    do
    {
      return bool1;
      bool3 = false;
      break;
      i2 = paramKeyEvent.getAction();
      bool1 = false;
    } while (i2 != 0);
    switch (paramKeyEvent.getKeyCode())
    {
    default: 
      return false;
    case 19: 
      if (!paramKeyEvent.isAltPressed()) {
        return c(i1);
      }
      return b(i1);
    case 20: 
      if (!paramKeyEvent.isAltPressed()) {
        return c(130);
      }
      return b(130);
    }
    if (paramKeyEvent.isShiftPressed()) {}
    for (;;)
    {
      a(i1);
      return false;
      i1 = 130;
    }
  }
  
  public void addView(View paramView)
  {
    if (getChildCount() > 0) {
      throw new IllegalStateException("ScrollView can host only one direct child");
    }
    super.addView(paramView);
  }
  
  public void addView(View paramView, int paramInt)
  {
    if (getChildCount() > 0) {
      throw new IllegalStateException("ScrollView can host only one direct child");
    }
    super.addView(paramView, paramInt);
  }
  
  public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams)
  {
    if (getChildCount() > 0) {
      throw new IllegalStateException("ScrollView can host only one direct child");
    }
    super.addView(paramView, paramInt, paramLayoutParams);
  }
  
  public void addView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
  {
    if (getChildCount() > 0) {
      throw new IllegalStateException("ScrollView can host only one direct child");
    }
    super.addView(paramView, paramLayoutParams);
  }
  
  public final void b(int paramInt1, int paramInt2)
  {
    a(paramInt1 - getScrollX(), paramInt2 - getScrollY());
  }
  
  public boolean b(int paramInt)
  {
    if (paramInt == 130) {}
    for (int i1 = 1;; i1 = 0)
    {
      int i2 = getHeight();
      this.b.top = 0;
      this.b.bottom = i2;
      if (i1 != 0)
      {
        int i3 = getChildCount();
        if (i3 > 0)
        {
          View localView = getChildAt(i3 - 1);
          this.b.bottom = (localView.getBottom() + getPaddingBottom());
          this.b.top = (this.b.bottom - i2);
        }
      }
      return a(paramInt, this.b.top, this.b.bottom);
    }
  }
  
  public boolean c(int paramInt)
  {
    View localView1 = findFocus();
    if (localView1 == this) {
      localView1 = null;
    }
    View localView2 = FocusFinder.getInstance().findNextFocus(this, localView1, paramInt);
    int i1 = getMaxScrollAmount();
    if ((localView2 != null) && (a(localView2, i1, getHeight())))
    {
      localView2.getDrawingRect(this.b);
      offsetDescendantRectToMyCoords(localView2, this.b);
      e(a(this.b));
      localView2.requestFocus(paramInt);
      if ((localView1 != null) && (localView1.isFocused()) && (a(localView1)))
      {
        int i4 = getDescendantFocusability();
        setDescendantFocusability(131072);
        requestFocus();
        setDescendantFocusability(i4);
      }
      return true;
    }
    if ((paramInt == 33) && (getScrollY() < i1)) {
      i1 = getScrollY();
    }
    while (i1 == 0)
    {
      return false;
      if ((paramInt == 130) && (getChildCount() > 0))
      {
        int i2 = getChildAt(0).getBottom();
        int i3 = getScrollY() + getHeight() - getPaddingBottom();
        if (i2 - i3 < i1) {
          i1 = i2 - i3;
        }
      }
    }
    if (paramInt == 130) {}
    for (;;)
    {
      e(i1);
      break;
      i1 = -i1;
    }
  }
  
  public void computeScroll()
  {
    int i2;
    int i4;
    int i5;
    int i7;
    if (this.c.g())
    {
      int i1 = getScrollX();
      i2 = getScrollY();
      int i3 = this.c.b();
      i4 = this.c.c();
      if ((i1 != i3) || (i2 != i4))
      {
        i5 = getScrollRange();
        int i6 = bv.a(this);
        if ((i6 != 0) && ((i6 != 1) || (i5 <= 0))) {
          break label134;
        }
        i7 = 1;
        a(i3 - i1, i4 - i2, i1, i2, 0, i5, 0, 0, false);
        if (i7 != 0)
        {
          f();
          if ((i4 > 0) || (i2 <= 0)) {
            break label140;
          }
          this.d.a((int)this.c.f());
        }
      }
    }
    label134:
    label140:
    while ((i4 < i5) || (i2 >= i5))
    {
      return;
      i7 = 0;
      break;
    }
    this.e.a((int)this.c.f());
  }
  
  protected int computeVerticalScrollOffset()
  {
    return Math.max(0, super.computeVerticalScrollOffset());
  }
  
  protected int computeVerticalScrollRange()
  {
    int i1 = getChildCount();
    int i2 = getHeight() - getPaddingBottom() - getPaddingTop();
    int i3;
    if (i1 == 0) {
      i3 = i2;
    }
    int i4;
    int i5;
    do
    {
      return i3;
      i3 = getChildAt(0).getBottom();
      i4 = getScrollY();
      i5 = Math.max(0, i3 - i2);
      if (i4 < 0) {
        return i3 - i4;
      }
    } while (i4 <= i5);
    return i3 + (i4 - i5);
  }
  
  public void d(int paramInt)
  {
    if (getChildCount() > 0)
    {
      int i1 = getHeight() - getPaddingBottom() - getPaddingTop();
      int i2 = getChildAt(0).getHeight();
      this.c.a(getScrollX(), getScrollY(), 0, paramInt, 0, 0, 0, Math.max(0, i2 - i1), 0, i1 / 2);
      bv.d(this);
    }
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
  {
    return (super.dispatchKeyEvent(paramKeyEvent)) || (a(paramKeyEvent));
  }
  
  public boolean dispatchNestedFling(float paramFloat1, float paramFloat2, boolean paramBoolean)
  {
    return this.y.a(paramFloat1, paramFloat2, paramBoolean);
  }
  
  public boolean dispatchNestedPreFling(float paramFloat1, float paramFloat2)
  {
    return this.y.a(paramFloat1, paramFloat2);
  }
  
  public boolean dispatchNestedPreScroll(int paramInt1, int paramInt2, int[] paramArrayOfInt1, int[] paramArrayOfInt2)
  {
    return this.y.a(paramInt1, paramInt2, paramArrayOfInt1, paramArrayOfInt2);
  }
  
  public boolean dispatchNestedScroll(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfInt)
  {
    return this.y.a(paramInt1, paramInt2, paramInt3, paramInt4, paramArrayOfInt);
  }
  
  public void draw(Canvas paramCanvas)
  {
    super.draw(paramCanvas);
    if (this.d != null)
    {
      int i1 = getScrollY();
      if (!this.d.a())
      {
        int i5 = paramCanvas.save();
        int i6 = getWidth() - getPaddingLeft() - getPaddingRight();
        paramCanvas.translate(getPaddingLeft(), Math.min(0, i1));
        this.d.a(i6, getHeight());
        if (this.d.a(paramCanvas)) {
          bv.d(this);
        }
        paramCanvas.restoreToCount(i5);
      }
      if (!this.e.a())
      {
        int i2 = paramCanvas.save();
        int i3 = getWidth() - getPaddingLeft() - getPaddingRight();
        int i4 = getHeight();
        paramCanvas.translate(-i3 + getPaddingLeft(), i4 + Math.max(getScrollRange(), i1));
        paramCanvas.rotate(180.0F, i3, 0.0F);
        this.e.a(i3, i4);
        if (this.e.a(paramCanvas)) {
          bv.d(this);
        }
        paramCanvas.restoreToCount(i2);
      }
    }
  }
  
  protected float getBottomFadingEdgeStrength()
  {
    if (getChildCount() == 0) {
      return 0.0F;
    }
    int i1 = getVerticalFadingEdgeLength();
    int i2 = getHeight() - getPaddingBottom();
    int i3 = getChildAt(0).getBottom() - getScrollY() - i2;
    if (i3 < i1) {
      return i3 / i1;
    }
    return 1.0F;
  }
  
  public int getMaxScrollAmount()
  {
    return (int)(0.5F * getHeight());
  }
  
  public int getNestedScrollAxes()
  {
    return this.x.a();
  }
  
  protected float getTopFadingEdgeStrength()
  {
    if (getChildCount() == 0) {
      return 0.0F;
    }
    int i1 = getVerticalFadingEdgeLength();
    int i2 = getScrollY();
    if (i2 < i1) {
      return i2 / i1;
    }
    return 1.0F;
  }
  
  public boolean hasNestedScrollingParent()
  {
    return this.y.b();
  }
  
  public boolean isNestedScrollingEnabled()
  {
    return this.y.a();
  }
  
  protected void measureChild(View paramView, int paramInt1, int paramInt2)
  {
    ViewGroup.LayoutParams localLayoutParams = paramView.getLayoutParams();
    paramView.measure(getChildMeasureSpec(paramInt1, getPaddingLeft() + getPaddingRight(), localLayoutParams.width), View.MeasureSpec.makeMeasureSpec(0, 0));
  }
  
  protected void measureChildWithMargins(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)paramView.getLayoutParams();
    paramView.measure(getChildMeasureSpec(paramInt1, paramInt2 + (getPaddingLeft() + getPaddingRight() + localMarginLayoutParams.leftMargin + localMarginLayoutParams.rightMargin), localMarginLayoutParams.width), View.MeasureSpec.makeMeasureSpec(localMarginLayoutParams.topMargin + localMarginLayoutParams.bottomMargin, 0));
  }
  
  public void onAttachedToWindow()
  {
    this.h = false;
  }
  
  public boolean onGenericMotionEvent(MotionEvent paramMotionEvent)
  {
    if ((0x2 & ba.d(paramMotionEvent)) != 0) {
      switch (paramMotionEvent.getAction())
      {
      }
    }
    for (;;)
    {
      return false;
      if (!this.j)
      {
        float f1 = ba.e(paramMotionEvent, 9);
        if (f1 != 0.0F)
        {
          int i1 = (int)(f1 * getVerticalScrollFactorCompat());
          int i2 = getScrollRange();
          int i3 = getScrollY();
          int i4 = i3 - i1;
          if (i4 < 0) {
            i2 = 0;
          }
          while (i2 != i3)
          {
            super.scrollTo(getScrollX(), i2);
            return true;
            if (i4 <= i2) {
              i2 = i4;
            }
          }
        }
      }
    }
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    int i1 = 1;
    int i3 = paramMotionEvent.getAction();
    if ((i3 == 2) && (this.j)) {
      return i1;
    }
    if ((getScrollY() == 0) && (!bv.b(this, i1))) {
      return false;
    }
    switch (i3 & 0xFF)
    {
    }
    for (;;)
    {
      return this.j;
      int i5 = this.q;
      if (i5 != -1)
      {
        int i6 = ba.a(paramMotionEvent, i5);
        if (i6 == -1)
        {
          Log.e("NestedScrollView", "Invalid pointerId=" + i5 + " in onInterceptTouchEvent");
        }
        else
        {
          int i7 = (int)ba.d(paramMotionEvent, i6);
          if ((Math.abs(i7 - this.f) > this.n) && ((0x2 & getNestedScrollAxes()) == 0))
          {
            this.j = i1;
            this.f = i7;
            c();
            this.k.addMovement(paramMotionEvent);
            this.t = 0;
            ViewParent localViewParent = getParent();
            if (localViewParent != null)
            {
              localViewParent.requestDisallowInterceptTouchEvent(i1);
              continue;
              int i4 = (int)paramMotionEvent.getY();
              if (!c((int)paramMotionEvent.getX(), i4))
              {
                this.j = false;
                d();
              }
              else
              {
                this.f = i4;
                this.q = ba.b(paramMotionEvent, 0);
                b();
                this.k.addMovement(paramMotionEvent);
                if (!this.c.a()) {}
                for (;;)
                {
                  this.j = i1;
                  startNestedScroll(2);
                  break;
                  int i2 = 0;
                }
                this.j = false;
                this.q = -1;
                d();
                stopNestedScroll();
                continue;
                a(paramMotionEvent);
              }
            }
          }
        }
      }
    }
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    this.g = false;
    if ((this.i != null) && (a(this.i, this))) {
      b(this.i);
    }
    this.i = null;
    int i1;
    if (!this.h)
    {
      if (this.u != null)
      {
        scrollTo(getScrollX(), this.u.a);
        this.u = null;
      }
      if (getChildCount() <= 0) {
        break label158;
      }
      i1 = getChildAt(0).getMeasuredHeight();
      int i2 = Math.max(0, i1 - (paramInt4 - paramInt2 - getPaddingBottom() - getPaddingTop()));
      if (getScrollY() <= i2) {
        break label164;
      }
      scrollTo(getScrollX(), i2);
    }
    for (;;)
    {
      scrollTo(getScrollX(), getScrollY());
      this.h = true;
      return;
      label158:
      i1 = 0;
      break;
      label164:
      if (getScrollY() < 0) {
        scrollTo(getScrollX(), 0);
      }
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    if (!this.l) {}
    View localView;
    int i1;
    do
    {
      do
      {
        return;
      } while ((View.MeasureSpec.getMode(paramInt2) == 0) || (getChildCount() <= 0));
      localView = getChildAt(0);
      i1 = getMeasuredHeight();
    } while (localView.getMeasuredHeight() >= i1);
    FrameLayout.LayoutParams localLayoutParams = (FrameLayout.LayoutParams)localView.getLayoutParams();
    localView.measure(getChildMeasureSpec(paramInt1, getPaddingLeft() + getPaddingRight(), localLayoutParams.width), View.MeasureSpec.makeMeasureSpec(i1 - getPaddingTop() - getPaddingBottom(), 1073741824));
  }
  
  public boolean onNestedFling(View paramView, float paramFloat1, float paramFloat2, boolean paramBoolean)
  {
    if (!paramBoolean)
    {
      f((int)paramFloat2);
      return true;
    }
    return false;
  }
  
  public boolean onNestedPreFling(View paramView, float paramFloat1, float paramFloat2)
  {
    return false;
  }
  
  public void onNestedPreScroll(View paramView, int paramInt1, int paramInt2, int[] paramArrayOfInt) {}
  
  public void onNestedScroll(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i1 = getScrollY();
    scrollBy(0, paramInt4);
    int i2 = getScrollY() - i1;
    dispatchNestedScroll(0, i2, 0, paramInt4 - i2, null);
  }
  
  public void onNestedScrollAccepted(View paramView1, View paramView2, int paramInt)
  {
    this.x.a(paramView1, paramView2, paramInt);
    startNestedScroll(2);
  }
  
  protected void onOverScrolled(int paramInt1, int paramInt2, boolean paramBoolean1, boolean paramBoolean2)
  {
    super.scrollTo(paramInt1, paramInt2);
  }
  
  protected boolean onRequestFocusInDescendants(int paramInt, Rect paramRect)
  {
    View localView;
    if (paramInt == 2)
    {
      paramInt = 130;
      if (paramRect != null) {
        break label40;
      }
      localView = FocusFinder.getInstance().findNextFocus(this, null, paramInt);
      label23:
      if (localView != null) {
        break label53;
      }
    }
    label40:
    label53:
    while (a(localView))
    {
      return false;
      if (paramInt != 1) {
        break;
      }
      paramInt = 33;
      break;
      localView = FocusFinder.getInstance().findNextFocusFromRect(this, paramRect, paramInt);
      break label23;
    }
    return localView.requestFocus(paramInt, paramRect);
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    NestedScrollView.SavedState localSavedState = (NestedScrollView.SavedState)paramParcelable;
    super.onRestoreInstanceState(localSavedState.getSuperState());
    this.u = localSavedState;
    requestLayout();
  }
  
  protected Parcelable onSaveInstanceState()
  {
    NestedScrollView.SavedState localSavedState = new NestedScrollView.SavedState(super.onSaveInstanceState());
    localSavedState.a = getScrollY();
    return localSavedState;
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    View localView = findFocus();
    if ((localView == null) || (this == localView)) {}
    while (!a(localView, 0, paramInt4)) {
      return;
    }
    localView.getDrawingRect(this.b);
    offsetDescendantRectToMyCoords(localView, this.b);
    e(a(this.b));
  }
  
  public boolean onStartNestedScroll(View paramView1, View paramView2, int paramInt)
  {
    return (paramInt & 0x2) != 0;
  }
  
  public void onStopNestedScroll(View paramView)
  {
    stopNestedScroll();
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    c();
    MotionEvent localMotionEvent = MotionEvent.obtain(paramMotionEvent);
    int i1 = ba.a(paramMotionEvent);
    if (i1 == 0) {
      this.t = 0;
    }
    localMotionEvent.offsetLocation(0.0F, this.t);
    int i4;
    int i5;
    int i6;
    int i7;
    switch (i1)
    {
    case 4: 
    default: 
    case 0: 
    case 2: 
      for (;;)
      {
        if (this.k != null) {
          this.k.addMovement(localMotionEvent);
        }
        localMotionEvent.recycle();
        return true;
        if (getChildCount() == 0) {
          return false;
        }
        if (!this.c.a()) {}
        for (boolean bool = true;; bool = false)
        {
          this.j = bool;
          if (bool)
          {
            ViewParent localViewParent2 = getParent();
            if (localViewParent2 != null) {
              localViewParent2.requestDisallowInterceptTouchEvent(true);
            }
          }
          if (!this.c.a()) {
            this.c.h();
          }
          this.f = ((int)paramMotionEvent.getY());
          this.q = ba.b(paramMotionEvent, 0);
          startNestedScroll(2);
          break;
        }
        i4 = ba.a(paramMotionEvent, this.q);
        if (i4 != -1) {
          break;
        }
        Log.e("NestedScrollView", "Invalid pointerId=" + this.q + " in onTouchEvent");
      }
      i5 = (int)ba.d(paramMotionEvent, i4);
      i6 = this.f - i5;
      if (dispatchNestedPreScroll(0, i6, this.s, this.r))
      {
        i6 -= this.s[1];
        localMotionEvent.offsetLocation(0.0F, this.r[1]);
        this.t += this.r[1];
      }
      if ((!this.j) && (Math.abs(i6) > this.n))
      {
        ViewParent localViewParent1 = getParent();
        if (localViewParent1 != null) {
          localViewParent1.requestDisallowInterceptTouchEvent(true);
        }
        this.j = true;
        if (i6 > 0) {
          i7 = i6 - this.n;
        }
      }
      break;
    }
    label385:
    while (this.j)
    {
      this.f = (i5 - this.r[1]);
      int i8 = getScrollY();
      int i9 = getScrollRange();
      int i10 = bv.a(this);
      if ((i10 == 0) || ((i10 == 1) && (i9 > 0))) {}
      for (int i11 = 1;; i11 = 0)
      {
        if ((a(0, i7, 0, getScrollY(), 0, i9, 0, 0, true)) && (!hasNestedScrollingParent())) {
          this.k.clear();
        }
        int i12 = getScrollY() - i8;
        if (!dispatchNestedScroll(0, i12, 0, i7 - i12, this.r)) {
          break label569;
        }
        this.f -= this.r[1];
        localMotionEvent.offsetLocation(0.0F, this.r[1]);
        this.t += this.r[1];
        break;
        i7 = i6 + this.n;
        break label385;
      }
      label569:
      if (i11 == 0) {
        break;
      }
      f();
      int i13 = i8 + i7;
      if (i13 < 0)
      {
        this.d.a(i7 / getHeight(), ba.c(paramMotionEvent, i4) / getWidth());
        if (!this.e.a()) {
          this.e.c();
        }
      }
      while ((this.d != null) && ((!this.d.a()) || (!this.e.a())))
      {
        bv.d(this);
        break;
        if (i13 > i9)
        {
          this.e.a(i7 / getHeight(), 1.0F - ba.c(paramMotionEvent, i4) / getWidth());
          if (!this.d.a()) {
            this.d.c();
          }
        }
      }
      if (!this.j) {
        break;
      }
      VelocityTracker localVelocityTracker = this.k;
      localVelocityTracker.computeCurrentVelocity(1000, this.p);
      int i3 = (int)bq.b(localVelocityTracker, this.q);
      if (Math.abs(i3) > this.o) {
        f(-i3);
      }
      this.q = -1;
      e();
      break;
      if ((!this.j) || (getChildCount() <= 0)) {
        break;
      }
      this.q = -1;
      e();
      break;
      int i2 = ba.b(paramMotionEvent);
      this.f = ((int)ba.d(paramMotionEvent, i2));
      this.q = ba.b(paramMotionEvent, i2);
      break;
      a(paramMotionEvent);
      this.f = ((int)ba.d(paramMotionEvent, ba.a(paramMotionEvent, this.q)));
      break;
      i7 = i6;
    }
  }
  
  public void requestChildFocus(View paramView1, View paramView2)
  {
    if (!this.g) {
      b(paramView2);
    }
    for (;;)
    {
      super.requestChildFocus(paramView1, paramView2);
      return;
      this.i = paramView2;
    }
  }
  
  public boolean requestChildRectangleOnScreen(View paramView, Rect paramRect, boolean paramBoolean)
  {
    paramRect.offset(paramView.getLeft() - paramView.getScrollX(), paramView.getTop() - paramView.getScrollY());
    return a(paramRect, paramBoolean);
  }
  
  public void requestDisallowInterceptTouchEvent(boolean paramBoolean)
  {
    if (paramBoolean) {
      d();
    }
    super.requestDisallowInterceptTouchEvent(paramBoolean);
  }
  
  public void requestLayout()
  {
    this.g = true;
    super.requestLayout();
  }
  
  public void scrollTo(int paramInt1, int paramInt2)
  {
    if (getChildCount() > 0)
    {
      View localView = getChildAt(0);
      int i1 = b(paramInt1, getWidth() - getPaddingRight() - getPaddingLeft(), localView.getWidth());
      int i2 = b(paramInt2, getHeight() - getPaddingBottom() - getPaddingTop(), localView.getHeight());
      if ((i1 != getScrollX()) || (i2 != getScrollY())) {
        super.scrollTo(i1, i2);
      }
    }
  }
  
  public void setFillViewport(boolean paramBoolean)
  {
    if (paramBoolean != this.l)
    {
      this.l = paramBoolean;
      requestLayout();
    }
  }
  
  public void setNestedScrollingEnabled(boolean paramBoolean)
  {
    this.y.a(paramBoolean);
  }
  
  public void setSmoothScrollingEnabled(boolean paramBoolean)
  {
    this.m = paramBoolean;
  }
  
  public boolean shouldDelayChildPressedState()
  {
    return true;
  }
  
  public boolean startNestedScroll(int paramInt)
  {
    return this.y.a(paramInt);
  }
  
  public void stopNestedScroll()
  {
    this.y.c();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/widget/NestedScrollView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */