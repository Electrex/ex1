package android.support.v4.widget;

import android.graphics.Rect;
import android.support.v4.view.a;
import android.support.v4.view.a.g;
import android.support.v4.view.bv;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import java.util.List;

class m
  extends a
{
  private final Rect c = new Rect();
  
  m(DrawerLayout paramDrawerLayout) {}
  
  private void a(g paramg1, g paramg2)
  {
    Rect localRect = this.c;
    paramg2.a(localRect);
    paramg1.b(localRect);
    paramg2.c(localRect);
    paramg1.d(localRect);
    paramg1.c(paramg2.g());
    paramg1.a(paramg2.o());
    paramg1.b(paramg2.p());
    paramg1.c(paramg2.r());
    paramg1.h(paramg2.l());
    paramg1.f(paramg2.j());
    paramg1.a(paramg2.e());
    paramg1.b(paramg2.f());
    paramg1.d(paramg2.h());
    paramg1.e(paramg2.i());
    paramg1.g(paramg2.k());
    paramg1.a(paramg2.b());
  }
  
  private void a(g paramg, ViewGroup paramViewGroup)
  {
    int i = paramViewGroup.getChildCount();
    for (int j = 0; j < i; j++)
    {
      View localView = paramViewGroup.getChildAt(j);
      if (DrawerLayout.l(localView)) {
        paramg.b(localView);
      }
    }
  }
  
  public void a(View paramView, g paramg)
  {
    if (DrawerLayout.e()) {
      super.a(paramView, paramg);
    }
    for (;;)
    {
      paramg.b(DrawerLayout.class.getName());
      paramg.a(false);
      paramg.b(false);
      return;
      g localg = g.a(paramg);
      super.a(paramView, localg);
      paramg.a(paramView);
      ViewParent localViewParent = bv.i(paramView);
      if ((localViewParent instanceof View)) {
        paramg.c((View)localViewParent);
      }
      a(paramg, localg);
      localg.s();
      a(paramg, (ViewGroup)paramView);
    }
  }
  
  public boolean a(ViewGroup paramViewGroup, View paramView, AccessibilityEvent paramAccessibilityEvent)
  {
    if ((DrawerLayout.e()) || (DrawerLayout.l(paramView))) {
      return super.a(paramViewGroup, paramView, paramAccessibilityEvent);
    }
    return false;
  }
  
  public boolean b(View paramView, AccessibilityEvent paramAccessibilityEvent)
  {
    if (paramAccessibilityEvent.getEventType() == 32)
    {
      List localList = paramAccessibilityEvent.getText();
      View localView = DrawerLayout.a(this.b);
      if (localView != null)
      {
        int i = this.b.e(localView);
        CharSequence localCharSequence = this.b.a(i);
        if (localCharSequence != null) {
          localList.add(localCharSequence);
        }
      }
      return true;
    }
    return super.b(paramView, paramAccessibilityEvent);
  }
  
  public void d(View paramView, AccessibilityEvent paramAccessibilityEvent)
  {
    super.d(paramView, paramAccessibilityEvent);
    paramAccessibilityEvent.setClassName(DrawerLayout.class.getName());
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/android/support/v4/widget/m.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */