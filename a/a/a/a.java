package a.a.a;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.util.Log;
import android.view.Display;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import java.util.Date;

public class a
{
  private Activity a;
  private String b;
  private int c = 5;
  private n d = n.b;
  private f e;
  private SharedPreferences f;
  private SharedPreferences.Editor g;
  private int h = 0;
  private long i;
  private boolean j;
  private String k;
  private g l = g.a;
  private long m;
  private boolean n = false;
  private long o;
  private long p;
  private int q;
  private ViewGroup r;
  
  private a(Activity paramActivity)
  {
    this.a = paramActivity;
  }
  
  public static a a(Activity paramActivity)
  {
    if (paramActivity == null) {
      throw new IllegalStateException("Activity cannot be null");
    }
    a locala = new a(paramActivity);
    locala.b = paramActivity.getString(m.dra_rate_app);
    locala.f = paramActivity.getSharedPreferences("app_rate_prefs", 0);
    locala.g = locala.f.edit();
    locala.k = paramActivity.getPackageName();
    return locala;
  }
  
  private void a(ViewGroup paramViewGroup)
  {
    if (this.n) {}
    for (Animation localAnimation = AnimationUtils.loadAnimation(this.a, i.fade_out_from_top);; localAnimation = AnimationUtils.loadAnimation(this.a, i.fade_out))
    {
      localAnimation.setAnimationListener(new e(this, paramViewGroup));
      paramViewGroup.startAnimation(localAnimation);
      return;
    }
  }
  
  private void a(String paramString)
  {
    Log.d("DicreetAppRate", paramString);
  }
  
  private void b(ViewGroup paramViewGroup)
  {
    this.a.addContentView(paramViewGroup, new ViewGroup.LayoutParams(-1, -1));
    if (paramViewGroup != null) {
      if (!this.n) {
        break label63;
      }
    }
    label63:
    for (Animation localAnimation = AnimationUtils.loadAnimation(this.a, i.fade_in_from_top);; localAnimation = AnimationUtils.loadAnimation(this.a, i.fade_in))
    {
      paramViewGroup.startAnimation(localAnimation);
      if (this.e != null) {
        this.e.a(this, paramViewGroup);
      }
      return;
    }
  }
  
  private boolean b()
  {
    if (System.currentTimeMillis() - this.f.getLong("last_count_update", 0L) < this.p)
    {
      if (this.j) {
        a("Count not incremented due to minimum interval not reached");
      }
      return false;
    }
    this.g.putInt("count", 1 + this.f.getInt("count", 0));
    this.g.putLong("last_count_update", System.currentTimeMillis());
    d();
    return true;
  }
  
  @SuppressLint({"NewApi"})
  private void c()
  {
    if (this.q != 0) {
      this.r = new FrameLayout(this.a);
    }
    label510:
    label545:
    label615:
    label635:
    label719:
    label725:
    label765:
    label939:
    label952:
    for (;;)
    {
      View localView;
      ViewGroup localViewGroup;
      WindowManager.LayoutParams localLayoutParams;
      int i2;
      try
      {
        this.a.getLayoutInflater().inflate(this.q, this.r);
        localView = this.r.findViewById(k.dar_close);
        TextView localTextView = (TextView)this.r.findViewById(k.dar_rate_element);
        localViewGroup = (ViewGroup)this.r.findViewById(k.dar_container);
        if (localViewGroup != null)
        {
          if (!this.n) {
            break label545;
          }
          if (!(localViewGroup.getParent() instanceof FrameLayout)) {
            break label510;
          }
          FrameLayout.LayoutParams localLayoutParams6 = (FrameLayout.LayoutParams)localViewGroup.getLayoutParams();
          localLayoutParams6.gravity = 48;
          localViewGroup.setLayoutParams(localLayoutParams6);
        }
        if (localTextView != null)
        {
          localTextView.setText(this.b);
          localTextView.setOnClickListener(new b(this, this.k));
        }
        if (localView != null) {
          localView.setOnClickListener(new c(this));
        }
        if (this.q == 0)
        {
          if (this.l != g.b) {
            break label635;
          }
          PorterDuff.Mode localMode = PorterDuff.Mode.SRC_ATOP;
          Drawable localDrawable2 = this.a.getResources().getDrawable(j.ic_action_remove);
          localDrawable2.setColorFilter(-16777216, localMode);
          ((ImageView)localView).setImageDrawable(localDrawable2);
          localTextView.setTextColor(-16777216);
          localViewGroup.setBackgroundColor(-1996488705);
          if (Build.VERSION.SDK_INT < 16) {
            break label615;
          }
          localView.setBackground(this.a.getResources().getDrawable(j.selectable_button_light));
        }
        if ((Build.VERSION.SDK_INT >= 19) && (localViewGroup != null))
        {
          localLayoutParams = this.a.getWindow().getAttributes();
          if (!this.n) {
            break label765;
          }
          boolean bool = o.a(localLayoutParams.flags, 67108864);
          if (this.a.getWindow().getDecorView().getSystemUiVisibility() != 512) {
            break label719;
          }
          i2 = 1;
          if ((bool) || (i2 != 0))
          {
            if (this.j) {
              a("Activity is translucent");
            }
            if (!(localViewGroup.getParent() instanceof FrameLayout)) {
              break label725;
            }
            FrameLayout.LayoutParams localLayoutParams2 = (FrameLayout.LayoutParams)localViewGroup.getLayoutParams();
            localLayoutParams2.topMargin = o.d(this.a);
            localViewGroup.setLayoutParams(localLayoutParams2);
          }
        }
        if (this.h <= 0) {
          break label939;
        }
        this.a.getWindow().getDecorView().postDelayed(new d(this), this.h);
        return;
      }
      catch (InflateException localInflateException)
      {
        this.r = ((ViewGroup)this.a.getLayoutInflater().inflate(l.app_rate, null));
        this.q = 0;
        continue;
      }
      catch (Resources.NotFoundException localNotFoundException)
      {
        this.r = ((ViewGroup)this.a.getLayoutInflater().inflate(l.app_rate, null));
        this.q = 0;
        continue;
      }
      this.r = ((ViewGroup)this.a.getLayoutInflater().inflate(l.app_rate, null));
      continue;
      if ((localViewGroup.getParent() instanceof RelativeLayout))
      {
        RelativeLayout.LayoutParams localLayoutParams5 = (RelativeLayout.LayoutParams)localViewGroup.getLayoutParams();
        localLayoutParams5.addRule(10);
        localViewGroup.setLayoutParams(localLayoutParams5);
        continue;
        if ((localViewGroup.getParent() instanceof FrameLayout))
        {
          FrameLayout.LayoutParams localLayoutParams4 = (FrameLayout.LayoutParams)localViewGroup.getLayoutParams();
          localLayoutParams4.gravity = 80;
          localViewGroup.setLayoutParams(localLayoutParams4);
        }
        else if ((localViewGroup.getParent() instanceof RelativeLayout))
        {
          RelativeLayout.LayoutParams localLayoutParams3 = (RelativeLayout.LayoutParams)localViewGroup.getLayoutParams();
          localLayoutParams3.addRule(12);
          localViewGroup.setLayoutParams(localLayoutParams3);
          continue;
          localView.setBackgroundDrawable(this.a.getResources().getDrawable(j.selectable_button_light));
          continue;
          Drawable localDrawable1 = this.a.getResources().getDrawable(j.ic_action_remove);
          localDrawable1.clearColorFilter();
          ((ImageView)localView).setImageDrawable(localDrawable1);
          localViewGroup.setBackgroundColor(-1442840576);
          if (Build.VERSION.SDK_INT >= 16)
          {
            localView.setBackground(this.a.getResources().getDrawable(j.selectable_button_dark));
          }
          else
          {
            localView.setBackgroundDrawable(this.a.getResources().getDrawable(j.selectable_button_dark));
            continue;
            i2 = 0;
            continue;
            if ((localViewGroup.getParent() instanceof RelativeLayout))
            {
              RelativeLayout.LayoutParams localLayoutParams1 = (RelativeLayout.LayoutParams)localViewGroup.getLayoutParams();
              localLayoutParams1.topMargin = o.d(this.a);
              localViewGroup.setLayoutParams(localLayoutParams1);
              continue;
              if (o.a(localLayoutParams.flags, 134217728))
              {
                if (this.j) {
                  a("Activity is translucent");
                }
                int i1 = ((WindowManager)this.a.getSystemService("window")).getDefaultDisplay().getRotation();
                Object localObject;
                if ((localViewGroup.getParent() instanceof FrameLayout)) {
                  localObject = (FrameLayout.LayoutParams)localViewGroup.getLayoutParams();
                }
                for (;;)
                {
                  if (localObject == null) {
                    break label952;
                  }
                  switch (i1)
                  {
                  default: 
                    break;
                  case 0: 
                  case 2: 
                    ((ViewGroup.MarginLayoutParams)localObject).bottomMargin = o.a(this.a);
                    localViewGroup.setLayoutParams((ViewGroup.LayoutParams)localObject);
                    break;
                    if ((localViewGroup.getParent() instanceof RelativeLayout)) {
                      localObject = (RelativeLayout.LayoutParams)localViewGroup.getLayoutParams();
                    }
                    break;
                  case 1: 
                  case 3: 
                    ((ViewGroup.MarginLayoutParams)localObject).rightMargin = o.b(this.a);
                    localViewGroup.setLayoutParams((ViewGroup.LayoutParams)localObject);
                    break;
                    b(this.r);
                    return;
                    localObject = null;
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  @SuppressLint({"NewApi"})
  private void d()
  {
    if (Build.VERSION.SDK_INT >= 9)
    {
      this.g.apply();
      return;
    }
    this.g.commit();
  }
  
  public a a(int paramInt)
  {
    this.c = paramInt;
    return this;
  }
  
  public a a(n paramn)
  {
    this.d = paramn;
    return this;
  }
  
  @SuppressLint({"NewApi"})
  public void a()
  {
    if (!o.a(this.a)) {
      if (this.j) {
        a("Play Store is not installed. Won't do anything");
      }
    }
    int i1;
    do
    {
      do
      {
        Date localDate1;
        Date localDate2;
        do
        {
          do
          {
            do
            {
              do
              {
                do
                {
                  return;
                  if (this.j) {
                    a("Last crash: " + (System.currentTimeMillis() - this.f.getLong("last_crash", 0L)) / 1000L + " seconds ago");
                  }
                  if (System.currentTimeMillis() - this.f.getLong("last_crash", 0L) >= this.m) {
                    break;
                  }
                } while (!this.j);
                a("A recent crash avoids anything to be done.");
                return;
                if (this.f.getLong("monitor_total", 0L) >= this.o) {
                  break;
                }
              } while (!this.j);
              a("Monitor time not reached. Nothing will be done");
              return;
              if (o.b(this.a)) {
                break;
              }
            } while (!this.j);
            a("Device is not online. AppRate try to show up next time.");
            return;
          } while (!b());
          if (Build.VERSION.SDK_INT < 9) {
            break;
          }
          localDate1 = o.a(this.a.getPackageManager(), this.k);
          localDate2 = new Date();
          if (localDate2.getTime() - localDate1.getTime() >= this.i) {
            break;
          }
        } while (!this.j);
        a("Date not reached. Time elapsed since installation (in sec.): " + (localDate2.getTime() - localDate1.getTime()));
        return;
        if (!this.f.getBoolean("elapsed_time", false))
        {
          this.g.putBoolean("elapsed_time", true);
          if (this.j) {
            a("First time after the time is elapsed");
          }
          if (this.f.getInt("count", 5) > this.c)
          {
            if (this.j) {
              a("Initial count passed. Resetting to initialLaunchCount");
            }
            this.g.putInt("count", this.c);
          }
          d();
        }
      } while (this.f.getBoolean("clicked", false));
      i1 = this.f.getInt("count", 0);
      if (i1 == this.c)
      {
        if (this.j) {
          a("initialLaunchCount reached");
        }
        c();
        return;
      }
      if ((this.d == n.a) && (i1 % this.c == 0))
      {
        if (this.j) {
          a("initialLaunchCount incremental reached");
        }
        c();
        return;
      }
      if ((this.d == n.b) && (i1 % this.c == 0) && (o.a(i1 / this.c)))
      {
        if (this.j) {
          a("initialLaunchCount exponential reached");
        }
        c();
        return;
      }
    } while (!this.j);
    a("Nothing to show. initialLaunchCount: " + this.c + " - Current count: " + i1);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/a/a/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */