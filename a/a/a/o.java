package a.a.a;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build.VERSION;
import android.util.TypedValue;
import java.lang.reflect.Field;
import java.util.Date;

public class o
{
  @SuppressLint({"NewApi"})
  public static int a(Activity paramActivity)
  {
    if (!b(paramActivity.getResources(), "config_showNavigationBar")) {
      return 0;
    }
    return a(paramActivity.getResources(), "navigation_bar_height");
  }
  
  public static int a(Resources paramResources, String paramString)
  {
    int i = paramResources.getIdentifier(paramString, "dimen", "android");
    int j = 0;
    if (i > 0) {
      j = paramResources.getDimensionPixelSize(i);
    }
    return j;
  }
  
  public static Date a(PackageManager paramPackageManager, String paramString)
  {
    try
    {
      PackageInfo localPackageInfo = paramPackageManager.getPackageInfo(paramString, 0);
      Date localDate = new Date(PackageInfo.class.getField("firstInstallTime").getLong(localPackageInfo));
      return localDate;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      return null;
    }
    catch (SecurityException localSecurityException)
    {
      return null;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;) {}
    }
    catch (NoSuchFieldException localNoSuchFieldException)
    {
      for (;;) {}
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      for (;;) {}
    }
  }
  
  public static boolean a(int paramInt)
  {
    return (paramInt & paramInt - 1) == 0;
  }
  
  public static boolean a(int paramInt1, int paramInt2)
  {
    return (paramInt1 & paramInt2) == paramInt2;
  }
  
  public static boolean a(Context paramContext)
  {
    PackageManager localPackageManager = paramContext.getPackageManager();
    try
    {
      String str = (String)localPackageManager.getPackageInfo("com.android.vending", 1).applicationInfo.loadLabel(localPackageManager);
      if (str != null)
      {
        boolean bool = str.equals("Market");
        if (!bool) {
          return true;
        }
      }
      return false;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
    return false;
  }
  
  @SuppressLint({"NewApi"})
  public static int b(Activity paramActivity)
  {
    if (!b(paramActivity.getResources(), "config_showNavigationBar")) {
      return 0;
    }
    return a(paramActivity.getResources(), "navigation_bar_width");
  }
  
  public static boolean b(Context paramContext)
  {
    if (paramContext.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == 0)
    {
      NetworkInfo localNetworkInfo = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
      return (localNetworkInfo != null) && (localNetworkInfo.isConnectedOrConnecting());
    }
    return true;
  }
  
  private static boolean b(Resources paramResources, String paramString)
  {
    int i = paramResources.getIdentifier(paramString, "bool", "android");
    if (i > 0) {
      return paramResources.getBoolean(i);
    }
    return false;
  }
  
  @SuppressLint({"NewApi"})
  public static int c(Activity paramActivity)
  {
    return a(paramActivity.getResources(), "status_bar_height");
  }
  
  public static int d(Activity paramActivity)
  {
    return c(paramActivity) + e(paramActivity);
  }
  
  public static int e(Activity paramActivity)
  {
    if (Build.VERSION.SDK_INT >= 19)
    {
      TypedValue localTypedValue = new TypedValue();
      if (paramActivity.getTheme().resolveAttribute(16843499, localTypedValue, true)) {
        return TypedValue.complexToDimensionPixelSize(localTypedValue.data, paramActivity.getResources().getDisplayMetrics());
      }
    }
    return 0;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/a/a/a/o.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */