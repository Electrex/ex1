package com.d.a;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;

public class b
  extends a
  implements Animation.AnimationListener
{
  private final View a;
  private final int b;
  private final int c;
  
  public b(View paramView, int paramInt1, int paramInt2)
  {
    super(paramView.getContext());
    this.a = paramView;
    this.b = paramInt1;
    this.c = paramInt2;
  }
  
  private void a(int paramInt)
  {
    if (paramInt != 0)
    {
      Animation localAnimation = AnimationUtils.loadAnimation(this.a.getContext(), paramInt);
      localAnimation.setAnimationListener(this);
      this.a.startAnimation(localAnimation);
      a(true);
    }
  }
  
  public void a()
  {
    if (this.a.getVisibility() != 0)
    {
      this.a.setVisibility(0);
      a(this.b);
    }
  }
  
  public void b()
  {
    if (this.a.getVisibility() == 0)
    {
      this.a.setVisibility(8);
      a(this.c);
    }
  }
  
  public void onAnimationEnd(Animation paramAnimation)
  {
    a(false);
  }
  
  public void onAnimationRepeat(Animation paramAnimation) {}
  
  public void onAnimationStart(Animation paramAnimation) {}
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/d/a/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */