package com.b.a.a.a;

import android.content.Context;
import android.util.Log;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public class n
  implements t
{
  private long a;
  private long b;
  private long c;
  private long d;
  private long e = 0L;
  private int f;
  private u g;
  
  public n(Context paramContext, s params)
  {
    this.g = new u(paramContext.getSharedPreferences("com.android.vending.licensing.ServerManagedPolicyx", 0), params);
    this.f = Integer.parseInt(this.g.b("lastResponsex", Integer.toString(291)));
    this.a = Long.parseLong(this.g.b("validityTimestampx", "0"));
    this.b = Long.parseLong(this.g.b("retryUntilx", "0"));
    this.c = Long.parseLong(this.g.b("maxRetriesx", "0"));
    this.d = Long.parseLong(this.g.b("retryCountx", "0"));
  }
  
  private void a(int paramInt)
  {
    this.e = System.currentTimeMillis();
    this.f = paramInt;
    this.g.a("lastResponsex", Integer.toString(paramInt));
  }
  
  private void a(long paramLong)
  {
    this.d = paramLong;
    this.g.a("retryCountx", Long.toString(paramLong));
  }
  
  private void a(String paramString)
  {
    this.g.a("authenticity", paramString);
  }
  
  private void b(String paramString)
  {
    try
    {
      Long localLong2 = Long.valueOf(Long.parseLong(paramString));
      localLong1 = localLong2;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      for (;;)
      {
        Log.w("ServerManagedPolicyx", "License validity xtimestamp (VT) missing, caching for a minute");
        Long localLong1 = Long.valueOf(60000L + System.currentTimeMillis());
        paramString = Long.toString(localLong1.longValue());
      }
    }
    this.a = localLong1.longValue();
    this.g.a("validityTimestampx", paramString);
  }
  
  private void c(String paramString)
  {
    try
    {
      Long localLong2 = Long.valueOf(Long.parseLong(paramString));
      localLong1 = localLong2;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      for (;;)
      {
        Log.w("ServerManagedPolicyx", "License retry xtimestamp (GT) missing, grace period disabled");
        paramString = "0";
        Long localLong1 = Long.valueOf(0L);
      }
    }
    this.b = localLong1.longValue();
    this.g.a("retryUntilx", paramString);
  }
  
  private void d(String paramString)
  {
    try
    {
      Long localLong2 = Long.valueOf(Long.parseLong(paramString));
      localLong1 = localLong2;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      for (;;)
      {
        Log.w("ServerManagedPolicyx", "Licence retry count (GR) missing, grace period disabled");
        paramString = "0";
        Long localLong1 = Long.valueOf(0L);
      }
    }
    this.c = localLong1.longValue();
    this.g.a("maxRetriesx", paramString);
  }
  
  private Map e(String paramString)
  {
    localHashMap = new HashMap();
    try
    {
      Iterator localIterator = URLEncodedUtils.parse(new URI("?" + paramString), "UTF-8").iterator();
      while (localIterator.hasNext())
      {
        NameValuePair localNameValuePair = (NameValuePair)localIterator.next();
        localHashMap.put(localNameValuePair.getName(), localNameValuePair.getValue());
      }
      return localHashMap;
    }
    catch (URISyntaxException localURISyntaxException)
    {
      Log.w("ServerManagedPolicyx", "Invalid syntax error while decoding extras data from server.");
    }
  }
  
  public void a()
  {
    this.g.a("authenticity", "13");
    this.g.a();
  }
  
  public void a(int paramInt, m paramm)
  {
    if (paramInt != 291)
    {
      a(0L);
      if (paramInt != 256) {
        break label110;
      }
      Map localMap = e(paramm.g);
      this.f = paramInt;
      b((String)localMap.get("VT"));
      c((String)localMap.get("GT"));
      d((String)localMap.get("GR"));
      a("11");
    }
    for (;;)
    {
      a(paramInt);
      this.g.a();
      return;
      a(1L + this.d);
      break;
      label110:
      if (paramInt == 561)
      {
        b("0");
        c("0");
        d("0");
        a("12");
      }
    }
  }
  
  public boolean b()
  {
    return this.g.b("authenticity", "0").equals("11");
  }
  
  public int c()
  {
    long l = System.currentTimeMillis();
    if (this.f == 256)
    {
      if ((l > this.a) || (!b())) {}
    }
    else
    {
      do
      {
        return 23;
        if ((this.f != 291) || (l >= 60000L + this.e)) {
          break;
        }
      } while (((l <= this.b) || (this.d <= this.c)) && (b()));
      return 22;
    }
    return 22;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/b/a/a/a/n.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */