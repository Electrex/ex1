package com.b.a.a.a;

import android.text.TextUtils;
import java.util.regex.Pattern;

public class m
{
  public int a;
  public int b;
  public String c;
  public String d;
  public String e;
  public long f;
  public String g;
  
  public static m a(String paramString)
  {
    int i = paramString.indexOf(':');
    String str2;
    String[] arrayOfString;
    if (-1 == i)
    {
      str2 = "";
      arrayOfString = TextUtils.split(paramString, Pattern.quote("|"));
      if (arrayOfString.length < 6) {
        throw new IllegalArgumentException("Wrong number of fields.");
      }
    }
    else
    {
      String str1 = paramString.substring(0, i);
      if (i >= paramString.length()) {}
      for (str2 = "";; str2 = paramString.substring(i + 1))
      {
        paramString = str1;
        break;
      }
    }
    Integer localInteger = Integer.valueOf(Integer.parseInt(arrayOfString[0]));
    m localm = new m();
    localm.g = str2;
    localm.a = localInteger.intValue();
    localm.b = Integer.parseInt(arrayOfString[1]);
    localm.c = arrayOfString[2];
    localm.d = arrayOfString[3];
    localm.e = arrayOfString[4];
    localm.f = Long.parseLong(arrayOfString[5]);
    return localm;
  }
  
  public String toString()
  {
    Object[] arrayOfObject = new Object[6];
    arrayOfObject[0] = Integer.valueOf(this.a);
    arrayOfObject[1] = Integer.valueOf(this.b);
    arrayOfObject[2] = this.c;
    arrayOfObject[3] = this.d;
    arrayOfObject[4] = this.e;
    arrayOfObject[5] = Long.valueOf(this.f);
    return TextUtils.join("|", arrayOfObject);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/b/a/a/a/m.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */