package com.b.a.a.a;

import android.text.TextUtils;
import android.util.Log;
import com.b.a.a.a.a.a;
import com.b.a.a.a.a.b;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;

class i
{
  private final t a;
  private final e b;
  private final int c;
  private final String d;
  private final String e;
  private final q f;
  
  i(t paramt, q paramq, e parame, int paramInt, String paramString1, String paramString2)
  {
    this.a = paramt;
    this.f = paramq;
    this.b = parame;
    this.c = paramInt;
    this.d = paramString1;
    this.e = paramString2;
  }
  
  private void a(int paramInt)
  {
    this.b.c(paramInt);
  }
  
  private void a(int paramInt, m paramm)
  {
    int i = 23;
    this.a.a(paramInt, paramm);
    if (this.a.c() == i) {}
    for (;;)
    {
      switch (i)
      {
      default: 
        return;
      case 23: 
        this.b.a(paramInt);
        return;
      }
      this.b.b(paramInt);
      return;
      i = 22;
    }
  }
  
  private void a(int paramInt, m paramm, String paramString)
  {
    if ((paramInt == 0) || (paramInt == 2))
    {
      a(this.f.a(paramString), paramm);
      return;
    }
    if (paramInt == 1)
    {
      a(paramm);
      return;
    }
    if (paramInt == 257)
    {
      Log.w("LicenseValidator", "Error contacting licensing server.");
      a(291, paramm);
      return;
    }
    if (paramInt == 4)
    {
      Log.w("LicenseValidator", "An error has occurred on the licensing server.");
      a(291, paramm);
      return;
    }
    if (paramInt == 5)
    {
      Log.w("LicenseValidator", "Licensing server is refusing to talk to this device, over quota.");
      a(291, paramm);
      return;
    }
    if (paramInt == 258)
    {
      a(1);
      return;
    }
    if (paramInt == 259)
    {
      a(2);
      return;
    }
    if (paramInt == 3)
    {
      a(3);
      return;
    }
    Log.e("LicenseValidator", "Unknown response code for license check.");
    d();
  }
  
  private void a(m paramm)
  {
    a(561, paramm);
  }
  
  private void d()
  {
    this.b.b(561);
  }
  
  public e a()
  {
    return this.b;
  }
  
  public void a(PublicKey paramPublicKey, int paramInt, String paramString1, String paramString2)
  {
    Object localObject = null;
    String str;
    if ((paramInt == 0) || (paramInt == 1) || (paramInt == 2))
    {
      try
      {
        Signature localSignature = Signature.getInstance("SHA1withRSA");
        localSignature.initVerify(paramPublicKey);
        localSignature.update(paramString1.getBytes());
        if (!localSignature.verify(b.a(paramString2)))
        {
          Log.e("LicenseValidator", "Signature verification failed.");
          d();
          return;
        }
      }
      catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
      {
        throw new RuntimeException(localNoSuchAlgorithmException);
      }
      catch (InvalidKeyException localInvalidKeyException)
      {
        a(5);
        return;
      }
      catch (SignatureException localSignatureException)
      {
        throw new RuntimeException(localSignatureException);
      }
      catch (a locala)
      {
        Log.e("LicenseValidator", "Could not Base64-xdecode signature.");
        d();
        return;
      }
      try
      {
        m localm = m.a(paramString1);
        localObject = localm;
        if (((m)localObject).a != paramInt)
        {
          Log.e("LicenseValidator", "Response codes don't match.");
          d();
          return;
        }
      }
      catch (IllegalArgumentException localIllegalArgumentException)
      {
        Log.e("LicenseValidator", "Could not xparse response.");
        d();
        return;
      }
      if (((m)localObject).b != this.c)
      {
        Log.e("LicenseValidator", "Nonce doesn't match.");
        d();
        return;
      }
      if (!((m)localObject).c.equals(this.d))
      {
        Log.e("LicenseValidator", "Package name doesn't match.");
        d();
        return;
      }
      if (!((m)localObject).d.equals(this.e))
      {
        Log.e("LicenseValidator", "Version codes don't match.");
        d();
        return;
      }
      str = ((m)localObject).e;
      if (TextUtils.isEmpty(str))
      {
        Log.e("LicenseValidator", "User identifier is empty.");
        d();
      }
    }
    else
    {
      str = null;
    }
    a(paramInt, (m)localObject, str);
  }
  
  public int b()
  {
    return this.c;
  }
  
  public String c()
  {
    return this.d;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/b/a/a/a/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */