package com.b.a.a.a;

import com.b.a.a.a.a.a;
import com.b.a.a.a.a.b;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class p
  implements s
{
  private static final byte[] a = { 16, 74, 71, -80, 32, 101, -47, 72, 117, -14, 0, -29, 70, 65, -12, 74 };
  private Cipher b;
  private Cipher c;
  
  public p(byte[] paramArrayOfByte, String paramString1, String paramString2)
  {
    try
    {
      SecretKeySpec localSecretKeySpec = new SecretKeySpec(SecretKeyFactory.getInstance("PBEWITHSHAAND256BITAES-CBC-BC").generateSecret(new PBEKeySpec((paramString1 + paramString2).toCharArray(), paramArrayOfByte, 1024, 256)).getEncoded(), "AES");
      this.b = Cipher.getInstance("AES/CBC/PKCS5Padding");
      this.b.init(1, localSecretKeySpec, new IvParameterSpec(a));
      this.c = Cipher.getInstance("AES/CBC/PKCS5Padding");
      this.c.init(2, localSecretKeySpec, new IvParameterSpec(a));
      return;
    }
    catch (GeneralSecurityException localGeneralSecurityException)
    {
      throw new RuntimeException("Invalid environment", localGeneralSecurityException);
    }
  }
  
  public String a(String paramString1, String paramString2)
  {
    if (paramString1 == null) {
      return null;
    }
    try
    {
      String str = b.a(this.b.doFinal(("com.android.vending.licensing.AESObfuscator-1|" + paramString2 + paramString1).getBytes("UTF-8")));
      return str;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      throw new RuntimeException("Invalid environment", localUnsupportedEncodingException);
    }
    catch (GeneralSecurityException localGeneralSecurityException)
    {
      throw new RuntimeException("Invalid environment", localGeneralSecurityException);
    }
  }
  
  public String b(String paramString1, String paramString2)
  {
    if (paramString1 == null) {
      return null;
    }
    try
    {
      str1 = new String(this.c.doFinal(b.a(paramString1)), "UTF-8");
      if (str1.indexOf("com.android.vending.licensing.AESObfuscator-1|" + paramString2) != 0) {
        throw new o("Header not found (invalid data or key):" + paramString1);
      }
    }
    catch (a locala)
    {
      String str1;
      throw new o(locala.getMessage() + ":" + paramString1);
      String str2 = str1.substring("com.android.vending.licensing.AESObfuscator-1|".length() + paramString2.length(), str1.length());
      return str2;
    }
    catch (IllegalBlockSizeException localIllegalBlockSizeException)
    {
      throw new o(localIllegalBlockSizeException.getMessage() + ":" + paramString1);
    }
    catch (BadPaddingException localBadPaddingException)
    {
      throw new o(localBadPaddingException.getMessage() + ":" + paramString1);
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      throw new RuntimeException("Invalid environment", localUnsupportedEncodingException);
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/b/a/a/a/p.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */