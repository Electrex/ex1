package com.b.a.a.a;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class a
  implements ServiceConnection
{
  private static final SecureRandom a = new SecureRandom();
  private j b;
  private PublicKey c;
  private final Context d;
  private final t e;
  private Handler f;
  private final String g;
  private final String h;
  private final Set i = new HashSet();
  private final Queue j = new LinkedList();
  
  public a(Context paramContext, t paramt, String paramString)
  {
    this.d = paramContext;
    this.e = paramt;
    this.c = a(paramString);
    this.g = this.d.getPackageName();
    this.h = a(paramContext, this.g);
    HandlerThread localHandlerThread = new HandlerThread("background thread");
    localHandlerThread.start();
    this.f = new Handler(localHandlerThread.getLooper());
  }
  
  private static String a(Context paramContext, String paramString)
  {
    try
    {
      String str = String.valueOf(paramContext.getPackageManager().getPackageInfo(paramString, 0).versionCode);
      return str;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      Log.e("LicenseChecker", "Package not found. could not get version code.");
    }
    return "";
  }
  
  private static PublicKey a(String paramString)
  {
    try
    {
      byte[] arrayOfByte = com.b.a.a.a.a.b.a(paramString);
      PublicKey localPublicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(arrayOfByte));
      return localPublicKey;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      throw new RuntimeException(localNoSuchAlgorithmException);
    }
    catch (com.b.a.a.a.a.a locala)
    {
      Log.e("LicenseChecker", "Could not xdecode from Base64.");
      throw new IllegalArgumentException(locala);
    }
    catch (InvalidKeySpecException localInvalidKeySpecException)
    {
      Log.e("LicenseChecker", "Invalid key specification.");
      throw new IllegalArgumentException(localInvalidKeySpecException);
    }
  }
  
  private void a(i parami)
  {
    try
    {
      this.i.remove(parami);
      if (this.i.isEmpty()) {
        c();
      }
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  private void b()
  {
    for (;;)
    {
      i locali = (i)this.j.poll();
      if (locali == null) {
        break;
      }
      try
      {
        Log.i("LicenseChecker", "Calling checkLicense on service for " + locali.c());
        this.b.a(locali.b(), locali.c(), new b(this, locali));
        this.i.add(locali);
      }
      catch (RemoteException localRemoteException)
      {
        Log.w("LicenseChecker", "RemoteException in xcheckLicense call.", localRemoteException);
        b(locali);
      }
    }
  }
  
  /* Error */
  private void b(i parami)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 50	com/b/a/a/a/a:e	Lcom/b/a/a/a/t;
    //   6: sipush 291
    //   9: aconst_null
    //   10: invokeinterface 238 3 0
    //   15: aload_0
    //   16: getfield 50	com/b/a/a/a/a:e	Lcom/b/a/a/a/t;
    //   19: invokeinterface 240 1 0
    //   24: iconst_1
    //   25: if_icmpne +18 -> 43
    //   28: aload_1
    //   29: invokevirtual 243	com/b/a/a/a/i:a	()Lcom/b/a/a/a/e;
    //   32: sipush 291
    //   35: invokeinterface 248 2 0
    //   40: aload_0
    //   41: monitorexit
    //   42: return
    //   43: aload_1
    //   44: invokevirtual 243	com/b/a/a/a/i:a	()Lcom/b/a/a/a/e;
    //   47: sipush 291
    //   50: invokeinterface 250 2 0
    //   55: goto -15 -> 40
    //   58: astore_2
    //   59: aload_0
    //   60: monitorexit
    //   61: aload_2
    //   62: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	63	0	this	a
    //   0	63	1	parami	i
    //   58	4	2	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   2	40	58	finally
    //   43	55	58	finally
  }
  
  private void c()
  {
    if (this.b != null) {}
    try
    {
      this.d.unbindService(this);
      this.b = null;
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;)
      {
        Log.e("LicenseChecker", "Unable to unbind from licensing service (already unbound)");
      }
    }
  }
  
  private int d()
  {
    return a.nextInt();
  }
  
  public void a()
  {
    try
    {
      c();
      this.f.getLooper().quit();
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  /* Error */
  public void a(e parame)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 50	com/b/a/a/a/a:e	Lcom/b/a/a/a/t;
    //   6: invokeinterface 240 1 0
    //   11: bipush 23
    //   13: if_icmpne +24 -> 37
    //   16: ldc 115
    //   18: ldc_w 271
    //   21: invokestatic 208	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   24: pop
    //   25: aload_1
    //   26: sipush 256
    //   29: invokeinterface 248 2 0
    //   34: aload_0
    //   35: monitorexit
    //   36: return
    //   37: new 192	com/b/a/a/a/i
    //   40: dup
    //   41: aload_0
    //   42: getfield 50	com/b/a/a/a/a:e	Lcom/b/a/a/a/t;
    //   45: new 273	com/b/a/a/a/r
    //   48: dup
    //   49: invokespecial 274	com/b/a/a/a/r:<init>	()V
    //   52: aload_1
    //   53: aload_0
    //   54: invokespecial 276	com/b/a/a/a/a:d	()I
    //   57: aload_0
    //   58: getfield 63	com/b/a/a/a/a:g	Ljava/lang/String;
    //   61: aload_0
    //   62: getfield 68	com/b/a/a/a/a:h	Ljava/lang/String;
    //   65: invokespecial 279	com/b/a/a/a/i:<init>	(Lcom/b/a/a/a/t;Lcom/b/a/a/a/q;Lcom/b/a/a/a/e;ILjava/lang/String;Ljava/lang/String;)V
    //   68: astore_3
    //   69: aload_0
    //   70: getfield 210	com/b/a/a/a/a:b	Lcom/b/a/a/a/j;
    //   73: ifnonnull +116 -> 189
    //   76: ldc 115
    //   78: ldc_w 281
    //   81: invokestatic 208	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   84: pop
    //   85: new 283	android/content/Intent
    //   88: dup
    //   89: new 109	java/lang/String
    //   92: dup
    //   93: ldc_w 285
    //   96: invokestatic 135	com/b/a/a/a/a/b:a	(Ljava/lang/String;)[B
    //   99: invokespecial 286	java/lang/String:<init>	([B)V
    //   102: invokespecial 287	android/content/Intent:<init>	(Ljava/lang/String;)V
    //   105: astore 6
    //   107: aload 6
    //   109: ldc_w 289
    //   112: invokevirtual 293	android/content/Intent:setPackage	(Ljava/lang/String;)Landroid/content/Intent;
    //   115: pop
    //   116: aload_0
    //   117: getfield 48	com/b/a/a/a/a:d	Landroid/content/Context;
    //   120: aload 6
    //   122: aload_0
    //   123: iconst_1
    //   124: invokevirtual 297	android/content/Context:bindService	(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    //   127: ifeq +35 -> 162
    //   130: aload_0
    //   131: getfield 46	com/b/a/a/a/a:j	Ljava/util/Queue;
    //   134: aload_3
    //   135: invokeinterface 300 2 0
    //   140: pop
    //   141: goto -107 -> 34
    //   144: astore 8
    //   146: aload_1
    //   147: bipush 6
    //   149: invokeinterface 302 2 0
    //   154: goto -120 -> 34
    //   157: astore_2
    //   158: aload_0
    //   159: monitorexit
    //   160: aload_2
    //   161: athrow
    //   162: ldc 115
    //   164: ldc_w 304
    //   167: invokestatic 122	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   170: pop
    //   171: aload_0
    //   172: aload_3
    //   173: invokespecial 169	com/b/a/a/a/a:b	(Lcom/b/a/a/a/i;)V
    //   176: goto -142 -> 34
    //   179: astore 7
    //   181: aload 7
    //   183: invokevirtual 307	com/b/a/a/a/a/a:printStackTrace	()V
    //   186: goto -152 -> 34
    //   189: aload_0
    //   190: getfield 46	com/b/a/a/a/a:j	Ljava/util/Queue;
    //   193: aload_3
    //   194: invokeinterface 300 2 0
    //   199: pop
    //   200: aload_0
    //   201: invokespecial 309	com/b/a/a/a/a:b	()V
    //   204: goto -170 -> 34
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	207	0	this	a
    //   0	207	1	parame	e
    //   157	4	2	localObject	Object
    //   68	126	3	locali	i
    //   105	16	6	localIntent	android.content.Intent
    //   179	3	7	locala	com.b.a.a.a.a.a
    //   144	1	8	localSecurityException	SecurityException
    // Exception table:
    //   from	to	target	type
    //   85	141	144	java/lang/SecurityException
    //   162	176	144	java/lang/SecurityException
    //   2	34	157	finally
    //   37	85	157	finally
    //   85	141	157	finally
    //   146	154	157	finally
    //   162	176	157	finally
    //   181	186	157	finally
    //   189	204	157	finally
    //   85	141	179	com/b/a/a/a/a/a
    //   162	176	179	com/b/a/a/a/a/a
  }
  
  public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    try
    {
      this.b = k.a(paramIBinder);
      b();
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
  
  public void onServiceDisconnected(ComponentName paramComponentName)
  {
    try
    {
      Log.w("LicenseChecker", "Service unexpectedly disconnected.");
      this.b = null;
      return;
    }
    finally
    {
      localObject = finally;
      throw ((Throwable)localObject);
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/b/a/a/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */