package com.b.a.a.a;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class u
{
  private final SharedPreferences a;
  private final s b;
  private SharedPreferences.Editor c;
  
  public u(SharedPreferences paramSharedPreferences, s params)
  {
    this.a = paramSharedPreferences;
    this.b = params;
    this.c = null;
  }
  
  public void a()
  {
    if (this.c != null)
    {
      this.c.commit();
      this.c = null;
    }
  }
  
  public void a(String paramString1, String paramString2)
  {
    if (this.c == null) {
      this.c = this.a.edit();
    }
    String str = this.b.a(paramString2, paramString1);
    this.c.putString(paramString1, str);
  }
  
  public String b(String paramString1, String paramString2)
  {
    String str1 = this.a.getString(paramString1, null);
    if (str1 != null) {}
    try
    {
      String str2 = this.b.b(str1, paramString1);
      paramString2 = str2;
      return paramString2;
    }
    catch (o localo)
    {
      Log.w("PreferenceObfuscator", "Validation error while reading preference: " + paramString1);
    }
    return paramString2;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/b/a/a/a/u.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */