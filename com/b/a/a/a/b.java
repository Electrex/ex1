package com.b.a.a.a;

import android.os.Handler;
import android.util.Log;

class b
  extends g
{
  private final i b;
  private Runnable c;
  
  public b(a parama, i parami)
  {
    this.b = parami;
    this.c = new c(this, parama);
    a();
  }
  
  private void a()
  {
    Log.i("LicenseChecker", "Start monitoring timeout.");
    a.c(this.a).postDelayed(this.c, 10000L);
  }
  
  private void b()
  {
    Log.i("LicenseChecker", "Clearing timeout.");
    a.c(this.a).removeCallbacks(this.c);
  }
  
  public void a(int paramInt, String paramString1, String paramString2)
  {
    a.c(this.a).post(new d(this, paramInt, paramString1, paramString2));
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/b/a/a/a/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */