package com.b.a.a.a;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

public abstract class g
  extends Binder
  implements f
{
  public g()
  {
    attachInterface(this, "com.android.vending.licensing.ILicenseResultListener");
  }
  
  public static f a(IBinder paramIBinder)
  {
    if (paramIBinder == null) {
      return null;
    }
    IInterface localIInterface = paramIBinder.queryLocalInterface("com.android.vending.licensing.ILicenseResultListener");
    if ((localIInterface != null) && ((localIInterface instanceof f))) {
      return (f)localIInterface;
    }
    return new h(paramIBinder);
  }
  
  public IBinder asBinder()
  {
    return this;
  }
  
  public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
  {
    if (paramInt1 == 1598968902)
    {
      paramParcel2.writeString("com.android.vending.licensing.ILicenseResultListener");
      return true;
    }
    if (paramInt1 == 1)
    {
      paramParcel1.enforceInterface("com.android.vending.licensing.ILicenseResultListener");
      a(paramParcel1.readInt(), paramParcel1.readString(), paramParcel1.readString());
      return true;
    }
    return super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/b/a/a/a/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */