package com.b.a.a.a;

import android.os.IBinder;
import android.os.Parcel;

class l
  implements j
{
  private IBinder a;
  
  l(IBinder paramIBinder)
  {
    this.a = paramIBinder;
  }
  
  public void a(long paramLong, String paramString, f paramf)
  {
    Parcel localParcel = Parcel.obtain();
    try
    {
      localParcel.writeInterfaceToken("com.android.vending.licensing.ILicensingService");
      localParcel.writeLong(paramLong);
      localParcel.writeString(paramString);
      IBinder localIBinder = null;
      if (paramf != null) {
        localIBinder = paramf.asBinder();
      }
      localParcel.writeStrongBinder(localIBinder);
      this.a.transact(1, localParcel, null, 1);
      return;
    }
    finally
    {
      localParcel.recycle();
    }
  }
  
  public IBinder asBinder()
  {
    return this.a;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/b/a/a/a/l.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */