package com.c.a.c;

import android.content.Context;
import android.content.res.Resources;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.view.ViewPropertyAnimator;

public class d
  implements View.OnTouchListener
{
  private int a;
  private int b;
  private int c;
  private long d;
  private View e;
  private f f;
  private int g = 1;
  private float h;
  private float i;
  private boolean j;
  private int k;
  private Object l;
  private VelocityTracker m;
  private float n;
  
  public d(View paramView, Object paramObject, f paramf)
  {
    ViewConfiguration localViewConfiguration = ViewConfiguration.get(paramView.getContext());
    this.a = localViewConfiguration.getScaledTouchSlop();
    this.b = (16 * localViewConfiguration.getScaledMinimumFlingVelocity());
    this.c = localViewConfiguration.getScaledMaximumFlingVelocity();
    this.d = paramView.getContext().getResources().getInteger(17694720);
    this.e = paramView;
    this.l = paramObject;
    this.f = paramf;
  }
  
  private void a()
  {
    this.f.a(this.e, this.l);
  }
  
  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    boolean bool1 = true;
    paramMotionEvent.offsetLocation(this.n, 0.0F);
    if (this.g < 2) {
      this.g = this.e.getWidth();
    }
    float f3;
    float f4;
    float f5;
    float f6;
    boolean bool2;
    switch (paramMotionEvent.getActionMasked())
    {
    default: 
    case 0: 
    case 1: 
      do
      {
        do
        {
          return false;
          this.h = paramMotionEvent.getRawX();
          this.i = paramMotionEvent.getRawY();
        } while (!this.f.a(this.l));
        this.f.a(bool1);
        this.m = VelocityTracker.obtain();
        this.m.addMovement(paramMotionEvent);
        return false;
      } while (this.m == null);
      this.f.a(false);
      f3 = paramMotionEvent.getRawX() - this.h;
      this.m.addMovement(paramMotionEvent);
      this.m.computeCurrentVelocity(1000);
      f4 = this.m.getXVelocity();
      f5 = Math.abs(f4);
      f6 = Math.abs(this.m.getYVelocity());
      if ((Math.abs(f3) > this.g / 2) && (this.j)) {
        if (f3 > 0.0F) {
          bool2 = bool1;
        }
      }
      break;
    }
    for (;;)
    {
      float f7;
      if (bool1)
      {
        ViewPropertyAnimator localViewPropertyAnimator = this.e.animate();
        if (bool2)
        {
          f7 = this.g;
          label257:
          localViewPropertyAnimator.translationX(f7).alpha(0.0F).setDuration(this.d).setListener(new e(this));
        }
      }
      for (;;)
      {
        if (this.m != null)
        {
          this.m.recycle();
          this.m = null;
        }
        this.n = 0.0F;
        this.h = 0.0F;
        this.i = 0.0F;
        this.j = false;
        return false;
        bool2 = false;
        break;
        if ((this.b > f5) || (f5 > this.c) || (f6 >= f5) || (f6 >= f5) || (!this.j)) {
          break label792;
        }
        boolean bool3;
        label389:
        boolean bool4;
        label399:
        boolean bool5;
        if (f4 < 0.0F)
        {
          bool3 = bool1;
          if (f3 >= 0.0F) {
            break label440;
          }
          bool4 = bool1;
          if (bool3 != bool4) {
            break label446;
          }
          bool5 = bool1;
          label409:
          if (this.m.getXVelocity() <= 0.0F) {
            break label452;
          }
        }
        for (;;)
        {
          boolean bool6 = bool1;
          bool1 = bool5;
          bool2 = bool6;
          break;
          bool3 = false;
          break label389;
          label440:
          bool4 = false;
          break label399;
          label446:
          bool5 = false;
          break label409;
          label452:
          bool1 = false;
        }
        f7 = -this.g;
        break label257;
        if (this.j) {
          this.e.animate().translationX(0.0F).alpha(1.0F).setDuration(this.d).setListener(null);
        }
      }
      if (this.m == null) {
        break;
      }
      this.e.animate().translationX(0.0F).alpha(1.0F).setDuration(this.d).setListener(null);
      this.m.recycle();
      this.m = null;
      this.n = 0.0F;
      this.h = 0.0F;
      this.i = 0.0F;
      this.j = false;
      return false;
      if (this.m == null) {
        break;
      }
      this.m.addMovement(paramMotionEvent);
      float f1 = paramMotionEvent.getRawX() - this.h;
      float f2 = paramMotionEvent.getRawY() - this.i;
      if ((Math.abs(f1) > this.a) && (Math.abs(f2) < Math.abs(f1) / 2.0F))
      {
        this.j = bool1;
        if (f1 <= 0.0F) {
          break label782;
        }
      }
      label782:
      for (int i1 = this.a;; i1 = -this.a)
      {
        this.k = i1;
        if (this.e.getParent() != null) {
          this.e.getParent().requestDisallowInterceptTouchEvent(bool1);
        }
        MotionEvent localMotionEvent = MotionEvent.obtain(paramMotionEvent);
        localMotionEvent.setAction(0x3 | paramMotionEvent.getActionIndex() << 8);
        this.e.onTouchEvent(localMotionEvent);
        localMotionEvent.recycle();
        if (!this.j) {
          break;
        }
        this.n = f1;
        this.e.setTranslationX(f1 - this.k);
        this.e.setAlpha(Math.max(0.0F, Math.min(1.0F, 1.0F - 2.0F * Math.abs(f1) / this.g)));
        return bool1;
      }
      label792:
      bool2 = false;
      bool1 = false;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/c/a/c/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */