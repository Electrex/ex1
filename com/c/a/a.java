package com.c.a;

import android.app.Activity;
import android.graphics.Point;
import android.os.Build.VERSION;
import android.view.Display;
import android.view.WindowManager;

class a
{
  private static final b a = new e();
  
  static
  {
    if (Build.VERSION.SDK_INT >= 17)
    {
      a = new d();
      return;
    }
    if (Build.VERSION.SDK_INT >= 13)
    {
      a = new c();
      return;
    }
  }
  
  public static int a(Activity paramActivity, Float paramFloat)
  {
    Display localDisplay = paramActivity.getWindowManager().getDefaultDisplay();
    Point localPoint = new Point();
    b(localDisplay, localPoint);
    return (int)(localPoint.x * paramFloat.floatValue());
  }
  
  public static void a(Display paramDisplay, Point paramPoint)
  {
    a.a(paramDisplay, paramPoint);
  }
  
  public static void b(Display paramDisplay, Point paramPoint)
  {
    a.b(paramDisplay, paramPoint);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/c/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */