package com.c.a;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

public class aa
{
  private static final String a = aa.class.getSimpleName();
  private static final Handler b = new Handler(Looper.getMainLooper());
  private static n c;
  
  public static void a(n paramn)
  {
    try
    {
      a(paramn, (Activity)paramn.getContext());
      return;
    }
    catch (ClassCastException localClassCastException)
    {
      Log.e(a, "Couldn't get Activity from the Snackbar's Context. Try calling #show(Snackbar, Activity) instead", localClassCastException);
    }
  }
  
  public static void a(n paramn, Activity paramActivity)
  {
    b.post(new ab(paramn, paramActivity));
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/c/a/aa.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */