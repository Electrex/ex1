package com.c.a;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build.VERSION;
import android.support.v4.view.a.aj;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.c.a.c.b;
import com.c.a.c.c;
import com.c.a.c.d;

public class n
  extends com.c.a.b.a
{
  private com.c.a.c.a A;
  private b B;
  private boolean C;
  private boolean D;
  private boolean E = true;
  private c F;
  private Typeface G;
  private Typeface H;
  private boolean I = false;
  private boolean J = true;
  private boolean K = false;
  private Rect L = new Rect();
  private Rect M = new Rect();
  private Point N = new Point();
  private Point O = new Point();
  private Activity P;
  private Float Q = null;
  private boolean R;
  private Runnable S = new o(this);
  private Runnable T = new p(this);
  private int a = 55536;
  private int b = 55536;
  private com.c.a.a.a c = com.c.a.a.a.a;
  private x d = x.b;
  private CharSequence e;
  private TextView f;
  private int g = this.a;
  private int h = this.a;
  private int i;
  private Integer j;
  private y k = y.b;
  private int l = this.b;
  private int m = 0;
  private int n = 0;
  private int o = 0;
  private int p = 0;
  private long q;
  private long r;
  private long s = -1L;
  private CharSequence t;
  private int u = this.a;
  private boolean v = true;
  private boolean w = true;
  private boolean x = false;
  private boolean y = false;
  private long z = -1L;
  
  private n(Context paramContext)
  {
    super(paramContext);
    if (Build.VERSION.SDK_INT >= 16) {
      addView(new z(getContext()));
    }
  }
  
  private static int a(int paramInt, float paramFloat)
  {
    return (int)(0.5F + paramFloat * paramInt);
  }
  
  public static int a(y paramy)
  {
    if (paramy == y.a) {
      return g.sb__top_in;
    }
    return g.sb__bottom_in;
  }
  
  private ViewGroup.MarginLayoutParams a(Context paramContext, Activity paramActivity, ViewGroup paramViewGroup, boolean paramBoolean)
  {
    com.c.a.b.a locala = (com.c.a.b.a)LayoutInflater.from(paramContext).inflate(m.sb__template, this, true);
    locala.setOrientation(1);
    Resources localResources = getResources();
    int i1;
    float f1;
    ViewGroup.MarginLayoutParams localMarginLayoutParams;
    label193:
    TextView localTextView;
    if (this.g != this.a)
    {
      i1 = this.g;
      this.g = i1;
      this.i = localResources.getDimensionPixelOffset(j.sb__offset);
      this.R = paramBoolean;
      f1 = localResources.getDisplayMetrics().density;
      if (!this.R) {
        break label416;
      }
      locala.setMinimumHeight(a(this.c.a(), f1));
      locala.setMaxHeight(a(this.c.b(), f1));
      locala.setBackgroundColor(this.g);
      localMarginLayoutParams = a(paramViewGroup, -1, -2, this.k);
      if (this.l != this.b) {
        a(locala, localResources.getDrawable(this.l));
      }
      if (this.j == null) {
        break label523;
      }
      locala.findViewById(l.sb__divider).setBackgroundColor(this.j.intValue());
      this.f = ((TextView)locala.findViewById(l.sb__text));
      this.f.setText(this.e);
      this.f.setTypeface(this.G);
      if (this.h != this.a) {
        this.f.setTextColor(this.h);
      }
      this.f.setMaxLines(this.c.c());
      localTextView = (TextView)locala.findViewById(l.sb__action);
      if (TextUtils.isEmpty(this.t)) {
        break label539;
      }
      requestLayout();
      localTextView.setText(this.t);
      localTextView.setTypeface(this.H);
      if (this.u != this.a) {
        localTextView.setTextColor(this.u);
      }
      localTextView.setOnClickListener(new q(this));
      localTextView.setMaxLines(this.c.c());
    }
    for (;;)
    {
      setClickable(true);
      if ((this.J) && (localResources.getBoolean(h.sb__is_swipeable))) {
        setOnTouchListener(new d(this, null, new r(this)));
      }
      return localMarginLayoutParams;
      i1 = localResources.getColor(i.sb__background);
      break;
      label416:
      this.c = com.c.a.a.a.a;
      locala.setMinimumWidth(localResources.getDimensionPixelSize(j.sb__min_width));
      if (this.Q == null) {}
      for (int i2 = localResources.getDimensionPixelSize(j.sb__max_width);; i2 = a.a(paramActivity, this.Q))
      {
        locala.setMaxWidth(i2);
        locala.setBackgroundResource(k.sb__bg);
        ((GradientDrawable)locala.getBackground()).setColor(this.g);
        localMarginLayoutParams = a(paramViewGroup, -2, a(this.c.b(), f1), this.k);
        break;
      }
      label523:
      locala.findViewById(l.sb__divider).setVisibility(8);
      break label193;
      label539:
      localTextView.setVisibility(8);
    }
  }
  
  private static ViewGroup.MarginLayoutParams a(ViewGroup paramViewGroup, int paramInt1, int paramInt2, y paramy)
  {
    if ((paramViewGroup instanceof FrameLayout))
    {
      FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(paramInt1, paramInt2);
      localLayoutParams.gravity = paramy.a();
      return localLayoutParams;
    }
    if ((paramViewGroup instanceof RelativeLayout))
    {
      RelativeLayout.LayoutParams localLayoutParams1 = new RelativeLayout.LayoutParams(paramInt1, paramInt2);
      if (paramy == y.a)
      {
        localLayoutParams1.addRule(10, -1);
        return localLayoutParams1;
      }
      localLayoutParams1.addRule(12, -1);
      return localLayoutParams1;
    }
    if ((paramViewGroup instanceof LinearLayout))
    {
      LinearLayout.LayoutParams localLayoutParams2 = new LinearLayout.LayoutParams(paramInt1, paramInt2);
      localLayoutParams2.gravity = paramy.a();
      return localLayoutParams2;
    }
    throw new IllegalStateException("Requires FrameLayout or RelativeLayout for the parent of Snackbar");
  }
  
  public static n a(Context paramContext)
  {
    return new n(paramContext);
  }
  
  private void a(long paramLong)
  {
    postDelayed(this.S, paramLong);
  }
  
  private void a(Activity paramActivity, Rect paramRect)
  {
    paramRect.bottom = 0;
    paramRect.right = 0;
    paramRect.top = 0;
    paramRect.left = 0;
    if (paramActivity == null) {}
    boolean bool1;
    boolean bool2;
    Rect localRect;
    Point localPoint1;
    Point localPoint2;
    do
    {
      do
      {
        return;
        ViewGroup localViewGroup = (ViewGroup)paramActivity.getWindow().getDecorView();
        Display localDisplay = paramActivity.getWindowManager().getDefaultDisplay();
        bool1 = c(paramActivity);
        bool2 = a(localViewGroup);
        localRect = this.M;
        localPoint1 = this.O;
        localPoint2 = this.N;
        localViewGroup.getWindowVisibleDisplayFrame(localRect);
        a.b(localDisplay, localPoint1);
        a.a(localDisplay, localPoint2);
        if (localPoint2.x >= localPoint1.x) {
          break;
        }
      } while ((!bool1) && (!bool2));
      paramRect.right = Math.max(Math.min(localPoint1.x - localPoint2.x, localPoint1.x - localRect.right), 0);
      return;
    } while ((localPoint2.y >= localPoint1.y) || ((!bool1) && (!bool2)));
    paramRect.bottom = Math.max(Math.min(localPoint1.y - localPoint2.y, localPoint1.y - localRect.bottom), 0);
  }
  
  private void a(Activity paramActivity, ViewGroup.MarginLayoutParams paramMarginLayoutParams, ViewGroup paramViewGroup)
  {
    paramViewGroup.removeView(this);
    if (Build.VERSION.SDK_INT >= 21) {
      for (int i1 = 0; i1 < paramViewGroup.getChildCount(); i1++)
      {
        float f1 = paramViewGroup.getChildAt(i1).getElevation();
        if (f1 > getElevation()) {
          setElevation(f1);
        }
      }
    }
    paramViewGroup.addView(this, paramMarginLayoutParams);
    bringToFront();
    if (Build.VERSION.SDK_INT < 19)
    {
      paramViewGroup.requestLayout();
      paramViewGroup.invalidate();
    }
    this.I = true;
    this.P = paramActivity;
    getViewTreeObserver().addOnPreDrawListener(new s(this));
    if (!this.v)
    {
      if (f()) {
        h();
      }
      return;
    }
    Animation localAnimation = AnimationUtils.loadAnimation(getContext(), a(this.k));
    localAnimation.setAnimationListener(new t(this));
    startAnimation(localAnimation);
  }
  
  private void a(View paramView)
  {
    AccessibilityEvent localAccessibilityEvent = AccessibilityEvent.obtain(8);
    android.support.v4.view.a.a.a(localAccessibilityEvent).a(paramView);
    try
    {
      paramView.sendAccessibilityEventUnchecked(localAccessibilityEvent);
      return;
    }
    catch (IllegalStateException localIllegalStateException) {}
  }
  
  public static void a(View paramView, Drawable paramDrawable)
  {
    if (Build.VERSION.SDK_INT < 16)
    {
      paramView.setBackgroundDrawable(paramDrawable);
      return;
    }
    paramView.setBackground(paramDrawable);
  }
  
  @TargetApi(16)
  private boolean a(ViewGroup paramViewGroup)
  {
    if (Build.VERSION.SDK_INT < 16) {}
    while ((0x200 & paramViewGroup.getWindowSystemUiVisibility()) != 512) {
      return false;
    }
    return true;
  }
  
  public static int b(y paramy)
  {
    if (paramy == y.a) {
      return g.sb__top_out;
    }
    return g.sb__bottom_out;
  }
  
  static boolean b(Context paramContext)
  {
    if (paramContext == null) {
      return true;
    }
    return paramContext.getResources().getBoolean(h.sb__is_phone);
  }
  
  private void c(boolean paramBoolean)
  {
    if (this.K) {
      return;
    }
    this.K = true;
    if ((this.F != null) && (this.I))
    {
      if (!this.x) {
        break label53;
      }
      this.F.e(this);
    }
    while (!paramBoolean)
    {
      i();
      return;
      label53:
      this.F.d(this);
    }
    Animation localAnimation = AnimationUtils.loadAnimation(getContext(), b(this.k));
    localAnimation.setAnimationListener(new v(this));
    startAnimation(localAnimation);
  }
  
  private boolean c(Activity paramActivity)
  {
    if (Build.VERSION.SDK_INT < 19) {}
    while ((0x8000000 & paramActivity.getWindow().getAttributes().flags) == 0) {
      return false;
    }
    return true;
  }
  
  private boolean f()
  {
    return !g();
  }
  
  private boolean g()
  {
    return getDuration() == x.c.a();
  }
  
  private void h()
  {
    postDelayed(this.S, getDuration());
  }
  
  private void i()
  {
    clearAnimation();
    ViewGroup localViewGroup = (ViewGroup)getParent();
    if (localViewGroup != null) {
      localViewGroup.removeView(this);
    }
    if ((this.F != null) && (this.I)) {
      this.F.f(this);
    }
    this.I = false;
    this.x = false;
    this.P = null;
  }
  
  public n a(int paramInt)
  {
    this.g = paramInt;
    return this;
  }
  
  public n a(com.c.a.c.a parama)
  {
    this.A = parama;
    return this;
  }
  
  public n a(x paramx)
  {
    this.d = paramx;
    return this;
  }
  
  public n a(CharSequence paramCharSequence)
  {
    this.e = paramCharSequence;
    if (this.f != null) {
      this.f.setText(this.e);
    }
    return this;
  }
  
  public n a(boolean paramBoolean)
  {
    this.v = paramBoolean;
    return this;
  }
  
  public void a()
  {
    this.x = true;
    b();
  }
  
  public void a(Activity paramActivity)
  {
    this.y = true;
    b(paramActivity);
  }
  
  protected void a(Activity paramActivity, ViewGroup.MarginLayoutParams paramMarginLayoutParams)
  {
    if (this.R)
    {
      paramMarginLayoutParams.topMargin = this.m;
      paramMarginLayoutParams.rightMargin = this.p;
      paramMarginLayoutParams.leftMargin = this.o;
    }
    for (paramMarginLayoutParams.bottomMargin = this.n;; paramMarginLayoutParams.bottomMargin = (this.n + this.i))
    {
      a(paramActivity, this.L);
      paramMarginLayoutParams.rightMargin += this.L.right;
      paramMarginLayoutParams.bottomMargin += this.L.bottom;
      return;
      paramMarginLayoutParams.topMargin = this.m;
      paramMarginLayoutParams.rightMargin = this.p;
      paramMarginLayoutParams.leftMargin = (this.o + this.i);
    }
  }
  
  public n b(int paramInt)
  {
    this.u = paramInt;
    return this;
  }
  
  public n b(CharSequence paramCharSequence)
  {
    this.t = paramCharSequence;
    return this;
  }
  
  public n b(boolean paramBoolean)
  {
    this.w = paramBoolean;
    return this;
  }
  
  public void b()
  {
    c(this.w);
  }
  
  public void b(Activity paramActivity)
  {
    ViewGroup localViewGroup = (ViewGroup)paramActivity.findViewById(16908290);
    ViewGroup.MarginLayoutParams localMarginLayoutParams = a(paramActivity, paramActivity, localViewGroup, b(paramActivity));
    a(paramActivity, localMarginLayoutParams);
    a(paramActivity, localMarginLayoutParams, localViewGroup);
  }
  
  protected void c()
  {
    if (this.K) {}
    while ((ViewGroup)getParent() == null) {
      return;
    }
    ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)getLayoutParams();
    a(this.P, localMarginLayoutParams);
    setLayoutParams(localMarginLayoutParams);
  }
  
  void c(int paramInt)
  {
    d(paramInt);
  }
  
  protected void d(int paramInt)
  {
    if (this.T != null) {
      post(this.T);
    }
  }
  
  public boolean d()
  {
    return this.I;
  }
  
  public boolean e()
  {
    return this.K;
  }
  
  public int getActionColor()
  {
    return this.u;
  }
  
  public CharSequence getActionLabel()
  {
    return this.t;
  }
  
  public int getColor()
  {
    return this.g;
  }
  
  public long getDuration()
  {
    if (this.z == -1L) {
      return this.d.a();
    }
    return this.z;
  }
  
  public int getLineColor()
  {
    return this.j.intValue();
  }
  
  public int getOffset()
  {
    return this.i;
  }
  
  public CharSequence getText()
  {
    return this.e;
  }
  
  public int getTextColor()
  {
    return this.h;
  }
  
  public com.c.a.a.a getType()
  {
    return this.c;
  }
  
  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    if (this.S != null) {
      removeCallbacks(this.S);
    }
    if (this.T != null) {
      removeCallbacks(this.T);
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/c/a/n.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */