package com.c.a;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.View;
import android.view.ViewParent;

@TargetApi(16)
class z
  extends View
{
  public z(Context paramContext)
  {
    super(paramContext);
    setSaveEnabled(false);
    setWillNotDraw(true);
    setVisibility(8);
  }
  
  public void onWindowSystemUiVisibilityChanged(int paramInt)
  {
    super.onWindowSystemUiVisibilityChanged(paramInt);
    ViewParent localViewParent = getParent();
    if ((localViewParent instanceof n)) {
      ((n)localViewParent).c(paramInt);
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/c/a/z.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */