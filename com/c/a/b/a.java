package com.c.a.b;

import android.content.Context;
import android.view.View.MeasureSpec;
import android.widget.LinearLayout;

public class a
  extends LinearLayout
{
  private int a = Integer.MAX_VALUE;
  private int b = Integer.MAX_VALUE;
  
  public a(Context paramContext)
  {
    super(paramContext);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int i = View.MeasureSpec.getSize(paramInt1);
    if (this.a < i)
    {
      int m = View.MeasureSpec.getMode(paramInt1);
      paramInt1 = View.MeasureSpec.makeMeasureSpec(this.a, m);
    }
    int j = View.MeasureSpec.getSize(paramInt2);
    if (this.b < j)
    {
      int k = View.MeasureSpec.getMode(paramInt2);
      paramInt2 = View.MeasureSpec.makeMeasureSpec(this.b, k);
    }
    super.onMeasure(paramInt1, paramInt2);
  }
  
  public void setMaxHeight(int paramInt)
  {
    this.b = paramInt;
    requestLayout();
  }
  
  public void setMaxWidth(int paramInt)
  {
    this.a = paramInt;
    requestLayout();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/c/a/b/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */