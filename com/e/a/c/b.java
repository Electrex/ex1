package com.e.a.c;

import android.util.Log;
import com.e.a.b.f;

public final class b
{
  public static void a()
  {
    com.e.a.a.a(new b());
  }
  
  private void a(f paramf, com.e.a.b.a parama)
  {
    while (!parama.e())
    {
      com.e.a.a.a("RootTools v3.5", paramf.c(parama));
      try
      {
        if (!parama.e()) {
          parama.wait(2000L);
        }
        if ((parama.d()) || (parama.e())) {
          continue;
        }
        if ((!paramf.b) && (!paramf.c))
        {
          Log.e("RootTools v3.5", "Waiting for a command to be executed in a shell that is not executing and not reading! \n\n Command: " + parama.c());
          Exception localException3 = new Exception();
          localException3.setStackTrace(Thread.currentThread().getStackTrace());
          localException3.printStackTrace();
          continue;
        }
      }
      catch (InterruptedException localInterruptedException)
      {
        for (;;)
        {
          localInterruptedException.printStackTrace();
        }
      }
      finally {}
      if ((paramf.b) && (!paramf.c))
      {
        Log.e("RootTools v3.5", "Waiting for a command to be executed in a shell that is executing but not reading! \n\n Command: " + parama.c());
        Exception localException2 = new Exception();
        localException2.setStackTrace(Thread.currentThread().getStackTrace());
        localException2.printStackTrace();
      }
      else
      {
        Log.e("RootTools v3.5", "Waiting for a command to be executed in a shell that is not reading! \n\n Command: " + parama.c());
        Exception localException1 = new Exception();
        localException1.setStackTrace(Thread.currentThread().getStackTrace());
        localException1.printStackTrace();
      }
    }
  }
  
  public boolean b()
  {
    try
    {
      com.e.a.a.a("Checking for Root access");
      a.a = false;
      c localc = new c(this, 2, false, new String[] { "id" });
      f.g().a(localc);
      a(f.g(), localc);
      boolean bool = a.a;
      return bool;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return false;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/e/a/c/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */