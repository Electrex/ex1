package com.e.a;

import android.util.Log;
import com.e.a.c.b;
import java.util.ArrayList;
import java.util.List;

public final class a
{
  public static boolean a = false;
  public static List b = new ArrayList();
  public static boolean c = true;
  public static int d = 20000;
  private static b e = null;
  
  public static void a(b paramb)
  {
    e = paramb;
  }
  
  public static void a(String paramString)
  {
    a(null, paramString, 3, null);
  }
  
  public static void a(String paramString, int paramInt, Exception paramException)
  {
    a(null, paramString, paramInt, paramException);
  }
  
  public static void a(String paramString1, String paramString2)
  {
    a(paramString1, paramString2, 3, null);
  }
  
  public static void a(String paramString1, String paramString2, int paramInt, Exception paramException)
  {
    if ((paramString2 != null) && (!paramString2.equals("")) && (a)) {
      if (paramString1 == null) {
        paramString1 = "RootTools v3.5";
      }
    }
    switch (paramInt)
    {
    default: 
      return;
    case 1: 
      Log.v(paramString1, paramString2);
      return;
    case 2: 
      Log.e(paramString1, paramString2, paramException);
      return;
    }
    Log.d(paramString1, paramString2);
  }
  
  public static boolean a()
  {
    return b().b();
  }
  
  private static final b b()
  {
    if (e == null)
    {
      b.a();
      return e;
    }
    return e;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/e/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */