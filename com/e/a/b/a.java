package com.e.a.b;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.io.File;
import java.io.IOException;

public abstract class a
{
  d a = null;
  Handler b = null;
  boolean c = false;
  String[] d = new String[0];
  boolean e = false;
  Context f = null;
  boolean g = false;
  boolean h = false;
  boolean i = true;
  int j = -1;
  int k = 0;
  int l = com.e.a.a.d;
  
  public a(int paramInt, boolean paramBoolean, String... paramVarArgs)
  {
    this.d = paramVarArgs;
    this.k = paramInt;
    a(paramBoolean);
  }
  
  private void a(boolean paramBoolean)
  {
    this.i = paramBoolean;
    if ((Looper.myLooper() != null) && (paramBoolean))
    {
      com.e.a.a.a("CommandHandler created");
      this.b = new c(this, null);
      return;
    }
    com.e.a.a.a("CommandHandler not created");
  }
  
  protected void a()
  {
    this.c = false;
    this.g = true;
    notifyAll();
  }
  
  protected void a(int paramInt)
  {
    try
    {
      this.j = paramInt;
      return;
    }
    finally {}
  }
  
  public abstract void a(int paramInt1, int paramInt2);
  
  public abstract void a(int paramInt, String paramString);
  
  public void a(String paramString)
  {
    try
    {
      f.e();
      com.e.a.a.a("Terminating all shells.");
      b(paramString);
      return;
    }
    catch (IOException localIOException) {}
  }
  
  protected void b()
  {
    if (!this.h) {
      try
      {
        if ((this.b != null) && (this.i))
        {
          Message localMessage = this.b.obtainMessage();
          Bundle localBundle = new Bundle();
          localBundle.putInt("action", 2);
          localMessage.setData(localBundle);
          this.b.sendMessage(localMessage);
        }
        for (;;)
        {
          com.e.a.a.a("Command " + this.k + " finished.");
          a();
          return;
          a(this.k, this.j);
        }
        return;
      }
      finally {}
    }
  }
  
  public abstract void b(int paramInt, String paramString);
  
  /* Error */
  protected void b(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 31	com/e/a/b/a:b	Landroid/os/Handler;
    //   6: ifnull +109 -> 115
    //   9: aload_0
    //   10: getfield 47	com/e/a/b/a:i	Z
    //   13: ifeq +102 -> 115
    //   16: aload_0
    //   17: getfield 31	com/e/a/b/a:b	Landroid/os/Handler;
    //   20: invokevirtual 100	android/os/Handler:obtainMessage	()Landroid/os/Message;
    //   23: astore_3
    //   24: new 102	android/os/Bundle
    //   27: dup
    //   28: invokespecial 103	android/os/Bundle:<init>	()V
    //   31: astore 4
    //   33: aload 4
    //   35: ldc 105
    //   37: iconst_3
    //   38: invokevirtual 109	android/os/Bundle:putInt	(Ljava/lang/String;I)V
    //   41: aload 4
    //   43: ldc -113
    //   45: aload_1
    //   46: invokevirtual 147	android/os/Bundle:putString	(Ljava/lang/String;Ljava/lang/String;)V
    //   49: aload_3
    //   50: aload 4
    //   52: invokevirtual 115	android/os/Message:setData	(Landroid/os/Bundle;)V
    //   55: aload_0
    //   56: getfield 31	com/e/a/b/a:b	Landroid/os/Handler;
    //   59: aload_3
    //   60: invokevirtual 119	android/os/Handler:sendMessage	(Landroid/os/Message;)Z
    //   63: pop
    //   64: new 121	java/lang/StringBuilder
    //   67: dup
    //   68: invokespecial 122	java/lang/StringBuilder:<init>	()V
    //   71: ldc 124
    //   73: invokevirtual 128	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   76: aload_0
    //   77: getfield 51	com/e/a/b/a:k	I
    //   80: invokevirtual 131	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   83: ldc -107
    //   85: invokevirtual 128	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   88: aload_1
    //   89: invokevirtual 128	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   92: invokevirtual 137	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   95: invokestatic 71	com/e/a/a:a	(Ljava/lang/String;)V
    //   98: aload_0
    //   99: iconst_m1
    //   100: invokevirtual 151	com/e/a/b/a:a	(I)V
    //   103: aload_0
    //   104: iconst_1
    //   105: putfield 45	com/e/a/b/a:h	Z
    //   108: aload_0
    //   109: invokevirtual 139	com/e/a/b/a:a	()V
    //   112: aload_0
    //   113: monitorexit
    //   114: return
    //   115: aload_0
    //   116: aload_0
    //   117: getfield 51	com/e/a/b/a:k	I
    //   120: aload_1
    //   121: invokevirtual 153	com/e/a/b/a:b	(ILjava/lang/String;)V
    //   124: goto -60 -> 64
    //   127: astore_2
    //   128: aload_0
    //   129: monitorexit
    //   130: aload_2
    //   131: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	132	0	this	a
    //   0	132	1	paramString	String
    //   127	4	2	localObject	Object
    //   23	37	3	localMessage	Message
    //   31	20	4	localBundle	Bundle
    // Exception table:
    //   from	to	target	type
    //   2	64	127	finally
    //   64	114	127	finally
    //   115	124	127	finally
    //   128	130	127	finally
  }
  
  public String c()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    boolean bool = this.e;
    int m = 0;
    if (bool)
    {
      String str = this.f.getFilesDir().getPath();
      while (m < this.d.length)
      {
        localStringBuilder.append("dalvikvm -cp " + str + "/anbuild.dex" + " com.android.internal.util.WithFramework" + " com.stericson.RootTools.containers.RootClass " + this.d[m]);
        localStringBuilder.append('\n');
        m++;
      }
    }
    while (m < this.d.length)
    {
      localStringBuilder.append(this.d[m]);
      localStringBuilder.append('\n');
      m++;
    }
    return localStringBuilder.toString();
  }
  
  protected void c(int paramInt, String paramString)
  {
    if ((this.b != null) && (this.i))
    {
      Message localMessage = this.b.obtainMessage();
      Bundle localBundle = new Bundle();
      localBundle.putInt("action", 1);
      localBundle.putString("text", paramString);
      localMessage.setData(localBundle);
      this.b.sendMessage(localMessage);
      return;
    }
    a(paramInt, paramString);
  }
  
  public boolean d()
  {
    return this.c;
  }
  
  public boolean e()
  {
    return this.g;
  }
  
  protected void f()
  {
    this.a = new d(this, null);
    this.a.setPriority(1);
    this.a.start();
    this.c = true;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/e/a/b/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */