package com.a.a.a.a;

import org.json.JSONObject;

public class h
{
  public final String a;
  public final String b;
  public final String c;
  public final boolean d;
  public final String e;
  public final Double f;
  public final String g;
  
  public h(JSONObject paramJSONObject)
  {
    String str = paramJSONObject.getString("type");
    if (str == null) {
      str = "inapp";
    }
    this.a = paramJSONObject.getString("productId");
    this.b = paramJSONObject.getString("title");
    this.c = paramJSONObject.getString("description");
    this.d = str.equalsIgnoreCase("subs");
    this.e = paramJSONObject.getString("price_currency_code");
    this.f = Double.valueOf(paramJSONObject.getDouble("price_amount_micros") / 1000000.0D);
    this.g = paramJSONObject.getString("price");
  }
  
  public String toString()
  {
    Object[] arrayOfObject = new Object[6];
    arrayOfObject[0] = this.a;
    arrayOfObject[1] = this.b;
    arrayOfObject[2] = this.c;
    arrayOfObject[3] = this.f;
    arrayOfObject[4] = this.e;
    arrayOfObject[5] = this.g;
    return String.format("%s: %s(%s) %f in %s (%s)", arrayOfObject);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/a/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */