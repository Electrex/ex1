package com.a.a.a.a;

import android.content.Context;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Pattern;

class b
  extends a
{
  private HashMap a = new HashMap();
  private String b;
  private String c;
  
  public b(Context paramContext, String paramString)
  {
    super(paramContext);
    this.b = paramString;
    g();
  }
  
  private String e()
  {
    return b() + this.b;
  }
  
  private String f()
  {
    return e() + ".version";
  }
  
  private void g()
  {
    String[] arrayOfString1 = b(e(), "").split(Pattern.quote("#####"));
    int i = arrayOfString1.length;
    int j = 0;
    if (j < i)
    {
      String str = arrayOfString1[j];
      String[] arrayOfString2;
      if (!TextUtils.isEmpty(str))
      {
        arrayOfString2 = str.split(Pattern.quote(">>>>>"));
        if (arrayOfString2.length <= 2) {
          break label94;
        }
        this.a.put(arrayOfString2[0], new f(arrayOfString2[1], arrayOfString2[2]));
      }
      for (;;)
      {
        j++;
        break;
        label94:
        if (arrayOfString2.length > 1) {
          this.a.put(arrayOfString2[0], new f(arrayOfString2[1], null));
        }
      }
    }
    this.c = i();
  }
  
  private void h()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.a.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      f localf = (f)this.a.get(str);
      localArrayList.add(str + ">>>>>" + localf.a + ">>>>>" + localf.b);
    }
    a(e(), TextUtils.join("#####", localArrayList));
    this.c = Long.toString(new Date().getTime());
    a(f(), this.c);
  }
  
  private String i()
  {
    return b(f(), "0");
  }
  
  private void j()
  {
    if (!this.c.equalsIgnoreCase(i()))
    {
      this.a.clear();
      g();
    }
  }
  
  public void a(String paramString1, String paramString2, String paramString3)
  {
    j();
    if (!this.a.containsKey(paramString1))
    {
      this.a.put(paramString1, new f(paramString2, paramString3));
      h();
    }
  }
  
  public boolean a(String paramString)
  {
    j();
    return this.a.containsKey(paramString);
  }
  
  public f b(String paramString)
  {
    j();
    if (this.a.containsKey(paramString)) {
      return (f)this.a.get(paramString);
    }
    return null;
  }
  
  public void c(String paramString)
  {
    j();
    if (this.a.containsKey(paramString))
    {
      this.a.remove(paramString);
      h();
    }
  }
  
  public void d()
  {
    j();
    this.a.clear();
    h();
  }
  
  public String toString()
  {
    return TextUtils.join(", ", this.a.keySet());
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/a/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */