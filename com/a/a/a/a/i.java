package com.a.a.a.a;

import java.util.Date;
import org.json.JSONObject;

public class i
{
  public final String a;
  public final String b;
  public final String c;
  public final Date d;
  public final f e;
  
  public i(f paramf)
  {
    JSONObject localJSONObject = new JSONObject(paramf.a);
    this.e = paramf;
    this.a = localJSONObject.getString("productId");
    this.b = localJSONObject.getString("orderId");
    this.c = localJSONObject.getString("purchaseToken");
    this.d = new Date(localJSONObject.getLong("purchaseTime"));
  }
  
  public String toString()
  {
    Object[] arrayOfObject = new Object[5];
    arrayOfObject[0] = this.a;
    arrayOfObject[1] = this.d;
    arrayOfObject[2] = this.b;
    arrayOfObject[3] = this.c;
    arrayOfObject[4] = this.e.b;
    return String.format("%s purchased at %s(%s). Token: %s, Signature: %s", arrayOfObject);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/a/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */