package com.a.a.a.a;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

public class c
  extends a
{
  private com.android.vending.a.a a;
  private String b;
  private String c;
  private b d;
  private b e;
  private e f;
  private ServiceConnection g = new d(this);
  
  public c(Context paramContext, String paramString, e parame)
  {
    super(paramContext);
    this.c = paramString;
    this.f = parame;
    this.b = paramContext.getApplicationContext().getPackageName();
    this.d = new b(paramContext, ".products.cache.v2_6");
    this.e = new b(paramContext, ".subscriptions.cache.v2_6");
    g();
  }
  
  private boolean a(Activity paramActivity, String paramString1, String paramString2)
  {
    if ((!d()) || (TextUtils.isEmpty(paramString1)) || (TextUtils.isEmpty(paramString2))) {
      return false;
    }
    int i;
    try
    {
      String str = paramString2 + ":" + UUID.randomUUID().toString();
      g(str);
      Bundle localBundle = this.a.a(3, this.b, paramString1, paramString2, str);
      if (localBundle == null) {
        break label263;
      }
      i = localBundle.getInt("RESPONSE_CODE");
      if (i == 0)
      {
        PendingIntent localPendingIntent = (PendingIntent)localBundle.getParcelable("BUY_INTENT");
        if (paramActivity != null) {
          paramActivity.startIntentSenderForResult(localPendingIntent.getIntentSender(), 2061984, new Intent(), 0, 0, 0);
        } else if (this.f != null) {
          this.f.a(103, null);
        }
      }
    }
    catch (Exception localException)
    {
      Log.e("iabv3", localException.toString());
      return false;
    }
    if (i == 7)
    {
      if ((!a(paramString1)) && (!b(paramString1))) {
        e();
      }
      if (this.f != null)
      {
        i locali = e(paramString1);
        if (locali == null) {
          locali = f(paramString1);
        }
        this.f.a(paramString1, locali);
      }
    }
    else if (this.f != null)
    {
      this.f.a(101, null);
    }
    label263:
    return true;
  }
  
  private boolean a(String paramString, b paramb)
  {
    if (!d()) {
      return false;
    }
    try
    {
      Bundle localBundle = this.a.a(3, this.b, paramString, null);
      if (localBundle.getInt("RESPONSE_CODE") == 0)
      {
        paramb.d();
        ArrayList localArrayList1 = localBundle.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
        ArrayList localArrayList2 = localBundle.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
        int i = 0;
        if (i < localArrayList1.size())
        {
          String str1 = (String)localArrayList1.get(i);
          JSONObject localJSONObject = new JSONObject(str1);
          if ((localArrayList2 != null) && (localArrayList2.size() > i)) {}
          for (String str2 = (String)localArrayList2.get(i);; str2 = null)
          {
            paramb.a(localJSONObject.getString("productId"), str1, str2);
            i++;
            break;
          }
        }
      }
      return true;
    }
    catch (Exception localException)
    {
      if (this.f != null) {
        this.f.a(100, localException);
      }
      Log.e("iabv3", localException.toString());
    }
    return false;
  }
  
  private boolean a(String paramString1, String paramString2, String paramString3)
  {
    try
    {
      if (TextUtils.isEmpty(this.c)) {
        return true;
      }
      boolean bool = g.a(paramString1, this.c, paramString2, paramString3);
      return bool;
    }
    catch (Exception localException) {}
    return false;
  }
  
  private i b(String paramString, b paramb)
  {
    f localf = paramb.b(paramString);
    if ((localf != null) && (!TextUtils.isEmpty(localf.a))) {
      try
      {
        i locali = new i(localf);
        return locali;
      }
      catch (JSONException localJSONException)
      {
        Log.e("iabv3", "Failed to load saved purchase details for " + paramString);
      }
    }
    return null;
  }
  
  private h c(String paramString1, String paramString2)
  {
    if (this.a != null) {}
    try
    {
      ArrayList localArrayList = new ArrayList();
      localArrayList.add(paramString1);
      Bundle localBundle1 = new Bundle();
      localBundle1.putStringArrayList("ITEM_ID_LIST", localArrayList);
      Bundle localBundle2 = this.a.a(3, this.b, paramString2, localBundle1);
      int i = localBundle2.getInt("RESPONSE_CODE");
      if (i == 0)
      {
        Iterator localIterator = localBundle2.getStringArrayList("DETAILS_LIST").iterator();
        JSONObject localJSONObject;
        do
        {
          if (!localIterator.hasNext()) {
            break;
          }
          localJSONObject = new JSONObject((String)localIterator.next());
        } while (!paramString1.equals(localJSONObject.getString("productId")));
        return new h(localJSONObject);
      }
      if (this.f != null) {
        this.f.a(i, null);
      }
      Object[] arrayOfObject2 = new Object[2];
      arrayOfObject2[0] = paramString1;
      arrayOfObject2[1] = Integer.valueOf(i);
      Log.e("iabv3", String.format("Failed to retrieve info for %s: error %d", arrayOfObject2));
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object[] arrayOfObject1 = new Object[1];
        arrayOfObject1[0] = localException.toString();
        Log.e("iabv3", String.format("Failed to call getSkuDetails %s", arrayOfObject1));
      }
    }
    return null;
  }
  
  private void g()
  {
    try
    {
      Intent localIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
      localIntent.setPackage("com.android.vending");
      a().bindService(localIntent, this.g, 1);
      return;
    }
    catch (Exception localException)
    {
      Log.e("iabv3", localException.toString());
    }
  }
  
  private void g(String paramString)
  {
    a(b() + ".purchase.last.v2_6", paramString);
  }
  
  private boolean h()
  {
    return a(b() + ".products.restored.v2_6", false);
  }
  
  private String i()
  {
    return b(b() + ".purchase.last.v2_6", null);
  }
  
  public boolean a(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramInt1 != 2061984) {
      return false;
    }
    if (paramIntent == null)
    {
      Log.e("iabv3", "handleActivityResult: data is null!");
      return false;
    }
    int i = paramIntent.getIntExtra("RESPONSE_CODE", 0);
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = Integer.valueOf(paramInt2);
    arrayOfObject[1] = Integer.valueOf(i);
    Log.d("iabv3", String.format("resultCode = %d, responseCode = %d", arrayOfObject));
    String str1 = i();
    if ((paramInt2 == -1) && (i == 0) && (!TextUtils.isEmpty(str1)))
    {
      String str2 = paramIntent.getStringExtra("INAPP_PURCHASE_DATA");
      String str3 = paramIntent.getStringExtra("INAPP_DATA_SIGNATURE");
      String str5;
      try
      {
        JSONObject localJSONObject = new JSONObject(str2);
        String str4 = localJSONObject.getString("productId");
        str5 = localJSONObject.getString("developerPayload");
        if (str5 == null) {
          str5 = "";
        }
        boolean bool = str1.startsWith("subs");
        if (!str1.equals(str5)) {
          break label320;
        }
        if (a(str4, str2, str3))
        {
          if (bool) {}
          for (b localb = this.e;; localb = this.d)
          {
            localb.a(str4, str2, str3);
            if (this.f == null) {
              break;
            }
            this.f.a(str4, new i(new f(str2, str3)));
            break;
          }
        }
        Log.e("iabv3", "Public key signature doesn't match!");
        if (this.f == null) {
          break label387;
        }
        this.f.a(102, null);
      }
      catch (Exception localException)
      {
        Log.e("iabv3", localException.toString());
        if (this.f == null) {
          break label387;
        }
      }
      this.f.a(110, null);
      break label387;
      label320:
      Log.e("iabv3", String.format("Payload mismatch: %s != %s", new Object[] { str1, str5 }));
      if (this.f != null) {
        this.f.a(102, null);
      }
    }
    else if (this.f != null)
    {
      this.f.a(i, null);
    }
    label387:
    return true;
  }
  
  public boolean a(Activity paramActivity, String paramString)
  {
    return a(paramActivity, paramString, "inapp");
  }
  
  public boolean a(String paramString)
  {
    return this.d.a(paramString);
  }
  
  public boolean b(String paramString)
  {
    return this.e.a(paramString);
  }
  
  public void c()
  {
    if ((this.g != null) && (a() != null)) {}
    try
    {
      a().unbindService(this.g);
      this.a = null;
      this.d.c();
      super.c();
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Log.e("iabv3", localException.toString());
      }
    }
  }
  
  public boolean c(String paramString)
  {
    if (!d()) {}
    for (;;)
    {
      return false;
      try
      {
        i locali = b(paramString, this.d);
        if ((locali != null) && (!TextUtils.isEmpty(locali.c)))
        {
          int i = this.a.b(3, this.b, locali.c);
          if (i == 0)
          {
            this.d.c(paramString);
            Log.d("iabv3", "Successfully consumed " + paramString + " purchase.");
            return true;
          }
          if (this.f != null) {
            this.f.a(i, null);
          }
          Object[] arrayOfObject = new Object[2];
          arrayOfObject[0] = paramString;
          arrayOfObject[1] = Integer.valueOf(i);
          Log.e("iabv3", String.format("Failed to consume %s: error %d", arrayOfObject));
          return false;
        }
      }
      catch (Exception localException)
      {
        Log.e("iabv3", localException.toString());
      }
    }
    return false;
  }
  
  public h d(String paramString)
  {
    return c(paramString, "inapp");
  }
  
  public boolean d()
  {
    return this.a != null;
  }
  
  public i e(String paramString)
  {
    return b(paramString, this.d);
  }
  
  public boolean e()
  {
    return (d()) && (a("inapp", this.d)) && (a("subs", this.e));
  }
  
  public i f(String paramString)
  {
    return b(paramString, this.e);
  }
  
  public void f()
  {
    a(b() + ".products.restored.v2_6", Boolean.valueOf(true));
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/a/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */