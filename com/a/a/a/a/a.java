package com.a.a.a.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import java.lang.ref.WeakReference;

class a
{
  private WeakReference a;
  
  public a(Context paramContext)
  {
    this.a = new WeakReference(paramContext);
  }
  
  private SharedPreferences d()
  {
    if (this.a.get() != null) {
      return PreferenceManager.getDefaultSharedPreferences((Context)this.a.get());
    }
    return null;
  }
  
  public Context a()
  {
    return (Context)this.a.get();
  }
  
  protected boolean a(String paramString, Boolean paramBoolean)
  {
    SharedPreferences localSharedPreferences = d();
    if (localSharedPreferences != null)
    {
      SharedPreferences.Editor localEditor = localSharedPreferences.edit();
      localEditor.putBoolean(paramString, paramBoolean.booleanValue());
      localEditor.commit();
      return true;
    }
    return false;
  }
  
  protected boolean a(String paramString1, String paramString2)
  {
    SharedPreferences localSharedPreferences = d();
    if (localSharedPreferences != null)
    {
      SharedPreferences.Editor localEditor = localSharedPreferences.edit();
      localEditor.putString(paramString1, paramString2);
      localEditor.commit();
      return true;
    }
    return false;
  }
  
  protected boolean a(String paramString, boolean paramBoolean)
  {
    SharedPreferences localSharedPreferences = d();
    if (localSharedPreferences != null) {
      paramBoolean = localSharedPreferences.getBoolean(paramString, paramBoolean);
    }
    return paramBoolean;
  }
  
  protected String b()
  {
    return ((Context)this.a.get()).getPackageName() + "_preferences";
  }
  
  protected String b(String paramString1, String paramString2)
  {
    SharedPreferences localSharedPreferences = d();
    if (localSharedPreferences != null) {
      paramString2 = localSharedPreferences.getString(paramString1, paramString2);
    }
    return paramString2;
  }
  
  public void c()
  {
    if (this.a != null) {
      this.a.clear();
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/a/a/a/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */