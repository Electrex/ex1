package com.getbase.floatingactionbutton;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;

class d
  extends LayerDrawable
{
  private final int a;
  
  public d(int paramInt, Drawable... paramVarArgs)
  {
    super(paramVarArgs);
    this.a = paramInt;
  }
  
  public void draw(Canvas paramCanvas)
  {
    Rect localRect = getBounds();
    paramCanvas.saveLayerAlpha(localRect.left, localRect.top, localRect.right, localRect.bottom, this.a, 31);
    super.draw(paramCanvas);
    paramCanvas.restore();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/getbase/floatingactionbutton/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */