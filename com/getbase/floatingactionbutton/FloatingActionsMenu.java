package com.getbase.floatingactionbutton;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.TextView;

public class FloatingActionsMenu
  extends ViewGroup
{
  private static Interpolator v = new OvershootInterpolator();
  private static Interpolator w = new DecelerateInterpolator(3.0F);
  private static Interpolator x = new DecelerateInterpolator();
  private int a;
  private int b;
  private int c;
  private int d;
  private boolean e;
  private int f;
  private int g;
  private int h;
  private int i;
  private boolean j;
  private AnimatorSet k = new AnimatorSet().setDuration(300L);
  private AnimatorSet l = new AnimatorSet().setDuration(300L);
  private a m;
  private i n;
  private int o;
  private int p;
  private int q;
  private int r;
  private int s;
  private p t;
  private h u;
  
  public FloatingActionsMenu(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public FloatingActionsMenu(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a(paramContext, paramAttributeSet);
  }
  
  public FloatingActionsMenu(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    a(paramContext, paramAttributeSet);
  }
  
  private int a(int paramInt)
  {
    return getResources().getColor(paramInt);
  }
  
  private void a(Context paramContext)
  {
    this.m = new e(this, paramContext);
    this.m.setId(n.fab_expand_menu_button);
    this.m.setSize(this.d);
    this.m.setOnClickListener(new f(this));
    addView(this.m, super.generateDefaultLayoutParams());
  }
  
  private void a(Context paramContext, AttributeSet paramAttributeSet)
  {
    this.g = ((int)(getResources().getDimension(l.fab_actions_spacing) - getResources().getDimension(l.fab_shadow_radius) - getResources().getDimension(l.fab_shadow_offset)));
    this.h = getResources().getDimensionPixelSize(l.fab_labels_margin);
    this.i = getResources().getDimensionPixelSize(l.fab_shadow_offset);
    this.t = new p(this);
    setTouchDelegate(this.t);
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, o.FloatingActionsMenu, 0, 0);
    this.a = localTypedArray.getColor(o.FloatingActionsMenu_fab_addButtonPlusIconColor, a(17170443));
    this.b = localTypedArray.getColor(o.FloatingActionsMenu_fab_addButtonColorNormal, a(17170451));
    this.c = localTypedArray.getColor(o.FloatingActionsMenu_fab_addButtonColorPressed, a(17170450));
    this.d = localTypedArray.getInt(o.FloatingActionsMenu_fab_addButtonSize, 0);
    this.e = localTypedArray.getBoolean(o.FloatingActionsMenu_fab_addButtonStrokeVisible, true);
    this.f = localTypedArray.getInt(o.FloatingActionsMenu_fab_expandDirection, 0);
    this.q = localTypedArray.getResourceId(o.FloatingActionsMenu_fab_labelStyle, 0);
    this.r = localTypedArray.getInt(o.FloatingActionsMenu_fab_labelsPosition, 0);
    localTypedArray.recycle();
    if ((this.q != 0) && (g())) {
      throw new IllegalStateException("Action labels in horizontal expand orientation is not supported.");
    }
    a(paramContext);
  }
  
  private int b(int paramInt)
  {
    return paramInt * 12 / 10;
  }
  
  private boolean g()
  {
    return (this.f == 2) || (this.f == 3);
  }
  
  private void h()
  {
    ContextThemeWrapper localContextThemeWrapper = new ContextThemeWrapper(getContext(), this.q);
    int i1 = 0;
    if (i1 < this.s)
    {
      FloatingActionButton localFloatingActionButton = (FloatingActionButton)getChildAt(i1);
      String str = localFloatingActionButton.getTitle();
      if ((localFloatingActionButton == this.m) || (str == null) || (localFloatingActionButton.getTag(n.fab_label) != null)) {}
      for (;;)
      {
        i1++;
        break;
        TextView localTextView = new TextView(localContextThemeWrapper);
        localTextView.setTextAppearance(getContext(), this.q);
        localTextView.setText(localFloatingActionButton.getTitle());
        addView(localTextView);
        localFloatingActionButton.setTag(n.fab_label, localTextView);
      }
    }
  }
  
  public void a()
  {
    if (this.j)
    {
      this.j = false;
      this.t.a(false);
      this.l.start();
      this.k.cancel();
      if (this.u != null) {
        this.u.b();
      }
    }
  }
  
  public void b()
  {
    if (this.j)
    {
      a();
      return;
    }
    c();
  }
  
  public void c()
  {
    if (!this.j)
    {
      this.j = true;
      this.t.a(true);
      this.l.cancel();
      this.k.start();
      if (this.u != null) {
        this.u.a();
      }
    }
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return super.checkLayoutParams(paramLayoutParams);
  }
  
  protected ViewGroup.LayoutParams generateDefaultLayoutParams()
  {
    return new g(this, super.generateDefaultLayoutParams());
  }
  
  public ViewGroup.LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
  {
    return new g(this, super.generateLayoutParams(paramAttributeSet));
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return new g(this, super.generateLayoutParams(paramLayoutParams));
  }
  
  protected void onFinishInflate()
  {
    super.onFinishInflate();
    bringChildToFront(this.m);
    this.s = getChildCount();
    if (this.q != 0) {
      h();
    }
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    switch (this.f)
    {
    default: 
    case 0: 
    case 1: 
      int i8;
      int i9;
      label77:
      int i10;
      label97:
      int i12;
      int i13;
      label169:
      int i14;
      label183:
      int i15;
      label191:
      do
      {
        return;
        if (this.f != 0) {
          break;
        }
        i8 = 1;
        if (paramBoolean) {
          this.t.a();
        }
        if (i8 == 0) {
          break label235;
        }
        i9 = paramInt4 - paramInt2 - this.m.getMeasuredHeight();
        if (this.r != 0) {
          break label241;
        }
        i10 = paramInt3 - paramInt1 - this.o / 2;
        int i11 = i10 - this.m.getMeasuredWidth() / 2;
        this.m.layout(i11, i9, i11 + this.m.getMeasuredWidth(), i9 + this.m.getMeasuredHeight());
        i12 = this.o / 2 + this.h;
        if (this.r != 0) {
          break label252;
        }
        i13 = i10 - i12;
        if (i8 == 0) {
          break label262;
        }
        i14 = i9 - this.g;
        i15 = -1 + this.s;
      } while (i15 < 0);
      View localView2 = getChildAt(i15);
      if ((localView2 == this.m) || (localView2.getVisibility() == 8)) {}
      for (;;)
      {
        i15--;
        break label191;
        i8 = 0;
        break;
        label235:
        i9 = 0;
        break label77;
        label241:
        i10 = this.o / 2;
        break label97;
        label252:
        i13 = i10 + i12;
        break label169;
        label262:
        i14 = i9 + this.m.getMeasuredHeight() + this.g;
        break label183;
        int i16 = i10 - localView2.getMeasuredWidth() / 2;
        int i17;
        label309:
        float f4;
        float f5;
        label352:
        float f6;
        label369:
        View localView3;
        int i18;
        label468:
        int i19;
        label479:
        float f7;
        if (i8 != 0)
        {
          i17 = i14 - localView2.getMeasuredHeight();
          localView2.layout(i16, i17, i16 + localView2.getMeasuredWidth(), i17 + localView2.getMeasuredHeight());
          f4 = i9 - i17;
          if (!this.j) {
            break label718;
          }
          f5 = 0.0F;
          localView2.setTranslationY(f5);
          if (!this.j) {
            break label725;
          }
          f6 = 1.0F;
          localView2.setAlpha(f6);
          g localg2 = (g)localView2.getLayoutParams();
          g.a(localg2).setFloatValues(new float[] { 0.0F, f4 });
          g.b(localg2).setFloatValues(new float[] { f4, 0.0F });
          localg2.a(localView2);
          localView3 = (View)localView2.getTag(n.fab_label);
          if (localView3 != null)
          {
            if (this.r != 0) {
              break label731;
            }
            i18 = i13 - localView3.getMeasuredWidth();
            if (this.r != 0) {
              break label744;
            }
            i19 = i18;
            if (this.r == 0) {
              i18 = i13;
            }
            int i20 = i17 - this.i + (localView2.getMeasuredHeight() - localView3.getMeasuredHeight()) / 2;
            localView3.layout(i19, i20, i18, i20 + localView3.getMeasuredHeight());
            Rect localRect = new Rect(Math.min(i16, i19), i17 - this.g / 2, Math.max(i16 + localView2.getMeasuredWidth(), i18), i17 + localView2.getMeasuredHeight() + this.g / 2);
            this.t.a(new TouchDelegate(localRect, localView2));
            if (!this.j) {
              break label751;
            }
            f7 = 0.0F;
            label613:
            localView3.setTranslationY(f7);
            if (!this.j) {
              break label758;
            }
          }
        }
        label718:
        label725:
        label731:
        label744:
        label751:
        label758:
        for (float f8 = 1.0F;; f8 = 0.0F)
        {
          localView3.setAlpha(f8);
          g localg3 = (g)localView3.getLayoutParams();
          g.a(localg3).setFloatValues(new float[] { 0.0F, f4 });
          g.b(localg3).setFloatValues(new float[] { f4, 0.0F });
          localg3.a(localView3);
          if (i8 == 0) {
            break label764;
          }
          i14 = i17 - this.g;
          break;
          i17 = i14;
          break label309;
          f5 = f4;
          break label352;
          f6 = 0.0F;
          break label369;
          i18 = i13 + localView3.getMeasuredWidth();
          break label468;
          i19 = i13;
          break label479;
          f7 = f4;
          break label613;
        }
        label764:
        i14 = i17 + localView2.getMeasuredHeight() + this.g;
      }
    }
    int i1;
    label793:
    int i2;
    label812:
    int i3;
    int i4;
    label883:
    int i5;
    label891:
    View localView1;
    if (this.f == 2)
    {
      i1 = 1;
      if (i1 == 0) {
        break label935;
      }
      i2 = paramInt3 - paramInt1 - this.m.getMeasuredWidth();
      i3 = paramInt4 - paramInt2 - this.p + (this.p - this.m.getMeasuredHeight()) / 2;
      this.m.layout(i2, i3, i2 + this.m.getMeasuredWidth(), i3 + this.m.getMeasuredHeight());
      if (i1 == 0) {
        break label941;
      }
      i4 = i2 - this.g;
      i5 = -1 + this.s;
      if (i5 >= 0)
      {
        localView1 = getChildAt(i5);
        if ((localView1 != this.m) && (localView1.getVisibility() != 8)) {
          break label961;
        }
      }
    }
    for (;;)
    {
      i5--;
      break label891;
      break;
      i1 = 0;
      break label793;
      label935:
      i2 = 0;
      break label812;
      label941:
      i4 = i2 + this.m.getMeasuredWidth() + this.g;
      break label883;
      label961:
      int i6;
      label976:
      float f1;
      float f2;
      if (i1 != 0)
      {
        i6 = i4 - localView1.getMeasuredWidth();
        int i7 = i3 + (this.m.getMeasuredHeight() - localView1.getMeasuredHeight()) / 2;
        localView1.layout(i6, i7, i6 + localView1.getMeasuredWidth(), i7 + localView1.getMeasuredHeight());
        f1 = i2 - i6;
        if (!this.j) {
          break label1144;
        }
        f2 = 0.0F;
        label1039:
        localView1.setTranslationX(f2);
        if (!this.j) {
          break label1151;
        }
      }
      label1144:
      label1151:
      for (float f3 = 1.0F;; f3 = 0.0F)
      {
        localView1.setAlpha(f3);
        g localg1 = (g)localView1.getLayoutParams();
        g.a(localg1).setFloatValues(new float[] { 0.0F, f1 });
        g.b(localg1).setFloatValues(new float[] { f1, 0.0F });
        localg1.a(localView1);
        if (i1 == 0) {
          break label1157;
        }
        i4 = i6 - this.g;
        break;
        i6 = i4;
        break label976;
        f2 = f1;
        break label1039;
      }
      label1157:
      i4 = i6 + localView1.getMeasuredWidth() + this.g;
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    measureChildren(paramInt1, paramInt2);
    this.o = 0;
    this.p = 0;
    int i1 = 0;
    int i2 = 0;
    int i3 = 0;
    int i4 = 0;
    while (i1 < this.s)
    {
      View localView = getChildAt(i1);
      int i7;
      int i8;
      if (localView.getVisibility() == 8)
      {
        i7 = i4;
        i8 = i3;
        i1++;
        i3 = i8;
        i4 = i7;
      }
      else
      {
        switch (this.f)
        {
        }
        for (;;)
        {
          i7 = i4;
          int i9;
          for (i8 = i3; !g(); i8 = i9)
          {
            TextView localTextView = (TextView)localView.getTag(n.fab_label);
            if (localTextView == null) {
              break;
            }
            i2 = Math.max(i2, localTextView.getMeasuredWidth());
            break;
            this.o = Math.max(this.o, localView.getMeasuredWidth());
            i9 = i3 + localView.getMeasuredHeight();
            i7 = i4;
          }
          i4 += localView.getMeasuredWidth();
          this.p = Math.max(this.p, localView.getMeasuredHeight());
        }
      }
    }
    if (!g())
    {
      int i5 = this.o;
      int i6 = 0;
      if (i2 > 0) {
        i6 = i2 + this.h;
      }
      i4 = i5 + i6;
      switch (this.f)
      {
      }
    }
    for (;;)
    {
      setMeasuredDimension(i4, i3);
      return;
      i3 = this.p;
      break;
      i3 = b(i3 + this.g * (-1 + getChildCount()));
      continue;
      i4 = b(i4 + this.g * (-1 + getChildCount()));
    }
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable)
  {
    if ((paramParcelable instanceof FloatingActionsMenu.SavedState))
    {
      FloatingActionsMenu.SavedState localSavedState = (FloatingActionsMenu.SavedState)paramParcelable;
      this.j = localSavedState.a;
      this.t.a(this.j);
      i locali;
      if (this.n != null)
      {
        locali = this.n;
        if (!this.j) {
          break label70;
        }
      }
      label70:
      for (float f1 = 135.0F;; f1 = 0.0F)
      {
        locali.setRotation(f1);
        super.onRestoreInstanceState(localSavedState.getSuperState());
        return;
      }
    }
    super.onRestoreInstanceState(paramParcelable);
  }
  
  public Parcelable onSaveInstanceState()
  {
    FloatingActionsMenu.SavedState localSavedState = new FloatingActionsMenu.SavedState(super.onSaveInstanceState());
    localSavedState.a = this.j;
    return localSavedState;
  }
  
  public void setOnFloatingActionsMenuUpdateListener(h paramh)
  {
    this.u = paramh;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/getbase/floatingactionbutton/FloatingActionsMenu.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */