package com.getbase.floatingactionbutton;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.view.View.BaseSavedState;

public class FloatingActionsMenu$SavedState
  extends View.BaseSavedState
{
  public static final Parcelable.Creator CREATOR = new j();
  public boolean a;
  
  private FloatingActionsMenu$SavedState(Parcel paramParcel)
  {
    super(paramParcel);
    if (paramParcel.readInt() == i) {}
    for (;;)
    {
      this.a = i;
      return;
      i = 0;
    }
  }
  
  public FloatingActionsMenu$SavedState(Parcelable paramParcelable)
  {
    super(paramParcelable);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    if (this.a) {}
    for (int i = 1;; i = 0)
    {
      paramParcel.writeInt(i);
      return;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/getbase/floatingactionbutton/FloatingActionsMenu$SavedState.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */