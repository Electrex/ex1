package com.getbase.floatingactionbutton;

import android.os.Parcel;
import android.os.Parcelable.Creator;

final class j
  implements Parcelable.Creator
{
  public FloatingActionsMenu.SavedState a(Parcel paramParcel)
  {
    return new FloatingActionsMenu.SavedState(paramParcel, null);
  }
  
  public FloatingActionsMenu.SavedState[] a(int paramInt)
  {
    return new FloatingActionsMenu.SavedState[paramInt];
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/getbase/floatingactionbutton/j.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */