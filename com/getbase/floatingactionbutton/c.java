package com.getbase.floatingactionbutton;

import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.ShapeDrawable.ShaderFactory;

class c
  extends ShapeDrawable.ShaderFactory
{
  c(FloatingActionButton paramFloatingActionButton, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {}
  
  public Shader resize(int paramInt1, int paramInt2)
  {
    float f1 = paramInt1 / 2;
    float f2 = paramInt1 / 2;
    float f3 = paramInt2;
    int[] arrayOfInt = new int[5];
    arrayOfInt[0] = this.a;
    arrayOfInt[1] = this.b;
    arrayOfInt[2] = this.c;
    arrayOfInt[3] = this.d;
    arrayOfInt[4] = this.e;
    return new LinearGradient(f1, 0.0F, f2, f3, arrayOfInt, new float[] { 0.0F, 0.2F, 0.5F, 0.8F, 1.0F }, Shader.TileMode.CLAMP);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/getbase/floatingactionbutton/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */