package com.getbase.floatingactionbutton;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.animation.OvershootInterpolator;

class e
  extends a
{
  e(FloatingActionsMenu paramFloatingActionsMenu, Context paramContext)
  {
    super(paramContext);
  }
  
  void a()
  {
    this.a = FloatingActionsMenu.a(this.g);
    this.b = FloatingActionsMenu.b(this.g);
    this.c = FloatingActionsMenu.c(this.g);
    this.f = FloatingActionsMenu.d(this.g);
    super.a();
  }
  
  Drawable getIconDrawable()
  {
    i locali = new i(super.getIconDrawable());
    FloatingActionsMenu.a(this.g, locali);
    OvershootInterpolator localOvershootInterpolator = new OvershootInterpolator();
    ObjectAnimator localObjectAnimator1 = ObjectAnimator.ofFloat(locali, "rotation", new float[] { 135.0F, 0.0F });
    ObjectAnimator localObjectAnimator2 = ObjectAnimator.ofFloat(locali, "rotation", new float[] { 0.0F, 135.0F });
    localObjectAnimator1.setInterpolator(localOvershootInterpolator);
    localObjectAnimator2.setInterpolator(localOvershootInterpolator);
    FloatingActionsMenu.e(this.g).play(localObjectAnimator2);
    FloatingActionsMenu.f(this.g).play(localObjectAnimator1);
    return locali;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/getbase/floatingactionbutton/e.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */