package com.getbase.floatingactionbutton;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;

class i
  extends LayerDrawable
{
  private float a;
  
  public i(Drawable paramDrawable)
  {
    super(new Drawable[] { paramDrawable });
  }
  
  public void draw(Canvas paramCanvas)
  {
    paramCanvas.save();
    paramCanvas.rotate(this.a, getBounds().centerX(), getBounds().centerY());
    super.draw(paramCanvas);
    paramCanvas.restore();
  }
  
  public float getRotation()
  {
    return this.a;
  }
  
  public void setRotation(float paramFloat)
  {
    this.a = paramFloat;
    invalidateSelf();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/getbase/floatingactionbutton/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */