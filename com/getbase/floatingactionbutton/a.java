package com.getbase.floatingactionbutton;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.util.AttributeSet;

public class a
  extends FloatingActionButton
{
  int a;
  
  public a(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public a(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  void a(Context paramContext, AttributeSet paramAttributeSet)
  {
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, o.AddFloatingActionButton, 0, 0);
    this.a = localTypedArray.getColor(o.AddFloatingActionButton_fab_plusIconColor, a(17170443));
    localTypedArray.recycle();
    super.a(paramContext, paramAttributeSet);
  }
  
  Drawable getIconDrawable()
  {
    float f1 = b(l.fab_icon_size);
    float f2 = f1 / 2.0F;
    float f3 = b(l.fab_plus_icon_size);
    float f4 = b(l.fab_plus_icon_stroke) / 2.0F;
    ShapeDrawable localShapeDrawable = new ShapeDrawable(new b(this, (f1 - f3) / 2.0F, f2, f4, f1));
    Paint localPaint = localShapeDrawable.getPaint();
    localPaint.setColor(this.a);
    localPaint.setStyle(Paint.Style.FILL);
    localPaint.setAntiAlias(true);
    return localShapeDrawable;
  }
  
  public int getPlusColor()
  {
    return this.a;
  }
  
  public void setIcon(int paramInt)
  {
    throw new UnsupportedOperationException("Use FloatingActionButton if you want to use custom icon");
  }
  
  public void setPlusColor(int paramInt)
  {
    if (this.a != paramInt)
    {
      this.a = paramInt;
      a();
    }
  }
  
  public void setPlusColorResId(int paramInt)
  {
    setPlusColor(a(paramInt));
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/getbase/floatingactionbutton/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */