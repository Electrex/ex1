package com.getbase.floatingactionbutton;

import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.TouchDelegate;
import android.view.View;
import java.util.ArrayList;

public class p
  extends TouchDelegate
{
  private static final Rect a = new Rect();
  private final ArrayList b = new ArrayList();
  private TouchDelegate c;
  private boolean d;
  
  public p(View paramView)
  {
    super(a, paramView);
  }
  
  public void a()
  {
    this.b.clear();
    this.c = null;
  }
  
  public void a(TouchDelegate paramTouchDelegate)
  {
    this.b.add(paramTouchDelegate);
  }
  
  public void a(boolean paramBoolean)
  {
    this.d = paramBoolean;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (!this.d) {
      return false;
    }
    TouchDelegate localTouchDelegate1;
    switch (paramMotionEvent.getAction())
    {
    default: 
      localTouchDelegate1 = null;
    }
    while ((localTouchDelegate1 != null) && (localTouchDelegate1.onTouchEvent(paramMotionEvent)))
    {
      return true;
      for (int i = 0; i < this.b.size(); i++)
      {
        TouchDelegate localTouchDelegate2 = (TouchDelegate)this.b.get(i);
        if (localTouchDelegate2.onTouchEvent(paramMotionEvent))
        {
          this.c = localTouchDelegate2;
          return true;
        }
      }
      localTouchDelegate1 = null;
      continue;
      localTouchDelegate1 = this.c;
      continue;
      localTouchDelegate1 = this.c;
      this.c = null;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/getbase/floatingactionbutton/p.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */