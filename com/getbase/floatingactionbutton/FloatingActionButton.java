package com.getbase.floatingactionbutton;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.widget.ImageButton;
import android.widget.TextView;

public class FloatingActionButton
  extends ImageButton
{
  private int a;
  int b;
  int c;
  int d;
  String e;
  boolean f;
  private Drawable g;
  private int h;
  private float i;
  private float j;
  private float k;
  private int l;
  
  public FloatingActionButton(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public FloatingActionButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a(paramContext, paramAttributeSet);
  }
  
  public FloatingActionButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    a(paramContext, paramAttributeSet);
  }
  
  private Drawable a(int paramInt, float paramFloat)
  {
    int m = Color.alpha(paramInt);
    int n = f(paramInt);
    ShapeDrawable localShapeDrawable = new ShapeDrawable(new OvalShape());
    Paint localPaint = localShapeDrawable.getPaint();
    localPaint.setAntiAlias(true);
    localPaint.setColor(n);
    Drawable[] arrayOfDrawable = new Drawable[2];
    arrayOfDrawable[0] = localShapeDrawable;
    arrayOfDrawable[1] = c(n, paramFloat);
    if ((m == 255) || (!this.f)) {}
    for (Object localObject = new LayerDrawable(arrayOfDrawable);; localObject = new d(m, arrayOfDrawable))
    {
      int i1 = (int)(paramFloat / 2.0F);
      ((LayerDrawable)localObject).setLayerInset(1, i1, i1, i1, i1);
      return (Drawable)localObject;
    }
  }
  
  private StateListDrawable a(float paramFloat)
  {
    StateListDrawable localStateListDrawable = new StateListDrawable();
    localStateListDrawable.addState(new int[] { -16842910 }, a(this.d, paramFloat));
    localStateListDrawable.addState(new int[] { 16842919 }, a(this.c, paramFloat));
    localStateListDrawable.addState(new int[0], a(this.b, paramFloat));
    return localStateListDrawable;
  }
  
  private int b(int paramInt, float paramFloat)
  {
    float[] arrayOfFloat = new float[3];
    Color.colorToHSV(paramInt, arrayOfFloat);
    arrayOfFloat[2] = Math.min(paramFloat * arrayOfFloat[2], 1.0F);
    return Color.HSVToColor(Color.alpha(paramInt), arrayOfFloat);
  }
  
  private Drawable b(float paramFloat)
  {
    ShapeDrawable localShapeDrawable = new ShapeDrawable(new OvalShape());
    Paint localPaint = localShapeDrawable.getPaint();
    localPaint.setAntiAlias(true);
    localPaint.setStrokeWidth(paramFloat);
    localPaint.setStyle(Paint.Style.STROKE);
    localPaint.setColor(-16777216);
    localPaint.setAlpha(c(0.02F));
    return localShapeDrawable;
  }
  
  private void b()
  {
    this.l = ((int)(this.i + 2.0F * this.j));
  }
  
  private int c(float paramFloat)
  {
    return (int)(255.0F * paramFloat);
  }
  
  private int c(int paramInt)
  {
    return b(paramInt, 0.9F);
  }
  
  private Drawable c(int paramInt, float paramFloat)
  {
    if (!this.f) {
      return new ColorDrawable(0);
    }
    ShapeDrawable localShapeDrawable = new ShapeDrawable(new OvalShape());
    int m = c(paramInt);
    int n = e(m);
    int i1 = d(paramInt);
    int i2 = e(i1);
    Paint localPaint = localShapeDrawable.getPaint();
    localPaint.setAntiAlias(true);
    localPaint.setStrokeWidth(paramFloat);
    localPaint.setStyle(Paint.Style.STROKE);
    localShapeDrawable.setShaderFactory(new c(this, i1, i2, paramInt, n, m));
    return localShapeDrawable;
  }
  
  private void c()
  {
    if (this.h == 0) {}
    for (int m = l.fab_size_normal;; m = l.fab_size_mini)
    {
      this.i = b(m);
      return;
    }
  }
  
  private int d(int paramInt)
  {
    return b(paramInt, 1.1F);
  }
  
  private int e(int paramInt)
  {
    return Color.argb(Color.alpha(paramInt) / 2, Color.red(paramInt), Color.green(paramInt), Color.blue(paramInt));
  }
  
  private int f(int paramInt)
  {
    return Color.rgb(Color.red(paramInt), Color.green(paramInt), Color.blue(paramInt));
  }
  
  @SuppressLint({"NewApi"})
  private void setBackgroundCompat(Drawable paramDrawable)
  {
    if (Build.VERSION.SDK_INT >= 16)
    {
      setBackground(paramDrawable);
      return;
    }
    setBackgroundDrawable(paramDrawable);
  }
  
  int a(int paramInt)
  {
    return getResources().getColor(paramInt);
  }
  
  void a()
  {
    float f1 = b(l.fab_stroke_width);
    float f2 = f1 / 2.0F;
    Drawable[] arrayOfDrawable = new Drawable[4];
    Resources localResources = getResources();
    if (this.h == 0) {}
    for (int m = m.fab_bg_normal;; m = m.fab_bg_mini)
    {
      arrayOfDrawable[0] = localResources.getDrawable(m);
      arrayOfDrawable[1] = a(f1);
      arrayOfDrawable[2] = b(f1);
      arrayOfDrawable[3] = getIconDrawable();
      LayerDrawable localLayerDrawable = new LayerDrawable(arrayOfDrawable);
      int n = (int)(this.i - b(l.fab_icon_size)) / 2;
      int i1 = (int)this.j;
      int i2 = (int)(this.j - this.k);
      int i3 = (int)(this.j + this.k);
      localLayerDrawable.setLayerInset(1, i1, i2, i1, i3);
      localLayerDrawable.setLayerInset(2, (int)(i1 - f2), (int)(i2 - f2), (int)(i1 - f2), (int)(i3 - f2));
      localLayerDrawable.setLayerInset(3, i1 + n, i2 + n, i1 + n, i3 + n);
      setBackgroundCompat(localLayerDrawable);
      return;
    }
  }
  
  void a(Context paramContext, AttributeSet paramAttributeSet)
  {
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, o.FloatingActionButton, 0, 0);
    this.b = localTypedArray.getColor(o.FloatingActionButton_fab_colorNormal, a(17170451));
    this.c = localTypedArray.getColor(o.FloatingActionButton_fab_colorPressed, a(17170450));
    this.d = localTypedArray.getColor(o.FloatingActionButton_fab_colorDisabled, a(17170432));
    this.h = localTypedArray.getInt(o.FloatingActionButton_fab_size, 0);
    this.a = localTypedArray.getResourceId(o.FloatingActionButton_fab_icon, 0);
    this.e = localTypedArray.getString(o.FloatingActionButton_fab_title);
    this.f = localTypedArray.getBoolean(o.FloatingActionButton_fab_stroke_visible, true);
    localTypedArray.recycle();
    c();
    this.j = b(l.fab_shadow_radius);
    this.k = b(l.fab_shadow_offset);
    b();
    a();
  }
  
  float b(int paramInt)
  {
    return getResources().getDimension(paramInt);
  }
  
  public int getColorDisabled()
  {
    return this.d;
  }
  
  public int getColorNormal()
  {
    return this.b;
  }
  
  public int getColorPressed()
  {
    return this.c;
  }
  
  Drawable getIconDrawable()
  {
    if (this.g != null) {
      return this.g;
    }
    if (this.a != 0) {
      return getResources().getDrawable(this.a);
    }
    return new ColorDrawable(0);
  }
  
  TextView getLabelView()
  {
    return (TextView)getTag(n.fab_label);
  }
  
  public int getSize()
  {
    return this.h;
  }
  
  public String getTitle()
  {
    return this.e;
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    setMeasuredDimension(this.l, this.l);
  }
  
  public void setColorDisabled(int paramInt)
  {
    if (this.d != paramInt)
    {
      this.d = paramInt;
      a();
    }
  }
  
  public void setColorDisabledResId(int paramInt)
  {
    setColorDisabled(a(paramInt));
  }
  
  public void setColorNormal(int paramInt)
  {
    if (this.b != paramInt)
    {
      this.b = paramInt;
      a();
    }
  }
  
  public void setColorNormalResId(int paramInt)
  {
    setColorNormal(a(paramInt));
  }
  
  public void setColorPressed(int paramInt)
  {
    if (this.c != paramInt)
    {
      this.c = paramInt;
      a();
    }
  }
  
  public void setColorPressedResId(int paramInt)
  {
    setColorPressed(a(paramInt));
  }
  
  public void setIcon(int paramInt)
  {
    if (this.a != paramInt)
    {
      this.a = paramInt;
      this.g = null;
      a();
    }
  }
  
  public void setIconDrawable(Drawable paramDrawable)
  {
    if (this.g != paramDrawable)
    {
      this.a = 0;
      this.g = paramDrawable;
      a();
    }
  }
  
  public void setSize(int paramInt)
  {
    if ((paramInt != 1) && (paramInt != 0)) {
      throw new IllegalArgumentException("Use @FAB_SIZE constants only!");
    }
    if (this.h != paramInt)
    {
      this.h = paramInt;
      c();
      b();
      a();
    }
  }
  
  public void setStrokeVisible(boolean paramBoolean)
  {
    if (this.f != paramBoolean)
    {
      this.f = paramBoolean;
      a();
    }
  }
  
  public void setTitle(String paramString)
  {
    this.e = paramString;
    TextView localTextView = getLabelView();
    if (localTextView != null) {
      localTextView.setText(paramString);
    }
  }
  
  public void setVisibility(int paramInt)
  {
    TextView localTextView = getLabelView();
    if (localTextView != null) {
      localTextView.setVisibility(paramInt);
    }
    super.setVisibility(paramInt);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/getbase/floatingactionbutton/FloatingActionButton.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */