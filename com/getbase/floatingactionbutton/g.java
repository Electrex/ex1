package com.getbase.floatingactionbutton;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

class g
  extends ViewGroup.LayoutParams
{
  private ObjectAnimator b = new ObjectAnimator();
  private ObjectAnimator c = new ObjectAnimator();
  private ObjectAnimator d = new ObjectAnimator();
  private ObjectAnimator e = new ObjectAnimator();
  private boolean f;
  
  public g(FloatingActionsMenu paramFloatingActionsMenu, ViewGroup.LayoutParams paramLayoutParams)
  {
    super(paramLayoutParams);
    this.b.setInterpolator(FloatingActionsMenu.d());
    this.c.setInterpolator(FloatingActionsMenu.e());
    this.d.setInterpolator(FloatingActionsMenu.f());
    this.e.setInterpolator(FloatingActionsMenu.f());
    this.e.setProperty(View.ALPHA);
    this.e.setFloatValues(new float[] { 1.0F, 0.0F });
    this.c.setProperty(View.ALPHA);
    this.c.setFloatValues(new float[] { 0.0F, 1.0F });
    switch (FloatingActionsMenu.g(paramFloatingActionsMenu))
    {
    default: 
      return;
    case 0: 
    case 1: 
      this.d.setProperty(View.TRANSLATION_Y);
      this.b.setProperty(View.TRANSLATION_Y);
      return;
    }
    this.d.setProperty(View.TRANSLATION_X);
    this.b.setProperty(View.TRANSLATION_X);
  }
  
  public void a(View paramView)
  {
    this.e.setTarget(paramView);
    this.d.setTarget(paramView);
    this.c.setTarget(paramView);
    this.b.setTarget(paramView);
    if (!this.f)
    {
      FloatingActionsMenu.f(this.a).play(this.e);
      FloatingActionsMenu.f(this.a).play(this.d);
      FloatingActionsMenu.e(this.a).play(this.c);
      FloatingActionsMenu.e(this.a).play(this.b);
      this.f = true;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/getbase/floatingactionbutton/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */