package com.getbase.floatingactionbutton;

public final class o
{
  public static final int[] AddFloatingActionButton = { 2130772000 };
  public static final int AddFloatingActionButton_fab_plusIconColor = 0;
  public static final int[] FloatingActionButton = { 2130772033, 2130772034, 2130772035, 2130772036, 2130772037, 2130772038, 2130772039, 2130772040, 2130772041, 2130772042, 2130772043 };
  public static final int FloatingActionButton_fab_colorDisabled = 1;
  public static final int FloatingActionButton_fab_colorNormal = 2;
  public static final int FloatingActionButton_fab_colorPressed = 0;
  public static final int FloatingActionButton_fab_icon = 3;
  public static final int FloatingActionButton_fab_size = 4;
  public static final int FloatingActionButton_fab_stroke_visible = 6;
  public static final int FloatingActionButton_fab_title = 5;
  public static final int[] FloatingActionsMenu = { 2130772044, 2130772045, 2130772046, 2130772047, 2130772048, 2130772049, 2130772050, 2130772051 };
  public static final int FloatingActionsMenu_fab_addButtonColorNormal = 1;
  public static final int FloatingActionsMenu_fab_addButtonColorPressed = 0;
  public static final int FloatingActionsMenu_fab_addButtonPlusIconColor = 3;
  public static final int FloatingActionsMenu_fab_addButtonSize = 2;
  public static final int FloatingActionsMenu_fab_addButtonStrokeVisible = 4;
  public static final int FloatingActionsMenu_fab_expandDirection = 7;
  public static final int FloatingActionsMenu_fab_labelStyle = 5;
  public static final int FloatingActionsMenu_fab_labelsPosition = 6;
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/getbase/floatingactionbutton/o.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */