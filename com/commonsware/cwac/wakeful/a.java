package com.commonsware.cwac.wakeful;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;

public abstract class a
  extends IntentService
{
  private static volatile PowerManager.WakeLock a = null;
  
  public a(String paramString)
  {
    super(paramString);
    setIntentRedelivery(true);
  }
  
  private static PowerManager.WakeLock a(Context paramContext)
  {
    try
    {
      if (a == null)
      {
        a = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(1, "com.commonsware.cwac.wakeful.WakefulIntentService");
        a.setReferenceCounted(true);
      }
      PowerManager.WakeLock localWakeLock = a;
      return localWakeLock;
    }
    finally {}
  }
  
  public static void a(Context paramContext, Intent paramIntent)
  {
    a(paramContext.getApplicationContext()).acquire();
    paramContext.startService(paramIntent);
  }
  
  public static void a(b paramb, Context paramContext, boolean paramBoolean)
  {
    long l = paramContext.getSharedPreferences("com.commonsware.cwac.wakeful.WakefulIntentService", 0).getLong("lastAlarm", 0L);
    if ((l == 0L) || (paramBoolean) || ((System.currentTimeMillis() > l) && (System.currentTimeMillis() - l > paramb.getMaxAge()))) {
      paramb.scheduleAlarms((AlarmManager)paramContext.getSystemService("alarm"), PendingIntent.getBroadcast(paramContext, 0, new Intent(paramContext, AlarmReceiver.class), 0), paramContext);
    }
  }
  
  protected abstract void a(Intent paramIntent);
  
  protected final void onHandleIntent(Intent paramIntent)
  {
    try
    {
      a(paramIntent);
      PowerManager.WakeLock localWakeLock2 = a(getApplicationContext());
      if (localWakeLock2.isHeld()) {}
      try
      {
        localWakeLock2.release();
        return;
      }
      catch (Exception localException2)
      {
        Log.e(getClass().getSimpleName(), "Exception when releasing wakelock", localException2);
        return;
      }
      try
      {
        PowerManager.WakeLock localWakeLock1;
        localWakeLock1.release();
        throw ((Throwable)localObject);
      }
      catch (Exception localException1)
      {
        for (;;)
        {
          Log.e(getClass().getSimpleName(), "Exception when releasing wakelock", localException1);
        }
      }
    }
    finally
    {
      localWakeLock1 = a(getApplicationContext());
      if (!localWakeLock1.isHeld()) {}
    }
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    PowerManager.WakeLock localWakeLock = a(getApplicationContext());
    if ((!localWakeLock.isHeld()) || ((paramInt1 & 0x1) != 0)) {
      localWakeLock.acquire();
    }
    super.onStartCommand(paramIntent, paramInt1, paramInt2);
    return 3;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/commonsware/cwac/wakeful/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */