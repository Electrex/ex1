package com.commonsware.cwac.wakeful;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;

public abstract interface b
{
  public abstract long getMaxAge();
  
  public abstract void scheduleAlarms(AlarmManager paramAlarmManager, PendingIntent paramPendingIntent, Context paramContext);
  
  public abstract void sendWakefulWork(Context paramContext);
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/commonsware/cwac/wakeful/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */