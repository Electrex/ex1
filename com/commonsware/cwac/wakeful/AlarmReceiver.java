package com.commonsware.cwac.wakeful;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.XmlResourceParser;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

public class AlarmReceiver
  extends BroadcastReceiver
{
  private b a(Context paramContext)
  {
    PackageManager localPackageManager = paramContext.getPackageManager();
    ComponentName localComponentName = new ComponentName(paramContext, getClass());
    try
    {
      XmlResourceParser localXmlResourceParser = localPackageManager.getReceiverInfo(localComponentName, 128).loadXmlMetaData(localPackageManager, "com.commonsware.cwac.wakeful");
      while (localXmlResourceParser.getEventType() != 1)
      {
        if ((localXmlResourceParser.getEventType() == 2) && (localXmlResourceParser.getName().equals("WakefulIntentService"))) {
          return (b)Class.forName(localXmlResourceParser.getAttributeValue(null, "listener")).newInstance();
        }
        localXmlResourceParser.next();
      }
      return null;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      throw new RuntimeException("Cannot find own info???", localNameNotFoundException);
    }
    catch (XmlPullParserException localXmlPullParserException)
    {
      throw new RuntimeException("Malformed metadata resource XML", localXmlPullParserException);
    }
    catch (IOException localIOException)
    {
      throw new RuntimeException("Could not read resource XML", localIOException);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new RuntimeException("Listener class not found", localClassNotFoundException);
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      throw new RuntimeException("Listener is not public or lacks public constructor", localIllegalAccessException);
    }
    catch (InstantiationException localInstantiationException)
    {
      throw new RuntimeException("Could not create instance of listener", localInstantiationException);
    }
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    b localb = a(paramContext);
    if (localb != null)
    {
      if (paramIntent.getAction() == null)
      {
        paramContext.getSharedPreferences("com.commonsware.cwac.wakeful.WakefulIntentService", 0).edit().putLong("lastAlarm", System.currentTimeMillis()).commit();
        localb.sendWakefulWork(paramContext);
      }
    }
    else {
      return;
    }
    a.a(localb, paramContext, true);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/com/commonsware/cwac/wakeful/AlarmReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */