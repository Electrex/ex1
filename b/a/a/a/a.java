package b.a.a.a;

import java.nio.charset.Charset;

public class a
{
  public static final Charset a = Charset.forName("ISO-8859-1");
  public static final Charset b = Charset.forName("US-ASCII");
  public static final Charset c = Charset.forName("UTF-16");
  public static final Charset d = Charset.forName("UTF-16BE");
  public static final Charset e = Charset.forName("UTF-16LE");
  public static final Charset f = Charset.forName("UTF-8");
  
  public static Charset a(Charset paramCharset)
  {
    if (paramCharset == null) {
      paramCharset = Charset.defaultCharset();
    }
    return paramCharset;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/b/a/a/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */