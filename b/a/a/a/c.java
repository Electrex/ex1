package b.a.a.a;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;

public class c
{
  public static final char a = File.separatorChar;
  public static final String b;
  
  static
  {
    b.a.a.a.a.a locala = new b.a.a.a.a.a(4);
    PrintWriter localPrintWriter = new PrintWriter(locala);
    localPrintWriter.println();
    b = locala.toString();
    localPrintWriter.close();
  }
  
  public static void a(Closeable paramCloseable)
  {
    if (paramCloseable != null) {}
    try
    {
      paramCloseable.close();
      return;
    }
    catch (IOException localIOException) {}
  }
  
  public static void a(OutputStream paramOutputStream)
  {
    a(paramOutputStream);
  }
  
  public static void a(String paramString, OutputStream paramOutputStream, Charset paramCharset)
  {
    if (paramString != null) {
      paramOutputStream.write(paramString.getBytes(a.a(paramCharset)));
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/b/a/a/a/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */