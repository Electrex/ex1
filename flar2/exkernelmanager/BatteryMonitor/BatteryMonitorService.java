package flar2.exkernelmanager.BatteryMonitor;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.SystemClock;
import flar2.exkernelmanager.utilities.k;

public class BatteryMonitorService
  extends Service
{
  private BroadcastReceiver a;
  private boolean b;
  
  private void a()
  {
    this.a = new b(this, null);
    IntentFilter localIntentFilter = new IntentFilter("android.intent.action.SCREEN_ON");
    localIntentFilter.addAction("android.intent.action.SCREEN_OFF");
    localIntentFilter.addAction("android.intent.action.ACTION_POWER_CONNECTED");
    localIntentFilter.addAction("android.intent.action.ACTION_POWER_DISCONNECTED");
    registerReceiver(this.a, localIntentFilter);
  }
  
  private int b()
  {
    Intent localIntent = registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    int j;
    int i;
    if (localIntent != null)
    {
      j = localIntent.getIntExtra("level", -1);
      i = localIntent.getIntExtra("scale", -1);
    }
    for (;;)
    {
      return j * 100 / i;
      i = 0;
      j = 0;
    }
  }
  
  private long c()
  {
    return System.currentTimeMillis();
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  public void onCreate()
  {
    super.onCreate();
    Intent localIntent = registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    if (localIntent != null) {}
    for (int i = localIntent.getIntExtra("status", -1);; i = 0)
    {
      boolean bool;
      if ((i == 2) || (i == 5))
      {
        bool = true;
        this.b = bool;
        if (k.d("prefBMStartMarker") == -1L)
        {
          k.a("prefBMFullContinue", true);
          k.a("prefBMScrOffTime", 0L);
          k.a("prefBMScrOffMarker", -1L);
          k.a("prefBMScrOffUsage", 0);
          k.a("prefBMScrOffLevel", 0);
          k.a("prefBMScrOnTime", 0L);
          k.a("prefBMScrOnMarker", -1L);
          k.a("prefBMScrOnUsage", 0);
          k.a("prefBMScrOnLevel", 0);
          k.a("prefBMPlugMarker", -1L);
          k.a("prefBMChargeTime", 0L);
          k.a("prefBMDeepSleepOffset", 0L);
          k.a("prefBMFullMarker", 0L);
          k.a("prefBMFullTimeCharge", 0L);
          k.a("prefBMDeepSleepFullOffset", 0L);
          k.a("prefBMFullTimeOn", 0L);
          k.a("prefBMFullTimeOff", 0L);
          k.a("prefBMFullUsageOn", 0);
          k.a("prefBMFullUsageOff", 0);
          k.a("prefBMCustomMarker", 0L);
          k.a("prefBMCustomTimeCharge", 0L);
          k.a("prefBMDeepSleepCustomOffset", 0L);
          k.a("prefBMCustomTimeOn", 0L);
          k.a("prefBMCustomTimeOff", 0L);
          k.a("prefBMCustomUsageOn", 0);
          k.a("prefBMCustomUsageOff", 0);
        }
        if (k.d("prefBMStartMarker") == -1L) {
          k.a("prefBMDeepSleepOffset", SystemClock.elapsedRealtime() - SystemClock.uptimeMillis());
        }
        if (this.b) {
          break label338;
        }
        if (k.d("prefBMStartMarker") == -1L) {
          k.a("prefBMStartMarker", c());
        }
        if (!((PowerManager)getSystemService("power")).isScreenOn()) {
          break label317;
        }
        k.a("prefBMScrOnMarker", c());
        k.a("prefBMScrOnLevel", b());
      }
      for (;;)
      {
        a();
        return;
        bool = false;
        break;
        label317:
        k.a("prefBMScrOffMarker", c());
        k.a("prefBMScrOffLevel", b());
        continue;
        label338:
        if (k.d("prefBMStartMarker") == -1L) {
          k.a("prefBMPlugMarker", c());
        }
      }
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    k.a("prefBMStartMarker", -1L);
    if (this.a != null) {
      unregisterReceiver(this.a);
    }
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    return 1;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/BatteryMonitor/BatteryMonitorService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */