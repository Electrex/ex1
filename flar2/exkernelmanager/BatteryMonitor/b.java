package flar2.exkernelmanager.BatteryMonitor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import flar2.exkernelmanager.utilities.k;

class b
  extends BroadcastReceiver
{
  private b(BatteryMonitorService paramBatteryMonitorService) {}
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if (paramIntent.getAction().equals("android.intent.action.SCREEN_ON")) {
      if (!BatteryMonitorService.a(this.a))
      {
        int j = BatteryMonitorService.b(this.a);
        k.a("prefBMScrOnMarker", BatteryMonitorService.c(this.a));
        k.a("prefBMScrOffTime", k.d("prefBMScrOffTime") + (BatteryMonitorService.c(this.a) - k.d("prefBMScrOffMarker")));
        k.a("prefBMScrOnLevel", j);
        k.a("prefBMScrOffUsage", k.c("prefBMScrOffUsage") + (k.c("prefBMScrOffLevel") - j));
      }
    }
    do
    {
      do
      {
        return;
        if (!paramIntent.getAction().equals("android.intent.action.SCREEN_OFF")) {
          break;
        }
      } while (BatteryMonitorService.a(this.a));
      int i = BatteryMonitorService.b(this.a);
      k.a("prefBMScrOffMarker", BatteryMonitorService.c(this.a));
      k.a("prefBMScrOnTime", k.d("prefBMScrOnTime") + (BatteryMonitorService.c(this.a) - k.d("prefBMScrOnMarker")));
      k.a("prefBMScrOffLevel", i);
      k.a("prefBMScrOnUsage", k.c("prefBMScrOnUsage") + (k.c("prefBMScrOnLevel") - i));
      return;
      if (paramIntent.getAction().equals("android.intent.action.ACTION_POWER_CONNECTED"))
      {
        BatteryMonitorService.a(this.a, true);
        k.a("prefBMPlugMarker", BatteryMonitorService.c(this.a));
        if (k.d("prefBMScrOffMarker") > k.d("prefBMScrOnMarker"))
        {
          k.a("prefBMScrOffTime", k.d("prefBMScrOffTime") + (BatteryMonitorService.c(this.a) - k.d("prefBMScrOffMarker")));
          k.a("prefBMScrOffUsage", k.c("prefBMScrOffUsage") + (k.c("prefBMScrOffLevel") - BatteryMonitorService.b(this.a)));
          return;
        }
        k.a("prefBMScrOnTime", k.d("prefBMScrOnTime") + (BatteryMonitorService.c(this.a) - k.d("prefBMScrOnMarker")));
        k.a("prefBMScrOnUsage", k.c("prefBMScrOnUsage") + (k.c("prefBMScrOnLevel") - BatteryMonitorService.b(this.a)));
        return;
      }
    } while (!paramIntent.getAction().equals("android.intent.action.ACTION_POWER_DISCONNECTED"));
    BatteryMonitorService.a(this.a, false);
    k.a("prefBMChargeTime", k.d("prefBMChargeTime") + (BatteryMonitorService.c(this.a) - k.d("prefBMPlugMarker")));
    if (k.d("prefBMStartMarker") == -1L) {
      k.a("prefBMStartMarker", BatteryMonitorService.c(this.a));
    }
    k.a("prefBMScrOnMarker", BatteryMonitorService.c(this.a));
    k.a("prefBMScrOnLevel", BatteryMonitorService.b(this.a));
    k.a("prefBMScrOffMarker", BatteryMonitorService.c(this.a));
    k.a("prefBMScrOffLevel", BatteryMonitorService.b(this.a));
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/BatteryMonitor/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */