package flar2.exkernelmanager.powersave;

import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import flar2.exkernelmanager.r;
import flar2.exkernelmanager.utilities.e;
import flar2.exkernelmanager.utilities.m;

public class a
{
  SharedPreferences a;
  protected Context b;
  
  public a(Context paramContext)
  {
    this.b = paramContext.getApplicationContext();
    this.a = PreferenceManager.getDefaultSharedPreferences(paramContext);
  }
  
  private void b()
  {
    AppWidgetManager localAppWidgetManager = AppWidgetManager.getInstance(this.b);
    ComponentName localComponentName = new ComponentName(this.b.getApplicationContext(), PowersaveWidgetProvider.class);
    Intent localIntent = new Intent(this.b, PowersaveWidgetProvider.class);
    int[] arrayOfInt = localAppWidgetManager.getAppWidgetIds(localComponentName);
    localIntent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
    localIntent.putExtra("appWidgetIds", arrayOfInt);
    this.b.sendBroadcast(localIntent);
  }
  
  public String a(String paramString)
  {
    return this.a.getString(paramString, null);
  }
  
  public void a()
  {
    Intent localIntent = new Intent("flar2.exkernelmanager.powersaver.DISABLE_POWERSAVE");
    PendingIntent localPendingIntent = PendingIntent.getBroadcast(this.b, 0, localIntent, 0);
    Notification localNotification = new Notification.Builder(this.b).setOngoing(true).setContentTitle(this.b.getString(2131624182)).setContentText(this.b.getString(2131624183)).setSmallIcon(2130837632).setContentIntent(localPendingIntent).setShowWhen(false).build();
    localNotification.flags = (0x10 | localNotification.flags);
    Context localContext = this.b;
    ((NotificationManager)localContext.getSystemService("notification")).notify(100, localNotification);
  }
  
  public void a(String paramString1, String paramString2)
  {
    SharedPreferences.Editor localEditor = this.a.edit();
    localEditor.putString(paramString1, paramString2);
    localEditor.commit();
  }
  
  public void a(String paramString, boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = this.a.edit();
    localEditor.putBoolean(paramString, paramBoolean);
    localEditor.commit();
  }
  
  public void a(boolean paramBoolean)
  {
    m localm = new m();
    e locale = new e();
    flar2.exkernelmanager.performance.a locala = new flar2.exkernelmanager.performance.a(this.b);
    int i = localm.a(r.k);
    int j = localm.a(r.i);
    int k = localm.a(r.e);
    String str1 = a("prefCPUMaxPrefPS");
    String str2 = a("prefGPUMaxPrefPS");
    if (paramBoolean)
    {
      if (b("prefPerformance").booleanValue()) {
        locala.a(false);
      }
      if (locale.a("/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"))
      {
        a("prefCPUMaxPS", localm.a("/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"));
        a("prefGPUMaxPS", localm.a(r.e[k]));
        a("prefBacklightBootPS", localm.a(r.i[j]));
        if ((a("prefDeviceName").equals(this.b.getString(2131624153))) || (a("prefDeviceName").equals(this.b.getString(2131624157))))
        {
          a("prefDT2WPS", localm.a(r.n[0]));
          a("prefS2WPS", localm.a(r.m[0]));
        }
        a("prefVibPS", localm.a(r.k[i]).replaceAll("[^0-9]", ""));
        if (localm.a("/sys/devices/platform/msm_sleeper/enabled").equals("1")) {
          a("prefSleeperMaxOnlinePS", localm.b("/sys/devices/platform/msm_sleeper/max_online"));
        }
        if (!locale.a("/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq")) {
          break label571;
        }
        localm.a(str1, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq");
      }
      for (;;)
      {
        if (locale.a("/sys/devices/system/cpu/cpu4/online"))
        {
          localm.a("0", "/sys/devices/system/cpu/cpu4/online");
          localm.a("0", "/sys/devices/system/cpu/cpu5/online");
          localm.a("0", "/sys/devices/system/cpu/cpu6/online");
          localm.a("0", "/sys/devices/system/cpu/cpu7/online");
        }
        localm.a(str2, r.e[k]);
        if (b("prefDimmerPS").booleanValue()) {
          localm.a("1", r.i[j]);
        }
        if (((a("prefDeviceName").equals(this.b.getString(2131624153))) || (a("prefDeviceName").equals(this.b.getString(2131624157)))) && (b("prefWakePS").booleanValue()))
        {
          localm.a("0", r.n[0]);
          localm.a("0", r.m[0]);
        }
        if (b("prefVibOptPS").booleanValue())
        {
          localm.a("0", r.k[0]);
          localm.a("1200", r.k[1]);
        }
        if (localm.a("/sys/devices/platform/msm_sleeper/enabled").equals("1")) {
          localm.a("2", "/sys/devices/platform/msm_sleeper/max_online");
        }
        a("prefPowersaver", true);
        b(true);
        b();
        return;
        if (locale.a("/sys/module/cpu_tegra/parameters/cpu_user_cap"))
        {
          a("prefCPUMaxPS", localm.a("/sys/module/cpu_tegra/parameters/cpu_user_cap"));
          break;
        }
        a("prefCPUMaxPS", localm.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"));
        break;
        label571:
        if (locale.a("/sys/module/cpu_tegra/parameters/cpu_user_cap"))
        {
          localm.a(str1, "/sys/module/cpu_tegra/parameters/cpu_user_cap");
        }
        else
        {
          localm.a(str1, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq");
          localm.a("1", "/sys/devices/system/cpu/cpu1/online");
          localm.a(str1, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_max_freq");
          localm.a("1", "/sys/devices/system/cpu/cpu2/online");
          localm.a(str1, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_max_freq");
          localm.a("1", "/sys/devices/system/cpu/cpu3/online");
          localm.a(str1, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_max_freq");
        }
      }
    }
    if (locale.a("/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq")) {
      localm.a(a("prefCPUMaxPS"), "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq");
    }
    for (;;)
    {
      if (locale.a("/sys/devices/system/cpu/cpu4/online"))
      {
        localm.a("1", "/sys/devices/system/cpu/cpu4/online");
        localm.a("1", "/sys/devices/system/cpu/cpu5/online");
        localm.a("1", "/sys/devices/system/cpu/cpu6/online");
        localm.a("1", "/sys/devices/system/cpu/cpu7/online");
      }
      localm.a(a("prefGPUMaxPS"), r.e[k]);
      localm.a(a("prefBacklightBootPS"), r.i[j]);
      if ((a("prefDeviceName").equals(this.b.getString(2131624153))) || (a("prefDeviceName").equals(this.b.getString(2131624157))))
      {
        localm.a(a("prefDT2WPS"), r.n[0]);
        localm.a(a("prefS2WPS"), r.m[0]);
      }
      if (localm.a("/sys/devices/platform/msm_sleeper/enabled").equals("1")) {
        localm.a(a("prefSleeperMaxOnlinePS"), "/sys/devices/platform/msm_sleeper/max_online");
      }
      localm.a(a("prefVibPS"), r.k[i]);
      a("prefPowersaver", false);
      b(false);
      b();
      return;
      if (locale.a("/sys/module/cpu_tegra/parameters/cpu_user_cap"))
      {
        localm.a(a("prefCPUMaxPS"), "/sys/module/cpu_tegra/parameters/cpu_user_cap");
      }
      else
      {
        String str3 = a("prefCPUMaxPS");
        localm.a(str3, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq");
        localm.a("1", "/sys/devices/system/cpu/cpu1/online");
        localm.a(str3, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_max_freq");
        localm.a("1", "/sys/devices/system/cpu/cpu2/online");
        localm.a(str3, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_max_freq");
        localm.a("1", "/sys/devices/system/cpu/cpu3/online");
        localm.a(str3, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_max_freq");
      }
    }
  }
  
  public Boolean b(String paramString)
  {
    return Boolean.valueOf(this.a.getBoolean(paramString, false));
  }
  
  public void b(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      if (b("prefPowersaverNotify").booleanValue()) {
        a();
      }
      return;
    }
    Context localContext = this.b;
    ((NotificationManager)localContext.getSystemService("notification")).cancel(100);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/powersave/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */