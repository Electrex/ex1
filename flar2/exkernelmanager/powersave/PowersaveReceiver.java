package flar2.exkernelmanager.powersave;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import flar2.exkernelmanager.MainApp;
import flar2.exkernelmanager.StartActivity;
import flar2.exkernelmanager.utilities.k;

public class PowersaveReceiver
  extends BroadcastReceiver
{
  SharedPreferences a;
  
  private void a()
  {
    Intent localIntent = new Intent(MainApp.a(), StartActivity.class);
    localIntent.addFlags(268435456);
    MainApp.a().startActivity(localIntent);
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    this.a = PreferenceManager.getDefaultSharedPreferences(paramContext);
    String str = paramIntent.getAction();
    if ("flar2.exkernelmanager.powersaver.DISABLE_POWERSAVE".equals(str))
    {
      if (k.e("prefDeviceName")) {
        break label80;
      }
      a();
    }
    for (;;)
    {
      if ((("android.intent.action.BATTERY_LOW".equals(str)) && (this.a.getBoolean("prefAutoPowersave", false))) || ("flar2.exkernelmanager.powersaver.ENABLE_POWERSAVE".equals(str)))
      {
        if (k.e("prefDeviceName")) {
          break;
        }
        a();
      }
      return;
      label80:
      new a(paramContext).a(false);
    }
    new a(paramContext).a(true);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/powersave/PowersaveReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */