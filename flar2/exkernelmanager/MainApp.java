package flar2.exkernelmanager;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import java.io.PrintStream;

public class MainApp
  extends Application
{
  public static int a = 0;
  public static boolean b = false;
  private static Context c;
  
  public static Context a()
  {
    return c;
  }
  
  private boolean b(Context paramContext)
  {
    try
    {
      paramContext.getPackageManager().getApplicationInfo("com.forpda.lp", 0);
      System.out.println("Lucky Patcher installed");
      return true;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException) {}
    return false;
  }
  
  public boolean a(Context paramContext)
  {
    String str = paramContext.getPackageManager().getInstallerPackageName("flar2.exkernelmanager");
    if (str == null) {}
    while (str.compareTo("com.android.vending") != 0) {
      return true;
    }
    return false;
  }
  
  public void onCreate()
  {
    super.onCreate();
    c = getApplicationContext();
    if ((!a(c)) || (b(c))) {
      b = true;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/MainApp.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */