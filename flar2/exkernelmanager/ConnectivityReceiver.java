package flar2.exkernelmanager;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.commonsware.cwac.wakeful.a;
import flar2.exkernelmanager.utilities.k;

public class ConnectivityReceiver
  extends BroadcastReceiver
{
  public static void a(Context paramContext)
  {
    ComponentName localComponentName = new ComponentName(paramContext, ConnectivityReceiver.class);
    paramContext.getPackageManager().setComponentEnabledSetting(localComponentName, 1, 1);
  }
  
  public static void b(Context paramContext)
  {
    ComponentName localComponentName = new ComponentName(paramContext, ConnectivityReceiver.class);
    paramContext.getPackageManager().setComponentEnabledSetting(localComponentName, 2, 1);
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if ((paramIntent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) && (!paramIntent.getBooleanExtra("noConnectivity", false)))
    {
      NetworkInfo localNetworkInfo = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
      if ((localNetworkInfo != null) && (localNetworkInfo.isConnectedOrConnecting()) && ((!k.b("prefWiFi").booleanValue()) || (localNetworkInfo.getType() == 1)))
      {
        a.a(paramContext, new Intent(paramContext, BackgroundService.class));
        b(paramContext);
      }
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/ConnectivityReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */