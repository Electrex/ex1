package flar2.exkernelmanager.fragments;

import android.os.Bundle;
import android.support.v4.app.ar;
import android.support.v7.app.ac;
import android.support.v7.app.ad;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import flar2.exkernelmanager.a.o;
import flar2.exkernelmanager.utilities.e;
import flar2.exkernelmanager.utilities.f;
import flar2.exkernelmanager.utilities.h;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;
import java.util.ArrayList;
import java.util.List;

public class BoostActivity
  extends ad
  implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener
{
  h n;
  private m o = new m();
  private e p = new e();
  private f q = new f();
  private ListView r;
  private flar2.exkernelmanager.a.a s;
  
  private void a(String paramString1, String paramString2)
  {
    ac localac = new ac(this);
    localac.a(getString(2131624091));
    localac.b(2131623999, null);
    EditText localEditText = new EditText(this);
    localac.a(localEditText);
    localEditText.setHint(this.o.b(paramString2));
    localEditText.setInputType(2);
    localac.a(2131624170, new u(this, localEditText, paramString1, paramString2));
    android.support.v7.app.ab localab = localac.a();
    localab.getWindow().setSoftInputMode(5);
    localab.show();
  }
  
  private void b(String paramString1, String paramString2)
  {
    k.a(paramString1 + "Boot", false);
    if (this.o.b(paramString2).equals("0"))
    {
      this.o.a("1", paramString2);
      k.a(paramString1, "1");
    }
    for (;;)
    {
      k();
      return;
      if (this.o.b(paramString2).equals("1"))
      {
        this.o.a("0", paramString2);
        k.a(paramString1, "0");
      }
      else if (this.o.b(paramString2).equals("Y"))
      {
        this.o.a("N", paramString2);
        k.a(paramString1, "N");
      }
      else if (this.o.b(paramString2).equals("N"))
      {
        this.o.a("Y", paramString2);
        k.a(paramString1, "Y");
      }
    }
  }
  
  private void c(String paramString1, String paramString2)
  {
    ac localac = new ac(this);
    localac.a(getString(2131624205));
    localac.b(2131623999, null);
    localac.a(this.p.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies", 1, 0), new v(this, this.p.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies", 0, 0), paramString1, paramString2));
    localac.a().show();
  }
  
  private void d(String paramString1, String paramString2)
  {
    View localView = getLayoutInflater().inflate(2130903105, null);
    Spinner localSpinner1 = (Spinner)localView.findViewById(2131493199);
    Spinner localSpinner2 = (Spinner)localView.findViewById(2131493200);
    Spinner localSpinner3 = (Spinner)localView.findViewById(2131493201);
    Spinner localSpinner4 = (Spinner)localView.findViewById(2131493202);
    String[] arrayOfString1 = this.p.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies", 1, 0);
    String[] arrayOfString2 = this.p.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies", 0, 0);
    ArrayAdapter localArrayAdapter = new ArrayAdapter(this, 17367049, arrayOfString1);
    localSpinner1.setAdapter(localArrayAdapter);
    localSpinner2.setAdapter(localArrayAdapter);
    localSpinner3.setAdapter(localArrayAdapter);
    localSpinner4.setAdapter(localArrayAdapter);
    String str1 = this.o.b(paramString2);
    String[] arrayOfString3 = new String[4];
    arrayOfString3[0] = str1.substring(2 + str1.indexOf("0:"), -1 + str1.indexOf("1:"));
    arrayOfString3[1] = str1.substring(2 + str1.indexOf("1:"), -1 + str1.indexOf("2:"));
    arrayOfString3[2] = str1.substring(2 + str1.indexOf("2:"), -1 + str1.indexOf("3:"));
    arrayOfString3[3] = str1.substring(2 + str1.indexOf("3:"), -1 + str1.length());
    int i = 0;
    int j = arrayOfString2.length;
    for (int k = 0; k < j; k++)
    {
      String str2 = arrayOfString2[k];
      if (str2.equals(arrayOfString3[0])) {
        localSpinner1.setSelection(i);
      }
      if (str2.equals(arrayOfString3[1])) {
        localSpinner2.setSelection(i);
      }
      if (str2.equals(arrayOfString3[2])) {
        localSpinner3.setSelection(i);
      }
      if (str2.equals(arrayOfString3[3])) {
        localSpinner4.setSelection(i);
      }
      i++;
    }
    localSpinner1.setOnItemSelectedListener(new w(this, arrayOfString2, paramString2));
    localSpinner2.setOnItemSelectedListener(new x(this, arrayOfString2, paramString2));
    localSpinner3.setOnItemSelectedListener(new y(this, arrayOfString2, paramString2));
    localSpinner4.setOnItemSelectedListener(new z(this, arrayOfString2, paramString2));
    ac localac = new ac(this);
    localac.a(getString(2131624204)).a(localView).a(getString(2131624170), new s(this, paramString1, paramString2)).b(2131623999, new aa(this, str1, paramString2, paramString1));
    localac.a().show();
  }
  
  private void k()
  {
    new ab(this, null).execute(new Void[0]);
  }
  
  private List l()
  {
    ArrayList localArrayList = new ArrayList();
    o localo1 = new o();
    localo1.a(0);
    localo1.a(getString(2131624010));
    localArrayList.add(localo1);
    o localo2;
    label148:
    o localo3;
    label250:
    o localo4;
    label336:
    label370:
    o localo5;
    label472:
    o localo6;
    label555:
    label589:
    o localo7;
    label691:
    o localo8;
    if (this.p.b("/sys/module/cpu_boost/parameters/cpuboost_enable"))
    {
      localo2 = new o();
      localo2.a(1);
      localo2.b(64835);
      localo2.a("CPU Boost enabled");
      if (this.o.b("/sys/module/cpu_boost/parameters/cpuboost_enable").equals("N"))
      {
        localo2.b(getString(2131624058));
        localo2.d("prefBoostEnabledBoot");
        if (!k.b("prefBoostEnabledBoot").booleanValue()) {
          break label826;
        }
        localo2.a(true);
        localo2.c(2130837611);
        localArrayList.add(localo2);
      }
    }
    else
    {
      if (this.p.b("/sys/module/cpu_boost/parameters/boost_ms"))
      {
        localo3 = new o();
        localo3.a(1);
        localo3.b(64836);
        localo3.a("Boost milliseconds");
        localo3.b(this.o.b("/sys/module/cpu_boost/parameters/boost_ms"));
        localo3.d("prefBoostMSBoot");
        if (!k.b("prefBoostMSBoot").booleanValue()) {
          break label843;
        }
        localo3.a(true);
        localo3.c(2130837611);
        localArrayList.add(localo3);
      }
      if (this.p.b("/sys/module/cpu_boost/parameters/input_boost_freq"))
      {
        localo4 = new o();
        localo4.a(1);
        localo4.b(64834);
        localo4.a("Input boost frequency");
        if (!this.o.b("/sys/module/cpu_boost/parameters/input_boost_freq").contains("0:")) {
          break label860;
        }
        localo4.b(this.o.b("/sys/module/cpu_boost/parameters/input_boost_freq"));
        localo4.d("prefBoostInputBoostFreqBoot");
        if (!k.b("prefBoostInputBoostFreqBoot").booleanValue()) {
          break label885;
        }
        localo4.a(true);
        localo4.c(2130837611);
        localArrayList.add(localo4);
      }
      if (this.p.b("/sys/module/cpu_boost/parameters/input_boost_ms"))
      {
        localo5 = new o();
        localo5.a(1);
        localo5.b(64833);
        localo5.a("Input boost milliseconds");
        localo5.b(this.o.b("/sys/module/cpu_boost/parameters/input_boost_ms"));
        localo5.d("prefBoostInputBoostMSBoot");
        if (!k.b("prefBoostInputBoostMSBoot").booleanValue()) {
          break label902;
        }
        localo5.a(true);
        localo5.c(2130837611);
        localArrayList.add(localo5);
      }
      if (this.p.b("/sys/module/cpu_boost/parameters/load_based_syncs"))
      {
        localo6 = new o();
        localo6.a(1);
        localo6.b(64832);
        localo6.a("Load based syncs");
        if (!this.o.b("/sys/module/cpu_boost/parameters/load_based_syncs").equals("N")) {
          break label919;
        }
        localo6.b(getString(2131624058));
        localo6.d("prefBoostLoadBasedSyncsBoot");
        if (!k.b("prefBoostLoadBasedSyncsBoot").booleanValue()) {
          break label934;
        }
        localo6.a(true);
        localo6.c(2130837611);
        localArrayList.add(localo6);
      }
      if (this.p.b("/sys/module/cpu_boost/parameters/migration_load_threshold"))
      {
        localo7 = new o();
        localo7.a(1);
        localo7.b(64831);
        localo7.a("Migration load threshold");
        localo7.b(this.o.b("/sys/module/cpu_boost/parameters/migration_load_threshold"));
        localo7.d("prefBoostMigrationLoadThresholdBoot");
        if (!k.b("prefBoostMigrationLoadThresholdBoot").booleanValue()) {
          break label951;
        }
        localo7.a(true);
        localo7.c(2130837611);
        localArrayList.add(localo7);
      }
      if (this.p.b("/sys/module/cpu_boost/parameters/sync_threshold"))
      {
        localo8 = new o();
        localo8.a(1);
        localo8.b(64830);
        localo8.a("Sync threshold frequency");
        localo8.b(this.p.c(this.o.b("/sys/module/cpu_boost/parameters/sync_threshold")));
        localo8.d("prefBoostSyncThresholdBoot");
        if (!k.b("prefBoostSyncThresholdBoot").booleanValue()) {
          break label968;
        }
        localo8.a(true);
        localo8.c(2130837611);
      }
    }
    for (;;)
    {
      localArrayList.add(localo8);
      return localArrayList;
      localo2.b(getString(2131624077));
      break;
      label826:
      localo2.a(false);
      localo2.c(2130837609);
      break label148;
      label843:
      localo3.a(false);
      localo3.c(2130837609);
      break label250;
      label860:
      localo4.b(this.p.c(this.o.b("/sys/module/cpu_boost/parameters/input_boost_freq")));
      break label336;
      label885:
      localo4.a(false);
      localo4.c(2130837609);
      break label370;
      label902:
      localo5.a(false);
      localo5.c(2130837609);
      break label472;
      label919:
      localo6.b(getString(2131624077));
      break label555;
      label934:
      localo6.a(false);
      localo6.c(2130837609);
      break label589;
      label951:
      localo7.a(false);
      localo7.c(2130837609);
      break label691;
      label968:
      localo8.a(false);
      localo8.c(2130837609);
    }
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    this.n.b().onTouchEvent(paramMotionEvent);
    return super.dispatchTouchEvent(paramMotionEvent);
  }
  
  public void onBackPressed()
  {
    super.onBackPressed();
    overridePendingTransition(0, 2130968604);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903069);
    a((Toolbar)findViewById(2131492951));
    overridePendingTransition(2130968605, 17432577);
    setTitle(getString(2131624012));
    g().a(true);
    this.n = new r(this, this);
    findViewById(2131492988).setOnTouchListener(this.n);
    this.r = ((ListView)findViewById(2131492989));
    this.s = new flar2.exkernelmanager.a.a(this, new ArrayList());
    this.r.setAdapter(this.s);
    this.r.setOnItemClickListener(this);
    this.r.setOnItemLongClickListener(this);
    this.r.setOnScrollListener(new t(this));
    k();
  }
  
  public void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    switch (((o)this.s.getItem(paramInt)).b())
    {
    default: 
      return;
    case -700: 
      a("prefBoostMS", "/sys/module/cpu_boost/parameters/boost_ms");
      return;
    case -701: 
      b("prefBoostEnable", "/sys/module/cpu_boost/parameters/cpuboost_enable");
      return;
    case -702: 
      if (this.o.b("/sys/module/cpu_boost/parameters/input_boost_freq").contains("0:"))
      {
        d("prefBoostInputBoostFreq", "/sys/module/cpu_boost/parameters/input_boost_freq");
        return;
      }
      c("prefBoostInputBoostFreq", "/sys/module/cpu_boost/parameters/input_boost_freq");
      return;
    case -703: 
      a("prefBoostInputBoostMS", "/sys/module/cpu_boost/parameters/input_boost_ms");
      return;
    case -704: 
      b("prefBoostLoadBasedSyncs", "/sys/module/cpu_boost/parameters/load_based_syncs");
      return;
    case -705: 
      a("prefBoostMigrationLoadThreshold", "/sys/module/cpu_boost/parameters/migration_load_threshold");
      return;
    }
    c("prefBoostSyncThreshold", "/sys/module/cpu_boost/parameters/sync_threshold");
  }
  
  public boolean onItemLongClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    switch (((o)this.s.getItem(paramInt)).b())
    {
    }
    for (;;)
    {
      return true;
      Toast.makeText(this, "This is how you change doubletap2wake ", 0).show();
    }
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(paramMenuItem);
    }
    ar.a(this);
    return true;
  }
  
  public void onResume()
  {
    super.onResume();
    k();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/BoostActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */