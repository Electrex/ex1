package flar2.exkernelmanager.fragments;

import android.os.Bundle;
import android.support.v4.app.ar;
import android.support.v7.app.ab;
import android.support.v7.app.ac;
import android.support.v7.app.ad;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import flar2.exkernelmanager.a.o;
import flar2.exkernelmanager.utilities.e;
import flar2.exkernelmanager.utilities.f;
import flar2.exkernelmanager.utilities.h;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;
import java.util.ArrayList;
import java.util.List;

public class GovernorC2Activity
  extends ad
  implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener
{
  h n;
  private m o = new m();
  private e p = new e();
  private f q = new f();
  private String r = null;
  private ListView s;
  private flar2.exkernelmanager.a.a t;
  private String u;
  
  private void a(String paramString1, String paramString2)
  {
    ac localac = new ac(this);
    localac.a(getString(2131624091));
    localac.b(2131623999, null);
    EditText localEditText = new EditText(this);
    localac.a(localEditText);
    localEditText.setHint(this.o.a(paramString2));
    localEditText.setInputType(2);
    localac.a(2131624170, new cr(this, localEditText, paramString1, paramString2));
    ab localab = localac.a();
    localab.getWindow().setSoftInputMode(5);
    localab.show();
  }
  
  private void b(String paramString1, String paramString2)
  {
    k.a(paramString1 + "Boot", false);
    if (this.o.a(paramString2).equals("0"))
    {
      this.o.a("1", paramString2);
      k.a(paramString1, "1");
    }
    for (;;)
    {
      k();
      return;
      if (this.o.a(paramString2).equals("1"))
      {
        this.o.a("0", paramString2);
        k.a(paramString1, "0");
      }
      else if (this.o.a(paramString2).equals("Y"))
      {
        this.o.a("N", paramString2);
        k.a(paramString1, "N");
      }
      else if (this.o.a(paramString2).equals("N"))
      {
        this.o.a("Y", paramString2);
        k.a(paramString1, "Y");
      }
    }
  }
  
  private void c(String paramString1, String paramString2)
  {
    ac localac = new ac(this);
    localac.a(getString(2131624205));
    localac.b(2131623999, null);
    localac.a(this.p.a("/sys/devices/system/cpu/cpu4/cpufreq/scaling_available_frequencies", 1, 0), new cs(this, this.p.a("/sys/devices/system/cpu/cpu4/cpufreq/scaling_available_frequencies", 0, 0), paramString1, paramString2));
    localac.a().show();
  }
  
  private void d(String paramString1, String paramString2)
  {
    View localView = getLayoutInflater().inflate(2130903105, null);
    Spinner localSpinner1 = (Spinner)localView.findViewById(2131493199);
    Spinner localSpinner2 = (Spinner)localView.findViewById(2131493200);
    Spinner localSpinner3 = (Spinner)localView.findViewById(2131493201);
    Spinner localSpinner4 = (Spinner)localView.findViewById(2131493202);
    String[] arrayOfString1 = this.p.a("/sys/devices/system/cpu/cpu4/cpufreq/scaling_available_frequencies", 1, 0);
    String[] arrayOfString2 = this.p.a("/sys/devices/system/cpu/cpu4/cpufreq/scaling_available_frequencies", 0, 0);
    ArrayAdapter localArrayAdapter = new ArrayAdapter(this, 17367049, arrayOfString1);
    localSpinner1.setAdapter(localArrayAdapter);
    localSpinner2.setAdapter(localArrayAdapter);
    localSpinner3.setAdapter(localArrayAdapter);
    localSpinner4.setAdapter(localArrayAdapter);
    String str1 = this.o.a(paramString2);
    String[] arrayOfString3 = str1.split(",");
    int i = 0;
    int j = arrayOfString2.length;
    for (int k = 0; k < j; k++)
    {
      String str2 = arrayOfString2[k];
      if (str2.equals(arrayOfString3[0])) {
        localSpinner1.setSelection(i);
      }
      if (str2.equals(arrayOfString3[1])) {
        localSpinner2.setSelection(i);
      }
      if (str2.equals(arrayOfString3[2])) {
        localSpinner3.setSelection(i);
      }
      if (str2.equals(arrayOfString3[3])) {
        localSpinner4.setSelection(i);
      }
      i++;
    }
    localSpinner1.setOnItemSelectedListener(new ct(this, arrayOfString2, paramString2));
    localSpinner2.setOnItemSelectedListener(new cu(this, paramString2, arrayOfString2));
    localSpinner3.setOnItemSelectedListener(new cv(this, paramString2, arrayOfString2));
    localSpinner4.setOnItemSelectedListener(new cw(this, paramString2, arrayOfString2));
    ac localac = new ac(this);
    localac.a(getString(2131624204)).a(localView).a(getString(2131624170), new cp(this, paramString1, paramString2)).b(2131623999, new cx(this, str1, paramString2, paramString1));
    localac.a().show();
  }
  
  private void k()
  {
    new cy(this, null).execute(new Void[0]);
  }
  
  private void l()
  {
    new f().c("chmod 666 " + this.u + "/*");
  }
  
  private List m()
  {
    ArrayList localArrayList = new ArrayList();
    o localo1 = new o();
    localo1.a(0);
    localo1.a(getString(2131624014));
    localArrayList.add(localo1);
    o localo2;
    label188:
    o localo3;
    label421:
    o localo4;
    label570:
    o localo5;
    label719:
    o localo6;
    label868:
    o localo7;
    label1017:
    o localo8;
    label1178:
    o localo9;
    label1320:
    o localo10;
    String[] arrayOfString2;
    label1526:
    label1560:
    o localo11;
    label1702:
    o localo12;
    label1844:
    o localo37;
    label2008:
    o localo38;
    label2157:
    o localo13;
    label2299:
    o localo14;
    label2441:
    o localo15;
    label2590:
    o localo16;
    label2732:
    o localo17;
    label2874:
    o localo18;
    label3016:
    o localo19;
    label3158:
    o localo20;
    label3300:
    o localo21;
    label3442:
    o localo22;
    label3591:
    o localo23;
    label3733:
    o localo24;
    label3875:
    o localo25;
    label4017:
    o localo26;
    label4159:
    o localo27;
    label4301:
    o localo28;
    label4450:
    o localo29;
    label4592:
    o localo30;
    label4741:
    o localo31;
    label4883:
    o localo32;
    label5025:
    o localo33;
    label5167:
    o localo34;
    label5309:
    o localo35;
    label5451:
    o localo36;
    if (this.p.a(this.u + "gboost"))
    {
      localo2 = new o();
      localo2.a(1);
      localo2.b(58437);
      localo2.a("gboost");
      if (this.o.a(this.u + "gboost").equals("0"))
      {
        localo2.b(getString(2131624058));
        localo2.d("prefC2GBoostBoot");
        if (!k.b("prefC2GBoostBoot").booleanValue()) {
          break label5619;
        }
        localo2.a(true);
        localo2.c(2130837611);
        localArrayList.add(localo2);
      }
    }
    else
    {
      if (this.p.a(this.u + "input_event_min_freq"))
      {
        localo3 = new o();
        localo3.a(1);
        localo3.b(58237);
        localo3.a("input_event_min_freq");
        String[] arrayOfString1 = this.o.a(this.u + "input_event_min_freq").split(",");
        localo3.b(this.p.c(arrayOfString1[0]) + ", " + this.p.c(arrayOfString1[1]) + ", " + this.p.c(arrayOfString1[2]) + ", " + this.p.c(arrayOfString1[3]));
        localo3.d("prefC2InputMinFreqBoot");
        if (!k.b("prefC2InputMinFreqBoot").booleanValue()) {
          break label5636;
        }
        localo3.a(true);
        localo3.c(2130837611);
        localArrayList.add(localo3);
      }
      if (this.p.a(this.u + "active_floor_freq"))
      {
        localo4 = new o();
        localo4.a(1);
        localo4.b(-92699);
        localo4.a("active_floor_freq");
        localo4.b(this.p.c(this.o.a(this.u + "active_floor_freq")));
        localo4.d("prefC2ActiveFloorFreqBoot");
        if (!k.b("prefC2ActiveFloorFreqBoot").booleanValue()) {
          break label5653;
        }
        localo4.a(true);
        localo4.c(2130837611);
        localArrayList.add(localo4);
      }
      if (this.p.a(this.u + "input_min_freq"))
      {
        localo5 = new o();
        localo5.a(1);
        localo5.b(-92399);
        localo5.a("input_min_freq");
        localo5.b(this.p.c(this.o.a(this.u + "input_min_freq")));
        localo5.d("PREFC2_INPUT_MIN_FREQ2Boot");
        if (!k.b("PREFC2_INPUT_MIN_FREQ2Boot").booleanValue()) {
          break label5670;
        }
        localo5.a(true);
        localo5.c(2130837611);
        localArrayList.add(localo5);
      }
      if (this.p.a(this.u + "gboost_min_freq"))
      {
        localo6 = new o();
        localo6.a(1);
        localo6.b(-92499);
        localo6.a("gboost_min_freq");
        localo6.b(this.p.c(this.o.a(this.u + "gboost_min_freq")));
        localo6.d("PREFC2_GBOOST_MIN_FREQBoot");
        if (!k.b("PREFC2_GBOOST_MIN_FREQBoot").booleanValue()) {
          break label5687;
        }
        localo6.a(true);
        localo6.c(2130837611);
        localArrayList.add(localo6);
      }
      if (this.p.a(this.u + "max_screen_off_freq"))
      {
        localo7 = new o();
        localo7.a(1);
        localo7.b(-92599);
        localo7.a("max_screen_off_freq");
        localo7.b(this.p.c(this.o.a(this.u + "max_screen_off_freq")));
        localo7.d("PREFC2_MAX_SCREEN_OFF_FREQBoot");
        if (!k.b("PREFC2_MAX_SCREEN_OFF_FREQBoot").booleanValue()) {
          break label5704;
        }
        localo7.a(true);
        localo7.c(2130837611);
        localArrayList.add(localo7);
      }
      if (this.p.a(this.u + "input_event_timeout"))
      {
        localo8 = new o();
        localo8.a(1);
        localo8.b(58137);
        localo8.a("input_event_timeout");
        localo8.b(this.o.a(new StringBuilder().append(this.u).append("input_event_timeout").toString()) + "ms");
        localo8.d("prefC2InputTimeoutBoot");
        if (!k.b("prefC2InputTimeoutBoot").booleanValue()) {
          break label5721;
        }
        localo8.a(true);
        localo8.c(2130837611);
        localArrayList.add(localo8);
      }
      if (this.p.a(this.u + "ui_sampling_rate"))
      {
        localo9 = new o();
        localo9.a(1);
        localo9.b(58037);
        localo9.a("ui_sampling_rate");
        localo9.b(this.o.a(this.u + "ui_sampling_rate"));
        localo9.d("prefC2UISamplingRateBoot");
        if (!k.b("prefC2UISamplingRateBoot").booleanValue()) {
          break label5738;
        }
        localo9.a(true);
        localo9.c(2130837611);
        localArrayList.add(localo9);
      }
      if (this.p.a(this.u + "two_phase_freq"))
      {
        localo10 = new o();
        localo10.a(1);
        localo10.b(57937);
        localo10.a("two_phase_freq");
        arrayOfString2 = this.o.a(this.u + "two_phase_freq").split(",");
        if (arrayOfString2.length <= 1) {
          break label5755;
        }
        localo10.b(this.p.c(arrayOfString2[0]) + ", " + this.p.c(arrayOfString2[1]) + ", " + this.p.c(arrayOfString2[2]) + ", " + this.p.c(arrayOfString2[3]));
        localo10.d("prefC2TwoPhaseFreqBoot");
        if (!k.b("prefC2TwoPhaseFreqBoot").booleanValue()) {
          break label5774;
        }
        localo10.a(true);
        localo10.c(2130837611);
        localArrayList.add(localo10);
      }
      if (this.p.a(this.u + "up_threshold"))
      {
        localo11 = new o();
        localo11.a(1);
        localo11.b(57837);
        localo11.a("up_threshold");
        localo11.b(this.o.a(this.u + "up_threshold"));
        localo11.d("prefC2UpThresholdBoot");
        if (!k.b("prefC2UpThresholdBoot").booleanValue()) {
          break label5791;
        }
        localo11.a(true);
        localo11.c(2130837611);
        localArrayList.add(localo11);
      }
      if (this.p.a(this.u + "down_differential"))
      {
        localo12 = new o();
        localo12.a(1);
        localo12.b(57737);
        localo12.a("down_differential");
        localo12.b(this.o.a(this.u + "down_differential"));
        localo12.d("prefC2DownDifferentialBoot");
        if (!k.b("prefC2DownDifferentialBoot").booleanValue()) {
          break label5808;
        }
        localo12.a(true);
        localo12.c(2130837611);
        localArrayList.add(localo12);
      }
      if (!k.a("prefDeviceName").contains("HTC_One_m"))
      {
        if (this.p.a(this.u + "sync_freq"))
        {
          localo37 = new o();
          localo37.a(1);
          localo37.b(57637);
          localo37.a("sync_freq");
          localo37.b(this.p.c(this.o.a(this.u + "sync_freq")));
          localo37.d("prefC2SyncFreqBoot");
          if (!k.b("prefC2SyncFreqBoot").booleanValue()) {
            break label5825;
          }
          localo37.a(true);
          localo37.c(2130837611);
          localArrayList.add(localo37);
        }
        if (this.p.a(this.u + "optimal_freq"))
        {
          localo38 = new o();
          localo38.a(1);
          localo38.b(57537);
          localo38.a("optimal_freq");
          localo38.b(this.p.c(this.o.a(this.u + "optimal_freq")));
          localo38.d("prefC2OptFreqBoot");
          if (!k.b("prefC2OptFreqBoot").booleanValue()) {
            break label5842;
          }
          localo38.a(true);
          localo38.c(2130837611);
          localArrayList.add(localo38);
        }
      }
      if (this.p.a(this.u + "shortcut"))
      {
        localo13 = new o();
        localo13.a(1);
        localo13.b(58337);
        localo13.a("shortcut");
        localo13.b(this.o.a(this.u + "shortcut"));
        localo13.d("prefC2ShortcutBoot");
        if (!k.b("prefC2ShortcutBoot").booleanValue()) {
          break label5859;
        }
        localo13.a(true);
        localo13.c(2130837611);
        localArrayList.add(localo13);
      }
      if (this.p.a(this.u + "sampling_rate_min"))
      {
        localo14 = new o();
        localo14.a(1);
        localo14.b(64636);
        localo14.a("sampling_rate_min");
        localo14.b(this.o.a(this.u + "sampling_rate_min"));
        localo14.d("PREF_SAMPLING_RATE_MINBoot");
        if (!k.b("PREF_SAMPLING_RATE_MINBoot").booleanValue()) {
          break label5876;
        }
        localo14.a(true);
        localo14.c(2130837611);
        localArrayList.add(localo14);
      }
      if (this.p.a(this.u + "freq_down_step_barriar"))
      {
        localo15 = new o();
        localo15.a(1);
        localo15.b(-90199);
        localo15.a("freq_down_step_barrier");
        localo15.b(this.p.c(this.o.a(this.u + "freq_down_step_barriar")));
        localo15.d("PREFC2_FREQ_DOWN_STEP_BARRIARBoot");
        if (!k.b("PREFC2_FREQ_DOWN_STEP_BARRIARBoot").booleanValue()) {
          break label5893;
        }
        localo15.a(true);
        localo15.c(2130837611);
        localArrayList.add(localo15);
      }
      if (this.p.a(this.u + "freq_down_step"))
      {
        localo16 = new o();
        localo16.a(1);
        localo16.b(-90299);
        localo16.a("freq_down_step");
        localo16.b(this.o.a(this.u + "freq_down_step"));
        localo16.d("PREFC2_FREQ_DOWN_STEPBoot");
        if (!k.b("PREFC2_FREQ_DOWN_STEPBoot").booleanValue()) {
          break label5910;
        }
        localo16.a(true);
        localo16.c(2130837611);
        localArrayList.add(localo16);
      }
      if (this.p.a(this.u + "sampling_rate"))
      {
        localo17 = new o();
        localo17.a(1);
        localo17.b(-90399);
        localo17.a("sampling_rate");
        localo17.b(this.o.a(this.u + "sampling_rate"));
        localo17.d("PREFC2_SAMPLING_RATEBoot");
        if (!k.b("PREFC2_SAMPLING_RATEBoot").booleanValue()) {
          break label5927;
        }
        localo17.a(true);
        localo17.c(2130837611);
        localArrayList.add(localo17);
      }
      if (this.p.a(this.u + "sampling_down_factor"))
      {
        localo18 = new o();
        localo18.a(1);
        localo18.b(-90499);
        localo18.a("sampling_down_factor");
        localo18.b(this.o.a(this.u + "sampling_down_factor"));
        localo18.d("PREFC2_SAMPLING_DOWN_FACTORBoot");
        if (!k.b("PREFC2_SAMPLING_DOWN_FACTORBoot").booleanValue()) {
          break label5944;
        }
        localo18.a(true);
        localo18.c(2130837611);
        localArrayList.add(localo18);
      }
      if (this.p.a(this.u + "up_threshold_any_cpu_load"))
      {
        localo19 = new o();
        localo19.a(1);
        localo19.b(-90699);
        localo19.a("up_threshold_any_cpu_load");
        localo19.b(this.o.a(this.u + "up_threshold_any_cpu_load"));
        localo19.d("PREFC2_UP_THRESHOLD_ANY_CPU_LOADBoot");
        if (!k.b("PREFC2_UP_THRESHOLD_ANY_CPU_LOADBoot").booleanValue()) {
          break label5961;
        }
        localo19.a(true);
        localo19.c(2130837611);
        localArrayList.add(localo19);
      }
      if (this.p.a(this.u + "target_loads"))
      {
        localo20 = new o();
        localo20.a(1);
        localo20.b(-90799);
        localo20.a("target_loads");
        localo20.b(this.o.a(this.u + "target_loads"));
        localo20.d("PREFC2_TARGET_LOADSBoot");
        if (!k.b("PREFC2_TARGET_LOADSBoot").booleanValue()) {
          break label5978;
        }
        localo20.a(true);
        localo20.c(2130837611);
        localArrayList.add(localo20);
      }
      if (this.p.a(this.u + "timer_slack"))
      {
        localo21 = new o();
        localo21.a(1);
        localo21.b(-90899);
        localo21.a("timer_slack");
        localo21.b(this.o.a(this.u + "timer_slack"));
        localo21.d("PREFC2_TIMER_SLACKBoot");
        if (!k.b("PREFC2_TIMER_SLACKBoot").booleanValue()) {
          break label5995;
        }
        localo21.a(true);
        localo21.c(2130837611);
        localArrayList.add(localo21);
      }
      if (this.p.a(this.u + "hispeed_freq"))
      {
        localo22 = new o();
        localo22.a(1);
        localo22.b(-91099);
        localo22.a("hispeed_freq");
        localo22.b(this.p.c(this.o.a(this.u + "hispeed_freq")));
        localo22.d("PREFC2_HISPEED_FREQBoot");
        if (!k.b("PREFC2_HISPEED_FREQBoot").booleanValue()) {
          break label6012;
        }
        localo22.a(true);
        localo22.c(2130837611);
        localArrayList.add(localo22);
      }
      if (this.p.a(this.u + "timer_rate"))
      {
        localo23 = new o();
        localo23.a(1);
        localo23.b(-91199);
        localo23.a("timer_rate");
        localo23.b(this.o.a(this.u + "timer_rate"));
        localo23.d("PREFC2_TIMER_RATEBoot");
        if (!k.b("PREFC2_TIMER_RATEBoot").booleanValue()) {
          break label6029;
        }
        localo23.a(true);
        localo23.c(2130837611);
        localArrayList.add(localo23);
      }
      if (this.p.a(this.u + "above_hispeed_delay"))
      {
        localo24 = new o();
        localo24.a(1);
        localo24.b(-91299);
        localo24.a("above_hispeed_delay");
        localo24.b(this.o.a(this.u + "above_hispeed_delay"));
        localo24.d("PREFC2_ABOVE_HISPEED_DELAYBoot");
        if (!k.b("PREFC2_ABOVE_HISPEED_DELAYBoot").booleanValue()) {
          break label6046;
        }
        localo24.a(true);
        localo24.c(2130837611);
        localArrayList.add(localo24);
      }
      if (this.p.a(this.u + "boostpulse"))
      {
        localo25 = new o();
        localo25.a(1);
        localo25.b(-91399);
        localo25.a("boostpulse");
        localo25.b(this.o.a(this.u + "boostpulse"));
        localo25.d("PREFC2_BOOSTPULSEBoot");
        if (!k.b("PREFC2_BOOSTPULSEBoot").booleanValue()) {
          break label6063;
        }
        localo25.a(true);
        localo25.c(2130837611);
        localArrayList.add(localo25);
      }
      if (this.p.a(this.u + "boostpulse_duration"))
      {
        localo26 = new o();
        localo26.a(1);
        localo26.b(-91499);
        localo26.a("boostpulse_duration");
        localo26.b(this.o.a(this.u + "boostpulse_duration"));
        localo26.d("PREFC2_BOOSTPULSE_DURATIONBoot");
        if (!k.b("PREFC2_BOOSTPULSE_DURATIONBoot").booleanValue()) {
          break label6080;
        }
        localo26.a(true);
        localo26.c(2130837611);
        localArrayList.add(localo26);
      }
      if (this.p.a(this.u + "go_hispeed_load"))
      {
        localo27 = new o();
        localo27.a(1);
        localo27.b(-91599);
        localo27.a("go_hispeed_load");
        localo27.b(this.o.a(this.u + "go_hispeed_load"));
        localo27.d("PREFC2_GO_HISPEED_LOADBoot");
        if (!k.b("PREFC2_GO_HISPEED_LOADBoot").booleanValue()) {
          break label6097;
        }
        localo27.a(true);
        localo27.c(2130837611);
        localArrayList.add(localo27);
      }
      if (this.p.a(this.u + "input_boost_freq"))
      {
        localo28 = new o();
        localo28.a(1);
        localo28.b(-91699);
        localo28.a("input_boost_freq");
        localo28.b(this.p.c(this.o.a(this.u + "input_boost_freq")));
        localo28.d("PREFC2_INPUT_BOOST_FREQBoot");
        if (!k.b("PREFC2_INPUT_BOOST_FREQBoot").booleanValue()) {
          break label6114;
        }
        localo28.a(true);
        localo28.c(2130837611);
        localArrayList.add(localo28);
      }
      if (this.p.a(this.u + "min_sample_time"))
      {
        localo29 = new o();
        localo29.a(1);
        localo29.b(-91799);
        localo29.a("min_sample_time");
        localo29.b(this.o.a(this.u + "min_sample_time"));
        localo29.d("PREFC2_MIN_SAMPLE_TIMEBoot");
        if (!k.b("PREFC2_MIN_SAMPLE_TIMEBoot").booleanValue()) {
          break label6131;
        }
        localo29.a(true);
        localo29.c(2130837611);
        localArrayList.add(localo29);
      }
      if (this.p.a(this.u + "up_threshold_any_cpu_freq"))
      {
        localo30 = new o();
        localo30.a(1);
        localo30.b(-91899);
        localo30.a("up_threshold_any_cpu_freq");
        localo30.b(this.p.c(this.o.a(this.u + "up_threshold_any_cpu_freq")));
        localo30.d("PREFC2_UP_THRESHOLD_ANY_CPU_FREQBoot");
        if (!k.b("PREFC2_UP_THRESHOLD_ANY_CPU_FREQBoot").booleanValue()) {
          break label6148;
        }
        localo30.a(true);
        localo30.c(2130837611);
        localArrayList.add(localo30);
      }
      if (this.p.a(this.u + "down_threshold"))
      {
        localo31 = new o();
        localo31.a(1);
        localo31.b(-92099);
        localo31.a("down_threshold");
        localo31.b(this.o.a(this.u + "down_threshold"));
        localo31.d("PREFC2_DOWN_THRESHOLDBoot");
        if (!k.b("PREFC2_DOWN_THRESHOLDBoot").booleanValue()) {
          break label6165;
        }
        localo31.a(true);
        localo31.c(2130837611);
        localArrayList.add(localo31);
      }
      if (this.p.a(this.u + "freq_step"))
      {
        localo32 = new o();
        localo32.a(1);
        localo32.b(-92199);
        localo32.a("freq_step");
        localo32.b(this.o.a(this.u + "freq_step"));
        localo32.d("PREFC2_FREQ_STEPBoot");
        if (!k.b("PREFC2_FREQ_STEPBoot").booleanValue()) {
          break label6182;
        }
        localo32.a(true);
        localo32.c(2130837611);
        localArrayList.add(localo32);
      }
      if (this.p.a(this.u + "boost"))
      {
        localo33 = new o();
        localo33.a(1);
        localo33.b(-92299);
        localo33.a("boost");
        localo33.b(this.o.a(this.u + "boost"));
        localo33.d("PREFC2_BOOSTBoot");
        if (!k.b("PREFC2_BOOSTBoot").booleanValue()) {
          break label6199;
        }
        localo33.a(true);
        localo33.c(2130837611);
        localArrayList.add(localo33);
      }
      if (this.p.a(this.u + "input_boost_duration"))
      {
        localo34 = new o();
        localo34.a(1);
        localo34.b(-92999);
        localo34.a("input boost duration");
        localo34.b(this.o.a(this.u + "input_boost_duration"));
        localo34.d("prefC2InputBoostDurationBoot");
        if (!k.b("prefC2InputBoostDurationBoot").booleanValue()) {
          break label6216;
        }
        localo34.a(true);
        localo34.c(2130837611);
        localArrayList.add(localo34);
      }
      if (this.p.a(this.u + "low_load_down_threshold"))
      {
        localo35 = new o();
        localo35.a(1);
        localo35.b(-92799);
        localo35.a("low load down threshold");
        localo35.b(this.o.a(this.u + "low_load_down_threshold"));
        localo35.d("prefC2LowLoadDownThresholdBoot");
        if (!k.b("prefC2LowLoadDownThresholdBoot").booleanValue()) {
          break label6233;
        }
        localo35.a(true);
        localo35.c(2130837611);
        localArrayList.add(localo35);
      }
      if (this.p.a(this.u + "max_freq_hysteresis"))
      {
        localo36 = new o();
        localo36.a(1);
        localo36.b(-92899);
        localo36.a("max freq hysteresis");
        localo36.b(this.o.a(this.u + "max_freq_hysteresis"));
        localo36.d("prefC2MaxFreqHysteresisBoot");
        if (!k.b("prefC2MaxFreqHysteresisBoot").booleanValue()) {
          break label6250;
        }
        localo36.a(true);
        localo36.c(2130837611);
      }
    }
    for (;;)
    {
      localArrayList.add(localo36);
      return localArrayList;
      localo2.b(getString(2131624077));
      break;
      label5619:
      localo2.a(false);
      localo2.c(2130837609);
      break label188;
      label5636:
      localo3.a(false);
      localo3.c(2130837609);
      break label421;
      label5653:
      localo4.a(false);
      localo4.c(2130837609);
      break label570;
      label5670:
      localo5.a(false);
      localo5.c(2130837609);
      break label719;
      label5687:
      localo6.a(false);
      localo6.c(2130837609);
      break label868;
      label5704:
      localo7.a(false);
      localo7.c(2130837609);
      break label1017;
      label5721:
      localo8.a(false);
      localo8.c(2130837609);
      break label1178;
      label5738:
      localo9.a(false);
      localo9.c(2130837609);
      break label1320;
      label5755:
      localo10.b(this.p.c(arrayOfString2[0]));
      break label1526;
      label5774:
      localo10.a(false);
      localo10.c(2130837609);
      break label1560;
      label5791:
      localo11.a(false);
      localo11.c(2130837609);
      break label1702;
      label5808:
      localo12.a(false);
      localo12.c(2130837609);
      break label1844;
      label5825:
      localo37.a(false);
      localo37.c(2130837609);
      break label2008;
      label5842:
      localo38.a(false);
      localo38.c(2130837609);
      break label2157;
      label5859:
      localo13.a(false);
      localo13.c(2130837609);
      break label2299;
      label5876:
      localo14.a(false);
      localo14.c(2130837609);
      break label2441;
      label5893:
      localo15.a(false);
      localo15.c(2130837609);
      break label2590;
      label5910:
      localo16.a(false);
      localo16.c(2130837609);
      break label2732;
      label5927:
      localo17.a(false);
      localo17.c(2130837609);
      break label2874;
      label5944:
      localo18.a(false);
      localo18.c(2130837609);
      break label3016;
      label5961:
      localo19.a(false);
      localo19.c(2130837609);
      break label3158;
      label5978:
      localo20.a(false);
      localo20.c(2130837609);
      break label3300;
      label5995:
      localo21.a(false);
      localo21.c(2130837609);
      break label3442;
      label6012:
      localo22.a(false);
      localo22.c(2130837609);
      break label3591;
      label6029:
      localo23.a(false);
      localo23.c(2130837609);
      break label3733;
      label6046:
      localo24.a(false);
      localo24.c(2130837609);
      break label3875;
      label6063:
      localo25.a(false);
      localo25.c(2130837609);
      break label4017;
      label6080:
      localo26.a(false);
      localo26.c(2130837609);
      break label4159;
      label6097:
      localo27.a(false);
      localo27.c(2130837609);
      break label4301;
      label6114:
      localo28.a(false);
      localo28.c(2130837609);
      break label4450;
      label6131:
      localo29.a(false);
      localo29.c(2130837609);
      break label4592;
      label6148:
      localo30.a(false);
      localo30.c(2130837609);
      break label4741;
      label6165:
      localo31.a(false);
      localo31.c(2130837609);
      break label4883;
      label6182:
      localo32.a(false);
      localo32.c(2130837609);
      break label5025;
      label6199:
      localo33.a(false);
      localo33.c(2130837609);
      break label5167;
      label6216:
      localo34.a(false);
      localo34.c(2130837609);
      break label5309;
      label6233:
      localo35.a(false);
      localo35.c(2130837609);
      break label5451;
      label6250:
      localo36.a(false);
      localo36.c(2130837609);
    }
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    this.n.b().onTouchEvent(paramMotionEvent);
    return super.dispatchTouchEvent(paramMotionEvent);
  }
  
  public void onBackPressed()
  {
    super.onBackPressed();
    overridePendingTransition(0, 2130968604);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903073);
    a((Toolbar)findViewById(2131492951));
    overridePendingTransition(2130968605, 17432577);
    setTitle(getString(2131624015));
    g().a(true);
    this.n = new co(this, this);
    findViewById(2131493039).setOnTouchListener(this.n);
    this.s = ((ListView)findViewById(2131492989));
    this.t = new flar2.exkernelmanager.a.a(this, new ArrayList());
    this.s.setAdapter(this.t);
    this.s.setOnItemClickListener(this);
    this.s.setOnItemLongClickListener(this);
    this.s.setOnScrollListener(new cq(this));
    this.r = this.o.a("/sys/devices/system/cpu/cpu4/cpufreq/scaling_governor");
    this.u = ("/sys/devices/system/cpu/cpu4/cpufreq/" + this.r + "/");
    k.a("prefC2GovPath", this.u);
    l();
    k();
  }
  
  public void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    switch (((o)this.t.getItem(paramInt)).b())
    {
    default: 
      return;
    case -7099: 
      b("prefC2GBoost", this.u + "gboost");
      return;
    case -7199: 
      b("prefC2Shortcut", this.u + "shortcut");
      return;
    case -7899: 
      c("prefC2SyncFreq", this.u + "sync_freq");
      return;
    case -7999: 
      c("prefC2OptFreq", this.u + "optimal_freq");
      return;
    case -7599: 
      if (this.o.a(this.u + "two_phase_freq").contains(","))
      {
        d("prefC2TwoPhaseFreq", this.u + "two_phase_freq");
        return;
      }
      c("prefC2TwoPhaseFreq", this.u + "two_phase_freq");
      return;
    case -7299: 
      if (this.o.a(this.u + "input_event_min_freq").contains(","))
      {
        d("prefC2InputMinFreq", this.u + "input_event_min_freq");
        return;
      }
      c("prefC2InputMinFreq", this.u + "input_event_min_freq");
      return;
    case -92699: 
      c("prefC2ActiveFloorFreq", this.u + "active_floor_freq");
      return;
    case -92399: 
      c("PREFC2_INPUT_MIN_FREQ2", this.u + "input_min_freq");
      return;
    case -92499: 
      c("PREFC2_GBOOST_MIN_FREQ", this.u + "gboost_min_freq");
      return;
    case -92599: 
      c("PREFC2_MAX_SCREEN_OFF_FREQ", this.u + "max_screen_off_freq");
      return;
    case -7399: 
      a("prefC2InputTimeout", this.u + "input_event_timeout");
      return;
    case -7499: 
      a("prefC2UISamplingRate", this.u + "ui_sampling_rate");
      return;
    case -7699: 
      a("prefC2UpThreshold", this.u + "up_threshold");
      return;
    case -7799: 
      a("prefC2DownDifferential", this.u + "down_differential");
      return;
    case -90099: 
      a("PREFC2_SAMPLING_RATE_MIN", this.u + "sampling_rate_min");
      return;
    case -90299: 
      a("PREFC2_FREQ_DOWN_STEP", this.u + "freq_down_step");
      return;
    case -90399: 
      a("PREFC2_SAMPLING_RATE", this.u + "sampling_rate");
      return;
    case -90499: 
      a("PREFC2_SAMPLING_DOWN_FACTOR", this.u + "sampling_down_factor");
      return;
    case -90699: 
      a("PREFC2_UP_THRESHOLD_ANY_CPU_LOAD", this.u + "up_threshold_any_cpu_load");
      return;
    case -90799: 
      a("PREFC2_TARGET_LOADS", this.u + "target_loads");
      return;
    case -90899: 
      a("PREFC2_TIMER_SLACK", this.u + "timer_slack");
      return;
    case -91199: 
      a("PREFC2_TIMER_RATE", this.u + "timer_rate");
      return;
    case -91299: 
      a("PREFC2_ABOVE_HISPEED_DELAY", this.u + "above_hispeed_delay");
      return;
    case -91399: 
      a("PREFC2_BOOSTPULSE", this.u + "boostpulse");
      return;
    case -91499: 
      a("PREFC2_BOOSTPULSE_DURATION", this.u + "boostpulse_duration");
      return;
    case -91599: 
      a("PREFC2_GO_HISPEED_LOAD", this.u + "go_hispeed_load");
      return;
    case -91799: 
      a("PREFC2_MIN_SAMPLE_TIME", this.u + "min_sample_time");
      return;
    case -92099: 
      a("PREFC2_DOWN_THRESHOLD", this.u + "down_threshold");
      return;
    case -92199: 
      a("PREFC2_FREQ_STEP", this.u + "freq_step");
      return;
    case -92299: 
      a("PREFC2_BOOST", this.u + "boost");
      return;
    case -91099: 
      c("PREFC2_HISPEED_FREQ", this.u + "hispeed_freq");
      return;
    case -91699: 
      c("PREFC2_INPUT_BOOST_FREQ", this.u + "input_boost_freq");
      return;
    case -91899: 
      c("PREFC2_UP_THRESHOLD_ANY_CPU_FREQ", this.u + "up_threshold_any_cpu_freq");
      return;
    case -90199: 
      c("PREFC2_FREQ_DOWN_STEP_BARRIAR", this.u + "freq_down_step_barriar");
      return;
    case -92799: 
      a("prefC2LowLoadDownThreshold", this.u + "low_load_down_threshold");
      return;
    case -92899: 
      a("prefC2MaxFreqHysteresis", this.u + "max_freq_hysteresis");
      return;
    }
    a("prefC2InputBoostDuration", this.u + "input_boost_duration");
  }
  
  public boolean onItemLongClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    switch (((o)this.t.getItem(paramInt)).b())
    {
    }
    for (;;)
    {
      return true;
      Toast.makeText(this, "This is how you change doubletap2wake ", 0).show();
    }
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(paramMenuItem);
    }
    ar.a(this);
    return true;
  }
  
  public void onResume()
  {
    super.onResume();
    k();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/GovernorC2Activity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */