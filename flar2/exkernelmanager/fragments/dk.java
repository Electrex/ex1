package flar2.exkernelmanager.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v7.app.ab;
import android.support.v7.app.ac;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import flar2.exkernelmanager.MainActivity;
import flar2.exkernelmanager.MainApp;
import flar2.exkernelmanager.a.o;
import flar2.exkernelmanager.r;
import flar2.exkernelmanager.utilities.e;
import flar2.exkernelmanager.utilities.f;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;
import java.util.ArrayList;
import java.util.List;

public class dk
  extends Fragment
  implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener
{
  private ListView a;
  private flar2.exkernelmanager.a.a b;
  private Context c;
  private m d = new m();
  private f e = new f();
  private e f = new e();
  private int g = 0;
  private int h = 0;
  private TextView i;
  private TextView j;
  private ImageView k;
  private int l;
  private int m;
  private RectF n = new RectF();
  private RectF o = new RectF();
  private AccelerateDecelerateInterpolator p;
  
  public static float a(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    return Math.max(paramFloat2, Math.min(paramFloat1, paramFloat3));
  }
  
  private RectF a(RectF paramRectF, View paramView)
  {
    paramRectF.set(paramView.getLeft(), paramView.getTop(), paramView.getRight(), paramView.getBottom());
    return paramRectF;
  }
  
  private void a(int paramInt)
  {
    MainActivity.o.setTranslationY(Math.max(-paramInt, this.m));
    MainActivity.p.setTranslationY(Math.max(-paramInt, this.m));
    float f1 = a(MainActivity.o.getTranslationY() / this.m, 0.0F, 1.0F);
    a(this.i, this.j, this.p.getInterpolation(f1));
    a(this.k, this.j, this.p.getInterpolation(f1));
    this.k.setAlpha(1.0F - f1 * 2.0F);
  }
  
  private void a(View paramView1, View paramView2, float paramFloat)
  {
    a(this.n, paramView1);
    a(this.o, paramView2);
    float f1 = 1.0F + paramFloat * (this.o.width() / this.n.width() - 1.0F);
    float f2 = 1.0F + paramFloat * (this.o.height() / this.n.height() - 1.0F);
    float f3 = 0.5F * (paramFloat * (this.o.left + this.o.right - this.n.left - this.n.right));
    float f4 = 0.5F * (paramFloat * (this.o.top + this.o.bottom - this.n.top - this.n.bottom));
    paramView1.setTranslationX(f3);
    paramView1.setTranslationY(f4 - MainActivity.o.getTranslationY());
    paramView1.setScaleX(f1);
    paramView1.setScaleY(f2);
  }
  
  private void a(String paramString1, String paramString2)
  {
    ac localac = new ac(getActivity());
    localac.a(this.c.getString(2131624091));
    localac.b(2131623999, null);
    EditText localEditText = new EditText(getActivity());
    localac.a(localEditText);
    localEditText.setHint(this.d.a(paramString2));
    localEditText.setInputType(2);
    localac.a(2131624170, new dm(this, localEditText, paramString1, paramString2));
    ab localab = localac.a();
    localab.getWindow().setSoftInputMode(5);
    localab.show();
  }
  
  private void b()
  {
    new ds(this, null).execute(new Void[0]);
  }
  
  private void b(String paramString1, String paramString2)
  {
    k.a(paramString1 + "Boot", false);
    if (this.d.a(paramString2).equals("0"))
    {
      this.d.a("1", paramString2);
      k.a(paramString1, "1");
    }
    for (;;)
    {
      b();
      return;
      if (this.d.a(paramString2).equals("1"))
      {
        this.d.a("0", paramString2);
        k.a(paramString1, "0");
      }
      else if (this.d.a(paramString2).equals("Y"))
      {
        this.d.a("N", paramString2);
        k.a(paramString1, "N");
      }
      else if (this.d.a(paramString2).equals("N"))
      {
        this.d.a("Y", paramString2);
        k.a(paramString1, "Y");
      }
    }
  }
  
  private void c()
  {
    this.e.c("chmod 666 " + r.k[this.h]);
  }
  
  private void c(String paramString1, String paramString2)
  {
    k.a(paramString1 + "Boot", false);
    if (this.d.a())
    {
      this.e.a("swapoff " + paramString2);
      this.e.a("swapoff /dev/block/zram1");
      this.e.a("swapoff /dev/block/zram2");
      this.e.a("swapoff /dev/block/zram3");
      k.a(paramString1, "swapoff ");
    }
    for (;;)
    {
      b();
      return;
      this.e.a("swapon " + paramString2);
      this.e.a("swapon /dev/block/zram1");
      this.e.a("swapon /dev/block/zram2");
      this.e.a("swapon /dev/block/zram3");
      k.a(paramString1, "swapon ");
    }
  }
  
  private void d()
  {
    startActivity(new Intent(getActivity(), BatteryActivity.class));
  }
  
  private void d(String paramString1, String paramString2)
  {
    k.a(paramString1 + "Boot", false);
    if (this.d.a(paramString2).equals("0"))
    {
      this.d.a("1", paramString2);
      k.a(paramString1, "1");
    }
    for (;;)
    {
      b();
      return;
      if (this.d.a(paramString2).equals("1"))
      {
        this.d.a("0", paramString2);
        k.a(paramString1, "0");
      }
    }
  }
  
  private void e()
  {
    ac localac = new ac(getActivity());
    localac.a(this.c.getString(2131624208));
    localac.b(2131623999, null);
    String[] arrayOfString = this.f.a("/sys/block/mmcblk0/queue/scheduler", 0, 1);
    localac.a(arrayOfString, new dn(this, arrayOfString));
    localac.a().show();
  }
  
  private void f()
  {
    ac localac = new ac(getActivity());
    localac.a(this.c.getString(2131624202));
    localac.b(2131623999, null);
    String[] arrayOfString = this.f.a("/sys/class/leds/charging/trigger", 0, 1);
    localac.a(arrayOfString, new do(this, arrayOfString));
    localac.a().show();
  }
  
  private void g()
  {
    ac localac = new ac(getActivity());
    localac.a(this.c.getString(2131624186));
    localac.b(2131623999, null);
    String[] arrayOfString = { "128", "256", "512", "1024", "2048" };
    localac.a(arrayOfString, new dp(this, arrayOfString));
    localac.a().show();
  }
  
  private void h()
  {
    ac localac = new ac(getActivity());
    localac.a(this.c.getString(2131624262));
    localac.b(2131623999, null);
    String[] arrayOfString = this.f.a("/proc/sys/net/ipv4/tcp_available_congestion_control", 0, 0);
    localac.a(arrayOfString, new dq(this, arrayOfString));
    localac.a().show();
  }
  
  private void i()
  {
    k.a("prefExFATBoot", false);
    if (this.f.e("lsmod").contains("exfat"))
    {
      this.e.a("rmmod exfat");
      k.a("prefExFAT", "0");
    }
    for (;;)
    {
      b();
      return;
      this.e.a("insmod /system/lib/modules/exfat.ko");
      k.a("prefExFAT", "1");
    }
  }
  
  private void j()
  {
    ac localac = new ac(getActivity());
    localac.a(this.c.getString(2131623982));
    localac.b(2131623999, null);
    localac.a(new String[] { "4200mV (~93%)", "4100mV (~83%)", "4000mV (~73%)", "4300 mV (Disabled)" }, new dr(this, new String[] { "4200", "4100", "4000", "4300" }));
    localac.a().show();
  }
  
  private List k()
  {
    ArrayList localArrayList = new ArrayList();
    if (getActivity().getResources().getConfiguration().orientation == 1)
    {
      o localo1 = new o();
      localo1.a(7);
      localArrayList.add(localo1);
    }
    o localo2 = new o();
    localo2.a(0);
    localo2.a(this.c.getString(2131624123));
    localArrayList.add(localo2);
    o localo3 = new o();
    localo3.a(1);
    localo3.b(-50);
    localo3.a(this.c.getString(2131624124));
    String str1 = this.d.a("/sys/block/mmcblk0/queue/scheduler");
    localo3.b(str1.substring(1 + str1.indexOf("["), str1.indexOf("]")).trim());
    localo3.d("prefSchedBoot");
    o localo4;
    label295:
    o localo5;
    label388:
    label422:
    o localo26;
    label552:
    label586:
    o localo7;
    label770:
    label804:
    o localo10;
    label982:
    label1016:
    o localo11;
    label1109:
    label1143:
    o localo12;
    label1236:
    label1270:
    o localo24;
    label1408:
    label1442:
    o localo25;
    label1537:
    o localo14;
    label1703:
    label1737:
    o localo15;
    label1845:
    o localo16;
    label1938:
    label1972:
    o localo17;
    label2065:
    label2099:
    o localo18;
    label2235:
    o localo19;
    label2328:
    label2362:
    o localo20;
    if (k.b("prefSchedBoot").booleanValue())
    {
      localo3.a(true);
      localo3.c(2130837611);
      localArrayList.add(localo3);
      localo4 = new o();
      localo4.a(1);
      localo4.b(-51);
      localo4.a(this.c.getString(2131624186));
      localo4.b(this.d.a("/sys/block/mmcblk0/queue/read_ahead_kb"));
      localo4.d("prefReadaheadBoot");
      if (!k.b("prefReadaheadBoot").booleanValue()) {
        break label2568;
      }
      localo4.a(true);
      localo4.c(2130837611);
      localArrayList.add(localo4);
      if (this.f.a("/sys/module/sync/parameters/fsync_enabled"))
      {
        localo5 = new o();
        localo5.a(1);
        localo5.b(-52);
        localo5.a(this.c.getString(2131624097));
        if (!this.d.a("/sys/module/sync/parameters/fsync_enabled").equals("N")) {
          break label2585;
        }
        localo5.b(this.c.getString(2131624058));
        localo5.d("prefFsyncBoot");
        if (!k.b("prefFsyncBoot").booleanValue()) {
          break label2603;
        }
        localo5.a(true);
        localo5.c(2130837611);
        localArrayList.add(localo5);
      }
      if ((this.f.e("ls /system/lib/modules/").contains("exfat.ko")) && (!k.a("prefDeviceName").equals(this.c.getString(2131624113))))
      {
        localo26 = new o();
        localo26.a(1);
        localo26.b(-53);
        localo26.a(this.c.getString(2131624093));
        if (!this.f.e("lsmod").contains("exfat")) {
          break label2620;
        }
        localo26.b(this.c.getString(2131624077));
        k.a("prefExFAT", "1");
        localo26.d("prefExFATBoot");
        if (!k.b("prefExFATBoot").booleanValue()) {
          break label2647;
        }
        localo26.a(true);
        localo26.c(2130837611);
        localArrayList.add(localo26);
      }
      if (this.f.a(r.k[this.h]))
      {
        o localo6 = new o();
        localo6.a(0);
        localo6.a(this.c.getString(2131624298));
        localArrayList.add(localo6);
        localo7 = new o();
        localo7.a(3);
        localo7.b(-54);
        if (this.h != 1) {
          break label2664;
        }
        int i1 = Integer.parseInt(this.d.a(r.k[this.h]).replaceAll("[^0-9]", ""));
        localo7.b((int)Math.ceil(100.0F * (i1 / 3100.0F)) + "%");
        localo7.e(1900);
        localo7.d(i1 - 1200);
        localo7.d("prefVibBoot");
        if (!k.b("prefVibBoot").booleanValue()) {
          break label2736;
        }
        localo7.a(true);
        localo7.c(2130837611);
        localArrayList.add(localo7);
      }
      o localo8 = new o();
      localo8.a(0);
      localo8.a(this.c.getString(2131624181));
      localArrayList.add(localo8);
      o localo9 = new o();
      localo9.a(2);
      localo9.b(-69);
      localo9.a(this.c.getString(2131623984));
      localArrayList.add(localo9);
      if (this.f.a("/sys/kernel/fast_charge/force_fast_charge"))
      {
        localo10 = new o();
        localo10.a(1);
        localo10.b(-55);
        localo10.a(this.c.getString(2131624297));
        if (!this.d.a("/sys/kernel/fast_charge/force_fast_charge").equals("0")) {
          break label2753;
        }
        localo10.b(this.c.getString(2131624058));
        localo10.d("prefFCBoot");
        if (!k.b("prefFCBoot").booleanValue()) {
          break label2771;
        }
        localo10.a(true);
        localo10.c(2130837611);
        localArrayList.add(localo10);
      }
      if (this.f.a("/sys/devices/i2c-0/0-006a/float_voltage"))
      {
        localo11 = new o();
        localo11.a(1);
        localo11.b(-56);
        localo11.a(this.c.getString(2131623983));
        if (!this.d.a("/sys/devices/i2c-0/0-006a/float_voltage").equals("4300")) {
          break label2788;
        }
        localo11.b(this.c.getString(2131624058));
        localo11.d("prefBLEBoot");
        if (!k.b("prefBLEBoot").booleanValue()) {
          break label2825;
        }
        localo11.a(true);
        localo11.c(2130837611);
        localArrayList.add(localo11);
      }
      if (this.f.a("/sys/module/msm_otg/parameters/usbhost_charge_mode"))
      {
        localo12 = new o();
        localo12.a(1);
        localo12.b(-57);
        localo12.a(this.c.getString(2131624171));
        if (!this.d.a("/sys/module/msm_otg/parameters/usbhost_charge_mode").equals("N")) {
          break label2842;
        }
        localo12.b(this.c.getString(2131624058));
        localo12.d("prefOTGCBoot");
        if (!k.b("prefOTGCBoot").booleanValue()) {
          break label2860;
        }
        localo12.a(true);
        localo12.c(2130837611);
        localArrayList.add(localo12);
      }
      if ((this.f.a("/sys/block/zram0")) && (!k.a("prefSubVersion").equals("HTC")))
      {
        o localo23 = new o();
        localo23.a(0);
        localo23.a(this.c.getString(2131624144));
        localArrayList.add(localo23);
        localo24 = new o();
        localo24.a(1);
        localo24.b(-62);
        localo24.a(this.c.getString(2131624311));
        if (!this.d.a()) {
          break label2877;
        }
        localo24.b(this.c.getString(2131624077));
        localo24.d("prefzRam0Boot");
        if (!k.b("prefzRam0Boot").booleanValue()) {
          break label2895;
        }
        localo24.a(true);
        localo24.c(2130837611);
        localArrayList.add(localo24);
        localo25 = new o();
        localo25.a(1);
        localo25.b(-64);
        localo25.a(this.c.getString(2131624249));
        localo25.b(this.d.a("/proc/sys/vm/swappiness"));
        localo25.d("prefSwappinessBoot");
        if (!k.b("prefSwappinessBoot").booleanValue()) {
          break label2912;
        }
        localo25.a(true);
        localo25.c(2130837611);
        localArrayList.add(localo25);
      }
      o localo13 = new o();
      localo13.a(0);
      localo13.a(this.c.getString(2131624149));
      localArrayList.add(localo13);
      if (this.f.a(r.l[this.g]))
      {
        localo14 = new o();
        localo14.a(1);
        localo14.b(-58);
        localo14.a(this.c.getString(2131624138));
        if ((!this.d.a(r.l[this.g]).equals("Y")) && (!this.d.a(r.l[this.g]).equals("0"))) {
          break label2929;
        }
        localo14.b(this.c.getString(2131624058));
        localo14.d("prefLidBoot");
        if (!k.b("prefLidBoot").booleanValue()) {
          break label2947;
        }
        localo14.a(true);
        localo14.c(2130837611);
        localArrayList.add(localo14);
      }
      if (this.f.a("/proc/sys/net/ipv4/tcp_congestion_control"))
      {
        localo15 = new o();
        localo15.a(1);
        localo15.b(-59);
        localo15.a(this.c.getString(2131624263));
        localo15.b(this.d.a("/proc/sys/net/ipv4/tcp_congestion_control"));
        localo15.d("prefTCPCongBoot");
        if (!k.b("prefTCPCongBoot").booleanValue()) {
          break label2964;
        }
        localo15.a(true);
        localo15.c(2130837611);
        localArrayList.add(localo15);
      }
      if (this.f.a("/sys/android_touch/logo2menu"))
      {
        localo16 = new o();
        localo16.a(1);
        localo16.b(-60);
        localo16.a(this.c.getString(2131624135));
        if (!this.d.a("/sys/android_touch/logo2menu").equals("0")) {
          break label2981;
        }
        localo16.b(this.c.getString(2131624058));
        localo16.d("prefL2MBoot");
        if (!k.b("prefL2MBoot").booleanValue()) {
          break label2999;
        }
        localo16.a(true);
        localo16.c(2130837611);
        localArrayList.add(localo16);
      }
      if (this.f.a("/sys/class/leds/button-backlight/blink_buttons"))
      {
        localo17 = new o();
        localo17.a(1);
        localo17.b(-61);
        localo17.a(this.c.getString(2131623992));
        if (!this.d.a("/sys/class/leds/button-backlight/blink_buttons").equals("1")) {
          break label3016;
        }
        localo17.b(this.c.getString(2131624077));
        localo17.d("prefBLNBoot");
        if (!k.b("prefBLNBoot").booleanValue()) {
          break label3034;
        }
        localo17.a(true);
        localo17.c(2130837611);
        localArrayList.add(localo17);
      }
      if (this.f.a("/sys/class/leds/charging/trigger"))
      {
        localo18 = new o();
        localo18.a(1);
        localo18.b(-65);
        localo18.a(this.c.getString(2131624002));
        String str2 = this.d.a("/sys/class/leds/charging/trigger");
        localo18.b(str2.substring(1 + str2.indexOf("["), str2.indexOf("]")).trim());
        localo18.d("prefChargeLEDBoot");
        if (!k.b("prefChargeLEDBoot").booleanValue()) {
          break label3051;
        }
        localo18.a(true);
        localo18.c(2130837611);
        localArrayList.add(localo18);
      }
      if (this.f.a("/sys/module/wakeup/parameters/enable_si_ws"))
      {
        localo19 = new o();
        localo19.a(1);
        localo19.b(-66);
        localo19.a(this.c.getString(2131624209));
        if (!this.d.a("/sys/module/wakeup/parameters/enable_si_ws").equals("Y")) {
          break label3068;
        }
        localo19.b(this.c.getString(2131624077));
        localo19.d("prefSensorIndBoot");
        if (!k.b("prefSensorIndBoot").booleanValue()) {
          break label3086;
        }
        localo19.a(true);
        localo19.c(2130837611);
        localArrayList.add(localo19);
      }
      if (this.f.a("/proc/sys/kernel/rr_interval"))
      {
        localo20 = new o();
        localo20.a(1);
        localo20.b(-68);
        localo20.a(this.c.getString(2131624148));
        localo20.b(this.d.a("/proc/sys/kernel/rr_interval"));
        localo20.d("prefBFSRRIntervalBoot");
        if (!k.b("prefSensorIndBoot").booleanValue()) {
          break label3103;
        }
        localo20.a(true);
        localo20.c(2130837611);
      }
    }
    for (;;)
    {
      localArrayList.add(localo20);
      if ((localArrayList.size() > 7) && (localArrayList.size() < 11))
      {
        o localo21 = new o();
        localo21.a(0);
        localArrayList.add(localo21);
        o localo22 = new o();
        localo22.a(0);
        localArrayList.add(localo22);
      }
      return localArrayList;
      localo3.a(false);
      localo3.c(2130837609);
      break;
      label2568:
      localo4.a(false);
      localo4.c(2130837609);
      break label295;
      label2585:
      localo5.b(this.c.getString(2131624077));
      break label388;
      label2603:
      localo5.a(false);
      localo5.c(2130837609);
      break label422;
      label2620:
      localo26.b(this.c.getString(2131624058));
      k.a("prefExFAT", "0");
      break label552;
      label2647:
      localo26.a(false);
      localo26.c(2130837609);
      break label586;
      label2664:
      localo7.b(this.d.a(r.k[this.h]) + "%");
      localo7.e(100);
      localo7.d(Integer.parseInt(this.d.a(r.k[this.h])));
      break label770;
      label2736:
      localo7.a(false);
      localo7.c(2130837609);
      break label804;
      label2753:
      localo10.b(this.c.getString(2131624077));
      break label982;
      label2771:
      localo10.a(false);
      localo10.c(2130837609);
      break label1016;
      label2788:
      localo11.b(this.d.a("/sys/devices/i2c-0/0-006a/float_voltage") + "mV");
      break label1109;
      label2825:
      localo11.a(false);
      localo11.c(2130837609);
      break label1143;
      label2842:
      localo12.b(this.c.getString(2131624077));
      break label1236;
      label2860:
      localo12.a(false);
      localo12.c(2130837609);
      break label1270;
      label2877:
      localo24.b(this.c.getString(2131624058));
      break label1408;
      label2895:
      localo24.a(false);
      localo24.c(2130837609);
      break label1442;
      label2912:
      localo25.a(false);
      localo25.c(2130837609);
      break label1537;
      label2929:
      localo14.b(this.c.getString(2131624077));
      break label1703;
      label2947:
      localo14.a(false);
      localo14.c(2130837609);
      break label1737;
      label2964:
      localo15.a(false);
      localo15.c(2130837609);
      break label1845;
      label2981:
      localo16.b(this.c.getString(2131624077));
      break label1938;
      label2999:
      localo16.a(false);
      localo16.c(2130837609);
      break label1972;
      label3016:
      localo17.b(this.c.getString(2131624058));
      break label2065;
      label3034:
      localo17.a(false);
      localo17.c(2130837609);
      break label2099;
      label3051:
      localo18.a(false);
      localo18.c(2130837609);
      break label2235;
      label3068:
      localo19.b(this.c.getString(2131624058));
      break label2328;
      label3086:
      localo19.a(false);
      localo19.c(2130837609);
      break label2362;
      label3103:
      localo20.a(false);
      localo20.c(2130837609);
    }
  }
  
  public int a()
  {
    View localView = this.a.getChildAt(0);
    if (localView == null) {
      return 0;
    }
    int i1 = this.a.getFirstVisiblePosition();
    int i2 = localView.getTop();
    int i3 = 0;
    if (i1 >= 1) {
      i3 = this.l;
    }
    return i3 + (-i2 + i1 * localView.getHeight());
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if (paramMenu != null)
    {
      paramMenu.removeItem(2131493247);
      paramMenu.removeItem(2131493246);
      paramMenu.removeItem(2131493236);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView = paramLayoutInflater.inflate(2130903090, paramViewGroup, false);
    this.c = MainApp.a();
    ((flar2.exkernelmanager.slidingmenu.a.a)flar2.exkernelmanager.slidingmenu.a.a.a.getAdapter()).f(flar2.exkernelmanager.slidingmenu.a.a.b.indexOf("Miscellaneous"));
    setHasOptionsMenu(true);
    String[] arrayOfString = getResources().getStringArray(2131230721);
    getActivity().setTitle(arrayOfString[6]);
    this.a = ((ListView)localView.findViewById(2131492989));
    this.b = new flar2.exkernelmanager.a.a(getActivity(), new ArrayList());
    this.a.setAdapter(this.b);
    this.a.setOnItemClickListener(this);
    this.a.setOnItemLongClickListener(this);
    flar2.exkernelmanager.a.a.b = true;
    if (getActivity().getResources().getConfiguration().orientation == 1)
    {
      MainActivity.o.getLayoutParams().height = getResources().getDimensionPixelSize(2131427421);
      MainActivity.p = getActivity().findViewById(2131493047);
      this.k = ((ImageView)getActivity().findViewById(2131493045));
      this.k.setImageResource(2130837626);
      this.j = ((TextView)getActivity().findViewById(2131493044));
      this.j.setText(arrayOfString[6]);
      this.i = ((TextView)getActivity().findViewById(2131493046));
      this.i.setText(arrayOfString[6]);
      this.l = getResources().getDimensionPixelSize(2131427421);
      this.m = (-this.l + this.e.b(getActivity()));
      this.p = new AccelerateDecelerateInterpolator();
    }
    for (;;)
    {
      this.a.setOnScrollListener(new dl(this));
      this.g = this.d.a(r.l);
      k.a("preflidPath", this.g);
      c();
      this.h = this.d.a(r.k);
      k.a("prefvibPath", this.h);
      b();
      if (k.a.getBoolean("prefFirstRunSettings", true)) {}
      return localView;
      MainActivity.o.getLayoutParams().height = new f().b(getActivity());
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    if (this.p != null) {
      a(0);
    }
  }
  
  public void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    switch (((o)this.b.getItem(paramInt)).b())
    {
    case -67: 
    case -63: 
    case -54: 
    default: 
      return;
    case -50: 
      e();
      return;
    case -51: 
      g();
      return;
    case -52: 
      b("prefFsync", "/sys/module/sync/parameters/fsync_enabled");
      return;
    case -53: 
      i();
      return;
    case -69: 
      d();
      return;
    case -55: 
      b("prefFC", "/sys/kernel/fast_charge/force_fast_charge");
      return;
    case -56: 
      j();
      return;
    case -57: 
      b("prefOTGC", "/sys/module/msm_otg/parameters/usbhost_charge_mode");
      return;
    case -58: 
      b("prefLid", r.l[k.c("preflidPath")]);
      return;
    case -59: 
      h();
      return;
    case -60: 
      d("prefL2M", "/sys/android_touch/logo2menu");
      return;
    case -61: 
      b("prefBLN", "/sys/class/leds/button-backlight/blink_buttons");
      return;
    case -65: 
      f();
      return;
    case -62: 
      c("prefzRam0", "/dev/block/zram0");
      return;
    case -64: 
      a("prefSwappiness", "/proc/sys/vm/swappiness");
      return;
    case -66: 
      b("prefSensorInd", "/sys/module/wakeup/parameters/enable_si_ws");
      return;
    }
    a("prefBFSRRInterval", "/proc/sys/kernel/rr_interval");
  }
  
  public boolean onItemLongClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    ((o)this.b.getItem(paramInt)).b();
    Toast.makeText(this.c, "No help for this item", 0).show();
    return true;
  }
  
  public void onPause()
  {
    super.onPause();
    flar2.exkernelmanager.a.a.b = false;
  }
  
  public void onResume()
  {
    super.onResume();
    b();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/dk.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */