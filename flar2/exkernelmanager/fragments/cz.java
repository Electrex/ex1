package flar2.exkernelmanager.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ab;
import android.support.v7.app.ac;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import flar2.exkernelmanager.MainActivity;
import flar2.exkernelmanager.MainApp;
import flar2.exkernelmanager.a.o;
import flar2.exkernelmanager.r;
import flar2.exkernelmanager.utilities.e;
import flar2.exkernelmanager.utilities.f;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class cz
  extends Fragment
  implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener
{
  private m a = new m();
  private e b = new e();
  private f c = new f();
  private Context d;
  private int e = 0;
  private int f = 0;
  private int g = 0;
  private int h = 0;
  private int i = 0;
  private int j = 0;
  private ListView k;
  private flar2.exkernelmanager.a.a l;
  private String m;
  private TextView n;
  private TextView o;
  private ImageView p;
  private int q;
  private int r;
  private RectF s = new RectF();
  private RectF t = new RectF();
  private AccelerateDecelerateInterpolator u;
  
  public static float a(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    return Math.max(paramFloat2, Math.min(paramFloat1, paramFloat3));
  }
  
  private RectF a(RectF paramRectF, View paramView)
  {
    paramRectF.set(paramView.getLeft(), paramView.getTop(), paramView.getRight(), paramView.getBottom());
    return paramRectF;
  }
  
  private void a(int paramInt)
  {
    MainActivity.o.setTranslationY(Math.max(-paramInt, this.r));
    MainActivity.p.setTranslationY(Math.max(-paramInt, this.r));
    float f1 = a(MainActivity.o.getTranslationY() / this.r, 0.0F, 1.0F);
    a(this.n, this.o, this.u.getInterpolation(f1));
    a(this.p, this.o, this.u.getInterpolation(f1));
    this.p.setAlpha(1.0F - f1 * 2.0F);
  }
  
  private void a(View paramView1, View paramView2, float paramFloat)
  {
    a(this.s, paramView1);
    a(this.t, paramView2);
    float f1 = 1.0F + paramFloat * (this.t.width() / this.s.width() - 1.0F);
    float f2 = 1.0F + paramFloat * (this.t.height() / this.s.height() - 1.0F);
    float f3 = 0.5F * (paramFloat * (this.t.left + this.t.right - this.s.left - this.s.right));
    float f4 = 0.5F * (paramFloat * (this.t.top + this.t.bottom - this.s.top - this.s.bottom));
    paramView1.setTranslationX(f3);
    paramView1.setTranslationY(f4 - MainActivity.o.getTranslationY());
    paramView1.setScaleX(f1);
    paramView1.setScaleY(f2);
  }
  
  private void a(String paramString)
  {
    ac localac = new ac(getActivity());
    localac.a(this.d.getString(2131624307));
    localac.b(this.d.getString(2131624060), null);
    localac.b(paramString);
    localac.a().show();
  }
  
  private void a(String paramString1, String paramString2)
  {
    k.a(paramString1 + "Boot", false);
    if (this.a.a(paramString2).equals("0"))
    {
      this.a.a("1", paramString2);
      k.a(paramString1, "1");
    }
    for (;;)
    {
      if ((paramString1.equals("prefCoolerColors")) && (k.a(paramString1).equals("1"))) {
        a(this.d.getString(2131624009));
      }
      d();
      return;
      if (this.a.a(paramString2).equals("1"))
      {
        this.a.a("0", paramString2);
        k.a(paramString1, "0");
      }
      else if (this.a.a(paramString2).equals("Y"))
      {
        this.a.a("N", paramString2);
        k.a(paramString1, "N");
      }
      else if (this.a.a(paramString2).equals("N"))
      {
        this.a.a("Y", paramString2);
        k.a(paramString1, "Y");
      }
    }
  }
  
  private void b()
  {
    this.j = this.a.a(r.q);
    k.a("prefkcalPath", this.j);
  }
  
  private void b(String paramString)
  {
    k.a("prefCCProfile", paramString);
    BufferedReader localBufferedReader = new BufferedReader(new FileReader(new File(Environment.getExternalStorageDirectory().getPath() + "/ElementalX/color_profiles/" + paramString)));
    ArrayList localArrayList = new ArrayList();
    for (;;)
    {
      String str = localBufferedReader.readLine();
      if (str == null) {
        break;
      }
      localArrayList.add(str);
    }
    String[] arrayOfString = (String[])localArrayList.toArray(new String[localArrayList.size()]);
    if (arrayOfString != null)
    {
      this.a.a(arrayOfString[0], "/sys/module/dsi_panel/kgamma_bn");
      this.a.a(arrayOfString[1], "/sys/module/dsi_panel/kgamma_bp");
      this.a.a(arrayOfString[2], "/sys/module/dsi_panel/kgamma_gn");
      this.a.a(arrayOfString[3], "/sys/module/dsi_panel/kgamma_gp");
      this.a.a(arrayOfString[4], "/sys/module/dsi_panel/kgamma_rn");
      this.a.a(arrayOfString[5], "/sys/module/dsi_panel/kgamma_rp");
      this.a.a(arrayOfString[6], "/sys/module/dsi_panel/kgamma_w");
    }
    Toast.makeText(this.d, this.d.getString(2131624200), 0).show();
    d();
  }
  
  private void b(String paramString1, String paramString2)
  {
    k.a(paramString1 + "Boot", false);
    if (this.m.equals(this.d.getString(2131624157))) {
      if (this.a.a(paramString2).equals("0"))
      {
        this.a.a("4", paramString2);
        k.a(paramString1, "4");
      }
    }
    for (;;)
    {
      Toast.makeText(this.d, this.d.getString(2131624200), 0).show();
      d();
      return;
      this.a.a("0", paramString2);
      k.a(paramString1, "0");
      continue;
      if (this.i == 3)
      {
        c(paramString1, paramString2);
      }
      else if (this.a.a(paramString2).equals("N"))
      {
        this.a.a("Y", paramString2);
        k.a(paramString1, "Y");
      }
      else
      {
        this.a.a("N", paramString2);
        k.a(paramString1, "N");
      }
    }
  }
  
  private void c()
  {
    this.c.c("chmod 666 " + r.e[this.e]);
    this.c.c("chmod 666 " + r.f[this.f]);
    this.c.c("chmod 666 " + r.h[this.h]);
    this.c.a("chmod 666 " + r.q[this.j]);
  }
  
  private void c(String paramString1, String paramString2)
  {
    ac localac = new ac(getActivity());
    localac.a(this.d.getString(2131623979));
    localac.b(2131623999, null);
    String[] arrayOfString = new String[3];
    arrayOfString[0] = this.d.getString(2131624059);
    arrayOfString[1] = this.d.getString(2131624056);
    arrayOfString[2] = this.d.getString(2131624057);
    localac.a(arrayOfString, new db(this, paramString2, paramString1));
    localac.a().show();
  }
  
  private void d()
  {
    new dj(this, null).execute(new Void[0]);
  }
  
  private void e()
  {
    startActivity(new Intent(getActivity(), ColorActivity.class));
  }
  
  private void f()
  {
    ac localac = new ac(getActivity());
    localac.a(this.d.getString(2131623980));
    localac.b(this.d.getString(2131623981));
    localac.b(2131623999, null);
    EditText localEditText = new EditText(getActivity());
    localac.a(localEditText);
    localEditText.setHint(this.a.a("/sys/module/lm3630_bl/parameters/min_brightness"));
    localEditText.setInputType(2);
    localac.a(2131624170, new dc(this, localEditText));
    ab localab = localac.a();
    localab.getWindow().setSoftInputMode(5);
    localab.show();
  }
  
  private void g()
  {
    ac localac = new ac(getActivity());
    localac.a(this.d.getString(2131624205));
    localac.b(2131623999, null);
    localac.a(this.b.a(r.g[this.e], 1, 0), new dd(this, this.b.a(r.g[this.e], 0, 0)));
    localac.a().show();
  }
  
  private void h()
  {
    ac localac = new ac(getActivity());
    localac.a(this.d.getString(2131624205));
    localac.b(2131623999, null);
    String[] arrayOfString1 = this.b.a(r.g[this.e], 1, 0);
    if (this.b.a("/sys/kernel/tegra_gpu/gpu_floor_rate"))
    {
      String[] arrayOfString2 = this.b.a(r.g[this.e], 0, 0);
      String[] arrayOfString3 = new String[5];
      arrayOfString3[0] = arrayOfString1[0];
      arrayOfString3[1] = arrayOfString1[1];
      arrayOfString3[2] = arrayOfString1[2];
      arrayOfString3[3] = arrayOfString1[3];
      arrayOfString3[4] = arrayOfString1[4];
      localac.a(arrayOfString3, new de(this, arrayOfString2));
    }
    for (;;)
    {
      localac.a().show();
      return;
      localac.a(arrayOfString1, new df(this, arrayOfString1));
    }
  }
  
  private void i()
  {
    ac localac = new ac(getActivity());
    localac.a(this.d.getString(2131624207));
    localac.b(2131623999, null);
    String[] arrayOfString;
    if (this.b.a("/sys/class/kgsl/kgsl-3d0/devfreq/available_governors")) {
      arrayOfString = this.b.a("/sys/class/kgsl/kgsl-3d0/devfreq/available_governors", 0, 0);
    }
    for (;;)
    {
      localac.a(arrayOfString, new dg(this, arrayOfString));
      localac.a().show();
      return;
      if ((System.getProperty("os.version").contains("ElementalX")) && ((this.m.equals(this.d.getString(2131624157))) || (this.m.equals(this.d.getString(2131624111))))) {
        arrayOfString = new String[] { "ondemand", "simple", "performance" };
      } else {
        arrayOfString = new String[] { "ondemand", "performance" };
      }
    }
  }
  
  private void j()
  {
    k.a("prefKCALModuleBoot", false);
    if (!this.b.e("lsmod").contains("kcal"))
    {
      new Thread(new dh(this)).start();
      if (!this.b.e("lsmod").contains("kcal")) {
        break label81;
      }
      k.a("prefKCALModule", "1");
    }
    for (;;)
    {
      b();
      d();
      return;
      label81:
      k.a("prefKCALModule", "0");
    }
  }
  
  private void k()
  {
    ac localac = new ac(getActivity());
    localac.a(this.d.getString(2131624203));
    localac.b(2131623999, null);
    String[] arrayOfString = new File(Environment.getExternalStorageDirectory().getPath() + "/ElementalX/color_profiles").list();
    localac.a(arrayOfString, new di(this, arrayOfString));
    localac.a().show();
    k.a("prefCCProfileBoot", false);
  }
  
  private List l()
  {
    ArrayList localArrayList = new ArrayList();
    if (getActivity().getResources().getConfiguration().orientation == 1)
    {
      o localo1 = new o();
      localo1.a(7);
      localArrayList.add(localo1);
    }
    o localo2 = new o();
    localo2.a(0);
    localo2.a(this.d.getString(2131624104));
    localArrayList.add(localo2);
    o localo3;
    o localo4;
    label325:
    label359:
    o localo5;
    if (this.b.a(r.e[this.e]))
    {
      localo3 = new o();
      localo3.a(1);
      localo3.b(-20);
      localo3.a(this.d.getString(2131624142));
      localo3.b(this.b.c(this.a.a(r.e[this.e])));
      localo3.d("prefGPUMaxBoot");
      if (k.b("prefGPUMaxBoot").booleanValue())
      {
        localo3.a(true);
        localo3.c(2130837611);
        localArrayList.add(localo3);
        if ((this.b.a(r.f[this.f])) || (this.b.a("/sys/kernel/tegra_gpu/gpu_floor_rate")))
        {
          localo4 = new o();
          localo4.a(1);
          localo4.b(-21);
          localo4.a(this.d.getString(2131624147));
          if (!this.m.equals(this.d.getString(2131624159))) {
            break label1694;
          }
          if (!this.a.a("/sys/kernel/tegra_gpu/gpu_floor_state").equals("0")) {
            break label1669;
          }
          localo4.b("72 MHz");
          localo4.d("prefGPUMinBoot");
          if (!k.b("prefGPUMinBoot").booleanValue()) {
            break label2070;
          }
          localo4.a(true);
          localo4.c(2130837611);
          localArrayList.add(localo4);
        }
        if (this.b.a(r.h[this.h]))
        {
          localo5 = new o();
          localo5.a(1);
          localo5.b(-22);
          localo5.a(this.d.getString(2131624103));
          localo5.b(this.a.a(r.h[this.h]));
          localo5.d("prefGPUGovBoot");
          if (!k.b("prefGPUGovBoot").booleanValue()) {
            break label2087;
          }
          localo5.a(true);
          localo5.c(2130837611);
          label477:
          localArrayList.add(localo5);
        }
      }
    }
    for (int i1 = 0;; i1 = 1)
    {
      if ((this.b.a(r.i[this.i])) || (this.b.a("/sys/module/mdss_dsi/parameters/color_preset")) || (this.b.a("/sys/devices/platform/msm_fb.590593/leds/lcd-backlight/color_enhance")))
      {
        o localo6 = new o();
        localo6.a(0);
        localo6.a(this.d.getString(2131624201));
        localArrayList.add(localo6);
      }
      o localo7;
      label689:
      label723:
      o localo17;
      label859:
      o localo8;
      label954:
      label988:
      o localo9;
      label1098:
      label1132:
      o localo11;
      label1313:
      label1347:
      o localo13;
      if (this.b.a(r.i[this.i]))
      {
        localo7 = new o();
        localo7.a(1);
        localo7.b(-24);
        localo7.a(this.d.getString(2131623977));
        if ((this.a.a(r.i[this.i]).equals("0")) || (this.a.a(r.i[this.i]).equals("N")))
        {
          localo7.b(this.d.getString(2131624058));
          localo7.d("prefBacklightBoot");
          if (!k.b("prefBacklightBoot").booleanValue()) {
            break label2214;
          }
          localo7.a(true);
          localo7.c(2130837611);
          localArrayList.add(localo7);
          i1 = 0;
        }
      }
      else
      {
        if ((this.b.a("/sys/module/lm3630_bl/parameters/min_brightness")) && (this.a.a(r.i[this.i]).equals("Y")))
        {
          localo17 = new o();
          localo17.a(1);
          localo17.b(65295);
          localo17.a(this.d.getString(2131623978));
          localo17.b(this.a.a("/sys/module/lm3630_bl/parameters/min_brightness"));
          localo17.d("prefBacklightMinBoot");
          if (!k.b("prefBacklightMinBoot").booleanValue()) {
            break label2231;
          }
          localo17.a(true);
          localo17.c(2130837611);
          localArrayList.add(localo17);
          i1 = 0;
        }
        if (this.b.a("/sys/module/mdss_dsi/parameters/color_preset"))
        {
          localo8 = new o();
          localo8.a(1);
          localo8.b(-25);
          localo8.a(this.d.getString(2131624008));
          if (!this.a.a("/sys/module/mdss_dsi/parameters/color_preset").equals("0")) {
            break label2248;
          }
          localo8.b(this.d.getString(2131624058));
          localo8.d("prefCoolerColorsBoot");
          if (!k.b("prefCoolerColorsBoot").booleanValue()) {
            break label2266;
          }
          localo8.a(true);
          localo8.c(2130837611);
          localArrayList.add(localo8);
          i1 = 0;
        }
        if (this.b.a("/sys/devices/platform/msm_fb.590593/leds/lcd-backlight/color_enhance"))
        {
          localo9 = new o();
          localo9.a(1);
          localo9.b(-26);
          localo9.a(this.d.getString(2131624110));
          localo9.b(this.a.a("/sys/devices/platform/msm_fb.590593/leds/lcd-backlight/color_enhance"));
          if (!this.a.a("/sys/devices/platform/msm_fb.590593/leds/lcd-backlight/color_enhance").equals("0")) {
            break label2283;
          }
          localo9.b(this.d.getString(2131624058));
          localo9.d("prefHTCColorBoot");
          if (!k.b("prefHTCColorBoot").booleanValue()) {
            break label2301;
          }
          localo9.a(true);
          localo9.c(2130837611);
          localArrayList.add(localo9);
          i1 = 0;
        }
        if ((this.b.a("/system/lib/modules/msm_kcal_ctrl.ko")) || (this.b.a(r.q[this.j])))
        {
          o localo10 = new o();
          localo10.a(0);
          localo10.a(this.d.getString(2131624006));
          localArrayList.add(localo10);
        }
        if (this.b.e("ls /system/lib/modules/").contains("kcal"))
        {
          localo11 = new o();
          localo11.a(1);
          localo11.b(-27);
          localo11.a(this.d.getString(2131624007));
          if (!this.b.e("lsmod").contains("msm_kcal")) {
            break label2318;
          }
          localo11.b(this.d.getString(2131624077));
          k.a("prefKCALModule", "1");
          localo11.d("prefKCALModuleBoot");
          if (!k.b("prefKCALModuleBoot").booleanValue()) {
            break label2344;
          }
          localo11.a(true);
          localo11.c(2130837611);
          localArrayList.add(localo11);
        }
        if (this.b.a(r.q[this.j]))
        {
          o localo12 = new o();
          localo12.a(2);
          localo12.b(65324);
          localo12.a(this.d.getString(2131623966));
          localArrayList.add(localo12);
        }
        if (this.m.equals(this.d.getString(2131624153)))
        {
          localo13 = new o();
          localo13.a(1);
          localo13.b(65325);
          localo13.a(this.d.getString(2131624132));
          localo13.b(k.a("prefCCProfile"));
          localo13.d("prefCCProfileBoot");
          if (!k.b("prefCCProfileBoot").booleanValue()) {
            break label2361;
          }
          localo13.a(true);
          localo13.c(2130837611);
        }
      }
      for (;;)
      {
        localArrayList.add(localo13);
        i1 = 0;
        if (i1 != 0)
        {
          o localo14 = new o();
          localo14.a(0);
          localo14.a(this.d.getString(2131624166));
          localArrayList.add(localo14);
        }
        if ((localArrayList.size() > 7) && (localArrayList.size() < 11))
        {
          o localo15 = new o();
          localo15.a(0);
          localArrayList.add(localo15);
          o localo16 = new o();
          localo16.a(0);
          localArrayList.add(localo16);
        }
        return localArrayList;
        localo3.a(false);
        localo3.c(2130837609);
        break;
        label1669:
        localo4.b(this.b.c(this.a.a("/sys/kernel/tegra_gpu/gpu_floor_rate")));
        break label325;
        label1694:
        if (this.m.equals(this.d.getString(2131624111)))
        {
          String[] arrayOfString2 = this.b.a(r.g[this.e], 1, 0);
          if (arrayOfString2[0].contains("585"))
          {
            if (this.a.a(r.f[this.f]).equals("0"))
            {
              localo4.b(this.b.c(this.a.a(r.e[this.e])));
              break label325;
            }
            localo4.b(arrayOfString2[(7 + Integer.parseInt(this.a.a(r.f[this.f])))]);
            break label325;
          }
          localo4.b(arrayOfString2[Integer.parseInt(this.a.a(r.f[this.f]))]);
          break label325;
        }
        if (this.m.equals(this.d.getString(2131624157)))
        {
          String[] arrayOfString1 = this.b.a(r.g[this.e], 1, 0);
          if (arrayOfString1[0].contains("585"))
          {
            if (this.a.a(r.f[this.f]).equals("0"))
            {
              localo4.b(this.b.c(this.a.a(r.e[this.e])));
              break label325;
            }
            localo4.b(arrayOfString1[(6 + Integer.parseInt(this.a.a(r.f[this.f])))]);
            break label325;
          }
          localo4.b(arrayOfString1[Integer.parseInt(this.a.a(r.f[this.f]))]);
          break label325;
        }
        localo4.b(this.b.a(r.g[this.e], 1, 0)[Integer.parseInt(this.a.a(r.f[this.f]))]);
        break label325;
        label2070:
        localo4.a(false);
        localo4.c(2130837609);
        break label359;
        label2087:
        localo5.a(false);
        localo5.c(2130837609);
        break label477;
        if (this.i == 3)
        {
          if (this.a.a(r.i[this.i]).equals("1"))
          {
            localo7.b(this.d.getString(2131624056));
            break label689;
          }
          if (!this.a.a(r.i[this.i]).equals("2")) {
            break label689;
          }
          localo7.b(this.d.getString(2131624057));
          break label689;
        }
        localo7.b(this.d.getString(2131624077));
        break label689;
        label2214:
        localo7.a(false);
        localo7.c(2130837609);
        break label723;
        label2231:
        localo17.a(false);
        localo17.c(2130837609);
        break label859;
        label2248:
        localo8.b(this.d.getString(2131624077));
        break label954;
        label2266:
        localo8.a(false);
        localo8.c(2130837609);
        break label988;
        label2283:
        localo9.b(this.d.getString(2131624077));
        break label1098;
        label2301:
        localo9.a(false);
        localo9.c(2130837609);
        break label1132;
        label2318:
        localo11.b(this.d.getString(2131624058));
        k.a("prefKCALModule", "0");
        break label1313;
        label2344:
        localo11.a(false);
        localo11.c(2130837609);
        break label1347;
        label2361:
        localo13.a(false);
        localo13.c(2130837609);
      }
    }
  }
  
  public int a()
  {
    View localView = this.k.getChildAt(0);
    if (localView == null) {
      return 0;
    }
    int i1 = this.k.getFirstVisiblePosition();
    int i2 = localView.getTop();
    int i3 = 0;
    if (i1 >= 1) {
      i3 = this.q;
    }
    return i3 + (-i2 + i1 * localView.getHeight());
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if (paramMenu != null)
    {
      paramMenu.removeItem(2131493247);
      paramMenu.removeItem(2131493246);
      paramMenu.removeItem(2131493236);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView = paramLayoutInflater.inflate(2130903090, paramViewGroup, false);
    this.d = MainApp.a();
    ((flar2.exkernelmanager.slidingmenu.a.a)flar2.exkernelmanager.slidingmenu.a.a.a.getAdapter()).f(flar2.exkernelmanager.slidingmenu.a.a.b.indexOf("Graphics"));
    setHasOptionsMenu(true);
    this.m = k.a("prefDeviceName");
    String[] arrayOfString = getResources().getStringArray(2131230721);
    getActivity().setTitle(arrayOfString[2]);
    this.k = ((ListView)localView.findViewById(2131492989));
    this.l = new flar2.exkernelmanager.a.a(getActivity(), new ArrayList());
    this.k.setAdapter(this.l);
    this.k.setOnItemClickListener(this);
    this.k.setOnItemLongClickListener(this);
    this.k.setItemsCanFocus(true);
    flar2.exkernelmanager.a.a.b = true;
    if (getActivity().getResources().getConfiguration().orientation == 1)
    {
      MainActivity.o.getLayoutParams().height = getResources().getDimensionPixelSize(2131427421);
      MainActivity.p = getActivity().findViewById(2131493047);
      this.p = ((ImageView)getActivity().findViewById(2131493045));
      this.p.setImageResource(2130837621);
      this.o = ((TextView)getActivity().findViewById(2131493044));
      this.o.setText(arrayOfString[2]);
      this.n = ((TextView)getActivity().findViewById(2131493046));
      this.n.setText(arrayOfString[2]);
      this.q = getResources().getDimensionPixelSize(2131427421);
      this.r = (-this.q + this.c.b(getActivity()));
      this.u = new AccelerateDecelerateInterpolator();
    }
    for (;;)
    {
      this.k.setOnScrollListener(new da(this));
      if (this.m.equals(this.d.getString(2131624153))) {
        this.c.c(getActivity());
      }
      this.e = this.a.a(r.e);
      if (this.b.a(r.e[this.e]))
      {
        this.f = this.a.a(r.f);
        this.g = this.a.a(r.g);
        this.h = this.a.a(r.h);
      }
      k.a("prefgpuMaxPath", this.e);
      k.a("prefgpuMinPath", this.f);
      k.a("prefgpuAvailPath", this.g);
      k.a("prefgpuGovPath", this.h);
      this.i = this.a.a(r.i);
      k.a("prefbacklightPath", this.i);
      b();
      c();
      d();
      if (k.a.getBoolean("prefFirstRunSettings", true)) {}
      return localView;
      MainActivity.o.getLayoutParams().height = new f().b(getActivity());
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    if (this.u != null) {
      a(0);
    }
  }
  
  public void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    switch (((o)this.l.getItem(paramInt)).b())
    {
    case -210: 
    case -29: 
    case -28: 
    case -23: 
    default: 
      return;
    case -20: 
      g();
      return;
    case -21: 
      h();
      return;
    case -22: 
      i();
      return;
    case -24: 
      b("prefBacklight", r.i[k.c("prefbacklightPath")]);
      return;
    case -241: 
      f();
      return;
    case -25: 
      a("prefCoolerColors", "/sys/module/mdss_dsi/parameters/color_preset");
      return;
    case -26: 
      a("prefHTCColor", "/sys/devices/platform/msm_fb.590593/leds/lcd-backlight/color_enhance");
      return;
    case -27: 
      j();
      return;
    case -212: 
      e();
      return;
    }
    k();
  }
  
  public boolean onItemLongClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    switch (((o)this.l.getItem(paramInt)).b())
    {
    default: 
      Toast.makeText(this.d, "No help for this item", 0).show();
    }
    for (;;)
    {
      return true;
      Toast.makeText(this.d, "This is how you change doubletap2wake ", 0).show();
    }
  }
  
  public void onPause()
  {
    super.onPause();
    flar2.exkernelmanager.a.a.b = false;
  }
  
  public void onResume()
  {
    super.onResume();
    d();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/cz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */