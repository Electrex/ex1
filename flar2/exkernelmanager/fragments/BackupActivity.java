package flar2.exkernelmanager.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ab;
import android.support.v7.app.ac;
import android.support.v7.app.ad;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.am;
import android.support.v7.widget.cl;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import com.getbase.floatingactionbutton.FloatingActionButton;
import flar2.exkernelmanager.utilities.h;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class BackupActivity
  extends ad
  implements flar2.exkernelmanager.b.c
{
  flar2.exkernelmanager.b.a n;
  String o;
  h p;
  private List q = new ArrayList();
  private RecyclerView r;
  private cl s;
  private boolean t = false;
  
  private String c(String paramString)
  {
    File localFile = new File(Environment.getExternalStorageDirectory().getPath() + "/ElementalX/kernel_backups/" + paramString);
    return new SimpleDateFormat("MMM d hh:mm a").format(Long.valueOf(localFile.lastModified()));
  }
  
  private void d(String paramString)
  {
    setRequestedOrientation(14);
    new i(this, null).execute(new String[] { paramString });
  }
  
  private void k()
  {
    TextView localTextView = (TextView)findViewById(2131492955);
    if (this.n.a() == 0)
    {
      localTextView.setText("No backups");
      return;
    }
    if (this.n.a() == 1)
    {
      localTextView.setText("You have " + this.n.a() + " backup");
      return;
    }
    localTextView.setText("You have " + this.n.a() + " backups");
  }
  
  private void l()
  {
    ac localac = new ac(this);
    localac.a("Backup name");
    localac.a(false);
    localac.b(2131623999, null);
    EditText localEditText = new EditText(this);
    String str1 = System.getProperty("os.version");
    localEditText.setHint(str1);
    localEditText.setMaxLines(1);
    localEditText.setInputType(1);
    localac.a(localEditText);
    File localFile = new File(Environment.getExternalStorageDirectory().getPath() + "/ElementalX/kernel_backups/");
    if (!localFile.exists()) {
      localFile.mkdir();
    }
    String str2 = flar2.exkernelmanager.r.b[new flar2.exkernelmanager.utilities.m().a(flar2.exkernelmanager.r.b)];
    if (str2 == null) {
      localac.b("Device not supported");
    }
    for (;;)
    {
      ab localab = localac.a();
      localab.getWindow().setSoftInputMode(5);
      localab.show();
      return;
      localac.a(2131624170, new c(this, localEditText, str1, str2));
    }
  }
  
  private void m()
  {
    flar2.exkernelmanager.b.f localf = new flar2.exkernelmanager.b.f(this.o, c(this.o));
    this.n.a(0, localf);
    k();
  }
  
  private void n()
  {
    String[] arrayOfString = new File(Environment.getExternalStorageDirectory().getPath() + "/ElementalX/kernel_backups").list();
    this.q.clear();
    if (arrayOfString == null) {}
    for (;;)
    {
      k();
      return;
      int i = arrayOfString.length;
      for (int j = 0; j < i; j++)
      {
        String str = arrayOfString[j];
        flar2.exkernelmanager.b.f localf = new flar2.exkernelmanager.b.f(str, c(str));
        this.q.add(localf);
        this.n.a(this.q);
      }
    }
  }
  
  private void o()
  {
    this.t = false;
    ac localac = new ac(this);
    localac.a(false).a(getString(2131624187)).b("").b(getString(2131623999), new f(this));
    ab localab = localac.a();
    localab.show();
    new g(this, 3000L, 1000L, localab).start();
  }
  
  private void p()
  {
    try
    {
      new flar2.exkernelmanager.utilities.f().f(this);
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }
  
  public void a()
  {
    k();
  }
  
  public void a(String paramString)
  {
    b(paramString);
  }
  
  public void b(String paramString)
  {
    ac localac = new ac(this);
    localac.a(false).a("Restore backup and reboot?").b("This will restore kernel from backup and reboot your device.").b(getString(2131623999), new e(this)).a("RESTORE", new d(this, paramString));
    localac.a().show();
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    this.p.b().onTouchEvent(paramMotionEvent);
    return super.dispatchTouchEvent(paramMotionEvent);
  }
  
  public void onBackPressed()
  {
    super.onBackPressed();
    overridePendingTransition(0, 2130968604);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903066);
    overridePendingTransition(2130968605, 17432577);
    a((Toolbar)findViewById(2131492951));
    g().a(true);
    setTitle("Kernel Backup");
    this.p = new a(this, this);
    findViewById(2131492953).setOnTouchListener(this.p);
    this.r = ((RecyclerView)findViewById(2131492956));
    this.r.setHasFixedSize(true);
    this.s = new LinearLayoutManager(this);
    this.r.setLayoutManager(this.s);
    this.r.setItemAnimator(new am());
    this.n = new flar2.exkernelmanager.b.a();
    this.r.setAdapter(this.n);
    this.n.a(this);
    ((FloatingActionButton)findViewById(2131492957)).setOnClickListener(new b(this));
  }
  
  public void onResume()
  {
    super.onResume();
    n();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/BackupActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */