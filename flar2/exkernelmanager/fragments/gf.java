package flar2.exkernelmanager.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.RectF;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v7.app.ab;
import android.support.v7.app.ac;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import flar2.exkernelmanager.MainActivity;
import flar2.exkernelmanager.MainApp;
import flar2.exkernelmanager.a.o;
import flar2.exkernelmanager.r;
import flar2.exkernelmanager.utilities.e;
import flar2.exkernelmanager.utilities.f;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;
import java.util.ArrayList;
import java.util.List;

public class gf
  extends Fragment
  implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener
{
  private Context a;
  private m b = new m();
  private e c = new e();
  private f d = new f();
  private int e = 0;
  private int f = 0;
  private String g;
  private String h;
  private String i;
  private String j;
  private ListView k;
  private flar2.exkernelmanager.a.a l;
  private TextView m;
  private TextView n;
  private ImageView o;
  private int p;
  private int q;
  private RectF r = new RectF();
  private RectF s = new RectF();
  private AccelerateDecelerateInterpolator t;
  
  public static float a(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    return Math.max(paramFloat2, Math.min(paramFloat1, paramFloat3));
  }
  
  private RectF a(RectF paramRectF, View paramView)
  {
    paramRectF.set(paramView.getLeft(), paramView.getTop(), paramView.getRight(), paramView.getBottom());
    return paramRectF;
  }
  
  private void a(int paramInt)
  {
    MainActivity.o.setTranslationY(Math.max(-paramInt, this.q));
    MainActivity.p.setTranslationY(Math.max(-paramInt, this.q));
    float f1 = a(MainActivity.o.getTranslationY() / this.q, 0.0F, 1.0F);
    a(this.m, this.n, this.t.getInterpolation(f1));
    a(this.o, this.n, this.t.getInterpolation(f1));
    this.o.setAlpha(1.0F - f1 * 2.0F);
  }
  
  private void a(View paramView1, View paramView2, float paramFloat)
  {
    a(this.r, paramView1);
    a(this.s, paramView2);
    float f1 = 1.0F + paramFloat * (this.s.width() / this.r.width() - 1.0F);
    float f2 = 1.0F + paramFloat * (this.s.height() / this.r.height() - 1.0F);
    float f3 = 0.5F * (paramFloat * (this.s.left + this.s.right - this.r.left - this.r.right));
    float f4 = 0.5F * (paramFloat * (this.s.top + this.s.bottom - this.r.top - this.r.bottom));
    paramView1.setTranslationX(f3);
    paramView1.setTranslationY(f4 - MainActivity.o.getTranslationY());
    paramView1.setScaleX(f1);
    paramView1.setScaleY(f2);
  }
  
  private void a(String paramString1, String paramString2)
  {
    k.a(paramString1 + "Boot", false);
    if (this.b.a(paramString2).equals("0"))
    {
      this.b.a("1", paramString2);
      k.a(paramString1, "1");
    }
    for (;;)
    {
      c();
      return;
      if (this.b.a(paramString2).equals("1"))
      {
        this.b.a("0", paramString2);
        k.a(paramString1, "0");
      }
      else if (this.b.a(paramString2).equals("Y"))
      {
        this.b.a("N", paramString2);
        k.a(paramString1, "N");
      }
      else if (this.b.a(paramString2).equals("N"))
      {
        this.b.a("Y", paramString2);
        k.a(paramString1, "Y");
      }
      else if ((this.b.a(paramString2).equals("OFF")) || (this.b.a(paramString2).equals("ON")))
      {
        this.b.a("AUTO", paramString2);
        k.a(paramString1, "AUTO");
      }
      else if (this.b.a(paramString2).equals("AUTO"))
      {
        this.b.a("OFF", paramString2);
        k.a(paramString1, "OFF");
      }
    }
  }
  
  private void b()
  {
    this.e = this.b.a(r.m);
    this.f = this.b.a(r.n);
    k.a("prefs2wPath", this.e);
    k.a("prefdt2wPath", this.f);
    this.g = r.m[this.e];
    this.h = r.n[this.f];
    this.i = r.o[this.e];
  }
  
  private void c()
  {
    new gq(this, null).execute(new Void[0]);
  }
  
  private void d()
  {
    ac localac = new ac(getActivity());
    localac.a(this.a.getString(2131624063));
    localac.b(2131623999, null);
    String[] arrayOfString = new String[3];
    arrayOfString[0] = this.a.getString(2131624058);
    arrayOfString[1] = this.a.getString(2131624080);
    arrayOfString[2] = this.a.getString(2131624081);
    localac.a(arrayOfString, new gi(this));
    localac.a().show();
  }
  
  private void e()
  {
    ac localac = new ac(getActivity());
    localac.a(this.a.getString(2131624254));
    localac.b(2131623999, null);
    CharSequence[] arrayOfCharSequence = new CharSequence[4];
    arrayOfCharSequence[0] = this.a.getString(2131624258);
    arrayOfCharSequence[1] = this.a.getString(2131624257);
    arrayOfCharSequence[2] = this.a.getString(2131624259);
    arrayOfCharSequence[3] = this.a.getString(2131624256);
    boolean[] arrayOfBoolean = { 0, 0, 0, 0 };
    int i1 = Integer.parseInt(this.b.a(this.g));
    if ((i1 & 0x1) == 1) {
      arrayOfBoolean[0] = true;
    }
    if ((i1 & 0x2) == 2) {
      arrayOfBoolean[1] = true;
    }
    if ((i1 & 0x4) == 4) {
      arrayOfBoolean[2] = true;
    }
    if ((i1 & 0x8) == 8) {
      arrayOfBoolean[3] = true;
    }
    localac.a(arrayOfCharSequence, arrayOfBoolean, new gj(this, arrayOfBoolean));
    localac.a(2131624170, new gk(this, arrayOfBoolean));
    localac.a().show();
  }
  
  private void f()
  {
    ac localac = new ac(getActivity());
    localac.a(this.a.getString(2131624250));
    localac.b(2131623999, null);
    String[] arrayOfString;
    if (this.j.equals(this.a.getString(2131624159)))
    {
      arrayOfString = new String[4];
      arrayOfString[0] = this.a.getString(2131624058);
      arrayOfString[1] = this.a.getString(2131624179);
      arrayOfString[2] = this.a.getString(2131624125);
      arrayOfString[3] = this.a.getString(2131624178);
    }
    for (;;)
    {
      localac.a(arrayOfString, new gl(this));
      localac.a().show();
      return;
      arrayOfString = new String[4];
      arrayOfString[0] = this.a.getString(2131624058);
      arrayOfString[1] = this.a.getString(2131624258);
      arrayOfString[2] = this.a.getString(2131624257);
      arrayOfString[3] = this.a.getString(2131624255);
    }
  }
  
  private void g()
  {
    ac localac = new ac(getActivity());
    localac.a(this.a.getString(2131624062));
    localac.b(2131623999, null);
    String[] arrayOfString = new String[4];
    arrayOfString[0] = this.a.getString(2131624058);
    arrayOfString[1] = this.a.getString(2131624073);
    arrayOfString[2] = this.a.getString(2131624074);
    arrayOfString[3] = this.a.getString(2131624075);
    localac.a(arrayOfString, new gm(this));
    localac.a().show();
  }
  
  private void h()
  {
    ac localac = new ac(getActivity());
    localac.a(this.a.getString(2131624063));
    localac.b(2131623999, null);
    String[] arrayOfString = new String[4];
    arrayOfString[0] = this.a.getString(2131624058);
    arrayOfString[1] = this.a.getString(2131624073);
    arrayOfString[2] = this.a.getString(2131624074);
    arrayOfString[3] = this.a.getString(2131624075);
    localac.a(arrayOfString, new gn(this));
    localac.a().show();
  }
  
  private void i()
  {
    ac localac = new ac(getActivity());
    localac.a(this.a.getString(2131624254));
    localac.b(2131623999, null);
    String[] arrayOfString = new String[3];
    arrayOfString[0] = this.a.getString(2131624058);
    arrayOfString[1] = this.a.getString(2131624260);
    arrayOfString[2] = this.a.getString(2131624251);
    localac.a(arrayOfString, new go(this));
    localac.a().show();
  }
  
  private void j()
  {
    ac localac = new ac(getActivity());
    localac.a(this.a.getString(2131624252));
    localac.b(2131623999, null);
    String[] arrayOfString = new String[3];
    arrayOfString[0] = this.a.getString(2131624178);
    arrayOfString[1] = this.a.getString(2131624179);
    arrayOfString[2] = this.a.getString(2131624125);
    localac.a(arrayOfString, new gp(this));
    localac.a().show();
  }
  
  private void k()
  {
    ac localac = new ac(getActivity());
    localac.a(this.a.getString(2131624306));
    localac.b(this.a.getString(2131624305));
    localac.b(2131623999, null);
    EditText localEditText = new EditText(getActivity());
    localac.a(localEditText);
    localEditText.setHint(this.b.a("/sys/android_touch/wake_timeout"));
    localEditText.setInputType(2);
    localac.a(2131624170, new gh(this, localEditText));
    ab localab = localac.a();
    localab.getWindow().setSoftInputMode(5);
    localab.show();
  }
  
  private List l()
  {
    ArrayList localArrayList1 = new ArrayList();
    if (getActivity().getResources().getConfiguration().orientation == 1)
    {
      o localo1 = new o();
      localo1.a(7);
      localArrayList1.add(localo1);
    }
    o localo2 = new o();
    localo2.a(0);
    localo2.a(this.a.getString(2131624303));
    localArrayList1.add(localo2);
    o localo24;
    label231:
    o localo25;
    label344:
    label378:
    o localo26;
    label472:
    label506:
    o localo27;
    label601:
    label635:
    int i1;
    if (this.c.a("/sys/android_touch/wake_gestures")) {
      if ((this.j.equals(this.a.getString(2131624113))) || (Build.VERSION.SDK_INT < 21))
      {
        localo24 = new o();
        localo24.a(1);
        localo24.b(-30);
        localo24.a(this.a.getString(2131624302));
        if (this.b.a("/sys/android_touch/wake_gestures").equals("0"))
        {
          localo24.b(this.a.getString(2131624058));
          localo24.d("prefWGBoot");
          if (!k.b("prefWGBoot").booleanValue()) {
            break label2139;
          }
          localo24.a(true);
          localo24.c(2130837611);
          localArrayList1.add(localo24);
        }
      }
      else
      {
        if (this.c.a(this.h))
        {
          localo25 = new o();
          localo25.a(1);
          localo25.b(-37);
          localo25.a(this.a.getString(2131624063));
          if ((!this.b.a(this.h).equals("0")) && (!this.b.a(this.h).equals("N"))) {
            break label2156;
          }
          localo25.b(this.a.getString(2131624058));
          localo25.d("prefDT2WBoot");
          if (!k.b("prefDT2WBoot").booleanValue()) {
            break label2288;
          }
          localo25.a(true);
          localo25.c(2130837611);
          localArrayList1.add(localo25);
        }
        if (this.c.a(this.g))
        {
          localo26 = new o();
          localo26.a(1);
          localo26.b(-36);
          localo26.a(this.a.getString(2131624254));
          if (!this.b.a(this.g).equals("0")) {
            break label2305;
          }
          localo26.b(this.a.getString(2131624058));
          localo26.d("prefS2WBoot");
          if (!k.b("prefS2WBoot").booleanValue()) {
            break label2464;
          }
          localo26.a(true);
          localo26.c(2130837611);
          localArrayList1.add(localo26);
        }
        localo27 = new o();
        localo27.a(1);
        localo27.b(-35);
        localo27.a(this.a.getString(2131624250));
        localo27.b(this.b.a("/sys/android_touch/sweep2sleep"));
        if (!this.b.a("/sys/android_touch/sweep2sleep").equals("3")) {
          break label2481;
        }
        localo27.b(this.a.getString(2131624079));
        localo27.d("prefS2SBoot");
        if (!k.b("prefS2SBoot").booleanValue()) {
          break label2610;
        }
        localo27.a(true);
        localo27.c(2130837611);
        localArrayList1.add(localo27);
        i1 = 0;
      }
    }
    label730:
    label764:
    label858:
    label892:
    label987:
    label1021:
    label1206:
    label1301:
    label1335:
    label1430:
    label1464:
    label1559:
    label1593:
    label1701:
    label1735:
    label1830:
    label1864:
    label1959:
    label1993:
    label2139:
    label2156:
    label2288:
    label2305:
    label2464:
    label2481:
    label2610:
    label2732:
    label2766:
    label2863:
    label3005:
    label3077:
    label3219:
    label3253:
    label3368:
    label3402:
    label3540:
    label3748:
    label3765:
    label3783:
    label3950:
    label4038:
    label4105:
    label4140:
    label4242:
    label4309:
    label4418:
    label4435:
    label4453:
    label4470:
    label4488:
    label4505:
    label4522:
    label4577:
    label4594:
    label4612:
    label4629:
    label4673:
    label4690:
    label4744:
    label4761:
    label4779:
    label4796:
    label4814:
    label4837:
    label4841:
    for (;;)
    {
      o localo4;
      o localo5;
      o localo6;
      o localo8;
      o localo9;
      o localo10;
      o localo11;
      o localo12;
      o localo13;
      o localo14;
      o localo22;
      if (this.c.a("/sys/android_key/doubletap2sleep"))
      {
        localo4 = new o();
        localo4.a(1);
        localo4.b(-38);
        localo4.a(this.a.getString(2131624062));
        if (this.b.a("/sys/android_key/doubletap2sleep").equals("1"))
        {
          localo4.b(this.a.getString(2131624078));
          localo4.d("prefDT2SBoot");
          if (!k.b("prefDT2SBoot").booleanValue()) {
            break label4418;
          }
          localo4.a(true);
          localo4.c(2130837611);
          localArrayList1.add(localo4);
        }
      }
      else
      {
        if (this.c.a(this.i))
        {
          localo5 = new o();
          localo5.a(1);
          localo5.b(-39);
          localo5.a(this.a.getString(2131624177));
          if (!this.b.a(this.i).equals("0")) {
            break label4435;
          }
          localo5.b(this.a.getString(2131624058));
          localo5.d("prefPDBoot");
          if (!k.b("prefPDBoot").booleanValue()) {
            break label4453;
          }
          localo5.a(true);
          localo5.c(2130837611);
          localArrayList1.add(localo5);
          i1 = 0;
        }
        if (this.c.a("/sys/android_touch/camera_gesture"))
        {
          localo6 = new o();
          localo6.a(1);
          localo6.b(-40);
          localo6.a(this.a.getString(2131623998));
          if (!this.b.a("/sys/android_touch/camera_gesture").equals("0")) {
            break label4470;
          }
          localo6.b(this.a.getString(2131624058));
          localo6.d("prefWGCamBoot");
          if (!k.b("prefWGCamBoot").booleanValue()) {
            break label4488;
          }
          localo6.a(true);
          localo6.c(2130837611);
          localArrayList1.add(localo6);
          i1 = 0;
        }
        if (this.c.a("/sys/android_touch/vib_strength"))
        {
          o localo7 = new o();
          localo7.a(0);
          localo7.a(this.a.getString(2131624100));
          localArrayList1.add(localo7);
          localo8 = new o();
          localo8.a(3);
          localo8.b(-41);
          int i2 = Integer.parseInt(this.b.a("/sys/android_touch/vib_strength"));
          localo8.b((int)Math.ceil(1.6666666F * i2) + "%");
          localo8.e(60);
          localo8.d(i2);
          localo8.d("prefWGVibBoot");
          if (!k.b("prefWGVibBoot").booleanValue()) {
            break label4505;
          }
          localo8.a(true);
          localo8.c(2130837611);
          localArrayList1.add(localo8);
          i1 = 0;
        }
        if (this.c.a("/sys/android_touch/orientation"))
        {
          localo9 = new o();
          localo9.a(1);
          localo9.b(-42);
          localo9.a(this.a.getString(2131624253));
          if (!this.b.a("/sys/android_touch/orientation").equals("1")) {
            break label4522;
          }
          localo9.b(this.a.getString(2131624196));
          localo9.d("prefOrientBoot");
          if (!k.b("prefOrientBoot").booleanValue()) {
            break label4577;
          }
          localo9.a(true);
          localo9.c(2130837611);
          localArrayList1.add(localo9);
          i1 = 0;
        }
        if (this.c.a("/sys/android_touch/shortsweep"))
        {
          localo10 = new o();
          localo10.a(1);
          localo10.b(-43);
          localo10.a(this.a.getString(2131624244));
          if (!this.b.a("/sys/android_touch/shortsweep").equals("0")) {
            break label4594;
          }
          localo10.b(this.a.getString(2131624058));
          localo10.d("prefShortSweepBoot");
          if (!k.b("prefShortSweepBoot").booleanValue()) {
            break label4612;
          }
          localo10.a(true);
          localo10.c(2130837611);
          localArrayList1.add(localo10);
          i1 = 0;
        }
        if (this.c.a("/sys/android_touch/wake_timeout"))
        {
          localo11 = new o();
          localo11.a(1);
          localo11.b(-44);
          localo11.a(this.a.getString(2131624301));
          if (!this.b.a("/sys/android_touch/wake_timeout").equals("0")) {
            break label4629;
          }
          localo11.b(this.a.getString(2131624058));
          localo11.d("prefWTOBoot");
          if (!k.b("prefWTOBoot").booleanValue()) {
            break label4673;
          }
          localo11.a(true);
          localo11.c(2130837611);
          localArrayList1.add(localo11);
          i1 = 0;
        }
        if ((this.c.a("/sys/module/qpnp_power_on/parameters/pwrkey_suspend")) || (this.c.a("/sys/android_touch/pwrkey_suspend")))
        {
          localo12 = new o();
          localo12.a(1);
          localo12.b(-45);
          localo12.a(this.a.getString(2131624185));
          if (!this.b.a("/sys/module/qpnp_power_on/parameters/pwrkey_suspend").equals("N")) {
            break label4690;
          }
          localo12.b(this.a.getString(2131624058));
          localo12.d("prefPwrKeySuspendBoot");
          if (!k.b("prefPwrKeySuspendBoot").booleanValue()) {
            break label4744;
          }
          localo12.a(true);
          localo12.c(2130837611);
          localArrayList1.add(localo12);
          i1 = 0;
        }
        if (this.c.a("/sys/android_touch/lid_suspend"))
        {
          localo13 = new o();
          localo13.a(1);
          localo13.b(-46);
          localo13.a(this.a.getString(2131624139));
          if (!this.b.a("/sys/android_touch/lid_suspend").equals("0")) {
            break label4761;
          }
          localo13.b(this.a.getString(2131624058));
          localo13.d("prefWGLidBoot");
          if (!k.b("prefWGLidBoot").booleanValue()) {
            break label4779;
          }
          localo13.a(true);
          localo13.c(2130837611);
          localArrayList1.add(localo13);
          i1 = 0;
        }
        if (this.c.a("/sys/android_touch/logo2wake"))
        {
          localo14 = new o();
          localo14.a(1);
          localo14.b(-47);
          localo14.a(this.a.getString(2131624136));
          if (!this.b.a("/sys/android_touch/logo2wake").equals("0")) {
            break label4796;
          }
          localo14.b(this.a.getString(2131624058));
          localo14.d("prefL2WBoot");
          if (!k.b("prefL2WBoot").booleanValue()) {
            break label4814;
          }
          localo14.a(true);
          localo14.c(2130837611);
          localArrayList1.add(localo14);
          i1 = 0;
        }
        if (i1 != 0)
        {
          o localo15 = new o();
          localo15.a(1);
          localo15.a(this.a.getString(2131624164));
          localArrayList1.add(localo15);
        }
        if ((localArrayList1.size() > 7) && (localArrayList1.size() < 11))
        {
          o localo16 = new o();
          localo16.a(0);
          localArrayList1.add(localo16);
          o localo17 = new o();
          localo17.a(0);
          localArrayList1.add(localo17);
        }
        return localArrayList1;
        localo24.b(this.a.getString(2131624077));
        break;
        localo24.a(false);
        localo24.c(2130837609);
        break label231;
        if ((this.b.a(this.h).equals("1")) || (this.b.a(this.h).equals("Y")))
        {
          if ((this.j.equals(this.a.getString(2131624153))) || (this.j.equals(this.a.getString(2131624111))))
          {
            localo25.b(this.a.getString(2131624080));
            break label344;
          }
          localo25.b(this.a.getString(2131624077));
          break label344;
        }
        localo25.b(this.a.getString(2131624081));
        break label344;
        localo25.a(false);
        localo25.c(2130837609);
        break label378;
        int i4 = Integer.parseInt(this.b.a(this.g));
        ArrayList localArrayList3 = new ArrayList();
        if ((i4 & 0x1) == 1) {
          localArrayList3.add(this.a.getString(2131624194));
        }
        if ((i4 & 0x2) == 2) {
          localArrayList3.add(this.a.getString(2131624127));
        }
        if ((i4 & 0x4) == 4) {
          localArrayList3.add(this.a.getString(2131624293));
        }
        if ((i4 & 0x8) == 8) {
          localArrayList3.add(this.a.getString(2131624064));
        }
        String str2 = localArrayList3.toString();
        localo26.b(str2.substring(1, -1 + str2.length()));
        break label472;
        localo26.a(false);
        localo26.c(2130837609);
        break label506;
        if (this.b.a("/sys/android_touch/sweep2sleep").equals("2"))
        {
          localo27.b(this.a.getString(2131624084));
          break label601;
        }
        if (this.b.a("/sys/android_touch/sweep2sleep").equals("1"))
        {
          if (this.j.equals(this.a.getString(2131624157)))
          {
            localo27.b(this.a.getString(2131624077));
            break label601;
          }
          localo27.b(this.a.getString(2131624088));
          break label601;
        }
        localo27.b(this.a.getString(2131624058));
        break label601;
        localo27.a(false);
        localo27.c(2130837609);
        break label635;
        if (this.j.equals(this.a.getString(2131624116)))
        {
          if (!this.c.a(this.h)) {
            break label4837;
          }
          localo22 = new o();
          localo22.a(1);
          localo22.b(-37);
          localo22.a(this.a.getString(2131624063));
          if (this.b.a(this.h).equals("0"))
          {
            localo22.b(this.a.getString(2131624058));
            localo22.d("prefDT2WBoot");
            if (!k.b("prefDT2WBoot").booleanValue()) {
              break label3005;
            }
            localo22.a(true);
            localo22.c(2130837611);
            localArrayList1.add(localo22);
          }
        }
      }
      for (i1 = 0;; i1 = 1)
      {
        if (!this.c.a(this.g)) {
          break label4841;
        }
        o localo23 = new o();
        localo23.a(1);
        localo23.b(-36);
        localo23.a(this.a.getString(2131624254));
        if (this.b.a(this.g).equals("0"))
        {
          localo23.b(this.a.getString(2131624058));
          localo23.d("prefS2WBoot");
          if (!k.b("prefS2WBoot").booleanValue()) {
            break label3077;
          }
          localo23.a(true);
          localo23.c(2130837611);
        }
        for (;;)
        {
          localArrayList1.add(localo23);
          i1 = 0;
          break;
          if (this.b.a(this.h).equals("1"))
          {
            localo22.b(this.a.getString(2131624078));
            break label2732;
          }
          if (this.b.a(this.h).equals("2"))
          {
            localo22.b(this.a.getString(2131624082));
            break label2732;
          }
          localo22.b(this.a.getString(2131624087));
          break label2732;
          localo22.a(false);
          localo22.c(2130837609);
          break label2766;
          if (this.b.a(this.g).equals("1"))
          {
            localo23.b(this.a.getString(2131624090));
            break label2863;
          }
          localo23.b(this.a.getString(2131624089));
          break label2863;
          localo23.a(false);
          localo23.c(2130837609);
        }
        o localo19;
        o localo20;
        if (this.c.a(this.g)) {
          if ((this.j.equals(this.a.getString(2131624159))) || (this.j.equals(this.a.getString(2131624155))))
          {
            localo19 = new o();
            localo19.a(1);
            localo19.b(-36);
            localo19.a(this.a.getString(2131624254));
            if (this.b.a(this.g).equals("0"))
            {
              localo19.b(this.a.getString(2131624058));
              localo19.d("prefS2WBoot");
              if (!k.b("prefS2WBoot").booleanValue()) {
                break label3748;
              }
              localo19.a(true);
              localo19.c(2130837611);
              localArrayList1.add(localo19);
              localo20 = new o();
              localo20.a(1);
              localo20.b(-35);
              localo20.a(this.a.getString(2131624250));
              localo20.b(this.b.a("/sys/android_touch/sweep2sleep"));
              if (!this.b.a("/sys/android_touch/sweep2sleep").equals("3")) {
                break label3783;
              }
              if (!this.j.equals(this.a.getString(2131624159))) {
                break label3765;
              }
              localo20.b(this.a.getString(2131624086));
              localo20.d("prefS2SBoot");
              if (!k.b("prefS2SBoot").booleanValue()) {
                break label3950;
              }
              localo20.a(true);
              localo20.c(2130837611);
              localArrayList1.add(localo20);
              i1 = 0;
            }
          }
        }
        for (;;)
        {
          if ((this.c.a(this.h)) && (this.f == 3) && (!this.c.a(this.g)))
          {
            o localo18 = new o();
            localo18.a(1);
            localo18.b(-37);
            localo18.a(this.a.getString(2131624063));
            if ((this.b.a(this.h).equals("OFF")) || (this.b.a(this.h).equals("ON")))
            {
              localo18.b(this.a.getString(2131624058));
              localo18.d("prefDT2WBoot");
              if (!k.b("prefDT2WBoot").booleanValue()) {
                break label4140;
              }
              localo18.a(true);
              localo18.c(2130837611);
            }
            for (;;)
            {
              localArrayList1.add(localo18);
              i1 = 0;
              break;
              int i3 = Integer.parseInt(this.b.a(this.g));
              ArrayList localArrayList2 = new ArrayList();
              if ((i3 & 0x1) == 1) {
                localArrayList2.add(this.a.getString(2131624194));
              }
              if ((i3 & 0x2) == 2) {
                localArrayList2.add(this.a.getString(2131624127));
              }
              if ((i3 & 0x4) == 4) {
                localArrayList2.add(this.a.getString(2131624293));
              }
              if ((i3 & 0x8) == 8) {
                localArrayList2.add(this.a.getString(2131624064));
              }
              String str1 = localArrayList2.toString();
              localo19.b(str1.substring(1, -1 + str1.length()));
              break label3219;
              localo19.a(false);
              localo19.c(2130837609);
              break label3253;
              localo20.b(this.a.getString(2131624079));
              break label3368;
              if (this.b.a("/sys/android_touch/sweep2sleep").equals("2"))
              {
                if (this.j.equals(this.a.getString(2131624159)))
                {
                  localo20.b(this.a.getString(2131624083));
                  break label3368;
                }
                localo20.b(this.a.getString(2131624084));
                break label3368;
              }
              if (this.b.a("/sys/android_touch/sweep2sleep").equals("1"))
              {
                if (this.j.equals(this.a.getString(2131624159)))
                {
                  localo20.b(this.a.getString(2131624085));
                  break label3368;
                }
                localo20.b(this.a.getString(2131624088));
                break label3368;
              }
              localo20.b(this.a.getString(2131624058));
              break label3368;
              localo20.a(false);
              localo20.c(2130837609);
              break label3402;
              o localo21 = new o();
              localo21.a(1);
              localo21.b(-36);
              localo21.a(this.a.getString(2131624254));
              if (this.b.a(this.g).equals("0"))
              {
                localo21.b(this.a.getString(2131624058));
                localo21.d("prefS2WBoot");
                if (!k.b("prefS2WBoot").booleanValue()) {
                  break label4105;
                }
                localo21.a(true);
                localo21.c(2130837611);
              }
              for (;;)
              {
                localArrayList1.add(localo21);
                i1 = 0;
                break;
                localo21.b(this.a.getString(2131624077));
                break label4038;
                localo21.a(false);
                localo21.c(2130837609);
              }
              localo18.b(this.a.getString(2131624077));
              break label3540;
              localo18.a(false);
              localo18.c(2130837609);
            }
          }
          if (!this.c.a(this.h)) {
            break;
          }
          o localo3 = new o();
          localo3.a(1);
          localo3.b(-37);
          localo3.a(this.a.getString(2131624063));
          if (this.b.a(this.h).equals("0"))
          {
            localo3.b(this.a.getString(2131624058));
            localo3.d("prefDT2WBoot");
            if (!k.b("prefDT2WBoot").booleanValue()) {
              break label4309;
            }
            localo3.a(true);
            localo3.c(2130837611);
          }
          for (;;)
          {
            localArrayList1.add(localo3);
            i1 = 0;
            break;
            localo3.b(this.a.getString(2131624077));
            break label4242;
            localo3.a(false);
            localo3.c(2130837609);
          }
          if (this.b.a("/sys/android_key/doubletap2sleep").equals("2"))
          {
            localo4.b(this.a.getString(2131624082));
            break label730;
          }
          if (this.b.a("/sys/android_key/doubletap2sleep").equals("3"))
          {
            localo4.b(this.a.getString(2131624087));
            break label730;
          }
          localo4.b(this.a.getString(2131624058));
          break label730;
          localo4.a(false);
          localo4.c(2130837609);
          break label764;
          localo5.b(this.a.getString(2131624077));
          break label858;
          localo5.a(false);
          localo5.c(2130837609);
          break label892;
          localo6.b(this.a.getString(2131624077));
          break label987;
          localo6.a(false);
          localo6.c(2130837609);
          break label1021;
          localo8.a(false);
          localo8.c(2130837609);
          break label1206;
          if (this.b.a("/sys/android_touch/orientation").equals("2"))
          {
            localo9.b(this.a.getString(2131624197));
            break label1301;
          }
          localo9.b(this.a.getString(2131624198));
          break label1301;
          localo9.a(false);
          localo9.c(2130837609);
          break label1335;
          localo10.b(this.a.getString(2131624077));
          break label1430;
          localo10.a(false);
          localo10.c(2130837609);
          break label1464;
          localo11.b(this.b.a("/sys/android_touch/wake_timeout") + this.a.getString(2131624304));
          break label1559;
          localo11.a(false);
          localo11.c(2130837609);
          break label1593;
          if (this.b.a("/sys/android_touch/pwrkey_suspend").equals("0"))
          {
            localo12.b(this.a.getString(2131624058));
            break label1701;
          }
          localo12.b(this.a.getString(2131624077));
          break label1701;
          localo12.a(false);
          localo12.c(2130837609);
          break label1735;
          localo13.b(this.a.getString(2131624077));
          break label1830;
          localo13.a(false);
          localo13.c(2130837609);
          break label1864;
          localo14.b(this.a.getString(2131624077));
          break label1959;
          localo14.a(false);
          localo14.c(2130837609);
          break label1993;
          i1 = 1;
        }
      }
    }
  }
  
  public int a()
  {
    View localView = this.k.getChildAt(0);
    if (localView == null) {
      return 0;
    }
    int i1 = this.k.getFirstVisiblePosition();
    int i2 = localView.getTop();
    int i3 = 0;
    if (i1 >= 1) {
      i3 = this.p;
    }
    return i3 + (-i2 + i1 * localView.getHeight());
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if (paramMenu != null)
    {
      paramMenu.removeItem(2131493247);
      paramMenu.removeItem(2131493246);
      paramMenu.removeItem(2131493236);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView = paramLayoutInflater.inflate(2130903090, paramViewGroup, false);
    this.a = MainApp.a();
    ((flar2.exkernelmanager.slidingmenu.a.a)flar2.exkernelmanager.slidingmenu.a.a.a.getAdapter()).f(flar2.exkernelmanager.slidingmenu.a.a.b.indexOf("Wake"));
    setHasOptionsMenu(true);
    String[] arrayOfString = getResources().getStringArray(2131230721);
    getActivity().setTitle(arrayOfString[3]);
    this.j = k.a("prefDeviceName");
    this.k = ((ListView)localView.findViewById(2131492989));
    this.l = new flar2.exkernelmanager.a.a(getActivity(), new ArrayList());
    this.k.setAdapter(this.l);
    this.k.setOnItemClickListener(this);
    this.k.setOnItemLongClickListener(this);
    flar2.exkernelmanager.a.a.b = true;
    if (getActivity().getResources().getConfiguration().orientation == 1)
    {
      MainActivity.o.getLayoutParams().height = getResources().getDimensionPixelSize(2131427421);
      MainActivity.p = getActivity().findViewById(2131493047);
      this.o = ((ImageView)getActivity().findViewById(2131493045));
      this.o.setImageResource(2130837648);
      this.n = ((TextView)getActivity().findViewById(2131493044));
      this.n.setText(arrayOfString[3]);
      this.m = ((TextView)getActivity().findViewById(2131493046));
      this.m.setText(arrayOfString[3]);
      this.p = getResources().getDimensionPixelSize(2131427421);
      this.q = (-this.p + this.d.b(getActivity()));
      this.t = new AccelerateDecelerateInterpolator();
    }
    for (;;)
    {
      this.k.setOnScrollListener(new gg(this));
      b();
      c();
      if (k.a.getBoolean("prefFirstRunSettings", true)) {}
      return localView;
      MainActivity.o.getLayoutParams().height = new f().b(getActivity());
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    if (this.t != null) {
      a(0);
    }
  }
  
  public void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    switch (((o)this.l.getItem(paramInt)).b())
    {
    case -34: 
    case -33: 
    case -32: 
    case -31: 
    default: 
      return;
    case -30: 
      a("prefWG", "/sys/android_touch/wake_gestures");
      return;
    case -35: 
      if (this.j.equals(this.a.getString(2131624157)))
      {
        a("prefS2S", "/sys/android_touch/sweep2sleep");
        return;
      }
      f();
      return;
    case -36: 
      if ((this.c.a("/sys/android_touch/wake_gestures")) || (this.j.equals(this.a.getString(2131624159))) || (this.j.equals(this.a.getString(2131624155))))
      {
        e();
        return;
      }
      if (this.j.equals(this.a.getString(2131624116)))
      {
        i();
        return;
      }
      a("prefS2W", r.m[this.e]);
      return;
    case -37: 
      if ((this.c.a("/sys/android_touch/wake_gestures")) && ((this.j.equals(this.a.getString(2131624153))) || (this.j.equals(this.a.getString(2131624111)))))
      {
        d();
        return;
      }
      if (this.j.equals(this.a.getString(2131624116)))
      {
        h();
        return;
      }
      a("prefDT2W", this.h);
      return;
    case -38: 
      g();
      return;
    case -39: 
      a("prefPD", r.o[k.c("prefs2wPath")]);
      return;
    case -40: 
      a("prefWGCam", "/sys/android_touch/camera_gesture");
      return;
    case -41: 
      Toast.makeText(this.a, this.a.getString(2131624167), 0).show();
      return;
    case -42: 
      j();
      return;
    case -43: 
      a("prefShortSweep", "/sys/android_touch/shortsweep");
      return;
    case -44: 
      k();
      return;
    case -45: 
      a("prefPwrKeySuspend", "/sys/module/qpnp_power_on/parameters/pwrkey_suspend");
      a("prefPwrKeySuspend", "/sys/android_touch/pwrkey_suspend");
      return;
    case -46: 
      a("prefWGLid", "/sys/android_touch/lid_suspend");
      return;
    }
    a("prefL2W", "/sys/android_touch/logo2wake");
  }
  
  public boolean onItemLongClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    switch (((o)this.l.getItem(paramInt)).b())
    {
    default: 
      Toast.makeText(this.a, "No help for this item", 0).show();
    }
    for (;;)
    {
      return true;
      Toast.makeText(this.a, "This is how you change doubletap2wake ", 0).show();
    }
  }
  
  public void onPause()
  {
    super.onPause();
    flar2.exkernelmanager.a.a.b = false;
  }
  
  public void onResume()
  {
    super.onResume();
    c();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/gf.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */