package flar2.exkernelmanager.fragments;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.DownloadManager.Query;
import android.app.DownloadManager.Request;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ab;
import android.support.v7.app.ac;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import com.getbase.floatingactionbutton.FloatingActionButton;
import flar2.exkernelmanager.MainActivity;
import flar2.exkernelmanager.MainApp;
import flar2.exkernelmanager.utilities.d;
import flar2.exkernelmanager.utilities.f;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.l;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class ey
  extends Fragment
{
  d a = new d();
  MenuItem b;
  MenuItem c;
  MenuItem d;
  MenuItem e;
  MenuItem f;
  MenuItem g;
  f h = new f();
  private Context i;
  private Button j;
  private View k;
  private int l = 0;
  private String m = null;
  private String n = null;
  private RadioButton o;
  private RadioButton p;
  private boolean q = true;
  private BroadcastReceiver r;
  private long s;
  private DownloadManager t;
  private boolean u = false;
  private SwipeRefreshLayout v;
  private TextView w;
  private ImageView x;
  
  private void a(View paramView)
  {
    if (this.o.isChecked())
    {
      h("AOSP");
      this.a.b();
      if (new f().a(getActivity()))
      {
        this.v.setRefreshing(true);
        this.v.postDelayed(new fw(this), 500L);
      }
    }
    if (this.p.isChecked())
    {
      h("CAF");
      this.a.b();
      if (new f().a(getActivity()))
      {
        this.v.setRefreshing(true);
        this.v.postDelayed(new fx(this), 500L);
      }
    }
  }
  
  private void b()
  {
    Animation localAnimation = AnimationUtils.loadAnimation(this.i, 2130968588);
    this.k.startAnimation(localAnimation);
  }
  
  private void b(View paramView)
  {
    switch (this.l)
    {
    case 0: 
    default: 
      return;
    case 1: 
      Toast.makeText(getActivity(), getString(2131624071), 1).show();
      return;
    }
    if ((!k.b("prefHTC").booleanValue()) && (!k.a("prefDeviceName").equals(this.i.getString(2131624155))) && (!k.a("prefDeviceName").equals(this.i.getString(2131624159))) && ((q().contains("franco")) || (q().contains("faux")) || (q().contains("leanKernel")) || (q().contains("ricked")) || (q().contains("UBER")) || (q().contains("haos")) || (q().contains("furnace")) || (q().contains("Code_Blue"))))
    {
      f();
      return;
    }
    g();
  }
  
  private void c()
  {
    ac localac = new ac(getActivity());
    localac.a(getString(2131623969)).b(getString(2131623968)).a(false).b(getString(2131624005), new fa(this));
    localac.a().show();
  }
  
  public static String d(String paramString)
  {
    try
    {
      String str = URLEncoder.encode(paramString, "UTF-8");
      return str;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      throw new RuntimeException("URLEncoder.encode() failed for " + paramString);
    }
  }
  
  private void d()
  {
    if ((this.j != null) && (isAdded())) {
      this.j.setText(getString(2131623994));
    }
    fy localfy = new fy(this, null);
    String[] arrayOfString = new String[1];
    arrayOfString[0] = (k.a("prefCheckVersion") + "latest");
    localfy.execute(arrayOfString);
  }
  
  private void e()
  {
    fz localfz = new fz(this, null);
    String[] arrayOfString = new String[1];
    arrayOfString[0] = k.a("prefCheckChangelog");
    localfz.execute(arrayOfString);
  }
  
  private void e(String paramString)
  {
    File localFile1 = new File(Environment.getExternalStorageDirectory().getPath() + "/ElementalX/" + p() + ".zip");
    File localFile2 = new File(Environment.getExternalStorageDirectory().getPath() + "/ElementalX/" + p() + "-express.zip");
    if (paramString.equals("express"))
    {
      if ((localFile2.exists()) && (k.b("prefAutoReboot").booleanValue()))
      {
        l();
        return;
      }
      if ((!localFile2.exists()) && (new f().a(getActivity())))
      {
        c(paramString);
        return;
      }
      k();
      return;
    }
    if ((localFile1.exists()) && (k.b("prefAutoReboot").booleanValue()))
    {
      l();
      return;
    }
    if ((!localFile1.exists()) && (new f().a(getActivity())))
    {
      c(paramString);
      return;
    }
    k();
  }
  
  private void f()
  {
    ac localac = new ac(getActivity());
    localac.a(getString(2131623971)).b(getString(2131623970)).b(2131624005, new fb(this));
    localac.a().show();
  }
  
  private void f(String paramString)
  {
    if (paramString.equals("express")) {}
    for (String str = k.a("prefChecksum") + "-express";; str = k.a("prefChecksum"))
    {
      new ga(this, null).execute(new String[] { str });
      return;
    }
  }
  
  private void g()
  {
    View localView = getActivity().getLayoutInflater().inflate(2130903083, null);
    ac localac = new ac(getActivity());
    localac.a(localView);
    ((TextView)localView.findViewById(2131493084)).setText(this.m);
    RadioButton localRadioButton1 = (RadioButton)localView.findViewById(2131493077);
    RadioButton localRadioButton2 = (RadioButton)localView.findViewById(2131493079);
    if (k.a("prefInstaller").equals("express"))
    {
      localRadioButton1.setChecked(true);
      localRadioButton2.setChecked(false);
    }
    ab localab;
    for (;;)
    {
      localRadioButton1.setOnClickListener(new fc(this));
      localRadioButton2.setOnClickListener(new fd(this));
      CheckBox localCheckBox = (CheckBox)localView.findViewById(2131493081);
      if (k.b("prefAutoReboot").booleanValue()) {
        localCheckBox.setChecked(true);
      }
      localCheckBox.setOnCheckedChangeListener(new fe(this));
      localac.a(this.i.getString(2131624065), new fg(this)).b(this.i.getString(2131623999), new ff(this));
      localac.a(this.i.getString(2131624066));
      localab = localac.a();
      localab.show();
      if (!getResources().getBoolean(2131296265)) {
        break;
      }
      localab.getWindow().setLayout(-1, -2);
      return;
      localRadioButton1.setChecked(false);
      localRadioButton2.setChecked(true);
    }
    localab.getWindow().setLayout(1000, -2);
  }
  
  private boolean g(String paramString)
  {
    String str1;
    try
    {
      String str2 = l.a(paramString);
      str1 = str2;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        str1 = null;
      }
    }
    if (str1 == null) {
      return false;
    }
    return this.n.equalsIgnoreCase(str1);
  }
  
  private void h()
  {
    DownloadManager.Query localQuery = new DownloadManager.Query();
    Cursor localCursor = this.t.query(localQuery);
    int i1;
    String str;
    if (localCursor.moveToFirst())
    {
      i1 = localCursor.getInt(localCursor.getColumnIndex("status"));
      str = localCursor.getString(localCursor.getColumnIndex("local_filename"));
    }
    switch (i1)
    {
    default: 
      return;
    case 16: 
      i();
      this.l = 2;
      return;
    }
    if (!g(str))
    {
      new File(str).delete();
      i();
      this.l = 2;
      return;
    }
    if (k.b("prefAutoReboot").booleanValue())
    {
      l();
      return;
    }
    j();
    this.l = 2;
  }
  
  private void h(String paramString)
  {
    k.a("prefSubVersion", paramString);
  }
  
  private void i()
  {
    ac localac = new ac(getActivity());
    localac.a(getString(2131624069)).b(getString(2131624070)).a(getString(2131624170), new fi(this)).b(getString(2131623999), new fh(this));
    localac.a().show();
  }
  
  private void i(String paramString)
  {
    k.a("prefDownloadFileName", paramString);
  }
  
  private void j()
  {
    ac localac = new ac(getActivity());
    localac.a(getString(2131624067)).b(getString(2131624068)).a(getString(2131624170), new fl(this)).b(getString(2131623999), new fj(this));
    localac.a().show();
  }
  
  private void j(String paramString)
  {
    k.a("prefLatestVersion", paramString);
  }
  
  private void k()
  {
    ac localac = new ac(getActivity());
    localac.a(getString(2131623972)).b(getString(2131624068)).a(getString(2131624170), new fn(this)).b(getString(2131623999), new fm(this));
    localac.a().show();
  }
  
  private void k(String paramString)
  {
    Intent localIntent1 = new Intent();
    localIntent1.setAction("android.intent.action.SEND");
    localIntent1.putExtra("android.intent.extra.SUBJECT", "ElementalX Update!");
    localIntent1.putExtra("android.intent.extra.TEXT", "New ElementalX Kernel for " + o() + ": " + p() + "\nhttp://elementalx.org/devices/" + t());
    localIntent1.setType("text/plain");
    if (paramString.equals("see_all"))
    {
      startActivity(Intent.createChooser(localIntent1, getString(2131624243)));
      return;
    }
    if (paramString.equals("twitter"))
    {
      Object[] arrayOfObject = new Object[2];
      arrayOfObject[0] = d("New ElementalX Kernel for " + o() + ": ");
      arrayOfObject[1] = d("http://elementalx.org/devices/" + t());
      Intent localIntent2 = new Intent("android.intent.action.VIEW", Uri.parse(String.format("https://twitter.com/intent/tweet?text=%s&url=%s", arrayOfObject)));
      Iterator localIterator2 = getActivity().getPackageManager().queryIntentActivities(localIntent2, 0).iterator();
      while (localIterator2.hasNext())
      {
        ResolveInfo localResolveInfo2 = (ResolveInfo)localIterator2.next();
        if (localResolveInfo2.activityInfo.packageName.toLowerCase().startsWith("com.twitter")) {
          localIntent2.setPackage(localResolveInfo2.activityInfo.packageName);
        }
      }
      startActivity(localIntent2);
      return;
    }
    List localList = getActivity().getPackageManager().queryIntentActivities(localIntent1, 0);
    int i1;
    if (!localList.isEmpty())
    {
      Iterator localIterator1 = localList.iterator();
      i1 = 0;
      label345:
      if (!localIterator1.hasNext()) {
        break label429;
      }
      ResolveInfo localResolveInfo1 = (ResolveInfo)localIterator1.next();
      if ((!localResolveInfo1.activityInfo.packageName.toLowerCase().contains(paramString)) && (!localResolveInfo1.activityInfo.name.toLowerCase().contains(paramString))) {
        break label450;
      }
      localIntent1.setPackage(localResolveInfo1.activityInfo.packageName);
    }
    label429:
    label450:
    for (int i2 = 1;; i2 = i1)
    {
      i1 = i2;
      break label345;
      i1 = 0;
      if (i1 == 0) {
        break;
      }
      startActivity(Intent.createChooser(localIntent1, getString(2131624243)));
      return;
    }
  }
  
  private void l()
  {
    f localf = new f();
    localf.a("mkdir /cache/recovery");
    localf.a("touch /cache/recovery/command");
    try
    {
      localf.a("777", "/cache/recovery/command");
      localf.a("echo boot-recovery > /cache/recovery/command");
      if (k.a("prefInstaller").equals("express"))
      {
        localf.a("echo --update_package=/sdcard/ElementalX/" + p() + "-express.zip >> /cache/recovery/command");
        this.u = false;
        ac localac = new ac(getActivity());
        localac.a(false).a(getString(2131624187)).b("").b(getString(2131623999), new fo(this));
        ab localab = localac.a();
        localab.show();
        new fp(this, 3000L, 1000L, localab).start();
        return;
      }
    }
    catch (TimeoutException localTimeoutException)
    {
      for (;;)
      {
        localTimeoutException.printStackTrace();
      }
    }
    catch (com.e.a.a.a locala)
    {
      for (;;)
      {
        locala.printStackTrace();
      }
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        localIOException.printStackTrace();
        continue;
        localf.a("echo --update_package=/sdcard/ElementalX/" + p() + ".zip >> /cache/recovery/command");
      }
    }
  }
  
  private void m()
  {
    try
    {
      new f().e(getActivity());
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }
  
  private String n()
  {
    return k.a("prefSubVersion");
  }
  
  private String o()
  {
    return k.a("prefDeviceName");
  }
  
  private String p()
  {
    return k.a("prefDownloadFileName");
  }
  
  private String q()
  {
    return k.a("prefCurrentKernel");
  }
  
  private String r()
  {
    return k.a("prefLatestVersion");
  }
  
  private void s()
  {
    if (r().contains("ElementalX")) {
      this.b.setVisible(true);
    }
    for (;;)
    {
      Intent localIntent = new Intent();
      localIntent.setAction("android.intent.action.SEND");
      localIntent.setType("text/plain");
      List localList = getActivity().getPackageManager().queryIntentActivities(localIntent, 0);
      if (localList.isEmpty()) {
        break;
      }
      Iterator localIterator = localList.iterator();
      while (localIterator.hasNext())
      {
        ResolveInfo localResolveInfo = (ResolveInfo)localIterator.next();
        if ((localResolveInfo.activityInfo.packageName.toLowerCase().contains("facebook")) || (localResolveInfo.activityInfo.name.toLowerCase().contains("facebook")))
        {
          this.c.setVisible(true);
          this.c.setIcon(localResolveInfo.loadIcon(getActivity().getPackageManager()));
        }
        if ((localResolveInfo.activityInfo.packageName.toLowerCase().contains("hangout")) || (localResolveInfo.activityInfo.name.toLowerCase().contains("hangout")))
        {
          this.f.setVisible(true);
          this.f.setIcon(localResolveInfo.loadIcon(getActivity().getPackageManager()));
        }
        if ((localResolveInfo.activityInfo.packageName.toLowerCase().contains("apps.plus")) || (localResolveInfo.activityInfo.name.toLowerCase().contains("apps.plus")))
        {
          this.e.setVisible(true);
          this.e.setIcon(localResolveInfo.loadIcon(getActivity().getPackageManager()));
        }
        if ((localResolveInfo.activityInfo.packageName.toLowerCase().contains("mail")) || (localResolveInfo.activityInfo.name.toLowerCase().contains("mail")))
        {
          this.g.setVisible(true);
          this.g.setIcon(localResolveInfo.loadIcon(getActivity().getPackageManager()));
        }
        if ((localResolveInfo.activityInfo.packageName.toLowerCase().contains("twitter")) || (localResolveInfo.activityInfo.name.toLowerCase().contains("twitter")))
        {
          this.d.setVisible(true);
          this.d.setIcon(localResolveInfo.loadIcon(getActivity().getPackageManager()));
        }
      }
      this.b.setVisible(false);
    }
  }
  
  private String t()
  {
    String str = o();
    if (str.equals(this.i.getString(2131624153))) {
      return "nexus-5";
    }
    if (str.equals(this.i.getString(2131624157))) {
      return "nexus-7-2013";
    }
    if (str.equals(this.i.getString(2131624113))) {
      return "htc-one-m8";
    }
    if (str.equals(this.i.getString(2131624111))) {
      return "htc-one-m7";
    }
    if (str.equals(this.i.getString(2131624116))) {
      return "htc-one-s";
    }
    if (str.equals(this.i.getString(2131624155))) {
      return "nexus-6";
    }
    if (str.equals(this.i.getString(2131624159))) {
      return "nexus-9";
    }
    return "";
  }
  
  private String u()
  {
    String str = o();
    if (str.equals(this.i.getString(2131624153))) {
      return "http://forum.xda-developers.com/google-nexus-5/orig-development/kernel-elementalx-n5-0-44-t2519607";
    }
    if (str.equals(this.i.getString(2131624157))) {
      return "http://forum.xda-developers.com/showthread.php?t=2389022";
    }
    if (str.equals(this.i.getString(2131624113))) {
      return "http://forum.xda-developers.com/showthread.php?t=2705613";
    }
    if (str.equals(this.i.getString(2131624111))) {
      return "http://forum.xda-developers.com/showthread.php?t=2249774";
    }
    if (str.equals(this.i.getString(2131624116))) {
      return "http://forum.xda-developers.com/showthread.php?t=2083229";
    }
    if (str.equals(this.i.getString(2131624155))) {
      return "http://forum.xda-developers.com/nexus-6/orig-development/kernel-elementalx-n6-0-01-alpha-t2954680";
    }
    if (str.equals(this.i.getString(2131624159))) {
      return "http://forum.xda-developers.com/nexus-9/orig-development/kernel-elementalx-n9-0-02-alpha-t2931657";
    }
    if (str.equals(this.i.getString(2131624115))) {
      return "http://forum.xda-developers.com/one-m9/orig-development/kernel-elementalx-m9-0-1-t3087586";
    }
    return "http://forum.xda-developers.com/";
  }
  
  protected void a()
  {
    String str1 = r();
    try
    {
      str1.substring(str1.indexOf("E"));
      String str5 = r();
      str4 = str5.substring(str5.indexOf("E"));
    }
    catch (StringIndexOutOfBoundsException localStringIndexOutOfBoundsException)
    {
      for (;;)
      {
        localStringIndexOutOfBoundsException = localStringIndexOutOfBoundsException;
        String str3 = r();
        String str4 = str3.substring(str3.indexOf("E"));
      }
    }
    finally
    {
      localObject = finally;
      String str2 = r();
      str2.substring(str2.indexOf("E"));
      throw ((Throwable)localObject);
    }
    i(str4);
  }
  
  protected void a(String paramString)
  {
    j(paramString);
    if (paramString.equals("Not available"))
    {
      this.j.setText(getString(2131623993));
      this.l = 0;
      this.j.setEnabled(false);
    }
    for (;;)
    {
      s();
      return;
      if (!paramString.equals(q()))
      {
        a();
        if (new f().a(getActivity())) {
          e();
        }
        this.j.setText(getString(2131623995));
        this.l = 2;
        this.j.setEnabled(true);
      }
      else
      {
        a();
        if (new f().a(getActivity())) {
          e();
        }
        this.j.setText(getString(2131623991));
        this.l = 2;
        this.j.setEnabled(true);
      }
    }
  }
  
  protected String b(String paramString)
  {
    String str1 = r();
    String str2 = str1.substring(str1.indexOf("E"));
    if (paramString.equals("express")) {
      return "http://elementalx.org/kernels/" + o() + "/" + Build.VERSION.RELEASE + "/" + n() + "/" + str2 + "-express.zip";
    }
    return "http://elementalx.org/kernels/" + o() + "/" + Build.VERSION.RELEASE + "/" + n() + "/" + str2 + ".zip";
  }
  
  protected void c(String paramString)
  {
    Uri localUri = Uri.parse(b(paramString));
    Activity localActivity = getActivity();
    getActivity();
    this.t = ((DownloadManager)localActivity.getSystemService("download"));
    DownloadManager.Request localRequest = new DownloadManager.Request(localUri);
    f(paramString);
    localRequest.setAllowedOverRoaming(false);
    localRequest.setTitle(r());
    if (paramString.equals("express")) {
      localRequest.setDestinationInExternalPublicDir("ElementalX", p() + "-express.zip");
    }
    for (;;)
    {
      this.l = 1;
      this.s = this.t.enqueue(localRequest);
      return;
      localRequest.setDestinationInExternalPublicDir("ElementalX", p() + ".zip");
    }
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if (paramMenu != null)
    {
      paramMenu.removeItem(2131493247);
      paramMenu.removeItem(2131493246);
    }
    if (paramMenu != null) {
      this.b = paramMenu.findItem(2131493236);
    }
    this.b.setVisible(false);
    if (paramMenu != null)
    {
      this.c = paramMenu.findItem(2131493238);
      this.c.setVisible(false);
      this.e = paramMenu.findItem(2131493237);
      this.e.setVisible(false);
      this.d = paramMenu.findItem(2131493239);
      this.f = paramMenu.findItem(2131493240);
      this.f.setVisible(false);
      this.g = paramMenu.findItem(2131493241);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    View localView = paramLayoutInflater.inflate(2130903095, paramViewGroup, false);
    String[] arrayOfString = getResources().getStringArray(2131230721);
    getActivity().setTitle(arrayOfString[7]);
    ((flar2.exkernelmanager.slidingmenu.a.a)flar2.exkernelmanager.slidingmenu.a.a.a.getAdapter()).f(flar2.exkernelmanager.slidingmenu.a.a.b.indexOf("Update"));
    this.w = ((TextView)getActivity().findViewById(2131493046));
    this.x = ((ImageView)getActivity().findViewById(2131493045));
    MainActivity.p.setVisibility(8);
    MainActivity.o.getLayoutParams().height = new f().b(getActivity());
    MainActivity.o.setTranslationY(0.0F);
    MainActivity.p.setTranslationY(0.0F);
    this.w.setText(null);
    this.i = MainApp.a();
    this.h.d(getActivity());
    o();
    ((TextView)localView.findViewById(2131493165)).setText(q());
    r();
    this.j = ((Button)localView.findViewById(2131493170));
    this.j.setLayerType(1, null);
    this.j.setText(getString(2131623990));
    this.j.setOnClickListener(new ez(this));
    this.l = 0;
    this.k = localView.findViewById(2131493168);
    b();
    this.o = ((RadioButton)localView.findViewById(2131493172));
    this.p = ((RadioButton)localView.findViewById(2131493173));
    if ((!k.b("prefHTC").booleanValue()) && (!k.a("prefDeviceName").equals(this.i.getString(2131624159))) && (!k.a("prefDeviceName").equals(this.i.getString(2131624155))))
    {
      this.o.setVisibility(0);
      this.p.setVisibility(0);
      if (n().equals("CAF")) {
        this.p.setChecked(true);
      }
      this.o.setOnClickListener(new fk(this));
      this.p.setOnClickListener(new fq(this));
    }
    localView.findViewById(2131493163).bringToFront();
    this.v = ((SwipeRefreshLayout)localView.findViewById(2131493166));
    this.v.setProgressBackgroundColorSchemeColor(getResources().getColor(2131361829));
    this.v.setColorSchemeResources(new int[] { 2131361807, 2131361798, 2131361807 });
    this.v.setOnRefreshListener(new fr(this));
    if (this.a.a().equals("incompatible")) {
      c();
    }
    this.r = new ft(this);
    getActivity().registerReceiver(this.r, new IntentFilter("android.intent.action.DOWNLOAD_COMPLETE"));
    setHasOptionsMenu(true);
    ((FloatingActionButton)localView.findViewById(2131493174)).setOnClickListener(new fu(this));
    return localView;
  }
  
  public void onDestroy()
  {
    getActivity().unregisterReceiver(this.r);
    MainActivity.p.setVisibility(0);
    super.onDestroy();
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131493238: 
      k("facebook");
      return true;
    case 2131493237: 
      k("apps.plus");
      return true;
    case 2131493239: 
      k("twitter");
      return true;
    case 2131493240: 
      k("hangout");
      return true;
    case 2131493241: 
      k("mail");
      return true;
    }
    k("see_all");
    return true;
  }
  
  public void onResume()
  {
    super.onResume();
    b();
    if (new f().a(getActivity()))
    {
      this.v.setRefreshing(true);
      this.v.postDelayed(new fv(this), 500L);
      return;
    }
    this.j.setText(getString(2131624161));
    this.l = 0;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/ey.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */