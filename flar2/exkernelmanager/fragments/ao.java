package flar2.exkernelmanager.fragments;

import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;

class ao
  implements View.OnClickListener
{
  ao(CPUTimeActivity paramCPUTimeActivity) {}
  
  public void onClick(View paramView)
  {
    int i = 0;
    String str1 = "";
    k.a("prefCPUTimeSaveOffsets", true);
    String[] arrayOfString = CPUTimeActivity.c(this.a).a("/sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state", false);
    int j = arrayOfString.length;
    while (i < j)
    {
      String str2 = arrayOfString[i];
      str1 = str1 + str2.split(" ")[1] + ",";
      i++;
    }
    k.a("prefCPUTimeOffsets", str1);
    k.a("prefCPUTimeDeepSleepOffset", Long.toString((SystemClock.elapsedRealtime() - SystemClock.uptimeMillis()) / 10L));
    CPUTimeActivity.a(this.a);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/ao.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */