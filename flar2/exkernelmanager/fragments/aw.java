package flar2.exkernelmanager.fragments;

import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Switch;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;

class aw
  implements SeekBar.OnSeekBarChangeListener
{
  aw(ColorActivity paramColorActivity) {}
  
  public void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean)
  {
    ColorActivity.i(this.a).a(Integer.toString(paramInt + 223), "/sys/devices/platform/kcal_ctrl.0/kcal_sat");
    ColorActivity.m(this.a).setText(Integer.toString(paramInt));
    ColorActivity.n(this.a).setChecked(false);
    k.a("prefKcalSat", Integer.toString(paramInt + 223));
  }
  
  public void onStartTrackingTouch(SeekBar paramSeekBar) {}
  
  public void onStopTrackingTouch(SeekBar paramSeekBar) {}
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/aw.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */