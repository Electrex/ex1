package flar2.exkernelmanager.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import flar2.exkernelmanager.MainActivity;
import flar2.exkernelmanager.MainApp;
import flar2.exkernelmanager.a.o;
import flar2.exkernelmanager.utilities.e;
import flar2.exkernelmanager.utilities.f;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;
import java.util.ArrayList;
import java.util.List;

public class ep
  extends Fragment
  implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener
{
  private ListView a;
  private flar2.exkernelmanager.a.a b;
  private Context c;
  private m d = new m();
  private e e = new e();
  private TextView f;
  private TextView g;
  private ImageView h;
  private int i;
  private int j;
  private RectF k = new RectF();
  private RectF l = new RectF();
  private AccelerateDecelerateInterpolator m;
  
  public static float a(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    return Math.max(paramFloat2, Math.min(paramFloat1, paramFloat3));
  }
  
  private int a(String paramString)
  {
    String str = this.d.a(paramString);
    int n = Integer.parseInt(str.substring(0, str.indexOf(" ")));
    if (k.a("prefDeviceName").equals(this.c.getString(2131624153))) {
      return 38 - n;
    }
    if (k.a("prefDeviceName").equals(this.c.getString(2131624157))) {
      return 22 - n;
    }
    return 38 - n;
  }
  
  private int a(String paramString, boolean paramBoolean)
  {
    String str = this.d.a(paramString);
    if (paramBoolean) {}
    for (int n = Integer.parseInt(str.substring(0, str.indexOf(" "))); n < 30; n = Integer.parseInt(str)) {
      return n;
    }
    return n - 256;
  }
  
  private RectF a(RectF paramRectF, View paramView)
  {
    paramRectF.set(paramView.getLeft(), paramView.getTop(), paramView.getRight(), paramView.getBottom());
    return paramRectF;
  }
  
  private void a(int paramInt)
  {
    MainActivity.o.setTranslationY(Math.max(-paramInt, this.j));
    MainActivity.p.setTranslationY(Math.max(-paramInt, this.j));
    float f1 = a(MainActivity.o.getTranslationY() / this.j, 0.0F, 1.0F);
    a(this.f, this.g, this.m.getInterpolation(f1));
    a(this.h, this.g, this.m.getInterpolation(f1));
    this.h.setAlpha(1.0F - f1 * 2.0F);
  }
  
  private void a(View paramView1, View paramView2, float paramFloat)
  {
    a(this.k, paramView1);
    a(this.l, paramView2);
    float f1 = 1.0F + paramFloat * (this.l.width() / this.k.width() - 1.0F);
    float f2 = 1.0F + paramFloat * (this.l.height() / this.k.height() - 1.0F);
    float f3 = 0.5F * (paramFloat * (this.l.left + this.l.right - this.k.left - this.k.right));
    float f4 = 0.5F * (paramFloat * (this.l.top + this.l.bottom - this.k.top - this.k.bottom));
    paramView1.setTranslationX(f3);
    paramView1.setTranslationY(f4 - MainActivity.o.getTranslationY());
    paramView1.setScaleX(f1);
    paramView1.setScaleY(f2);
  }
  
  private void a(String paramString1, String paramString2)
  {
    k.a(paramString1 + "Boot", false);
    if (this.d.a(paramString2).equals("0"))
    {
      this.d.a("2", paramString2);
      k.a(paramString1, "2");
    }
    for (;;)
    {
      b();
      return;
      if (this.d.a(paramString2).equals("2"))
      {
        this.d.a("0", paramString2);
        k.a(paramString1, "0");
      }
    }
  }
  
  private void b()
  {
    new er(this, null).execute(new Void[0]);
  }
  
  private List c()
  {
    ArrayList localArrayList = new ArrayList();
    if (getActivity().getResources().getConfiguration().orientation == 1)
    {
      o localo1 = new o();
      localo1.a(7);
      localArrayList.add(localo1);
    }
    o localo2 = new o();
    localo2.a(0);
    localo2.a(this.c.getString(2131624247));
    localArrayList.add(localo2);
    if ((!this.e.a("/sys/kernel/sound_control_3")) && (!this.e.a("/sys/devices/virtual/misc/soundcontrol/volume_boost")))
    {
      o localo15 = new o();
      localo15.a(1);
      localo15.a(this.c.getString(2131624164));
      localArrayList.add(localo15);
    }
    o localo3;
    label269:
    o localo5;
    label452:
    o localo14;
    label636:
    o localo13;
    label900:
    o localo7;
    label1044:
    o localo8;
    label1188:
    o localo9;
    label1304:
    label1338:
    o localo10;
    label1454:
    label1488:
    o localo11;
    label1621:
    o localo12;
    if (this.e.a("/sys/kernel/sound_control_3/gpl_sound_control_locked"))
    {
      localo3 = new o();
      localo3.a(1);
      localo3.b(64731);
      localo3.a(this.c.getString(2131624133));
      if (this.d.a("/sys/kernel/sound_control_3/gpl_sound_control_locked").equals("0"))
      {
        localo3.b(this.c.getString(2131624168));
        localo3.d("prefSoundLockedBoot");
        if (!k.b("prefSoundLockedBoot").booleanValue()) {
          break label1783;
        }
        localo3.a(true);
        localo3.c(2130837611);
        localArrayList.add(localo3);
      }
    }
    else
    {
      if (this.e.a("/sys/kernel/sound_control_3/gpl_headphone_gain"))
      {
        o localo4 = new o();
        localo4.a(0);
        localo4.a(this.c.getString(2131624108));
        localArrayList.add(localo4);
        localo5 = new o();
        localo5.a(3);
        localo5.b(64733);
        localo5.b(this.c.getString(2131624098) + Integer.toString(a("/sys/kernel/sound_control_3/gpl_headphone_gain", true)));
        localo5.e(50);
        localo5.d(30 + a("/sys/kernel/sound_control_3/gpl_headphone_gain", true));
        localo5.d("PREF_SOUND_HEADPHONE_GAINBoot");
        if (!k.b("PREF_SOUND_HEADPHONE_GAINBoot").booleanValue()) {
          break label1800;
        }
        localo5.a(true);
        localo5.c(2130837611);
        localArrayList.add(localo5);
      }
      if ((this.e.a("/sys/kernel/sound_control_3/gpl_headphone_pa_gain")) && (!k.a("prefDeviceName").equals(this.c.getString(2131624113))) && (!k.a("prefDeviceName").equals(this.c.getString(2131624155))))
      {
        localo14 = new o();
        localo14.a(3);
        localo14.b(64732);
        localo14.b(this.c.getString(2131624180) + Integer.toString(a("/sys/kernel/sound_control_3/gpl_headphone_pa_gain")));
        localo14.e(12);
        localo14.d(6 + a("/sys/kernel/sound_control_3/gpl_headphone_pa_gain"));
        localo14.d("PREF_SOUND_HEADPHONE_PA_GAINBoot");
        if (!k.b("PREF_SOUND_HEADPHONE_PA_GAINBoot").booleanValue()) {
          break label1817;
        }
        localo14.a(true);
        localo14.c(2130837611);
        localArrayList.add(localo14);
      }
      if ((this.e.a("/sys/kernel/sound_control_3/gpl_speaker_gain")) || (this.e.a("/sys/kernel/sound_control_3/gpl_mic_gain")) || (this.e.a("/sys/kernel/sound_control_3/gpl_cam_mic_gain")))
      {
        o localo6 = new o();
        localo6.a(0);
        localo6.a(this.c.getString(2131624099));
        localArrayList.add(localo6);
      }
      if ((this.e.a("/sys/kernel/sound_control_3/gpl_speaker_gain")) && (!k.a("prefDeviceName").equals(this.c.getString(2131624113))) && (!k.a("prefDeviceName").equals(this.c.getString(2131624155))))
      {
        localo13 = new o();
        localo13.a(3);
        localo13.b(64734);
        localo13.b(this.c.getString(2131624248) + Integer.toString(a("/sys/kernel/sound_control_3/gpl_speaker_gain", true)));
        localo13.e(50);
        localo13.d(30 + a("/sys/kernel/sound_control_3/gpl_speaker_gain", true));
        localo13.d("PREF_SOUND_SPEAKER_GAINBoot");
        if (!k.b("PREF_SOUND_SPEAKER_GAINBoot").booleanValue()) {
          break label1834;
        }
        localo13.a(true);
        localo13.c(2130837611);
        localArrayList.add(localo13);
      }
      if (this.e.a("/sys/kernel/sound_control_3/gpl_mic_gain"))
      {
        localo7 = new o();
        localo7.a(3);
        localo7.b(64736);
        localo7.b(this.c.getString(2131624107) + Integer.toString(a("/sys/kernel/sound_control_3/gpl_mic_gain", false)));
        localo7.e(50);
        localo7.d(30 + a("/sys/kernel/sound_control_3/gpl_mic_gain", false));
        localo7.d("PREF_SOUND_MIC_GAINBoot");
        if (!k.b("PREF_SOUND_MIC_GAINBoot").booleanValue()) {
          break label1851;
        }
        localo7.a(true);
        localo7.c(2130837611);
        localArrayList.add(localo7);
      }
      if (this.e.a("/sys/kernel/sound_control_3/gpl_cam_mic_gain"))
      {
        localo8 = new o();
        localo8.a(3);
        localo8.b(64735);
        localo8.b(this.c.getString(2131623997) + Integer.toString(a("/sys/kernel/sound_control_3/gpl_cam_mic_gain", false)));
        localo8.e(50);
        localo8.d(30 + a("/sys/kernel/sound_control_3/gpl_cam_mic_gain", false));
        localo8.d("PREF_SOUND_CAM_GAINBoot");
        if (!k.b("PREF_SOUND_CAM_GAINBoot").booleanValue()) {
          break label1868;
        }
        localo8.a(true);
        localo8.c(2130837611);
        localArrayList.add(localo8);
      }
      if (this.e.a("/sys/devices/virtual/misc/soundcontrol/mic_boost"))
      {
        localo9 = new o();
        localo9.a(3);
        localo9.b(64730);
        localo9.b("Mic gain: " + this.d.a("/sys/devices/virtual/misc/soundcontrol/mic_boost"));
        if (!k.a("prefDeviceName").equals("Nexus6")) {
          break label1885;
        }
        localo9.e(40);
        localo9.d(20 + a("/sys/devices/virtual/misc/soundcontrol/mic_boost", false));
        localo9.d("prefMicBoostBoot");
        if (!k.b("prefMicBoostBoot").booleanValue()) {
          break label1908;
        }
        localo9.a(true);
        localo9.c(2130837611);
        localArrayList.add(localo9);
      }
      if (this.e.a("/sys/devices/virtual/misc/soundcontrol/volume_boost"))
      {
        localo10 = new o();
        localo10.a(3);
        localo10.b(64729);
        localo10.b("Volume gain: " + this.d.a("/sys/devices/virtual/misc/soundcontrol/volume_boost"));
        if (!k.a("prefDeviceName").equals("Nexus6")) {
          break label1925;
        }
        localo10.e(40);
        localo10.d(20 + a("/sys/devices/virtual/misc/soundcontrol/volume_boost", false));
        localo10.d("prefVolBoostBoot");
        if (!k.b("prefVolBoostBoot").booleanValue()) {
          break label1948;
        }
        localo10.a(true);
        localo10.c(2130837611);
        localArrayList.add(localo10);
      }
      if (this.e.a("/sys/devices/virtual/misc/soundcontrol/headset_boost"))
      {
        localo11 = new o();
        localo11.a(3);
        localo11.b(64727);
        localo11.b("Headset volume: " + this.d.a("/sys/devices/virtual/misc/soundcontrol/headset_boost"));
        localo11.e(20);
        localo11.d(a("/sys/devices/virtual/misc/soundcontrol/volume_boost", false));
        localo11.d("prefHeadsetBoostBoot");
        if (!k.b("prefHeadsetBoostBoot").booleanValue()) {
          break label1965;
        }
        localo11.a(true);
        localo11.c(2130837611);
        localArrayList.add(localo11);
      }
      if (this.e.a("/sys/devices/virtual/misc/soundcontrol/speaker_boost"))
      {
        localo12 = new o();
        localo12.a(3);
        localo12.b(64728);
        localo12.b("Speaker volume: " + this.d.a("/sys/devices/virtual/misc/soundcontrol/speaker_boost"));
        localo12.e(20);
        localo12.d(a("/sys/devices/virtual/misc/soundcontrol/speaker_boost", false));
        localo12.d("prefSpeakerBoostBoot");
        if (!k.b("prefSpeakerBoostBoot").booleanValue()) {
          break label1982;
        }
        localo12.a(true);
        localo12.c(2130837611);
      }
    }
    for (;;)
    {
      localArrayList.add(localo12);
      return localArrayList;
      localo3.b(this.c.getString(2131624134));
      break;
      label1783:
      localo3.a(false);
      localo3.c(2130837609);
      break label269;
      label1800:
      localo5.a(false);
      localo5.c(2130837609);
      break label452;
      label1817:
      localo14.a(false);
      localo14.c(2130837609);
      break label636;
      label1834:
      localo13.a(false);
      localo13.c(2130837609);
      break label900;
      label1851:
      localo7.a(false);
      localo7.c(2130837609);
      break label1044;
      label1868:
      localo8.a(false);
      localo8.c(2130837609);
      break label1188;
      label1885:
      localo9.e(20);
      localo9.d(a("/sys/devices/virtual/misc/soundcontrol/mic_boost", false));
      break label1304;
      label1908:
      localo9.a(false);
      localo9.c(2130837609);
      break label1338;
      label1925:
      localo10.e(20);
      localo10.d(a("/sys/devices/virtual/misc/soundcontrol/volume_boost", false));
      break label1454;
      label1948:
      localo10.a(false);
      localo10.c(2130837609);
      break label1488;
      label1965:
      localo11.a(false);
      localo11.c(2130837609);
      break label1621;
      label1982:
      localo12.a(false);
      localo12.c(2130837609);
    }
  }
  
  public int a()
  {
    View localView = this.a.getChildAt(0);
    if (localView == null) {
      return 0;
    }
    int n = this.a.getFirstVisiblePosition();
    int i1 = localView.getTop();
    int i2 = 0;
    if (n >= 1) {
      i2 = this.i;
    }
    return i2 + (-i1 + n * localView.getHeight());
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if (paramMenu != null)
    {
      paramMenu.removeItem(2131493247);
      paramMenu.removeItem(2131493246);
      paramMenu.removeItem(2131493236);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView = paramLayoutInflater.inflate(2130903090, paramViewGroup, false);
    this.c = MainApp.a();
    ((flar2.exkernelmanager.slidingmenu.a.a)flar2.exkernelmanager.slidingmenu.a.a.a.getAdapter()).f(flar2.exkernelmanager.slidingmenu.a.a.b.indexOf("Sound"));
    setHasOptionsMenu(true);
    String[] arrayOfString = getResources().getStringArray(2131230721);
    getActivity().setTitle(arrayOfString[5]);
    this.a = ((ListView)localView.findViewById(2131492989));
    this.b = new flar2.exkernelmanager.a.a(getActivity(), new ArrayList());
    this.a.setAdapter(this.b);
    this.a.setOnItemClickListener(this);
    this.a.setOnItemLongClickListener(this);
    flar2.exkernelmanager.a.a.b = true;
    MainActivity.o = getActivity().findViewById(2131493043);
    if (getActivity().getResources().getConfiguration().orientation == 1)
    {
      MainActivity.o.getLayoutParams().height = getResources().getDimensionPixelSize(2131427421);
      MainActivity.p = getActivity().findViewById(2131493047);
      this.h = ((ImageView)getActivity().findViewById(2131493045));
      this.h.setImageResource(2130837638);
      this.g = ((TextView)getActivity().findViewById(2131493044));
      this.g.setText(arrayOfString[5]);
      this.f = ((TextView)getActivity().findViewById(2131493046));
      this.f.setText(arrayOfString[5]);
      this.i = getResources().getDimensionPixelSize(2131427421);
      this.j = (-this.i + new f().b(getActivity()));
      this.m = new AccelerateDecelerateInterpolator();
    }
    for (;;)
    {
      this.a.setOnScrollListener(new eq(this));
      if (!k.e("prefSoundLocked")) {
        k.a("prefSoundLocked", "0");
      }
      b();
      if (k.a.getBoolean("prefFirstRunSettings", true)) {}
      return localView;
      MainActivity.o.getLayoutParams().height = new f().b(getActivity());
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    if (this.m != null) {
      a(0);
    }
  }
  
  public void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    switch (((o)this.b.getItem(paramInt)).b())
    {
    default: 
      return;
    }
    a("prefSoundLocked", "/sys/kernel/sound_control_3/gpl_sound_control_locked");
  }
  
  public boolean onItemLongClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    ((o)this.b.getItem(paramInt)).b();
    Toast.makeText(this.c, "No help for this item", 0).show();
    return true;
  }
  
  public void onPause()
  {
    super.onPause();
    flar2.exkernelmanager.a.a.b = false;
  }
  
  public void onResume()
  {
    super.onResume();
    b();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/ep.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */