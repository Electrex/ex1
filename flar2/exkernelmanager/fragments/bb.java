package flar2.exkernelmanager.fragments;

import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

class bb
  implements View.OnFocusChangeListener
{
  bb(ColorActivity paramColorActivity, InputMethodManager paramInputMethodManager) {}
  
  public void onFocusChange(View paramView, boolean paramBoolean)
  {
    if (ColorActivity.l(this.b).hasFocus())
    {
      ColorActivity.l(this.b).selectAll();
      return;
    }
    ColorActivity.a(this.b, paramView);
    this.a.hideSoftInputFromWindow(paramView.getApplicationWindowToken(), 2);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/bb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */