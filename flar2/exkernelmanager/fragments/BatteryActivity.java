package flar2.exkernelmanager.fragments;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.a;
import android.support.v7.app.ad;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import com.c.a.aa;
import com.c.a.x;
import com.getbase.floatingactionbutton.FloatingActionButton;
import flar2.exkernelmanager.AboutActivity;
import flar2.exkernelmanager.BatteryMonitor.BatteryMonitorService;
import flar2.exkernelmanager.UserSettingsActivity;
import flar2.exkernelmanager.utilities.h;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BatteryActivity
  extends ad
  implements AdapterView.OnItemSelectedListener
{
  TextView A;
  TextView B;
  Spinner C;
  List D;
  int E;
  long F;
  long G;
  int H;
  int I;
  long J;
  long K;
  MenuItem L;
  MenuItem M;
  MenuItem N;
  MenuItem O;
  MenuItem P;
  MenuItem Q;
  h R;
  SwipeRefreshLayout S;
  private BroadcastReceiver T = new n(this);
  View n;
  TextView o;
  TextView p;
  TextView q;
  TextView r;
  TextView s;
  TextView t;
  TextView u;
  TextView v;
  TextView w;
  TextView x;
  TextView y;
  TextView z;
  
  private String a(long paramLong)
  {
    Object[] arrayOfObject = new Object[3];
    arrayOfObject[0] = Long.valueOf(TimeUnit.MILLISECONDS.toHours(paramLong));
    arrayOfObject[1] = Long.valueOf(TimeUnit.MILLISECONDS.toMinutes(paramLong) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(paramLong)));
    arrayOfObject[2] = Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(paramLong) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(paramLong)));
    return String.format("%02d:%02d:%02d", arrayOfObject);
  }
  
  private void a(int paramInt1, int paramInt2, int paramInt3)
  {
    String str = flar2.exkernelmanager.utilities.k.a("prefTempUnit");
    this.z.setText(paramInt2 + "%");
    int i;
    if (paramInt2 > 25)
    {
      this.z.setTextColor(getResources().getColor(2131361807));
      i = paramInt1 / 10;
      if (!str.equals("2")) {
        break label193;
      }
      double d = 32.0D + 1.8D * i;
      this.A.setText((int)d + "°F");
      label112:
      if (i >= 40) {
        break label233;
      }
      this.A.setTextColor(getResources().getColor(2131361807));
    }
    for (;;)
    {
      if (paramInt3 != 2) {
        break label279;
      }
      this.B.setText("Charging");
      return;
      if ((paramInt2 <= 25) && (paramInt2 >= 14))
      {
        this.z.setTextColor(65280);
        break;
      }
      if (paramInt2 >= 14) {
        break;
      }
      this.z.setTextColor(-65536);
      break;
      label193:
      if (!str.equals("1")) {
        break label112;
      }
      this.A.setText(i + "°C");
      break label112;
      label233:
      if ((i >= 40) && (i <= 52)) {
        this.A.setTextColor(65280);
      } else if (i > 52) {
        this.A.setTextColor(-65536);
      }
    }
    label279:
    if (paramInt3 == 5)
    {
      this.B.setText("Full");
      return;
    }
    this.B.setText("Discharging");
  }
  
  private void b(String paramString)
  {
    Intent localIntent1 = new Intent();
    localIntent1.setAction("android.intent.action.SEND");
    localIntent1.putExtra("android.intent.extra.SUBJECT", "EX Battery Monitor");
    localIntent1.putExtra("android.intent.extra.TEXT", Build.MODEL + "\n" + System.getProperty("os.version"));
    localIntent1.setType("image/jpeg");
    String str1 = Environment.getExternalStorageDirectory() + "/Pictures/Screenshots/battery.jpg";
    View localView = findViewById(2131492968);
    localView.setDrawingCacheEnabled(true);
    Bitmap localBitmap = localView.getDrawingCache();
    List localList;
    try
    {
      Bitmap.createScaledBitmap(localBitmap, (int)(0.6D * localBitmap.getWidth()), (int)(0.6D * localBitmap.getHeight()), true).compress(Bitmap.CompressFormat.JPEG, 85, new FileOutputStream(str1));
      localView.setDrawingCacheEnabled(false);
      localIntent1.putExtra("android.intent.extra.STREAM", Uri.parse("file:///" + str1));
      if (paramString.equals("see_all"))
      {
        startActivity(Intent.createChooser(localIntent1, getString(2131624243)));
        return;
      }
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      for (;;)
      {
        localFileNotFoundException.printStackTrace();
      }
      if (paramString.equals("twitter"))
      {
        String str2 = "EX battery stats: Screen " + this.r.getText() + ", " + this.q.getText() + ", " + this.p.getText();
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = str2.replace("%", " percent");
        Intent localIntent2 = new Intent("android.intent.action.VIEW", Uri.parse(String.format("https://twitter.com/intent/tweet?text=%s", arrayOfObject)));
        Iterator localIterator2 = getPackageManager().queryIntentActivities(localIntent2, 0).iterator();
        while (localIterator2.hasNext())
        {
          ResolveInfo localResolveInfo2 = (ResolveInfo)localIterator2.next();
          if (localResolveInfo2.activityInfo.packageName.toLowerCase().startsWith("com.twitter")) {
            localIntent2.setPackage(localResolveInfo2.activityInfo.packageName);
          }
        }
        startActivity(localIntent2);
        return;
      }
      localList = getPackageManager().queryIntentActivities(localIntent1, 0);
      if (localList.isEmpty()) {
        break label541;
      }
    }
    Iterator localIterator1 = localList.iterator();
    int i = 0;
    label460:
    if (localIterator1.hasNext())
    {
      ResolveInfo localResolveInfo1 = (ResolveInfo)localIterator1.next();
      if ((!localResolveInfo1.activityInfo.packageName.toLowerCase().contains(paramString)) && (!localResolveInfo1.activityInfo.name.toLowerCase().contains(paramString))) {
        break label565;
      }
      localIntent1.setPackage(localResolveInfo1.activityInfo.packageName);
    }
    label541:
    label565:
    for (int j = 1;; j = i)
    {
      i = j;
      break label460;
      i = 0;
      if (i == 0) {
        break;
      }
      startActivity(Intent.createChooser(localIntent1, getString(2131624243)));
      return;
    }
  }
  
  private void k()
  {
    Intent localIntent = new Intent();
    localIntent.setAction("android.intent.action.SEND");
    localIntent.setType("text/plain");
    List localList = getPackageManager().queryIntentActivities(localIntent, 0);
    if (!localList.isEmpty())
    {
      Iterator localIterator = localList.iterator();
      while (localIterator.hasNext())
      {
        ResolveInfo localResolveInfo = (ResolveInfo)localIterator.next();
        if ((localResolveInfo.activityInfo.packageName.toLowerCase().contains("facebook")) || (localResolveInfo.activityInfo.name.toLowerCase().contains("facebook")))
        {
          this.M.setVisible(true);
          this.M.setIcon(localResolveInfo.loadIcon(getPackageManager()));
        }
        if ((localResolveInfo.activityInfo.packageName.toLowerCase().contains("hangout")) || (localResolveInfo.activityInfo.name.toLowerCase().contains("hangout")))
        {
          this.P.setVisible(true);
          this.P.setIcon(localResolveInfo.loadIcon(getPackageManager()));
        }
        if ((localResolveInfo.activityInfo.packageName.toLowerCase().contains("apps.plus")) || (localResolveInfo.activityInfo.name.toLowerCase().contains("apps.plus")))
        {
          this.O.setVisible(true);
          this.O.setIcon(localResolveInfo.loadIcon(getPackageManager()));
        }
        if ((localResolveInfo.activityInfo.packageName.toLowerCase().contains("mail")) || (localResolveInfo.activityInfo.name.toLowerCase().contains("mail")))
        {
          this.Q.setVisible(true);
          this.Q.setIcon(localResolveInfo.loadIcon(getPackageManager()));
        }
        if ((localResolveInfo.activityInfo.packageName.toLowerCase().contains("twitter")) || (localResolveInfo.activityInfo.name.toLowerCase().contains("twitter")))
        {
          this.N.setVisible(true);
          this.N.setIcon(localResolveInfo.loadIcon(getPackageManager()));
        }
      }
    }
  }
  
  private void l()
  {
    if (flar2.exkernelmanager.utilities.k.b("prefDisableBatteryMon").booleanValue())
    {
      this.n.setVisibility(8);
      this.o.setVisibility(0);
      aa.a(com.c.a.n.a(this).a(x.c).a(getResources().getColor(2131361798)).b(getResources().getColor(2131361807)).b("ENABLE").a(new q(this)).a("Service is disabled"));
      return;
    }
    this.n.setVisibility(0);
    this.o.setVisibility(8);
    p();
  }
  
  private boolean m()
  {
    int i = registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED")).getIntExtra("status", -1);
    return (i == 2) || (i == 5);
  }
  
  private int n()
  {
    Intent localIntent = registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    int j;
    int i;
    if (localIntent != null)
    {
      j = localIntent.getIntExtra("level", -1);
      i = localIntent.getIntExtra("scale", -1);
    }
    for (;;)
    {
      return j * 100 / i;
      i = 0;
      j = 0;
    }
  }
  
  private long o()
  {
    return System.currentTimeMillis();
  }
  
  private void p()
  {
    long l1;
    int i;
    long l3;
    long l4;
    float f1;
    label92:
    float f2;
    if (!m()) {
      if (flar2.exkernelmanager.utilities.k.d("prefBMScrOnMarker") == -1L)
      {
        l1 = 0L;
        i = 0;
        long l2 = flar2.exkernelmanager.utilities.k.d("prefBMScrOffTime");
        int j = flar2.exkernelmanager.utilities.k.c("prefBMScrOffUsage");
        l3 = l1 - this.F;
        l4 = l2 - this.G;
        int k = i - this.H;
        int m = j - this.I;
        if (m <= 0) {
          break label624;
        }
        f1 = 3600000.0F * m / (float)l4;
        if (k <= 0) {
          break label630;
        }
        f2 = 3600000.0F * k / (float)l3;
        label110:
        DecimalFormat localDecimalFormat = new DecimalFormat("###.##");
        String str1 = "Time:  " + a(l3);
        String str2 = "Usage:  " + k + "%";
        String str3 = "Idle drain:  " + localDecimalFormat.format(f1) + "% per hour";
        String str4 = "Time:  " + a(l4);
        String str5 = "Usage:  " + m + "%";
        String str6 = "Active drain:  " + localDecimalFormat.format(f2) + "% per hour";
        this.r.setText(str1);
        this.t.setText(str2);
        this.p.setText(str3);
        this.s.setText(str4);
        this.u.setText(str5);
        this.q.setText(str6);
        if (!m()) {
          break label636;
        }
      }
    }
    label624:
    label630:
    label636:
    for (long l5 = flar2.exkernelmanager.utilities.k.d("prefBMChargeTime") + (o() - flar2.exkernelmanager.utilities.k.d("prefBMPlugMarker"));; l5 = flar2.exkernelmanager.utilities.k.d("prefBMChargeTime"))
    {
      String str7 = "Time charging:  " + a(l5 - this.K);
      long l6 = SystemClock.elapsedRealtime() - SystemClock.uptimeMillis();
      String str8 = "Deep sleep:  " + a(l6 - this.J);
      long l7 = l3 + l4;
      String str9 = "Time on Battery:  " + a(l7);
      long l8 = l4 - (l6 - this.J);
      String str10 = "Held awake:  " + a(l8);
      this.v.setText(str7);
      this.w.setText(str8);
      this.y.setText(str9);
      this.x.setText(str10);
      return;
      l1 = flar2.exkernelmanager.utilities.k.d("prefBMScrOnTime") + (o() - flar2.exkernelmanager.utilities.k.d("prefBMScrOnMarker"));
      i = flar2.exkernelmanager.utilities.k.c("prefBMScrOnUsage") + (flar2.exkernelmanager.utilities.k.c("prefBMScrOnLevel") - n());
      break;
      l1 = flar2.exkernelmanager.utilities.k.d("prefBMScrOnTime");
      i = flar2.exkernelmanager.utilities.k.c("prefBMScrOnUsage");
      break;
      f1 = 0.0F;
      break label92;
      f2 = 0.0F;
      break label110;
    }
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    this.R.b().onTouchEvent(paramMotionEvent);
    return super.dispatchTouchEvent(paramMotionEvent);
  }
  
  public void onBackPressed()
  {
    super.onBackPressed();
    overridePendingTransition(0, 2130968604);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903067);
    overridePendingTransition(2130968605, 17432577);
    a((Toolbar)findViewById(2131492951));
    g().a(true);
    setTitle("Battery Monitor");
    this.n = findViewById(2131492966);
    this.o = ((TextView)findViewById(2131492964));
    this.R = new j(this, this);
    findViewById(2131492958).setOnTouchListener(this.R);
    this.S = ((SwipeRefreshLayout)findViewById(2131492965));
    this.S.setProgressBackgroundColorSchemeColor(getResources().getColor(2131361829));
    this.S.setColorSchemeResources(new int[] { 2131361807, 2131361798, 2131361807 });
    this.S.setOnRefreshListener(new k(this));
    ((FloatingActionButton)findViewById(2131492986)).setOnClickListener(new m(this));
    this.z = ((TextView)findViewById(2131492960));
    this.B = ((TextView)findViewById(2131492963));
    this.A = ((TextView)findViewById(2131492961));
    this.C = ((Spinner)findViewById(2131492967));
    this.D = new ArrayList();
    this.D.add("Since start");
    if (flar2.exkernelmanager.utilities.k.d("prefBMFullMarker") > 0L) {}
    this.E = 1;
    this.D.add("Since now");
    if (flar2.exkernelmanager.utilities.k.d("prefBMCustomMarker") > 0L)
    {
      new DateFormat();
      this.D.add("Since " + DateFormat.format("MMM dd hh:mm a", flar2.exkernelmanager.utilities.k.d("prefBMCustomMarker")));
    }
    ArrayAdapter localArrayAdapter = new ArrayAdapter(this, 17367048, this.D);
    localArrayAdapter.setDropDownViewResource(17367049);
    this.C.setAdapter(localArrayAdapter);
    this.C.setOnItemSelectedListener(this);
    if (flar2.exkernelmanager.utilities.k.e("prefBMSpinnerSelection"))
    {
      if (flar2.exkernelmanager.utilities.k.c("prefBMSpinnerSelection") != this.E) {
        break label587;
      }
      this.C.setSelection(1 + this.E);
    }
    for (;;)
    {
      this.p = ((TextView)findViewById(2131492983));
      this.q = ((TextView)findViewById(2131492976));
      this.r = ((TextView)findViewById(2131492974));
      this.s = ((TextView)findViewById(2131492979));
      this.t = ((TextView)findViewById(2131492975));
      this.u = ((TextView)findViewById(2131492980));
      this.v = ((TextView)findViewById(2131492985));
      this.w = ((TextView)findViewById(2131492981));
      this.y = ((TextView)findViewById(2131492971));
      this.x = ((TextView)findViewById(2131492982));
      l();
      return;
      label587:
      this.C.setSelection(flar2.exkernelmanager.utilities.k.c("prefBMSpinnerSelection"));
    }
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131755008, paramMenu);
    if (flar2.exkernelmanager.utilities.k.b("prefDisableBatteryMon").booleanValue()) {
      paramMenu.findItem(2131493232).setChecked(true);
    }
    for (;;)
    {
      this.L = paramMenu.findItem(2131493236);
      if (paramMenu != null)
      {
        this.M = paramMenu.findItem(2131493238);
        this.M.setVisible(false);
        this.O = paramMenu.findItem(2131493237);
        this.O.setVisible(false);
        this.N = paramMenu.findItem(2131493239);
        this.P = paramMenu.findItem(2131493240);
        this.P.setVisible(false);
        this.Q = paramMenu.findItem(2131493241);
      }
      k();
      return true;
      paramMenu.findItem(2131493232).setChecked(false);
    }
  }
  
  public void onItemSelected(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    long l1 = 0L;
    this.C.setSelection(paramInt);
    String str = (String)this.C.getSelectedItem();
    flar2.exkernelmanager.utilities.k.a("prefBMSpinnerSelection", paramInt);
    if (str.equals("Since start"))
    {
      this.G = l1;
      this.F = l1;
      this.H = 0;
      this.I = 0;
      this.K = l1;
      this.J = flar2.exkernelmanager.utilities.k.d("prefBMDeepSleepOffset");
    }
    for (;;)
    {
      l();
      return;
      if (str.equals("Since last full charge"))
      {
        if (flar2.exkernelmanager.utilities.k.d("prefBMFullMarker") > l1)
        {
          this.F = flar2.exkernelmanager.utilities.k.d("prefBMFullTimeOn");
          this.G = flar2.exkernelmanager.utilities.k.d("prefBMFullTimeOff");
          this.H = flar2.exkernelmanager.utilities.k.c("prefBMFullUsageOn");
          this.I = flar2.exkernelmanager.utilities.k.c("prefBMFullUsageOff");
          this.K = flar2.exkernelmanager.utilities.k.d("prefBMFullTimeCharge");
          this.J = flar2.exkernelmanager.utilities.k.d("prefBMDeepSleepFullOffset");
        }
      }
      else
      {
        if (str.equals("Since now"))
        {
          flar2.exkernelmanager.utilities.k.a("prefBMCustomMarker", o());
          int i;
          if (!m()) {
            if (flar2.exkernelmanager.utilities.k.d("prefBMScrOnMarker") == -1L)
            {
              i = 0;
              label214:
              flar2.exkernelmanager.utilities.k.a("prefBMCustomTimeOn", l1);
              flar2.exkernelmanager.utilities.k.a("prefBMCustomTimeOff", flar2.exkernelmanager.utilities.k.d("prefBMScrOffTime"));
              this.F = flar2.exkernelmanager.utilities.k.d("prefBMCustomTimeOn");
              this.G = flar2.exkernelmanager.utilities.k.d("prefBMCustomTimeOff");
              flar2.exkernelmanager.utilities.k.a("prefBMCustomUsageOn", i);
              flar2.exkernelmanager.utilities.k.a("prefBMCustomUsageOff", flar2.exkernelmanager.utilities.k.c("prefBMScrOffUsage"));
              this.H = flar2.exkernelmanager.utilities.k.c("prefBMCustomUsageOn");
              this.I = flar2.exkernelmanager.utilities.k.c("prefBMCustomUsageOff");
              if (!m()) {
                break label491;
              }
            }
          }
          label491:
          for (long l2 = flar2.exkernelmanager.utilities.k.d("prefBMChargeTime") + (o() - flar2.exkernelmanager.utilities.k.d("prefBMPlugMarker"));; l2 = flar2.exkernelmanager.utilities.k.d("prefBMChargeTime"))
          {
            flar2.exkernelmanager.utilities.k.a("prefBMCustomTimeCharge", l2);
            this.K = l2;
            this.J = (SystemClock.elapsedRealtime() - SystemClock.uptimeMillis());
            flar2.exkernelmanager.utilities.k.a("prefBMDeepSleepCustomOffset", this.J);
            if (this.D.size() > 2) {
              this.D.remove(1 + this.E);
            }
            this.D.add("Since " + DateFormat.format("MMM dd hh:mm a", flar2.exkernelmanager.utilities.k.d("prefBMCustomMarker")));
            break;
            l1 = flar2.exkernelmanager.utilities.k.d("prefBMScrOnTime") + (o() - flar2.exkernelmanager.utilities.k.d("prefBMScrOnMarker"));
            i = flar2.exkernelmanager.utilities.k.c("prefBMScrOnUsage") + (flar2.exkernelmanager.utilities.k.c("prefBMScrOnLevel") - n());
            break label214;
            l1 = flar2.exkernelmanager.utilities.k.d("prefBMScrOnTime");
            i = flar2.exkernelmanager.utilities.k.c("prefBMScrOnUsage");
            break label214;
          }
        }
        this.F = flar2.exkernelmanager.utilities.k.d("prefBMCustomTimeOn");
        this.G = flar2.exkernelmanager.utilities.k.d("prefBMCustomTimeOff");
        this.H = flar2.exkernelmanager.utilities.k.c("prefBMCustomUsageOn");
        this.I = flar2.exkernelmanager.utilities.k.c("prefBMCustomUsageOff");
        this.J = flar2.exkernelmanager.utilities.k.d("prefBMDeepSleepCustomOffset");
        this.K = flar2.exkernelmanager.utilities.k.d("prefBMCustomTimeCharge");
      }
    }
  }
  
  public void onNothingSelected(AdapterView paramAdapterView) {}
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    Handler localHandler = new Handler();
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(paramMenuItem);
    case 16908332: 
      ar.a(this);
      return true;
    case 2131493232: 
      if (paramMenuItem.isChecked())
      {
        paramMenuItem.setChecked(false);
        flar2.exkernelmanager.utilities.k.a("prefDisableBatteryMon", false);
        aa.a(com.c.a.n.a(this).a("Service enabled").a(x.a));
      }
      for (;;)
      {
        this.C.setSelection(0);
        localHandler.postDelayed(new o(this), 1000L);
        return true;
        paramMenuItem.setChecked(true);
        flar2.exkernelmanager.utilities.k.a("prefDisableBatteryMon", true);
      }
    case 2131493233: 
      stopService(new Intent(this, BatteryMonitorService.class));
      flar2.exkernelmanager.utilities.k.a("prefBMDeepSleepOffset", SystemClock.elapsedRealtime() - SystemClock.uptimeMillis());
      if (!flar2.exkernelmanager.utilities.k.b("prefDisableBatteryMon").booleanValue()) {
        startService(new Intent(this, BatteryMonitorService.class));
      }
      this.C.setSelection(0);
      localHandler.postDelayed(new p(this), 1500L);
      return true;
    case 2131493238: 
      b("facebook");
      return true;
    case 2131493237: 
      b("apps.plus");
      return true;
    case 2131493239: 
      b("twitter");
      return true;
    case 2131493240: 
      b("hangout");
      return true;
    case 2131493241: 
      b("mail");
      return true;
    case 2131493242: 
      b("see_all");
      return true;
    case 2131493234: 
      startActivity(new Intent(this, UserSettingsActivity.class));
      return true;
    }
    startActivity(new Intent(this, AboutActivity.class));
    return true;
  }
  
  public void onPause()
  {
    super.onPause();
    unregisterReceiver(this.T);
  }
  
  public void onResume()
  {
    super.onResume();
    if (getResources().getBoolean(2131296265)) {
      setRequestedOrientation(1);
    }
    registerReceiver(this.T, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    l();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/BatteryActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */