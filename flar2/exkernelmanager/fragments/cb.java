package flar2.exkernelmanager.fragments;

import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;

class cb
  implements CompoundButton.OnCheckedChangeListener
{
  cb(ColorActivity paramColorActivity) {}
  
  public void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean)
  {
    k.a("pref_KcalGreyscale", paramBoolean);
    if (paramBoolean)
    {
      k.a("prefKcalSat", ColorActivity.i(this.a).a("/sys/devices/platform/kcal_ctrl.0/kcal_sat"));
      ColorActivity.i(this.a).a("128", "/sys/devices/platform/kcal_ctrl.0/kcal_sat");
      return;
    }
    ColorActivity.i(this.a).a(k.a("prefKcalSat"), "/sys/devices/platform/kcal_ctrl.0/kcal_sat");
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/cb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */