package flar2.exkernelmanager.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.c.a.aa;
import com.c.a.n;
import com.c.a.x;
import flar2.exkernelmanager.MainActivity;
import flar2.exkernelmanager.MainApp;
import flar2.exkernelmanager.slidingmenu.a.a;
import flar2.exkernelmanager.utilities.e;
import flar2.exkernelmanager.utilities.f;
import flar2.exkernelmanager.utilities.k;
import java.util.ArrayList;

public class es
  extends Fragment
{
  ViewGroup a;
  CardView b;
  CardView c;
  CardView d;
  CardView e;
  CardView f;
  private Context g;
  private e h = new e();
  private TextView i;
  private ImageView j;
  
  private void a()
  {
    Animation localAnimation1 = AnimationUtils.loadAnimation(this.g, 2130968606);
    localAnimation1.setDuration(300L);
    this.b.startAnimation(localAnimation1);
    Animation localAnimation2 = AnimationUtils.loadAnimation(this.g, 2130968606);
    localAnimation2.setDuration(350L);
    this.c.startAnimation(localAnimation2);
    Animation localAnimation3 = AnimationUtils.loadAnimation(this.g, 2130968606);
    localAnimation3.setDuration(375L);
    this.d.startAnimation(localAnimation3);
    Animation localAnimation4 = AnimationUtils.loadAnimation(this.g, 2130968606);
    localAnimation4.setDuration(400L);
    this.e.startAnimation(localAnimation4);
    Animation localAnimation5 = AnimationUtils.loadAnimation(this.g, 2130968606);
    localAnimation5.setDuration(425L);
    this.f.startAnimation(localAnimation5);
  }
  
  private void b()
  {
    f localf = new f();
    if ((localf.b("cat /proc/last_kmsg > " + "/sdcard/last_kmsg.txt").contains("No such file")) && (localf.b("cat /sys/fs/pstore/console-ramoops >> " + "/sdcard/last_kmsg.txt").contains("No such file")))
    {
      aa.a(n.a(getActivity()).a(getResources().getColor(2131361798)).a("No log available"));
      return;
    }
    aa.a(n.a(getActivity()).a(x.c).a(getResources().getColor(2131361798)).b(getResources().getColor(2131361807)).b("CLOSE").a("Saved as " + "/sdcard/last_kmsg.txt"));
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if (paramMenu != null)
    {
      paramMenu.removeItem(2131493247);
      paramMenu.removeItem(2131493246);
      paramMenu.removeItem(2131493236);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView = paramLayoutInflater.inflate(2130903093, paramViewGroup, false);
    this.a = ((ViewGroup)localView.findViewById(2131493148));
    this.g = MainApp.a();
    ((a)a.a.getAdapter()).f(a.b.indexOf("Tools"));
    setHasOptionsMenu(true);
    String[] arrayOfString = getResources().getStringArray(2131230721);
    getActivity().setTitle(arrayOfString[8]);
    this.i = ((TextView)getActivity().findViewById(2131493046));
    this.j = ((ImageView)getActivity().findViewById(2131493045));
    if (!getResources().getBoolean(2131296263))
    {
      MainActivity.o.getLayoutParams().height = getResources().getDimensionPixelSize(2131427421);
      this.j.setImageResource(2130837641);
      this.i.setText(arrayOfString[8]);
      this.b = ((CardView)localView.findViewById(2131493149));
      this.b.setOnClickListener(new et(this));
      this.c = ((CardView)localView.findViewById(2131493151));
      if (!this.h.a(flar2.exkernelmanager.r.q[new flar2.exkernelmanager.utilities.m().a(flar2.exkernelmanager.r.q)])) {
        break label417;
      }
      this.c.setOnClickListener(new eu(this));
      label251:
      this.d = ((CardView)localView.findViewById(2131493153));
      this.d.setOnClickListener(new ev(this));
      this.e = ((CardView)localView.findViewById(2131493155));
      if (!k.b("prefHTC").booleanValue()) {
        break label429;
      }
      this.e.setVisibility(8);
    }
    for (;;)
    {
      this.f = ((CardView)localView.findViewById(2131493157));
      this.f.setOnClickListener(new ex(this));
      a();
      if (k.a.getBoolean("prefFirstRunSettings", true)) {}
      return localView;
      MainActivity.o.getLayoutParams().height = new f().b(getActivity());
      MainActivity.o.setTranslationY(0.0F);
      MainActivity.p.setTranslationY(0.0F);
      this.i.setText(null);
      break;
      label417:
      this.c.setVisibility(8);
      break label251;
      label429:
      this.e.setOnClickListener(new ew(this));
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/es.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */