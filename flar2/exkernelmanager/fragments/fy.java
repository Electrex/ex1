package flar2.exkernelmanager.fragments;

import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

class fy
  extends AsyncTask
{
  private fy(ey paramey) {}
  
  protected String a(String... paramVarArgs)
  {
    try
    {
      BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(new URL(paramVarArgs[0]).openConnection().getInputStream()));
      String str = localBufferedReader.readLine();
      localBufferedReader.close();
      return str;
    }
    catch (MalformedURLException localMalformedURLException)
    {
      return null;
    }
    catch (IOException localIOException) {}
    return null;
  }
  
  protected void a(String paramString)
  {
    if (this.a.isAdded())
    {
      if (paramString == null) {
        break label34;
      }
      this.a.a(paramString);
    }
    for (;;)
    {
      ey.c(this.a).setRefreshing(false);
      return;
      label34:
      this.a.a("Not available");
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/fy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */