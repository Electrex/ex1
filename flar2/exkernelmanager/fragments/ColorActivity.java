package flar2.exkernelmanager.fragments;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ar;
import android.support.v7.app.a;
import android.support.v7.app.ab;
import android.support.v7.app.ac;
import android.support.v7.app.ad;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import com.d.a.b;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import flar2.exkernelmanager.AboutActivity;
import flar2.exkernelmanager.UserSettingsActivity;
import flar2.exkernelmanager.r;
import flar2.exkernelmanager.utilities.e;
import flar2.exkernelmanager.utilities.f;
import flar2.exkernelmanager.utilities.h;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;
import flar2.exkernelmanager.utilities.n;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class ColorActivity
  extends ad
{
  private EditText A;
  private EditText B;
  private EditText C;
  private EditText D;
  private EditText E;
  private EditText F;
  private EditText G;
  private FloatingActionsMenu H;
  private Toolbar I;
  private View J;
  private View K;
  private int L;
  private int M;
  private int N;
  private boolean O = true;
  private int P;
  boolean n = false;
  h o;
  private m p = new m();
  private f q = new f();
  private Switch r;
  private Switch s;
  private SeekBar t;
  private SeekBar u;
  private SeekBar v;
  private SeekBar w;
  private SeekBar x;
  private SeekBar y;
  private SeekBar z;
  
  private float a(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    return Math.max(paramFloat2, Math.min(paramFloat1, paramFloat3));
  }
  
  private void a(View paramView)
  {
    String str1 = ((EditText)paramView).getText().toString();
    if (str1.equals(""))
    {
      l();
      return;
    }
    switch (paramView.getId())
    {
    }
    for (;;)
    {
      ((InputMethodManager)getSystemService("input_method")).hideSoftInputFromWindow(paramView.getApplicationWindowToken(), 2);
      return;
      this.p.a(Integer.parseInt(str1), n.a);
      this.t.setProgress(Integer.parseInt(str1));
      k.a("prefRed", str1);
      continue;
      this.p.a(Integer.parseInt(str1), n.b);
      this.u.setProgress(Integer.parseInt(str1));
      k.a("prefGreen", str1);
      continue;
      this.p.a(Integer.parseInt(str1), n.c);
      this.v.setProgress(Integer.parseInt(str1));
      k.a("prefBlue", str1);
      continue;
      String str4 = Integer.toString(223 + Integer.parseInt(str1));
      this.p.a(str4, "/sys/devices/platform/kcal_ctrl.0/kcal_sat");
      this.w.setProgress(Integer.parseInt(str1));
      this.s.setChecked(false);
      k.a("prefKcalSat", str4);
      continue;
      String str3 = Integer.toString(128 + Integer.parseInt(str1));
      this.p.a(str3, "/sys/devices/platform/kcal_ctrl.0/kcal_val");
      this.x.setProgress(Integer.parseInt(str1));
      k.a("prefKcalVal", str3);
      continue;
      String str2 = Integer.toString(128 + Integer.parseInt(str1));
      this.p.a(str2, "/sys/devices/platform/kcal_ctrl.0/kcal_cont");
      this.y.setProgress(Integer.parseInt(str1));
      k.a("prefKcalCont", str2);
      continue;
      this.p.a(str1, "/sys/devices/platform/kcal_ctrl.0/kcal_hue");
      this.z.setProgress(Integer.parseInt(str1));
      k.a("prefKcalHue", str1);
    }
  }
  
  private void b(String paramString)
  {
    BufferedReader localBufferedReader = new BufferedReader(new FileReader(new File(Environment.getExternalStorageDirectory().getPath() + "/ElementalX/kcal_profiles/" + paramString)));
    ArrayList localArrayList = new ArrayList();
    for (;;)
    {
      String str = localBufferedReader.readLine();
      if (str == null) {
        break;
      }
      localArrayList.add(str);
    }
    String[] arrayOfString1 = (String[])localArrayList.toArray(new String[localArrayList.size()]);
    if (arrayOfString1 != null)
    {
      this.p.a(arrayOfString1[0], r.q[this.P]);
      if (this.O)
      {
        String[] arrayOfString2 = arrayOfString1[1].split(" ");
        this.p.a(arrayOfString2[0], "/sys/devices/platform/kcal_ctrl.0/kcal_sat");
        this.p.a(arrayOfString2[1], "/sys/devices/platform/kcal_ctrl.0/kcal_val");
        this.p.a(arrayOfString2[2], "/sys/devices/platform/kcal_ctrl.0/kcal_cont");
        this.p.a(arrayOfString2[3], "/sys/devices/platform/kcal_ctrl.0/kcal_hue");
      }
    }
  }
  
  private void k()
  {
    k.a("pref_KcalGreyscale", false);
    if (this.O)
    {
      k.a("prefRed", "256");
      k.a("prefGreen", "256");
      k.a("prefBlue", "256");
      if (this.O)
      {
        k.a("prefKcalSat", "255");
        k.a("prefKcalVal", "255");
        k.a("prefKcalCont", "255");
        k.a("prefKcalHue", "0");
      }
      if (!this.O) {
        break label218;
      }
      this.p.a(256, n.a);
      this.p.a(256, n.b);
      this.p.a(256, n.c);
    }
    for (;;)
    {
      if (this.O)
      {
        this.p.a("255", "/sys/devices/platform/kcal_ctrl.0/kcal_sat");
        this.p.a("255", "/sys/devices/platform/kcal_ctrl.0/kcal_val");
        this.p.a("255", "/sys/devices/platform/kcal_ctrl.0/kcal_cont");
        this.p.a("0", "/sys/devices/platform/kcal_ctrl.0/kcal_hue");
        this.s.setChecked(false);
      }
      l();
      return;
      k.a("prefRed", "255");
      k.a("prefGreen", "255");
      k.a("prefBlue", "255");
      break;
      label218:
      this.p.a(255, n.a);
      this.p.a(255, n.b);
      this.p.a(255, n.c);
    }
  }
  
  private void l()
  {
    this.p.a("1", "/sys/devices/platform/kcal_ctrl.0/kcal_enable");
    this.p.a("35", "/sys/devices/platform/kcal_ctrl.0/kcal_min");
    if (k.b("prefKcalACCBoot").booleanValue()) {
      this.r.setChecked(true);
    }
    if ((this.O) && (this.p.a("/sys/devices/platform/kcal_ctrl.0/kcal_sat").equals("128"))) {
      this.s.setChecked(true);
    }
    this.t.setProgress(Integer.valueOf(this.p.a(n.a)).intValue());
    this.u.setProgress(Integer.valueOf(this.p.a(n.b)).intValue());
    this.v.setProgress(Integer.valueOf(this.p.a(n.c)).intValue());
    if (this.O)
    {
      this.w.setProgress(65313 + Integer.valueOf(this.p.a("/sys/devices/platform/kcal_ctrl.0/kcal_sat")).intValue());
      this.x.setProgress(-127 + Integer.valueOf(this.p.a("/sys/devices/platform/kcal_ctrl.0/kcal_val")).intValue());
      this.y.setProgress(-127 + Integer.valueOf(this.p.a("/sys/devices/platform/kcal_ctrl.0/kcal_cont")).intValue());
      this.z.setProgress(Integer.valueOf(this.p.a("/sys/devices/platform/kcal_ctrl.0/kcal_hue")).intValue());
    }
    this.A.setText(Integer.toString(this.t.getProgress()));
    this.B.setText(Integer.toString(this.u.getProgress()));
    this.C.setText(Integer.toString(this.v.getProgress()));
    if (this.O)
    {
      this.D.setText(Integer.toString(this.w.getProgress()));
      this.E.setText(Integer.toString(this.x.getProgress()));
      this.F.setText(Integer.toString(this.y.getProgress()));
      this.G.setText(Integer.toString(this.z.getProgress()));
    }
  }
  
  private void m()
  {
    ac localac = new ac(this);
    localac.a(getString(2131624199));
    localac.b(2131623999, null);
    EditText localEditText = new EditText(this);
    localEditText.setHint("myprofile");
    localEditText.setMaxLines(1);
    localEditText.setInputType(1);
    localac.a(localEditText);
    localac.a(2131624170, new bq(this, localEditText));
    localac.a(new br(this));
    ab localab = localac.a();
    localab.getWindow().setSoftInputMode(5);
    localab.show();
    localab.getWindow().setLayout(800, -2);
  }
  
  private void n()
  {
    String[] arrayOfString = new File(Environment.getExternalStorageDirectory().getPath() + "/ElementalX/kcal_profiles").list();
    if (arrayOfString == null)
    {
      ac localac1 = new ac(this);
      localac1.a(getString(2131624203));
      localac1.a(2131624170, null);
      localac1.b(getString(2131624163));
      localac1.a(new bs(this));
      localac1.a().show();
      return;
    }
    View localView = getLayoutInflater().inflate(2130903097, null);
    ac localac2 = new ac(this);
    localac2.a(localView);
    localac2.a("Load profile");
    localac2.a(2131624170, null);
    ImageView localImageView = (ImageView)localView.findViewById(2131493182);
    if (!this.O)
    {
      localImageView.setVisibility(8);
      localac2.a(new bu(this));
      ab localab = localac2.a();
      ArrayAdapter localArrayAdapter = new ArrayAdapter(this, 17367043, 16908308, arrayOfString);
      ListView localListView = (ListView)localView.findViewById(2131493183);
      localListView.setAdapter(localArrayAdapter);
      localListView.setOnItemClickListener(new bv(this, arrayOfString, localab));
      localab.show();
      localab.getWindow().setLayout(-1, -2);
      return;
    }
    String str = k.a("prefKcalTestImage");
    if (str.equals("colorscale")) {
      localImageView.setImageResource(2130837577);
    }
    for (;;)
    {
      localImageView.setOnClickListener(new bt(this, localImageView));
      break;
      if (str.equals("healthyfood")) {
        localImageView.setImageResource(2130837594);
      } else if (str.equals("grayscale")) {
        localImageView.setImageResource(2130837593);
      } else if (str.equals("macbeth")) {
        localImageView.setImageResource(2130837655);
      } else if (str.equals("babies")) {
        localImageView.setImageResource(2130837564);
      }
    }
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    this.o.b().onTouchEvent(paramMotionEvent);
    return super.dispatchTouchEvent(paramMotionEvent);
  }
  
  public void onBackPressed()
  {
    super.onBackPressed();
    overridePendingTransition(0, 2130968604);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903070);
    overridePendingTransition(2130968605, 17432577);
    if (!new e().a("/sys/devices/platform/kcal_ctrl.0/kcal_sat")) {
      this.O = false;
    }
    this.P = this.p.a(r.q);
    k.a("prefkcalPath", this.P);
    this.I = ((Toolbar)findViewById(2131492951));
    a(this.I);
    setTitle(getString(2131624268));
    g().a(true);
    this.o = new at(this, this);
    findViewById(2131492990).setOnTouchListener(this.o);
    this.J = findViewById(2131493028);
    this.K = findViewById(2131493030);
    this.M = getResources().getDimensionPixelSize(2131427422);
    this.N = (-this.M);
    label349:
    ImageView localImageView;
    String str;
    if (((getResources().getBoolean(2131296267)) || (getResources().getBoolean(2131296266))) && (getResources().getBoolean(2131296263)))
    {
      View localView1 = findViewById(2131492992);
      localView1.getViewTreeObserver().addOnScrollChangedListener(new be(this, localView1));
      this.H = ((FloatingActionsMenu)findViewById(2131493024));
      localView1.setOnTouchListener(new b(this.H, 2130968590, 2130968589));
      ((FloatingActionButton)findViewById(2131493025)).setOnClickListener(new bp(this));
      ((FloatingActionButton)findViewById(2131493026)).setOnClickListener(new bw(this));
      if (!k.a("prefDeviceName").equals(getString(2131624155))) {
        break label1272;
      }
      this.q.a(this, "Nexus6");
      if (!k.e("prefKcalTestImage")) {
        k.a("prefKcalTestImage", "macbeth");
      }
      localImageView = (ImageView)findViewById(2131493007);
      str = k.a("prefKcalTestImage");
      if (!str.equals("macbeth")) {
        break label1305;
      }
      localImageView.setImageResource(2130837655);
    }
    for (;;)
    {
      localImageView.setOnClickListener(new bx(this, localImageView));
      InputMethodManager localInputMethodManager = (InputMethodManager)getSystemService("input_method");
      View localView2 = findViewById(2131492991);
      localView2.getViewTreeObserver().addOnGlobalLayoutListener(new by(this, localView2));
      ((RelativeLayout)findViewById(2131492993)).requestFocus();
      this.r = ((Switch)findViewById(2131493029));
      this.s = ((Switch)findViewById(2131492994));
      if (!this.O) {
        this.s.setVisibility(4);
      }
      this.t = ((SeekBar)findViewById(2131492997));
      this.u = ((SeekBar)findViewById(2131493001));
      this.v = ((SeekBar)findViewById(2131493005));
      this.w = ((SeekBar)findViewById(2131493010));
      this.x = ((SeekBar)findViewById(2131493014));
      this.y = ((SeekBar)findViewById(2131493018));
      this.z = ((SeekBar)findViewById(2131493022));
      if (!this.O)
      {
        this.t.setMax(255);
        this.u.setMax(255);
        this.v.setMax(255);
        this.w.setVisibility(8);
        this.x.setVisibility(8);
        this.y.setVisibility(8);
        this.z.setVisibility(8);
        findViewById(2131493008).setVisibility(8);
        findViewById(2131493012).setVisibility(8);
        findViewById(2131493020).setVisibility(8);
        findViewById(2131493016).setVisibility(8);
      }
      this.A = ((EditText)findViewById(2131492998));
      this.B = ((EditText)findViewById(2131493002));
      this.C = ((EditText)findViewById(2131493006));
      this.D = ((EditText)findViewById(2131493011));
      this.E = ((EditText)findViewById(2131493015));
      this.F = ((EditText)findViewById(2131493019));
      this.G = ((EditText)findViewById(2131493023));
      if (!this.O)
      {
        this.D.setVisibility(8);
        this.E.setVisibility(8);
        this.F.setVisibility(8);
        this.G.setVisibility(8);
      }
      l();
      this.r.setOnCheckedChangeListener(new ca(this));
      if (this.O) {
        this.s.setOnCheckedChangeListener(new cb(this));
      }
      this.t.setOnSeekBarChangeListener(new cc(this));
      this.u.setOnSeekBarChangeListener(new au(this));
      this.v.setOnSeekBarChangeListener(new av(this));
      if (this.O)
      {
        this.w.setOnSeekBarChangeListener(new aw(this));
        this.x.setOnSeekBarChangeListener(new ax(this));
        this.y.setOnSeekBarChangeListener(new ay(this));
        this.z.setOnSeekBarChangeListener(new az(this));
      }
      this.A.setOnFocusChangeListener(new ba(this, localInputMethodManager));
      this.C.setOnFocusChangeListener(new bb(this, localInputMethodManager));
      this.B.setOnFocusChangeListener(new bc(this, localInputMethodManager));
      if (this.O)
      {
        this.D.setOnFocusChangeListener(new bd(this, localInputMethodManager));
        this.E.setOnFocusChangeListener(new bf(this, localInputMethodManager));
        this.F.setOnFocusChangeListener(new bg(this, localInputMethodManager));
        this.G.setOnFocusChangeListener(new bh(this, localInputMethodManager));
      }
      this.A.setOnClickListener(new bi(this));
      this.B.setOnClickListener(new bj(this));
      this.C.setOnClickListener(new bk(this));
      if (this.O)
      {
        this.D.setOnClickListener(new bl(this));
        this.F.setOnClickListener(new bm(this));
        this.E.setOnClickListener(new bn(this));
        this.G.setOnClickListener(new bo(this));
      }
      return;
      this.L = 0;
      break;
      label1272:
      if (!k.a("prefDeviceName").equals(getString(2131624157))) {
        break label349;
      }
      this.q.a(this, "Nexus7");
      break label349;
      label1305:
      if (str.equals("healthyfood")) {
        localImageView.setImageResource(2130837594);
      } else if (str.equals("grayscale")) {
        localImageView.setImageResource(2130837593);
      } else if (str.equals("colorscale")) {
        localImageView.setImageResource(2130837577);
      } else if (str.equals("babies")) {
        localImageView.setImageResource(2130837564);
      }
    }
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131755009, paramMenu);
    return true;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(paramMenuItem);
    case 16908332: 
      ar.a(this);
      return true;
    case 2131493243: 
      k();
      return false;
    case 2131493234: 
      startActivity(new Intent(this, UserSettingsActivity.class));
      return true;
    }
    startActivity(new Intent(this, AboutActivity.class));
    return true;
  }
  
  public void onResume()
  {
    super.onResume();
    l();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/ColorActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */