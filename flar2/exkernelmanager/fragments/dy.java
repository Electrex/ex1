package flar2.exkernelmanager.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import flar2.exkernelmanager.MainActivity;
import flar2.exkernelmanager.MainApp;
import flar2.exkernelmanager.TutorialActivity;
import flar2.exkernelmanager.r;
import flar2.exkernelmanager.slidingmenu.a.a;
import flar2.exkernelmanager.utilities.e;
import flar2.exkernelmanager.utilities.f;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;
import java.io.File;
import java.util.ArrayList;

public class dy
  extends Fragment
{
  private View A;
  private View B;
  private View C;
  private View D;
  private View E;
  private boolean F = true;
  private boolean G = true;
  private String H;
  private m I = new m();
  private e J = new e();
  private int K = 0;
  private int L = 0;
  private ViewGroup M;
  private SwipeRefreshLayout N;
  private Context O;
  private TypedValue P = new TypedValue();
  private TextView Q;
  private ImageView R;
  private Handler S;
  private Runnable T = new dz(this);
  private BroadcastReceiver U = new ei(this);
  SharedPreferences a;
  private TextView b;
  private TextView c;
  private TextView d;
  private TextView e;
  private TextView f;
  private TextView g;
  private TextView h;
  private TextView i;
  private TextView j;
  private TextView k;
  private TextView l;
  private TextView m;
  private TextView n;
  private TextView o;
  private TextView p;
  private TextView q;
  private TextView r;
  private TextView s;
  private TextView t;
  private TextView u;
  private TextView v;
  private TextView w;
  private TextView x;
  private View y;
  private View z;
  
  private void a()
  {
    Animation localAnimation1 = AnimationUtils.loadAnimation(this.O, 2130968606);
    localAnimation1.setDuration(300L);
    this.y.startAnimation(localAnimation1);
    Animation localAnimation2 = AnimationUtils.loadAnimation(this.O, 2130968606);
    localAnimation2.setDuration(350L);
    this.z.startAnimation(localAnimation2);
    Animation localAnimation3 = AnimationUtils.loadAnimation(this.O, 2130968606);
    localAnimation3.setDuration(375L);
    this.A.startAnimation(localAnimation3);
    Animation localAnimation4 = AnimationUtils.loadAnimation(this.O, 2130968606);
    localAnimation4.setDuration(400L);
    this.B.startAnimation(localAnimation4);
    Animation localAnimation5 = AnimationUtils.loadAnimation(this.O, 2130968606);
    localAnimation5.setDuration(425L);
    this.C.startAnimation(localAnimation5);
    Animation localAnimation6 = AnimationUtils.loadAnimation(this.O, 2130968606);
    localAnimation6.setDuration(450L);
    this.D.startAnimation(localAnimation6);
    Animation localAnimation7 = AnimationUtils.loadAnimation(this.O, 2130968606);
    localAnimation7.setDuration(485L);
    this.E.startAnimation(localAnimation7);
  }
  
  private void a(int paramInt1, int paramInt2)
  {
    this.m.setText(paramInt2 + "%");
    int i1;
    if (paramInt2 > 25)
    {
      this.m.setTextColor(-16711936);
      i1 = paramInt1 / 10;
      if (!this.H.equals("2")) {
        break label157;
      }
      double d1 = 32.0D + 1.8D * i1;
      this.l.setText((int)d1 + "°F");
      label98:
      if (i1 >= 40) {
        break label198;
      }
      this.l.setTextColor(-16711936);
    }
    label157:
    label198:
    do
    {
      return;
      if ((paramInt2 <= 25) && (paramInt2 >= 14))
      {
        this.m.setTextColor(65280);
        break;
      }
      if (paramInt2 >= 14) {
        break;
      }
      this.m.setTextColor(-65536);
      break;
      if (!this.H.equals("1")) {
        break label98;
      }
      this.l.setText(i1 + "°C");
      break label98;
      if ((i1 >= 40) && (i1 <= 52))
      {
        this.l.setTextColor(65280);
        return;
      }
    } while (i1 <= 52);
    this.l.setTextColor(-65536);
  }
  
  private void a(String paramString)
  {
    if (!a.b.contains(paramString)) {
      return;
    }
    MainApp.a = a.b.indexOf(paramString);
    Object localObject;
    if (paramString.equals("Graphics")) {
      localObject = new cz();
    }
    for (;;)
    {
      if (localObject != null) {
        getFragmentManager().beginTransaction().setCustomAnimations(2131034112, 2131034113).replace(2131493041, (Fragment)localObject).commit();
      }
      ((a)a.a.getAdapter()).f(MainApp.a);
      return;
      if (paramString.equals("Wake"))
      {
        localObject = new gf();
      }
      else if (paramString.equals("Miscellaneous"))
      {
        localObject = new dk();
      }
      else
      {
        boolean bool = paramString.equals("Update");
        localObject = null;
        if (bool) {
          localObject = new ey();
        }
      }
    }
  }
  
  private void b()
  {
    l();
    j();
    k();
  }
  
  private void c()
  {
    i();
    h();
    g();
    f();
    e();
    d();
  }
  
  private void d()
  {
    int i1 = this.I.a(r.e);
    this.x.setText(this.O.getString(2131624042) + this.J.c(this.I.a(r.e[i1])));
  }
  
  private void e()
  {
    for (;;)
    {
      try
      {
        if (this.J.a("/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"))
        {
          this.u.setText(this.O.getString(2131624034) + this.J.c(this.I.a("/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq")));
          this.t.setText(this.O.getString(2131624033) + this.I.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"));
          return;
        }
        if (this.J.a("/sys/module/cpu_tegra/parameters/cpu_user_cap")) {
          this.u.setText(this.O.getString(2131624034) + this.J.c(this.I.a("/sys/module/cpu_tegra/parameters/cpu_user_cap")));
        } else {
          this.u.setText(this.O.getString(2131624034) + this.J.c(this.I.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq")));
        }
      }
      catch (NullPointerException localNullPointerException)
      {
        this.u.setText(this.O.getString(2131624034));
        return;
      }
    }
  }
  
  private void f()
  {
    String str = this.I.a("/sys/block/mmcblk0/queue/scheduler");
    if (str == null) {
      this.v.setText(this.O.getString(2131624043) + " NA");
    }
    for (;;)
    {
      this.w.setText(this.O.getString(2131624047) + this.I.a("/sys/block/mmcblk0/queue/read_ahead_kb"));
      return;
      try
      {
        this.v.setText(this.O.getString(2131624043) + str.substring(1 + str.indexOf("["), str.indexOf("]")).trim());
      }
      catch (IndexOutOfBoundsException localIndexOutOfBoundsException)
      {
        this.v.setText(this.O.getString(2131624043) + " NA");
      }
    }
  }
  
  private void g()
  {
    int i1 = this.I.a(r.m);
    int i2 = this.I.a(r.n);
    if (!this.J.a(r.m[i1])) {
      this.r.setText(this.O.getString(2131624050));
    }
    while (!this.J.a(r.n[i2]))
    {
      this.s.setText(this.O.getString(2131624039));
      return;
      if (this.I.a(r.m[i1]).equals("0")) {
        this.r.setText(this.O.getString(2131624048));
      } else {
        this.r.setText(this.O.getString(2131624049));
      }
    }
    if (this.I.a(r.n[i2]).equals("0"))
    {
      this.s.setText(this.O.getString(2131624037));
      return;
    }
    this.s.setText(this.O.getString(2131624038));
  }
  
  /* Error */
  private void h()
  {
    // Byte code:
    //   0: lconst_0
    //   1: lstore_1
    //   2: new 410	java/io/BufferedReader
    //   5: dup
    //   6: new 412	java/io/FileReader
    //   9: dup
    //   10: ldc_w 414
    //   13: invokespecial 416	java/io/FileReader:<init>	(Ljava/lang/String;)V
    //   16: sipush 512
    //   19: invokespecial 419	java/io/BufferedReader:<init>	(Ljava/io/Reader;I)V
    //   22: astore_3
    //   23: aload_3
    //   24: invokevirtual 422	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   27: astore 11
    //   29: aload_3
    //   30: invokevirtual 422	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   33: astore 12
    //   35: aload_3
    //   36: invokevirtual 422	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   39: pop
    //   40: aload_3
    //   41: invokevirtual 422	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   44: astore 14
    //   46: aload 11
    //   48: ifnull +227 -> 275
    //   51: aload 12
    //   53: ifnull +222 -> 275
    //   56: aload 14
    //   58: ifnull +217 -> 275
    //   61: aload 11
    //   63: ldc_w 424
    //   66: invokevirtual 428	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   69: astore 15
    //   71: aload 15
    //   73: arraylength
    //   74: iconst_3
    //   75: if_icmpne +194 -> 269
    //   78: aload 15
    //   80: iconst_1
    //   81: aaload
    //   82: invokestatic 434	java/lang/Long:parseLong	(Ljava/lang/String;)J
    //   85: ldc2_w 435
    //   88: ldiv
    //   89: lstore 7
    //   91: aload 12
    //   93: ldc_w 424
    //   96: invokevirtual 428	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   99: astore 17
    //   101: aload 17
    //   103: arraylength
    //   104: iconst_3
    //   105: if_icmpne +158 -> 263
    //   108: aload 17
    //   110: iconst_1
    //   111: aaload
    //   112: invokestatic 434	java/lang/Long:parseLong	(Ljava/lang/String;)J
    //   115: ldc2_w 435
    //   118: ldiv
    //   119: lstore 5
    //   121: aload 14
    //   123: ldc_w 424
    //   126: invokevirtual 428	java/lang/String:split	(Ljava/lang/String;)[Ljava/lang/String;
    //   129: astore 19
    //   131: aload 19
    //   133: arraylength
    //   134: iconst_3
    //   135: if_icmpne +15 -> 150
    //   138: aload 19
    //   140: iconst_1
    //   141: aaload
    //   142: invokestatic 434	java/lang/Long:parseLong	(Ljava/lang/String;)J
    //   145: ldc2_w 435
    //   148: ldiv
    //   149: lstore_1
    //   150: lload_1
    //   151: lload 5
    //   153: ladd
    //   154: lstore 9
    //   156: aload_0
    //   157: getfield 438	flar2/exkernelmanager/fragments/dy:n	Landroid/widget/TextView;
    //   160: new 159	java/lang/StringBuilder
    //   163: dup
    //   164: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   167: aload_0
    //   168: getfield 107	flar2/exkernelmanager/fragments/dy:O	Landroid/content/Context;
    //   171: ldc_w 439
    //   174: invokevirtual 331	android/content/Context:getString	(I)Ljava/lang/String;
    //   177: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   180: lload 9
    //   182: invokevirtual 442	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   185: ldc_w 444
    //   188: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   191: invokevirtual 173	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   194: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   197: aload_0
    //   198: getfield 446	flar2/exkernelmanager/fragments/dy:o	Landroid/widget/TextView;
    //   201: new 159	java/lang/StringBuilder
    //   204: dup
    //   205: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   208: aload_0
    //   209: getfield 107	flar2/exkernelmanager/fragments/dy:O	Landroid/content/Context;
    //   212: ldc_w 447
    //   215: invokevirtual 331	android/content/Context:getString	(I)Ljava/lang/String;
    //   218: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   221: lload 7
    //   223: invokevirtual 442	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   226: ldc_w 444
    //   229: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   232: invokevirtual 173	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   235: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   238: return
    //   239: astore 4
    //   241: lload_1
    //   242: lstore 5
    //   244: lload_1
    //   245: lstore 7
    //   247: goto -97 -> 150
    //   250: astore 16
    //   252: lload_1
    //   253: lstore 5
    //   255: goto -105 -> 150
    //   258: astore 18
    //   260: goto -110 -> 150
    //   263: lload_1
    //   264: lstore 5
    //   266: goto -145 -> 121
    //   269: lload_1
    //   270: lstore 7
    //   272: goto -181 -> 91
    //   275: lload_1
    //   276: lstore 5
    //   278: lload_1
    //   279: lstore 7
    //   281: goto -131 -> 150
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	284	0	this	dy
    //   1	278	1	l1	long
    //   22	19	3	localBufferedReader	java.io.BufferedReader
    //   239	1	4	localIOException1	java.io.IOException
    //   119	158	5	l2	long
    //   89	191	7	l3	long
    //   154	27	9	l4	long
    //   27	35	11	str1	String
    //   33	59	12	str2	String
    //   44	78	14	str3	String
    //   69	10	15	arrayOfString1	String[]
    //   250	1	16	localIOException2	java.io.IOException
    //   99	10	17	arrayOfString2	String[]
    //   258	1	18	localIOException3	java.io.IOException
    //   129	10	19	arrayOfString3	String[]
    // Exception table:
    //   from	to	target	type
    //   2	46	239	java/io/IOException
    //   61	91	239	java/io/IOException
    //   91	121	250	java/io/IOException
    //   121	150	258	java/io/IOException
  }
  
  private void i()
  {
    int i1 = (int)(SystemClock.elapsedRealtime() - SystemClock.uptimeMillis());
    int i2 = (int)SystemClock.elapsedRealtime();
    String str1 = i1 / 1000 % 60 + "";
    String str2 = i1 / 60000 % 60 + "";
    String str3 = i1 / 3600000 % 24 + "";
    String str4 = i1 / 86400000 + "";
    StringBuilder localStringBuilder1 = new StringBuilder();
    if (!str4.equals("0")) {
      localStringBuilder1.append(str4).append("d ");
    }
    if (!str3.equals("0")) {
      localStringBuilder1.append(str3).append("h ");
    }
    if (!str2.equals("0")) {
      localStringBuilder1.append(str2).append("m ");
    }
    localStringBuilder1.append(str1).append("s");
    String str5 = localStringBuilder1.toString();
    String str6 = i2 / 1000 % 60 + "";
    String str7 = i2 / 60000 % 60 + "";
    String str8 = i2 / 3600000 % 24 + "";
    String str9 = i2 / 86400000 + "";
    StringBuilder localStringBuilder2 = new StringBuilder();
    if (!str9.equals("0")) {
      localStringBuilder2.append(str9).append("d ");
    }
    if (!str8.equals("0")) {
      localStringBuilder2.append(str8).append("h ");
    }
    if (!str7.equals("0")) {
      localStringBuilder2.append(str7).append("m ");
    }
    localStringBuilder2.append(str6).append("s");
    String str10 = localStringBuilder2.toString();
    float f1 = 100.0F * (i1 / i2);
    this.p.setText(this.O.getString(2131624036) + str5 + " (" + (int)f1 + "%)");
    this.q.setText(this.O.getString(2131624051) + str10);
  }
  
  /* Error */
  private void j()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 79	flar2/exkernelmanager/fragments/dy:I	Lflar2/exkernelmanager/utilities/m;
    //   4: ldc_w 483
    //   7: invokevirtual 334	flar2/exkernelmanager/utilities/m:a	(Ljava/lang/String;)Ljava/lang/String;
    //   10: astore_1
    //   11: aload_1
    //   12: ldc_w 485
    //   15: invokevirtual 194	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   18: ifne +710 -> 728
    //   21: aload_1
    //   22: ifnull +706 -> 728
    //   25: aload_0
    //   26: getfield 487	flar2/exkernelmanager/fragments/dy:c	Landroid/widget/TextView;
    //   29: new 159	java/lang/StringBuilder
    //   32: dup
    //   33: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   36: aload_1
    //   37: invokevirtual 390	java/lang/String:trim	()Ljava/lang/String;
    //   40: iconst_0
    //   41: bipush -3
    //   43: aload_1
    //   44: invokevirtual 490	java/lang/String:length	()I
    //   47: iadd
    //   48: invokevirtual 387	java/lang/String:substring	(II)Ljava/lang/String;
    //   51: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   54: ldc_w 492
    //   57: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   60: invokevirtual 173	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   63: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   66: aload_0
    //   67: getfield 487	flar2/exkernelmanager/fragments/dy:c	Landroid/widget/TextView;
    //   70: ldc_w 493
    //   73: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   76: aload_0
    //   77: getfield 79	flar2/exkernelmanager/fragments/dy:I	Lflar2/exkernelmanager/utilities/m;
    //   80: ldc_w 495
    //   83: invokevirtual 334	flar2/exkernelmanager/utilities/m:a	(Ljava/lang/String;)Ljava/lang/String;
    //   86: astore_2
    //   87: aload_2
    //   88: ldc_w 485
    //   91: invokevirtual 194	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   94: ifne +682 -> 776
    //   97: aload_2
    //   98: ifnull +678 -> 776
    //   101: aload_0
    //   102: getfield 497	flar2/exkernelmanager/fragments/dy:d	Landroid/widget/TextView;
    //   105: new 159	java/lang/StringBuilder
    //   108: dup
    //   109: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   112: aload_2
    //   113: invokevirtual 390	java/lang/String:trim	()Ljava/lang/String;
    //   116: iconst_0
    //   117: bipush -3
    //   119: aload_2
    //   120: invokevirtual 490	java/lang/String:length	()I
    //   123: iadd
    //   124: invokevirtual 387	java/lang/String:substring	(II)Ljava/lang/String;
    //   127: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   130: ldc_w 492
    //   133: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   136: invokevirtual 173	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   139: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   142: aload_0
    //   143: getfield 497	flar2/exkernelmanager/fragments/dy:d	Landroid/widget/TextView;
    //   146: ldc_w 493
    //   149: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   152: aload_0
    //   153: getfield 72	flar2/exkernelmanager/fragments/dy:F	Z
    //   156: ifeq +165 -> 321
    //   159: aload_0
    //   160: getfield 79	flar2/exkernelmanager/fragments/dy:I	Lflar2/exkernelmanager/utilities/m;
    //   163: ldc_w 499
    //   166: invokevirtual 334	flar2/exkernelmanager/utilities/m:a	(Ljava/lang/String;)Ljava/lang/String;
    //   169: astore 11
    //   171: aload 11
    //   173: ldc_w 485
    //   176: invokevirtual 194	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   179: ifne +645 -> 824
    //   182: aload 11
    //   184: ifnull +640 -> 824
    //   187: aload_0
    //   188: getfield 501	flar2/exkernelmanager/fragments/dy:e	Landroid/widget/TextView;
    //   191: new 159	java/lang/StringBuilder
    //   194: dup
    //   195: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   198: aload 11
    //   200: invokevirtual 390	java/lang/String:trim	()Ljava/lang/String;
    //   203: iconst_0
    //   204: bipush -3
    //   206: aload 11
    //   208: invokevirtual 490	java/lang/String:length	()I
    //   211: iadd
    //   212: invokevirtual 387	java/lang/String:substring	(II)Ljava/lang/String;
    //   215: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   218: ldc_w 492
    //   221: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   224: invokevirtual 173	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   227: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   230: aload_0
    //   231: getfield 501	flar2/exkernelmanager/fragments/dy:e	Landroid/widget/TextView;
    //   234: ldc_w 493
    //   237: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   240: aload_0
    //   241: getfield 79	flar2/exkernelmanager/fragments/dy:I	Lflar2/exkernelmanager/utilities/m;
    //   244: ldc_w 503
    //   247: invokevirtual 334	flar2/exkernelmanager/utilities/m:a	(Ljava/lang/String;)Ljava/lang/String;
    //   250: astore 12
    //   252: aload 12
    //   254: ldc_w 485
    //   257: invokevirtual 194	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   260: ifne +612 -> 872
    //   263: aload 12
    //   265: ifnull +607 -> 872
    //   268: aload_0
    //   269: getfield 505	flar2/exkernelmanager/fragments/dy:f	Landroid/widget/TextView;
    //   272: new 159	java/lang/StringBuilder
    //   275: dup
    //   276: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   279: aload 12
    //   281: invokevirtual 390	java/lang/String:trim	()Ljava/lang/String;
    //   284: iconst_0
    //   285: bipush -3
    //   287: aload 12
    //   289: invokevirtual 490	java/lang/String:length	()I
    //   292: iadd
    //   293: invokevirtual 387	java/lang/String:substring	(II)Ljava/lang/String;
    //   296: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   299: ldc_w 492
    //   302: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   305: invokevirtual 173	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   308: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   311: aload_0
    //   312: getfield 505	flar2/exkernelmanager/fragments/dy:f	Landroid/widget/TextView;
    //   315: ldc_w 493
    //   318: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   321: aload_0
    //   322: getfield 74	flar2/exkernelmanager/fragments/dy:G	Z
    //   325: ifeq +402 -> 727
    //   328: aload_0
    //   329: getfield 487	flar2/exkernelmanager/fragments/dy:c	Landroid/widget/TextView;
    //   332: ldc_w 506
    //   335: invokevirtual 510	android/widget/TextView:setTextSize	(F)V
    //   338: aload_0
    //   339: getfield 497	flar2/exkernelmanager/fragments/dy:d	Landroid/widget/TextView;
    //   342: ldc_w 506
    //   345: invokevirtual 510	android/widget/TextView:setTextSize	(F)V
    //   348: aload_0
    //   349: getfield 501	flar2/exkernelmanager/fragments/dy:e	Landroid/widget/TextView;
    //   352: ldc_w 506
    //   355: invokevirtual 510	android/widget/TextView:setTextSize	(F)V
    //   358: aload_0
    //   359: getfield 505	flar2/exkernelmanager/fragments/dy:f	Landroid/widget/TextView;
    //   362: ldc_w 506
    //   365: invokevirtual 510	android/widget/TextView:setTextSize	(F)V
    //   368: aload_0
    //   369: getfield 512	flar2/exkernelmanager/fragments/dy:g	Landroid/widget/TextView;
    //   372: ldc_w 506
    //   375: invokevirtual 510	android/widget/TextView:setTextSize	(F)V
    //   378: aload_0
    //   379: getfield 514	flar2/exkernelmanager/fragments/dy:h	Landroid/widget/TextView;
    //   382: ldc_w 506
    //   385: invokevirtual 510	android/widget/TextView:setTextSize	(F)V
    //   388: aload_0
    //   389: getfield 516	flar2/exkernelmanager/fragments/dy:i	Landroid/widget/TextView;
    //   392: ldc_w 506
    //   395: invokevirtual 510	android/widget/TextView:setTextSize	(F)V
    //   398: aload_0
    //   399: getfield 518	flar2/exkernelmanager/fragments/dy:j	Landroid/widget/TextView;
    //   402: ldc_w 506
    //   405: invokevirtual 510	android/widget/TextView:setTextSize	(F)V
    //   408: aload_0
    //   409: getfield 79	flar2/exkernelmanager/fragments/dy:I	Lflar2/exkernelmanager/utilities/m;
    //   412: ldc_w 520
    //   415: invokevirtual 334	flar2/exkernelmanager/utilities/m:a	(Ljava/lang/String;)Ljava/lang/String;
    //   418: astore_3
    //   419: aload_3
    //   420: ldc_w 485
    //   423: invokevirtual 194	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   426: ifne +494 -> 920
    //   429: aload_3
    //   430: ifnull +490 -> 920
    //   433: aload_0
    //   434: getfield 512	flar2/exkernelmanager/fragments/dy:g	Landroid/widget/TextView;
    //   437: new 159	java/lang/StringBuilder
    //   440: dup
    //   441: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   444: aload_3
    //   445: invokevirtual 390	java/lang/String:trim	()Ljava/lang/String;
    //   448: iconst_0
    //   449: bipush -3
    //   451: aload_3
    //   452: invokevirtual 490	java/lang/String:length	()I
    //   455: iadd
    //   456: invokevirtual 387	java/lang/String:substring	(II)Ljava/lang/String;
    //   459: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   462: ldc_w 492
    //   465: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   468: invokevirtual 173	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   471: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   474: aload_0
    //   475: getfield 512	flar2/exkernelmanager/fragments/dy:g	Landroid/widget/TextView;
    //   478: ldc_w 493
    //   481: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   484: aload_0
    //   485: getfield 79	flar2/exkernelmanager/fragments/dy:I	Lflar2/exkernelmanager/utilities/m;
    //   488: ldc_w 522
    //   491: invokevirtual 334	flar2/exkernelmanager/utilities/m:a	(Ljava/lang/String;)Ljava/lang/String;
    //   494: astore 4
    //   496: aload 4
    //   498: ldc_w 485
    //   501: invokevirtual 194	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   504: ifne +464 -> 968
    //   507: aload 4
    //   509: ifnull +459 -> 968
    //   512: aload_0
    //   513: getfield 514	flar2/exkernelmanager/fragments/dy:h	Landroid/widget/TextView;
    //   516: new 159	java/lang/StringBuilder
    //   519: dup
    //   520: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   523: aload 4
    //   525: invokevirtual 390	java/lang/String:trim	()Ljava/lang/String;
    //   528: iconst_0
    //   529: bipush -3
    //   531: aload 4
    //   533: invokevirtual 490	java/lang/String:length	()I
    //   536: iadd
    //   537: invokevirtual 387	java/lang/String:substring	(II)Ljava/lang/String;
    //   540: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   543: ldc_w 492
    //   546: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   549: invokevirtual 173	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   552: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   555: aload_0
    //   556: getfield 514	flar2/exkernelmanager/fragments/dy:h	Landroid/widget/TextView;
    //   559: ldc_w 493
    //   562: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   565: aload_0
    //   566: getfield 79	flar2/exkernelmanager/fragments/dy:I	Lflar2/exkernelmanager/utilities/m;
    //   569: ldc_w 524
    //   572: invokevirtual 334	flar2/exkernelmanager/utilities/m:a	(Ljava/lang/String;)Ljava/lang/String;
    //   575: astore 5
    //   577: aload 5
    //   579: ldc_w 485
    //   582: invokevirtual 194	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   585: ifne +431 -> 1016
    //   588: aload 5
    //   590: ifnull +426 -> 1016
    //   593: aload_0
    //   594: getfield 516	flar2/exkernelmanager/fragments/dy:i	Landroid/widget/TextView;
    //   597: new 159	java/lang/StringBuilder
    //   600: dup
    //   601: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   604: aload 5
    //   606: invokevirtual 390	java/lang/String:trim	()Ljava/lang/String;
    //   609: iconst_0
    //   610: bipush -3
    //   612: aload 5
    //   614: invokevirtual 490	java/lang/String:length	()I
    //   617: iadd
    //   618: invokevirtual 387	java/lang/String:substring	(II)Ljava/lang/String;
    //   621: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   624: ldc_w 492
    //   627: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   630: invokevirtual 173	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   633: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   636: aload_0
    //   637: getfield 516	flar2/exkernelmanager/fragments/dy:i	Landroid/widget/TextView;
    //   640: ldc_w 493
    //   643: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   646: aload_0
    //   647: getfield 79	flar2/exkernelmanager/fragments/dy:I	Lflar2/exkernelmanager/utilities/m;
    //   650: ldc_w 526
    //   653: invokevirtual 334	flar2/exkernelmanager/utilities/m:a	(Ljava/lang/String;)Ljava/lang/String;
    //   656: astore 6
    //   658: aload 6
    //   660: ldc_w 485
    //   663: invokevirtual 194	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   666: ifne +396 -> 1062
    //   669: aload 6
    //   671: ifnull +391 -> 1062
    //   674: aload_0
    //   675: getfield 518	flar2/exkernelmanager/fragments/dy:j	Landroid/widget/TextView;
    //   678: new 159	java/lang/StringBuilder
    //   681: dup
    //   682: invokespecial 160	java/lang/StringBuilder:<init>	()V
    //   685: aload 6
    //   687: invokevirtual 390	java/lang/String:trim	()Ljava/lang/String;
    //   690: iconst_0
    //   691: bipush -3
    //   693: aload 6
    //   695: invokevirtual 490	java/lang/String:length	()I
    //   698: iadd
    //   699: invokevirtual 387	java/lang/String:substring	(II)Ljava/lang/String;
    //   702: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   705: ldc_w 492
    //   708: invokevirtual 169	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   711: invokevirtual 173	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   714: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   717: aload_0
    //   718: getfield 518	flar2/exkernelmanager/fragments/dy:j	Landroid/widget/TextView;
    //   721: ldc_w 493
    //   724: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   727: return
    //   728: aload_0
    //   729: getfield 487	flar2/exkernelmanager/fragments/dy:c	Landroid/widget/TextView;
    //   732: ldc_w 528
    //   735: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   738: aload_0
    //   739: getfield 497	flar2/exkernelmanager/fragments/dy:d	Landroid/widget/TextView;
    //   742: ldc_w 529
    //   745: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   748: goto -672 -> 76
    //   751: astore 15
    //   753: aload_0
    //   754: getfield 497	flar2/exkernelmanager/fragments/dy:d	Landroid/widget/TextView;
    //   757: ldc_w 528
    //   760: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   763: aload_0
    //   764: getfield 497	flar2/exkernelmanager/fragments/dy:d	Landroid/widget/TextView;
    //   767: ldc_w 529
    //   770: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   773: goto -621 -> 152
    //   776: aload_0
    //   777: getfield 497	flar2/exkernelmanager/fragments/dy:d	Landroid/widget/TextView;
    //   780: ldc_w 528
    //   783: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   786: aload_0
    //   787: getfield 497	flar2/exkernelmanager/fragments/dy:d	Landroid/widget/TextView;
    //   790: ldc_w 529
    //   793: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   796: goto -644 -> 152
    //   799: astore 14
    //   801: aload_0
    //   802: getfield 501	flar2/exkernelmanager/fragments/dy:e	Landroid/widget/TextView;
    //   805: ldc_w 528
    //   808: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   811: aload_0
    //   812: getfield 501	flar2/exkernelmanager/fragments/dy:e	Landroid/widget/TextView;
    //   815: ldc_w 529
    //   818: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   821: goto -581 -> 240
    //   824: aload_0
    //   825: getfield 501	flar2/exkernelmanager/fragments/dy:e	Landroid/widget/TextView;
    //   828: ldc_w 528
    //   831: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   834: aload_0
    //   835: getfield 501	flar2/exkernelmanager/fragments/dy:e	Landroid/widget/TextView;
    //   838: ldc_w 529
    //   841: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   844: goto -604 -> 240
    //   847: astore 13
    //   849: aload_0
    //   850: getfield 505	flar2/exkernelmanager/fragments/dy:f	Landroid/widget/TextView;
    //   853: ldc_w 528
    //   856: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   859: aload_0
    //   860: getfield 505	flar2/exkernelmanager/fragments/dy:f	Landroid/widget/TextView;
    //   863: ldc_w 529
    //   866: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   869: goto -548 -> 321
    //   872: aload_0
    //   873: getfield 505	flar2/exkernelmanager/fragments/dy:f	Landroid/widget/TextView;
    //   876: ldc_w 528
    //   879: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   882: aload_0
    //   883: getfield 505	flar2/exkernelmanager/fragments/dy:f	Landroid/widget/TextView;
    //   886: ldc_w 529
    //   889: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   892: goto -571 -> 321
    //   895: astore 10
    //   897: aload_0
    //   898: getfield 512	flar2/exkernelmanager/fragments/dy:g	Landroid/widget/TextView;
    //   901: ldc_w 528
    //   904: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   907: aload_0
    //   908: getfield 512	flar2/exkernelmanager/fragments/dy:g	Landroid/widget/TextView;
    //   911: ldc_w 529
    //   914: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   917: goto -433 -> 484
    //   920: aload_0
    //   921: getfield 512	flar2/exkernelmanager/fragments/dy:g	Landroid/widget/TextView;
    //   924: ldc_w 528
    //   927: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   930: aload_0
    //   931: getfield 512	flar2/exkernelmanager/fragments/dy:g	Landroid/widget/TextView;
    //   934: ldc_w 529
    //   937: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   940: goto -456 -> 484
    //   943: astore 9
    //   945: aload_0
    //   946: getfield 514	flar2/exkernelmanager/fragments/dy:h	Landroid/widget/TextView;
    //   949: ldc_w 528
    //   952: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   955: aload_0
    //   956: getfield 514	flar2/exkernelmanager/fragments/dy:h	Landroid/widget/TextView;
    //   959: ldc_w 529
    //   962: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   965: goto -400 -> 565
    //   968: aload_0
    //   969: getfield 514	flar2/exkernelmanager/fragments/dy:h	Landroid/widget/TextView;
    //   972: ldc_w 528
    //   975: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   978: aload_0
    //   979: getfield 514	flar2/exkernelmanager/fragments/dy:h	Landroid/widget/TextView;
    //   982: ldc_w 529
    //   985: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   988: goto -423 -> 565
    //   991: astore 8
    //   993: aload_0
    //   994: getfield 516	flar2/exkernelmanager/fragments/dy:i	Landroid/widget/TextView;
    //   997: ldc_w 528
    //   1000: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   1003: aload_0
    //   1004: getfield 516	flar2/exkernelmanager/fragments/dy:i	Landroid/widget/TextView;
    //   1007: ldc_w 529
    //   1010: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   1013: goto -367 -> 646
    //   1016: aload_0
    //   1017: getfield 516	flar2/exkernelmanager/fragments/dy:i	Landroid/widget/TextView;
    //   1020: ldc_w 528
    //   1023: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   1026: aload_0
    //   1027: getfield 516	flar2/exkernelmanager/fragments/dy:i	Landroid/widget/TextView;
    //   1030: ldc_w 529
    //   1033: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   1036: goto -390 -> 646
    //   1039: astore 7
    //   1041: aload_0
    //   1042: getfield 518	flar2/exkernelmanager/fragments/dy:j	Landroid/widget/TextView;
    //   1045: ldc_w 528
    //   1048: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   1051: aload_0
    //   1052: getfield 518	flar2/exkernelmanager/fragments/dy:j	Landroid/widget/TextView;
    //   1055: ldc_w 529
    //   1058: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   1061: return
    //   1062: aload_0
    //   1063: getfield 518	flar2/exkernelmanager/fragments/dy:j	Landroid/widget/TextView;
    //   1066: ldc_w 528
    //   1069: invokevirtual 179	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   1072: aload_0
    //   1073: getfield 518	flar2/exkernelmanager/fragments/dy:j	Landroid/widget/TextView;
    //   1076: ldc_w 529
    //   1079: invokevirtual 184	android/widget/TextView:setTextColor	(I)V
    //   1082: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	1083	0	this	dy
    //   10	34	1	str1	String
    //   86	34	2	str2	String
    //   418	34	3	str3	String
    //   494	38	4	str4	String
    //   575	38	5	str5	String
    //   656	38	6	str6	String
    //   1039	1	7	localStringIndexOutOfBoundsException1	StringIndexOutOfBoundsException
    //   991	1	8	localStringIndexOutOfBoundsException2	StringIndexOutOfBoundsException
    //   943	1	9	localStringIndexOutOfBoundsException3	StringIndexOutOfBoundsException
    //   895	1	10	localStringIndexOutOfBoundsException4	StringIndexOutOfBoundsException
    //   169	38	11	str7	String
    //   250	38	12	str8	String
    //   847	1	13	localStringIndexOutOfBoundsException5	StringIndexOutOfBoundsException
    //   799	1	14	localStringIndexOutOfBoundsException6	StringIndexOutOfBoundsException
    //   751	1	15	localStringIndexOutOfBoundsException7	StringIndexOutOfBoundsException
    // Exception table:
    //   from	to	target	type
    //   101	152	751	java/lang/StringIndexOutOfBoundsException
    //   187	240	799	java/lang/StringIndexOutOfBoundsException
    //   268	321	847	java/lang/StringIndexOutOfBoundsException
    //   433	484	895	java/lang/StringIndexOutOfBoundsException
    //   512	565	943	java/lang/StringIndexOutOfBoundsException
    //   593	646	991	java/lang/StringIndexOutOfBoundsException
    //   674	727	1039	java/lang/StringIndexOutOfBoundsException
  }
  
  private void k()
  {
    String str = this.I.a(r.d[this.K]).trim();
    if ((str.equals("NA")) || (str == null))
    {
      this.k.setText("");
      return;
    }
    if (!str.equals("27000000"))
    {
      this.k.setText(this.O.getString(2131624040) + str.trim().substring(0, -6 + str.length()) + " MHz");
      return;
    }
    this.k.setText(this.O.getString(2131624041));
  }
  
  private void l()
  {
    String str1;
    if (k.a("prefDeviceName").equals("Nexus6"))
    {
      str1 = this.I.a("/sys/class/thermal/thermal_zone6/temp").trim();
      if (str1 == null) {
        str1 = "NA";
      }
      if (!str1.equals("NA")) {
        break label78;
      }
      this.b.setText(str1);
    }
    label78:
    label240:
    int i1;
    do
    {
      do
      {
        int i2;
        do
        {
          do
          {
            return;
            str1 = this.I.a(r.c[this.L]).trim();
            break;
          } while ((str1.equals("")) && (str1.length() == 0));
          if (str1.length() > 4) {
            str1 = str1.substring(0, -3 + str1.length());
          }
          if (!this.H.equals("2")) {
            break label240;
          }
          String str2 = String.valueOf(32 + (int)(1.8D * Double.parseDouble(str1)));
          this.b.setText(str2 + "°F");
          i2 = f.a(str2, 0);
          if (i2 < 131)
          {
            this.b.setTextColor(-16711936);
            return;
          }
          if ((i2 >= 131) && (i2 < 156))
          {
            this.b.setTextColor(65280);
            return;
          }
        } while (i2 < 156);
        this.b.setTextColor(-65536);
        return;
      } while (!this.H.equals("1"));
      this.b.setText(str1 + "°C");
      i1 = f.a(str1, 0);
      if (i1 < 60)
      {
        this.b.setTextColor(-16711936);
        return;
      }
      if ((i1 >= 60) && (i1 <= 69))
      {
        this.b.setTextColor(65280);
        return;
      }
    } while (i1 <= 69);
    this.b.setTextColor(-65536);
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if (paramMenu != null) {
      paramMenu.removeItem(2131493236);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.a = PreferenceManager.getDefaultSharedPreferences(getActivity());
    String[] arrayOfString;
    if (this.a.getBoolean("prefColorDashboard", false))
    {
      this.M = ((ViewGroup)paramLayoutInflater.inflate(2130903092, paramViewGroup, false));
      setHasOptionsMenu(true);
      arrayOfString = getResources().getStringArray(2131230721);
      getActivity().setTitle(arrayOfString[0]);
      ((a)a.a.getAdapter()).f(a.b.indexOf("Dashboard"));
      this.Q = ((TextView)getActivity().findViewById(2131493046));
      this.R = ((ImageView)getActivity().findViewById(2131493045));
      if (((!getResources().getBoolean(2131296267)) && (!getResources().getBoolean(2131296266))) || (getResources().getBoolean(2131296263))) {
        break label1164;
      }
      MainActivity.o.getLayoutParams().height = getResources().getDimensionPixelSize(2131427421);
      this.R.setImageResource(2130837639);
      this.Q.setText(arrayOfString[0]);
      label205:
      this.O = MainApp.a();
      this.S = new Handler();
      if (!k.b("prefColorDashboard").booleanValue()) {
        break label1281;
      }
      this.y = this.M.findViewById(2131493139);
      this.z = this.M.findViewById(2131493109);
      this.A = this.M.findViewById(2131493115);
      this.B = this.M.findViewById(2131493121);
      this.C = this.M.findViewById(2131493125);
      this.D = this.M.findViewById(2131493129);
    }
    for (this.E = this.M.findViewById(2131493133);; this.E = this.M.findViewById(2131493132))
    {
      a();
      this.y.setOnClickListener(new ea(this));
      this.z.setOnClickListener(new eb(this));
      this.C.setOnClickListener(new ec(this));
      this.D.setOnClickListener(new ed(this));
      this.A.setOnClickListener(new ee(this));
      this.E.setOnClickListener(new ef(this));
      this.b = ((TextView)this.M.findViewById(2131493093));
      this.c = ((TextView)this.M.findViewById(2131493096));
      this.d = ((TextView)this.M.findViewById(2131493098));
      this.e = ((TextView)this.M.findViewById(2131493099));
      this.f = ((TextView)this.M.findViewById(2131493100));
      this.g = ((TextView)this.M.findViewById(2131493102));
      this.h = ((TextView)this.M.findViewById(2131493103));
      this.i = ((TextView)this.M.findViewById(2131493104));
      this.j = ((TextView)this.M.findViewById(2131493105));
      this.t = ((TextView)this.M.findViewById(2131493106));
      this.u = ((TextView)this.M.findViewById(2131493107));
      this.k = ((TextView)this.M.findViewById(2131493110));
      this.x = ((TextView)this.M.findViewById(2131493112));
      this.l = ((TextView)this.M.findViewById(2131493119));
      this.m = ((TextView)this.M.findViewById(2131493117));
      this.n = ((TextView)this.M.findViewById(2131493122));
      this.o = ((TextView)this.M.findViewById(2131493123));
      this.r = ((TextView)this.M.findViewById(2131493126));
      this.s = ((TextView)this.M.findViewById(2131493127));
      this.v = ((TextView)this.M.findViewById(2131493130));
      this.w = ((TextView)this.M.findViewById(2131493131));
      this.q = ((TextView)this.M.findViewById(2131493135));
      this.p = ((TextView)this.M.findViewById(2131493136));
      if (!new File("/sys/devices/system/cpu/cpu2/online").exists())
      {
        this.F = false;
        this.e.setVisibility(8);
        this.f.setVisibility(8);
      }
      if (!new File("/sys/devices/system/cpu/cpu4/online").exists())
      {
        this.G = false;
        this.M.findViewById(2131493101).setVisibility(8);
        this.g.setVisibility(8);
        this.h.setVisibility(8);
        this.i.setVisibility(8);
        this.j.setVisibility(8);
      }
      ((TextView)this.M.findViewById(2131493137)).setText(this.O.getString(2131624044) + System.getProperty("os.version"));
      this.L = this.I.a(r.c);
      this.K = this.I.a(r.d);
      this.N = ((SwipeRefreshLayout)this.M.findViewById(2131493089));
      this.N.setProgressBackgroundColorSchemeColor(getResources().getColor(2131361829));
      this.N.setColorSchemeResources(new int[] { 2131361807, 2131361798, 2131361807 });
      this.N.setOnRefreshListener(new eg(this));
      if (k.b("prefFirstRunMonitor").booleanValue())
      {
        startActivity(new Intent(getActivity(), TutorialActivity.class));
        k.a("prefFirstRunMonitor", false);
        k.a("prefFirstRunSettings", true);
        k.a("prefFirstRunUpdater", true);
        k.a("prefFirstRunVoltage", true);
      }
      return this.M;
      this.M = ((ViewGroup)paramLayoutInflater.inflate(2130903091, paramViewGroup, false));
      break;
      label1164:
      if ((getResources().getBoolean(2131296264)) && (!getResources().getBoolean(2131296263)))
      {
        MainActivity.o.getLayoutParams().height = getResources().getDimensionPixelSize(2131427421);
        this.R.setImageResource(2130837639);
        this.Q.setText(arrayOfString[0]);
        break label205;
      }
      MainActivity.o.getLayoutParams().height = new f().b(getActivity());
      MainActivity.o.setTranslationY(0.0F);
      MainActivity.p.setTranslationY(0.0F);
      this.Q.setText(null);
      break label205;
      label1281:
      this.y = this.M.findViewById(2131493091);
      this.z = this.M.findViewById(2131493108);
      this.A = this.M.findViewById(2131493114);
      this.B = this.M.findViewById(2131493120);
      this.C = this.M.findViewById(2131493124);
      this.D = this.M.findViewById(2131493128);
    }
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131493247: 
      c();
      return true;
    }
    c();
    return true;
  }
  
  public void onPause()
  {
    super.onPause();
    getActivity().getApplicationContext().unregisterReceiver(this.U);
    this.S.removeCallbacks(this.T);
  }
  
  public void onResume()
  {
    super.onResume();
    this.S.post(this.T);
    getActivity().getApplicationContext().registerReceiver(this.U, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    this.H = k.a("prefTempUnit");
    c();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/dy.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */