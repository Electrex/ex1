package flar2.exkernelmanager.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.RectF;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v7.app.ab;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import flar2.exkernelmanager.MainActivity;
import flar2.exkernelmanager.MainApp;
import flar2.exkernelmanager.a.o;
import flar2.exkernelmanager.r;
import flar2.exkernelmanager.utilities.e;
import flar2.exkernelmanager.utilities.f;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class ac
  extends Fragment
  implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener
{
  m a = new m();
  f b = new f();
  e c = new e();
  ListView d;
  private flar2.exkernelmanager.a.a e;
  private flar2.exkernelmanager.powersave.a f;
  private flar2.exkernelmanager.performance.a g;
  private Context h;
  private String i;
  private int j;
  private TextView k;
  private TextView l;
  private ImageView m;
  private int n;
  private int o;
  private RectF p = new RectF();
  private RectF q = new RectF();
  private AccelerateDecelerateInterpolator r;
  
  private static float a(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    return Math.max(paramFloat2, Math.min(paramFloat1, paramFloat3));
  }
  
  private RectF a(RectF paramRectF, View paramView)
  {
    paramRectF.set(paramView.getLeft(), paramView.getTop(), paramView.getRight(), paramView.getBottom());
    return paramRectF;
  }
  
  private void a(int paramInt)
  {
    MainActivity.o.setTranslationY(Math.max(-paramInt, this.o));
    MainActivity.p.setTranslationY(Math.max(-paramInt, this.o));
    float f1 = a(MainActivity.o.getTranslationY() / this.o, 0.0F, 1.0F);
    a(this.k, this.l, this.r.getInterpolation(f1));
    a(this.m, this.l, this.r.getInterpolation(f1));
    this.m.setAlpha(1.0F - f1 * 2.0F);
  }
  
  private void a(View paramView1, View paramView2, float paramFloat)
  {
    a(this.p, paramView1);
    a(this.q, paramView2);
    float f1 = 1.0F + paramFloat * (this.q.width() / this.p.width() - 1.0F);
    float f2 = 1.0F + paramFloat * (this.q.height() / this.p.height() - 1.0F);
    float f3 = 0.5F * (paramFloat * (this.q.left + this.q.right - this.p.left - this.p.right));
    float f4 = 0.5F * (paramFloat * (this.q.top + this.q.bottom - this.p.top - this.p.bottom));
    paramView1.setTranslationX(f3);
    paramView1.setTranslationY(f4 - MainActivity.o.getTranslationY());
    paramView1.setScaleX(f1);
    paramView1.setScaleY(f2);
  }
  
  private void a(String paramString1, String paramString2)
  {
    android.support.v7.app.ac localac = new android.support.v7.app.ac(getActivity());
    localac.a(paramString1);
    localac.b(getString(2131624005), null);
    localac.b(paramString2);
    localac.a().show();
  }
  
  private void a(boolean paramBoolean)
  {
    String str;
    if (paramBoolean)
    {
      str = "444";
      this.a.a(k.a("prefCPUMin"), "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq");
    }
    for (;;)
    {
      try
      {
        this.b.a(str, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq");
        return;
      }
      catch (TimeoutException localTimeoutException)
      {
        localTimeoutException.printStackTrace();
        return;
      }
      catch (com.e.a.a.a locala)
      {
        locala.printStackTrace();
        return;
      }
      catch (IOException localIOException)
      {
        localIOException.printStackTrace();
      }
      str = "666";
    }
  }
  
  private void b()
  {
    this.b.a("666", "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq");
    this.b.a("666", "/sys/devices/system/cpu/cpu0/cpufreq/screen_off_max_freq");
    this.b.a("666", "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor");
  }
  
  private void b(int paramInt)
  {
    k.a("prefMPDecisionBoot", false);
    if (this.c.a("/sys/power/pnpmgr/hotplug/mp_nw")) {
      switch (paramInt)
      {
      default: 
        if (!k.a("prefMPDecision").equals("Disabled"))
        {
          this.b.a("start mpdecision");
          this.a.a(k.a("prefMPDecision_NW"), "/sys/power/pnpmgr/hotplug/mp_nw");
          this.a.a(k.a("prefMPDecision_NS"), "/sys/power/pnpmgr/hotplug/mp_ns");
          this.a.a(k.a("prefMPDecision_TW"), "/sys/power/pnpmgr/hotplug/mp_tw");
        }
        break;
      }
    }
    for (;;)
    {
      c();
      return;
      this.b.a("stop mpdecision");
      this.a.a("1", "/sys/devices/system/cpu/cpu1/online");
      this.a.a("1", "/sys/devices/system/cpu/cpu2/online");
      this.a.a("1", "/sys/devices/system/cpu/cpu3/online");
      k.a("prefMPDecision", "Disabled");
      break;
      k.a("prefMPDecision_NW", "0 1.9 2.2 3.0");
      k.a("prefMPDecision_NS", "0 0.9 1.9 3.0");
      k.a("prefMPDecision_TW", "0 90 90 90");
      k.a("prefMPDecision", "Aggressive");
      break;
      k.a("prefMPDecision_NW", "0 1.9 2.7 3.5");
      k.a("prefMPDecision_NS", "0 1.1 2.1 3.1");
      k.a("prefMPDecision_TW", "0 140 90 90");
      k.a("prefMPDecision", "Default");
      break;
      k.a("prefMPDecision_NW", "0 2.0 3.1 4.5");
      k.a("prefMPDecision_NS", "0 1.1 2.1 3.5");
      k.a("prefMPDecision_TW", "0 140 140 140");
      k.a("prefMPDecision", "Battery Saving");
      break;
      switch (paramInt)
      {
      default: 
        break;
      case 0: 
        this.b.a("stop mpdecision");
        this.a.a("1", "/sys/devices/system/cpu/cpu1/online");
        if ((this.i.equals(this.h.getString(2131624153))) || (this.i.equals(this.h.getString(2131624155))) || (this.i.equals(this.h.getString(2131624153))))
        {
          this.a.a("300000", "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq");
          this.a.a("300000", "/sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq");
          this.a.a("300000", "/sys/devices/system/cpu/cpu2/cpufreq/scaling_min_freq");
          this.a.a("300000", "/sys/devices/system/cpu/cpu3/cpufreq/scaling_min_freq");
        }
        for (;;)
        {
          this.a.a("1", "/sys/devices/system/cpu/cpu2/online");
          this.a.a("1", "/sys/devices/system/cpu/cpu3/online");
          k.a("prefMPDecision", "Disabled");
          break;
          if (this.i.equals(this.h.getString(2131624157)))
          {
            this.a.a("384000", "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq");
            this.a.a("384000", "/sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq");
          }
        }
      case 1: 
        this.b.a("start mpdecision");
        k.a("prefMPDecision", "Enabled");
      }
    }
  }
  
  private void b(String paramString1, String paramString2)
  {
    android.support.v7.app.ac localac = new android.support.v7.app.ac(getActivity());
    localac.a(this.h.getString(2131624092));
    localac.b(2131623999, null);
    EditText localEditText = new EditText(getActivity());
    localac.a(localEditText);
    localEditText.setHint(this.a.a(paramString2));
    localac.a(2131624170, new ae(this, localEditText, paramString1, paramString2));
    ab localab = localac.a();
    localab.getWindow().setSoftInputMode(5);
    localab.show();
  }
  
  private void c()
  {
    new ak(this, null).execute(new Void[0]);
  }
  
  private void c(int paramInt)
  {
    k.a("prefThermBoot", false);
    if (this.i.equals(this.h.getString(2131624153))) {
      switch (paramInt)
      {
      }
    }
    for (;;)
    {
      this.b.a("stop thermal-engine");
      this.b.a("start thermal-engine");
      c();
      return;
      k.a("prefTherm", "warmer");
      this.c.a("THERM=", "THERM=1");
      this.b.b(this.h, "N5_elex");
      continue;
      k.a("prefTherm", "stock");
      this.c.a("THERM=", "THERM=3");
      this.b.b(this.h, "N5_stock");
      continue;
      k.a("prefTherm", "cooler");
      this.c.a("THERM=", "THERM=2");
      this.b.b(this.h, "N5_cool");
      continue;
      if (this.i.equals("HTC_One_m8")) {
        switch (paramInt)
        {
        default: 
          break;
        case 0: 
          k.a("prefTherm", "warmer");
          this.c.a("THERM=", "THERM=1");
          this.b.b(this.h, "m8_elex");
          break;
        case 1: 
          k.a("prefTherm", "stock");
          this.c.a("THERM=", "THERM=2");
          this.b.b(this.h, "m8_stock");
          break;
        case 2: 
          k.a("prefTherm", "cooler");
          this.b.b(this.h, "m8_cool");
        }
      }
    }
  }
  
  private void d()
  {
    if (!k.e("prefCPUMin"))
    {
      if ((this.i.equals(this.h.getString(2131624153))) || (this.i.equals(this.h.getString(2131624113))) || (this.i.equals(this.h.getString(2131624155)))) {
        k.a("prefCPUMin", "300000");
      }
    }
    else {
      return;
    }
    if ((this.i.equals(this.h.getString(2131624157))) || (this.i.equals(this.h.getString(2131624111))) || (this.i.equals(this.h.getString(2131624116))))
    {
      k.a("prefCPUMin", "384000");
      return;
    }
    if (this.i.equals(this.h.getString(2131624159)))
    {
      k.a("prefCPUMin", "204000");
      return;
    }
    k.a("prefCPUMin", this.a.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq"));
  }
  
  private void d(int paramInt)
  {
    android.support.v7.app.ac localac = new android.support.v7.app.ac(getActivity());
    localac.a(this.h.getString(2131624205));
    localac.b(2131623999, null);
    localac.a(this.c.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies", 1, 0), new ah(this, this.c.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies", 0, 0), paramInt));
    localac.a().show();
  }
  
  private void e()
  {
    if (!k.b("prefPowersaver").booleanValue())
    {
      if (k.b("prefPerformance").booleanValue())
      {
        k.a("prefPerformance", false);
        this.g.a(false);
      }
      k.a("prefPowersaver", true);
      this.f.a(true);
    }
    for (;;)
    {
      c();
      return;
      k.a("prefPowersaver", false);
      this.f.a(false);
    }
  }
  
  private void e(int paramInt)
  {
    android.support.v7.app.ac localac = new android.support.v7.app.ac(getActivity());
    localac.a(this.h.getString(2131624205));
    localac.b(2131623999, null);
    localac.a(this.c.a("/sys/devices/system/cpu/cpu4/cpufreq/scaling_available_frequencies", 1, 0), new ai(this, this.c.a("/sys/devices/system/cpu/cpu4/cpufreq/scaling_available_frequencies", 0, 0), paramInt));
    localac.a().show();
  }
  
  private void f()
  {
    if (!k.b("prefPerformance").booleanValue())
    {
      if (k.b("prefPowersaver").booleanValue())
      {
        k.a("prefPowersaver", false);
        this.f.a(false);
      }
      k.a("prefPerformance", true);
      this.g.a(true);
    }
    for (;;)
    {
      c();
      return;
      k.a("prefPerformance", false);
      this.g.a(false);
    }
  }
  
  private void g()
  {
    k.a("prefCPUQuietEnabledBoot", false);
    if (this.a.a("/sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable").equals("1"))
    {
      this.a.a("0", "/sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable");
      this.a.a("userspace", "/sys/devices/system/cpu/cpuquiet/current_governor");
      k.a("prefCPUQuietEnabled", "0");
      k.a("prefCPUQuietGovernor", "userspace");
    }
    for (;;)
    {
      c();
      return;
      if (this.a.a("/sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable").equals("0"))
      {
        this.a.a("1", "/sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable");
        this.a.a("balanced", "/sys/devices/system/cpu/cpuquiet/current_governor");
        k.a("prefCPUQuietEnabled", "1");
        k.a("prefCPUQuietGovernor", "balanced");
      }
    }
  }
  
  private void h()
  {
    android.support.v7.app.ac localac = new android.support.v7.app.ac(getActivity());
    localac.a(this.h.getString(2131624151));
    localac.b(2131623999, null);
    String[] arrayOfString;
    if (this.c.a("/sys/power/pnpmgr/hotplug/mp_nw"))
    {
      arrayOfString = new String[4];
      arrayOfString[0] = "Disabled";
      arrayOfString[1] = "Aggressive";
      arrayOfString[2] = "Default";
      arrayOfString[3] = "Battery Saving";
    }
    for (;;)
    {
      localac.a(arrayOfString, new af(this));
      localac.a().show();
      return;
      arrayOfString = new String[] { "Disabled", "Enabled" };
    }
  }
  
  private void i()
  {
    android.support.v7.app.ac localac = new android.support.v7.app.ac(getActivity());
    localac.a(this.h.getString(2131624266));
    localac.b(2131623999, null);
    localac.a(new String[] { "ElementalX", "Stock", "Extra cool" }, new ag(this));
    localac.b();
  }
  
  private void j()
  {
    startActivity(new Intent(getActivity(), CPUTimeActivity.class));
  }
  
  private void k()
  {
    startActivity(new Intent(getActivity(), GovernorActivity.class));
  }
  
  private void l()
  {
    startActivity(new Intent(getActivity(), GovernorC2Activity.class));
  }
  
  private void m()
  {
    startActivity(new Intent(getActivity(), BoostActivity.class));
  }
  
  private void n()
  {
    startActivity(new Intent(getActivity(), SleeperActivity.class));
  }
  
  private void o()
  {
    android.support.v7.app.ac localac = new android.support.v7.app.ac(getActivity());
    localac.a(this.h.getString(2131624206));
    localac.b(2131623999, null);
    String[] arrayOfString = this.c.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors", 0, 0);
    localac.a(arrayOfString, new aj(this, arrayOfString));
    localac.a().show();
  }
  
  private List p()
  {
    ArrayList localArrayList = new ArrayList();
    if (this.i == null) {
      this.i = "unknown";
    }
    if (getActivity().getResources().getConfiguration().orientation == 1)
    {
      o localo1 = new o();
      localo1.a(7);
      localArrayList.add(localo1);
    }
    o localo2 = new o();
    localo2.a(0);
    localo2.a(this.h.getString(2131624096));
    localArrayList.add(localo2);
    o localo3 = new o();
    localo3.a(1);
    localo3.b(-10);
    localo3.a(this.h.getString(2131624141));
    label206:
    o localo4;
    label323:
    label377:
    o localo5;
    label492:
    o localo6;
    label594:
    o localo7;
    label707:
    o localo9;
    label847:
    label881:
    o localo21;
    label981:
    label1024:
    label1033:
    o localo13;
    label1290:
    o localo20;
    if (this.c.a("/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"))
    {
      localo3.b(this.c.c(this.a.a("/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq")));
      localo3.d("prefCPUMaxBoot");
      if (!k.b("prefCPUMaxBoot").booleanValue()) {
        break label1906;
      }
      localo3.a(true);
      localo3.c(2130837611);
      localArrayList.add(localo3);
      localo4 = new o();
      localo4.a(1);
      localo4.b(-11);
      localo4.a(this.h.getString(2131624146));
      a(true);
      localo4.b(this.c.c(this.a.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq")));
      if ((!k.b("prefHTC").booleanValue()) || (k.a("prefCPUMin").equals("300000")) || (k.a("prefCPUMin").equals("384000"))) {
        break label1923;
      }
      a(true);
      if (this.i.equals(this.h.getString(2131624159)))
      {
        localo4.d("prefCPUMinBoot");
        if (!k.b("prefCPUMinBoot").booleanValue()) {
          break label1931;
        }
        localo4.a(true);
        localo4.c(2130837611);
      }
      localArrayList.add(localo4);
      if (this.c.a("/sys/devices/system/cpu/cpu4/online"))
      {
        localo5 = new o();
        localo5.a(1);
        localo5.b(-115);
        localo5.a(this.h.getString(2131624140));
        localo5.b(this.c.c(this.a.a("/sys/devices/system/cpu/cpu4/cpufreq/scaling_max_freq")));
        localo5.d("prefCPUC2MaxBoot");
        if (!k.b("prefCPUC2MaxBoot").booleanValue()) {
          break label1948;
        }
        localo5.a(true);
        localo5.c(2130837611);
        localArrayList.add(localo5);
        localo6 = new o();
        localo6.a(1);
        localo6.b(-116);
        localo6.a(this.h.getString(2131624145));
        localo6.b(this.c.c(this.a.a("/sys/devices/system/cpu/cpu4/cpufreq/scaling_min_freq")));
        localo6.d("prefCPUC2MinBoot");
        if (!k.b("prefCPUC2MinBoot").booleanValue()) {
          break label1965;
        }
        localo6.a(true);
        localo6.c(2130837611);
        localArrayList.add(localo6);
      }
      if (this.c.a("/sys/devices/system/cpu/cpu0/cpufreq/screen_off_max_freq"))
      {
        localo7 = new o();
        localo7.a(1);
        localo7.b(-12);
        localo7.a(this.h.getString(2131624143));
        localo7.b(this.c.c(this.a.a("/sys/devices/system/cpu/cpu0/cpufreq/screen_off_max_freq")));
        localo7.d("prefMaxScroffBoot");
        if (!k.b("prefMaxScroffBoot").booleanValue()) {
          break label1982;
        }
        localo7.a(true);
        localo7.c(2130837611);
        localArrayList.add(localo7);
      }
      o localo8 = new o();
      localo8.a(2);
      localo8.b(-114);
      localo8.a(this.h.getString(2131624021));
      localArrayList.add(localo8);
      if (this.c.a("/sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable"))
      {
        localo9 = new o();
        localo9.a(1);
        localo9.b(64778);
        localo9.a(this.h.getString(2131624020));
        if (!this.a.a("/sys/devices/system/cpu/cpuquiet/tegra_cpuquiet/enable").equals("0")) {
          break label1999;
        }
        localo9.b(this.h.getString(2131624058));
        localo9.d("prefCPUQuietEnabledBoot");
        if (!k.b("prefCPUQuietEnabledBoot").booleanValue()) {
          break label2017;
        }
        localo9.a(true);
        localo9.c(2130837611);
        localArrayList.add(localo9);
      }
      if ((!this.c.a("/system/bin/mpdecision")) || (this.c.a("/sys/devices/platform/msm_sleeper"))) {
        break label2150;
      }
      localo21 = new o();
      localo21.a(1);
      localo21.b(-19);
      localo21.a(this.h.getString(2131624150));
      if (this.b.a("busybox pidof mpdecision").contains("\n")) {
        break label2034;
      }
      k.a("prefMPDecision", "Disabled");
      localo21.b(k.a("prefMPDecision"));
      localo21.d("prefMPDecisionBoot");
      if (!k.b("prefMPDecisionBoot").booleanValue()) {
        break label2133;
      }
      localo21.a(true);
      localo21.c(2130837611);
      localArrayList.add(localo21);
      if (this.c.a("/sys/devices/platform/msm_sleeper"))
      {
        o localo10 = new o();
        localo10.a(2);
        localo10.b(64786);
        localo10.a(this.h.getString(2131624018));
        localArrayList.add(localo10);
      }
      if ((this.c.a("/sys/module/blu_plug/parameters")) || (this.c.a("/sys/class/misc/mako_hotplug_control")))
      {
        o localo11 = new o();
        localo11.a(2);
        localo11.b(64786);
        localo11.a(this.h.getString(2131624018));
        localArrayList.add(localo11);
      }
      o localo12 = new o();
      localo12.a(0);
      localo12.a(this.h.getString(2131624102));
      localArrayList.add(localo12);
      localo13 = new o();
      localo13.a(1);
      localo13.b(-13);
      localo13.a(this.h.getString(2131624016));
      localo13.b(this.a.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"));
      localo13.d("prefCPUGovBoot");
      if (!k.b("prefCPUGovBoot").booleanValue()) {
        break label2162;
      }
      localo13.a(true);
      localo13.c(2130837611);
      localArrayList.add(localo13);
      o localo14 = new o();
      localo14.a(2);
      localo14.b(-14);
      localo14.a(this.h.getString(2131624013));
      localArrayList.add(localo14);
      if (this.c.a("/sys/devices/system/cpu/cpu4/online"))
      {
        o localo15 = new o();
        localo15.a(2);
        localo15.b(-117);
        localo15.a(this.h.getString(2131624022));
        localArrayList.add(localo15);
      }
      if (this.c.a("/sys/module/cpu_boost"))
      {
        o localo16 = new o();
        localo16.a(2);
        localo16.b(-113);
        localo16.a(this.h.getString(2131624011));
        localArrayList.add(localo16);
      }
      if (((this.c.a("/system/etc/elementalx.conf")) && ((Build.VERSION.SDK_INT >= 20) || (!this.i.equals(this.h.getString(2131624153))))) || (this.c.a(r.r[this.j])))
      {
        o localo19 = new o();
        localo19.a(0);
        localo19.a(this.h.getString(2131624265));
        localArrayList.add(localo19);
        localo20 = new o();
        localo20.a(1);
        localo20.b(-15);
        localo20.a(this.h.getString(2131624264));
        if ((Build.VERSION.SDK_INT <= 20) || (!this.i.equals(this.h.getString(2131624153))) || (this.c.a(r.r[this.j]))) {
          break label2220;
        }
        if (!this.c.d("THERM=").equals("THERM=1")) {
          break label2179;
        }
        localo20.b("ElementalX");
        label1671:
        if ((!this.i.equals(this.h.getString(2131624153))) || (!this.i.equals(this.h.getString(2131624113))) || (this.c.a(r.r[this.j])))
        {
          localo20.d("prefThermBoot");
          if (!k.b("prefThermBoot").booleanValue()) {
            break label2340;
          }
          localo20.a(true);
          localo20.c(2130837611);
        }
      }
    }
    for (;;)
    {
      localArrayList.add(localo20);
      if ((localArrayList.size() > 7) && (localArrayList.size() < 11))
      {
        o localo17 = new o();
        localo17.a(0);
        localArrayList.add(localo17);
        o localo18 = new o();
        localo18.a(0);
        localArrayList.add(localo18);
      }
      return localArrayList;
      if (this.c.a("/sys/module/cpu_tegra/parameters/cpu_user_cap"))
      {
        localo3.b(this.c.c(this.a.a("/sys/module/cpu_tegra/parameters/cpu_user_cap")));
        break;
      }
      localo3.b(this.c.c(this.a.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq")));
      break;
      label1906:
      localo3.a(false);
      localo3.c(2130837609);
      break label206;
      label1923:
      a(false);
      break label323;
      label1931:
      localo4.a(false);
      localo4.c(2130837609);
      break label377;
      label1948:
      localo5.a(false);
      localo5.c(2130837609);
      break label492;
      label1965:
      localo6.a(false);
      localo6.c(2130837609);
      break label594;
      label1982:
      localo7.a(false);
      localo7.c(2130837609);
      break label707;
      label1999:
      localo9.b(this.h.getString(2131624077));
      break label847;
      label2017:
      localo9.a(false);
      localo9.c(2130837609);
      break label881;
      label2034:
      if (this.c.a("/sys/power/pnpmgr/hotplug/mp_nw"))
      {
        if (this.a.a("/sys/power/pnpmgr/hotplug/mp_nw").equals("0 1.9 2.2 3.0"))
        {
          k.a("prefMPDecision", "Aggressive");
          break label981;
        }
        if (this.a.a("/sys/power/pnpmgr/hotplug/mp_nw").equals("0 2.0 3.1 4.5"))
        {
          k.a("prefMPDecision", "Battery Saving");
          break label981;
        }
        k.a("prefMPDecision", "Default");
        break label981;
      }
      k.a("prefMPDecision", "Enabled");
      break label981;
      label2133:
      localo21.a(false);
      localo21.c(2130837609);
      break label1024;
      label2150:
      k.a("prefMPDecision", "Disabled");
      break label1033;
      label2162:
      localo13.a(false);
      localo13.c(2130837609);
      break label1290;
      label2179:
      if (this.c.d("THERM=").equals("THERM=2"))
      {
        localo20.b("Extra Cool");
        break label1671;
      }
      localo20.b("Stock");
      break label1671;
      label2220:
      if ((this.i.equals(this.h.getString(2131624113))) && (!this.c.a(r.r[this.j])))
      {
        if (this.c.d("THERM=").equals("THERM=1"))
        {
          localo20.b("ElementalX");
          break label1671;
        }
        localo20.b("Stock");
        break label1671;
      }
      if (!this.c.a(r.r[this.j])) {
        break label1671;
      }
      localo20.b(this.a.a(r.r[this.j]));
      break label1671;
      label2340:
      localo20.a(false);
      localo20.c(2130837609);
    }
  }
  
  public int a()
  {
    View localView = this.d.getChildAt(0);
    if (localView == null) {
      return 0;
    }
    int i1 = this.d.getFirstVisiblePosition();
    int i2 = localView.getTop();
    int i3 = 0;
    if (i1 >= 1) {
      i3 = this.n;
    }
    return i3 + (-i2 + i1 * localView.getHeight());
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if (paramMenu != null) {
      paramMenu.removeItem(2131493236);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView = paramLayoutInflater.inflate(2130903090, paramViewGroup, false);
    this.h = MainApp.a();
    ((flar2.exkernelmanager.slidingmenu.a.a)flar2.exkernelmanager.slidingmenu.a.a.a.getAdapter()).f(flar2.exkernelmanager.slidingmenu.a.a.b.indexOf("CPU"));
    setHasOptionsMenu(true);
    this.f = new flar2.exkernelmanager.powersave.a(this.h);
    this.g = new flar2.exkernelmanager.performance.a(this.h);
    this.i = k.a("prefDeviceName");
    d();
    String[] arrayOfString = getResources().getStringArray(2131230721);
    getActivity().setTitle(arrayOfString[1]);
    flar2.exkernelmanager.a.a.b = true;
    this.d = ((ListView)localView.findViewById(2131492989));
    this.e = new flar2.exkernelmanager.a.a(getActivity(), new ArrayList());
    this.d.setAdapter(this.e);
    this.d.setOnItemClickListener(this);
    if (!getActivity().getResources().getBoolean(2131296263))
    {
      MainActivity.o.getLayoutParams().height = getResources().getDimensionPixelSize(2131427421);
      this.m = ((ImageView)getActivity().findViewById(2131493045));
      this.m.setImageResource(2130837615);
      this.l = ((TextView)getActivity().findViewById(2131493044));
      this.l.setText(arrayOfString[1]);
      this.k = ((TextView)getActivity().findViewById(2131493046));
      this.k.setText(arrayOfString[1]);
      this.n = getResources().getDimensionPixelSize(2131427421);
      this.o = (-this.n + this.b.b(getActivity()));
      this.r = new AccelerateDecelerateInterpolator();
    }
    for (;;)
    {
      this.d.setOnScrollListener(new ad(this));
      try
      {
        b();
        c();
        if (k.a.getBoolean("prefFirstRunSettings", true)) {}
        this.j = this.a.a(r.r);
        k.a("prefThermPath", this.j);
        return localView;
        MainActivity.o.getLayoutParams().height = new f().b(getActivity());
      }
      catch (IOException localIOException)
      {
        for (;;)
        {
          localIOException.printStackTrace();
        }
      }
      catch (com.e.a.a.a locala)
      {
        for (;;)
        {
          locala.printStackTrace();
        }
      }
      catch (TimeoutException localTimeoutException)
      {
        for (;;)
        {
          localTimeoutException.printStackTrace();
        }
      }
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    if (this.r != null) {
      a(0);
    }
  }
  
  public void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    o localo = (o)this.e.getItem(paramInt);
    switch (localo.b())
    {
    default: 
    case -10: 
    case -11: 
    case -115: 
    case -116: 
    case -12: 
    case -18: 
    case -199: 
    case -19: 
    case -114: 
    case -13: 
    case -14: 
    case -117: 
    case -113: 
    case -750: 
    case -15: 
      do
      {
        return;
        d(localo.b());
        return;
        d(localo.b());
        return;
        e(localo.b());
        return;
        e(localo.b());
        return;
        a(true);
        d(localo.b());
        if ((k.b("prefHTC").booleanValue()) && (!k.a("prefCPUMin").equals("300000")) && (!k.a("prefCPUMin").equals("384000")))
        {
          a(true);
          return;
        }
        a(false);
        return;
        e();
        return;
        f();
        return;
        h();
        return;
        j();
        return;
        o();
        return;
        k();
        return;
        l();
        return;
        m();
        return;
        n();
        return;
        if (((this.i.equals(this.h.getString(2131624113))) || (this.i.equals(this.h.getString(2131624153)))) && (!this.c.a(r.r[this.j])))
        {
          i();
          return;
        }
      } while (!this.c.a(r.r[this.j]));
      b("prefTherm", r.r[this.j]);
      return;
    }
    g();
  }
  
  public boolean onItemLongClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    switch (((o)this.e.getItem(paramInt)).b())
    {
    default: 
      Toast.makeText(this.h, "No help for this item", 0).show();
    }
    for (;;)
    {
      return true;
      a("Max CPU frequency", "Choose the maximum CPU frequency to be used");
      continue;
      a("Min CPU frequency", "This is for information only.  It is best not to set the minimum frequency.");
      continue;
      a("Max screen off frequency", "Choose the maximum CPU frequency to be used while the screen is off.  It is recommended not to set this too low.");
      continue;
      a("MPDecision settings", "Controls when CPUs come online");
      continue;
      a("Powersave mode", "Reduce max CPU frequency, dim screen, turn off vibration and disable wake functions to save battery");
      continue;
      a("Performance mode", "Maximize CPU and GPU frequencies and put all CPU cores online");
      continue;
      a("CPU Governor", "The governor controls frequency scaling for the CPU");
      continue;
      a("Thermal throttling", "To prevent damage, the CPU frequency is reduced when the device gets hot.  This setting controls the temperature at which throttling begins.  Cooler means throttling starts earlier, thus keeping your device cooler at the expense of performance.");
    }
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131493247: 
      c();
      return true;
    }
    c();
    return true;
  }
  
  public void onPause()
  {
    super.onPause();
    flar2.exkernelmanager.a.a.b = false;
  }
  
  public void onResume()
  {
    super.onResume();
    c();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/ac.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */