package flar2.exkernelmanager.fragments;

import android.content.res.Resources;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.widget.Switch;

class be
  implements ViewTreeObserver.OnScrollChangedListener
{
  be(ColorActivity paramColorActivity, View paramView) {}
  
  public void onScrollChanged()
  {
    int i = this.a.getScrollY();
    ColorActivity.b(this.b).setTranslationY(Math.max(-i, ColorActivity.a(this.b)));
    ColorActivity.c(this.b).setTranslationY(Math.max(-i, ColorActivity.a(this.b)));
    if (this.b.getResources().getBoolean(2131296263)) {
      ColorActivity.d(this.b).setTranslationY(Math.max(-i / 2, ColorActivity.a(this.b)));
    }
    for (;;)
    {
      float f = ColorActivity.a(this.b, ColorActivity.b(this.b).getTranslationY() / ColorActivity.a(this.b), 0.0F, 1.0F);
      ColorActivity.e(this.b).setAlpha(1.0F - f * 2.0F);
      return;
      ColorActivity.d(this.b).setTranslationY(Math.max(-i / 3, ColorActivity.a(this.b)));
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/be.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */