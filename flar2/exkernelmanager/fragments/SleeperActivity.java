package flar2.exkernelmanager.fragments;

import android.os.Bundle;
import android.support.v4.app.ar;
import android.support.v7.app.ab;
import android.support.v7.app.ac;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import flar2.exkernelmanager.a.o;
import flar2.exkernelmanager.utilities.f;
import flar2.exkernelmanager.utilities.h;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;
import java.util.ArrayList;
import java.util.List;

public class SleeperActivity
  extends android.support.v7.app.e
  implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener
{
  h n;
  private m o = new m();
  private flar2.exkernelmanager.utilities.e p = new flar2.exkernelmanager.utilities.e();
  private f q = new f();
  private ListView r;
  private flar2.exkernelmanager.a.a s;
  
  private void a(String paramString1, String paramString2)
  {
    ac localac = new ac(this);
    localac.a(getString(2131624205));
    localac.b(2131623999, null);
    localac.a(this.p.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies", 1, 0), new el(this, this.p.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies", 0, 0), paramString1, paramString2));
    localac.a().show();
  }
  
  private void b(String paramString1, String paramString2)
  {
    ac localac = new ac(this);
    localac.a(getString(2131624091));
    localac.b(2131623999, null);
    EditText localEditText = new EditText(this);
    localac.a(localEditText);
    localEditText.setHint(this.o.b(paramString2));
    localEditText.setInputType(2);
    localac.a(2131624170, new en(this, localEditText, paramString1, paramString2));
    ab localab = localac.a();
    localab.getWindow().setSoftInputMode(5);
    localab.show();
  }
  
  private void c(String paramString1, String paramString2)
  {
    k.a(paramString1 + "Boot", false);
    if (this.o.b(paramString2).equals("0"))
    {
      this.o.a("1", paramString2);
      k.a(paramString1, "1");
    }
    for (;;)
    {
      k();
      return;
      if (this.o.b(paramString2).equals("1"))
      {
        this.o.a("0", paramString2);
        k.a(paramString1, "0");
      }
      else if (this.o.b(paramString2).equals("Y"))
      {
        this.o.a("N", paramString2);
        k.a(paramString1, "N");
      }
      else if (this.o.b(paramString2).equals("N"))
      {
        this.o.a("Y", paramString2);
        k.a(paramString1, "Y");
      }
    }
  }
  
  private void k()
  {
    new eo(this, null).execute(new Void[0]);
  }
  
  private void l()
  {
    ac localac = new ac(this);
    localac.a(getString(2131624109));
    localac.b(2131623999, null);
    String[] arrayOfString = new String[3];
    arrayOfString[0] = getString(2131624150);
    arrayOfString[1] = getString(2131624029);
    arrayOfString[2] = getString(2131624058);
    localac.a(arrayOfString, new em(this));
    localac.a().show();
  }
  
  private List m()
  {
    ArrayList localArrayList = new ArrayList();
    o localo1 = new o();
    localo1.a(0);
    localo1.a(getString(2131624017));
    localArrayList.add(localo1);
    o localo27;
    label152:
    label161:
    o localo21;
    label267:
    o localo22;
    label350:
    label383:
    o localo23;
    label484:
    o localo24;
    label585:
    o localo25;
    label686:
    o localo26;
    label787:
    o localo11;
    label886:
    label919:
    o localo12;
    label1020:
    o localo13;
    label1128:
    o localo14;
    label1229:
    o localo15;
    label1330:
    o localo16;
    label1431:
    o localo17;
    label1532:
    o localo18;
    label1633:
    o localo19;
    label1734:
    o localo20;
    label1842:
    o localo3;
    label1956:
    o localo4;
    label2057:
    o localo5;
    label2158:
    o localo6;
    label2259:
    o localo7;
    label2367:
    o localo8;
    label2467:
    o localo9;
    label2568:
    o localo10;
    if ((this.p.a("/system/bin/mpdecision")) && (this.p.a("/sys/devices/platform/msm_sleeper")))
    {
      localo27 = new o();
      localo27.a(1);
      localo27.b(64779);
      localo27.a(getString(2131624109));
      if (this.q.a("busybox pidof mpdecision").contains("\n"))
      {
        localo27.b("mpdecision");
        localo27.d("prefHotplugBoot");
        if (!k.b("prefHotplugBoot").booleanValue()) {
          break label2725;
        }
        localo27.a(true);
        localo27.c(2130837611);
        localArrayList.add(localo27);
        if (this.o.a("/sys/devices/platform/msm_sleeper/enabled").equals("1"))
        {
          if (this.p.b("/sys/devices/platform/msm_sleeper/up_threshold"))
          {
            localo21 = new o();
            localo21.a(1);
            localo21.b(64785);
            localo21.a("Up threshold");
            localo21.b(this.o.b("/sys/devices/platform/msm_sleeper/up_threshold"));
            localo21.d("prefSleeperUpThresholdBoot");
            if (!k.b("prefSleeperUpThresholdBoot").booleanValue()) {
              break label2894;
            }
            localo21.a(true);
            localo21.c(2130837611);
            localArrayList.add(localo21);
          }
          if (this.p.b("/sys/devices/platform/msm_sleeper/plug_all"))
          {
            localo22 = new o();
            localo22.a(1);
            localo22.b(64776);
            localo22.a("Run with all cores online");
            if (!this.o.b("/sys/devices/platform/msm_sleeper/plug_all").equals("1")) {
              break label2911;
            }
            localo22.b(getString(2131624077));
            localo22.d("prefSleeperPlugAllBoot");
            if (!k.b("prefSleeperPlugAllBoot").booleanValue()) {
              break label2925;
            }
            localo22.a(true);
            localo22.c(2130837611);
            localArrayList.add(localo22);
          }
          if (this.p.b("/sys/devices/platform/msm_sleeper/up_count_max"))
          {
            localo23 = new o();
            localo23.a(1);
            localo23.b(64782);
            localo23.a("Up count (tenths of a second)");
            localo23.b(this.o.b("/sys/devices/platform/msm_sleeper/up_count_max"));
            localo23.d("prefSleeperUpCountMaxBoot");
            if (!k.b("prefSleeperUpCountMaxBoot").booleanValue()) {
              break label2942;
            }
            localo23.a(true);
            localo23.c(2130837611);
            localArrayList.add(localo23);
          }
          if (this.p.b("/sys/devices/platform/msm_sleeper/down_count_max"))
          {
            localo24 = new o();
            localo24.a(1);
            localo24.b(64781);
            localo24.a("Down count (tenths of a second)");
            localo24.b(this.o.b("/sys/devices/platform/msm_sleeper/down_count_max"));
            localo24.d("prefSleeperDownCountMaxBoot");
            if (!k.b("prefSleeperDownCountMaxBoot").booleanValue()) {
              break label2959;
            }
            localo24.a(true);
            localo24.c(2130837611);
            localArrayList.add(localo24);
          }
          if (this.p.b("/sys/devices/platform/msm_sleeper/max_online"))
          {
            localo25 = new o();
            localo25.a(1);
            localo25.b(64783);
            localo25.a("Max cores online");
            localo25.b(this.o.b("/sys/devices/platform/msm_sleeper/max_online"));
            localo25.d("prefSleeperMaxOnlineBoot");
            if (!k.b("prefSleeperMaxOnlineBoot").booleanValue()) {
              break label2976;
            }
            localo25.a(true);
            localo25.c(2130837611);
            localArrayList.add(localo25);
          }
          if (this.p.b("/sys/devices/platform/msm_sleeper/suspend_max_online"))
          {
            localo26 = new o();
            localo26.a(1);
            localo26.b(64784);
            localo26.a("Cores online during suspend");
            localo26.b(this.o.b("/sys/devices/platform/msm_sleeper/suspend_max_online"));
            localo26.d("prefSleeperSuspendMaxOnlineBoot");
            if (!k.b("prefSleeperSuspendMaxOnlineBoot").booleanValue()) {
              break label2993;
            }
            localo26.a(true);
            localo26.c(2130837611);
            localArrayList.add(localo26);
          }
        }
        if (this.p.a("/sys/class/misc/mako_hotplug_control"))
        {
          if (this.p.b("/sys/class/misc/mako_hotplug_control/enabled"))
          {
            localo11 = new o();
            localo11.a(1);
            localo11.b(64436);
            localo11.a(getString(2131624076));
            if (!this.o.a("/sys/class/misc/mako_hotplug_control/enabled").equals("0")) {
              break label3010;
            }
            localo11.b(getString(2131624058));
            localo11.d("prefEnabledBoot");
            if (!k.b("prefEnabledBoot").booleanValue()) {
              break label3025;
            }
            localo11.a(true);
            localo11.c(2130837611);
            localArrayList.add(localo11);
          }
          if (this.p.b("/sys/class/misc/mako_hotplug_control/cores_on_touch"))
          {
            localo12 = new o();
            localo12.a(1);
            localo12.b(64435);
            localo12.a("Cores on touch");
            localo12.b(this.o.b("/sys/class/misc/mako_hotplug_control/cores_on_touch"));
            localo12.d("prefCoresOnTouchBoot");
            if (!k.b("prefCoresOnTouchBoot").booleanValue()) {
              break label3042;
            }
            localo12.a(true);
            localo12.c(2130837611);
            localArrayList.add(localo12);
          }
          if (this.p.b("/sys/class/misc/mako_hotplug_control/cpufreq_unplug_limit"))
          {
            localo13 = new o();
            localo13.a(1);
            localo13.b(64434);
            localo13.a("Cpufreq unplug limit");
            localo13.b(this.p.c(this.o.b("/sys/class/misc/mako_hotplug_control/cpufreq_unplug_limit")));
            localo13.d("prefCpufreqUnplugLimitBoot");
            if (!k.b("prefCpufreqUnplugLimitBoot").booleanValue()) {
              break label3059;
            }
            localo13.a(true);
            localo13.c(2130837611);
            localArrayList.add(localo13);
          }
          if (this.p.b("/sys/class/misc/mako_hotplug_control/first_level"))
          {
            localo14 = new o();
            localo14.a(1);
            localo14.b(64433);
            localo14.a("First level");
            localo14.b(this.o.b("/sys/class/misc/mako_hotplug_control/first_level"));
            localo14.d("prefFirstLevelBoot");
            if (!k.b("prefFirstLevelBoot").booleanValue()) {
              break label3076;
            }
            localo14.a(true);
            localo14.c(2130837611);
            localArrayList.add(localo14);
          }
          if (this.p.b("/sys/class/misc/mako_hotplug_control/high_load_counter"))
          {
            localo15 = new o();
            localo15.a(1);
            localo15.b(64432);
            localo15.a("High load counter");
            localo15.b(this.o.b("/sys/class/misc/mako_hotplug_control/high_load_counter"));
            localo15.d("prefHighLoadCounterBoot");
            if (!k.b("prefHighLoadCounterBoot").booleanValue()) {
              break label3093;
            }
            localo15.a(true);
            localo15.c(2130837611);
            localArrayList.add(localo15);
          }
          if (this.p.b("/sys/class/misc/mako_hotplug_control/load_threshold"))
          {
            localo16 = new o();
            localo16.a(1);
            localo16.b(64431);
            localo16.a("Load threshold");
            localo16.b(this.o.b("/sys/class/misc/mako_hotplug_control/load_threshold"));
            localo16.d("prefLoadThresholdBoot");
            if (!k.b("prefLoadThresholdBoot").booleanValue()) {
              break label3110;
            }
            localo16.a(true);
            localo16.c(2130837611);
            localArrayList.add(localo16);
          }
          if (this.p.b("/sys/class/misc/mako_hotplug_control/max_load_counter"))
          {
            localo17 = new o();
            localo17.a(1);
            localo17.b(64430);
            localo17.a("Max load counter");
            localo17.b(this.o.b("/sys/class/misc/mako_hotplug_control/max_load_counter"));
            localo17.d("prefMaxLoadCounterBoot");
            if (!k.b("prefMaxLoadCounterBoot").booleanValue()) {
              break label3127;
            }
            localo17.a(true);
            localo17.c(2130837611);
            localArrayList.add(localo17);
          }
          if (this.p.b("/sys/class/misc/mako_hotplug_control/min_time_cpu_online"))
          {
            localo18 = new o();
            localo18.a(1);
            localo18.b(64429);
            localo18.a("Min time cpu online");
            localo18.b(this.o.b("/sys/class/misc/mako_hotplug_control/min_time_cpu_online"));
            localo18.d("prefMinTimeCpuOnlineBoot");
            if (!k.b("prefMinTimeCpuOnlineBoot").booleanValue()) {
              break label3144;
            }
            localo18.a(true);
            localo18.c(2130837611);
            localArrayList.add(localo18);
          }
          if (this.p.b("/sys/class/misc/mako_hotplug_control/timer"))
          {
            localo19 = new o();
            localo19.a(1);
            localo19.b(64428);
            localo19.a("Timer");
            localo19.b(this.o.b("/sys/class/misc/mako_hotplug_control/timer"));
            localo19.d("prefTimerBoot");
            if (!k.b("prefTimerBoot").booleanValue()) {
              break label3161;
            }
            localo19.a(true);
            localo19.c(2130837611);
            localArrayList.add(localo19);
          }
          if (this.p.b("/sys/class/misc/mako_hotplug_control/suspend_frequency"))
          {
            localo20 = new o();
            localo20.a(1);
            localo20.b(64427);
            localo20.a("Suspend frequency");
            localo20.b(this.p.c(this.o.b("/sys/class/misc/mako_hotplug_control/suspend_frequency")));
            localo20.d("prefSuspendFrequencyBoot");
            if (!k.b("prefSuspendFrequencyBoot").booleanValue()) {
              break label3178;
            }
            localo20.a(true);
            localo20.c(2130837611);
            localArrayList.add(localo20);
          }
        }
        if (this.p.a("/sys/module/blu_plug/parameters"))
        {
          if (this.p.b("/sys/module/blu_plug/parameters/powersaver_mode"))
          {
            localo3 = new o();
            localo3.a(1);
            localo3.b(64335);
            localo3.a("Powersaver mode");
            localo3.b(this.o.b("/sys/module/blu_plug/parameters/powersaver_mode"));
            localo3.d("prefBluPlugPowersaverModeBoot");
            if (!k.b("prefBluPlugPowersaverModeBoot").booleanValue()) {
              break label3195;
            }
            localo3.a(true);
            localo3.c(2130837611);
            localArrayList.add(localo3);
          }
          if (this.p.b("/sys/module/blu_plug/parameters/min_online"))
          {
            localo4 = new o();
            localo4.a(1);
            localo4.b(64334);
            localo4.a("Min online");
            localo4.b(this.o.b("/sys/module/blu_plug/parameters/min_online"));
            localo4.d("prefBluPlugMinOnlineBoot");
            if (!k.b("prefBluPlugMinOnlineBoot").booleanValue()) {
              break label3212;
            }
            localo4.a(true);
            localo4.c(2130837611);
            localArrayList.add(localo4);
          }
          if (this.p.b("/sys/module/blu_plug/parameters/max_online"))
          {
            localo5 = new o();
            localo5.a(1);
            localo5.b(64333);
            localo5.a("Max online");
            localo5.b(this.o.b("/sys/module/blu_plug/parameters/max_online"));
            localo5.d("prefBluPlug/max_onlineBoot");
            if (!k.b("prefBluPlug/max_onlineBoot").booleanValue()) {
              break label3229;
            }
            localo5.a(true);
            localo5.c(2130837611);
            localArrayList.add(localo5);
          }
          if (this.p.b("/sys/module/blu_plug/parameters/max_cores_screenoff"))
          {
            localo6 = new o();
            localo6.a(1);
            localo6.b(64332);
            localo6.a("Max cores screenoff");
            localo6.b(this.o.b("/sys/module/blu_plug/parameters/max_cores_screenoff"));
            localo6.d("prefBluPlugMaxCoresScreenoffBoot");
            if (!k.b("prefBluPlugMaxCoresScreenoffBoot").booleanValue()) {
              break label3246;
            }
            localo6.a(true);
            localo6.c(2130837611);
            localArrayList.add(localo6);
          }
          if (this.p.b("/sys/module/blu_plug/parameters/max_freq_screenoff"))
          {
            localo7 = new o();
            localo7.a(1);
            localo7.b(64331);
            localo7.a("Max freq screenoff");
            localo7.b(this.p.c(this.o.b("/sys/module/blu_plug/parameters/max_freq_screenoff")));
            localo7.d("prefBluPlugMaxFreqScreenoffBoot");
            if (!k.b("prefBluPlugMaxFreqScreenoffBoot").booleanValue()) {
              break label3263;
            }
            localo7.a(true);
            localo7.c(2130837611);
            localArrayList.add(localo7);
          }
          if (this.p.b("/sys/module/blu_plug/parameters/up_threshold"))
          {
            localo8 = new o();
            localo8.a(1);
            localo8.b(64330);
            localo8.a("Up threshold");
            localo8.b(this.o.b("/sys/module/blu_plug/parameters/up_threshold"));
            localo8.d("prefBluPlugUpThresholdBoot");
            if (!k.b("prefBluPlugUpThresholdBoot").booleanValue()) {
              break label3280;
            }
            localo8.a(true);
            localo8.c(2130837611);
            localArrayList.add(localo8);
          }
          if (this.p.b("/sys/module/blu_plug/parameters/up_timer_cnt"))
          {
            localo9 = new o();
            localo9.a(1);
            localo9.b(64329);
            localo9.a("Up timer count");
            localo9.b(this.o.b("/sys/module/blu_plug/parameters/up_timer_cnt"));
            localo9.d("prefBluPlugUpTimerCntBoot");
            if (!k.b("prefBluPlugUpTimerCntBoot").booleanValue()) {
              break label3297;
            }
            localo9.a(true);
            localo9.c(2130837611);
            localArrayList.add(localo9);
          }
          if (this.p.b("/sys/module/blu_plug/parameters/down_timer_cnt"))
          {
            localo10 = new o();
            localo10.a(1);
            localo10.b(64328);
            localo10.a("Down timer count");
            localo10.b(this.o.b("/sys/module/blu_plug/parameters/down_timer_cnt"));
            localo10.d("prefBluPlugDownTimerCntBoot");
            if (!k.b("prefBluPlugDownTimerCntBoot").booleanValue()) {
              break label3314;
            }
            localo10.a(true);
            localo10.c(2130837611);
          }
        }
      }
    }
    for (;;)
    {
      localArrayList.add(localo10);
      return localArrayList;
      if (this.o.b("/sys/devices/platform/msm_sleeper/enabled").equals("1"))
      {
        localo27.b(getString(2131624029));
        break;
      }
      localo27.b(getString(2131624058));
      break;
      label2725:
      localo27.a(false);
      localo27.c(2130837609);
      break label152;
      if (!this.p.a("/sys/devices/platform/msm_sleeper")) {
        break label161;
      }
      o localo2 = new o();
      localo2.a(1);
      localo2.b(64780);
      localo2.a(getString(2131624076));
      if (this.o.b("/sys/devices/platform/msm_sleeper/enabled").equals("0"))
      {
        localo2.b(getString(2131624058));
        label2817:
        localo2.d("prefSleeperEnabledBoot");
        if (!k.b("prefSleeperEnabledBoot").booleanValue()) {
          break label2877;
        }
        localo2.a(true);
        localo2.c(2130837611);
      }
      for (;;)
      {
        localArrayList.add(localo2);
        break;
        localo2.b(getString(2131624077));
        break label2817;
        label2877:
        localo2.a(false);
        localo2.c(2130837609);
      }
      label2894:
      localo21.a(false);
      localo21.c(2130837609);
      break label267;
      label2911:
      localo22.b(getString(2131624058));
      break label350;
      label2925:
      localo22.a(false);
      localo22.c(2130837609);
      break label383;
      label2942:
      localo23.a(false);
      localo23.c(2130837609);
      break label484;
      label2959:
      localo24.a(false);
      localo24.c(2130837609);
      break label585;
      label2976:
      localo25.a(false);
      localo25.c(2130837609);
      break label686;
      label2993:
      localo26.a(false);
      localo26.c(2130837609);
      break label787;
      label3010:
      localo11.b(getString(2131624077));
      break label886;
      label3025:
      localo11.a(false);
      localo11.c(2130837609);
      break label919;
      label3042:
      localo12.a(false);
      localo12.c(2130837609);
      break label1020;
      label3059:
      localo13.a(false);
      localo13.c(2130837609);
      break label1128;
      label3076:
      localo14.a(false);
      localo14.c(2130837609);
      break label1229;
      label3093:
      localo15.a(false);
      localo15.c(2130837609);
      break label1330;
      label3110:
      localo16.a(false);
      localo16.c(2130837609);
      break label1431;
      label3127:
      localo17.a(false);
      localo17.c(2130837609);
      break label1532;
      label3144:
      localo18.a(false);
      localo18.c(2130837609);
      break label1633;
      label3161:
      localo19.a(false);
      localo19.c(2130837609);
      break label1734;
      label3178:
      localo20.a(false);
      localo20.c(2130837609);
      break label1842;
      label3195:
      localo3.a(false);
      localo3.c(2130837609);
      break label1956;
      label3212:
      localo4.a(false);
      localo4.c(2130837609);
      break label2057;
      label3229:
      localo5.a(false);
      localo5.c(2130837609);
      break label2158;
      label3246:
      localo6.a(false);
      localo6.c(2130837609);
      break label2259;
      label3263:
      localo7.a(false);
      localo7.c(2130837609);
      break label2367;
      label3280:
      localo8.a(false);
      localo8.c(2130837609);
      break label2467;
      label3297:
      localo9.a(false);
      localo9.c(2130837609);
      break label2568;
      label3314:
      localo10.a(false);
      localo10.c(2130837609);
    }
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    this.n.b().onTouchEvent(paramMotionEvent);
    return super.dispatchTouchEvent(paramMotionEvent);
  }
  
  public void onBackPressed()
  {
    super.onBackPressed();
    overridePendingTransition(0, 2130968604);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903077);
    a((Toolbar)findViewById(2131492951));
    overridePendingTransition(2130968605, 17432577);
    setTitle(getString(2131624019));
    g().a(true);
    this.n = new ej(this, this);
    findViewById(2131493053).setOnTouchListener(this.n);
    this.r = ((ListView)findViewById(2131492989));
    this.s = new flar2.exkernelmanager.a.a(this, new ArrayList());
    this.r.setAdapter(this.s);
    this.r.setOnItemClickListener(this);
    this.r.setOnItemLongClickListener(this);
    this.r.setOnScrollListener(new ek(this));
    k();
  }
  
  public void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    switch (((o)this.s.getItem(paramInt)).b())
    {
    default: 
      return;
    case -751: 
      b("prefSleeperUpThreshold", "/sys/devices/platform/msm_sleeper/up_threshold");
      return;
    case -756: 
      c("prefSleeperEnabled", "/sys/devices/platform/msm_sleeper/enabled");
      return;
    case -754: 
      b("prefSleeperUpCountMax", "/sys/devices/platform/msm_sleeper/up_count_max");
      return;
    case -755: 
      b("prefSleeperDownCountMax", "/sys/devices/platform/msm_sleeper/down_count_max");
      return;
    case -753: 
      b("prefSleeperMaxOnline", "/sys/devices/platform/msm_sleeper/max_online");
      return;
    case -752: 
      b("prefSleeperSuspendMaxOnline", "/sys/devices/platform/msm_sleeper/suspend_max_online");
      return;
    case -757: 
      l();
      return;
    case -760: 
      c("prefSleeperPlugAll", "/sys/devices/platform/msm_sleeper/plug_all");
      return;
    case -1100: 
      c("prefEnabled", "/sys/class/misc/mako_hotplug_control/enabled");
      return;
    case -1101: 
      b("prefCoresOnTouch", "/sys/class/misc/mako_hotplug_control/cores_on_touch");
      return;
    case -1102: 
      a("prefCpufreqUnplugLimit", "/sys/class/misc/mako_hotplug_control/cpufreq_unplug_limit");
      return;
    case -1103: 
      b("prefFirstLevel", "/sys/class/misc/mako_hotplug_control/first_level");
      return;
    case -1104: 
      b("prefHighLoadCounter", "/sys/class/misc/mako_hotplug_control/high_load_counter");
      return;
    case -1105: 
      b("prefLoadThreshold", "/sys/class/misc/mako_hotplug_control/load_threshold");
      return;
    case -1106: 
      b("prefMaxLoadCounter", "/sys/class/misc/mako_hotplug_control/max_load_counter");
      return;
    case -1107: 
      b("prefMinTimeCpuOnline", "/sys/class/misc/mako_hotplug_control/min_time_cpu_online");
      return;
    case -1108: 
      b("prefTimer", "/sys/class/misc/mako_hotplug_control/timer");
      return;
    case -1109: 
      a("prefSuspendFrequency", "/sys/class/misc/mako_hotplug_control/suspend_frequency");
      return;
    case -1200: 
      c("prefBluPlugEnabled", "/sys/module/blu_plug/parameters/enabled");
      return;
    case -1201: 
      b("prefBluPlugPowersaverMode", "/sys/module/blu_plug/parameters/powersaver_mode");
      return;
    case -1202: 
      b("prefBluPlugMinOnline", "/sys/module/blu_plug/parameters/min_online");
      return;
    case -1203: 
      b("prefBluPlug/max_online", "/sys/module/blu_plug/parameters/max_online");
      return;
    case -1204: 
      b("prefBluPlugMaxCoresScreenoff", "/sys/module/blu_plug/parameters/max_cores_screenoff");
      return;
    case -1205: 
      a("prefBluPlugMaxFreqScreenoff", "/sys/module/blu_plug/parameters/max_freq_screenoff");
      return;
    case -1206: 
      b("prefBluPlugUpThreshold", "/sys/module/blu_plug/parameters/up_threshold");
      return;
    case -1207: 
      b("prefBluPlugUpTimerCnt", "/sys/module/blu_plug/parameters/up_timer_cnt");
      return;
    }
    b("prefBluPlugDownTimerCnt", "/sys/module/blu_plug/parameters/down_timer_cnt");
  }
  
  public boolean onItemLongClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    switch (((o)this.s.getItem(paramInt)).b())
    {
    }
    for (;;)
    {
      return true;
      Toast.makeText(this, "This is how you change doubletap2wake ", 0).show();
    }
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(paramMenuItem);
    }
    ar.a(this);
    return true;
  }
  
  public void onResume()
  {
    super.onResume();
    k();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/SleeperActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */