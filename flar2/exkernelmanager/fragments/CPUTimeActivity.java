package flar2.exkernelmanager.fragments;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ad;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import flar2.exkernelmanager.AboutActivity;
import flar2.exkernelmanager.UserSettingsActivity;
import flar2.exkernelmanager.a.o;
import flar2.exkernelmanager.utilities.e;
import flar2.exkernelmanager.utilities.h;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CPUTimeActivity
  extends ad
{
  h n;
  private m o = new m();
  private e p = new e();
  private ListView q;
  private flar2.exkernelmanager.a.a r;
  private SwipeRefreshLayout s;
  private ArrayList t = new ArrayList();
  private Toolbar u;
  private View v;
  private View w;
  private int x;
  private int y;
  
  private float a(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    return Math.max(paramFloat2, Math.min(paramFloat1, paramFloat3));
  }
  
  private String a(long paramLong)
  {
    long l = 10L * paramLong;
    Object[] arrayOfObject = new Object[3];
    arrayOfObject[0] = Long.valueOf(TimeUnit.MILLISECONDS.toHours(l));
    arrayOfObject[1] = Long.valueOf(TimeUnit.MILLISECONDS.toMinutes(l) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(l)));
    arrayOfObject[2] = Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(l) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(l)));
    return String.format("%02d:%02d:%02d", arrayOfObject);
  }
  
  private void l()
  {
    new ar(this, null).execute(new Void[0]);
  }
  
  private void m()
  {
    if (!this.p.a("/sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state")) {
      return;
    }
    String[] arrayOfString = this.o.a("/sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state", false);
    this.t.clear();
    int i = arrayOfString.length;
    double d1 = 0.0D;
    int j = 0;
    while (j < i)
    {
      double d8 = d1 + Long.valueOf(Long.parseLong(arrayOfString[j].split(" ")[1])).longValue();
      j++;
      d1 = d8;
    }
    long l4;
    double d2;
    if (k.b("prefCPUTimeDeepSleep").booleanValue())
    {
      l4 = (SystemClock.elapsedRealtime() - SystemClock.uptimeMillis()) / 10L;
      d2 = d1 + l4;
    }
    for (long l1 = l4;; l1 = 0L)
    {
      ArrayList localArrayList1;
      double d3;
      double d4;
      ArrayList localArrayList2;
      long l2;
      if (k.b("prefCPUTimeSaveOffsets").booleanValue())
      {
        localArrayList1 = new ArrayList(Arrays.asList(k.a("prefCPUTimeOffsets").split(",")));
        Long localLong1 = Long.valueOf(0L);
        Iterator localIterator = localArrayList1.iterator();
        String str3;
        for (Long localLong2 = localLong1; localIterator.hasNext(); localLong2 = Long.valueOf(localLong2.longValue() + Long.parseLong(str3))) {
          str3 = (String)localIterator.next();
        }
        d3 = d2 - localLong2.longValue();
        if (k.b("prefCPUTimeDeepSleep").booleanValue())
        {
          Object localObject = Long.valueOf(0L);
          try
          {
            Long localLong4 = Long.valueOf(Long.parseLong(k.a("prefCPUTimeDeepSleepOffset")));
            localObject = localLong4;
          }
          catch (Exception localException)
          {
            for (;;)
            {
              double d7;
              long l3;
              k.a("prefCPUTimeSaveOffsets", false);
            }
          }
          d7 = d3 - ((Long)localObject).longValue();
          l3 = l1 - ((Long)localObject).longValue();
          d4 = d7;
          localArrayList2 = localArrayList1;
          l2 = l3;
        }
      }
      for (;;)
      {
        if (k.b("prefCPUTimeDeepSleep").booleanValue())
        {
          double d6 = Math.round(1000.0D * (l2 / d4));
          this.t.add(new as(this, getString(2131624024), l2, a(l2), Double.toString(d6 / 10.0D), (int)Math.round(d6 / 10.0D)));
        }
        int k = arrayOfString.length;
        int m = 0;
        int i2;
        for (int i1 = 0; m < k; i1 = i2)
        {
          String str1 = arrayOfString[m];
          String str2 = str1.split(" ")[0];
          Long localLong3 = Long.valueOf(Long.parseLong(str1.split(" ")[1]));
          if ((k.b("prefCPUTimeSaveOffsets").booleanValue()) && (localArrayList2 != null)) {
            localLong3 = Long.valueOf(localLong3.longValue() - Long.parseLong((String)localArrayList2.get(i1)));
          }
          double d5 = Math.round(1000.0D * (localLong3.longValue() / d4));
          if (localLong3.longValue() > 0L) {
            this.t.add(new as(this, this.p.c(str2), localLong3.longValue(), a(localLong3.longValue()), Double.toString(d5 / 10.0D), (int)Math.round(d5 / 10.0D)));
          }
          if (k.b("prefCPUTimeSort").booleanValue()) {
            Collections.sort(this.t);
          }
          i2 = i1 + 1;
          m++;
        }
        break;
        d4 = d3;
        localArrayList2 = localArrayList1;
        l2 = l1;
        continue;
        d4 = d2;
        l2 = l1;
        localArrayList2 = null;
      }
      d2 = d1;
    }
  }
  
  private List n()
  {
    ArrayList localArrayList = new ArrayList();
    m();
    o localo1 = new o();
    localo1.a(0);
    localo1.a(getString(2131624026));
    localArrayList.add(localo1);
    if (this.p.a("/sys/devices/system/cpu/cpu0/cpufreq/stats/time_in_state")) {
      for (int i = -1 + this.t.size(); i >= 0; i--)
      {
        as localas = (as)this.t.get(i);
        o localo4 = new o();
        localo4.a(6);
        localo4.a(localas.a);
        localo4.b(localas.b);
        localo4.c(localas.c + "%");
        localo4.d(localas.e);
        localArrayList.add(localo4);
      }
    }
    o localo2 = new o();
    localo2.a(1);
    localo2.a(getString(2131624165));
    localArrayList.add(localo2);
    o localo3 = new o();
    localo3.a(0);
    localArrayList.add(localo3);
    return localArrayList;
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    this.n.b().onTouchEvent(paramMotionEvent);
    return super.dispatchTouchEvent(paramMotionEvent);
  }
  
  public int k()
  {
    View localView = this.q.getChildAt(0);
    if (localView == null) {
      return 0;
    }
    int i = this.q.getFirstVisiblePosition();
    int j = localView.getTop();
    int k = 0;
    if (i >= 1) {
      k = this.x;
    }
    return k + (-j + i * localView.getHeight());
  }
  
  public void onBackPressed()
  {
    super.onBackPressed();
    overridePendingTransition(0, 2130968604);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903071);
    this.u = ((Toolbar)findViewById(2131492951));
    a(this.u);
    overridePendingTransition(2130968605, 17432577);
    setTitle(getString(2131624023));
    g().a(true);
    this.n = new al(this, this);
    findViewById(2131493031).setOnTouchListener(this.n);
    this.q = ((ListView)findViewById(2131492989));
    this.r = new flar2.exkernelmanager.a.a(this, new ArrayList());
    this.q.setAdapter(this.r);
    View localView = getLayoutInflater().inflate(2130903072, this.q, false);
    this.q.addHeaderView(localView);
    this.v = findViewById(2131493034);
    this.w = findViewById(2131493038);
    this.x = getResources().getDimensionPixelSize(2131427423);
    this.y = (-100 + -this.x);
    Button localButton1 = (Button)findViewById(2131493036);
    Button localButton2 = (Button)findViewById(2131493037);
    int i;
    if ((getResources().getBoolean(2131296267)) || (getResources().getBoolean(2131296266))) {
      i = 320;
    }
    for (;;)
    {
      this.s = ((SwipeRefreshLayout)findViewById(2131493032));
      this.s.a(false, 0, i);
      this.s.setProgressBackgroundColorSchemeColor(getResources().getColor(2131361829));
      this.s.setColorSchemeResources(new int[] { 2131361807, 2131361798, 2131361807 });
      this.s.setOnRefreshListener(new am(this));
      localButton1.setOnClickListener(new ao(this));
      localButton2.setOnClickListener(new ap(this));
      this.q.setOnScrollListener(new aq(this, localButton1, localButton2));
      l();
      return;
      if (getResources().getBoolean(2131296263)) {
        i = 350;
      } else if (getResources().getBoolean(2131296264)) {
        i = 530;
      } else if ((getResources().getBoolean(2131296264)) && (getResources().getBoolean(2131296263))) {
        i = 420;
      } else {
        i = 450;
      }
    }
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131755010, paramMenu);
    if (k.b("prefCPUTimeDeepSleep").booleanValue()) {
      paramMenu.findItem(2131493244).setChecked(true);
    }
    while (k.b("prefCPUTimeSort").booleanValue())
    {
      paramMenu.findItem(2131493245).setChecked(true);
      return true;
      paramMenu.findItem(2131493244).setChecked(false);
    }
    paramMenu.findItem(2131493245).setChecked(false);
    return true;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(paramMenuItem);
    case 16908332: 
      android.support.v4.app.ar.a(this);
      return true;
    case 2131493244: 
      if (paramMenuItem.isChecked())
      {
        paramMenuItem.setChecked(false);
        k.a("prefCPUTimeDeepSleep", false);
      }
      for (;;)
      {
        l();
        return true;
        paramMenuItem.setChecked(true);
        k.a("prefCPUTimeDeepSleep", true);
      }
    case 2131493245: 
      if (paramMenuItem.isChecked())
      {
        paramMenuItem.setChecked(false);
        k.a("prefCPUTimeSort", false);
      }
      for (;;)
      {
        l();
        return true;
        paramMenuItem.setChecked(true);
        k.a("prefCPUTimeSort", true);
      }
    case 2131493234: 
      startActivity(new Intent(this, UserSettingsActivity.class));
      return true;
    }
    startActivity(new Intent(this, AboutActivity.class));
    return true;
  }
  
  public void onResume()
  {
    super.onResume();
    l();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/CPUTimeActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */