package flar2.exkernelmanager.fragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;
import flar2.exkernelmanager.utilities.f;
import java.io.PrintStream;

class i
  extends AsyncTask
{
  private ProgressDialog b;
  
  private i(BackupActivity paramBackupActivity) {}
  
  protected String a(String... paramVarArgs)
  {
    return new f().e(paramVarArgs[0]);
  }
  
  protected void a(String paramString)
  {
    this.b.dismiss();
    if (!paramString.contains("records"))
    {
      Toast.makeText(this.a.getApplicationContext(), "Restore backup failed", 0).show();
      System.out.println("EXKM: " + paramString);
      this.a.setRequestedOrientation(4);
      return;
    }
    BackupActivity.e(this.a);
  }
  
  protected void onPreExecute()
  {
    super.onPreExecute();
    this.b = new ProgressDialog(this.a);
    this.b.setMessage("Restoring...");
    this.b.setCancelable(false);
    this.b.show();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/i.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */