package flar2.exkernelmanager.fragments;

import android.os.CountDownTimer;
import android.support.v7.app.ab;

class g
  extends CountDownTimer
{
  g(BackupActivity paramBackupActivity, long paramLong1, long paramLong2, ab paramab)
  {
    super(paramLong1, paramLong2);
  }
  
  public void onFinish()
  {
    this.a.dismiss();
    if (!BackupActivity.c(this.b)) {
      BackupActivity.d(this.b);
    }
  }
  
  public void onTick(long paramLong)
  {
    this.a.a("System will reboot in " + paramLong / 1000L + " seconds");
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/g.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */