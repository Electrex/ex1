package flar2.exkernelmanager.fragments;

import android.os.Bundle;
import android.support.v4.app.ar;
import android.support.v7.app.ab;
import android.support.v7.app.ac;
import android.support.v7.app.ad;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;
import flar2.exkernelmanager.a.o;
import flar2.exkernelmanager.r;
import flar2.exkernelmanager.utilities.e;
import flar2.exkernelmanager.utilities.f;
import flar2.exkernelmanager.utilities.h;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;
import java.util.ArrayList;
import java.util.List;

public class ModesActivity
  extends ad
  implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener
{
  int n;
  h o;
  private m p = new m();
  private e q = new e();
  private f r = new f();
  private ListView s;
  private flar2.exkernelmanager.a.a t;
  
  private void a(String paramString1, String paramString2)
  {
    ac localac = new ac(this);
    localac.a(getString(2131624205));
    localac.b(2131623999, null);
    localac.a(this.q.a(paramString2, 1, 0), new dw(this, this.q.a(paramString2, 0, 0), paramString1));
    localac.a().show();
  }
  
  private void b(String paramString)
  {
    if (k.b(paramString).booleanValue()) {
      k.a(paramString, false);
    }
    for (;;)
    {
      k();
      return;
      k.a(paramString, true);
    }
  }
  
  private void k()
  {
    new dx(this, null).execute(new Void[0]);
  }
  
  private void l()
  {
    ac localac = new ac(this);
    localac.a(getString(2131624206));
    localac.b(2131623999, null);
    String[] arrayOfString = this.q.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors", 0, 0);
    localac.a(arrayOfString, new dv(this, arrayOfString));
    localac.a().show();
  }
  
  private List m()
  {
    ArrayList localArrayList = new ArrayList();
    o localo4;
    o localo5;
    label330:
    label352:
    o localo6;
    if (!k.a("prefSubVersion").equals("Sense"))
    {
      o localo1 = new o();
      localo1.a(0);
      localo1.a(getString(2131624184));
      localArrayList.add(localo1);
      o localo2 = new o();
      localo2.a(1);
      localo2.b(64935);
      localo2.a(getString(2131624130));
      localo2.b(this.q.c(k.a("prefCPUMaxPrefPS")));
      localArrayList.add(localo2);
      o localo3 = new o();
      localo3.a(1);
      localo3.b(64934);
      localo3.a(getString(2131624131));
      localo3.b(this.q.c(k.a("prefGPUMaxPrefPS")));
      localArrayList.add(localo3);
      if (this.q.a(r.i[this.p.a(r.i)]))
      {
        localo4 = new o();
        localo4.a(1);
        localo4.b(64933);
        localo4.a(getString(2131624137));
        if (!k.b("prefDimmerPS").booleanValue()) {
          break label704;
        }
        localo4.b(getString(2131624077));
        localArrayList.add(localo4);
      }
      if (this.q.a(r.k[this.p.a(r.k)]))
      {
        localo5 = new o();
        localo5.a(1);
        localo5.b(64932);
        if (!k.b("prefHTC").booleanValue()) {
          break label718;
        }
        localo5.a(getString(2131624190));
        if (!k.b("prefVibOptPS").booleanValue()) {
          break label732;
        }
        localo5.b(getString(2131624077));
        localArrayList.add(localo5);
      }
      if ((k.a("prefDeviceName").equals(getString(2131624153))) || (k.a("prefDeviceName").equals(getString(2131624157))))
      {
        localo6 = new o();
        localo6.a(1);
        localo6.b(64936);
        localo6.a(getString(2131624271));
        if (!k.b("prefWakePS").booleanValue()) {
          break label746;
        }
        localo6.b(getString(2131624077));
      }
    }
    for (;;)
    {
      localArrayList.add(localo6);
      o localo7 = new o();
      localo7.a(0);
      localo7.a(getString(2131624175));
      localArrayList.add(localo7);
      o localo8 = new o();
      localo8.a(1);
      localo8.b(64931);
      localo8.a(getString(2131624141));
      localo8.b(this.q.c(k.a("prefCPUMaxPrefPF")));
      localArrayList.add(localo8);
      o localo9 = new o();
      localo9.a(1);
      localo9.b(64930);
      localo9.a(getString(2131624142));
      localo9.b(this.q.c(k.a("prefGPUMaxPrefPF")));
      localArrayList.add(localo9);
      if ((!k.a("prefDeviceName").equals(getString(2131624159))) && (!k.a("prefDeviceName").equals(getString(2131624155))))
      {
        o localo10 = new o();
        localo10.a(1);
        localo10.b(64929);
        localo10.a(getString(2131624016));
        localo10.b(k.a("prefCPUGovPF"));
        localArrayList.add(localo10);
      }
      return localArrayList;
      label704:
      localo4.b(getString(2131624058));
      break;
      label718:
      localo5.a(getString(2131624272));
      break label330;
      label732:
      localo5.b(getString(2131624058));
      break label352;
      label746:
      localo6.b(getString(2131624058));
    }
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    this.o.b().onTouchEvent(paramMotionEvent);
    return super.dispatchTouchEvent(paramMotionEvent);
  }
  
  public void onBackPressed()
  {
    super.onBackPressed();
    overridePendingTransition(0, 2130968604);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903075);
    overridePendingTransition(2130968605, 17432577);
    a((Toolbar)findViewById(2131492951));
    setTitle(getString(2131623967));
    g().a(true);
    this.o = new dt(this, this);
    findViewById(2131493050).setOnTouchListener(this.o);
    this.s = ((ListView)findViewById(2131492989));
    this.t = new flar2.exkernelmanager.a.a(this, new ArrayList());
    this.s.setAdapter(this.t);
    this.s.setOnItemClickListener(this);
    this.s.setOnItemLongClickListener(this);
    this.s.setOnScrollListener(new du(this));
    this.n = this.p.a(r.e);
    if (!k.e("prefCPUMaxPrefPF")) {
      k.a("prefCPUMaxPrefPF", this.p.a("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq"));
    }
    String[] arrayOfString = this.q.a(r.g[this.n], 0, 0);
    if (!k.e("prefGPUMaxPrefPF"))
    {
      if (!k.a("prefDeviceName").equals(getString(2131624159))) {
        break label294;
      }
      if (Integer.parseInt(this.p.a(r.e[this.n])) >= 852000000) {
        break label271;
      }
      k.a("prefGPUMaxPrefPF", arrayOfString[14]);
    }
    for (;;)
    {
      k();
      return;
      label271:
      k.a("prefGPUMaxPrefPF", this.p.a(r.e[this.n]));
      continue;
      label294:
      if (k.a("prefDeviceName").equals(getString(2131624157)))
      {
        if (arrayOfString.length > 3) {
          k.a("prefGPUMaxPrefPF", arrayOfString[4]);
        } else {
          k.a("prefGPUMaxPrefPF", arrayOfString[0]);
        }
      }
      else if (k.a("prefDeviceName").equals(getString(2131624111)))
      {
        if (arrayOfString.length > 3) {
          k.a("prefGPUMaxPrefPF", arrayOfString[5]);
        } else {
          k.a("prefGPUMaxPrefPF", arrayOfString[0]);
        }
      }
      else {
        k.a("prefGPUMaxPrefPF", arrayOfString[0]);
      }
    }
  }
  
  public void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    switch (((o)this.t.getItem(paramInt)).b())
    {
    default: 
      return;
    case -601: 
      a("prefCPUMaxPrefPS", "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies");
      return;
    case -602: 
      a("prefGPUMaxPrefPS", r.g[this.n]);
      return;
    case -603: 
      b("prefDimmerPS");
      return;
    case -604: 
      b("prefVibOptPS");
      return;
    case -600: 
      b("prefWakePS");
      return;
    case -605: 
      a("prefCPUMaxPrefPF", "/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies");
      return;
    case -606: 
      a("prefGPUMaxPrefPF", r.g[this.n]);
      return;
    }
    l();
  }
  
  public boolean onItemLongClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    switch (((o)this.t.getItem(paramInt)).b())
    {
    }
    for (;;)
    {
      return true;
      Toast.makeText(this, "This is how you change doubletap2wake ", 0).show();
    }
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(paramMenuItem);
    }
    ar.a(this);
    return true;
  }
  
  public void onResume()
  {
    super.onResume();
    k();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/ModesActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */