package flar2.exkernelmanager.fragments;

import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;
import flar2.exkernelmanager.utilities.n;

class au
  implements SeekBar.OnSeekBarChangeListener
{
  au(ColorActivity paramColorActivity) {}
  
  public void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean)
  {
    ColorActivity.i(this.a).a(paramInt, n.b);
    ColorActivity.k(this.a).setText(Integer.toString(paramInt));
    k.a("prefGreen", Integer.toString(paramInt));
  }
  
  public void onStartTrackingTouch(SeekBar paramSeekBar) {}
  
  public void onStopTrackingTouch(SeekBar paramSeekBar) {}
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/au.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */