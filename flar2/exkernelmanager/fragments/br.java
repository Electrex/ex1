package flar2.exkernelmanager.fragments;

import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

class br
  implements DialogInterface.OnDismissListener
{
  br(ColorActivity paramColorActivity) {}
  
  public void onDismiss(DialogInterface paramDialogInterface)
  {
    InputMethodManager localInputMethodManager = (InputMethodManager)this.a.getSystemService("input_method");
    if (localInputMethodManager.isActive()) {
      localInputMethodManager.hideSoftInputFromWindow(this.a.getCurrentFocus().getWindowToken(), 0);
    }
    ColorActivity.g(this.a).setVisibility(0);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/br.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */