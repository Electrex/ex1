package flar2.exkernelmanager.fragments;

import android.os.Bundle;
import android.support.v4.app.ar;
import android.support.v7.app.ab;
import android.support.v7.app.ac;
import android.support.v7.app.ad;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import flar2.exkernelmanager.a.o;
import flar2.exkernelmanager.utilities.e;
import flar2.exkernelmanager.utilities.f;
import flar2.exkernelmanager.utilities.h;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;
import java.util.ArrayList;
import java.util.List;

public class GovernorActivity
  extends ad
  implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener
{
  h n;
  private m o = new m();
  private e p = new e();
  private f q = new f();
  private String r = null;
  private ListView s;
  private flar2.exkernelmanager.a.a t;
  private String u;
  
  private void a(String paramString1, String paramString2)
  {
    ac localac = new ac(this);
    localac.a(getString(2131624091));
    localac.b(2131623999, null);
    EditText localEditText = new EditText(this);
    localac.a(localEditText);
    localEditText.setHint(this.o.a(paramString2));
    localEditText.setInputType(2);
    localac.a(2131624170, new cg(this, localEditText, paramString1, paramString2));
    ab localab = localac.a();
    localab.getWindow().setSoftInputMode(5);
    localab.show();
  }
  
  private void b(String paramString1, String paramString2)
  {
    k.a(paramString1 + "Boot", false);
    if (this.o.a(paramString2).equals("0"))
    {
      this.o.a("1", paramString2);
      k.a(paramString1, "1");
    }
    for (;;)
    {
      k();
      return;
      if (this.o.a(paramString2).equals("1"))
      {
        this.o.a("0", paramString2);
        k.a(paramString1, "0");
      }
      else if (this.o.a(paramString2).equals("Y"))
      {
        this.o.a("N", paramString2);
        k.a(paramString1, "N");
      }
      else if (this.o.a(paramString2).equals("N"))
      {
        this.o.a("Y", paramString2);
        k.a(paramString1, "Y");
      }
    }
  }
  
  private void c(String paramString1, String paramString2)
  {
    ac localac = new ac(this);
    localac.a(getString(2131624205));
    localac.b(2131623999, null);
    localac.a(this.p.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies", 1, 0), new ch(this, this.p.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies", 0, 0), paramString1, paramString2));
    localac.a().show();
  }
  
  private void d(String paramString1, String paramString2)
  {
    View localView = getLayoutInflater().inflate(2130903105, null);
    Spinner localSpinner1 = (Spinner)localView.findViewById(2131493199);
    Spinner localSpinner2 = (Spinner)localView.findViewById(2131493200);
    Spinner localSpinner3 = (Spinner)localView.findViewById(2131493201);
    Spinner localSpinner4 = (Spinner)localView.findViewById(2131493202);
    String[] arrayOfString1 = this.p.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies", 1, 0);
    String[] arrayOfString2 = this.p.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies", 0, 0);
    ArrayAdapter localArrayAdapter = new ArrayAdapter(this, 17367049, arrayOfString1);
    localSpinner1.setAdapter(localArrayAdapter);
    localSpinner2.setAdapter(localArrayAdapter);
    localSpinner3.setAdapter(localArrayAdapter);
    localSpinner4.setAdapter(localArrayAdapter);
    String str1 = this.o.a(paramString2);
    String[] arrayOfString3 = str1.split(",");
    int i = 0;
    int j = arrayOfString2.length;
    for (int k = 0; k < j; k++)
    {
      String str2 = arrayOfString2[k];
      if (str2.equals(arrayOfString3[0])) {
        localSpinner1.setSelection(i);
      }
      if (str2.equals(arrayOfString3[1])) {
        localSpinner2.setSelection(i);
      }
      if (str2.equals(arrayOfString3[2])) {
        localSpinner3.setSelection(i);
      }
      if (str2.equals(arrayOfString3[3])) {
        localSpinner4.setSelection(i);
      }
      i++;
    }
    localSpinner1.setOnItemSelectedListener(new ci(this, arrayOfString2, paramString2));
    localSpinner2.setOnItemSelectedListener(new cj(this, paramString2, arrayOfString2));
    localSpinner3.setOnItemSelectedListener(new ck(this, paramString2, arrayOfString2));
    localSpinner4.setOnItemSelectedListener(new cl(this, paramString2, arrayOfString2));
    ac localac = new ac(this);
    localac.a(getString(2131624204)).a(localView).a(getString(2131624170), new ce(this, paramString1, paramString2)).b(2131623999, new cm(this, str1, paramString2, paramString1));
    localac.a().show();
  }
  
  private void k()
  {
    new cn(this, null).execute(new Void[0]);
  }
  
  private void l()
  {
    new f().c("chmod 666 " + this.u + "/*");
  }
  
  private List m()
  {
    ArrayList localArrayList = new ArrayList();
    o localo1 = new o();
    localo1.a(0);
    localo1.a(getString(2131624014));
    localArrayList.add(localo1);
    o localo2;
    label187:
    o localo3;
    label419:
    o localo4;
    label568:
    o localo5;
    label717:
    o localo6;
    label866:
    o localo7;
    label1015:
    o localo8;
    label1175:
    o localo9;
    label1316:
    o localo10;
    String[] arrayOfString2;
    label1521:
    label1555:
    o localo11;
    label1696:
    o localo12;
    label1837:
    o localo37;
    label2000:
    o localo38;
    label2148:
    o localo13;
    label2289:
    o localo14;
    label2431:
    o localo15;
    label2580:
    o localo16;
    label2722:
    o localo17;
    label2864:
    o localo18;
    label3006:
    o localo19;
    label3148:
    o localo20;
    label3290:
    o localo21;
    label3432:
    o localo22;
    label3581:
    o localo23;
    label3723:
    o localo24;
    label3865:
    o localo25;
    label4007:
    o localo26;
    label4149:
    o localo27;
    label4291:
    o localo28;
    label4440:
    o localo29;
    label4582:
    o localo30;
    label4731:
    o localo31;
    label4873:
    o localo32;
    label5015:
    o localo33;
    label5157:
    o localo34;
    label5299:
    o localo35;
    label5441:
    o localo36;
    if (this.p.a(this.u + "gboost"))
    {
      localo2 = new o();
      localo2.a(1);
      localo2.b(-70);
      localo2.a("gboost");
      if (this.o.a(this.u + "gboost").equals("0"))
      {
        localo2.b(getString(2131624058));
        localo2.d("prefGBoostBoot");
        if (!k.b("prefGBoostBoot").booleanValue()) {
          break label5609;
        }
        localo2.a(true);
        localo2.c(2130837611);
        localArrayList.add(localo2);
      }
    }
    else
    {
      if (this.p.a(this.u + "input_event_min_freq"))
      {
        localo3 = new o();
        localo3.a(1);
        localo3.b(-72);
        localo3.a("input_event_min_freq");
        String[] arrayOfString1 = this.o.a(this.u + "input_event_min_freq").split(",");
        localo3.b(this.p.c(arrayOfString1[0]) + ", " + this.p.c(arrayOfString1[1]) + ", " + this.p.c(arrayOfString1[2]) + ", " + this.p.c(arrayOfString1[3]));
        localo3.d("prefInputMinFreqBoot");
        if (!k.b("prefInputMinFreqBoot").booleanValue()) {
          break label5626;
        }
        localo3.a(true);
        localo3.c(2130837611);
        localArrayList.add(localo3);
      }
      if (this.p.a(this.u + "active_floor_freq"))
      {
        localo4 = new o();
        localo4.a(1);
        localo4.b(64610);
        localo4.a("active_floor_freq");
        localo4.b(this.p.c(this.o.a(this.u + "active_floor_freq")));
        localo4.d("prefActiveFloorFreqBoot");
        if (!k.b("prefActiveFloorFreqBoot").booleanValue()) {
          break label5643;
        }
        localo4.a(true);
        localo4.c(2130837611);
        localArrayList.add(localo4);
      }
      if (this.p.a(this.u + "input_min_freq"))
      {
        localo5 = new o();
        localo5.a(1);
        localo5.b(64613);
        localo5.a("input_min_freq");
        localo5.b(this.p.c(this.o.a(this.u + "input_min_freq")));
        localo5.d("PREF_INPUT_MIN_FREQ2Boot");
        if (!k.b("PREF_INPUT_MIN_FREQ2Boot").booleanValue()) {
          break label5660;
        }
        localo5.a(true);
        localo5.c(2130837611);
        localArrayList.add(localo5);
      }
      if (this.p.a(this.u + "gboost_min_freq"))
      {
        localo6 = new o();
        localo6.a(1);
        localo6.b(64612);
        localo6.a("gboost_min_freq");
        localo6.b(this.p.c(this.o.a(this.u + "gboost_min_freq")));
        localo6.d("PREF_GBOOST_MIN_FREQBoot");
        if (!k.b("PREF_GBOOST_MIN_FREQBoot").booleanValue()) {
          break label5677;
        }
        localo6.a(true);
        localo6.c(2130837611);
        localArrayList.add(localo6);
      }
      if (this.p.a(this.u + "max_screen_off_freq"))
      {
        localo7 = new o();
        localo7.a(1);
        localo7.b(64611);
        localo7.a("max_screen_off_freq");
        localo7.b(this.p.c(this.o.a(this.u + "max_screen_off_freq")));
        localo7.d("PREF_MAX_SCREEN_OFF_FREQBoot");
        if (!k.b("PREF_MAX_SCREEN_OFF_FREQBoot").booleanValue()) {
          break label5694;
        }
        localo7.a(true);
        localo7.c(2130837611);
        localArrayList.add(localo7);
      }
      if (this.p.a(this.u + "input_event_timeout"))
      {
        localo8 = new o();
        localo8.a(1);
        localo8.b(-73);
        localo8.a("input_event_timeout");
        localo8.b(this.o.a(new StringBuilder().append(this.u).append("input_event_timeout").toString()) + "ms");
        localo8.d("prefInputTimeoutBoot");
        if (!k.b("prefInputTimeoutBoot").booleanValue()) {
          break label5711;
        }
        localo8.a(true);
        localo8.c(2130837611);
        localArrayList.add(localo8);
      }
      if (this.p.a(this.u + "ui_sampling_rate"))
      {
        localo9 = new o();
        localo9.a(1);
        localo9.b(-74);
        localo9.a("ui_sampling_rate");
        localo9.b(this.o.a(this.u + "ui_sampling_rate"));
        localo9.d("prefUISamplingRateBoot");
        if (!k.b("prefUISamplingRateBoot").booleanValue()) {
          break label5728;
        }
        localo9.a(true);
        localo9.c(2130837611);
        localArrayList.add(localo9);
      }
      if (this.p.a(this.u + "two_phase_freq"))
      {
        localo10 = new o();
        localo10.a(1);
        localo10.b(-75);
        localo10.a("two_phase_freq");
        arrayOfString2 = this.o.a(this.u + "two_phase_freq").split(",");
        if (arrayOfString2.length <= 1) {
          break label5745;
        }
        localo10.b(this.p.c(arrayOfString2[0]) + ", " + this.p.c(arrayOfString2[1]) + ", " + this.p.c(arrayOfString2[2]) + ", " + this.p.c(arrayOfString2[3]));
        localo10.d("prefTwoPhaseFreqBoot");
        if (!k.b("prefTwoPhaseFreqBoot").booleanValue()) {
          break label5764;
        }
        localo10.a(true);
        localo10.c(2130837611);
        localArrayList.add(localo10);
      }
      if (this.p.a(this.u + "up_threshold"))
      {
        localo11 = new o();
        localo11.a(1);
        localo11.b(-76);
        localo11.a("up_threshold");
        localo11.b(this.o.a(this.u + "up_threshold"));
        localo11.d("prefUpThresholdBoot");
        if (!k.b("prefUpThresholdBoot").booleanValue()) {
          break label5781;
        }
        localo11.a(true);
        localo11.c(2130837611);
        localArrayList.add(localo11);
      }
      if (this.p.a(this.u + "down_differential"))
      {
        localo12 = new o();
        localo12.a(1);
        localo12.b(-77);
        localo12.a("down_differential");
        localo12.b(this.o.a(this.u + "down_differential"));
        localo12.d("prefDownDifferentialBoot");
        if (!k.b("prefDownDifferentialBoot").booleanValue()) {
          break label5798;
        }
        localo12.a(true);
        localo12.c(2130837611);
        localArrayList.add(localo12);
      }
      if (!k.a("prefDeviceName").contains("HTC_One_m"))
      {
        if (this.p.a(this.u + "sync_freq"))
        {
          localo37 = new o();
          localo37.a(1);
          localo37.b(-78);
          localo37.a("sync_freq");
          localo37.b(this.p.c(this.o.a(this.u + "sync_freq")));
          localo37.d("prefSyncFreqBoot");
          if (!k.b("prefSyncFreqBoot").booleanValue()) {
            break label5815;
          }
          localo37.a(true);
          localo37.c(2130837611);
          localArrayList.add(localo37);
        }
        if (this.p.a(this.u + "optimal_freq"))
        {
          localo38 = new o();
          localo38.a(1);
          localo38.b(-79);
          localo38.a("optimal_freq");
          localo38.b(this.p.c(this.o.a(this.u + "optimal_freq")));
          localo38.d("prefOptFreqBoot");
          if (!k.b("prefOptFreqBoot").booleanValue()) {
            break label5832;
          }
          localo38.a(true);
          localo38.c(2130837611);
          localArrayList.add(localo38);
        }
      }
      if (this.p.a(this.u + "shortcut"))
      {
        localo13 = new o();
        localo13.a(1);
        localo13.b(-71);
        localo13.a("shortcut");
        localo13.b(this.o.a(this.u + "shortcut"));
        localo13.d("prefShortcutBoot");
        if (!k.b("prefShortcutBoot").booleanValue()) {
          break label5849;
        }
        localo13.a(true);
        localo13.c(2130837611);
        localArrayList.add(localo13);
      }
      if (this.p.a(this.u + "sampling_rate_min"))
      {
        localo14 = new o();
        localo14.a(1);
        localo14.b(64636);
        localo14.a("sampling_rate_min");
        localo14.b(this.o.a(this.u + "sampling_rate_min"));
        localo14.d("PREF_SAMPLING_RATE_MINBoot");
        if (!k.b("PREF_SAMPLING_RATE_MINBoot").booleanValue()) {
          break label5866;
        }
        localo14.a(true);
        localo14.c(2130837611);
        localArrayList.add(localo14);
      }
      if (this.p.a(this.u + "freq_down_step_barriar"))
      {
        localo15 = new o();
        localo15.a(1);
        localo15.b(64635);
        localo15.a("freq_down_step_barrier");
        localo15.b(this.p.c(this.o.a(this.u + "freq_down_step_barriar")));
        localo15.d("PREF_FREQ_DOWN_STEP_BARRIARBoot");
        if (!k.b("PREF_FREQ_DOWN_STEP_BARRIARBoot").booleanValue()) {
          break label5883;
        }
        localo15.a(true);
        localo15.c(2130837611);
        localArrayList.add(localo15);
      }
      if (this.p.a(this.u + "freq_down_step"))
      {
        localo16 = new o();
        localo16.a(1);
        localo16.b(64634);
        localo16.a("freq_down_step");
        localo16.b(this.o.a(this.u + "freq_down_step"));
        localo16.d("PREF_FREQ_DOWN_STEPBoot");
        if (!k.b("PREF_FREQ_DOWN_STEPBoot").booleanValue()) {
          break label5900;
        }
        localo16.a(true);
        localo16.c(2130837611);
        localArrayList.add(localo16);
      }
      if (this.p.a(this.u + "sampling_rate"))
      {
        localo17 = new o();
        localo17.a(1);
        localo17.b(64633);
        localo17.a("sampling_rate");
        localo17.b(this.o.a(this.u + "sampling_rate"));
        localo17.d("PREF_SAMPLING_RATEBoot");
        if (!k.b("PREF_SAMPLING_RATEBoot").booleanValue()) {
          break label5917;
        }
        localo17.a(true);
        localo17.c(2130837611);
        localArrayList.add(localo17);
      }
      if (this.p.a(this.u + "sampling_down_factor"))
      {
        localo18 = new o();
        localo18.a(1);
        localo18.b(64632);
        localo18.a("sampling_down_factor");
        localo18.b(this.o.a(this.u + "sampling_down_factor"));
        localo18.d("PREF_SAMPLING_DOWN_FACTORBoot");
        if (!k.b("PREF_SAMPLING_DOWN_FACTORBoot").booleanValue()) {
          break label5934;
        }
        localo18.a(true);
        localo18.c(2130837611);
        localArrayList.add(localo18);
      }
      if (this.p.a(this.u + "up_threshold_any_cpu_load"))
      {
        localo19 = new o();
        localo19.a(1);
        localo19.b(64630);
        localo19.a("up_threshold_any_cpu_load");
        localo19.b(this.o.a(this.u + "up_threshold_any_cpu_load"));
        localo19.d("PREF_UP_THRESHOLD_ANY_CPU_LOADBoot");
        if (!k.b("PREF_UP_THRESHOLD_ANY_CPU_LOADBoot").booleanValue()) {
          break label5951;
        }
        localo19.a(true);
        localo19.c(2130837611);
        localArrayList.add(localo19);
      }
      if (this.p.a(this.u + "target_loads"))
      {
        localo20 = new o();
        localo20.a(1);
        localo20.b(64629);
        localo20.a("target_loads");
        localo20.b(this.o.a(this.u + "target_loads"));
        localo20.d("PREF_TARGET_LOADSBoot");
        if (!k.b("PREF_TARGET_LOADSBoot").booleanValue()) {
          break label5968;
        }
        localo20.a(true);
        localo20.c(2130837611);
        localArrayList.add(localo20);
      }
      if (this.p.a(this.u + "timer_slack"))
      {
        localo21 = new o();
        localo21.a(1);
        localo21.b(64628);
        localo21.a("timer_slack");
        localo21.b(this.o.a(this.u + "timer_slack"));
        localo21.d("PREF_TIMER_SLACKBoot");
        if (!k.b("PREF_TIMER_SLACKBoot").booleanValue()) {
          break label5985;
        }
        localo21.a(true);
        localo21.c(2130837611);
        localArrayList.add(localo21);
      }
      if (this.p.a(this.u + "hispeed_freq"))
      {
        localo22 = new o();
        localo22.a(1);
        localo22.b(64626);
        localo22.a("hispeed_freq");
        localo22.b(this.p.c(this.o.a(this.u + "hispeed_freq")));
        localo22.d("PREF_HISPEED_FREQBoot");
        if (!k.b("PREF_HISPEED_FREQBoot").booleanValue()) {
          break label6002;
        }
        localo22.a(true);
        localo22.c(2130837611);
        localArrayList.add(localo22);
      }
      if (this.p.a(this.u + "timer_rate"))
      {
        localo23 = new o();
        localo23.a(1);
        localo23.b(64625);
        localo23.a("timer_rate");
        localo23.b(this.o.a(this.u + "timer_rate"));
        localo23.d("PREF_TIMER_RATEBoot");
        if (!k.b("PREF_TIMER_RATEBoot").booleanValue()) {
          break label6019;
        }
        localo23.a(true);
        localo23.c(2130837611);
        localArrayList.add(localo23);
      }
      if (this.p.a(this.u + "above_hispeed_delay"))
      {
        localo24 = new o();
        localo24.a(1);
        localo24.b(64624);
        localo24.a("above_hispeed_delay");
        localo24.b(this.o.a(this.u + "above_hispeed_delay"));
        localo24.d("PREF_ABOVE_HISPEED_DELAYBoot");
        if (!k.b("PREF_ABOVE_HISPEED_DELAYBoot").booleanValue()) {
          break label6036;
        }
        localo24.a(true);
        localo24.c(2130837611);
        localArrayList.add(localo24);
      }
      if (this.p.a(this.u + "boostpulse"))
      {
        localo25 = new o();
        localo25.a(1);
        localo25.b(64623);
        localo25.a("boostpulse");
        localo25.b(this.o.a(this.u + "boostpulse"));
        localo25.d("PREF_BOOSTPULSEBoot");
        if (!k.b("PREF_BOOSTPULSEBoot").booleanValue()) {
          break label6053;
        }
        localo25.a(true);
        localo25.c(2130837611);
        localArrayList.add(localo25);
      }
      if (this.p.a(this.u + "boostpulse_duration"))
      {
        localo26 = new o();
        localo26.a(1);
        localo26.b(64622);
        localo26.a("boostpulse_duration");
        localo26.b(this.o.a(this.u + "boostpulse_duration"));
        localo26.d("PREF_BOOSTPULSE_DURATIONBoot");
        if (!k.b("PREF_BOOSTPULSE_DURATIONBoot").booleanValue()) {
          break label6070;
        }
        localo26.a(true);
        localo26.c(2130837611);
        localArrayList.add(localo26);
      }
      if (this.p.a(this.u + "go_hispeed_load"))
      {
        localo27 = new o();
        localo27.a(1);
        localo27.b(64621);
        localo27.a("go_hispeed_load");
        localo27.b(this.o.a(this.u + "go_hispeed_load"));
        localo27.d("PREF_GO_HISPEED_LOADBoot");
        if (!k.b("PREF_GO_HISPEED_LOADBoot").booleanValue()) {
          break label6087;
        }
        localo27.a(true);
        localo27.c(2130837611);
        localArrayList.add(localo27);
      }
      if (this.p.a(this.u + "input_boost_freq"))
      {
        localo28 = new o();
        localo28.a(1);
        localo28.b(64620);
        localo28.a("input_boost_freq");
        localo28.b(this.p.c(this.o.a(this.u + "input_boost_freq")));
        localo28.d("PREF_INPUT_BOOST_FREQBoot");
        if (!k.b("PREF_INPUT_BOOST_FREQBoot").booleanValue()) {
          break label6104;
        }
        localo28.a(true);
        localo28.c(2130837611);
        localArrayList.add(localo28);
      }
      if (this.p.a(this.u + "min_sample_time"))
      {
        localo29 = new o();
        localo29.a(1);
        localo29.b(64619);
        localo29.a("min_sample_time");
        localo29.b(this.o.a(this.u + "min_sample_time"));
        localo29.d("PREF_MIN_SAMPLE_TIMEBoot");
        if (!k.b("PREF_MIN_SAMPLE_TIMEBoot").booleanValue()) {
          break label6121;
        }
        localo29.a(true);
        localo29.c(2130837611);
        localArrayList.add(localo29);
      }
      if (this.p.a(this.u + "up_threshold_any_cpu_freq"))
      {
        localo30 = new o();
        localo30.a(1);
        localo30.b(64618);
        localo30.a("up_threshold_any_cpu_freq");
        localo30.b(this.p.c(this.o.a(this.u + "up_threshold_any_cpu_freq")));
        localo30.d("PREF_UP_THRESHOLD_ANY_CPU_FREQBoot");
        if (!k.b("PREF_UP_THRESHOLD_ANY_CPU_FREQBoot").booleanValue()) {
          break label6138;
        }
        localo30.a(true);
        localo30.c(2130837611);
        localArrayList.add(localo30);
      }
      if (this.p.a(this.u + "down_threshold"))
      {
        localo31 = new o();
        localo31.a(1);
        localo31.b(64616);
        localo31.a("down_threshold");
        localo31.b(this.o.a(this.u + "down_threshold"));
        localo31.d("PREF_DOWN_THRESHOLDBoot");
        if (!k.b("PREF_DOWN_THRESHOLDBoot").booleanValue()) {
          break label6155;
        }
        localo31.a(true);
        localo31.c(2130837611);
        localArrayList.add(localo31);
      }
      if (this.p.a(this.u + "freq_step"))
      {
        localo32 = new o();
        localo32.a(1);
        localo32.b(64615);
        localo32.a("freq_step");
        localo32.b(this.o.a(this.u + "freq_step"));
        localo32.d("PREF_FREQ_STEPBoot");
        if (!k.b("PREF_FREQ_STEPBoot").booleanValue()) {
          break label6172;
        }
        localo32.a(true);
        localo32.c(2130837611);
        localArrayList.add(localo32);
      }
      if (this.p.a(this.u + "boost"))
      {
        localo33 = new o();
        localo33.a(1);
        localo33.b(64614);
        localo33.a("boost");
        localo33.b(this.o.a(this.u + "boost"));
        localo33.d("PREF_BOOSTBoot");
        if (!k.b("PREF_BOOSTBoot").booleanValue()) {
          break label6189;
        }
        localo33.a(true);
        localo33.c(2130837611);
        localArrayList.add(localo33);
      }
      if (this.p.a(this.u + "input_boost_duration"))
      {
        localo34 = new o();
        localo34.a(1);
        localo34.b(64607);
        localo34.a("input boost duration");
        localo34.b(this.o.a(this.u + "input_boost_duration"));
        localo34.d("prefInputBoostDurationBoot");
        if (!k.b("prefInputBoostDurationBoot").booleanValue()) {
          break label6206;
        }
        localo34.a(true);
        localo34.c(2130837611);
        localArrayList.add(localo34);
      }
      if (this.p.a(this.u + "low_load_down_threshold"))
      {
        localo35 = new o();
        localo35.a(1);
        localo35.b(64609);
        localo35.a("low load down threshold");
        localo35.b(this.o.a(this.u + "low_load_down_threshold"));
        localo35.d("prefLowLoadDownThresholdBoot");
        if (!k.b("prefLowLoadDownThresholdBoot").booleanValue()) {
          break label6223;
        }
        localo35.a(true);
        localo35.c(2130837611);
        localArrayList.add(localo35);
      }
      if (this.p.a(this.u + "max_freq_hysteresis"))
      {
        localo36 = new o();
        localo36.a(1);
        localo36.b(64608);
        localo36.a("max freq hysteresis");
        localo36.b(this.o.a(this.u + "max_freq_hysteresis"));
        localo36.d("prefMaxFreqHysteresisBoot");
        if (!k.b("prefMaxFreqHysteresisBoot").booleanValue()) {
          break label6240;
        }
        localo36.a(true);
        localo36.c(2130837611);
      }
    }
    for (;;)
    {
      localArrayList.add(localo36);
      return localArrayList;
      localo2.b(getString(2131624077));
      break;
      label5609:
      localo2.a(false);
      localo2.c(2130837609);
      break label187;
      label5626:
      localo3.a(false);
      localo3.c(2130837609);
      break label419;
      label5643:
      localo4.a(false);
      localo4.c(2130837609);
      break label568;
      label5660:
      localo5.a(false);
      localo5.c(2130837609);
      break label717;
      label5677:
      localo6.a(false);
      localo6.c(2130837609);
      break label866;
      label5694:
      localo7.a(false);
      localo7.c(2130837609);
      break label1015;
      label5711:
      localo8.a(false);
      localo8.c(2130837609);
      break label1175;
      label5728:
      localo9.a(false);
      localo9.c(2130837609);
      break label1316;
      label5745:
      localo10.b(this.p.c(arrayOfString2[0]));
      break label1521;
      label5764:
      localo10.a(false);
      localo10.c(2130837609);
      break label1555;
      label5781:
      localo11.a(false);
      localo11.c(2130837609);
      break label1696;
      label5798:
      localo12.a(false);
      localo12.c(2130837609);
      break label1837;
      label5815:
      localo37.a(false);
      localo37.c(2130837609);
      break label2000;
      label5832:
      localo38.a(false);
      localo38.c(2130837609);
      break label2148;
      label5849:
      localo13.a(false);
      localo13.c(2130837609);
      break label2289;
      label5866:
      localo14.a(false);
      localo14.c(2130837609);
      break label2431;
      label5883:
      localo15.a(false);
      localo15.c(2130837609);
      break label2580;
      label5900:
      localo16.a(false);
      localo16.c(2130837609);
      break label2722;
      label5917:
      localo17.a(false);
      localo17.c(2130837609);
      break label2864;
      label5934:
      localo18.a(false);
      localo18.c(2130837609);
      break label3006;
      label5951:
      localo19.a(false);
      localo19.c(2130837609);
      break label3148;
      label5968:
      localo20.a(false);
      localo20.c(2130837609);
      break label3290;
      label5985:
      localo21.a(false);
      localo21.c(2130837609);
      break label3432;
      label6002:
      localo22.a(false);
      localo22.c(2130837609);
      break label3581;
      label6019:
      localo23.a(false);
      localo23.c(2130837609);
      break label3723;
      label6036:
      localo24.a(false);
      localo24.c(2130837609);
      break label3865;
      label6053:
      localo25.a(false);
      localo25.c(2130837609);
      break label4007;
      label6070:
      localo26.a(false);
      localo26.c(2130837609);
      break label4149;
      label6087:
      localo27.a(false);
      localo27.c(2130837609);
      break label4291;
      label6104:
      localo28.a(false);
      localo28.c(2130837609);
      break label4440;
      label6121:
      localo29.a(false);
      localo29.c(2130837609);
      break label4582;
      label6138:
      localo30.a(false);
      localo30.c(2130837609);
      break label4731;
      label6155:
      localo31.a(false);
      localo31.c(2130837609);
      break label4873;
      label6172:
      localo32.a(false);
      localo32.c(2130837609);
      break label5015;
      label6189:
      localo33.a(false);
      localo33.c(2130837609);
      break label5157;
      label6206:
      localo34.a(false);
      localo34.c(2130837609);
      break label5299;
      label6223:
      localo35.a(false);
      localo35.c(2130837609);
      break label5441;
      label6240:
      localo36.a(false);
      localo36.c(2130837609);
    }
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    this.n.b().onTouchEvent(paramMotionEvent);
    return super.dispatchTouchEvent(paramMotionEvent);
  }
  
  public void onBackPressed()
  {
    super.onBackPressed();
    overridePendingTransition(0, 2130968604);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903073);
    a((Toolbar)findViewById(2131492951));
    overridePendingTransition(2130968605, 17432577);
    setTitle(getString(2131624015));
    g().a(true);
    this.n = new cd(this, this);
    findViewById(2131493039).setOnTouchListener(this.n);
    this.s = ((ListView)findViewById(2131492989));
    this.t = new flar2.exkernelmanager.a.a(this, new ArrayList());
    this.s.setAdapter(this.t);
    this.s.setOnItemClickListener(this);
    this.s.setOnItemLongClickListener(this);
    this.s.setOnScrollListener(new cf(this));
    this.r = this.o.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor");
    this.u = ("sys/devices/system/cpu/cpufreq/" + this.r + "/");
    if (!this.p.a(this.u)) {
      this.u = ("sys/devices/system/cpu/cpu0/cpufreq/" + this.r + "/");
    }
    k.a("prefGovPath", this.u);
    l();
    k();
  }
  
  public void onItemClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    switch (((o)this.t.getItem(paramInt)).b())
    {
    default: 
      return;
    case -70: 
      b("prefGBoost", this.u + "gboost");
      return;
    case -71: 
      b("prefShortcut", this.u + "shortcut");
      return;
    case -78: 
      c("prefSyncFreq", this.u + "sync_freq");
      return;
    case -79: 
      c("prefOptFreq", this.u + "optimal_freq");
      return;
    case -75: 
      if (this.o.a(this.u + "two_phase_freq").contains(","))
      {
        d("prefTwoPhaseFreq", this.u + "two_phase_freq");
        return;
      }
      c("prefTwoPhaseFreq", this.u + "two_phase_freq");
      return;
    case -72: 
      if (this.o.a(this.u + "input_event_min_freq").contains(","))
      {
        d("prefInputMinFreq", this.u + "input_event_min_freq");
        return;
      }
      c("prefInputMinFreq", this.u + "input_event_min_freq");
      return;
    case -926: 
      c("prefActiveFloorFreq", this.u + "active_floor_freq");
      return;
    case -923: 
      c("PREF_INPUT_MIN_FREQ2", this.u + "input_min_freq");
      return;
    case -924: 
      c("PREF_GBOOST_MIN_FREQ", this.u + "gboost_min_freq");
      return;
    case -925: 
      c("PREF_MAX_SCREEN_OFF_FREQ", this.u + "max_screen_off_freq");
      return;
    case -73: 
      a("prefInputTimeout", this.u + "input_event_timeout");
      return;
    case -74: 
      a("prefUISamplingRate", this.u + "ui_sampling_rate");
      return;
    case -76: 
      a("prefUpThreshold", this.u + "up_threshold");
      return;
    case -77: 
      a("prefDownDifferential", this.u + "down_differential");
      return;
    case -900: 
      a("PREF_SAMPLING_RATE_MIN", this.u + "sampling_rate_min");
      return;
    case -902: 
      a("PREF_FREQ_DOWN_STEP", this.u + "freq_down_step");
      return;
    case -903: 
      a("PREF_SAMPLING_RATE", this.u + "sampling_rate");
      return;
    case -904: 
      a("PREF_SAMPLING_DOWN_FACTOR", this.u + "sampling_down_factor");
      return;
    case -906: 
      a("PREF_UP_THRESHOLD_ANY_CPU_LOAD", this.u + "up_threshold_any_cpu_load");
      return;
    case -907: 
      a("PREF_TARGET_LOADS", this.u + "target_loads");
      return;
    case -908: 
      a("PREF_TIMER_SLACK", this.u + "timer_slack");
      return;
    case -911: 
      a("PREF_TIMER_RATE", this.u + "timer_rate");
      return;
    case -912: 
      a("PREF_ABOVE_HISPEED_DELAY", this.u + "above_hispeed_delay");
      return;
    case -913: 
      a("PREF_BOOSTPULSE", this.u + "boostpulse");
      return;
    case -914: 
      a("PREF_BOOSTPULSE_DURATION", this.u + "boostpulse_duration");
      return;
    case -915: 
      a("PREF_GO_HISPEED_LOAD", this.u + "go_hispeed_load");
      return;
    case -917: 
      a("PREF_MIN_SAMPLE_TIME", this.u + "min_sample_time");
      return;
    case -920: 
      a("PREF_DOWN_THRESHOLD", this.u + "down_threshold");
      return;
    case -921: 
      a("PREF_FREQ_STEP", this.u + "freq_step");
      return;
    case -922: 
      a("PREF_BOOST", this.u + "boost");
      return;
    case -910: 
      c("PREF_HISPEED_FREQ", this.u + "hispeed_freq");
      return;
    case -916: 
      c("PREF_INPUT_BOOST_FREQ", this.u + "input_boost_freq");
      return;
    case -918: 
      c("PREF_UP_THRESHOLD_ANY_CPU_FREQ", this.u + "up_threshold_any_cpu_freq");
      return;
    case -901: 
      c("PREF_FREQ_DOWN_STEP_BARRIAR", this.u + "freq_down_step_barriar");
      return;
    case -927: 
      a("prefLowLoadDownThreshold", this.u + "low_load_down_threshold");
      return;
    case -928: 
      a("prefMaxFreqHysteresis", this.u + "max_freq_hysteresis");
      return;
    }
    a("prefInputBoostDuration", this.u + "input_boost_duration");
  }
  
  public boolean onItemLongClick(AdapterView paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    switch (((o)this.t.getItem(paramInt)).b())
    {
    }
    for (;;)
    {
      return true;
      Toast.makeText(this, "This is how you change doubletap2wake ", 0).show();
    }
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(paramMenuItem);
    }
    ar.a(this);
    return true;
  }
  
  public void onResume()
  {
    super.onResume();
    k();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/GovernorActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */