package flar2.exkernelmanager.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import flar2.exkernelmanager.utilities.e;
import flar2.exkernelmanager.utilities.f;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;

class ah
  implements DialogInterface.OnClickListener
{
  ah(ac paramac, String[] paramArrayOfString, int paramInt) {}
  
  public void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    String str = this.a[paramInt];
    if (str != null) {}
    switch (this.b)
    {
    default: 
      return;
    case -10: 
      k.a("prefCPUMaxBoot", false);
      k.a("prefCPUMax", str);
      if (this.c.c.a("/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq")) {
        this.c.a.a(str, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq");
      }
      for (;;)
      {
        ac.a(this.c);
        return;
        if (this.c.c.a("/sys/module/cpu_tegra/parameters/cpu_user_cap"))
        {
          this.c.a.a(str, "/sys/module/cpu_tegra/parameters/cpu_user_cap");
          this.c.a.a(str, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq");
        }
        else
        {
          this.c.a.a(str, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq");
          this.c.a.a("1", "/sys/devices/system/cpu/cpu1/online");
          this.c.a.a(str, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_max_freq");
          this.c.a.a("1", "/sys/devices/system/cpu/cpu2/online");
          this.c.a.a(str, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_max_freq");
          this.c.a.a("1", "/sys/devices/system/cpu/cpu3/online");
          this.c.a.a(str, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_max_freq");
        }
      }
    case -11: 
      k.a("prefCPUMinBoot", false);
      k.a("prefCPUMin", str);
      ac.a(this.c, false);
      if ((ac.c(this.c).equals(ac.b(this.c).getString(2131624153))) && (!k.a("prefMPDecision").equals("Disabled"))) {
        this.c.b.a("stop mpdecision");
      }
      break;
    }
    for (int i = 1;; i = 0)
    {
      this.c.a.a(str, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq");
      this.c.a.a("1", "/sys/devices/system/cpu/cpu1/online");
      this.c.a.a(str, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_min_freq");
      this.c.a.a("1", "/sys/devices/system/cpu/cpu2/online");
      this.c.a.a(str, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_min_freq");
      this.c.a.a("1", "/sys/devices/system/cpu/cpu3/online");
      this.c.a.a(str, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_min_freq");
      if (i != 0)
      {
        ac.a(this.c, true);
        this.c.b.a("start mpdecision");
        ac.a(this.c, false);
      }
      if ((k.b("prefHTC").booleanValue()) && (!k.a("prefCPUMin").equals("300000")) && (!k.a("prefCPUMin").equals("384000"))) {
        ac.a(this.c, true);
      }
      ac.a(this.c);
      return;
      k.a("prefMaxScroffBoot", false);
      k.a("prefMaxScroff", str);
      this.c.a.a(str, "/sys/devices/system/cpu/cpu0/cpufreq/screen_off_max_freq");
      ac.a(this.c);
      return;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/ah.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */