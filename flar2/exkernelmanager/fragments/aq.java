package flar2.exkernelmanager.fragments;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.ListView;

class aq
  implements AbsListView.OnScrollListener
{
  aq(CPUTimeActivity paramCPUTimeActivity, Button paramButton1, Button paramButton2) {}
  
  public void onScroll(AbsListView paramAbsListView, int paramInt1, int paramInt2, int paramInt3)
  {
    boolean bool1 = true;
    ListView localListView = CPUTimeActivity.d(this.c);
    boolean bool2 = false;
    int k;
    int m;
    if (localListView != null)
    {
      int j = CPUTimeActivity.d(this.c).getChildCount();
      bool2 = false;
      if (j > 0)
      {
        if (CPUTimeActivity.d(this.c).getFirstVisiblePosition() != 0) {
          break label244;
        }
        k = bool1;
        if (CPUTimeActivity.d(this.c).getChildAt(0).getTop() != 0) {
          break label250;
        }
        m = bool1;
        label78:
        if ((k == 0) || (m == 0)) {
          break label256;
        }
      }
    }
    for (;;)
    {
      bool2 = bool1;
      CPUTimeActivity.b(this.c).setEnabled(bool2);
      int i = this.c.k();
      CPUTimeActivity.f(this.c).setTranslationY(Math.max(-i, CPUTimeActivity.e(this.c)));
      CPUTimeActivity.g(this.c).setTranslationY(Math.max(-i, CPUTimeActivity.e(this.c)));
      CPUTimeActivity.h(this.c).setTranslationY(Math.max(-i / 2, CPUTimeActivity.e(this.c)));
      float f = CPUTimeActivity.a(this.c, CPUTimeActivity.f(this.c).getTranslationY() / CPUTimeActivity.e(this.c), 0.0F, 1.0F);
      this.a.setAlpha(1.0F - f * 2.0F);
      this.b.setAlpha(1.0F - f * 2.0F);
      return;
      label244:
      k = 0;
      break;
      label250:
      m = 0;
      break label78;
      label256:
      bool1 = false;
    }
  }
  
  public void onScrollStateChanged(AbsListView paramAbsListView, int paramInt) {}
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/aq.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */