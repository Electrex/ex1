package flar2.exkernelmanager.fragments;

import android.os.AsyncTask;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

class fz
  extends AsyncTask
{
  private fz(ey paramey) {}
  
  protected String a(String... paramVarArgs)
  {
    String str1 = "";
    try
    {
      BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(new URL(paramVarArgs[0]).openConnection().getInputStream()));
      for (;;)
      {
        String str2 = localBufferedReader.readLine();
        if (str2 == null) {
          break;
        }
        str1 = str1 + str2 + "\n";
      }
      localBufferedReader.close();
      return str1;
    }
    catch (MalformedURLException localMalformedURLException)
    {
      return null;
    }
    catch (IOException localIOException) {}
    return null;
  }
  
  protected void a(String paramString)
  {
    if (paramString != null)
    {
      ey.a(this.a, paramString);
      return;
    }
    ey.a(this.a, this.a.getString(2131624165));
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/fz.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */