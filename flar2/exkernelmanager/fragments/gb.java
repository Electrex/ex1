package flar2.exkernelmanager.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import flar2.exkernelmanager.MainActivity;
import flar2.exkernelmanager.MainApp;
import flar2.exkernelmanager.a.g;
import flar2.exkernelmanager.a.o;
import flar2.exkernelmanager.r;
import flar2.exkernelmanager.utilities.a.b;
import flar2.exkernelmanager.utilities.e;
import flar2.exkernelmanager.utilities.f;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;
import java.util.ArrayList;
import java.util.List;

public class gb
  extends Fragment
  implements g
{
  public static final ArrayList a = new ArrayList();
  public static final ArrayList b = new ArrayList();
  private m c = new m();
  private e d = new e();
  private Context e;
  private ListView f;
  private flar2.exkernelmanager.a.a g;
  private CheckedTextView h;
  private int i = 0;
  private TextView j;
  private View k;
  private View l;
  private View m;
  private View n;
  
  private void a()
  {
    List localList = b();
    this.g.clear();
    this.g.addAll(localList);
    this.g.notifyDataSetChanged();
  }
  
  private List b()
  {
    String[] arrayOfString = this.c.a(r.j[this.i], false);
    ArrayList localArrayList = new ArrayList();
    a.clear();
    b.clear();
    if (!this.d.a(r.j[this.i]))
    {
      o localo1 = new o();
      localo1.a(1);
      localo1.a(this.e.getString(2131624164));
      localArrayList.add(localo1);
    }
    for (;;)
    {
      return localArrayList;
      if (this.i == 0)
      {
        int i3 = arrayOfString.length;
        for (int i4 = 0; i4 < i3; i4++)
        {
          String str6 = arrayOfString[i4];
          String str7 = str6.split(":")[0].replace("mhz", "");
          String str8 = str6.split(":")[1].replace(" ", "").replace("mV", "");
          a.add(str8);
          o localo3 = new o();
          localo3.a(4);
          localo3.a(str7 + " MHz");
          localo3.b(str8 + " mV");
          localArrayList.add(localo3);
        }
      }
      else
      {
        int i1 = arrayOfString.length;
        for (int i2 = 0; i2 < i1; i2++)
        {
          String str1 = arrayOfString[i2];
          String str2 = str1.split(":")[0];
          b.add(str2);
          String str3 = str2.substring(0, -3 + str2.length());
          String str4 = str1.split(":")[1].replace(" ", "");
          a.add(str4);
          String str5 = str4.substring(0, -3 + str4.length());
          o localo2 = new o();
          localo2.a(4);
          localo2.a(str3 + " MHz");
          localo2.b(str5 + " mV");
          localArrayList.add(localo2);
        }
      }
    }
  }
  
  private void b(String paramString)
  {
    int i1 = 0;
    k.a("prefVoltageBoot", false);
    this.h.setChecked(false);
    this.c.a(paramString, r.j[this.i]);
    a();
    String[] arrayOfString1 = (String[])b.toArray(new String[b.size()]);
    String[] arrayOfString2 = (String[])a.toArray(new String[a.size()]);
    if (this.i == 0) {
      k.a("prefVoltage", paramString);
    }
    for (;;)
    {
      return;
      k.a("prefVoltSize", a.size());
      int i2 = arrayOfString2.length;
      int i3 = 0;
      while (i1 < i2)
      {
        String str = arrayOfString2[i1];
        k.a("prefVolt" + i3, arrayOfString1[i3] + " " + str);
        i3++;
        i1++;
      }
    }
  }
  
  public void a(String paramString)
  {
    b(paramString);
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if (paramMenu != null)
    {
      paramMenu.removeItem(2131493247);
      paramMenu.removeItem(2131493246);
      paramMenu.removeItem(2131493236);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    View localView = paramLayoutInflater.inflate(2130903096, paramViewGroup, false);
    this.e = MainApp.a();
    String[] arrayOfString = getResources().getStringArray(2131230721);
    getActivity().setTitle(arrayOfString[4]);
    ((flar2.exkernelmanager.slidingmenu.a.a)flar2.exkernelmanager.slidingmenu.a.a.a.getAdapter()).f(flar2.exkernelmanager.slidingmenu.a.a.b.indexOf("Voltage"));
    this.i = this.c.a(r.j);
    k.a("prefVoltagePath", this.i);
    this.f = ((ListView)localView.findViewById(2131492989));
    this.g = new flar2.exkernelmanager.a.a(getActivity(), new ArrayList());
    this.g.a(this);
    this.f.setAdapter(this.g);
    flar2.exkernelmanager.a.a.b = true;
    ImageButton localImageButton1 = (ImageButton)localView.findViewById(2131493178);
    ImageButton localImageButton2 = (ImageButton)localView.findViewById(2131493180);
    this.h = ((CheckedTextView)localView.findViewById(2131493177));
    if (k.b("prefVoltageBoot").booleanValue()) {
      this.h.setChecked(true);
    }
    this.h.setOnClickListener(new gc(this));
    setHasOptionsMenu(true);
    localImageButton1.setOnClickListener(new gd(this));
    localImageButton2.setOnClickListener(new ge(this));
    MainActivity.o.getLayoutParams().height = new f().b(getActivity());
    MainActivity.o.setTranslationY(0.0F);
    MainActivity.p.setTranslationY(0.0F);
    this.k = localView.findViewById(2131493176);
    this.l = localView.findViewById(2131493181);
    this.j = ((TextView)getActivity().findViewById(2131493046));
    this.j.setText(null);
    this.m = getActivity().findViewById(2131493047);
    this.m.setVisibility(8);
    this.n = paramLayoutInflater.inflate(2130903089, this.f, false);
    this.f.addHeaderView(this.n);
    this.k.getLayoutParams().height = (getResources().getDimensionPixelSize(2131427424) - new f().b(getActivity()));
    this.f.setOnTouchListener(new b(this.k, this.l, this.m, 2130968608, 2130968607));
    if (k.b("prefFirstRunVoltage").booleanValue()) {
      k.a("prefFirstRunVoltage", false);
    }
    a();
    return localView;
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    if (this.m != null) {
      this.m.setVisibility(0);
    }
  }
  
  public void onPause()
  {
    super.onPause();
    flar2.exkernelmanager.a.a.b = false;
  }
  
  public void onResume()
  {
    super.onResume();
    a();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/fragments/gb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */