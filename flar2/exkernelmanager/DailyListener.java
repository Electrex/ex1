package flar2.exkernelmanager;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import com.commonsware.cwac.wakeful.a;
import com.commonsware.cwac.wakeful.b;
import flar2.exkernelmanager.utilities.k;
import java.util.Calendar;

public class DailyListener
  implements b
{
  public long getMaxAge()
  {
    return 86460000L;
  }
  
  public void scheduleAlarms(AlarmManager paramAlarmManager, PendingIntent paramPendingIntent, Context paramContext)
  {
    if (k.b("prefCheckForUpdates").booleanValue())
    {
      paramAlarmManager.cancel(paramPendingIntent);
      Log.i("ElementalX Kernel", "Schedule update check...");
      Calendar localCalendar = Calendar.getInstance();
      if (Calendar.getInstance().get(11) >= 23) {
        localCalendar.add(6, 1);
      }
      localCalendar.set(11, 23);
      localCalendar.set(12, 10);
      localCalendar.set(13, 0);
      paramAlarmManager.setInexactRepeating(1, localCalendar.getTimeInMillis(), 86400000L, paramPendingIntent);
      return;
    }
    Log.i("ElementalX Kernel", "Do not schedule update check...");
    paramAlarmManager.cancel(paramPendingIntent);
  }
  
  public void sendWakefulWork(Context paramContext)
  {
    NetworkInfo localNetworkInfo = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
    if ((localNetworkInfo != null) && (localNetworkInfo.isConnectedOrConnecting()) && ((!k.b("prefWiFi").booleanValue()) || (localNetworkInfo.getType() == 1)))
    {
      a.a(paramContext, new Intent(paramContext, BackgroundService.class));
      return;
    }
    ConnectivityReceiver.a(paramContext);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/DailyListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */