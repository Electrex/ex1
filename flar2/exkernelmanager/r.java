package flar2.exkernelmanager;

public final class r
{
  public static final String[] a = { "/sys/devices/system/cpu/cpufreq/ondemand/gboost", "/sys/devices/system/cpu/cpufreq/elementalx/gboost" };
  public static final String[] b = { "/dev/block/platform/msm_sdcc.1/by-name/boot", "/dev/block/platform/sdhci-tegra.3/by-name/LNX", "/dev/block/platform/f9824900.sdhci/by-name/boot" };
  public static final String[] c = { "/sys/class/thermal/thermal_zone0/temp", "/sys/devices/system/cpu/cpu0/cpufreq/cpu_temp", "/sys/devices/system/cpu/cpu0/cpufreq/FakeShmoo_cpu_temp", "/sys/class/thermal/thermal_zone1/temp", "/sys/class/i2c-adapter/i2c-4/4-004c/temperature", "/sys/devices/platform/tegra-i2c.3/i2c-4/4-004c/temperature", "/sys/devices/platform/omap/omap_temp_sensor.0/temperature", "/sys/devices/platform/tegra_tmon/temp1_input", "/sys/kernel/debug/tegra_thermal/temp_tj", "/sys/devices/platform/s5p-tmu/temperature" };
  public static final String[] d = { "/sys/devices/platform/kgsl-3d0.0/kgsl/kgsl-3d0/gpuclk", "/sys/class/kgsl/kgsl-3d0/gpuclk", "/sys/kernel/tegra_gpu/gpu_rate" };
  public static final String[] e = { "/sys/devices/platform/kgsl-3d0.0/kgsl/kgsl-3d0/max_gpuclk", "/sys/class/kgsl/kgsl-3d0/max_gpuclk", "/sys/kernel/tegra_gpu/gpu_cap_rate" };
  public static final String[] f = { "/sys/devices/platform/kgsl-3d0.0/kgsl/kgsl-3d0/min_pwrlevel", "/sys/class/kgsl/kgsl-3d0/min_pwrlevel" };
  public static final String[] g = { "/sys/devices/platform/kgsl-3d0.0/kgsl/kgsl-3d0/gpu_available_frequencies", "/sys/class/kgsl/kgsl-3d0/gpu_available_frequencies", "/sys/kernel/tegra_gpu/gpu_available_rates" };
  public static final String[] h = { "/sys/devices/platform/kgsl-3d0.0/kgsl/kgsl-3d0/pwrscale/trustzone/governor", "/sys/class/kgsl/kgsl-3d0/pwrscale/trustzone/governor", "/sys/class/kgsl/kgsl-3d0/devfreq/governor" };
  public static final String[] i = { "/sys/module/lm3630_bl/parameters/backlight_dimmer", "/sys/module/msm_fb/parameters/backlight_dimmer", "/sys/module/mdss_dsi/parameters/backlight_dimmer", "/sys/backlight_dimmer/backlight_dimmer" };
  public static final String[] j = { "/sys/devices/system/cpu/cpu0/cpufreq/UV_mV_table", "/sys/devices/system/cpu/cpufreq/vdd_table/vdd_levels", "/sys/devices/system/cpu/cpu0/cpufreq/vdd_levels" };
  public static final String[] k = { "/sys/class/timed_output/vibrator/amp", "/sys/class/timed_output/vibrator/voltage_level", "/sys/drv2605/rtp_strength" };
  public static final String[] l = { "/sys/module/hall_sensor/parameters/disable_cover", "/sys/module/lid/parameters/enable_lid" };
  public static final String[] m = { "/sys/android_touch/sweep2wake", "/sys/android_key/sweep2wake" };
  public static final String[] n = { "/sys/android_touch/doubletap2wake", "/sys/android_key/doubletap2wake", "/sys/devices/platform/spi-tegra114.2/spi_master/spi2/spi2.0/input/input0/wake_gesture", "/sys/devices/f9966000.i2c/i2c-1/1-004a/tsp", "/sys/devices/virtual/input/lge_touch/dt_wake_enabled", "/sys/module/lge_touch_core/parameters/doubletap_to_wake", "/sys/devices/virtual/input/lge_touch/touch_gesture", "/proc/touchpanel/double_tap_enable", "/sys/devices/virtual/input/input1/wakeup_gesture" };
  public static final String[] o = { "/sys/android_touch/pocket_detect", "/sys/android_key/pocket_detect" };
  public static final String[] p = { "/sys/devices/platform/kcal_ctrl.0/kcal_ctrl", "/sys/devices/platform/DIAG0.0/power_rail_ctrl" };
  public static final String[] q = { "/sys/devices/platform/kcal_ctrl.0/kcal", "/sys/devices/platform/DIAG0.0/power_rail" };
  public static final String[] r = { "/sys/module/msm_thermal/parameters/limit_temp_degC", "/sys/module/msm_thermal/parameters/temp_threshold" };
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/r.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */