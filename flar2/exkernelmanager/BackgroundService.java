package flar2.exkernelmanager;

import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;
import com.commonsware.cwac.wakeful.a;
import flar2.exkernelmanager.utilities.d;
import flar2.exkernelmanager.utilities.f;
import flar2.exkernelmanager.utilities.k;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class BackgroundService
  extends a
{
  public BackgroundService()
  {
    super("BackgroundService");
  }
  
  public void a()
  {
    new d().a();
    String str1 = k.a("prefCheckVersion") + "latest";
    try
    {
      BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(new URL(str1).openConnection().getInputStream()));
      String str2 = localBufferedReader.readLine();
      localBufferedReader.close();
      a(str2);
      return;
    }
    catch (MalformedURLException localMalformedURLException)
    {
      localMalformedURLException.printStackTrace();
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }
  
  public void a(Intent paramIntent)
  {
    if (new f().a(this)) {
      a();
    }
    Log.i("ElementalX Kernel", "Checking for updates...");
  }
  
  public void a(String paramString)
  {
    k.a("prefCurrentKernel", System.getProperty("os.version"));
    if (!paramString.equals(k.a("prefCurrentKernel")))
    {
      k.a("prefLatestVersion", paramString);
      b(paramString);
    }
  }
  
  public void b(String paramString)
  {
    Intent localIntent;
    if (k.e("prefCheckForUpdates"))
    {
      localIntent = new Intent(this, MainActivity.class);
      localIntent.putExtra("update", "openUpdaterFragment");
    }
    for (;;)
    {
      TaskStackBuilder localTaskStackBuilder = TaskStackBuilder.create(this);
      localTaskStackBuilder.addParentStack(MainActivity.class);
      localTaskStackBuilder.addNextIntent(localIntent);
      PendingIntent localPendingIntent = PendingIntent.getActivity(this, 0, localIntent, 134217728);
      Uri localUri = RingtoneManager.getDefaultUri(2);
      Notification localNotification = new Notification.Builder(this).setContentTitle(getString(2131624169)).setContentText(getString(2131624004) + " " + paramString).setSmallIcon(2130837619).setContentIntent(localPendingIntent).setSound(localUri).build();
      localNotification.flags = (0x10 | localNotification.flags);
      ((NotificationManager)getSystemService("notification")).notify(0, localNotification);
      return;
      localIntent = new Intent(this, StartActivity.class);
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/BackgroundService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */