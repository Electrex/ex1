package flar2.exkernelmanager;

import android.animation.ArgbEvaluator;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.o;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import flar2.exkernelmanager.utilities.CircleIndicator;

public class TutorialActivity
  extends o
{
  private static final int[] o = { 2130837643, 2130837645, 2130837644, 2130837646, 2130837627 };
  private static final String p = TutorialActivity.class.getSimpleName();
  ArgbEvaluator n = new ArgbEvaluator();
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903079);
    View localView = findViewById(2131493056);
    ViewPager localViewPager = (ViewPager)findViewById(2131493057);
    CircleIndicator localCircleIndicator = (CircleIndicator)findViewById(2131493059);
    Button localButton1 = (Button)findViewById(2131493058);
    localButton1.setOnClickListener(new ad(this));
    Button localButton2 = (Button)findViewById(2131493060);
    localButton2.setOnClickListener(new ae(this, localViewPager));
    localViewPager.setAdapter(new ai(this, f()));
    localCircleIndicator.setViewPager(localViewPager);
    int[] arrayOfInt = new int[5];
    arrayOfInt[0] = getResources().getColor(2131361859);
    arrayOfInt[1] = getResources().getColor(2131361861);
    arrayOfInt[2] = getResources().getColor(2131361860);
    arrayOfInt[3] = getResources().getColor(2131361858);
    arrayOfInt[4] = getResources().getColor(2131361857);
    localCircleIndicator.setOnPageChangeListener(new af(this, localViewPager, arrayOfInt, localView, localButton2, localButton1, localCircleIndicator));
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/TutorialActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */