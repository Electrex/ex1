package flar2.exkernelmanager.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import java.util.List;

public class a
  extends ArrayAdapter
{
  public static boolean b;
  g a;
  private flar2.exkernelmanager.utilities.a c = new flar2.exkernelmanager.utilities.a();
  private flar2.exkernelmanager.utilities.m d = new flar2.exkernelmanager.utilities.m();
  private int e = 0;
  private int f = 100;
  private int g = -1;
  private int h;
  private LayoutInflater i;
  private List j;
  private Context k;
  
  public a(Context paramContext, List paramList)
  {
    super(paramContext, 0, paramList);
    this.j = paramList;
    this.k = paramContext;
    this.i = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
    this.e = flar2.exkernelmanager.utilities.k.c("prefkcalPath");
  }
  
  private void a(View paramView)
  {
    ImageButton localImageButton = (ImageButton)paramView.findViewById(2131493187);
    int m = ((Integer)paramView.getTag()).intValue();
    int n = ((o)this.j.get(m)).b();
    String str = ((o)this.j.get(m)).g();
    Animation localAnimation1 = AnimationUtils.loadAnimation(this.k, 2130968598);
    Animation localAnimation2 = AnimationUtils.loadAnimation(this.k, 2130968599);
    if (Boolean.valueOf(PreferenceManager.getDefaultSharedPreferences(this.k).getBoolean(str, false)).booleanValue())
    {
      localImageButton.startAnimation(localAnimation1);
      localImageButton.setImageResource(2130837609);
      ((o)this.j.get(m)).c(2130837609);
      ((o)this.j.get(m)).a(false);
      this.c.a(n, false);
      return;
    }
    localImageButton.setImageResource(2130837611);
    localImageButton.startAnimation(localAnimation2);
    ((o)this.j.get(m)).c(2130837611);
    ((o)this.j.get(m)).a(true);
    this.c.a(n, true);
  }
  
  private void a(SeekBar paramSeekBar)
  {
    o localo = (o)paramSeekBar.getTag();
    int m = -30 + this.h;
    int n = 38;
    if (flar2.exkernelmanager.utilities.k.a("prefDeviceName").equals(this.k.getString(2131624153))) {
      n = 38 - (-6 + this.h);
    }
    if (flar2.exkernelmanager.utilities.k.a("prefDeviceName").equals(this.k.getString(2131624157))) {}
    for (int i1 = 22 - (-6 + this.h);; i1 = n)
    {
      if (m < 0) {}
      for (int i2 = m + 256;; i2 = m)
      {
        this.d.a("0", "/sys/kernel/sound_control_3/gpl_sound_control_locked");
        if (localo.b() == 64736)
        {
          String str5 = i2 + " " + Long.toString(4294967295L - i2);
          this.d.a(str5, "/sys/kernel/sound_control_3/gpl_mic_gain");
          flar2.exkernelmanager.utilities.k.a("PREF_SOUND_MIC_GAIN", str5);
        }
        if (localo.b() == 64735)
        {
          String str4 = i2 + " " + Long.toString(4294967295L - i2);
          this.d.a(str4, "/sys/kernel/sound_control_3/gpl_cam_mic_gain");
          flar2.exkernelmanager.utilities.k.a("PREF_SOUND_CAM_GAIN", str4);
        }
        if (localo.b() == 64734)
        {
          String str3 = i2 + " " + i2 + " " + Long.toString(4294967295L - i2 * 2);
          this.d.a(str3, "/sys/kernel/sound_control_3/gpl_speaker_gain");
          flar2.exkernelmanager.utilities.k.a("PREF_SOUND_SPEAKER_GAIN", str3);
        }
        if (localo.b() == 64733)
        {
          String str2 = i2 + " " + i2 + " " + Long.toString(4294967295L - i2 * 2);
          this.d.a(str2, "/sys/kernel/sound_control_3/gpl_headphone_gain");
          flar2.exkernelmanager.utilities.k.a("PREF_SOUND_HEADPHONE_GAIN", str2);
        }
        if (localo.b() == 64732)
        {
          String str1 = i1 + " " + i1 + " " + Long.toString(4294967295L - i1 * 2);
          this.d.a(str1, "/sys/kernel/sound_control_3/gpl_headphone_pa_gain");
          flar2.exkernelmanager.utilities.k.a("PREF_SOUND_HEADPHONE_PA_GAIN", str1);
        }
        if (localo.b() == 64730)
        {
          if (flar2.exkernelmanager.utilities.k.a("prefDeviceName").equals("Nexus6"))
          {
            this.d.a(Integer.toString(-20 + this.h), "/sys/devices/virtual/misc/soundcontrol/mic_boost");
            flar2.exkernelmanager.utilities.k.a("prefMicBoost", Integer.toString(-20 + this.h));
          }
        }
        else if (localo.b() == 64729)
        {
          if (!flar2.exkernelmanager.utilities.k.a("prefDeviceName").equals("Nexus6")) {
            break label679;
          }
          this.d.a(Integer.toString(-20 + this.h), "/sys/devices/virtual/misc/soundcontrol/volume_boost");
          flar2.exkernelmanager.utilities.k.a("prefVolBoost", Integer.toString(-20 + this.h));
        }
        for (;;)
        {
          if (localo.b() == 64727)
          {
            this.d.a(Integer.toString(this.h), "/sys/devices/virtual/misc/soundcontrol/headset_boost");
            flar2.exkernelmanager.utilities.k.a("prefHeadsetBoost", Integer.toString(this.h));
          }
          if (localo.b() == 64728)
          {
            this.d.a(Integer.toString(this.h), "/sys/devices/virtual/misc/soundcontrol/speaker_boost");
            flar2.exkernelmanager.utilities.k.a("prefSpeakerBoost", Integer.toString(this.h));
          }
          return;
          this.d.a(Integer.toString(this.h), "/sys/devices/virtual/misc/soundcontrol/mic_boost");
          flar2.exkernelmanager.utilities.k.a("prefMicBoost", Integer.toString(this.h));
          break;
          label679:
          this.d.a(Integer.toString(this.h), "/sys/devices/virtual/misc/soundcontrol/volume_boost");
          flar2.exkernelmanager.utilities.k.a("prefVolBoost", Integer.toString(this.h));
        }
      }
    }
  }
  
  private void b(SeekBar paramSeekBar)
  {
    o localo = (o)paramSeekBar.getTag();
    localo.a(false);
    this.c.a(localo.b(), false);
    if (localo.b() == -28)
    {
      if (this.e == 1) {
        this.d.a(this.h, flar2.exkernelmanager.utilities.n.a);
      }
      flar2.exkernelmanager.utilities.k.a("prefRedBoot", false);
      flar2.exkernelmanager.utilities.k.a("prefRed", Integer.toString(this.h));
    }
    if (localo.b() == 65326)
    {
      if (this.e == 1) {
        this.d.a(this.h, flar2.exkernelmanager.utilities.n.b);
      }
      flar2.exkernelmanager.utilities.k.a("prefGreenBoot", false);
      flar2.exkernelmanager.utilities.k.a("prefGreen", Integer.toString(this.h));
    }
    if (localo.b() == -29)
    {
      if (this.e == 1) {
        this.d.a(this.h, flar2.exkernelmanager.utilities.n.c);
      }
      flar2.exkernelmanager.utilities.k.a("prefBlueBoot", false);
      flar2.exkernelmanager.utilities.k.a("prefBlue", Integer.toString(this.h));
    }
    if (localo.b() == -54)
    {
      flar2.exkernelmanager.utilities.k.a("prefVibBoot", false);
      flar2.exkernelmanager.utilities.k.a("prefVib", Integer.toString(this.h));
      this.d.a(Integer.toString(this.h), flar2.exkernelmanager.r.k[flar2.exkernelmanager.utilities.k.c("prefvibPath")]);
      ((Vibrator)getContext().getSystemService("vibrator")).vibrate(280L);
    }
    if (localo.b() == -41)
    {
      flar2.exkernelmanager.utilities.k.a("prefWGVibBoot", false);
      flar2.exkernelmanager.utilities.k.a("prefWGVib", Integer.toString(this.h));
      this.d.a(Integer.toString(this.h), "/sys/android_touch/vib_strength");
      ((Vibrator)getContext().getSystemService("vibrator")).vibrate(this.h);
    }
    if (localo.b() == 64736)
    {
      flar2.exkernelmanager.utilities.k.a("PREF_SOUND_MIC_GAINBoot", false);
      if (flar2.exkernelmanager.utilities.k.a("prefSoundLocked").equals("2")) {
        this.d.a("2", "/sys/kernel/sound_control_3/gpl_sound_control_locked");
      }
    }
    if (localo.b() == 64735)
    {
      flar2.exkernelmanager.utilities.k.a("PREF_SOUND_CAM_GAINBoot", false);
      if (flar2.exkernelmanager.utilities.k.a("prefSoundLocked").equals("2")) {
        this.d.a("2", "/sys/kernel/sound_control_3/gpl_sound_control_locked");
      }
    }
    if (localo.b() == 64734)
    {
      flar2.exkernelmanager.utilities.k.a("PREF_SOUND_SPEAKER_GAINBoot", false);
      if (flar2.exkernelmanager.utilities.k.a("prefSoundLocked").equals("2")) {
        this.d.a("2", "/sys/kernel/sound_control_3/gpl_sound_control_locked");
      }
    }
    if (localo.b() == 64733)
    {
      flar2.exkernelmanager.utilities.k.a("PREF_SOUND_HEADPHONE_GAINBoot", false);
      if (flar2.exkernelmanager.utilities.k.a("prefSoundLocked").equals("2")) {
        this.d.a("2", "/sys/kernel/sound_control_3/gpl_sound_control_locked");
      }
    }
    if (localo.b() == 64732)
    {
      flar2.exkernelmanager.utilities.k.a("PREF_SOUND_HEADPHONE_PA_GAINBoot", false);
      if (flar2.exkernelmanager.utilities.k.a("prefSoundLocked").equals("2")) {
        this.d.a("2", "/sys/kernel/sound_control_3/gpl_sound_control_locked");
      }
    }
  }
  
  public void a(g paramg)
  {
    this.a = paramg;
  }
  
  public int getCount()
  {
    return this.j.size();
  }
  
  public long getItemId(int paramInt)
  {
    return paramInt;
  }
  
  public int getItemViewType(int paramInt)
  {
    return ((o)getItem(paramInt)).a();
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    o localo = (o)getItem(paramInt);
    int m;
    if (this.k.getResources().getBoolean(2131296263)) {
      if (this.k.getResources().getBoolean(2131296265)) {
        m = 5;
      }
    }
    for (;;)
    {
      Animation localAnimation = AnimationUtils.loadAnimation(this.k, 2130968597);
      j localj2;
      int i2;
      label144:
      j localj1;
      if (((o)getItem(paramInt)).a() == 0) {
        if (paramView == null)
        {
          paramView = this.i.inflate(2130903101, paramViewGroup, false);
          localj2 = new j(this);
          localj2.a = ((o)this.j.get(paramInt));
          localj2.b = ((TextView)paramView.findViewById(2131493196));
          paramView.setTag(localj2);
          if (paramInt > 0)
          {
            i2 = paramInt;
            localAnimation.setDuration(i2 * this.f);
            if ((!b) || (paramInt >= m)) {
              break label1601;
            }
            paramView.startAnimation(localAnimation);
            localj1 = localj2;
          }
        }
      }
      for (;;)
      {
        label179:
        localj1.b.setText(localo.c());
        label591:
        label688:
        label1060:
        label1222:
        label1349:
        do
        {
          for (;;)
          {
            this.g = paramInt;
            return paramView;
            if (this.k.getResources().getBoolean(2131296267))
            {
              m = 7;
              break;
            }
            if (!this.k.getResources().getBoolean(2131296266)) {
              break label1608;
            }
            m = 10;
            break;
            if (this.k.getResources().getBoolean(2131296267))
            {
              m = 14;
              break;
            }
            if (this.k.getResources().getBoolean(2131296264))
            {
              m = 10;
              break;
            }
            if (!this.k.getResources().getBoolean(2131296266)) {
              break label1608;
            }
            m = 20;
            break;
            i2 = 1;
            break label144;
            localj1 = (j)paramView.getTag();
            break label179;
            if (((o)getItem(paramInt)).a() == 7)
            {
              if (paramView == null) {
                paramView = this.i.inflate(2130903088, paramViewGroup, false);
              }
            }
            else if (((o)getItem(paramInt)).a() == 5)
            {
              if (paramView == null) {
                paramView = this.i.inflate(2130903102, paramViewGroup, false);
              }
              k localk = new k(this);
              localk.a = ((ImageView)paramView.findViewById(2131493197));
              localk.a.setImageResource(localo.f());
            }
            else
            {
              if (((o)getItem(paramInt)).a() == 6)
              {
                i locali1;
                Context localContext2;
                if (paramView == null)
                {
                  paramView = this.i.inflate(2130903100, paramViewGroup, false);
                  i locali2 = new i(this);
                  locali2.a = ((o)this.j.get(paramInt));
                  locali2.f = paramInt;
                  locali2.b = ((TextView)paramView.findViewById(2131493192));
                  locali2.c = ((TextView)paramView.findViewById(2131493193));
                  locali2.d = ((TextView)paramView.findViewById(2131493195));
                  locali2.e = ((ProgressBar)paramView.findViewById(2131493194));
                  locali2.e.setTag(locali2.a);
                  paramView.setTag(locali2);
                  locali1 = locali2;
                  locali1.b.setText(localo.c());
                  locali1.c.setText(localo.d());
                  locali1.d.setText(localo.e());
                  locali1.e.setProgress(localo.h());
                  localContext2 = getContext();
                  if (paramInt <= this.g) {
                    break label688;
                  }
                }
                for (int i1 = 2130968609;; i1 = 2130968587)
                {
                  paramView.startAnimation(AnimationUtils.loadAnimation(localContext2, i1));
                  break;
                  locali1 = (i)paramView.getTag();
                  break label591;
                }
              }
              if (((o)getItem(paramInt)).a() == 2)
              {
                if (paramView == null)
                {
                  paramView = this.i.inflate(2130903099, paramViewGroup, false);
                  localAnimation.setDuration(paramInt * this.f);
                  if ((b) && (paramInt < m)) {
                    paramView.startAnimation(localAnimation);
                  }
                }
                h localh = new h(this);
                localh.a = ((TextView)paramView.findViewById(2131493190));
                localh.a.setText(localo.c());
              }
              else
              {
                if (((o)getItem(paramInt)).a() != 3) {
                  break label1060;
                }
                paramView = this.i.inflate(2130903103, paramViewGroup, false);
                m localm = new m(this);
                localm.a = ((o)this.j.get(paramInt));
                localm.b = ((TextView)paramView.findViewById(2131493198));
                localm.c = ((SeekBar)paramView.findViewById(2131492873));
                localm.c.setTag(localm.a);
                localm.d = ((ImageButton)paramView.findViewById(2131493187));
                localm.d.setOnClickListener(new b(this));
                paramView.setTag(localm);
                paramView.setTag(2131493187, localm.d);
                localAnimation.setDuration(paramInt * this.f);
                if ((b) && (paramInt < m)) {
                  paramView.startAnimation(localAnimation);
                }
                localm.d.setTag(Integer.valueOf(paramInt));
                localm.b.setText(localo.d());
                localm.c.setMax(localo.i());
                localm.c.setProgress(localo.h());
                localm.d.setImageResource(localo.f());
                localm.c.setOnSeekBarChangeListener(new c(this, localm));
              }
            }
          }
          if (((o)getItem(paramInt)).a() == 4)
          {
            n localn1;
            Context localContext1;
            if (paramView == null)
            {
              paramView = this.i.inflate(2130903104, paramViewGroup, false);
              n localn2 = new n(this);
              localn2.a = ((o)this.j.get(paramInt));
              localn2.f = paramInt;
              localn2.b = ((TextView)paramView.findViewById(2131493185));
              localn2.d = ((ImageButton)paramView.findViewById(2131493178));
              localn2.d.setTag(localn2.a);
              localn2.c = ((TextView)paramView.findViewById(2131493186));
              localn2.e = ((ImageButton)paramView.findViewById(2131493180));
              localn2.e.setTag(localn2.a);
              paramView.setTag(localn2);
              localn1 = localn2;
              localn1.b.setText(localo.c());
              localn1.c.setText(localo.d());
              localn1.d.setImageResource(2130837608);
              localn1.e.setImageResource(2130837610);
              localn1.d.setOnClickListener(new d(this, paramInt));
              localn1.e.setOnClickListener(new e(this, paramInt));
              localContext1 = getContext();
              if (paramInt <= this.g) {
                break label1349;
              }
            }
            for (int n = 2130968609;; n = 2130968587)
            {
              paramView.startAnimation(AnimationUtils.loadAnimation(localContext1, n));
              break;
              localn1 = (n)paramView.getTag();
              break label1222;
            }
          }
        } while (((o)getItem(paramInt)).a() != 1);
        l locall2;
        l locall1;
        if (paramView == null)
        {
          paramView = this.i.inflate(2130903098, paramViewGroup, false);
          locall2 = new l(this);
          locall2.a = ((o)this.j.get(paramInt));
          locall2.b = ((TextView)paramView.findViewById(2131493185));
          locall2.c = ((TextView)paramView.findViewById(2131493186));
          locall2.d = ((ImageButton)paramView.findViewById(2131493187));
          locall2.d.setOnClickListener(new f(this));
          paramView.setTag(locall2);
          paramView.setTag(2131493187, locall2.d);
          localAnimation.setDuration(paramInt * this.f);
          if ((!b) || (paramInt >= m)) {
            break label1594;
          }
          paramView.startAnimation(localAnimation);
          locall1 = locall2;
        }
        for (;;)
        {
          locall1.d.setTag(Integer.valueOf(paramInt));
          locall1.b.setText(localo.c());
          locall1.c.setText(localo.d());
          locall1.d.setImageResource(localo.f());
          break;
          locall1 = (l)paramView.getTag();
          continue;
          label1594:
          locall1 = locall2;
        }
        label1601:
        localj1 = localj2;
      }
      label1608:
      m = 9;
    }
  }
  
  public int getViewTypeCount()
  {
    return 8;
  }
  
  public boolean hasStableIds()
  {
    return true;
  }
  
  public boolean isEnabled(int paramInt)
  {
    return (getItemViewType(paramInt) == 1) || (getItemViewType(paramInt) == 2);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */