package flar2.exkernelmanager.a;

import android.content.Context;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.n;

class c
  implements SeekBar.OnSeekBarChangeListener
{
  c(a parama, m paramm) {}
  
  public void onProgressChanged(SeekBar paramSeekBar, int paramInt, boolean paramBoolean)
  {
    o localo = (o)paramSeekBar.getTag();
    if (localo.b() == -54) {
      if (k.c("prefvibPath") == 1)
      {
        a.a(this.b, paramInt + 1200);
        if (localo.b() == -28)
        {
          this.a.b.setText(a.a(this.b).getString(2131624188) + a.b(this.b));
          if (a.c(this.b) == 0) {
            a.d(this.b).a(a.b(this.b), n.a);
          }
        }
        if (localo.b() == 65326)
        {
          this.a.b.setText(a.a(this.b).getString(2131624105) + a.b(this.b));
          if (a.c(this.b) == 0) {
            a.d(this.b).a(a.b(this.b), n.b);
          }
        }
        if (localo.b() == -29)
        {
          this.a.b.setText(a.a(this.b).getString(2131623986) + a.b(this.b));
          if (a.c(this.b) == 0) {
            a.d(this.b).a(a.b(this.b), n.c);
          }
        }
        if (localo.b() == -54)
        {
          if (k.c("prefvibPath") != 1) {
            break label985;
          }
          a.a(this.b, 100 * (a.b(this.b) / 100));
          this.a.b.setText((int)Math.ceil(100.0F * (a.b(this.b) / 3100.0F)) + "%");
        }
        label384:
        if (localo.b() == -41) {
          this.a.b.setText((int)Math.ceil(100.0F * (a.b(this.b) / 60.0F)) + "%");
        }
        if (localo.b() == 64736) {
          this.a.b.setText(a.a(this.b).getString(2131624107) + (-30 + a.b(this.b)));
        }
        if (localo.b() == 64735) {
          this.a.b.setText(a.a(this.b).getString(2131623997) + (-30 + a.b(this.b)));
        }
        if (localo.b() == 64734) {
          this.a.b.setText(a.a(this.b).getString(2131624248) + (-30 + a.b(this.b)));
        }
        if (localo.b() == 64733) {
          this.a.b.setText(a.a(this.b).getString(2131624098) + (-30 + a.b(this.b)));
        }
        if (localo.b() == 64732) {
          this.a.b.setText(a.a(this.b).getString(2131624180) + (-6 + a.b(this.b)));
        }
        if (localo.b() == 64730)
        {
          if (!k.a("prefDeviceName").equals("Nexus6")) {
            break label1023;
          }
          this.a.b.setText("Mic gain: " + (-20 + a.b(this.b)));
        }
        label798:
        if (localo.b() == 64729)
        {
          if (!k.a("prefDeviceName").equals("Nexus6")) {
            break label1061;
          }
          this.a.b.setText("Volume gain: " + (-20 + a.b(this.b)));
        }
      }
    }
    for (;;)
    {
      if (localo.b() == 64727) {
        this.a.b.setText("Headset volume: " + a.b(this.b));
      }
      if (localo.b() == 64728) {
        this.a.b.setText("Speaker volume: " + a.b(this.b));
      }
      a.a(this.b, paramSeekBar);
      return;
      a.a(this.b, paramInt);
      break;
      a.a(this.b, paramInt);
      break;
      label985:
      this.a.b.setText(a.b(this.b) + "%");
      break label384;
      label1023:
      this.a.b.setText("Mic gain: " + a.b(this.b));
      break label798;
      label1061:
      this.a.b.setText("Volume gain: " + a.b(this.b));
    }
  }
  
  public void onStartTrackingTouch(SeekBar paramSeekBar) {}
  
  public void onStopTrackingTouch(SeekBar paramSeekBar)
  {
    a.b(this.b, paramSeekBar);
    this.a.d.setImageResource(2130837609);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/a/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */