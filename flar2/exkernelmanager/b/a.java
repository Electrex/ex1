package flar2.exkernelmanager.b;

import android.support.v7.widget.cc;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class a
  extends cc
{
  c a;
  private List b = Collections.emptyList();
  
  public int a()
  {
    return this.b.size();
  }
  
  public void a(int paramInt, f paramf)
  {
    if (this.b.size() > 0)
    {
      this.b.add(paramInt, paramf);
      d(paramInt);
      return;
    }
    this.b = new ArrayList();
    this.b.add(paramf);
    c();
  }
  
  public void a(c paramc)
  {
    this.a = paramc;
  }
  
  public void a(d paramd, int paramInt)
  {
    paramd.j.setText(((f)this.b.get(paramInt)).a());
    paramd.k.setText(((f)this.b.get(paramInt)).b());
    paramd.a(new b(this, paramd));
  }
  
  public void a(List paramList)
  {
    this.b = paramList;
    c();
  }
  
  public d c(ViewGroup paramViewGroup, int paramInt)
  {
    return new d(LayoutInflater.from(paramViewGroup.getContext()).inflate(2130903081, paramViewGroup, false));
  }
  
  public void f(int paramInt)
  {
    this.b.remove(paramInt);
    e(paramInt);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/b/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */