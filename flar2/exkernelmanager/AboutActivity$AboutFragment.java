package flar2.exkernelmanager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AboutActivity$AboutFragment
  extends PreferenceFragment
{
  private int a = 0;
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    AboutActivity localAboutActivity = (AboutActivity)getActivity();
    addPreferencesFromResource(2131165184);
    findPreference("donateAction").setOnPreferenceClickListener(new g(this, localAboutActivity));
    findPreference("openSourceLicenses").setOnPreferenceClickListener(new h(this, localAboutActivity));
  }
  
  public boolean onPreferenceTreeClick(PreferenceScreen paramPreferenceScreen, Preference paramPreference)
  {
    if ((paramPreference.getKey() != null) && (paramPreference.getKey().equals("prefAbout")))
    {
      this.a = (1 + this.a);
      Toast localToast = Toast.makeText(getActivity(), "", 0);
      switch (this.a)
      {
      }
      for (;;)
      {
        return true;
        localToast.setText("one more...");
        localToast.show();
        continue;
        localToast.cancel();
        this.a = 0;
        Random localRandom = new Random();
        ArrayList localArrayList = new ArrayList();
        localArrayList.add("http://www.metroperspectives.com/p432533056/slideshow");
        localArrayList.add("http://www.metroperspectives.com/p570195139/slideshow");
        localArrayList.add("http://www.metroperspectives.com/p381697527/slideshow");
        localArrayList.add("http://www.metroperspectives.com/p185028595/slideshow");
        localArrayList.add("http://www.metroperspectives.com/p1017027173/slideshow");
        localArrayList.add("http://www.metroperspectives.com/p988347040/slideshow");
        localArrayList.add("http://www.metroperspectives.com/p843179115/slideshow");
        localArrayList.add("http://www.metroperspectives.com/p922041233/slideshow");
        localArrayList.add("http://www.metroperspectives.com/p351495148/slideshow");
        localArrayList.add("http://www.metroperspectives.com/p331432028/slideshow");
        localArrayList.add("http://www.metroperspectives.com/p994483897/slideshow");
        localArrayList.add("http://www.metroperspectives.com/p83337615/slideshow");
        localArrayList.add("http://www.metroperspectives.com/p931516590/slideshow");
        localArrayList.add("http://www.metroperspectives.com/p419884029/slideshow");
        String str = (String)localArrayList.get(localRandom.nextInt(localArrayList.size()));
        Intent localIntent = new Intent("android.intent.action.VIEW");
        localIntent.setData(Uri.parse(str));
        startActivity(localIntent);
      }
    }
    return super.onPreferenceTreeClick(paramPreferenceScreen, paramPreference);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/AboutActivity$AboutFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */