package flar2.exkernelmanager.performance;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import flar2.exkernelmanager.MainApp;
import flar2.exkernelmanager.StartActivity;
import flar2.exkernelmanager.utilities.k;

public class PerformanceReceiver
  extends BroadcastReceiver
{
  private void a()
  {
    Intent localIntent = new Intent(MainApp.a(), StartActivity.class);
    localIntent.addFlags(268435456);
    MainApp.a().startActivity(localIntent);
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    String str = paramIntent.getAction();
    if ("flar2.exkernelmanager.performance.DISABLE_PERFORMANCE".equals(str))
    {
      if (k.e("prefDeviceName")) {
        break label48;
      }
      a();
    }
    for (;;)
    {
      if ("flar2.exkernelmanager.performance.ENABLE_PERFORMANCE".equals(str))
      {
        if (k.e("prefDeviceName")) {
          break;
        }
        a();
      }
      return;
      label48:
      new a(paramContext).a(false);
    }
    new a(paramContext).a(true);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/performance/PerformanceReceiver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */