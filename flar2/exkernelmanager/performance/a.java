package flar2.exkernelmanager.performance;

import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import flar2.exkernelmanager.r;
import flar2.exkernelmanager.utilities.e;
import flar2.exkernelmanager.utilities.f;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;

public class a
{
  SharedPreferences a;
  protected Context b;
  m c = new m();
  e d = new e();
  f e = new f();
  int f;
  int g;
  int h;
  int i;
  String j;
  
  public a(Context paramContext)
  {
    this.b = paramContext.getApplicationContext();
    this.a = PreferenceManager.getDefaultSharedPreferences(paramContext);
    this.j = k.a("prefDeviceName");
  }
  
  private void b()
  {
    label180:
    String str1;
    String[] arrayOfString;
    if (this.d.a("/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"))
    {
      a("prefCPUMaxPF", this.c.a("/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq"));
      a("prefGPUMaxPF", this.c.a(r.e[this.i]));
      a("prefGPUMinPF", this.c.a(r.f[this.f]));
      a("prefGovPF", this.c.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"));
      if (this.d.a(r.a[this.g])) {
        a("prefgboostPF", this.c.a(r.a[this.g]));
      }
      a("prefGPUGovPF", this.c.a(r.h[this.h]));
      if (!c("prefCPUMaxPrefPF"))
      {
        if (!this.j.equals(this.b.getString(2131624159))) {
          break label611;
        }
        a("prefCPUMaxPrefPF", "2295000");
      }
      str1 = a("prefCPUMaxPrefPF");
      arrayOfString = this.d.a(r.g[this.i], 0, 0);
      if (!c("prefGPUMaxPrefPF"))
      {
        if (!this.j.equals(this.b.getString(2131624159))) {
          break label683;
        }
        if (Integer.parseInt(this.c.a(r.e[this.i])) >= 852000000) {
          break label659;
        }
        a("prefGPUMaxPrefPF", arrayOfString[14]);
      }
      label266:
      if (!this.d.a("/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq")) {
        break label777;
      }
      this.c.a(str1, "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq");
      label288:
      if (!this.j.equals(this.b.getString(2131624159))) {
        break label845;
      }
      this.c.a(a("prefGPUMaxPrefPF"), r.e[this.i]);
      this.c.a("1", "/sys/kernel/tegra_gpu/gpu_cap_state");
    }
    for (;;)
    {
      if (!this.j.equals(this.b.getString(2131624155))) {
        this.c.a("2", r.f[this.f]);
      }
      if ((!this.j.equals(this.b.getString(2131624159))) && (!this.j.equals(this.b.getString(2131624155))))
      {
        String str2 = a("prefCPUGovPF");
        this.c.a(str2, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor");
        this.c.a(str2, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_governor");
        if (this.d.a("/sys/devices/system/cpu/cpu2/cpufreq/scaling_governor"))
        {
          this.c.a(str2, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_governor");
          this.c.a(str2, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_governor");
        }
      }
      if (this.d.a(r.a[this.g])) {
        this.c.a("1", r.a[this.g]);
      }
      if ((this.j.equals(this.b.getString(2131624157))) || (this.j.equals(this.b.getString(2131624111)))) {
        this.c.a("simple", r.h[this.h]);
      }
      return;
      if (this.d.a("/sys/module/cpu_tegra/parameters/cpu_user_cap"))
      {
        a("prefCPUMaxPF", this.c.a("/sys/module/cpu_tegra/parameters/cpu_user_cap"));
        break;
      }
      a("prefCPUMaxPF", this.c.a("/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq"));
      break;
      label611:
      if (this.j.equals(this.b.getString(2131624155)))
      {
        a("prefCPUMaxPrefPF", "2649600");
        break label180;
      }
      a("prefCPUMaxPrefPF", this.c.a("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq"));
      break label180;
      label659:
      a("prefGPUMaxPrefPF", this.c.a(r.e[this.i]));
      break label266;
      label683:
      if ((this.j.equals(this.b.getString(2131624157))) || (this.j.equals(this.b.getString(2131624111))))
      {
        if (arrayOfString.length > 3)
        {
          try
          {
            a("prefGPUMaxPrefPF", arrayOfString[5]);
          }
          catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
          {
            a("prefGPUMaxPrefPF", arrayOfString[0]);
          }
          break label266;
        }
        a("prefGPUMaxPrefPF", arrayOfString[0]);
        break label266;
      }
      a("prefGPUMaxPrefPF", arrayOfString[0]);
      break label266;
      label777:
      if (this.d.a("/sys/module/cpu_tegra/parameters/cpu_user_cap"))
      {
        this.c.a(str1, "/sys/module/cpu_tegra/parameters/cpu_user_cap");
        break label288;
      }
      this.c.a(str1, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq");
      this.c.a(str1, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_max_freq");
      this.c.a(str1, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_max_freq");
      this.c.a(str1, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_max_freq");
      break label288;
      label845:
      this.c.a(a("prefGPUMaxPrefPF"), r.e[this.i]);
    }
  }
  
  private void c()
  {
    if (this.d.a("/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq")) {
      this.c.a(a("prefCPUMaxPF"), "/sys/devices/system/cpu/cpu0/cpufreq/ex_max_freq");
    }
    for (;;)
    {
      this.c.a(a("prefGPUMaxPF"), r.e[this.i]);
      if (this.d.a("/sys/kernel/tegra_gpu/gpu_cap_state")) {
        this.c.a("1", "/sys/kernel/tegra_gpu/gpu_cap_state");
      }
      if (!this.j.equals(this.b.getString(2131624155))) {
        this.c.a(a("prefGPUMinPF"), r.f[this.f]);
      }
      if ((!this.j.equals(this.b.getString(2131624159))) && (!this.j.equals(this.b.getString(2131624155))))
      {
        this.c.a(a("prefGovPF"), "/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor");
        this.c.a("1", "/sys/devices/system/cpu/cpu1/online");
        this.c.a(a("prefGovPF"), "/sys/devices/system/cpu/cpu1/cpufreq/scaling_governor");
        this.c.a("1", "/sys/devices/system/cpu/cpu2/online");
        this.c.a(a("prefGovPF"), "/sys/devices/system/cpu/cpu2/cpufreq/scaling_governor");
        this.c.a("1", "/sys/devices/system/cpu/cpu3/online");
        this.c.a(a("prefGovPF"), "/sys/devices/system/cpu/cpu3/cpufreq/scaling_governor");
      }
      if (this.d.a(r.a[this.g])) {
        this.c.a(a("prefgboostPF"), r.a[this.g]);
      }
      if ((this.j.equals(this.b.getString(2131624157))) || (this.j.equals(this.b.getString(2131624111)))) {
        this.c.a(a("prefGPUGovPF"), r.h[this.h]);
      }
      return;
      if (this.d.a("/sys/module/cpu_tegra/parameters/cpu_user_cap"))
      {
        this.c.a(a("prefCPUMaxPF"), "/sys/module/cpu_tegra/parameters/cpu_user_cap");
      }
      else
      {
        String str = a("prefCPUMaxPF");
        this.c.a(str, "/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq");
        this.c.a(str, "/sys/devices/system/cpu/cpu1/cpufreq/scaling_max_freq");
        this.c.a(str, "/sys/devices/system/cpu/cpu2/cpufreq/scaling_max_freq");
        this.c.a(str, "/sys/devices/system/cpu/cpu3/cpufreq/scaling_max_freq");
      }
    }
  }
  
  private void d()
  {
    AppWidgetManager localAppWidgetManager = AppWidgetManager.getInstance(this.b);
    ComponentName localComponentName = new ComponentName(this.b.getApplicationContext(), PerformanceWidgetProvider.class);
    Intent localIntent = new Intent(this.b, PerformanceWidgetProvider.class);
    int[] arrayOfInt = localAppWidgetManager.getAppWidgetIds(localComponentName);
    localIntent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
    localIntent.putExtra("appWidgetIds", arrayOfInt);
    this.b.sendBroadcast(localIntent);
  }
  
  public String a(String paramString)
  {
    return this.a.getString(paramString, null);
  }
  
  public void a()
  {
    Intent localIntent = new Intent("flar2.exkernelmanager.performance.DISABLE_PERFORMANCE");
    PendingIntent localPendingIntent = PendingIntent.getBroadcast(this.b, 0, localIntent, 0);
    Notification localNotification = new Notification.Builder(this.b).setOngoing(true).setContentTitle(this.b.getString(2131624173)).setContentText(this.b.getString(2131624174)).setSmallIcon(2130837628).setContentIntent(localPendingIntent).setShowWhen(false).build();
    localNotification.flags = (0x10 | localNotification.flags);
    Context localContext = this.b;
    ((NotificationManager)localContext.getSystemService("notification")).notify(101, localNotification);
  }
  
  public void a(String paramString1, String paramString2)
  {
    SharedPreferences.Editor localEditor = this.a.edit();
    localEditor.putString(paramString1, paramString2);
    localEditor.commit();
  }
  
  public void a(String paramString, boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = this.a.edit();
    localEditor.putBoolean(paramString, paramBoolean);
    localEditor.commit();
  }
  
  public void a(boolean paramBoolean)
  {
    flar2.exkernelmanager.powersave.a locala = new flar2.exkernelmanager.powersave.a(this.b);
    this.f = this.c.a(r.f);
    this.g = this.c.a(r.a);
    this.h = this.c.a(r.h);
    this.i = this.c.a(r.e);
    if (paramBoolean)
    {
      if (b("prefPowersaver").booleanValue()) {
        locala.a(false);
      }
      b();
      a("prefPerformance", true);
      b(true);
    }
    for (;;)
    {
      d();
      return;
      c();
      a("prefPerformance", false);
      b(false);
    }
  }
  
  public Boolean b(String paramString)
  {
    return Boolean.valueOf(this.a.getBoolean(paramString, false));
  }
  
  public void b(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      if (b("prefPerformanceNotify").booleanValue()) {
        a();
      }
      return;
    }
    Context localContext = this.b;
    ((NotificationManager)localContext.getSystemService("notification")).cancel(101);
  }
  
  public boolean c(String paramString)
  {
    return this.a.contains(paramString);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/performance/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */