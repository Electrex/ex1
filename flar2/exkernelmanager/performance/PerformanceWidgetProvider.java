package flar2.exkernelmanager.performance;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;

public class PerformanceWidgetProvider
  extends AppWidgetProvider
{
  SharedPreferences a;
  
  public Boolean a(String paramString)
  {
    return Boolean.valueOf(this.a.getBoolean(paramString, false));
  }
  
  public void onUpdate(Context paramContext, AppWidgetManager paramAppWidgetManager, int[] paramArrayOfInt)
  {
    this.a = PreferenceManager.getDefaultSharedPreferences(paramContext);
    int[] arrayOfInt = paramAppWidgetManager.getAppWidgetIds(new ComponentName(paramContext, PerformanceWidgetProvider.class));
    int i = arrayOfInt.length;
    int j = 0;
    if (j < i)
    {
      int k = arrayOfInt[j];
      RemoteViews localRemoteViews = new RemoteViews(paramContext.getPackageName(), 2130903107);
      label82:
      Intent localIntent;
      if (a("prefPerformance").booleanValue())
      {
        localRemoteViews.setImageViewResource(2131493222, 2130837630);
        localIntent = new Intent(paramContext, PerformanceReceiver.class);
        if (!a("prefPerformance").booleanValue()) {
          break label156;
        }
        localIntent.setAction("flar2.exkernelmanager.performance.DISABLE_PERFORMANCE");
      }
      for (;;)
      {
        localRemoteViews.setOnClickPendingIntent(2131493222, PendingIntent.getBroadcast(paramContext, 0, localIntent, 134217728));
        paramAppWidgetManager.updateAppWidget(k, localRemoteViews);
        j++;
        break;
        localRemoteViews.setImageViewResource(2131493222, 2130837629);
        break label82;
        label156:
        localIntent.setAction("flar2.exkernelmanager.performance.ENABLE_PERFORMANCE");
      }
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/performance/PerformanceWidgetProvider.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */