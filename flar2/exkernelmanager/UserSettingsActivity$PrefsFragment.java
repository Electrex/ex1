package flar2.exkernelmanager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.v7.app.ab;
import android.support.v7.app.ac;
import flar2.exkernelmanager.BatteryMonitor.BatteryMonitorService;
import flar2.exkernelmanager.utilities.k;
import java.io.File;

public class UserSettingsActivity$PrefsFragment
  extends PreferenceFragment
{
  private Context a;
  
  private void a()
  {
    Preference localPreference = findPreference("prefTempUnit");
    if (k.a("prefTempUnit").equals("2"))
    {
      localPreference.setSummary(this.a.getString(2131624095));
      return;
    }
    localPreference.setSummary(this.a.getString(2131624000));
  }
  
  private void b()
  {
    ac localac = new ac(getActivity());
    localac.a(this.a.getString(2131624053)).b(this.a.getString(2131624052)).a(false).b(getString(2131623999), new al(this)).a(getString(2131624170), new ak(this));
    localac.a().show();
  }
  
  private void c()
  {
    ac localac = new ac(getActivity());
    localac.a(this.a.getString(2131624310)).b(this.a.getString(2131624309)).a(false).b(getString(2131623999), new an(this)).a(getString(2131624170), new am(this));
    localac.a().show();
  }
  
  private void d()
  {
    File[] arrayOfFile = new File(Environment.getExternalStorageDirectory().getPath() + "/ElementalX").listFiles();
    for (int i = 0; i < arrayOfFile.length; i++) {
      if (arrayOfFile[i].toString().endsWith(".zip")) {
        arrayOfFile[i].delete();
      }
    }
  }
  
  private void e()
  {
    getActivity().stopService(new Intent(getActivity(), BatteryMonitorService.class));
    new flar2.exkernelmanager.powersave.a(this.a).a(false);
    new flar2.exkernelmanager.performance.a(this.a).a(false);
    PreferenceManager.getDefaultSharedPreferences(this.a).edit().clear().commit();
    getActivity().finishAffinity();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.a = MainApp.a();
    addPreferencesFromResource(2131165187);
    if (k.a("prefSubVersion").equals("Sense"))
    {
      PreferenceCategory localPreferenceCategory = (PreferenceCategory)findPreference("catPower");
      localPreferenceCategory.removePreference(findPreference("prefPowersaverNotify"));
      localPreferenceCategory.removePreference(findPreference("prefAutoPowersave"));
    }
  }
  
  public boolean onPreferenceTreeClick(PreferenceScreen paramPreferenceScreen, Preference paramPreference)
  {
    if (paramPreference.getKey().equals("prefTempUnit"))
    {
      if (k.a("prefTempUnit").equals("1"))
      {
        k.a("prefTempUnit", "2");
        paramPreference.setSummary(this.a.getString(2131624095));
        return true;
      }
      k.a("prefTempUnit", "1");
      paramPreference.setSummary(this.a.getString(2131624000));
      return true;
    }
    if (paramPreference.getKey().equals("prefTutorial"))
    {
      startActivity(new Intent(getActivity(), TutorialActivity.class));
      return true;
    }
    if (paramPreference.getKey().equals("prefDeleteDownloads"))
    {
      b();
      return true;
    }
    if (paramPreference.getKey().equals("prefDeleteData"))
    {
      c();
      return true;
    }
    return super.onPreferenceTreeClick(paramPreferenceScreen, paramPreference);
  }
  
  public void onResume()
  {
    super.onResume();
    a();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/UserSettingsActivity$PrefsFragment.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */