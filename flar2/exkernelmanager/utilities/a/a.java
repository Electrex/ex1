package flar2.exkernelmanager.utilities.a;

import android.content.Context;
import android.os.Build.VERSION;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;

public abstract class a
  extends GestureDetector.SimpleOnGestureListener
  implements View.OnTouchListener
{
  private final GestureDetector a = new GestureDetector(paramContext, this);
  private final int b = a(paramContext);
  private float c;
  private boolean d;
  private boolean e;
  
  public a(Context paramContext) {}
  
  protected int a(Context paramContext)
  {
    if (Build.VERSION.SDK_INT < 8) {
      return 2 * ViewConfiguration.getTouchSlop();
    }
    return ViewConfiguration.get(paramContext).getScaledPagingTouchSlop();
  }
  
  public abstract void a();
  
  public void a(boolean paramBoolean)
  {
    this.e = paramBoolean;
  }
  
  public abstract void b();
  
  public boolean onDown(MotionEvent paramMotionEvent)
  {
    this.c = paramMotionEvent.getY();
    return false;
  }
  
  public boolean onScroll(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
  {
    boolean bool1 = true;
    if (this.e) {}
    float f;
    label92:
    label98:
    do
    {
      return false;
      boolean bool2 = this.d;
      boolean bool3;
      if (paramFloat2 > 0.0F)
      {
        bool3 = bool1;
        if (bool2 != bool3) {
          if (this.d) {
            break label92;
          }
        }
      }
      for (;;)
      {
        this.d = bool1;
        this.c = paramMotionEvent2.getY();
        f = this.c - paramMotionEvent2.getY();
        if (f >= -this.b) {
          break label98;
        }
        a();
        return false;
        bool3 = false;
        break;
        bool1 = false;
      }
    } while (f <= this.b);
    b();
    return false;
  }
  
  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    this.a.onTouchEvent(paramMotionEvent);
    return false;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/utilities/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */