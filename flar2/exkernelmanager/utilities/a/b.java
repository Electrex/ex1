package flar2.exkernelmanager.utilities.a;

import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;

public class b
  extends a
  implements Animation.AnimationListener
{
  private final View a;
  private final View b;
  private final View c;
  private final int d;
  private final int e;
  
  public b(View paramView1, View paramView2, View paramView3, int paramInt1, int paramInt2)
  {
    super(paramView1.getContext());
    this.a = paramView1;
    this.b = paramView2;
    this.c = paramView3;
    this.d = paramInt1;
    this.e = paramInt2;
  }
  
  private void a(int paramInt)
  {
    if (paramInt != 0)
    {
      Animation localAnimation = AnimationUtils.loadAnimation(this.a.getContext(), paramInt);
      localAnimation.setAnimationListener(this);
      this.a.startAnimation(localAnimation);
      this.b.startAnimation(localAnimation);
      a(true);
    }
  }
  
  public void a()
  {
    if (this.a.getVisibility() != 0)
    {
      this.a.setVisibility(0);
      this.b.setVisibility(0);
      if (this.c != null) {
        this.c.setVisibility(8);
      }
      a(this.d);
    }
  }
  
  public void b()
  {
    if (this.a.getVisibility() == 0)
    {
      a(this.e);
      new Handler().postDelayed(new c(this), 200L);
    }
  }
  
  public void onAnimationEnd(Animation paramAnimation)
  {
    a(false);
  }
  
  public void onAnimationRepeat(Animation paramAnimation) {}
  
  public void onAnimationStart(Animation paramAnimation) {}
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/utilities/a/b.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */