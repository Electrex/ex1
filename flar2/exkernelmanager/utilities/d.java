package flar2.exkernelmanager.utilities;

import android.os.Build;
import android.os.Build.VERSION;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class d
{
  private void a(String paramString)
  {
    k.a("prefSubVersion", paramString);
  }
  
  private void b(String paramString)
  {
    k.a("prefDeviceName", paramString);
  }
  
  private String c()
  {
    return k.a("prefSubVersion");
  }
  
  private String d()
  {
    return k.a("prefDeviceName");
  }
  
  public String a()
  {
    String str1 = System.getProperty("os.version");
    k.a("prefCurrentKernel", str1);
    a("null");
    File localFile = new File("/system/build.prop");
    if (!localFile.canRead())
    {
      a("incompatible");
      b("unknown");
      return "incompatible";
    }
    String str2 = null;
    for (;;)
    {
      Scanner localScanner;
      try
      {
        localScanner = new Scanner(localFile);
        if (localScanner.hasNextLine())
        {
          str2 = localScanner.nextLine();
          boolean bool = str2.contains("ro.product.device=");
          if (!bool) {
            continue;
          }
        }
        if (str2 == null)
        {
          a("incompatible");
          b("unknown");
          if (!c().equals("incompatible")) {
            break;
          }
          return "incompatible";
        }
      }
      catch (IOException localIOException)
      {
        localIOException.printStackTrace();
        b("unknown");
        return "incompatible";
      }
      if (str2.contains("m7")) {
        for (;;)
        {
          String str6;
          if (localScanner.hasNextLine())
          {
            str6 = localScanner.nextLine();
            if (!str6.contains("SENSE")) {
              break label191;
            }
            a("Sense");
          }
          for (;;)
          {
            k.a("prefHTC", true);
            b("HTC_One_m7");
            break;
            label191:
            if (str6.contains("_google"))
            {
              a("GPE");
            }
            else if (str6.contains("cyanogenmod"))
            {
              a("incompatible");
            }
            else
            {
              if ((!str6.contains("m7wls")) && (!str6.contains("m7wlv"))) {
                break label258;
              }
              a("incompatible");
            }
          }
          label258:
          a("incompatible");
        }
      }
      if (str2.contains("m8")) {
        for (;;)
        {
          String str5;
          if (localScanner.hasNextLine())
          {
            str5 = localScanner.nextLine();
            if (!str5.contains("SENSE")) {
              break label322;
            }
            a("Sense");
          }
          for (;;)
          {
            k.a("prefHTC", true);
            b("HTC_One_m8");
            break;
            label322:
            if (str5.contains("_google"))
            {
              a("GPE");
            }
            else if (str5.contains("cyanogenmod"))
            {
              a("incompatible");
            }
            else
            {
              if ((!str5.contains("m8wls")) && (!str5.contains("m8wlv"))) {
                break label389;
              }
              a("incompatible");
            }
          }
          label389:
          a("incompatible");
        }
      }
      if (Build.MODEL.equals("HTC One M9")) {
        for (;;)
        {
          String str4;
          if (localScanner.hasNextLine())
          {
            str4 = localScanner.nextLine();
            if (!str4.contains("SENSE")) {
              break label455;
            }
            a("Sense");
          }
          for (;;)
          {
            k.a("prefHTC", true);
            b("HTC_One_m9");
            break;
            label455:
            if (!str4.contains("cyanogenmod")) {
              break label474;
            }
            a("incompatible");
          }
          label474:
          a("incompatible");
        }
      }
      if (str2.contains("shamu"))
      {
        a("AOSP");
        b("Nexus6");
      }
      else
      {
        if (str2.contains("hammerhead")) {
          for (;;)
          {
            if (localScanner.hasNextLine())
            {
              String str3 = localScanner.nextLine();
              if ((str3.contains("cyanogenmod")) || (str3.contains("beanstalk")) || (str3.contains("aicp_"))) {
                a("CAF");
              }
            }
            else
            {
              if (str1.contains("cm")) {
                a("CAF");
              }
              b("Nexus5");
              break;
            }
            a("AOSP");
          }
        }
        if (str2.contains("flounder"))
        {
          a("AOSP");
          b("Nexus9");
        }
        else
        {
          if (str2.contains("flo")) {
            for (;;)
            {
              if (localScanner.hasNextLine())
              {
                if (localScanner.nextLine().contains("cyanogenmod")) {
                  a("CAF");
                }
              }
              else
              {
                if (str1.contains("cm")) {
                  a("CAF");
                }
                b("Nexus7");
                break;
              }
              a("AOSP");
            }
          }
          if (str2.contains("deb")) {
            for (;;)
            {
              if (localScanner.hasNextLine())
              {
                if (localScanner.nextLine().contains("cyanogenmod")) {
                  a("CAF");
                }
              }
              else
              {
                if (str1.contains("cm")) {
                  a("CAF");
                }
                b("Nexus7");
                break;
              }
              a("AOSP");
            }
          }
          if (str2.contains("ville")) {
            for (;;)
            {
              if (localScanner.hasNextLine())
              {
                if (localScanner.nextLine().contains("SENSE")) {
                  a("Sense");
                }
              }
              else
              {
                k.a("prefHTC", true);
                b("HTC_One_S");
                break;
              }
              a("incompatible");
            }
          }
          b("unknown");
          a("incompatible");
        }
      }
    }
    b();
    return "compatible";
  }
  
  public void b()
  {
    k.a("prefCheckVersion", "http://elementalx.org/kernels/" + d() + "/" + Build.VERSION.RELEASE + "/" + c() + "/");
    k.a("prefCheckChangelog", "http://elementalx.org/kernels/" + d() + "/" + Build.VERSION.RELEASE + "/" + c() + "/" + "changelog");
    k.a("prefChecksum", "http://elementalx.org/kernels/" + d() + "/" + Build.VERSION.RELEASE + "/" + c() + "/" + "checksum");
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/utilities/d.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */