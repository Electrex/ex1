package flar2.exkernelmanager.utilities;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.support.v4.view.ViewPager;
import android.support.v4.view.bo;
import android.support.v4.view.do;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import flar2.exkernelmanager.t;

public class CircleIndicator
  extends LinearLayout
  implements do
{
  private ViewPager a;
  private do b;
  private int c;
  private int d;
  private int e;
  private int f = 2131034114;
  private int g = -1;
  private int h = 2130837666;
  private int i = 2130837666;
  private int j = 0;
  private Animator k;
  private Animator l;
  private int m;
  
  public CircleIndicator(Context paramContext)
  {
    super(paramContext);
    a(paramContext, null);
  }
  
  public CircleIndicator(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a(paramContext, paramAttributeSet);
  }
  
  private void a(int paramInt, Animator paramAnimator)
  {
    if (paramAnimator.isRunning()) {
      paramAnimator.end();
    }
    View localView = new View(getContext());
    localView.setBackgroundResource(paramInt);
    addView(localView, this.d, this.e);
    LinearLayout.LayoutParams localLayoutParams = (LinearLayout.LayoutParams)localView.getLayoutParams();
    localLayoutParams.leftMargin = this.c;
    localLayoutParams.rightMargin = this.c;
    localView.setLayoutParams(localLayoutParams);
    paramAnimator.setTarget(localView);
    paramAnimator.start();
  }
  
  private void a(Context paramContext, AttributeSet paramAttributeSet)
  {
    setOrientation(0);
    setGravity(17);
    b(paramContext, paramAttributeSet);
    this.k = AnimatorInflater.loadAnimator(paramContext, this.f);
    if (this.g == -1)
    {
      this.l = AnimatorInflater.loadAnimator(paramContext, this.f);
      this.l.setInterpolator(new c(this, null));
      return;
    }
    this.l = AnimatorInflater.loadAnimator(paramContext, this.g);
  }
  
  private void a(ViewPager paramViewPager)
  {
    removeAllViews();
    this.m = paramViewPager.getAdapter().b();
    if (this.m <= 0) {}
    for (;;)
    {
      return;
      a(this.h, this.k);
      for (int n = 1; n < -1 + this.m; n++) {
        a(this.i, this.l);
      }
    }
  }
  
  private void b(Context paramContext, AttributeSet paramAttributeSet)
  {
    if (paramAttributeSet != null)
    {
      TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, t.CircleIndicator);
      this.d = localTypedArray.getDimensionPixelSize(0, -1);
      this.e = localTypedArray.getDimensionPixelSize(1, -1);
      this.c = localTypedArray.getDimensionPixelSize(2, -1);
      this.f = localTypedArray.getResourceId(3, 2131034114);
      this.g = localTypedArray.getResourceId(4, -1);
      this.h = localTypedArray.getResourceId(5, 2130837666);
      this.i = localTypedArray.getResourceId(6, this.h);
      localTypedArray.recycle();
    }
    int n;
    int i1;
    if (this.d == -1)
    {
      n = a(5.0F);
      this.d = n;
      if (this.e != -1) {
        break label175;
      }
      i1 = a(5.0F);
      label138:
      this.e = i1;
      if (this.c != -1) {
        break label184;
      }
    }
    label175:
    label184:
    for (int i2 = a(5.0F);; i2 = this.c)
    {
      this.c = i2;
      return;
      n = this.d;
      break;
      i1 = this.e;
      break label138;
    }
  }
  
  public int a(float paramFloat)
  {
    return (int)(0.5F + paramFloat * getResources().getDisplayMetrics().density);
  }
  
  public void a(int paramInt)
  {
    if (this.b != null) {
      this.b.a(paramInt);
    }
    if (this.l.isRunning()) {
      this.l.end();
    }
    if (this.k.isRunning()) {
      this.k.end();
    }
    View localView1 = getChildAt(this.j);
    localView1.setBackgroundResource(this.i);
    this.l.setTarget(localView1);
    this.l.start();
    if (paramInt < -1 + this.m)
    {
      View localView2 = getChildAt(paramInt);
      localView2.setBackgroundResource(this.h);
      this.k.setTarget(localView2);
      this.k.start();
    }
    this.j = paramInt;
  }
  
  public void a(int paramInt1, float paramFloat, int paramInt2)
  {
    if (this.b != null) {
      this.b.a(paramInt1, paramFloat, paramInt2);
    }
  }
  
  public void b(int paramInt)
  {
    if (this.b != null) {
      this.b.b(paramInt);
    }
  }
  
  public void setOnPageChangeListener(do paramdo)
  {
    if (this.a == null) {
      throw new NullPointerException("can not find Viewpager , setViewPager first");
    }
    this.b = paramdo;
    this.a.setOnPageChangeListener(this);
  }
  
  public void setViewPager(ViewPager paramViewPager)
  {
    this.a = paramViewPager;
    this.j = this.a.getCurrentItem();
    a(paramViewPager);
    this.a.setOnPageChangeListener(this);
    a(this.j);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/utilities/CircleIndicator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */