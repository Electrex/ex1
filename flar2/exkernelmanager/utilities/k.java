package flar2.exkernelmanager.utilities;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import flar2.exkernelmanager.MainApp;

public class k
{
  public static SharedPreferences a = PreferenceManager.getDefaultSharedPreferences(MainApp.a());
  private static boolean b;
  
  public static String a(String paramString)
  {
    return a.getString(paramString, null);
  }
  
  public static void a(String paramString, int paramInt)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putInt(paramString, paramInt);
    localEditor.commit();
  }
  
  public static void a(String paramString, long paramLong)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putLong(paramString, paramLong);
    localEditor.commit();
  }
  
  public static void a(String paramString1, String paramString2)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putString(paramString1, paramString2);
    localEditor.commit();
  }
  
  public static void a(String paramString, boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putBoolean(paramString, paramBoolean);
    localEditor.commit();
  }
  
  public static void a(boolean paramBoolean)
  {
    b = paramBoolean;
  }
  
  public static boolean a()
  {
    return b;
  }
  
  public static Boolean b(String paramString)
  {
    return Boolean.valueOf(a.getBoolean(paramString, false));
  }
  
  public static int c(String paramString)
  {
    return a.getInt(paramString, 0);
  }
  
  public static long d(String paramString)
  {
    return a.getLong(paramString, 0L);
  }
  
  public static boolean e(String paramString)
  {
    return a.contains(paramString);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/utilities/k.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */