package flar2.exkernelmanager.utilities;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class h
  implements View.OnTouchListener
{
  private final GestureDetector a = new GestureDetector(paramContext, new j(this, null));
  
  public h(Context paramContext) {}
  
  public void a() {}
  
  public GestureDetector b()
  {
    return this.a;
  }
  
  public void c() {}
  
  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    return this.a.onTouchEvent(paramMotionEvent);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/utilities/h.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */