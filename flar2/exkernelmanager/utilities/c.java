package flar2.exkernelmanager.utilities;

import android.view.animation.Interpolator;

class c
  implements Interpolator
{
  private c(CircleIndicator paramCircleIndicator) {}
  
  public float getInterpolation(float paramFloat)
  {
    return Math.abs(1.0F - paramFloat);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/utilities/c.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */