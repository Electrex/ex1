package flar2.exkernelmanager.utilities;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build.VERSION;
import android.os.Environment;
import android.util.TypedValue;
import android.widget.Toast;
import com.e.a.b.e;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class f
{
  public static int a(String paramString, int paramInt)
  {
    try
    {
      int i = Integer.parseInt(paramString);
      return i;
    }
    catch (Exception localException) {}
    return paramInt;
  }
  
  private void a(ZipInputStream paramZipInputStream, String paramString)
  {
    BufferedOutputStream localBufferedOutputStream = new BufferedOutputStream(new FileOutputStream(paramString));
    byte[] arrayOfByte = new byte['က'];
    for (;;)
    {
      int i = paramZipInputStream.read(arrayOfByte);
      if (i == -1) {
        break;
      }
      localBufferedOutputStream.write(arrayOfByte, 0, i);
    }
    localBufferedOutputStream.close();
  }
  
  private boolean a(com.e.a.b.a parama)
  {
    while (!parama.e()) {
      try
      {
        if (!parama.e()) {
          parama.wait(2000L);
        }
        if ((!parama.d()) && (!parama.e())) {
          return false;
        }
      }
      catch (InterruptedException localInterruptedException)
      {
        for (;;)
        {
          localInterruptedException.printStackTrace();
        }
      }
      finally {}
    }
    return true;
  }
  
  /* Error */
  private void b(com.e.a.b.a parama)
  {
    // Byte code:
    //   0: bipush 50
    //   2: istore_2
    //   3: invokestatic 71	java/lang/System:currentTimeMillis	()J
    //   6: pop2
    //   7: aload_1
    //   8: invokevirtual 51	com/e/a/b/a:e	()Z
    //   11: ifne +51 -> 62
    //   14: iload_2
    //   15: sipush 3200
    //   18: if_icmpgt +44 -> 62
    //   21: aload_1
    //   22: monitorenter
    //   23: aload_1
    //   24: invokevirtual 51	com/e/a/b/a:e	()Z
    //   27: ifne +13 -> 40
    //   30: aload_1
    //   31: iload_2
    //   32: i2l
    //   33: invokevirtual 57	java/lang/Object:wait	(J)V
    //   36: iload_2
    //   37: iconst_2
    //   38: imul
    //   39: istore_2
    //   40: aload_1
    //   41: monitorexit
    //   42: goto -35 -> 7
    //   45: astore 6
    //   47: aload_1
    //   48: monitorexit
    //   49: aload 6
    //   51: athrow
    //   52: astore 5
    //   54: aload 5
    //   56: invokevirtual 63	java/lang/InterruptedException:printStackTrace	()V
    //   59: goto -19 -> 40
    //   62: return
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	63	0	this	f
    //   0	63	1	parama	com.e.a.b.a
    //   2	38	2	i	int
    //   52	3	5	localInterruptedException	InterruptedException
    //   45	5	6	localObject	Object
    // Exception table:
    //   from	to	target	type
    //   23	36	45	finally
    //   40	42	45	finally
    //   47	49	45	finally
    //   54	59	45	finally
    //   23	36	52	java/lang/InterruptedException
  }
  
  public String a(String paramString)
  {
    e locale = new e(0, false, new String[] { paramString });
    try
    {
      com.e.a.b.f.d(locale);
      if (!a(locale)) {
        return null;
      }
      return locale.toString();
    }
    catch (Exception localException) {}
    return null;
  }
  
  public void a()
  {
    File localFile = new File(Environment.getExternalStorageDirectory().getPath() + "/ElementalX");
    if (!localFile.exists()) {
      localFile.mkdir();
    }
    if (!localFile.exists()) {
      a("mkdir /sdcard/ElementalX");
    }
  }
  
  public void a(Context paramContext, String paramString)
  {
    String str1 = paramContext.getFilesDir().getPath();
    AssetManager localAssetManager;
    String str2;
    if (!new File(Environment.getExternalStorageDirectory().getPath() + "/ElementalX/kcal_profiles/stock").exists())
    {
      localAssetManager = paramContext.getAssets();
      str2 = "color_profiles_flo.zip";
      if (!paramString.equals("Nexus6")) {
        break label185;
      }
      str2 = "color_profiles_shamu.zip";
    }
    label185:
    label217:
    for (;;)
    {
      try
      {
        InputStream localInputStream = localAssetManager.open(str2);
        FileOutputStream localFileOutputStream = new FileOutputStream(new File(str1, str2));
        byte[] arrayOfByte = new byte['Ѐ'];
        int i = localInputStream.read(arrayOfByte);
        if (i > 0)
        {
          localFileOutputStream.write(arrayOfByte, 0, i);
          continue;
        }
        try
        {
          b(str1 + "/" + str2, Environment.getExternalStorageDirectory().getPath() + "/ElementalX/kcal_profiles");
          return;
        }
        catch (IOException localIOException2)
        {
          localIOException2.printStackTrace();
          a("cp -r " + str1 + "/kcal_profiles /sdcard/ElementalX/");
        }
      }
      catch (IOException localIOException1) {}
      for (;;)
      {
        if (!paramString.equals("Nexus7")) {
          break label217;
        }
        str2 = "color_profiles_flo.zip";
        break;
        localInputStream.close();
        localFileOutputStream.flush();
        localFileOutputStream.close();
      }
    }
  }
  
  public void a(String paramString1, String paramString2)
  {
    a("chmod " + paramString1 + " " + paramString2);
  }
  
  public boolean a(Context paramContext)
  {
    NetworkInfo localNetworkInfo = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
    if ((localNetworkInfo != null) && (localNetworkInfo.isConnectedOrConnecting()))
    {
      if ((!k.b("prefWiFi").booleanValue()) || (localNetworkInfo.getType() == 1)) {
        return true;
      }
      Toast.makeText(paramContext, paramContext.getString(2131624308), 0).show();
      return false;
    }
    Toast.makeText(paramContext, paramContext.getString(2131624162), 0).show();
    return false;
  }
  
  public int b(Context paramContext)
  {
    TypedValue localTypedValue = new TypedValue();
    paramContext.getTheme().resolveAttribute(16843499, localTypedValue, true);
    int i = TypedValue.complexToDimensionPixelSize(localTypedValue.data, paramContext.getResources().getDisplayMetrics());
    if (Build.VERSION.SDK_INT < 21) {
      i += 24;
    }
    return i;
  }
  
  public String b(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    g localg = new g(this, 0, false, new String[] { paramString }, localStringBuilder);
    try
    {
      com.e.a.b.f.d(localg);
      b(localg);
      return localStringBuilder.toString();
    }
    catch (Exception localException) {}
    return null;
  }
  
  public void b(Context paramContext, String paramString)
  {
    String str1 = paramContext.getFilesDir().getPath();
    AssetManager localAssetManager = paramContext.getAssets();
    String str2;
    InputStream localInputStream;
    FileOutputStream localFileOutputStream;
    if ((Build.VERSION.SDK_INT > 20) && (k.a("prefDeviceName").equals("Nexus5")))
    {
      str2 = "thermal-engine-8974.conf";
      try
      {
        localInputStream = localAssetManager.open(paramString);
        File localFile = new File(str1, str2);
        System.out.println(localFile.getName());
        localFileOutputStream = new FileOutputStream(localFile);
        byte[] arrayOfByte = new byte['Ѐ'];
        for (;;)
        {
          int i = localInputStream.read(arrayOfByte);
          if (i <= 0) {
            break;
          }
          localFileOutputStream.write(arrayOfByte, 0, i);
        }
        a("mount -o remount,rw /system");
      }
      catch (IOException localIOException) {}
    }
    for (;;)
    {
      a("cp " + str1 + "/" + str2 + " /system/etc/");
      a("mount -o remount,ro /system");
      return;
      str2 = "thermal-engine.conf";
      break;
      localInputStream.close();
      localFileOutputStream.flush();
      localFileOutputStream.close();
    }
  }
  
  public void b(String paramString1, String paramString2)
  {
    File localFile = new File(paramString2);
    if (!localFile.exists()) {
      localFile.mkdir();
    }
    ZipInputStream localZipInputStream = new ZipInputStream(new FileInputStream(paramString1));
    ZipEntry localZipEntry = localZipInputStream.getNextEntry();
    if (localZipEntry != null)
    {
      String str = paramString2 + File.separator + localZipEntry.getName();
      if (!localZipEntry.isDirectory()) {
        a(localZipInputStream, str);
      }
      for (;;)
      {
        localZipInputStream.closeEntry();
        localZipEntry = localZipInputStream.getNextEntry();
        break;
        new File(str).mkdir();
      }
    }
    localZipInputStream.close();
  }
  
  public boolean b()
  {
    return com.e.a.a.a();
  }
  
  public void c(Context paramContext)
  {
    String str = paramContext.getFilesDir().getPath();
    if (!new File(Environment.getExternalStorageDirectory().getPath() + "/ElementalX/color_profiles/0rigin").exists())
    {
      AssetManager localAssetManager = paramContext.getAssets();
      try
      {
        InputStream localInputStream = localAssetManager.open("color_profiles.zip");
        FileOutputStream localFileOutputStream = new FileOutputStream(new File(str, "color_profiles.zip"));
        byte[] arrayOfByte = new byte['Ѐ'];
        for (;;)
        {
          int i = localInputStream.read(arrayOfByte);
          if (i <= 0) {
            break;
          }
          localFileOutputStream.write(arrayOfByte, 0, i);
        }
        try
        {
          b(str + "/color_profiles.zip", Environment.getExternalStorageDirectory().getPath() + "/ElementalX/color_profiles");
          return;
        }
        catch (IOException localIOException2)
        {
          localIOException2.printStackTrace();
          a("cp -r " + str + "/color_profiles /sdcard/ElementalX/");
        }
      }
      catch (IOException localIOException1) {}
    }
    for (;;)
    {
      localInputStream.close();
      localFileOutputStream.flush();
      localFileOutputStream.close();
    }
  }
  
  public void c(String paramString)
  {
    e locale = new e(0, false, new String[] { paramString });
    try
    {
      com.e.a.b.f.d(locale);
      return;
    }
    catch (Exception localException) {}
  }
  
  public void d(Context paramContext)
  {
    String str = paramContext.getFilesDir().getPath();
    File localFile = new File(str + "/reboot");
    if (localFile.exists())
    {
      localFile.setExecutable(true);
      return;
    }
    AssetManager localAssetManager = paramContext.getAssets();
    InputStream localInputStream;
    FileOutputStream localFileOutputStream;
    try
    {
      localInputStream = localAssetManager.open("reboot");
      localFileOutputStream = new FileOutputStream(new File(str, "reboot"));
      byte[] arrayOfByte = new byte['Ѐ'];
      for (;;)
      {
        int i = localInputStream.read(arrayOfByte);
        if (i <= 0) {
          break;
        }
        localFileOutputStream.write(arrayOfByte, 0, i);
      }
      localFile.setExecutable(true);
    }
    catch (IOException localIOException) {}
    for (;;)
    {
      return;
      localInputStream.close();
      localFileOutputStream.flush();
      localFileOutputStream.close();
    }
  }
  
  public void d(String paramString)
  {
    try
    {
      new File(Environment.getExternalStorageDirectory().getPath() + "/ElementalX/kernel_backups/" + paramString).delete();
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }
  
  public String e(String paramString)
  {
    String str1 = flar2.exkernelmanager.r.b[new m().a(flar2.exkernelmanager.r.b)];
    String str2 = Environment.getExternalStorageDirectory().getPath() + "/ElementalX/kernel_backups/" + paramString;
    return new f().b("dd if=\"" + str2 + "\" of=" + str1);
  }
  
  public void e(Context paramContext)
  {
    String str1 = paramContext.getFilesDir().getPath();
    String str2 = str1 + "/reboot";
    try
    {
      a("sync");
      a(str2 + " recovery");
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }
  
  public void f(Context paramContext)
  {
    String str1 = paramContext.getFilesDir().getPath();
    String str2 = str1 + "/reboot";
    try
    {
      a("sync");
      a(str2);
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/utilities/f.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */