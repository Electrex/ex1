package flar2.exkernelmanager.utilities;

import android.content.Context;
import android.os.SystemClock;
import android.widget.Toast;
import flar2.exkernelmanager.MainApp;
import flar2.exkernelmanager.r;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class m
{
  f a = new f();
  e b = new e();
  Toast c;
  
  public int a(String[] paramArrayOfString)
  {
    for (int i = 0; i < paramArrayOfString.length; i++) {
      if (new File(paramArrayOfString[i]).exists()) {
        return i;
      }
    }
    return 0;
  }
  
  public String a(n paramn)
  {
    int i = a(r.q);
    return this.b.a(r.q[i], 0, 0)[paramn.ordinal()];
  }
  
  /* Error */
  public final String a(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnull +17 -> 18
    //   4: new 27	java/io/File
    //   7: dup
    //   8: aload_1
    //   9: invokespecial 30	java/io/File:<init>	(Ljava/lang/String;)V
    //   12: invokevirtual 34	java/io/File:exists	()Z
    //   15: ifne +6 -> 21
    //   18: ldc 57
    //   20: areturn
    //   21: new 59	java/io/BufferedReader
    //   24: dup
    //   25: new 61	java/io/FileReader
    //   28: dup
    //   29: aload_1
    //   30: invokespecial 62	java/io/FileReader:<init>	(Ljava/lang/String;)V
    //   33: sipush 512
    //   36: invokespecial 65	java/io/BufferedReader:<init>	(Ljava/io/Reader;I)V
    //   39: astore_2
    //   40: aload_2
    //   41: invokevirtual 69	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   44: astore 5
    //   46: aload_2
    //   47: invokevirtual 72	java/io/BufferedReader:close	()V
    //   50: aload 5
    //   52: areturn
    //   53: astore 4
    //   55: aload_0
    //   56: getfield 19	flar2/exkernelmanager/utilities/m:a	Lflar2/exkernelmanager/utilities/f;
    //   59: new 74	java/lang/StringBuilder
    //   62: dup
    //   63: invokespecial 75	java/lang/StringBuilder:<init>	()V
    //   66: ldc 77
    //   68: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   71: aload_1
    //   72: invokevirtual 81	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   75: invokevirtual 84	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   78: invokevirtual 86	flar2/exkernelmanager/utilities/f:b	(Ljava/lang/String;)Ljava/lang/String;
    //   81: areturn
    //   82: astore_3
    //   83: aload_2
    //   84: invokevirtual 72	java/io/BufferedReader:close	()V
    //   87: aload_3
    //   88: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	89	0	this	m
    //   0	89	1	paramString	String
    //   39	45	2	localBufferedReader	BufferedReader
    //   82	6	3	localObject	Object
    //   53	1	4	localIOException	IOException
    //   44	7	5	str	String
    // Exception table:
    //   from	to	target	type
    //   21	40	53	java/io/IOException
    //   46	50	53	java/io/IOException
    //   83	89	53	java/io/IOException
    //   40	46	82	finally
  }
  
  public void a(int paramInt, n paramn)
  {
    int i = a(r.q);
    String[] arrayOfString = this.b.a(r.q[i], 0, 0);
    arrayOfString[paramn.ordinal()] = Integer.toString(paramInt);
    a(arrayOfString[n.a.ordinal()] + " " + arrayOfString[n.b.ordinal()] + " " + arrayOfString[n.c.ordinal()], r.q[i]);
    if (k.c("prefkcalPath") == 0) {
      a("1", r.p[i]);
    }
    if (k.c("prefkcalPath") == 1)
    {
      if (this.c != null) {
        this.c.cancel();
      }
      this.c = Toast.makeText(MainApp.a(), MainApp.a().getString(2131624200), 0);
      this.c.setGravity(16, 0, 0);
      this.c.show();
    }
  }
  
  public void a(String paramString1, String paramString2)
  {
    this.a.a("echo " + paramString1 + " > " + paramString2);
  }
  
  public final boolean a()
  {
    if (("/proc/swaps" == null) || (!new File("/proc/swaps").exists())) {}
    while (!this.b.e("cat " + "/proc/swaps").contains("zram0")) {
      return false;
    }
    return true;
  }
  
  public final String[] a(String paramString, boolean paramBoolean)
  {
    arrayOfString = new String[0];
    ArrayList localArrayList = new ArrayList();
    if (paramBoolean) {
      localArrayList.add(Long.toString((SystemClock.elapsedRealtime() - SystemClock.uptimeMillis()) / 10L));
    }
    try
    {
      BufferedReader localBufferedReader = new BufferedReader(new FileReader(paramString), 512);
      try
      {
        String str;
        for (Object localObject2 = localBufferedReader.readLine(); localObject2 != null; localObject2 = str)
        {
          localArrayList.add(localObject2);
          str = localBufferedReader.readLine();
        }
        return (String[])localArrayList.toArray(new String[localArrayList.size()]);
      }
      finally
      {
        localBufferedReader.close();
      }
      return arrayOfString;
    }
    catch (IOException localIOException) {}
  }
  
  public final String b(String paramString)
  {
    if (paramString == null) {
      return "NA";
    }
    return this.a.b("cat " + paramString);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/utilities/m.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */