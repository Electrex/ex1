package flar2.exkernelmanager;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.a;
import android.support.v7.app.ad;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import flar2.exkernelmanager.utilities.h;

public class UserSettingsActivity
  extends ad
{
  h n;
  private Context o;
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    this.n.b().onTouchEvent(paramMotionEvent);
    return super.dispatchTouchEvent(paramMotionEvent);
  }
  
  public void onBackPressed()
  {
    super.onBackPressed();
    overridePendingTransition(0, 2130968604);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    overridePendingTransition(2130968605, 17432577);
    this.o = MainApp.a();
    setContentView(2130903076);
    a((Toolbar)findViewById(2131492951));
    g().a(true);
    this.n = new aj(this, this);
    findViewById(2131493051).setOnTouchListener(this.n);
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(paramMenuItem);
    }
    onBackPressed();
    return true;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/UserSettingsActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */