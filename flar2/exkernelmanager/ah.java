package flar2.exkernelmanager;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint({"ValidFragment"})
public class ah
  extends Fragment
{
  public ah(TutorialActivity paramTutorialActivity) {}
  
  public View a(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    this.aa.getWindow().setFlags(67108864, 67108864);
    this.aa.getWindow().addFlags(Integer.MIN_VALUE);
    View localView = paramLayoutInflater.inflate(2130903094, paramViewGroup, false);
    int i = b().getInt("section_number");
    ((ImageView)localView.findViewById(2131493162)).setImageResource(TutorialActivity.g()[i]);
    TextView localTextView1 = (TextView)localView.findViewById(2131493160);
    TextView localTextView2 = (TextView)localView.findViewById(2131493161);
    switch (i)
    {
    default: 
      return localView;
    case 0: 
      localTextView1.setText("SWIPE");
      localTextView2.setText("from left edge to open nav drawer or return to previous screen");
      return localView;
    case 1: 
      localTextView1.setText("TAP");
      localTextView2.setText("on cards for shortcuts to other parts of the app");
      return localView;
    case 2: 
      localTextView1.setText("PULL");
      localTextView2.setText("down to refresh screens with changing content");
      return localView;
    case 3: 
      localTextView1.setText("TAP");
      localTextView2.setText("the icon beside a setting to apply it each time you reboot");
      return localView;
    }
    localTextView1.setVisibility(8);
    localTextView2.setVisibility(8);
    return localView;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/ah.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */