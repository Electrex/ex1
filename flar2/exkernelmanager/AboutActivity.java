package flar2.exkernelmanager;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ab;
import android.support.v7.app.ac;
import android.support.v7.app.ad;
import android.support.v7.widget.Toolbar;
import android.text.util.Linkify;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import flar2.exkernelmanager.utilities.h;

public class AboutActivity
  extends ad
{
  com.a.a.a.a.c n;
  h o;
  String p = "$2.00 CAD";
  String q = "$5.00 CAD";
  String r = "$10.00 CAD";
  private boolean s = false;
  
  private void b(String paramString)
  {
    Toast.makeText(this, paramString, 1).show();
  }
  
  private void k()
  {
    View localView = getLayoutInflater().inflate(2130903106, null);
    ac localac = new ac(this);
    localac.a(localView);
    localac.a(getString(2131623955));
    TextView localTextView1 = (TextView)localView.findViewById(2131493205);
    localTextView1.setLinkTextColor(Color.parseColor("#4270c3"));
    Linkify.addLinks(localTextView1, 15);
    TextView localTextView2 = (TextView)localView.findViewById(2131493206);
    localTextView2.setLinkTextColor(Color.parseColor("#4270c3"));
    Linkify.addLinks(localTextView2, 15);
    TextView localTextView3 = (TextView)localView.findViewById(2131493208);
    localTextView3.setLinkTextColor(Color.parseColor("#4270c3"));
    Linkify.addLinks(localTextView3, 15);
    TextView localTextView4 = (TextView)localView.findViewById(2131493210);
    localTextView4.setLinkTextColor(Color.parseColor("#4270c3"));
    Linkify.addLinks(localTextView4, 15);
    TextView localTextView5 = (TextView)localView.findViewById(2131493212);
    localTextView5.setLinkTextColor(Color.parseColor("#4270c3"));
    Linkify.addLinks(localTextView5, 15);
    TextView localTextView6 = (TextView)localView.findViewById(2131493214);
    localTextView6.setLinkTextColor(Color.parseColor("#4270c3"));
    Linkify.addLinks(localTextView6, 15);
    TextView localTextView7 = (TextView)localView.findViewById(2131493216);
    localTextView7.setLinkTextColor(Color.parseColor("#4270c3"));
    Linkify.addLinks(localTextView7, 15);
    TextView localTextView8 = (TextView)localView.findViewById(2131493218);
    localTextView8.setLinkTextColor(Color.parseColor("#4270c3"));
    Linkify.addLinks(localTextView8, 15);
    TextView localTextView9 = (TextView)localView.findViewById(2131493220);
    localTextView9.setLinkTextColor(Color.parseColor("#4270c3"));
    Linkify.addLinks(localTextView9, 15);
    localac.a(getString(2131624170), new c(this));
    localac.a().show();
  }
  
  private void l()
  {
    View localView = getLayoutInflater().inflate(2130903082, null);
    ac localac = new ac(this);
    localac.a(localView);
    localac.a(getString(2131623953));
    ((TextView)localView.findViewById(2131493069)).setText(this.p);
    ((TextView)localView.findViewById(2131493072)).setText(this.q);
    ((TextView)localView.findViewById(2131493075)).setText(this.r);
    localView.findViewById(2131493067).setOnClickListener(new d(this));
    localView.findViewById(2131493070).setOnClickListener(new e(this));
    localView.findViewById(2131493073).setOnClickListener(new f(this));
    localac.a().show();
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    this.o.b().onTouchEvent(paramMotionEvent);
    return super.dispatchTouchEvent(paramMotionEvent);
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (!this.n.a(paramInt1, paramInt2, paramIntent)) {
      super.onActivityResult(paramInt1, paramInt2, paramIntent);
    }
  }
  
  public void onBackPressed()
  {
    super.onBackPressed();
    overridePendingTransition(0, 2130968604);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    overridePendingTransition(2130968605, 17432577);
    setContentView(2130903065);
    a((Toolbar)findViewById(2131492951));
    g().a(true);
    this.o = new a(this, this);
    findViewById(2131492950).setOnTouchListener(this.o);
    this.n = new com.a.a.a.a.c(this, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAm5DGkWq4RC8dQJwEUZTOlWoCPaztDE759fAQmHLFLq8JTXWO4V+lEVdjGdMe8YtOIpEZHJ133SuEiSvCt9YQtJU06sbpjkzSUKFnvLYyv1dPV/HmEn7NOzlYGgD7QjcAuSE4bW5FFI8pa4/KYruDtcZA1UFKiNM2wEYz0b3TSg3cTr1BAivY6jLld9pP1lDRZw0RYJj378oVo2IYuBvVzI+IF0FlrAxylYKqz0c6AQol874y20y21X3J7zg5YoZm2ec+uU2sMntbGczorwLiHYvnL8lFAeTNWixw8KGeTh6YW8+q3PXw3nsx8jcLSvgfuxSp5djp+g9Bf9cx6pE0hQIDAQAB", new b(this));
  }
  
  public void onDestroy()
  {
    if (this.n != null) {
      this.n.c();
    }
    super.onDestroy();
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(paramMenuItem);
    }
    onBackPressed();
    return true;
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/AboutActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */