package flar2.exkernelmanager.CPUTempMonitor;

import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.az;
import flar2.exkernelmanager.MainActivity;
import flar2.exkernelmanager.r;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;

public class CPUTempService
  extends Service
{
  private NotificationManager a;
  private m b;
  private Handler c;
  private BroadcastReceiver d;
  private String e;
  private String f;
  private final int g = 4500;
  private Runnable h = new a(this);
  
  private void a()
  {
    String str1 = this.b.a(this.e).trim();
    if (str1 == null) {
      a(getString(2131624165));
    }
    do
    {
      do
      {
        return;
      } while ((str1.equals("")) && (str1.length() == 0));
      if (str1.length() > 4) {
        str1 = str1.substring(0, -3 + str1.length());
      }
      if (this.f.equals("2"))
      {
        String str2 = String.valueOf(32 + (int)(1.8D * Double.parseDouble(str1)));
        a(str2 + "°F");
        return;
      }
    } while (!this.f.equals("1"));
    a(str1 + "°C");
  }
  
  private void a(String paramString)
  {
    Intent localIntent = new Intent(getApplicationContext(), MainActivity.class);
    localIntent.setAction("android.intent.action.MAIN");
    localIntent.addCategory("android.intent.category.LAUNCHER");
    localIntent.addFlags(268435456);
    az localaz = az.a(this);
    localaz.a(MainActivity.class);
    localaz.a(localIntent);
    PendingIntent localPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, localIntent, 134217728);
    Notification localNotification = new Notification.Builder(getApplicationContext()).setOngoing(true).setContentTitle(paramString).setContentText(getString(2131624210)).setSmallIcon(2130837640).setOnlyAlertOnce(true).setPriority(-2).setContentIntent(localPendingIntent).setShowWhen(false).build();
    localNotification.flags = (0x10 | localNotification.flags);
    this.a.notify(42, localNotification);
  }
  
  private void b()
  {
    this.d = new b(this, null);
    IntentFilter localIntentFilter = new IntentFilter("android.intent.action.SCREEN_ON");
    localIntentFilter.addAction("android.intent.action.SCREEN_OFF");
    registerReceiver(this.d, localIntentFilter);
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  public void onCreate()
  {
    super.onCreate();
    this.b = new m();
    int i = this.b.a(r.c);
    if (Build.MODEL.equals("Nexus 6")) {}
    for (this.e = "/sys/class/thermal/thermal_zone6/temp";; this.e = r.c[i])
    {
      this.a = ((NotificationManager)getSystemService("notification"));
      b();
      this.c = new Handler();
      this.c.post(this.h);
      return;
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    if (this.d != null) {
      unregisterReceiver(this.d);
    }
    this.c.removeCallbacks(this.h);
    this.a.cancel(42);
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    if (paramIntent != null) {}
    for (this.f = paramIntent.getStringExtra("tempUnit");; this.f = k.a("prefTempUnit")) {
      return 1;
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/CPUTempMonitor/CPUTempService.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */