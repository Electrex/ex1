package flar2.exkernelmanager;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings.Secure;
import android.widget.TextView;
import com.b.a.a.a.e;
import com.b.a.a.a.n;
import com.b.a.a.a.p;
import flar2.exkernelmanager.utilities.d;
import flar2.exkernelmanager.utilities.f;
import flar2.exkernelmanager.utilities.k;
import flar2.exkernelmanager.utilities.m;

public class StartActivity
  extends Activity
  implements x
{
  public static boolean b = false;
  private static final byte[] c = { -84, 93, 55, -16, -39, 99, -67, -65, -34, 27, 46, 63, -99, 41, 44, -13, 77, -51, 27, -75 };
  Context a;
  private com.b.a.a.a.a d;
  private e e;
  private n f;
  private ProgressDialog g = null;
  private TextView h;
  private boolean i;
  
  private void d()
  {
    this.e = new ac(this, null);
    this.d = new com.b.a.a.a.a(this, this.f, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAm5DGkWq4RC8dQJwEUZTOlWoCPaztDE759fAQmHLFLq8JTXWO4V+lEVdjGdMe8YtOIpEZHJ133SuEiSvCt9YQtJU06sbpjkzSUKFnvLYyv1dPV/HmEn7NOzlYGgD7QjcAuSE4bW5FFI8pa4/KYruDtcZA1UFKiNM2wEYz0b3TSg3cTr1BAivY6jLld9pP1lDRZw0RYJj378oVo2IYuBvVzI+IF0FlrAxylYKqz0c6AQol874y20y21X3J7zg5YoZm2ec+uU2sMntbGczorwLiHYvnL8lFAeTNWixw8KGeTh6YW8+q3PXw3nsx8jcLSvgfuxSp5djp+g9Bf9cx6pE0hQIDAQAB");
    this.h.setText(getString(2131624003));
    e();
  }
  
  private void e()
  {
    this.d.a(this.e);
  }
  
  private void f()
  {
    android.support.v7.app.ac localac = new android.support.v7.app.ac(this);
    localac.a(getString(2131624128)).a(2130837650).a(false).b(getString(2131624129)).a(getString(2131623996), new aa(this)).c(getString(2131624193), new z(this)).b(getString(2131624005), new y(this));
    localac.a().show();
  }
  
  private void g()
  {
    this.h.setText(getString(2131624242));
    new d().a();
    this.i = k.a.getBoolean("prefFirstRun", true);
    if (this.i)
    {
      k.a("prefFirstRun", false);
      k.a("prefTutorial", false);
      k.a("prefFirstRunMonitor", true);
    }
    com.commonsware.cwac.wakeful.a.a(new DailyListener(), this, false);
    if (k.a("prefSubVersion").equals("Sense")) {
      this.a.getPackageManager().setComponentEnabledSetting(new ComponentName(this, "flar2.exkernelmanager.powersave.PowersaveWidgetProvider"), 2, 1);
    }
    k();
    if (k.a("prefTempUnit") == null) {
      k.a("prefTempUnit", "1");
    }
    if (!k.e("prefCheckForUpdates")) {
      k.a("prefCheckForUpdates", true);
    }
    if (!k.e("prefAutoReboot")) {
      k.a("prefAutoReboot", true);
    }
    if (!k.e("prefInstaller")) {
      k.a("prefInstaller", "express");
    }
    if (!k.e("prefHotplug"))
    {
      if (new f().a("busybox pidof mpdecision").contains("\n")) {
        k.a("prefHotplug", "mpdecision");
      }
    }
    else
    {
      k.a("prefBMSpinnerSelection", 0);
      if (!k.e("prefDimmerPS")) {
        k.a("prefDimmerPS", true);
      }
      if (!k.e("prefVibOptPS")) {
        k.a("prefVibOptPS", true);
      }
      if (!k.e("prefWakePS")) {
        k.a("prefWakePS", true);
      }
      if (!k.e("prefCPUMaxPrefPS"))
      {
        if (!k.a("prefDeviceName").equals("Nexus6")) {
          break label415;
        }
        k.a("prefCPUMaxPrefPS", "1497600");
      }
    }
    for (;;)
    {
      if (!k.e("prefGPUMaxPrefPS")) {
        k.a("prefGPUMaxPrefPS", "320000000");
      }
      if (!k.e("prefCPUGovPF")) {
        k.a("prefCPUGovPF", "ondemand");
      }
      if (!k.e("prefAppVersion")) {
        k.a("prefAppVersion", 1);
      }
      i();
      if (MainApp.b) {
        j();
      }
      if (!k.a()) {
        break label427;
      }
      l();
      return;
      if (new m().b("/sys/devices/platform/msm_sleeper/enabled").equals("1"))
      {
        k.a("prefHotplug", "custom");
        break;
      }
      k.a("prefHotplug", "disabled");
      break;
      label415:
      k.a("prefCPUMaxPrefPS", "1300000");
    }
    label427:
    if ((!this.i) && (h()))
    {
      l();
      return;
    }
    d();
  }
  
  private boolean h()
  {
    return this.f.b();
  }
  
  private void i()
  {
    if (k.c("prefAppVersion") != 5)
    {
      k.a("prefAppVersion", 5);
      j();
      k.a("prefBMStartMarker", -1L);
    }
  }
  
  private void j()
  {
    AlarmManager localAlarmManager = (AlarmManager)this.a.getSystemService("alarm");
    Intent localIntent = new Intent(this.a, PowerService.class);
    PendingIntent localPendingIntent = PendingIntent.getService(this.a, 0, localIntent, 0);
    localAlarmManager.set(2, 9000000L + SystemClock.elapsedRealtime(), localPendingIntent);
  }
  
  private void k()
  {
    if (!k.e("prefPowersaver"))
    {
      k.a("prefPowersaver", false);
      k.a("prefPowersaverNotify", true);
    }
    if (!k.e("prefPerformance"))
    {
      k.a("prefPerformance", false);
      k.a("prefPerformanceNotify", true);
    }
  }
  
  private void l()
  {
    ApplicationInfo localApplicationInfo = getApplication().getApplicationInfo();
    int j = 0x2 & localApplicationInfo.flags;
    localApplicationInfo.flags = j;
    if (j != 0) {}
    for (int k = 1; k != 0; k = 0)
    {
      showDialog(0);
      finish();
      return;
    }
    startActivity(new Intent(this.a, MainActivity.class));
    if (Build.VERSION.SDK_INT < 21) {
      overridePendingTransition(17432576, 17432577);
    }
    o();
    finish();
  }
  
  private void m()
  {
    android.support.v7.app.ac localac = new android.support.v7.app.ac(this);
    localac.a(getString(2131624054)).a(2130837650).a(false).b(getString(2131624055)).a(getString(2131624170), new ab(this));
    localac.a().show();
  }
  
  private void n()
  {
    setRequestedOrientation(14);
  }
  
  private void o()
  {
    setRequestedOrientation(4);
  }
  
  public void a()
  {
    this.h.setText(getString(2131623960));
  }
  
  public void a(int paramInt) {}
  
  public void b() {}
  
  public void c()
  {
    n();
    this.g.dismiss();
    if (!b)
    {
      m();
      return;
    }
    g();
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903078);
    this.a = MainApp.a();
    String str = Settings.Secure.getString(getContentResolver(), "android_id");
    this.f = new n(this, new p(c, getPackageName(), str));
    this.g = new ProgressDialog(this);
    this.g.setMessage(getString(2131624195));
    this.g.setIndeterminate(true);
    this.g.setCancelable(false);
    this.h = ((TextView)findViewById(2131493055));
    this.h.setText(getString(2131624242));
    FragmentManager localFragmentManager = getFragmentManager();
    if ((u)localFragmentManager.findFragmentByTag("root_fragment") == null)
    {
      u localu = new u();
      localFragmentManager.beginTransaction().add(localu, "root_fragment").commit();
    }
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    if (this.d != null) {
      this.d.a();
    }
    this.g.dismiss();
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/StartActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */