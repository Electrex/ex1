package flar2.exkernelmanager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import flar2.exkernelmanager.BatteryMonitor.BatteryMonitorService;
import flar2.exkernelmanager.CPUTempMonitor.CPUTempService;
import flar2.exkernelmanager.utilities.k;

class q
  implements SharedPreferences.OnSharedPreferenceChangeListener
{
  private q(MainActivity paramMainActivity) {}
  
  private void a()
  {
    if (k.b("prefCPUNotify").booleanValue())
    {
      Intent localIntent = new Intent(this.a.getApplicationContext(), CPUTempService.class);
      localIntent.putExtra("tempUnit", k.a("prefTempUnit"));
      this.a.startService(localIntent);
      return;
    }
    this.a.stopService(new Intent(this.a.getApplicationContext(), CPUTempService.class));
  }
  
  private void b()
  {
    if (k.b("prefDisableBatteryMon").booleanValue())
    {
      k.a("prefDisableBatteryMon", true);
      this.a.stopService(new Intent(this.a.getApplicationContext(), BatteryMonitorService.class));
      return;
    }
    k.a("prefDisableBatteryMon", false);
    this.a.startService(new Intent(this.a.getApplicationContext(), BatteryMonitorService.class));
  }
  
  public void onSharedPreferenceChanged(SharedPreferences paramSharedPreferences, String paramString)
  {
    if (paramString.equals("prefCheckForUpdates")) {
      com.commonsware.cwac.wakeful.a.a(new DailyListener(), this.a.getApplicationContext(), false);
    }
    flar2.exkernelmanager.powersave.a locala;
    flar2.exkernelmanager.performance.a locala1;
    if (paramString.equals("prefPowersaverNotify"))
    {
      locala = new flar2.exkernelmanager.powersave.a(this.a.getApplicationContext());
      if ((k.b("prefPowersaverNotify").booleanValue()) && (k.b("prefPowersaver").booleanValue())) {
        locala.b(true);
      }
    }
    else if (paramString.equals("prefPerformanceNotify"))
    {
      locala1 = new flar2.exkernelmanager.performance.a(this.a.getApplicationContext());
      if ((!k.b("prefPerformanceNotify").booleanValue()) || (!k.b("prefPerformance").booleanValue())) {
        break label199;
      }
      locala1.b(true);
    }
    for (;;)
    {
      if ((!paramString.equals("prefColorDashboard")) || (paramString.equals("prefDisableBatteryMon"))) {
        b();
      }
      if (paramString.equals("prefCPUNotify")) {
        a();
      }
      if ((paramString.equals("prefTempUnit")) && (k.b("prefCPUNotify").booleanValue())) {
        a();
      }
      return;
      locala.b(false);
      break;
      label199:
      locala1.b(false);
    }
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/q.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */