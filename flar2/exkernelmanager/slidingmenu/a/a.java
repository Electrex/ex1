package flar2.exkernelmanager.slidingmenu.a;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.cc;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

public class a
  extends cc
{
  public static RecyclerView a;
  public static ArrayList b;
  private ArrayList c;
  private flar2.exkernelmanager.slidingmenu.a d;
  private int e;
  private int f = -1;
  
  public a(ArrayList paramArrayList)
  {
    this.c = paramArrayList;
  }
  
  public int a()
  {
    return this.c.size();
  }
  
  public int a(int paramInt)
  {
    return ((flar2.exkernelmanager.slidingmenu.b.a)this.c.get(paramInt)).a();
  }
  
  public void a(c paramc, int paramInt)
  {
    if (paramc.j == 1)
    {
      paramc.k.setText(((flar2.exkernelmanager.slidingmenu.b.a)this.c.get(paramInt)).b());
      paramc.l.setImageResource(((flar2.exkernelmanager.slidingmenu.b.a)this.c.get(paramInt)).c());
    }
    do
    {
      do
      {
        for (;;)
        {
          paramc.a.setOnClickListener(new b(this, paramInt));
          if (this.e != paramInt) {
            break;
          }
          if (paramc.a != null) {
            paramc.a.setBackgroundColor(paramc.a.getContext().getResources().getColor(2131361798));
          }
          if (paramc.k != null) {
            paramc.k.setTextColor(paramc.k.getContext().getResources().getColor(2131361807));
          }
          return;
          if (paramc.j == 2)
          {
            paramc.k.setText(((flar2.exkernelmanager.slidingmenu.b.a)this.c.get(paramInt)).b());
            paramc.l.setImageResource(((flar2.exkernelmanager.slidingmenu.b.a)this.c.get(paramInt)).c());
          }
        }
      } while (paramInt <= 0);
      if (paramc.a != null) {
        paramc.a.setBackgroundColor(0);
      }
    } while (paramc.k == null);
    paramc.k.setTextColor(-1);
  }
  
  public c c(ViewGroup paramViewGroup, int paramInt)
  {
    if (paramInt == 0) {
      return new c(LayoutInflater.from(paramViewGroup.getContext()).inflate(2130903085, paramViewGroup, false), paramInt);
    }
    if (paramInt == 1) {
      return new c(LayoutInflater.from(paramViewGroup.getContext()).inflate(2130903086, paramViewGroup, false), paramInt);
    }
    if (paramInt == 2) {
      return new c(LayoutInflater.from(paramViewGroup.getContext()).inflate(2130903087, paramViewGroup, false), paramInt);
    }
    if (paramInt == 3) {
      return new c(LayoutInflater.from(paramViewGroup.getContext()).inflate(2130903084, paramViewGroup, false), paramInt);
    }
    return null;
  }
  
  public void f(int paramInt)
  {
    int i = this.e;
    this.e = paramInt;
    c(i);
    c(paramInt);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/slidingmenu/a/a.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */