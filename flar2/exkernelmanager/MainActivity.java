package flar2.exkernelmanager;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ab;
import android.support.v7.app.ad;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.cl;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import flar2.exkernelmanager.BatteryMonitor.BatteryMonitorService;
import flar2.exkernelmanager.CPUTempMonitor.CPUTempService;
import flar2.exkernelmanager.fragments.cz;
import flar2.exkernelmanager.fragments.dk;
import flar2.exkernelmanager.fragments.dy;
import flar2.exkernelmanager.fragments.ep;
import flar2.exkernelmanager.fragments.es;
import flar2.exkernelmanager.fragments.ey;
import flar2.exkernelmanager.fragments.gb;
import flar2.exkernelmanager.fragments.gf;
import flar2.exkernelmanager.utilities.d;
import flar2.exkernelmanager.utilities.e;
import java.util.ArrayList;

public class MainActivity
  extends ad
{
  public static View o;
  public static View p;
  private static final Handler y = new Handler(Looper.getMainLooper());
  private flar2.exkernelmanager.powersave.a A;
  private flar2.exkernelmanager.performance.a B;
  private int C;
  cl n;
  SharedPreferences q;
  q r;
  private DrawerLayout s;
  private android.support.v7.app.f t;
  private Toolbar u;
  private CharSequence v;
  private CharSequence w;
  private int x = 0;
  private boolean z = false;
  
  private final void a(Fragment paramFragment, int paramInt)
  {
    if (paramInt == 0) {
      ((flar2.exkernelmanager.slidingmenu.a.a)flar2.exkernelmanager.slidingmenu.a.a.a.getAdapter()).f(MainApp.a);
    }
    while (MainApp.a == paramInt) {
      return;
    }
    if (paramInt < this.C) {
      MainApp.a = paramInt;
    }
    y.postDelayed(new p(this, paramFragment, paramInt), 220L);
    ((flar2.exkernelmanager.slidingmenu.a.a)flar2.exkernelmanager.slidingmenu.a.a.a.getAdapter()).f(MainApp.a);
  }
  
  private void k()
  {
    android.support.v7.app.ac localac = new android.support.v7.app.ac(this);
    localac.a(getString(2131623988)).a(2130837650).a(false).b(getString(2131623989)).b(getString(2131623999), new m(this)).a("Install", new l(this));
    localac.a().show();
  }
  
  private void l()
  {
    if (!flar2.exkernelmanager.utilities.k.b("prefPowersaver").booleanValue())
    {
      flar2.exkernelmanager.utilities.k.a("prefPowersaver", true);
      this.A.a(true);
      return;
    }
    flar2.exkernelmanager.utilities.k.a("prefPowersaver", false);
    this.A.a(false);
  }
  
  private void m()
  {
    if (!flar2.exkernelmanager.utilities.k.b("prefPerformance").booleanValue())
    {
      flar2.exkernelmanager.utilities.k.a("prefPerformance", true);
      this.B.a(true);
      return;
    }
    flar2.exkernelmanager.utilities.k.a("prefPerformance", false);
    this.B.a(false);
  }
  
  public void a(int paramInt)
  {
    Object localObject;
    if (((String)flar2.exkernelmanager.slidingmenu.a.a.b.get(paramInt)).equals("Dashboard"))
    {
      localObject = new dy();
      if (this.s != null) {
        this.s.b();
      }
      if (!this.z) {
        break label473;
      }
      if (localObject != null) {
        getFragmentManager().beginTransaction().setCustomAnimations(17498112, 17498113).replace(2131493041, (Fragment)localObject).commit();
      }
      if (paramInt >= this.C) {
        break label455;
      }
      ((flar2.exkernelmanager.slidingmenu.a.a)flar2.exkernelmanager.slidingmenu.a.a.a.getAdapter()).f(paramInt);
      label96:
      this.z = false;
    }
    for (;;)
    {
      this.x = 0;
      return;
      if (((String)flar2.exkernelmanager.slidingmenu.a.a.b.get(paramInt)).equals("CPU"))
      {
        localObject = new flar2.exkernelmanager.fragments.ac();
        break;
      }
      if (((String)flar2.exkernelmanager.slidingmenu.a.a.b.get(paramInt)).equals("Graphics"))
      {
        localObject = new cz();
        break;
      }
      if (((String)flar2.exkernelmanager.slidingmenu.a.a.b.get(paramInt)).equals("Wake"))
      {
        localObject = new gf();
        break;
      }
      if (((String)flar2.exkernelmanager.slidingmenu.a.a.b.get(paramInt)).equals("Voltage"))
      {
        localObject = new gb();
        break;
      }
      if (((String)flar2.exkernelmanager.slidingmenu.a.a.b.get(paramInt)).equals("Sound"))
      {
        localObject = new ep();
        break;
      }
      if (((String)flar2.exkernelmanager.slidingmenu.a.a.b.get(paramInt)).equals("Miscellaneous"))
      {
        localObject = new dk();
        break;
      }
      if (((String)flar2.exkernelmanager.slidingmenu.a.a.b.get(paramInt)).equals("Update"))
      {
        localObject = new ey();
        break;
      }
      if (((String)flar2.exkernelmanager.slidingmenu.a.a.b.get(paramInt)).equals("Tools"))
      {
        localObject = new es();
        break;
      }
      if (((String)flar2.exkernelmanager.slidingmenu.a.a.b.get(paramInt)).equals("Settings"))
      {
        Intent localIntent1 = new Intent(this, UserSettingsActivity.class);
        y.postDelayed(new n(this, localIntent1), 225L);
        localObject = null;
        break;
      }
      if (((String)flar2.exkernelmanager.slidingmenu.a.a.b.get(paramInt)).equals("About"))
      {
        Intent localIntent2 = new Intent(this, AboutActivity.class);
        y.postDelayed(new o(this, localIntent2), 225L);
      }
      localObject = null;
      break;
      label455:
      ((flar2.exkernelmanager.slidingmenu.a.a)flar2.exkernelmanager.slidingmenu.a.a.a.getAdapter()).f(MainApp.a);
      break label96;
      label473:
      a((Fragment)localObject, paramInt);
    }
  }
  
  public void onBackPressed()
  {
    if (MainApp.a == 1) {
      finish();
    }
    for (;;)
    {
      if (this.s != null) {
        this.s.b();
      }
      return;
      getFragmentManager().beginTransaction().setCustomAnimations(17498112, 17498113).replace(2131493041, new dy()).commit();
      MainApp.a = 1;
    }
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    if (this.s != null) {
      this.t.a(paramConfiguration);
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903074);
    if (Build.VERSION.SDK_INT >= 21)
    {
      if ((getResources().getBoolean(2131296263)) && ((getResources().getBoolean(2131296267)) || (getResources().getBoolean(2131296266)))) {
        getWindow().setFlags(67108864, 67108864);
      }
    }
    else
    {
      this.u = ((Toolbar)findViewById(2131492951));
      a(this.u);
      o = findViewById(2131493043);
      p = findViewById(2131493047);
      if (!new e().a("/system/xbin/busybox")) {
        k();
      }
      this.q = PreferenceManager.getDefaultSharedPreferences(this);
      this.r = new q(this, null);
      this.q.registerOnSharedPreferenceChangeListener(this.r);
      this.A = new flar2.exkernelmanager.powersave.a(getApplicationContext());
      this.B = new flar2.exkernelmanager.performance.a(getApplicationContext());
      new flar2.exkernelmanager.utilities.f().a();
      CharSequence localCharSequence = getTitle();
      this.v = localCharSequence;
      this.w = localCharSequence;
      flar2.exkernelmanager.slidingmenu.a.a.a = (RecyclerView)findViewById(2131493049);
      flar2.exkernelmanager.slidingmenu.a.a.a.setHasFixedSize(true);
      String[] arrayOfString = getResources().getStringArray(2131230721);
      TypedArray localTypedArray = getResources().obtainTypedArray(2131230720);
      ArrayList localArrayList = new ArrayList();
      flar2.exkernelmanager.slidingmenu.a.a.b = new ArrayList();
      flar2.exkernelmanager.utilities.m localm = new flar2.exkernelmanager.utilities.m();
      e locale = new e();
      localArrayList.add(new flar2.exkernelmanager.slidingmenu.b.a(0));
      flar2.exkernelmanager.slidingmenu.a.a.b.add("Header");
      localArrayList.add(new flar2.exkernelmanager.slidingmenu.b.a(1, arrayOfString[0], localTypedArray.getResourceId(0, -1)));
      flar2.exkernelmanager.slidingmenu.a.a.b.add("Dashboard");
      localArrayList.add(new flar2.exkernelmanager.slidingmenu.b.a(1, arrayOfString[1], localTypedArray.getResourceId(1, -1)));
      flar2.exkernelmanager.slidingmenu.a.a.b.add("CPU");
      localArrayList.add(new flar2.exkernelmanager.slidingmenu.b.a(1, arrayOfString[2], localTypedArray.getResourceId(2, -1)));
      flar2.exkernelmanager.slidingmenu.a.a.b.add("Graphics");
      if ((locale.a(r.m[localm.a(r.m)])) || (locale.a(r.n[localm.a(r.n)])))
      {
        localArrayList.add(new flar2.exkernelmanager.slidingmenu.b.a(1, arrayOfString[3], localTypedArray.getResourceId(3, -1)));
        flar2.exkernelmanager.slidingmenu.a.a.b.add("Wake");
      }
      if (locale.a(r.j[localm.a(r.j)]))
      {
        localArrayList.add(new flar2.exkernelmanager.slidingmenu.b.a(1, arrayOfString[4], localTypedArray.getResourceId(4, -1)));
        flar2.exkernelmanager.slidingmenu.a.a.b.add("Voltage");
      }
      if ((locale.a("/sys/kernel/sound_control_3")) || (locale.a("/sys/devices/virtual/misc/soundcontrol/volume_boost")))
      {
        localArrayList.add(new flar2.exkernelmanager.slidingmenu.b.a(1, arrayOfString[5], localTypedArray.getResourceId(5, -1)));
        flar2.exkernelmanager.slidingmenu.a.a.b.add("Sound");
      }
      localArrayList.add(new flar2.exkernelmanager.slidingmenu.b.a(1, arrayOfString[6], localTypedArray.getResourceId(6, -1)));
      flar2.exkernelmanager.slidingmenu.a.a.b.add("Miscellaneous");
      if (!new d().a().equals("incompatible"))
      {
        localArrayList.add(new flar2.exkernelmanager.slidingmenu.b.a(1, arrayOfString[7], localTypedArray.getResourceId(7, -1)));
        flar2.exkernelmanager.slidingmenu.a.a.b.add("Update");
      }
      if (Build.VERSION.SDK_INT >= 21)
      {
        localArrayList.add(new flar2.exkernelmanager.slidingmenu.b.a(1, arrayOfString[8], localTypedArray.getResourceId(8, -1)));
        flar2.exkernelmanager.slidingmenu.a.a.b.add("Tools");
      }
      localArrayList.add(new flar2.exkernelmanager.slidingmenu.b.a(3));
      flar2.exkernelmanager.slidingmenu.a.a.b.add("Settings");
      localArrayList.add(new flar2.exkernelmanager.slidingmenu.b.a(2, arrayOfString[9], localTypedArray.getResourceId(9, -1)));
      flar2.exkernelmanager.slidingmenu.a.a.b.add("Settings");
      this.C = (-1 + flar2.exkernelmanager.slidingmenu.a.a.b.size());
      localArrayList.add(new flar2.exkernelmanager.slidingmenu.b.a(2, arrayOfString[10], localTypedArray.getResourceId(10, -1)));
      flar2.exkernelmanager.slidingmenu.a.a.b.add("About");
      localTypedArray.recycle();
      flar2.exkernelmanager.slidingmenu.a.a locala = new flar2.exkernelmanager.slidingmenu.a.a(localArrayList);
      flar2.exkernelmanager.slidingmenu.a.a.a.setAdapter(locala);
      this.n = new LinearLayoutManager(this);
      flar2.exkernelmanager.slidingmenu.a.a.a.setLayoutManager(this.n);
      ((flar2.exkernelmanager.slidingmenu.a.a)flar2.exkernelmanager.slidingmenu.a.a.a.getAdapter()).f(MainApp.a);
      this.s = ((DrawerLayout)findViewById(2131493040));
      if (this.s != null)
      {
        if (!flar2.exkernelmanager.utilities.k.b("prefHTC").booleanValue()) {
          break label1278;
        }
        this.s.setStatusBarBackgroundColor(getResources().getColor(2131361799));
      }
      label960:
      if (this.s != null)
      {
        this.s.a(2130837589, 8388611);
        this.t = new i(this, this, this.s, this.u, 2131623974, 2131623974);
        this.s.setDrawerListener(this.t);
        this.t.a();
      }
      GestureDetector localGestureDetector = new GestureDetector(this, new j(this));
      flar2.exkernelmanager.slidingmenu.a.a.a.a(new k(this, localGestureDetector));
      if (getIntent().getStringExtra("update") == null) {
        break label1298;
      }
      if (getIntent().getStringExtra("update").equals("openUpdaterFragment"))
      {
        getIntent().removeExtra("update");
        a(flar2.exkernelmanager.slidingmenu.a.a.b.indexOf("Update"));
      }
      label1114:
      if (flar2.exkernelmanager.utilities.k.b("prefPowersaver").booleanValue()) {
        this.A.b(true);
      }
      if (flar2.exkernelmanager.utilities.k.b("prefPerformance").booleanValue()) {
        this.B.b(true);
      }
      if (!new e().a("/system/xbin/busybox")) {
        k();
      }
      if (!flar2.exkernelmanager.utilities.k.b("prefCPUNotify").booleanValue()) {
        break label1315;
      }
      Intent localIntent = new Intent(this, CPUTempService.class);
      localIntent.putExtra("tempUnit", flar2.exkernelmanager.utilities.k.a("prefTempUnit"));
      startService(localIntent);
      label1219:
      if (flar2.exkernelmanager.utilities.k.b("prefDisableBatteryMon").booleanValue()) {
        break label1334;
      }
      startService(new Intent(this, BatteryMonitorService.class));
    }
    for (;;)
    {
      a.a.a.a.a(this).a(5).a(a.a.a.n.b).a();
      return;
      getWindow().addFlags(67108864);
      break;
      label1278:
      this.s.setStatusBarBackgroundColor(getResources().getColor(2131361800));
      break label960;
      label1298:
      if (paramBundle != null) {
        break label1114;
      }
      this.z = true;
      a(1);
      break label1114;
      label1315:
      stopService(new Intent(this, CPUTempService.class));
      break label1219;
      label1334:
      stopService(new Intent(this, BatteryMonitorService.class));
    }
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(2131755011, paramMenu);
    return true;
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    this.q.unregisterOnSharedPreferenceChangeListener(this.r);
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if ((this.s != null) && (this.t.a(paramMenuItem))) {
      return true;
    }
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131493234: 
      startActivity(new Intent(this, UserSettingsActivity.class));
      return true;
    case 2131493235: 
      startActivity(new Intent(this, AboutActivity.class));
      return true;
    case 2131493247: 
      l();
      return false;
    }
    m();
    return false;
  }
  
  protected void onPostCreate(Bundle paramBundle)
  {
    super.onPostCreate(paramBundle);
    if (this.s != null) {
      this.t.a();
    }
  }
  
  public void setTitle(CharSequence paramCharSequence)
  {
    this.w = paramCharSequence;
    if (((paramCharSequence.equals("Dashboard")) && (getResources().getBoolean(2131296265)) && (!getResources().getBoolean(2131296264))) || (paramCharSequence.equals("Voltage")) || (paramCharSequence.equals("ElementalX")))
    {
      g().a(this.w);
      return;
    }
    if (!getResources().getBoolean(2131296263))
    {
      g().a(null);
      return;
    }
    g().a(this.w);
  }
}


/* Location:              /home/storm/Documents/dex2jar-0.0.9.15/classes-dex2jar.jar!/flar2/exkernelmanager/MainActivity.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */